
/* JavaScript content from js/controller/userInfo.js in folder common */
(function() {
	jq("#userInfoView").live("pagebeforecreate", function() {
		if(mor.ticket.viewControl.tab3_cur_page=="userInfo.html"){
			mor.ticket.viewControl.isNeedRequest=false;
		}
		
		if(mor.ticket.viewControl.isNeedRequest){
			initUserInfo();
		}else{
			var	userInfo = mor.ticket.userInfo;
			userInfoDisplay(userInfo);
			mor.ticket.viewControl.isNeedRequest = true;
		}
		
		function userinfoModifyFn(){
			jq("#modifyUserInfoBtn").off();
				jq("#modifyuserInfoView").remove();
				mor.ticket.util.changePage("modifyUserInfo.html",true);
				setTimeout(function(){
					jq("#modifyUserInfoBtn").off().on("tap",userinfoModifyFn);
				},1000);
				return false;
		}
		jq("#modifyUserInfoBtn").off().on("tap",userinfoModifyFn);	
		jq("#userInfoBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("my12306.html");
			return false;
		});
		
	});
	
	function initUserInfo(){
		var util = mor.ticket.util;
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "initUser"
		};
		
		var options =  {
				onSuccess: requestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		
		mor.ticket.util.invokeWLProcedure(null, invocationData, options);
	}
	
	function requestSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.userInfo = invocationResult;
			var	userInfo = mor.ticket.userInfo;
			userInfoDisplay(userInfo);
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}	
	}
	
	function userInfoDisplay(userInfo){
		var util = mor.ticket.util;
		if(!util.isNoValue(userInfo)){
			jq("#label_username").html("  "+(util.isNoValue(userInfo.user_name)?"":userInfo.user_name));
			//jq("#label_pwd_question").html("  "+(util.isNoValue(userInfo.pwd_question)?"":userInfo.pwd_question));
			jq("#label_name").html("  "+(util.isNoValue(userInfo.name)?"":userInfo.name));
			jq("#label_sex").html("  "+(util.isNoValue(userInfo.sex_code)?"":util.getSexName(userInfo.sex_code)));
			jq("#label_born_date").html("  "+(util.isNoValue(userInfo.born_date)?"":util.changeDateType(userInfo.born_date)));
			if(userInfo.id_type_code=="1" || userInfo.id_type_code=="2" || userInfo.id_type_code=="C" || userInfo.id_type_code=="G"){
				jq("#label_country").html("  "+(util.isNoValue(userInfo.country_code)?"":util.getCountryByCode("CN")));
			}else{
				jq("#label_country").html("  "+(util.isNoValue(userInfo.country_code)?"":util.getCountryByCode(userInfo.country_code)));
			}
			jq("#label_id_type").html("  "+(util.isNoValue(userInfo.id_type_code)?"":util.getIdTypeName(userInfo.id_type_code)));
			if(userInfo.display_control_flag==="1" && userInfo.id_type_code === "1"){
				jq("#label_id_no").html("  "+(util.isNoValue(userInfo.id_no)?"":mor.ticket.util.getIdNoStar(userInfo.id_no)));
			}else{
				jq("#label_id_no").html("  "+(util.isNoValue(userInfo.id_no)?"":userInfo.id_no));
			}
			if( userInfo.display_control_flag=="1" && userInfo.user_status=="1" && (userInfo.id_type_code=="C" || userInfo.id_type_code=="G" || userInfo.id_type_code=="B" || userInfo.id_type_code=="H")){
				jq("#label_ExaminationState").html("预通过");
			}else if(userInfo.display_control_flag==="1"){
				jq("#label_ExaminationState").html("已通过");
			}else if(userInfo.display_control_flag==="2" || userInfo.display_control_flag==="4"){
				jq("#label_ExaminationState").html("未通过");
			}else if(userInfo.display_control_flag==="3"){
				jq("#label_ExaminationState").html("待核验");
			}else if(userInfo.display_control_flag==="5"){
				jq("#label_ExaminationState").html("请报验");
			}else{
				jq("#label_ExaminationState").html("");
			}
			jq("#label_mobile_no").html("  "+(util.isNoValue(userInfo.mobile_no)?"":userInfo.mobile_no));
			jq("#label_phone_no").html("  "+(util.isNoValue(userInfo.phone_no)?"":userInfo.phone_no));
			jq("#label_email").html("  "+(util.isNoValue(userInfo.email)?"":userInfo.email));
			jq("#label_address").html("  "+(util.isNoValue(userInfo.address)?"":userInfo.address));
			jq("#label_postalcode").html("  "+(util.isNoValue(userInfo.postalcode)?"":userInfo.postalcode));
			jq("#label_user_type").html("  "+(util.isNoValue(userInfo.user_type)?"":util.getPassengerTypeName(userInfo.user_type)));
			if(userInfo.user_type=="3"){
				mor.ticket.util.initJSONStoreFromLocal(universityDisplay,cityDisplay);
				jq("#label_studentProvince").html("  "+ util.getProvinceByCode(userInfo.student_province_code));
				jq("#label_studentDepartment").html("  "+userInfo.student_department);
				jq("#label_studentClass").html("  "+userInfo.student_school_class);
				jq("#label_studentNo").html("  "+userInfo.student_student_no);
				jq("#label_studentSystem").html("  "+userInfo.student_school_system);
				jq("#label_studentEnterYear").html("  "+userInfo.student_enter_year);
				jq("#label_preferenceCardNo").html("  "+userInfo.student_card_no);
				jq("#studentOptions").show();
			}else{
				jq("#studentOptions").hide();
				jq("#label_user_type").parent().removeClass("departshortline");
				jq("#label_user_type").parent().addClass("departshortnoline");
			}
			contentIscrollRefresh();
	   }
	}
	
	function universityDisplay(){
		var	userInfo = mor.ticket.userInfo;
		try{
			var query = {university_code:userInfo.student_school_code};
			var options = {exact: true,limit:1};
			WL.JSONStore.get("university").find(query,options)
			.then(function(res){
				jq("#label_studentSchool").html("  "+ res[0].json.university_name);
			})
			.fail(function(errorObject){
				WL.Logger.info(errorObject.msg);
				jq("#label_studentSchool").html("  ");
			});
		}catch(e){
			WL.Logger.info(e.message);
			jq("#label_studentSchool").html("  ");
		}
	}
	
	function cityDisplay(){
		var	userInfo = mor.ticket.userInfo;
		try{
			var query = {city_code:userInfo.student_from_station_code};
			var options = {exact: true,limit:1};
			WL.JSONStore.get("city").find(query,options)
			.then(function(res){
				jq("#label_preferenceFromStation").html("  " + res[0].json.city_name);
			})
			.fail(function(errorObject){
				WL.Logger.info(errorObject.msg);
				jq("#label_preferenceFromStation").html("  ");
			});
		}catch(e){
			WL.Logger.info(e.message);
			jq("#label_preferenceFromStation").html("  ");
		}
		try{
			var query = {city_code:userInfo.student_to_station_code};
			var options = {exact: true,limit:1};
			WL.JSONStore.get("city").find(query,options)
			.then(function(res){
				jq("#label_preferenceToStation").html("  " + res[0].json.city_name);
			})
			.fail(function(errorObject){
				WL.Logger.info(errorObject.msg);
				jq("#label_preferenceToStation").html("  ");
			});
		}catch(e){
			WL.Logger.info(e.message);
			jq("#label_preferenceToStation").html("  ");
		}
	}
	
	function contentIscrollRefresh(){
		jq("#userInfoView .ui-content").iscrollview("refresh");
	}
	
	jq("#userInfoView").live("pagebeforeshow", function() {
		mor.ticket.util.initAppVersionInfo();
		var user = mor.ticket.loginUser;
		if (user.isAuthenticated === "Y") {
			mor.ticket.viewControl.tab3_cur_page="userInfo.html";
		} else {
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
				} else {
				registerAutoLoginHandler(function(){
					mor.ticket.viewControl.tab3_cur_page="userInfo.html";
				}, autologinFailJump);
				}

		}
	});
})();