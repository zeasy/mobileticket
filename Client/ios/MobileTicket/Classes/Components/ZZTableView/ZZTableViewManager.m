//
//  ZZTableViewManager.m
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZTableViewManager.h"
#import "ZZTableView.h"

#import <RCTUIManager.h>
#import <RCTSparseArray.h>
#import "ZZTableViewCellManager.h"

@interface ZZTableViewManager ()

@end

@implementation ZZTableViewManager

RCT_EXPORT_MODULE()

-(UIView<RCTComponent> *)view {
	ZZTableView *tableView = [[ZZTableView alloc] initWithEventDispatcher:self.bridge.eventDispatcher];


	return (UIView<RCTComponent> *) tableView;
}

RCT_CUSTOM_VIEW_PROPERTY(tableViewStyle, UITableViewStyle, ZZTableView) {
	[view setTableViewStyle:[RCTConvert NSInteger:json]];
}

RCT_EXPORT_VIEW_PROPERTY(dataSource, NSDictionary)
RCT_EXPORT_VIEW_PROPERTY(delegate, NSDictionary)

RCT_EXPORT_VIEW_PROPERTY(onReuseCell, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onReuseHeaderFooterView, RCTDirectEventBlock)



//delegate
RCT_EXPORT_VIEW_PROPERTY(onDidSelectRowAtIndexPath, RCTDirectEventBlock)


RCT_EXPORT_VIEW_PROPERTY(onDidSelectedRow, RCTDirectEventBlock)


RCT_EXPORT_METHOD(deselectRow:(NSDictionary *) indexPathDict animated:(BOOL)animated :(nonnull NSNumber *) reactTag :(RCTResponseSenderBlock) success :(RCTResponseErrorBlock) fail) {
	[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
		ZZTableView *tableView = viewRegistry[reactTag];
		if ([tableView isKindOfClass:[ZZTableView class]]) {
			NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[indexPathDict[@"row"] integerValue] inSection:[indexPathDict[@"section"] integerValue]];
			[tableView.tableView deselectRowAtIndexPath:indexPath animated:animated];
			if (success) {
				success(@[@YES]);
			}
		}
	}];
}

RCT_EXPORT_METHOD(reloadData:(nonnull NSNumber *) reactTag dataSource:(NSDictionary *) dataSource)
{
	[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
		ZZTableView *tableView = viewRegistry[reactTag];
		if ([tableView isKindOfClass:[ZZTableView class]]) {
			tableView.dataSource = dataSource;
			[tableView.tableView reloadData];
		}
	}];
}

RCT_EXPORT_METHOD(beginUpdates:(nonnull NSNumber *) reactTag :(RCTResponseSenderBlock) success :(RCTResponseErrorBlock) fail) {
	[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
		ZZTableView *tableView = viewRegistry[reactTag];
		if ([tableView isKindOfClass:[ZZTableView class]]) {
			[tableView.tableView beginUpdates];
			if (success) {
					success(@[@YES]);
			}
		}
	}];
}

RCT_EXPORT_METHOD(endUpdates:(nonnull NSNumber *) reactTag :(RCTResponseSenderBlock) success :(RCTResponseErrorBlock) fail) {
	[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
		ZZTableView *tableView = viewRegistry[reactTag];
		if ([tableView isKindOfClass:[ZZTableView class]]) {
			[tableView.tableView endUpdates];
			if (success) {
				success(@[@YES]);
			}
		}
	}];
}


RCT_EXPORT_METHOD(reloadRows:(NSArray *)indexPathDicts withRowAnimation:(int)animation :(nonnull NSNumber *) reactTag :(RCTResponseSenderBlock) success :(RCTResponseErrorBlock) fail){
	[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
		ZZTableView *tableView = viewRegistry[reactTag];
		if ([tableView isKindOfClass:[ZZTableView class]]) {
			
			NSMutableArray<NSIndexPath *> *indexPaths = [NSMutableArray array];
			for (NSDictionary *dict in indexPathDicts) {
				[indexPaths addObject:[NSIndexPath indexPathForRow:[dict[@"row"] intValue] inSection:[dict[@"section"] intValue]]];
			}
			[tableView.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:animation];
			if (success) {
				success(@[@YES]);
			}
		}
	}];
}

//RCT_EXPORT_METHOD(selectRow:(NSNumber *) reactTag indexPath:(NSDictionary *) indexPath animated:(BOOL) animated scrollPosition:(UITableViewScrollPosition) position) {
//	[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
//		ZZTableView *tableView = viewRegistry[reactTag];
//		if ([tableView isKindOfClass:[ZZTableView class]]) {
//			[tableView.tableView selectRowAtIndexPath:[RCTConvert NSIndexPath:indexPath] animated:animated scrollPosition:position];
//		}
//	}];
//}
//
//RCT_EXPORT_METHOD(deselectRow:(NSNumber *) reactTag indexPath:(NSDictionary *) indexPath animated:(BOOL) animated) {
//	[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
//		ZZTableView *tableView = viewRegistry[reactTag];
//		if ([tableView isKindOfClass:[ZZTableView class]]) {
//			[tableView.tableView deselectRowAtIndexPath:[RCTConvert NSIndexPath:indexPath] animated:animated];
//		}
//	}];
//}
//
//RCT_EXPORT_METHOD(setEditing:(NSNumber *) reactTag editing:(BOOL) editing animated:(BOOL) animated) {
//	[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
//		ZZTableView *tableView = viewRegistry[reactTag];
//		if ([tableView isKindOfClass:[ZZTableView class]]) {
//			[tableView.tableView setEditing:editing animated:animated];
//		}
//	}];
//}



-(NSDictionary *)constantsToExport {
	return @{
					 @"Style" : @{
								@"Plan" : @(UITableViewStylePlain),
								@"Grouped" : @(UITableViewStyleGrouped)
							 },
					 @"RowAnimation":@{
							 @"Automatic":@(UITableViewRowAnimationAutomatic)
							 }
					 }
;
}

@end