
/* JavaScript content from js/anPush.js in folder common */
var channels = ["all"];
var apiServer = "182.92.182.34:7000";
var dsServer = "182.92.182.34:7001";
var appKey = "6PNPTvjPCAETFZMJC3oi2Nk2FOUP4IWI";

// setup callbacks
var sdk = window.plugins.AnPush;
sdk.onRegistered = function(err, anid) {
	if(err) {
		//alert("register error: " + err.message);
	} else {
		if(anid) {
			//alert("anid: " + anid);
			var storeValue = channels.sort().join(",");
			sdk.saveToLocalStorage("channels", storeValue, function(err) {});
			//sdk.saveToLocalStorage("anPush_UserAccount", storeValue, function(err) {});
			enable();
		} else {
			//alert("register error: no anid returned.")
		}
	}
};

sdk.onUnregistered = function(err) {
	if(err) {
		//alert("unregister error: " + err.message);
	} else {
	
	}
};

sdk.onStatusChanged = function(err, status) {
	if(err) {
		//alert("push status error: " + err.message);
	} else {
		//alert("current status: " + status);
	}
};

sdk.onReceivedPushNotification = function(payloads) {
	for(var i = 0; i< payloads.length; i++) {
		var payload = payloads[i];
		if(payload.aps && payload.aps.badge) {
			//sdk.setApplicationIconBadgeNumber(0);
		}
		//alert(JSON.stringify(payload));	//payload是javascript的一个对象字面量
		var value={};
		jq.each(payload,function(index,item){
			index = index.toString().toLowerCase();
			if(index === "aps"){
				value.message = item.alert||"推送内容";
			}else if(index ==="title" || index ==="主题"){
				value.theme = item || "推送主题";
			}else if(index ==="android"){
				value.message = item.alert || "推送内容";
				value.theme = item.title || "推送主题";
			}else{
				value.message = item.alert || "推送内容";
				value.theme = item.title || "推送主题";
			}
		});
		value.time = (new Date(window.ticketStorage.getItem('serverTime'))).toString() || (new Date()).toString();
		value.readOrNo = false;
		//alert("推送消息的键值为:"+index+"。消息的内容为：" + item.alert);
		try{
			insertPushMessage(JSON.stringify(value));	//转化为JSON字符串存储		
		}
		catch(e){
			alert("推送消息存盘出错！相应的推送消息为：" + JSON.stringify(value));
		}
	}
	sdk.setApplicationIconBadgeNumber(0);
};

sdk.onSetMute = function(err) {
	if(err) {
		//alert("setMute error: " + err.message);
	} else {
		//alert("setMute succeed!");
	}
};

sdk.onClearMute = function(err) {
	if(err) {
		//alert("clearMute error: " + err.message);
	} else {
		//alert("clearMute succeed!");
	}
};

sdk.onSetSilentPeriod = function(err) {
	if(err) {
		//alert("setSilentPeriod error: " + err.message);
	} else {
		//alert("setSilentPeriod succeed!");
	}
};

sdk.onClearSilentPeriod = function(err) {
	if(err) {
		//alert("clearSilentPeriod error: " + err.message);
	} else {
		//alert("clearSilentPeriod succeed!");
	}
};
/*
document.addEventListener("deviceready", function(){
	sdk.initialize(appKey, function(err) {
		if(err) {
			//alert(err);
		} else {
			sdk.setServerHosts(apiServer, dsServer, function(err) {
				if(err) {
					//alert(err);
				}
			});
		}
	}, {secureConnection:true,alert: true, badge: true, sound: true});
}, false); 
*/
function insertPushMessage(value){
	var count = window.ticketStorage.pushmessageNum();
	for(var j=count;j>0;j--){
		var getValue = window.ticketStorage.getItem("pushmessage"+(j-1));
		if(getValue.length != 0){
			window.ticketStorage.setItem("pushmessage"+j, getValue);
			window.ticketStorage.removeItem("pushmessage"+(j-1));
		}
	}
	window.ticketStorage.setItem("pushmessage0", value);
}

function registerChannelsall() {
	var storeValue = channels.sort().join(",");
	sdk.getFromLocalStorage("channels", function(err, value) {
		if(value !== storeValue) {
			sdk.register(channels);
		} else {
			console.log("Already registered the channels: " + storeValue);
			enable();
		}
	});
}

function registerChannelsUser(UserAccount) {
	if(UserAccount)
		{
		sdk.register([UserAccount]);
		sdk.getFromLocalStorage("anPush_UserAccount", function(err, value) {
			if(value !== UserAccount) {
				if(value)
					{
					sdk.unregister([value]);
					sdk.saveToLocalStorage("anPush_UserAccount", UserAccount, function(err) {});
					//alert(value);
					}
				else
					{
					var TempChannels = [];
					//alert("TempChannels1:" + channels.length);
					for(var i=0;i<channels.length;i++)
						{
						TempChannels.push(channels[i]);
						}
					//alert("TempChannels2");
					TempChannels.push(UserAccount);
					sdk.register(TempChannels, true);
					sdk.saveToLocalStorage("anPush_UserAccount", UserAccount, function(err) {});
					//alert(TempChannels);
					}
			} else {
				console.log("Already registered the channels: " + UserAccount);
				enable();
			}
		});
		}
	//var storeValue = channels.sort().join(",");
	
}

function unregisterChannelsUser(UserAccount) {
	if(UserAccount)
	{
		sdk.unregister([UserAccount]);
		sdk.saveToLocalStorage("anPush_UserAccount", null, function(err) {});
	}
	
}

function unregister() {
	sdk.unregister();
}

function enable() {
	window.plugins.AnPush.enable();
	/*
	sdk.isEnabled(function(err, isEnabled){
		if(err) {
			//alert(err);
		} else {
			if(!isEnabled) {
				window.plugins.AnPush.enable();
			}
		}
	});
	*/
}

function disable() {
	sdk.disable(function(err){
		if(err) {
			//alert(err.message);
		} 
	});
}

function clearStorage() {
	sdk.saveToLocalStorage("channels", null, function(err) {
		if(err) {
			//alert(err);
		}
	});
}

function setMute() {
	sdk.setMute();
}

function clearMute() {
	sdk.clearMute();
}

function setScheduledMute() {
	sdk.setMute(23, 0, 60 * 8);
}

function setSilentPeriod() {
	sdk.setSilentPeriod(23, 0, 60 * 8, true);
}

function clearSilentPeriod() {
	sdk.clearSilentPeriod();
}

function getAnID() {
	sdk.getAnID(function(err, anid) {
		if(err) {
			//alert(err.message);
		} else {
			alert(anid);
		}
	});
}

function setDeviceId() {
	var deviceId = prompt("Customized Device ID:");
	if(deviceId) {
		sdk.setDeviceId(deviceId, function(err) {
			if(err) {
				//alert(err.message);
			} else {
				document.getElementById("currentDeviceId").innerText = deviceId;
			}
		});
	}
}


function setServerHosts() {
	sdk.setServerHosts("114.255.140.184:8280/1351", "114.255.140.184:8280/1352", function(err) {
		if(err) {
			//alert("set server hosts error");
		}
	});
}