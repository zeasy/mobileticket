
/* JavaScript content from js/controller/agencySellTicketDetail.js in folder common */
(function(){
	jq("#AgencySellTicketDetailView").live("pageinit", function() {
		jq("#AgencySellTicketDetailBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("agencySellTicketList.html");
			return false;
		});
	});
	
	jq("#AgencySellTicketDetailView").live("pagebeforeshow", function(){
		mor.ticket.viewControl.tab1_cur_page="agencySellTicketDetail.html";
		mor.ticket.util.initAppVersionInfo();
		initAgencySellTicketDetails();
	});
	
	function initAgencySellTicketDetails(){
		var agencySellTicket = mor.ticket.currentQueryObject.agencySellTicket;
		var agency_name = agencySellTicket.agency_name;
		var util = mor.ticket.util;
		jq("#agencysell_name").html(agency_name);
		jq("#agencysell_address").html("地址：" + agencySellTicket.address);
		jq("#agencysell_tel").html("电话："+ agencySellTicket.phone_no);
		jq("#agencysell_period").html("营业时间：" + util.formateTrainTime(agencySellTicket.start_time_am) + " -- " + util.formateTrainTime(agencySellTicket.stop_time_pm));
		jq("#agencysell_window").html("窗口数量：" + agencySellTicket.windows_quantity);
	};
	
})();