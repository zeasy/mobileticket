package com.nativeModules;

import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableMap;
import com.nativeModules.utils.JsonConvert;
import com.nativeModules.utils.KeyValuePair;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by easy on 16/3/21.
 */
public class NetworkModule extends ReactContextBaseJavaModule {


    private final CookieManager cookieManager = new CookieManager(null, CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    private final OkHttpClient client = this.getUnsafeOkHttpClient();

    public NetworkModule(ReactApplicationContext reactContext) {
        super(reactContext);

        client.setCookieHandler(cookieManager);




    }

    @ReactMethod
    public void request(ReadableMap options, final Promise promise) throws IOException {
        Log.i("[NETWORK]",options.toString());
        String url = options.getString("url");
        String method = options.getString("method");
        if (method == null) {
            method = "GET";
        }
        method = method.toUpperCase();

        final String format = options.getString("format");

        ReadableMap headersMap = null;
        ReadableArray headersArray = null;
        if (options.getType("headers") == ReadableType.Map) {
            headersMap = options.getMap("headers");
        } else if (options.getType("headers") == ReadableType.Array) {
            headersArray = options.getArray("headers");
        }



        ReadableMap paramsMap = null;
        ReadableArray paramsArray = null;
        if (options.getType("params") == ReadableType.Map) {
            paramsMap = options.getMap("params");
        } else if(options.getType("params") == ReadableType.Array) {
            paramsArray = options.getArray("params");
        }

        ReadableMap cookiesMap = null;
        ReadableArray cookiesArray = null;
        if (options.getType("cookies") == ReadableType.Map) {
            cookiesMap = options.getMap("cookies");
        } else if(options.getType("cookies") == ReadableType.Array) {
            cookiesArray = options.getArray("cookies");
        }



        Request.Builder builder = new Request.Builder();
        //params
        ArrayList<KeyValuePair> paramPairs = (ArrayList<KeyValuePair>) this.pairs(paramsMap,paramsArray);

        boolean get = "GET".equalsIgnoreCase(method);
        if (get) {
            StringBuilder stringBuilder = new StringBuilder(url);
            if (url.indexOf("?") < 0) {
                stringBuilder.append("?");
            }
            for (int i = 0;i < paramPairs.size();i++){
                KeyValuePair pair = paramPairs.get(i);
                stringBuilder.append(pair.getKey() + "=" + pair.getValue());
                if (i < paramPairs.size() - 1) {
                    stringBuilder.append("&");
                }
            }
            Log.i("[NETWORK]",stringBuilder.toString());
            builder.url(stringBuilder.toString()).get();
        } else {
            FormEncodingBuilder formBuilder = new FormEncodingBuilder();
            for (int i = 0;i < paramPairs.size();i++){
                KeyValuePair pair = paramPairs.get(i);
                formBuilder.addEncoded(pair.getKey(),pair.getValue().toString());
            }
            builder.url(url).method(method,formBuilder.build());
        }

        //headers
        ArrayList<KeyValuePair> headerPairs = (ArrayList<KeyValuePair>) this.pairs(headersMap,headersArray);

        for (int i = 0;i < headerPairs.size();i++) {
            KeyValuePair pair = headerPairs.get(i);
            builder.addHeader(pair.getKey(),pair.getValue().toString());
        }

        //cookies
        ArrayList<KeyValuePair> cookiePairs = (ArrayList<KeyValuePair>) this.pairs(cookiesMap,cookiesArray);
        HashMap<String,List<String>> cs = new HashMap<String,List<String>>();
        for (int i = 0;i < cookiePairs.size();i++) {
            KeyValuePair pair = cookiePairs.get(i);
            Object value = pair.getValue();

            ArrayList<String> list = new ArrayList<>();
            if (value instanceof String) {
                list.add(value.toString());
            } else if(value instanceof ReadableArray) {
                for (int j = 0;j < ((ReadableArray) value).size();j++) {
                    list.add(((ReadableArray) value).getString(i));
                }
            }
            cs.put(pair.getKey(),list);
        }

        cookieManager.put(URI.create(url),cs);

        Request request = builder.build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                promise.reject(e);
                Log.w("NETWORK",e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                ResponseBody body = response.body();
                if ("DATA".equalsIgnoreCase(format)) {
                    String result = Base64.encodeToString(body.bytes(),Base64.DEFAULT);
                    promise.resolve(result);
                } else {
                    promise.resolve(body.string());
                }
            }
        });
    }


    List<KeyValuePair> pairs(ReadableMap map,ReadableArray array){
        ArrayList<KeyValuePair> pairs = new ArrayList<KeyValuePair>();
        if (map != null) {
            ReadableMapKeySetIterator iter = map.keySetIterator();
            while(iter.hasNextKey()) {
                String key = iter.nextKey();
                String value = map.getString(key);
                pairs.add(new KeyValuePair(key,value));
            }
        } else if(array != null) {
            for (int i = 0;i < array.size();i++) {
                ReadableMap rmap = array.getMap(i);
                ReadableMapKeySetIterator iter = rmap.keySetIterator();
                while(iter.hasNextKey()) {
                    String key = iter.nextKey();
                    String value = rmap.getString(key);
                    pairs.add(new KeyValuePair(key,value));
                }
            }
        }
        return pairs;
    }

    @Override
    public String getName() {
        return "NativeNetwork";
    }


    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)  {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient client = new OkHttpClient();
            client.setSslSocketFactory(sslSocketFactory);

            client.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            return client;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
