/**
 *
 * web接口版
 * Created by easy on 16/3/17.
 */


'use strict'

module.exports = {
    rootController : ()=>require('./containers/TWRootController'),
    reducers : require('./reducers')
}