
/* JavaScript content from js/controller/about.js in folder common */
(function(){
	jq("#aboutView").live("pageinit", function(){
		clickCount = 0;
		jq("#aboutBackBtn").off().bind("tap",function(){
			mor.ticket.util.changePage("moreOption.html");
			return false;
		});
		jq("#systemInfo").off().bind("tap",function(){
			if(clickCount != 0){
				mor.ticket.util.changePage("pushmessage.html");
				clickCount += 1;
			}
			return false;
		});
	});
	
	jq("#aboutView").live("pagebeforeshow", function(){
		if(window.ticketStorage.getItem("systemNotice") != "Y"){
			jq("#systemInfo").hide();
		}
		registerUpdateAppVersionListener();
		initAppVersionInfo();
		setTimeout(function(){
			clickCount = 1;
		},500);
		mor.ticket.viewControl.tab4_cur_page="about.html";		
	});
	function initAppVersionInfo(){
		var common = mor.ticket.common;
		var noReadNum = 0;
		var count = window.ticketStorage.pushmessageNum();
		for(var i = 0; i < count ; i++){
			var pushmessage = jq.parseJSON(window.ticketStorage.getItem("pushmessage"+i));
			if(pushmessage.readOrNo === false){
				noReadNum++;
			}
			if(noReadNum > 0 ){
				break;
			}
		}
		if(common["baseDTO.app_need_update"] == "true" || common["baseDTO.app_need_update"] == true || noReadNum > 0){
			jq(".circle").show();
			if(common["baseDTO.app_need_update"] == "true" || common["baseDTO.app_need_update"] == true){
				jq(".newVersion_about").show();
			}else{
				jq(".newVersion_about").hide();
			}
			if(noReadNum > 0){
				jq(".pushmessage_noread").show();
			}else{
				jq(".pushmessage_noread").hide();
			}
		}else{
			jq(".circle").hide();
			jq(".newVersion_about").hide();
			jq(".pushmessage_noread").hide();
		}
	}
	
	function registerUpdateAppVersionListener(){
		jq("#updateAppVersion").on("tap",function(){
			if(clickCount == 0){
				return false;
			}
			var common = mor.ticket.common;
			var app_download_url = common["baseDTO.app_download_url"];
			var os_type = common["baseDTO.os_type"];
			if(common["baseDTO.app_need_update"] == "false" || common["baseDTO.app_need_update"] == false){
				var app_version_no = WL.StaticAppProps.APP_VERSION == null ? "0" 
						: WL.StaticAppProps.APP_VERSION;
				var app_version_no_new = common["baseDTO.version_no_new"];
				if(app_version_no_new > app_version_no){
					if(os_type === 'a'){
						navigator.app.loadUrl(app_download_url, {openExternal:true});
					}else if(os_type === 'i'){
						window.open(app_download_url, "_system");
					}
				}else{
					mor.ticket.util.alertMessage("当前版本已是最新版本!");
					return false;
				}
			}else{
				common["baseDTO.app_need_update"] = "false";
				initAppVersionInfo();
				if(os_type === "a"){
					navigator.app.loadUrl(app_download_url, {openExternal:true});
				}
				if(os_type === "i"){
					window.open(app_download_url, "_system");
				}
			}
			clickCount += 1;
		});
	}
})();