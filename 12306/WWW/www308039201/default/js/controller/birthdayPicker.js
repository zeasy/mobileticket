
/* JavaScript content from js/controller/birthdayPicker.js in folder common */
(

function() {

	jq("#birthdayPickerView").live("pageinit", function() {
		registerCalenderPage();
		jq("#birthdayPickBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage(mor.ticket.currentPagePath.fromPath);
			return false;
		});
	});
	jq("#birthdayPickerView").live("pagebeforeshow",function(){
		changePrevAndNextStyle();
		if(mor.ticket.currentPagePath.fromPath == "advanceQueryOrder.html"){
			var innerText = jq("#birthdayPickBackBtn").nextAll();
			innerText.html("选择日期");
		}
	});
	//初始化日历控件
	function changePrevAndNextStyle(){
		if(mor.ticket.currentPagePath.type == false){
			jq(".nextMonth").addClass("birthNextMonth");
			jq(".nextYear").addClass("birthNextYear");
			jq(".preMonth").addClass("birthPreMonth");
			jq(".preYear").addClass("birthPreYear");
			jq(".datePickerContent  .ui-grid-a>.ui-block-b").addClass("birthYearSeclect");
			jq(".datePickerContent  .ui-grid-a>.ui-block-a").addClass("birthMonthSeclect");
		}
	};
	function registerCalenderPage(){
		if(mor.ticket.currentPagePath.fromPath == "advanceQueryOrder.html"){
			if(mor.queryOrder.views.advanceQuery.flag == "fromDateInput")
				{
					if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == false){
						jq("#birthdayPickerInputHidden").val(mor.queryOrder.views.advanceQuery.fromDate);
					}else if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == true){
						jq("#birthdayPickerInputHidden").val(mor.queryOrder.views.advanceQuery.trainFromDate);
					}
				}
			else if(mor.queryOrder.views.advanceQuery.flag == "toDateInput"){
				if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == false){
					jq("#birthdayPickerInputHidden").val(mor.queryOrder.views.advanceQuery.toDate);
				}else if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == true){
					jq("#birthdayPickerInputHidden").val(mor.queryOrder.views.advanceQuery.trainToDate);
				}
			}
			
		}else if(mor.ticket.currentPagePath.fromPath == "modifyPassenger.html"){
			jq("#birthdayPickerInputHidden").val(jq("#modify_PassengerBornDateShow").val());
		}else if(mor.ticket.currentPagePath.fromPath == "modifyUserInfo.html"){
			jq("#birthdayPickerInputHidden").val(jq("#modify_bornDateShow").val());
		}else{
		jq("#birthdayPickerInputHidden").val(jq("#regist_bornDateShow").val());
		}
		jq("#birthdayPickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});

	}	
	
})();