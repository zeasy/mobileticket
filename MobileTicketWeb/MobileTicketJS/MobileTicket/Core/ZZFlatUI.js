/**
 * @providesModule ZZFlatUI
 * Created by easy on 16/1/13.
 */

class ZZFlatUI {
    static turquoise = '#1abc9c';//青绿色
    static emerland = '#2ecc71';//绿宝石
    static peterRiver = '#3498db'; //彼河
    static amethyst = '#9b59b6';//紫水晶
    static wetAsphalt = '#34495e';
    static greenSea = '#16a085';//绿海
    static nephritis = '#27ae60';
    static belizeHole = '#2980b9';
    static wisteria = '#8e44ad';
    static midnightBlue = '#2c3e50';
    static sunFlower = '#f1c40f';
    static carrot = '#e67e22';
    static alizarin = '#e74c3c';
    static clouds = '#ecf0f1';
    static concrete = '#95a5a6';
    static orange = '#f39c12';
    static pumpkin = '#d35400';
    static pomegranate = '#c0392b';
    static silver = '#bdc3c7';
    static asbestos = '#7f8c8d';
}

module.exports = ZZFlatUI;


