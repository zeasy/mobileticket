/**
 * @providesModule ZZHUD
 * Created by easy on 16/1/13.
 */


'use strict';
let Async = require('ZZAsync');
let NativeHUD = require('NativeModules').NativeHUD;

class ZZHUD {
    static async showWithOptions(options) {
        return Async.invoke(NativeHUD.show,options);
    }

    static async hide() {
        return Async.invoke(NativeHUD.hide);
    }

    static async showSuccess(status) {
        return ZZHUD.showWithOptions({type:'success',status});
    }

    static async showError(status) {
        return ZZHUD.showWithOptions({type:'error',status});
    }

    static async showInfo(status) {
        return ZZHUD.showWithOptions({type:'info',status});
    }

    static async show(status) {
        return ZZHUD.showWithOptions({status});
    }

    static async showProgress(progress,status) {
        return ZZHUD.showWithOptions({progress,status});
    }
}

module.exports = ZZHUD;