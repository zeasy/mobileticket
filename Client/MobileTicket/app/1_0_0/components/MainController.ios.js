/**
 * Created by easy on 15/11/2.
 */

var React = require('react-native');
var Components = require('Components');
var {
    StyleSheet,
    NavigatorIOS,
    TabBarIOS,
    } = React;

var {
    Controller
    } = Components;

var TestController = require('./TestController.ios.js');
var MoreController = require('./MoreController.ios.js');
var SearchController = require('./SearchController.ios.js');

class MainController extends Controller {

    constructor(props) {
        super(props);
        this.route.title = 'MainController';
        this.route.navigationBarHidden = true;


        this.state = {
            selectedTab: 'search',
            notifCount: 0,
            presses: 0,
        }
    }

    render() {
        return (
            <TabBarIOS barTintColor="darkslateblue" tintColor="white">
                <TabBarIOS.Item
                    systemIcon="search"
                    selected = {this.state.selectedTab === 'search'}
                    onPress={() => {
                    this.setState({selectedTab:'search'});
                }}
                >
                    <NavigatorIOS ref="nav"
                                  style={{flex:1}}

                                  initialRoute={{
                                    component: SearchController,title:''
                                    }}
                    >
                    </NavigatorIOS>
                </TabBarIOS.Item>

                <TabBarIOS.Item

                    systemIcon="history"
                    selected = {this.state.selectedTab === 'history'}
                    onPress={() => {
                        this.setState({selectedTab:'history'});
                    }}
                >
                    <NavigatorIOS ref="nav"
                                  style={{flex:1}}
                                  initialRoute={{
                                        component: TestController,title:''
                                        }}
                    >
                    </NavigatorIOS>
                </TabBarIOS.Item>

                <TabBarIOS.Item
                    systemIcon="more"
                    selected = {this.state.selectedTab === 'more'}
                    onPress={() => {
                        this.setState({selectedTab:'more'});
                    }}
                >
                    <NavigatorIOS ref="nav"
                                  style={{flex:1}}
                                  initialRoute={{
                                        component: MoreController,title:''
                                        }}
                    >
                    </NavigatorIOS>
                </TabBarIOS.Item>
            </TabBarIOS>
        );
    }
}

exports = module.exports = MainController;
