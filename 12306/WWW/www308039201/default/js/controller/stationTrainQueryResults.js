
/* JavaScript content from js/controller/stationTrainQueryResults.js in folder common */
(function() {
	
	jq("#stationTrainQueryResultsView").live("pageinit", function() {
		registerPrevDateBtnClickHandler();
		registerNextDateBtnClickHandler();
		registSelectDateChangeHandler();
		registerBackButtonHandler();
//		photoButtonHandler();
		

	});
	//new add by zzc begin
	function registSelectDateChangeHandler(){
		jq("#stationTrainQueryResultsView").change(function(){
			var date = mor.ticket.util.setMyDate(jq("#stationTrainChooseDateBtn").val());
			var selectDate = new Date(date.getTime());
			mor.ticket.stationTrainQuery.trainDate = selectDate.format("yyyy-MM-dd");
			searchAndShowResult(mor.ticket.stationTrainQuery);
		});
		jq("#stationTrainDateInputId").bind("tap",function(){
			var current = mor.ticket.currentPagePath;
			var monthValue;
			current.defaultDateValue[0]=jq("#stationTrainChooseDateBtn").val().slice(0,4);
			monthValue=parseInt(jq("#stationTrainChooseDateBtn").val().slice(5,7));
			if(monthValue == "1"){
				monthValue = "0";
			}else monthValue = monthValue-1;
			if(monthValue < 10){
				current.defaultDateValue[1] = "0"+monthValue;
			}else{
				current.defaultDateValue[1] = monthValue;
			}
			current.defaultDateValue[2]=jq("#stationTrainChooseDateBtn").val().slice(8,10);
			var reservePeriod = mor.ticket.history.reservePeriod;
			jQuery.extend(jQuery.mobile.datebox.prototype.options, {
				mode:"calbox",
				useInline: true,
				calControlGroup: true,
				defaultValue: current.defaultDateValue,
				showInitialValue: true,
				lockInput: false,
				useFocus: true,
				maxDays: reservePeriod,
				buttonIcon: "grid",
				calUsePickers: true,
				afterToday: true,
				beforeToday: false,
				notToday: false,
				calOnlyMonth: false,
				overrideSlideFieldOrder: ['y','m','d','h','i']
			});
			mor.ticket.currentPagePath.fromPath = "stationTrainQueryResults.html";
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.pickYears = false;
			mor.ticket.currentPagePath.show = false;
			jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
			mor.ticket.util.changePage(goingPath);
		});
	}
	//new add by zzc end
	/*
	 * 返回日期的星期
	 *new add by zzc begin
	 */
	
	function getWeek(prompt) {
		var desc = [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ];
		if (typeof prompt == "string") {
			var date = mor.ticket.util.setMyDate(prompt);
			return desc[date.getDay()];
		} else {
			return desc[prompt.getDay()];
		}
	}

	jq("#stationTrainQueryResultsView")
			.live(
					"pageshow",
					function(e,data) {

						// mor.ticket.seat_Type_index="";
						mor.ticket.viewControl.tab1_cur_page = "stationTrainQueryResults.html";
						var query = mor.ticket.stationTrainQuery;
						//new add by zzc begin
						var reservePeriod = initReservedPeriod();
						mor.ticket.history.reservePeriod = reservePeriod;
//						var jq_dateInput = jq("#stationTrainChooseDateBtn");
//						for ( var i = 0; i < reservePeriod + 1; i++) {
//							var date = new Date(mor.ticket.util.getNewDate());
//							var date1 = new Date(date.setDate(date.getDate() + i));
//							var week = getWeek(date1);
//							var date1Str = date1.format("yyyy-MM-dd");
//							var htmlStr = "<option value='" + date1Str + "'>" + date1Str
//							+ "      " + week + "</option>";
//							jq_dateInput.append(htmlStr);
//								jq("#stationTrainChooseDateBtn option[value=" + query.train_date + "]").attr(
//										"selected", "selected");
//								jq("#stationTrainChooseDateBtn").selectmenu('refresh', true);
//						}
						var prevId = data.prevPage.attr("id");
						if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
							mor.ticket.stationTrainQuery.trainDate = mor.ticket.currentPagePath.calenderDate;
						}
						jq("#stationTrainChooseDateBtn").val(mor.ticket.stationTrainQuery.trainDate);
						
						if (query.stationTrainQueryResult.length == 0) {
								if (query.queryStation == '') {
									mor.ticket.util.alertMessage("车站不能为空");
									jq.mobile.changePage(vPathViewCallBack()
											+ "stationTrainQuery.html");
									return false;
								}
								
							//searchAndShowResult(query);
						}
						
						var results = mor.ticket.stationTrainQuery.stationTrainQueryResult;
						jq("#stationTrainQueryResultList").html(generateStationTrainQueryListContent(results)).listview("refresh");
						jq("#stationTrainQueryResultList").show();
//						searchAndShowResult(mor.ticket.stationTrainQuery);
						
						initDateBtn();
					});

	function searchAndShowResult(query) {
		var util = mor.ticket.util;
		var commonParameters = null;
		
		commonParameters = {
							'train_start_date':  util.processDateCode(query.trainDate),
							'train_station_code': query.queryStation,
							'startTimes':query.startTimes,
							'stopTimes':query.stopTimes,
							'trainType':query.trainType
							};

		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "stationTrainQuery"
		};
		var options = {
			onSuccess : requestSucceeded,
			onFailure : util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,
				options, true);
		jq("#stationTrainQueryResultList").hide();

	}

	function requestSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.stationTrainQuery.stationTrainQueryResult = invocationResult.cacheStopTimeDTO;
			var results = mor.ticket.stationTrainQuery.stationTrainQueryResult;
			jq("#stationTrainQueryResultList").html(generateStationTrainQueryListContent(results)).listview("refresh");
			jq("#stationTrainQueryResultList").show();
			initDateBtn();
		} else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			return;
		}
		if (busy.isVisible()) {
			busy.hide();
		}
	};	

	function registerBackButtonHandler() {
		jq("#stationTrainQueryResultsBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage(vPathCallBack() + "stationTrainQuery.html");
			return false;
		});
	}
	



	
	
	function initReservedPeriod() {
		var model = mor.ticket.leftTicketQuery;
		var reservedPeriodType = '';
		if (model.purpose_codes == '0X') {
			reservedPeriodType = '3';
		} else if (model.purpose_codes == '1F') {
			reservedPeriodType = 'n';
		} else {
			reservedPeriodType = '1';
		}
		var reservePeriod = parseInt(window.ticketStorage
				.getItem("reservePeriod_" + reservedPeriodType), 10);
		return reservePeriod;
	}
	
	function initDateBtn() {
		var date = mor.ticket.util
				.setMyDate(mor.ticket.stationTrainQuery.trainDate);
		var nowDate = mor.ticket.util.getNewDate();
		
			if (date < nowDate) {
				jq("#prevDateBtn").addClass("ui-disabled");
				jq("#nextDateBtn").removeClass("ui-disabled");
			} else if (date - nowDate > (initReservedPeriod() - 1) * 86400000) {
				jq("#nextDateBtn").addClass("ui-disabled");
				jq("#prevDateBtn").removeClass("ui-disabled");
			} else {
				jq("#prevDateBtn").removeClass("ui-disabled");
				jq("#nextDateBtn").removeClass("ui-disabled");
			}
	}

	function registerPrevDateBtnClickHandler() {
		jq("#prevDateBtn")
				.off()
				.on(
						"tap",
						function() {
							jq("#iscroll-pulldown").css("display","none");
							var date = mor.ticket.util
									.setMyDate(mor.ticket.stationTrainQuery.trainDate);

							var nowDate = mor.ticket.util.getNewDate();
							if (date > nowDate) {
								var preDate = new Date(date.getTime() - 24 * 60
										* 60 * 1000);
								
								mor.ticket.stationTrainQuery.trainDate = preDate
											.format("yyyy-MM-dd");
//									jq("#stationTrainChooseDateBtn option[value=" + mor.ticket.stationTrainQuery.trainDate + "]").attr(
//											"selected", "selected");
//									jq("#stationTrainChooseDateBtn").selectmenu('refresh', true);
								jq("#stationTrainChooseDateBtn").val(mor.ticket.stationTrainQuery.trainDate);
								searchAndShowResult(mor.ticket.stationTrainQuery);
								mor.ticket.util.contentIscrollTo(0, 0, 0);
							}
							return false;
						});
	}

	function registerNextDateBtnClickHandler() {
		jq("#nextDateBtn")
				.off()
				.on(
						"tap",
						function() {
							jq("#iscroll-pulldown").css("display","none");
							var date = mor.ticket.util
									.setMyDate(mor.ticket.stationTrainQuery.trainDate);
							
							var nowDate = mor.ticket.util.getNewDate();
							if (date - nowDate < (initReservedPeriod() - 1) * 86400000) {
								var nextDate = new Date(date.getTime() + 24
										* 60 * 60 * 1000);								
								mor.ticket.stationTrainQuery.trainDate = nextDate
											.format("yyyy-MM-dd");								
//									jq("#stationTrainChooseDateBtn option[value=" + mor.ticket.stationTrainQuery.trainDate + "]").attr(
//											"selected", "selected");
//									jq("#stationTrainChooseDateBtn").selectmenu('refresh', true);
								jq("#stationTrainChooseDateBtn").val(mor.ticket.stationTrainQuery.trainDate);								
								searchAndShowResult(mor.ticket.stationTrainQuery);
								mor.ticket.util.contentIscrollTo(0, 0, 0);

							}
							return false;
						});
	}

	function getLiClass(index) {
		switch (index % 4) {
		case 0:
			return "ui-block-a";
		case 1:
			return "ui-block-b";
		case 2:
			return "ui-block-c";
		case 3:
			return "ui-block-d";
		default:
			return "ui-block-e";

		}
	}

	var stationTrainQueryListTemplate = "{{~it :stationTrainList:index}}"
		+ "<li data-index='{{=index}}' data-iconshadow='false' data-icon='none'>"
		+ "<a><div class='ticket-train-row'>"
		+ "<div class='ticket-train-code-col'>{{=stationTrainList.station_train_code}}</div>"
		+ "<div class='ticket-android-fix'><div class='ticket-train-code-txt'><div class='ui-grid-b detailsGrid'>"
		+ "<div class='ui-block-a'>"
		+ "{{ if(stationTrainList.start_station_telecode == stationTrainList.station_telecode) { }}"
		+ "<div class='ticket-station-div-highlight'>"
		+ "{{ }else{ }}"
	    + "<div class='ticket-station-div'>"
	    + "{{ } }}"
		+ "{{=stationTrainList.start_station_name}}</div>"
		+ "<div class='ticket-time'>{{=stationTrainList.start_start_time}}<font style='color:#2a88c1;'> 出发</font></div>"
		+ "</div>"
		+ "<div class='ui-block-b'>"
		+ "{{ if((stationTrainList.start_station_telecode !== stationTrainList.station_telecode) && (stationTrainList.end_station_telecode !== stationTrainList.station_telecode)) { }}"
		+ "<div id='changeStationName' class='changeStationName'>" 
		+ "{{=stationTrainList.station_name}}</div>"
		+ "<div class='ticket-right-arrow'></div>"
	    + "<div class='ticket-duration'>"
        + "{{=stationTrainList.arrive_time}}-{{=stationTrainList.start_time}}</div>"
	    + "{{ }else{ }}"
	    + "<div id='changeStationName' class='changeStationName'>&nbsp</div>"
		+ "<div class='ticket-right-arrow'></div>"
	    + "<div class='ticket-duration'>&nbsp</div>"
	    + "{{ } }}"
		+ "</div>"
		+ "<div class='ui-block-c'>"
		+ "{{ if(stationTrainList.end_station_telecode == stationTrainList.station_telecode) { }}"
		+ "<div class='ticket-station-div-highlight'>"
		+ "{{ }else{ }}"
		+ "<div class='ticket-station-div'>"
		+ "{{ } }}"
		+ "<span style='text-align:center;display:inline-block;width:80px;vertical-align:top'>"
		+ "{{=stationTrainList.end_station_name}}</span></div>"
		+ "<div class='ticket-time'>{{=stationTrainList.end_arrive_time}}<font style='color:#2a88c1;'> 到达</font></div>"
		+ "</div>"
		+ "</div>"
		+ "</div></div></div>"
		+ "</a>"
		+ "</li>"
		+ "{{~}}";
var generateStationTrainQueryListContent = doT.template(stationTrainQueryListTemplate);

})();