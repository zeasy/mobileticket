/**
 * @providesModule ZZAsync
 * Created by easy on 16/1/12.
 */
'use strict';

class ZZAsync {
    static async invoke(method,... args) {
        return new Promise(async (resolve, reject) => {
            if (typeof method != 'function') {
                var error = new TypeError('method is not a function,can\'t asyncCall it');
                reject(error);
            } else {
                let failed = (err) => {
                    reject(err);
                };
                let success = (result) => {
                    resolve(result);
                };
                if (typeof args[args.length - 1] == 'function') {
                    failed = args.pop();
                    if (typeof args[args.length - 1] == 'function') {
                        success = args.pop();
                    } else {
                        success = failed;
                    }
                }

                method(...args,success,failed);
            }
        });
    }
}

module.exports = ZZAsync;