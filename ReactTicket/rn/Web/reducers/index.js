/**
 * Created by easy on 16/3/17.
 */

import {combineReducers} from 'redux'

module.exports = combineReducers({
    Login : require('./LoginReducer'),
    Query : require('./QueryReducer'),
});