//
//  MTDatabaseTableSchema.h
//  MobileTicket
//
//  Created by EASY on 15/10/28.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZZNativeDatabaseTableColumnSchema : NSObject

@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) BOOL isId;

@property (nonatomic, readonly) NSString *name;


-(instancetype)initWithDictionary:(NSDictionary *) dict name:(NSString *) name;

@end

@interface ZZNativeDatabaseTableSchema : NSObject

@property (nonatomic, readonly) NSArray *columns;

-(instancetype)initWithDictionary:(NSDictionary *) dict;

@end
