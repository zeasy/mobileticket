//
//  CloudShare.m
//  MobileTicketWeb
//
//  Created by EASY on 16/1/16.
//  Copyright © 2016年 Z.EASY. All rights reserved.
//

#import "CloudShare.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDKUI.h>
#import <RCTConvert.h>

@implementation CloudShare

RCT_EXPORT_MODULE()
RCT_EXPORT_METHOD(share:(NSDictionary *) options s:(RCTResponseSenderBlock) s f:(RCTResponseErrorBlock) f) {
	options = [RCTConvert NSDictionary:options];
	NSString *text = [RCTConvert NSString:options[@"text"]];
	NSString *title = [RCTConvert NSString:options[@"title"]];
	NSString *url = [RCTConvert NSString:options[@"url"]];
	NSArray *imageUrls = [RCTConvert NSArray:options[@"images"]];//base64 url image or path or url
	
	NSMutableArray *images = [NSMutableArray array];
	for (NSString *imageData in imageUrls) {
		NSString *data = [RCTConvert NSString:imageData];
		NSURL *url = [NSURL URLWithString:data];
		UIImage *image = nil;
		if (url) {
			image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
		}
		if (image) {
			[images addObject:image];
		}
	}

	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params SSDKSetupShareParamsByText:text images:images url:[NSURL URLWithString:url] title:title type:SSDKContentTypeAuto];
	dispatch_async(dispatch_get_main_queue(), ^{
		[ShareSDK showShareActionSheet:nil
								 items:nil shareParams:params onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
									 if (state == SSDKResponseStateSuccess || state == SSDKResponseStateCancel) {
										 s(@[@{@"state":@(state),@"platformType":@(platformType),@"userData":userData?:@{}}]);
									 } else if(state == SSDKResponseStateFail) {
										 f(error);
									 }
								 }];
	});
}

@end
