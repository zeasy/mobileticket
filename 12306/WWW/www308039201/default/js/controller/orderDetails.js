
/* JavaScript content from js/controller/orderDetails.js in folder common */
(function(){
	var prevPageId = "";
	//requestEpayFlag判断是否调用支付接口 false--调用，true--不调用
	var  requestEpayFlag = false;
	jq("#orderDetailsView").live("pageinit", function(){
		registerCancelBtnLisener(); 
		registerBuyBackTicketBtnLisener(); 
		registerConfirmPaymentBtnListener();
		//线下支付btn
		//registerOffLinePayConfirmTicketBtnListener();
		//改签btn
		registerConfirmChangeTicketBtnListener();
		//var proMsg = mor.ticket.cache.promptMsg.confirmOrder;
		//jq("#confirmOrderTips").html(proMsg);
		
	});
	jq("#orderDetailsView").live("pagehide", function(e, data){
		clearInterval(mor.ticket.paytimer.intervalId);
		
		//如果是gc，那么当前页面代表改签订单已经成功提交这时候将mode改成dc
		
		if(mor.ticket.viewControl.bookMode == "gc"){
			
			mor.ticket.viewControl.bookMode = "dc";
		}
		if(mor.ticket.viewControl.bookMode != "fc"){
			
			//不更改mode
			//mor.ticket.viewControl.bookMode = "dc";
		}else{
			if(data.nextPage.attr("id") != "bookTicketView" &&
					data.nextPage.attr("id") != "sendTicketVisitView"){
				mor.ticket.viewControl.bookMode = "dc";
			}
		}
	});
	jq("#orderDetailsView").live("pagebeforeshow", function(e,data){
		mor.ticket.util.initAppVersionInfo();
		prevPageId = data.prevPage.attr("id");
		if(mor.ticket.loginUser.isAuthenticated === "Y"){
			orderDetailsFn();
		}else{
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
				} else {
				registerAutoLoginHandler(orderDetailsFn, autologinFailJump);
				}
			//mor.ticket.util.changePage(vPathCallBack()+"loginTicket.html");
		}
		jq("#sendTicketVisitBtn").bind("tap", function(e){
			e.stopImmediatePropagation();
			e.preventDefault();
			var ticket = mor.ticket.currentTicket;
			var util=mor.ticket.util;			
			var orderManager=mor.ticket.orderManager;
			var mode = mor.ticket.viewControl.bookMode;
			if(mode == "fc"){
				var preTicket = orderManager.getPreTicketDetail();
				var goTime = ticket.train_date;
				var backTime = preTicket.train_date;
				if(!util.getSendTicketTimeLimit(goTime) && !util.getSendTicketTimeLimit(backTime)){
					util.alertMessage("目前只支持配送距票面记载开车时间超过2天的车票");
					return false;
				}
			}else{
				var startTime = ticket.train_date;
				if(!util.getSendTicketTimeLimit(startTime)){
					util.alertMessage("目前只支持配送距票面记载开车时间超过2天的车票");
					return false;
				}
			}
			mor.ticket.viewControl.current_tab = "orderDetailsTab";
			mor.ticket.util.changePage(vPathCallBack() + "sendTicketVisit.html");
			return false;
		});
	});
	
	function orderDetailsFn(){
		requestEpayFlag = false;
		mor.ticket.viewControl.tab1_cur_page=vPathViewCallBack()+"MobileTicket.html";
		var ticket = mor.ticket.currentTicket;
		var train_date = ticket.train_date;
		var util=mor.ticket.util;		
		var cache = mor.ticket.cache;		
		var orderManager=mor.ticket.orderManager;
		var ticketList=orderManager.getTicketList();
		var preOrderLen = 0;
		var mode = mor.ticket.viewControl.bookMode;
		var str="单程：";	
		
		var endTime=mor.ticket.paytimer.endTime;
    	var strs = "";
    	if(mor.ticket.paytimer.intervalId){
		    clearInterval(mor.ticket.paytimer.intervalId);
			mor.ticket.paytimer.intervalId = null;
		}
    	jq("#sendTicketTotalPriceInfo").hide();
    	var paddingFn = mor.ticket.util.paddingWidth;
    	mor.ticket.paytimer.intervalId=setInterval(function(){
	          var nowTime = mor.ticket.util.getNewDate();
	          var nMS = (endTime.getTime() - nowTime.getTime());
	          var myD=Math.floor(nMS/(1000 * 60 * 60 * 24));
	          var myM=paddingFn(Math.floor(nMS/(1000*60)) % 60, 2, '0');
	          var myS=paddingFn(Math.floor(nMS/1000) % 60, 2, '0');
	          if(myD >= 0){
		  			strs = myM+":"+myS;
		  			jq("#payTime").html(strs);
		          }else{
		        	//取消时钟倒计时
		        	clearInterval(mor.ticket.paytimer.intervalId);
		        	mor.ticket.paytimer.intervalId = null;
		  		    jq("#pay_line").css('display','none'); 
		  		    WL.SimpleDialog.show(
							"温馨提示", 
							"支付有效时间已过，席位已自动释放给其他乘客", 
							[ {text : '确定', handler: function() {
								mor.ticket.viewControl.bookMode = "dc";
								 //add by yiguo
					            mor.ticket.queryOrder.currentQueryOrder = null;
					            mor.ticket.queryOrder.queryOrderList = null;
					            jq.mobile.changePage(vPathViewCallBack()+"MobileTicket.html");
							}}]
						);
		  		}
	      }, 1000);
    	jq("#orderDetailPre").hide();	
		if(mode === "fc") {
			jq("#orderDetailPre").show();	
			var preTicket = orderManager.getPreTicketDetail();
			jq("#orderDetailsPromptPre").html("往程：" + mor.ticket.util.getLocalDateString1(ticket.pre_train_date));
			jq("#fromStationNamePre").html(cache.getStationNameByCode(preTicket.from_station_telecode));
			if(preTicket.start_time.length< 5){
				jq("#trainStartTimePre").html(preTicket.start_time.substring(0,2)+":"+preTicket.start_time.substring(2,4) + " 出发");
			}else{
				jq("#trainStartTimePre").html(preTicket.start_time + " 出发");
			}
			jq("#trainCodeNamePre").html(preTicket.station_train_code);
			if(preTicket.lishi && preTicket.lishi != ""){
				jq("#trainDurationTimePre").html(util.getLiShiStr(preTicket.lishi));
			}
			jq("#toStationNamePre").html(cache.getStationNameByCode(preTicket.to_station_telecode));
			if(preTicket.arrive_time.length< 5){
				jq("#trainReachTimePre").html(preTicket.arrive_time.substring(0,2)+":"+preTicket.arrive_time.substring(2,4) + " 到达");
			}else{
				jq("#trainReachTimePre").html(preTicket.arrive_time + " 到达");	
			}		
			str="返程：";
			var roundTripOrder = orderManager.getRoundTripOrderList();
			var preOrderDetail = roundTripOrder.current;
			ticketList = roundTripOrder.pre;
			preOrderLen = preOrderDetail.length;
			jq("#bookedTicketDetailsGridPre").html(generateTicketDetailsGrid(preOrderDetail));
			jq("#confirmPaymentBtn").show();
			if(window.ticketStorage.getItem("isSendTicket")!="Y" || window.ticketStorage.getItem("isSendTicket")!=="Y"){
				jq("#sendTicketVisitBtn").hide();
			}else{
				jq("#sendTicketVisitBtn").show();
			}
			jq("#buyBackTicketBtn").hide();	
			jq("#offLinePayConfirmTicket").show();
		}else{
			jq("#buyBackTicketBtn").show();	
			jq("#confirmPaymentBtn").show();
			if(window.ticketStorage.getItem("isSendTicket")!="Y" || window.ticketStorage.getItem("isSendTicket")!=="Y"){
				jq("#sendTicketVisitBtn").hide();
			}else{
				jq("#sendTicketVisitBtn").show();
			}
			if(mode === "dc"){
				jq("#offLinePayConfirmTicket").hide();	
			}else{
				jq("#offLinePayConfirmTicket").hide();
			}
			
		}	
		
		jq("#orderDetailsPrompt").html(str +mor.ticket.util.getLocalDateString1(train_date));
		jq("#fromStationName").html(cache.getStationNameByCode(ticket.from_station_telecode));
		jq("#trainStartTime").html(ticket.start_time + " 出发");
		jq("#trainCodeName").html(ticket.station_train_code);
		if(ticket.lishi && ticket.lishi!= ""){
			jq("#trainDurationTime").html(util.getLiShiStr(ticket.lishi));
		}
		jq("#toStationName").html(cache.getStationNameByCode(ticket.to_station_telecode));
		jq("#trainReachTime").html(ticket.arrive_time + " 到达");	
		
		if(mode === "gc"){
			var str = "";
			if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
				str = "变更到站";
			}if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "改签"){
				str = "改签";
			}
			jq("#orderDetailsView .ui-header>h1").html("确认支付("+str+")");
			jq("#ticketPriceInfo").hide();
			jq("#buyBackTicketBtn").hide();
			if(ticketList[0].pay_mode != 'Y'){
				jq("#orderDifPrice").html(ticketList[0].return_total + "元&nbsp;");
				mor.ticket.returnCost.returnCost = ticketList[0].return_total;
				//高改低 退票费率及实际退还票款
				if(ticketList[0].return_total == "0.00"){
					jq("#returnRateAndFact").hide();
				}else{
					jq("#returnRateAndFact").show();
					mor.ticket.returnCost.returnCostPrice = (ticketList[0].return_total - ticketList[0].return_fact).toFixed(2);
					mor.ticket.returnCost.returnRate = ticketList[0].return_rate;
					mor.ticket.returnCost.returnFact = ticketList[0].return_fact;
					jq("#returnRate").html(ticketList[0].return_rate + "%&nbsp;&nbsp;&nbsp;");
					jq("#returnFact").html(ticketList[0].return_fact + "元 &nbsp;&nbsp;");
					jq("#returnCost").html(mor.ticket.returnCost.returnCostPrice + "元");
				}
				if(ticketList[0].return_rate === "0"){
					jq("#returnRateId").hide();
				}else{
					jq("#returnRateId").show();
				}
				jq("#orderRepayPriceInfo").hide();	
				jq("#orderDifPriceInfo").show();
				jq("#confirmChangeTicket").html("确认"+str+"");
			}else{
				var newPrice = 0;
				for(var i=0; i<ticketList[0].myTicketList.length;i++){
					newPrice += parseFloat(ticketList[0].myTicketList[i].ticket_price);
				}
				jq("#newTicketPrice").html(newPrice.toFixed(2) + "元");
				jq("#oldTicketPrice").html(ticketList[0].old_ticket_price + "元");
				jq("#confirmChangeTicket").html("立即支付");
				jq("#orderDifPriceInfo").hide();
				jq("#orderRepayPriceInfo").show();			
			}			
			jq("#cancelOrderBtn").html("取消"+str+"");
			jq("#confirmPaymentBtn").hide();
			jq("#sendTicketVisitBtn").hide();
			jq("#offLinePayConfirmTicket").show();
			jq("#confirmChangeTicket").show();
			jq("#bookedTicketDetailsGrid").html(generateTicketDetailsGrid(ticketList[0].myTicketList));
		}else {
			jq("#orderDetailsView .ui-header>h1").html("确认支付");
			jq("#orderTicketNumber").html(orderManager.totalTicketNum);
			jq("#orderSuccessTicktNumber").html(ticketList.length + preOrderLen);
			jq("#orderTotalPrice").html((orderManager.getActualTotalTicketPrice()).toFixed(2));
			for(var i=0; i<ticketList.length;i++){
				var seatNo = ticketList[i].seat_no;
				var seatType = ticketList[i].seat_type_code;
				var lastnum = ticketList[i].seat_no.substring(3, 4);
				if(lastnum >= 'a' && lastnum <= 'z'){
					   WL.SimpleDialog.show(
								"温馨提示", 
								"本次申请席位的结果为"+mor.ticket.util.getSeatTypeName(seatType,seatNo)+"，请确认。", 
								[ {text : '确定', handler: function() {}}]
							);
					   break;
				}
			}
			jq("#bookedTicketDetailsGrid").html(generateTicketDetailsGrid(ticketList));
			jq("#orderRepayPriceInfo").hide();
			jq("#orderDifPriceInfo").hide();
			jq("#ticketPriceInfo").show();
			jq("#cancelOrderBtn").html("取消订单");
			jq("#confirmChangeTicket").hide();
			//jq("#sendTicketVisitBtn").hide()
			if(prevPageId != "passengersView" && prevPageId != "bookTicketView"){
				queryExpressTickets(ticketList[0].sequence_no);
			}
			if(prevPageId == "passengersView" && mode == "dc"){
				requestEpayFlag = true;
			}else{
				requestEpayFlag = false;
			}
		}
		mor.ticket.util.contentIscrollTo(0,0,0);
		jq("#orderDetailsView .ui-content").iscrollview("refresh");
		if(prevPageId == "passengersView"){
			noSeatNoTip();
		}
		if(prevPageId == "insuranceTicketView"){
			var orderManager=mor.ticket.orderManager;
			var ticketList=orderManager.getTicketList();
				for(var j = 0;j<ticketList.length;j++){
						var currTicket = jq("#" + ticketList[j].station_train_code + ticketList[j].ticket_type_code + ticketList[j].seat_no + ticketList[j].passenger_id_type_code + ticketList[j].passenger_id_no);
						currTicket.find("#insuranceTicketFlag").addClass("insuranceTicketFlag").html("保");
			}
		}
	}
	
	function contentIscrollTo(x,y,time){
		if(jq("#orderDetailsView .ui-content").attr("data-iscroll")!=undefined){
			jq("#orderDetailsView .ui-content").jqmData('iscrollview').iscroll.scrollTo(x,y,time);
		}
	}
	
	function updateTicketDetails(passengers,tickets){		
		for(var i=0;i<passengers.length;i++){
			var seat_type=passengers[i].seat_type;
			for(var m=0;m<tickets.length;m++){
				if(tickets[m].type_id==seat_type){
					passengers[i].ticket_price=tickets[m].price;
					break;
				}
			}
		}
		
	};
	
	function registerConfirmPaymentBtnListener(){
		jq("#confirmPaymentBtn").off().on("tap", function(){
			// by yiguo
			// 判断当前用户是否为激活用户，未激活用户不可以提交订单
			/*if (mor.ticket.loginUser.activeUser != 'Y') {
				mor.ticket.util.alertMessage("当前用户未激活，请到您的注册邮箱收取12306激活邮件后按提示激活用户后再登录！");
				return false;
			}*/
			if(requestEpayFlag == true){
				submitPaymentGateWay();
			}else{
				requestEpayProcedurePayment();
			}
		}); 
	}
	
	function requestEpayProcedurePayment(){
		var order = mor.ticket.orderManager;
		var util = mor.ticket.util;
		var batch_no_list = [];
		for(var i=0;i<order.orderTicketList.length;i++){
			batch_no_list.push(order.orderTicketList[i].batch_no);
		}
		batch_no_list.sort();
		util.uniq(batch_no_list);
		var batch_no_str = batch_no_list.join("#");
		
		var commonParameters = {			
				'sequence_no': order.orderTicketList[0].sequence_no,
				'batch_no': batch_no_str,//order.orderTicketList[0].batch_no,
				'order_timeout_date':order.orderTicketList[0].pay_limit_time,
				'pay_start':'1'
			};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "epay"
		};
		
		var options =  {
				onSuccess: requestEpayProcedurePaymentSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
	}
	
	function requestEpayProcedurePaymentSucceeded(result){
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.payment.interfaceName = invocationResult.interfaceName;
			mor.ticket.payment.interfaceVersion = invocationResult.interfaceVersion;
			mor.ticket.payment.tranData = invocationResult.tranData;
			mor.ticket.payment.merSignMsg = invocationResult.merSignMsg;
			mor.ticket.payment.appId = invocationResult.appId;
			mor.ticket.payment.transType = invocationResult.transType;
			mor.ticket.payment.epayurl = invocationResult.epayurl;
			submitPaymentGateWay();
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	function submitPaymentGateWay(){	
		var orderManager = mor.ticket.orderManager;
		orderManager.paymentOrder.myTicketList = [];
		var ticketList = orderManager.getTicketList();
		for(var i=0; i<ticketList.length; i++){
			var ticket = ticketList[i];
			orderManager.paymentOrder.myTicketList.push(ticket);
		}
		//获取batch no用来在支付完成后过滤对应车票
		var myTicketList = orderManager.paymentOrder.myTicketList;
		mor.ticket.payment.batch_nos = [];
		for(var i=0;i<myTicketList.length;i++){
			var batch_no = myTicketList[i].batch_no;
			mor.ticket.payment.batch_nos.push(batch_no);
		}
		orderManager.clearTicketList();
		var payment=mor.ticket.payment;
		var parameters={
				         "showLocationBar": true,
				         "interfaceName":payment['interfaceName'],
				         "interfaceVersion":payment['interfaceVersion'],
				         "tranData":payment['tranData'],
				         "merSignMsg":payment['merSignMsg'],
				         "appId":payment['appId'],
				         "transType":payment['transType'],
						 "epayurl":payment['epayurl']
		  };
		/*for(var i=0;i<mor.ticket.payment.batch_nos.length;i++){
			WL.Logger.debug("#######################order detail payment.batch_nos:"+mor.ticket.payment.batch_nos[i]);
		}*/
		window.plugins.childBrowser.showWebPage(payment['epayurl'], parameters);
		//mor.ticket.util.changePage("queryOrder.html");
		mor.ticket.viewControl.bookMode = "dc";
  	    return false;
	}
	
	function registerBuyBackTicketBtnLisener(){
		jq("#buyBackTicketBtn").bind("tap",function(){
			var purposeArray = new Array();
			var orderManager = mor.ticket.orderManager;
			orderManager.confirmRoundTripTicketList(mor.ticket.passengerList);
			orderManager.setPreTicketDetail(mor.ticket.currentTicket);
			mor.ticket.viewControl.bookMode = "fc";
			var model = mor.ticket.leftTicketQuery;
			//根据往程时间判断返程乘车时间 gu
			var train_date = mor.ticket.currentTicket.train_date;
			var date = mor.ticket.util.setMyDate(train_date);
			var queryDate = new Date(date.setDate(date.getDate() + 1));
			model.train_date_back = queryDate.format("yyyy-MM-dd");
			//返程学生票purpose_codes  gu
			purposeArray = jq.grep(orderManager.orderTicketList,function(item, planIndex) {
				return item.ticket_type_code != "3";	
		});
			if(purposeArray.length >0){
				model.purpose_codes = "00";	
			}else{
				model.purpose_codes = "0X";	
			}
			mor.ticket.util.changePage(vPathViewCallBack()+"MobileTicket.html");
			return false;
		});
	}

	function registerCancelBtnLisener(){
		jq("#cancelOrderBtn").bind("tap", function(){
			if(mor.ticket.util.isAndroid()){
				WL.SimpleDialog.show(
	    				"温馨提示", 
	    				"一天内3次申请车票成功后取消订单，当日将不能在12306继续购票！", 
	    				[ 
	    				  {text : '确定', handler: function(){sendCancelOrderRequest();}},
	    				  {text : '取消', handler: function(){}}
	    				 ]
	    		);
			}else{
				WL.SimpleDialog.show(
	    				"温馨提示", 
	    				"一天内3次申请车票成功后取消订单，当日将不能在12306继续购票！", 
	    				[ {text : '取消', handler: function(){}},
	    				  {text : '确定', handler: function(){sendCancelOrderRequest();}}]
	    		);
			}
		});
	};
	
	function sendCancelOrderRequest() {  	  	
  	  	var orderManager = mor.ticket.orderManager;
  	    var ticketList=orderManager.getTicketList();
  	    
		var util = mor.ticket.util;
		var commonParameters;
		if(mor.ticket.viewControl.bookMode == "gc"){
			commonParameters = {			
				'sequence_no': ticketList[0].myTicketList[0].sequence_no,
				'batch_no': '0',
				'cancel_flag':'1'// 0:取消普通待支付订单;1取消改签待支付订单
			};
		}else{
			commonParameters = {			
				'sequence_no': ticketList[0].sequence_no,
				'batch_no': ticketList[0].batch_no,
				'cancel_flag':'0'// 0:取消普通待支付订单;1取消改签待支付订单
			};
		}		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "cancelOrder"
		};
		
		var options =  {
				onSuccess: cancelOrderRequestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};	
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function cancelOrderRequestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.viewControl.bookMode = "dc";	
		//不更改mode
			
		//mor.ticket.viewControl.bookMode = "dc";
		mor.ticket.orderManager.clearTicketList();
		//取消时钟倒计时
			WL.SimpleDialog.show(
					"温馨提示", 
					"订单取消成功!", 
					[ {text : '确定', handler: function(){
						if(mor.ticket.viewControl.bookMode === "gc"){
							jq.mobile.changePage("queryOrder.html");				
						}else{
							jq.mobile.changePage(vPathViewCallBack()+"MobileTicket.html");
						}	
					}}]
				);	
		}else {
			if(invocationResult.isSuccessful && invocationResult.succ_flag == "-1"){
				mor.ticket.viewControl.bookMode = "dc";
				jq.mobile.changePage("queryOrder.html");				
			}
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	//线下支付--Begin
	//周自昌
	

		function registerOffLinePayConfirmTicketBtnListener() {
				jq("#offLinePayConfirmTicket").bind("tap",function(){
					var orderManager=mor.ticket.orderManager;
					var ticketList=orderManager.getTicketList();
						WL.SimpleDialog
								.show(
										"温馨提示",
										"确定要线下支付吗?",
										[
												{
													text : '取消',
													handler : function() {
													}
												},
												{
													text : '确定',
													handler : function() {
														var parameter = {
															'sequence_no' : ticketList[0].sequence_no
														};
														getOffLinePayParameters(parameter);
													}
												} ]);
					});	
	}
		
	function getOffLinePayParameters(parameter){
		var util = mor.ticket.util;
		var commonParameters = parameter;
//		WL.Logger.debug("#######################order detail payment.sequence_no:"+commonParameters.sequence_no);
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "offLinePay"
		};
		
		var options =  {
				onSuccess: offLinePaySucceededs,
				onFailure: util.creatCommonRequestFailureHandler()
		};
//		WL.Client.invokeProcedure(invocationData, options);
//		busy.show();
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function offLinePaySucceededs(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var orderManager = mor.ticket.orderManager;
		var ticketList = orderManager.orderTicketList;
			var invocationResult = result.invocationResult;
			if (mor.ticket.util.invocationIsSuccessful(invocationResult)){
				orderManager.paymentOrder.myTicketList = [];
				for(var i=0;i<ticketList.length;i++){
					orderManager.paymentOrder.myTicketList.push(ticketList[i]);
					//var ticket = mor.ticket.currentTicket;
					ticketList[i]._ticket = mor.ticket.currentTicket;
					//WL.Logger.error(ticketList[i]._ticket.ticket_type_code);
				}
				for(var i=0;i<ticketList.length;i++){
				if(ticketList[i].sequence_no!=invocationResult.sequence_no){
					mor.ticket.orderManager.paymentOrder.myTicketList[i].sequence_no=invocationResult.sequence_no;
					}
				}
				jq.mobile.changePage("offLinePayFinishEnter.html");
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}	
	}
	
	//线下支付--End
	
	
	
	function registerConfirmChangeTicketBtnListener(){
		jq("#confirmChangeTicket").bind("tap",function(){
			var orderManager=mor.ticket.orderManager;
			var ticketList=orderManager.getTicketList();
			var currentFinishedOrders = mor.ticket.queryOrder.getCurrentFinishedOrders();
			ticketList.if_deliver = currentFinishedOrders.if_deliver;
			var currentTicketDetail = ticketList[0].myTicketList[0];
			//var util = mor.ticket.util;
			if(ticketList[0].pay_mode != 'Y'){
				changeTicketNormalProcedure(ticketList);
			}else{
				
				//订单批次号
				var batch_no_list = [];
				for(var i=0;i<ticketList[0].myTicketList.length;i++){
					batch_no_list.push(ticketList[0].myTicketList[i].batch_no);
				}
				batch_no_list.sort();
				util.uniq(batch_no_list);
				var batch_no_str = batch_no_list.join("#");
				var currentFinishedTicketDetail = mor.ticket.queryOrder.getCurrentFinishedOrders;
				var checkResignExpress;
				if(ticketList.if_deliver == "Y"){
					checkResignExpress = "Y";
				}else{
					checkResignExpress = "N";
				}
				if(mor.ticket.util.isAndroid()){
					WL.SimpleDialog.show(
							"温馨提示", 
							"确定要支付吗？", 
							[ 
							  {text : '确定', handler: function(){
								  var parameters = {			
											'sequence_no': currentTicketDetail.sequence_no,
											'batch_no': batch_no_str,//currentTicketDetail.batch_no,
											'order_timeout_date':currentTicketDetail.pay_limit_time,
											'pay_start':'2',//改签为2
											'checkResignExpress':checkResignExpress,//是否检查物流但,Y-需要,N-不需要
										};	
										getEpayParameter(parameters);
							  }},
							  {text : '取消', handler: function(){}}
							  ]
						);
				}else{WL.SimpleDialog.show(
							"温馨提示", 
							"确定要支付吗？", 
							[ {text : '取消', handler: function(){}},
							  {text : '确定', handler: function(){
								  var parameters = {			
											'sequence_no': currentTicketDetail.sequence_no,
											'batch_no': batch_no_str,//currentTicketDetail.batch_no,
											'order_timeout_date':currentTicketDetail.pay_limit_time,
											'pay_start':'2',//改签为2
											'checkResignExpress':checkResignExpress,//是否检查物流但,Y-需要,N-不需要
										};	
										getEpayParameter(parameters);
							  }}]
						);
				}
			}
		});
	}
	
	function changeTicketNormalProcedure(ticketList){//平改、或者高改低时流程
		var currentTicketDetail = ticketList[0].myTicketList[0];
		var util = mor.ticket.util;
		var checkResignExpress;
		if(ticketList.if_deliver == "Y"){
			checkResignExpress = "Y";
		}else{
			checkResignExpress = "N";
		}
		var commonParameters = {			
			'sequence_no': currentTicketDetail.sequence_no,
			'batch_no':currentTicketDetail.batch_no,
			'lose_time':ticketList[0].lose_time,
			'pay_type':ticketList[0].pay_mode,
			'checkResignExpress':checkResignExpress
		};
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "confirmChangeTicket"
		};
		
		var options =  {
				onSuccess: requestConfirmChangeTicketSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function  requestConfirmChangeTicketSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			if(invocationResult.isCheckResignExpress == "0"){
				if(mor.ticket.util.isAndroid()){
					WL.SimpleDialog.show(
							"温馨提示", 
							invocationResult.checkResignExpressMsg, 
							[{text : '确定', handler: function(){
								  var orderManager=mor.ticket.orderManager;
								  var ticketList=orderManager.getTicketList();
								  var currentTicketDetail = ticketList[0].myTicketList[0];
									var util = mor.ticket.util;
									var commonParameters = {			
										'sequence_no': currentTicketDetail.sequence_no,
										'batch_no':currentTicketDetail.batch_no,
										'lose_time':ticketList[0].lose_time,
										'pay_type':ticketList[0].pay_mode,
										'checkResignExpress':'N'
									};
									
									var invocationData = {
											adapter: mor.ticket.viewControl.adapterUsed,
											procedure: "confirmChangeTicket"
									};
									
									var options =  {
											onSuccess: requestConfirmChangeTicketSucceeded,
											onFailure: util.creatCommonRequestFailureHandler()
									};
									mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
//									return false;
							  }},{text : '取消', handler: function(){}}]
						);
					
				}else{WL.SimpleDialog.show(
							"温馨提示", 
							invocationResult.checkResignExpressMsg, 
							[ {text : '取消', handler: function(){}},
							  {text : '确定', handler: function(){
								  var orderManager=mor.ticket.orderManager;
								  var ticketList=orderManager.getTicketList();
								  var currentTicketDetail = ticketList[0].myTicketList[0];
									var util = mor.ticket.util;
									var commonParameters = {			
										'sequence_no': currentTicketDetail.sequence_no,
										'batch_no':currentTicketDetail.batch_no,
										'lose_time':ticketList[0].lose_time,
										'pay_type':ticketList[0].pay_mode,
										'checkResignExpress':'N'
									};
									
									var invocationData = {
											adapter: mor.ticket.viewControl.adapterUsed,
											procedure: "confirmChangeTicket"
									};
									
									var options =  {
											onSuccess: requestConfirmChangeTicketSucceeded,
											onFailure: util.creatCommonRequestFailureHandler()
									};
									
									mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
//									return false;
							  }}]
						);
				}
			}else{
				mor.ticket.viewControl.bookMode = "dc";
				jq.mobile.changePage("queryOrder.html");
				if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "改签"){
					mor.ticket.util.alertMessage("改签成功！");
				}
				if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
					mor.ticket.util.alertMessage("变更到站成功！");
				}
				mor.ticket.changeOrchangeStation.changeOrchangeStation = "";
				mor.ticket.orderManager.clearTicketList();
			}
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	function getEpayParameter(parameters){
			var util = mor.ticket.util;
			var commonParameters = parameters;
//			WL.Logger.debug("#######################order detail payment.sequence_no:"+commonParameters.sequence_no);
			
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "epay"
			};
			
			var options =  {
					onSuccess: epaySucceededNew,
					onFailure: util.creatCommonRequestFailureHandler()
			};
//			WL.Client.invokeProcedure(invocationData, options);
//			busy.show();
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
	}
	function epaySucceededNew(result){
		if(busy.isVisible()){
			busy.hide();
		}	
		var orderManager = mor.ticket.orderManager;
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			if(invocationResult.isCheckResignExpress == "0"){
				var ticketList=orderManager.getTicketList();
				var currentTicketDetail = ticketList[0].myTicketList[0];
				
				var batch_no_list = [];
				for(var i=0;i<ticketList[0].myTicketList.length;i++){
					batch_no_list.push(ticketList[0].myTicketList[i].batch_no);
				}
				batch_no_list.sort();
				util.uniq(batch_no_list);
				var batch_no_str = batch_no_list.join("#");
				
				if(mor.ticket.util.isAndroid()){
					WL.SimpleDialog.show(
							"温馨提示", 
							invocationResult.checkResignExpressMsg, 
							[{text : '确定', handler: function(){
									var parameters = {			
												'sequence_no': currentTicketDetail.sequence_no,
												'batch_no': batch_no_str,//currentTicketDetail.batch_no,
												'order_timeout_date':currentTicketDetail.pay_limit_time,
												'pay_start':'2',//改签为2
												'checkResignExpress':'N',//是否检查物流但,Y-需要,N-不需要
											};	
										getEpayParameter(parameters);	 
							  }},{text : '取消', handler: function(){orderManager.clearTicketList();}}]
						);
				}else{
					WL.SimpleDialog.show(
							"温馨提示", 
							invocationResult.checkResignExpressMsg, 
							[ {text : '取消', handler: function(){orderManager.clearTicketList();}},
							  {text : '确定', handler: function(){
									var parameters = {			
												'sequence_no': currentTicketDetail.sequence_no,
												'batch_no': batch_no_str,//currentTicketDetail.batch_no,
												'order_timeout_date':currentTicketDetail.pay_limit_time,
												'pay_start':'2',//改签为2
												'checkResignExpress':'N',//是否检查物流但,Y-需要,N-不需要
											};	
										getEpayParameter(parameters);	 
							  }}]
						);
				}
				
			}else{
				orderManager.paymentOrder.myTicketList = [];
				var ticketList = orderManager.getTicketList();
				for(var i=0; i<ticketList[0].myTicketList.length; i++){
				var ticket = ticketList[0].myTicketList[i];
					orderManager.paymentOrder.myTicketList.push(ticket);
				}
				
				//获取batch no用来在支付完成后过滤对应车票
				var myTicketList = orderManager.paymentOrder.myTicketList;
				mor.ticket.payment.batch_nos = [];
				for(var i=0;i<myTicketList.length;i++){
					var batch_no = myTicketList[i].batch_no;
					mor.ticket.payment.batch_nos.push(batch_no);
				}
				/*for(var i=0;i<mor.ticket.payment.batch_nos.length;i++){
					WL.Logger.debug("#######################order detail payment.batch_nos:"+mor.ticket.payment.batch_nos[i]);
				}*/
				
				var parameters={
			         "showLocationBar": true,
			         "interfaceName":invocationResult['interfaceName'],
			         "interfaceVersion":invocationResult['interfaceVersion'],
			         "tranData":invocationResult['tranData'],
			         "merSignMsg":invocationResult['merSignMsg'],
			         "appId":invocationResult['appId'],
			         "transType":invocationResult['transType'],
					 "epayurl":invocationResult['epayurl']
					  };
				window.plugins.childBrowser.showWebPage(invocationResult['epayurl'], parameters);
				orderManager.clearTicketList();
			}
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			orderManager.clearTicketList();
		}
	}
	function noSeatNoTip(){
	    var util = mor.ticket.util;
	  	var orderManager = mor.ticket.orderManager;
	  	if(mor.ticket.viewControl.bookMode == "dc"){
	  	    var ticketList=orderManager.getTicketList();
	  	}else if(mor.ticket.viewControl.bookMode == "wc"){
	  	    var ticketList=orderManager.getTicketList();
	  	}else if(mor.ticket.viewControl.bookMode == "fc"){
	  	    var ticketList=orderManager.getRoundTripOrderList().pre;
	  	}else if(mor.ticket.viewControl.bookMode == "gc"){
	  	    var ticketList=orderManager.getTicketList()[0].myTicketList;
	  	}
	  	var coachNoArray = [];
  	    for(var i=0;i< ticketList.length;i++){
			var ticket = ticketList[i];
			coachNoArray.push(ticketList[i].coach_no); 
			if(ticket.seat_name.indexOf("无座")>=0){
				WL.SimpleDialog.show("温馨提示", "您所购车次的"+util.getSeatTypeName(ticket.seat_type_code)+"已售完，系统自动为您分配了无座票", [ {
					text : '确定',
					handler : function() {
						var UniCoachNoArray = mor.ticket.util.uniq(coachNoArray);
						if(UniCoachNoArray.length>1){
							WL.SimpleDialog.show("温馨提示", "本次列车已无满足您需求的集中席位，系统为您自动分配了不同车厢席位，您可在列车上联系列车员，在征得其他旅客同意后，调换座/铺位，请确认后继续支付或取消订单。", [ {
								text : '确定',
								handler : function() {
									secondAlertMessage(0);
									return false;
								}
							} ]);
						}
						secondAlertMessage(0);
						return false;
					}
				} ]);
			}
		}
  	    var UniCoachNoArray = mor.ticket.util.uniq(coachNoArray);
		if(UniCoachNoArray.length>1){
			WL.SimpleDialog.show("温馨提示", "本次列车已无满足您需求的集中席位，系统为您自动分配了不同车厢席位，您可在列车上联系列车员，在征得其他旅客同意后，调换座/铺位，请确认后继续支付或取消订单。", [ {
				text : '确定',
				handler : function() {
					secondAlertMessage(0);
					return false;
				}
			} ]);
		}
  	  secondAlertMessage(0);
	} 
	var util = mor.ticket.util;
	var nowDate = util.getNewDate();
	 function secondAlertMessage(m){
	  	    var saleTicketMessge = eval(window.ticketStorage.getItem("saleTicketMessage"));
	  	    if(m >= saleTicketMessge.length){
	  	    	return false;
	  	    }
  	    	if(saleTicketMessge[m].message_stop_date === undefined || saleTicketMessge[m].message_stop_date === null || saleTicketMessge[m].message_stop_date === ""){
	  	    	saleTicketMessge[m].message_stop_date = "20991231";
	  	    }
	  	    if(saleTicketMessge[m].train_date_begin === undefined || saleTicketMessge[m].train_date_begin === null || saleTicketMessge[m].train_date_begin === ""){
	  	    	saleTicketMessge[m].train_date_begin = "20150624";
	  	    }
	  	    if(saleTicketMessge[m].train_date_end === undefined || saleTicketMessge[m].train_date_end === null || saleTicketMessge[m].train_date_end === ""){
	  	    	saleTicketMessge[m].train_date_end = "20991231";
	  	    }
	  	    var messageStopAlertTime = util.setMyDate2(saleTicketMessge[m].message_stop_date);
	  	    var train_date_begin = util.setMyDate2(saleTicketMessge[m].train_date_begin);
	  	    var train_date_end = util.setMyDate2(saleTicketMessge[m].train_date_end);
			var train_date_time = util.setMyDate(mor.ticket.currentTicket.train_date);
  	    	if((saleTicketMessge[m].from_station.indexOf(mor.ticket.currentTicket.from_station_telecode) !=-1 || saleTicketMessge[m].to_station.indexOf(mor.ticket.currentTicket.to_station_telecode) !=-1)
					&& (saleTicketMessge[m].train_header.indexOf(mor.ticket.currentTicket.station_train_code.substring(0,1)) != -1 || saleTicketMessge[m].train_header.length ==0)
					&& (saleTicketMessge[m].train_station_code.indexOf(util.getBeginTrain(mor.ticket.currentTicket.train_no)) != -1 || saleTicketMessge[m].train_station_code.length ==0)
					&& saleTicketMessge[m].confirm_before_behind == "behind"
					&& messageStopAlertTime >= nowDate
					&& train_date_time >= train_date_begin
					&& train_date_time <= train_date_end){
  	   			WL.SimpleDialog.show(
						"温馨提示", saleTicketMessge[m].message, 
						[{text : '确定', handler: function(){
							secondAlertMessage(m+1);
							return false;
						  }}]
					);
  	    	}else{
  	    		secondAlertMessage(m+1);
  	    	}
	    }
	
	//送票上门
	/*function sendTicketVistPage(){
		jq("#sendTicketVisitBtn").bind("tap", function(){
			mor.ticket.viewControl.current_tab = "orderDetailsTab";
			mor.ticket.util.changePage(vPathCallBack() + "sendTicketVisit.html");
		});
	}*/
	var ticketDetailsGridTemplate =
		"{{~it :ticket:index}}" +
		"<div class='orderDetailList' id='{{=ticket.station_train_code}}{{=ticket.ticket_type_code}}{{=ticket.seat_no}}{{=ticket.passenger_id_type_code}}{{=ticket.passenger_id_no}}'>" +
		"<div class='ui-grid-a'>" +
			"<div class='ui-block-a text-ellipsis'><span id='sendFlag'></span>{{=ticket.passenger_name}}</div>" +
			"{{ if(ticket.passenger_id_type_code === '1'){ }}"+ 
			"<div class='ui-block-b'>{{=mor.ticket.util.getIdNoStar(ticket.passenger_id_no)}}</div>"+
			"{{ }else{ }}"+
			"<div class='ui-block-b'>{{=ticket.passenger_id_no}}</div>"+
			"{{ } }}"+
			 "<div class='ui-block-c'>{{=mor.ticket.util.getIdTypeName(ticket.passenger_id_type_code)}}</div>"+
		"</div>" +
		"<div class='ui-grid-b'>" +
			"<div class='ui-block-a'>{{=mor.ticket.util.getTicketTypeName(ticket.ticket_type_code)}}</div>" +
			"<div class='ui-block-b'>{{=mor.ticket.util.getSeatTypeName(ticket.seat_type_code,ticket.seat_no)}}</div>" +
			"<div class='ui-block-c'><span class='text_orange'>{{=ticket.coach_name}}</span>车</div>" + 
			"<div class='ui-block-d'><span class='text_orange'>{{=ticket.seat_name}}</span></div>" + 
			"<div class='ui-block-e'><span class='text_orange'>{{=ticket.ticket_price}}元</span></div>" + 
		"</div>" +
		"</div>" +
		"{{~}}";
	var generateTicketDetailsGrid = doT.template(ticketDetailsGridTemplate);
	
	// 查询已保存快递订单信息
	function queryExpressTickets(sequence_no){
		var util = mor.ticket.util;
		var commonParameters = {
					"sequence_no" : sequence_no,
					"query_flag" : '0',
					"pay_flag" : '0'
			};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryExpressTickets"
			};
		var options =  {
				onSuccess: requestQueryExpressTicketsSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestQueryExpressTicketsSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			requestEpayFlag = false;
			var expressTickets = invocationResult.listExpressOrderForQueryReturnBean;
			if(expressTickets.length > 0){
				jq("#buyBackTicketBtn").css("display","none");
				var orderManager=mor.ticket.orderManager;
				var ticketList=orderManager.getTicketList();
				var expressTotalPrice = 0;
				for(var i = 0;i<expressTickets.length;i++){
					var ticket_message = expressTickets[i].ticket_message.split("#");
					var total_price = expressTickets[i].total_price !=="0" ? expressTickets[i].total_price : "1700";
					expressTotalPrice += parseInt(total_price);
					for(var j = 0;j<ticketList.length;j++){
						var temp_message = ticketList[j].sequence_no + "+" +ticketList[j].batch_no + "+" +
						ticketList[j].coach_no + "+" + ticketList[j].seat_no + "+" + ticketList[j].passenger_id_type_code + "+" +
						ticketList[j].passenger_id_no + "+" + ticketList[j].passenger_name;
						if(jq.inArray(temp_message,ticket_message) != -1){
							var currTicket = jq("#" + ticketList[j].station_train_code + ticketList[j].ticket_type_code + ticketList[j].seat_no + ticketList[j].passenger_id_type_code + ticketList[j].passenger_id_no);
							currTicket.find("#sendFlag").addClass("sendTicketFlag").html("送");
						}
					}
				}
				jq("#sendTicketTotalPriceInfo").show();
				jq("#expressTotalPrice").text(parseFloat(expressTotalPrice/100).toFixed(2));
				//requestEpayProcedurePayment(orderManager);
			}else{
				jq("#sendTicketTotalPriceInfo").hide();
			}
		}
	}
})();