/**
 * Created by easy on 15/11/4.
 */

var Commons = {};
Commons.BaseObject = require('./BaseObject');


Commons.Controller = require('./components/Controller');
Commons.View = require('./components/View');
Commons.TableView = require('./components/TableView/ZZTableViewIOS');
Commons.Control = require('./components/ControlIOS.js');
exports = module.exports = Commons;