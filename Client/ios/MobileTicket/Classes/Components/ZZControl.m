//
//  ZZControl.m
//  MobileTicket
//
//  Created by EASY on 15/11/13.
//  Copyright (c) 2015年 Facebook. All rights reserved.
//

#import "ZZControl.h"

@interface ZZControl ()



@end

@implementation ZZControl

- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher
{
  self = [super init];
  if (self) {
    [self addTarget:self action:@selector(onPressed:) forControlEvents:UIControlEventTouchUpInside];
  }
  return self;
}

-(void) onPressed:(id) sender {
  if (self.onTouch) {
    self.onTouch(@{});
  }
}

@end
