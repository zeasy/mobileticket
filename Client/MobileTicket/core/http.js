/**
 * @providesModule HTTP
 * Created by easy on 15/11/17.
 */

"use strict";

var Network = require('Network');

class HTTP {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    async asyncGet(path,params) {
        return Network.asyncRequest({url:this.baseUrl,path:path,params:params});
    }

    async asyncPost(path,body,urlParams = {}) {
        return Network.asyncRequest({url:this.baseUrl,path:path,body:body,params:urlParams,method:'POST'});
    }
}

exports = module.exports = HTTP;