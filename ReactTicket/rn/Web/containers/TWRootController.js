/**
 * Created by easy on 16/3/17.
 */

import React,{
    Component,
    View,
    Text,
    TouchableOpacity
} from 'react-native'

import moment from 'moment'


class TWRootController extends Component {

    onLoginPress() {
        this.props.navigator.push({
            component:require('./TWLoginController'),
        });
    }

    onQueryPress() {
        this.props.navigator.push({
            component:require('./TWQueryController'),
            title:'查询',
            passProps : {
                day : moment().format('YYYY-MM-DD')
            }
        });
    }


    render() {
        return (
            <View style={{flex:1,justifyContent:'center',backgroundColor:'white'}}>
            <TouchableOpacity style={{justifyContent:'center',height:40}} onPress={this.onLoginPress.bind(this)}>
                <Text style={{alignSelf:'center'}}>Login</Text>
            </TouchableOpacity>

                <TouchableOpacity style={{justifyContent:'center',height:40}} onPress={this.onQueryPress.bind(this)}>
                    <Text style={{alignSelf:'center'}}>Query</Text>
                </TouchableOpacity>
                </View>
        );
    }
}



module.exports = TWRootController;