//
//  MTDatabaseBridge.m
//  MobileTicket
//
//  Created by EASY on 15/10/28.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZNativeDatabase.h"
#import <BZObjectStore/BZObjectStore.h>
#import <MJExtension/MJExtension.h>
#import <RCTConvert.h>
#import "ZZNativeDatabaseTableSchema.h"
#import "ZZNativeDatabaseUtils.h"

@interface ZZNativeDatabase ()

@property (nonatomic, strong) NSMutableDictionary *databases;

@end

@implementation ZZNativeDatabase

RCT_EXPORT_MODULE(NativeDatabase)

-(NSMutableDictionary *)databases {
  if (!_databases) {
    _databases = [NSMutableDictionary dictionary];
  }
  return _databases;
}

-(BZObjectStoreConditionModel *) conditionWithDictionary:(NSDictionary *) condition {
	condition = [RCTConvert NSDictionary:condition];
	BZObjectStoreConditionModel *cond;
	if (condition) {
		cond = [BZObjectStoreConditionModel condition];
		cond.sqlite.where = [RCTConvert NSString:condition[@"where"]];
		cond.sqlite.parameters = [RCTConvert NSArray:condition[@"fields"]];
		cond.sqlite.orderBy = [RCTConvert NSString:condition[@"order"]];
		cond.sqlite.limit = [RCTConvert NSNumber:condition[@"limit"]];
		cond.sqlite.offset = [RCTConvert NSNumber:condition[@"offset"]];
	}
	return cond;
}

RCT_EXPORT_METHOD(openDatabase:(NSString *) path successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	path = [RCTConvert NSString:path];
  if (![path length]) {
    fail(nil);
    return;
  }
  NSError *error = nil;
  BZObjectStore *store = [BZObjectStore openWithPath:path error:&error];
  if (error) {
    fail(error);	
  } else {
    if (store) {
			self.databases[path] = store;
      success(@[@YES]);
    } else {
      fail(nil);
    }
  }
  
}

RCT_EXPORT_METHOD(closeDatabase:(NSString *) path successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	path = [RCTConvert NSString:path];
	if (![path length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[path];
	if (store) {
		[store close];
    [self.databases removeObjectForKey:path];
	}
	success(@[@YES]);
}

RCT_EXPORT_METHOD(createTable:(NSString *) databasePath table:(NSString *) tableName schema:(NSDictionary *) schema successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	schema = [RCTConvert NSDictionary:schema];
	if (![databasePath length] || ![tableName length] || ![schema count]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	if (!store) {
		fail(nil);
		return;
	}
	Class clazz = NSClassFromString(tableName);
	if (clazz) {
		success(@[@YES]);
		return;
	}
	ZZNativeDatabaseTableSchema *tableSchema = [[ZZNativeDatabaseTableSchema alloc] initWithDictionary:schema];
	clazz = [ZZNativeDatabaseUtils createClass:tableName withSchema:tableSchema];
	if (clazz) {
		NSError *error;
		BOOL result = [store registerClass:clazz error:&error];
		if (error) {
			fail(error);
		} else {
			success(@[@(result)]);
		}
		
	} else {
		success(@[@NO]);
	}
}

RCT_EXPORT_METHOD(save:(NSString *) databasePath table:(NSString *) tableName objects:(NSArray *) objects successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	objects = [RCTConvert NSDictionaryArray:objects];
	if (![databasePath length] || ![tableName length] || ![objects count]) {
		fail(nil);
		return;
	}
	
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	NSArray *models = [clazz objectArrayWithKeyValuesArray:objects];
	NSError *error = nil;
	BOOL result = [store saveObjects:models error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[@(result)]);
	}
}

RCT_EXPORT_METHOD(fetch:(NSString *) databasePath table:(NSString *) tableName condition:(NSDictionary *) condition successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	
	if (![databasePath length] || ![tableName length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	BZObjectStoreConditionModel *cond = [self conditionWithDictionary:condition];
	
	NSError *error = nil;
	NSArray *objects = [store fetchObjects:clazz condition:cond error:&error];
	if (error) {
		fail(error);
	} else {
		NSArray *models = [clazz keyValuesArrayWithObjectArray:objects];
		success(@[models]);
	}

}



RCT_EXPORT_METHOD(remove:(NSString *) databasePath table:(NSString *) tableName condition:(NSDictionary *) condition successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	
	if (![databasePath length] || ![tableName length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	
	BZObjectStoreConditionModel *cond = [self conditionWithDictionary:condition];
	
	NSError *error = nil;
	
	BOOL result = [store deleteObjects:clazz condition:cond error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[@(result)]);
	}
}



RCT_EXPORT_METHOD(count:(NSString *) databasePath table:(NSString *) tableName condition:(NSDictionary *) condition successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	
	if (![databasePath length] || ![tableName length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	
	BZObjectStoreConditionModel *cond = [self conditionWithDictionary:condition];
	
	NSError *error = nil;
	
	NSNumber *result = [store count:clazz condition:cond error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[result]);
	}
}

RCT_EXPORT_METHOD(max:(NSString *) databasePath table:(NSString *) tableName column:(NSString *) column condition:(NSDictionary *) condition successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	
	if (![databasePath length] || ![tableName length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	
	BZObjectStoreConditionModel *cond = [self conditionWithDictionary:condition];
	
	NSError *error = nil;
	
	NSNumber *result = [store max:column class:clazz condition:cond error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[result]);
	}
}

RCT_EXPORT_METHOD(min:(NSString *) databasePath table:(NSString *) tableName column:(NSString *) column condition:(NSDictionary *) condition successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	
	if (![databasePath length] || ![tableName length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	
	BZObjectStoreConditionModel *cond = [self conditionWithDictionary:condition];
	
	NSError *error = nil;
	
	NSNumber *result = [store max:column class:clazz condition:cond error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[result]);
	}
}

RCT_EXPORT_METHOD(sum:(NSString *) databasePath table:(NSString *) tableName column:(NSString *) column condition:(NSDictionary *) condition successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	
	if (![databasePath length] || ![tableName length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	
	BZObjectStoreConditionModel *cond = [self conditionWithDictionary:condition];
	
	NSError *error = nil;
	
	NSNumber *result = [store max:column class:clazz condition:cond error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[result]);
	}
}

RCT_EXPORT_METHOD(total:(NSString *) databasePath table:(NSString *) tableName column:(NSString *) column condition:(NSDictionary *) condition successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	
	if (![databasePath length] || ![tableName length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	
	BZObjectStoreConditionModel *cond = [self conditionWithDictionary:condition];
	
	NSError *error = nil;
	
	NSNumber *result = [store max:column class:clazz condition:cond error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[result]);
	}
}

RCT_EXPORT_METHOD(avg:(NSString *) databasePath table:(NSString *) tableName column:(NSString *) column condition:(NSDictionary *) condition successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	databasePath = [RCTConvert NSString:databasePath];
	tableName = [RCTConvert NSString:tableName];
	
	if (![databasePath length] || ![tableName length]) {
		fail(nil);
		return;
	}
	BZObjectStore *store = self.databases[databasePath];
	Class clazz = NSClassFromString(tableName);
	if (!store || !clazz) {
		fail(nil);
		return;
	}
	
	BZObjectStoreConditionModel *cond = [self conditionWithDictionary:condition];
	
	NSError *error = nil;
	
	NSNumber *result = [store max:column class:clazz condition:cond error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[result]);
	}
}
@end
