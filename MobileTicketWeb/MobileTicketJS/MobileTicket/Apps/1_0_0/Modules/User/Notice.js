/**
 * 12306 公告
 * @providesModule Notice12306_1_0_0
 * Created by easy on 16/1/14.
 */
'use strict';

var React = require('react-native');
let ZZRandCodeView = require('ZZRandCodeView');
let ZZNetwork = require('ZZNetwork');
let ZZHUD = require('ZZHUD');


let Browser = require('react-native-browser');

var {
    TouchableOpacity,
    View,
    ListView,
    WebView,
    Text,
    } = React;

let Notice = React.createClass({


    getInitialState : function() {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            dataSource: ds.cloneWithRows([]),
        };
    },

    componentDidMount : function() {
        ZZHUD.show('加载中');
    },

    _onNavigationStateChange : function(state) {
        //this.setState({
        //    backButtonEnabled: navState.canGoBack,
        //    forwardButtonEnabled: navState.canGoForward,
        //    url: navState.url,
        //    status: navState.title,
        //    loading: navState.loading,
        //});
        console.log(state);

        ZZHUD.hide();

    },
    _injectedJavaScript : function() {
        let htmlFunc = ()=> {
            var newsList = document.getElementById('newList').getElementsByTagName('a');
            var times = document.getElementsByClassName('zxdt_time_in');
            var news = [];
            for (var i = 0; i < newsList.length;i++) {
                var v = newsList[i];
                var href = v.toString();// v.getAttribute('href');
                var title = v.getAttribute('title');
                var time = times[i].innerText;
                news.push({href, title,time});
            }
            var src = 'http://app/'+JSON.stringify(news);
            var iframe = document.createElement("iframe");
            iframe.setAttribute("src", src);
            document.documentElement.appendChild(iframe);
            iframe.parentNode.removeChild(iframe);
            iframe = null;
        };
        let js = 'var func = ' + htmlFunc.toString() + ';func();';
        return js;
    },

    _onShouldStartLoadWithRequest : function(event) {
        if (event.url.indexOf('http://app/') === 0) {
            var data = decodeURIComponent(event.url.substring('http://app/'.length));
            var urls = JSON.parse(data);
            var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.setState({dataSource:ds.cloneWithRows(urls)});
            return false;
        }
        return true;
    },

    _renderRow : function(rowData,sectionId,rowId) {
        return (
            <View style={{marginTop:10,marginLeft:5,marginRight:5}}>
                <TouchableOpacity onPress={()=>{
                    //this.props.navigator.push({component:WebView,title:rowData.title,passProps:{url:rowData.href}});

                    Browser.open(rowData.href);
                }}>
                    <Text style={{fontSize:20,color:rowId == 0 ?'red':'black'}}>{rowData.title}</Text>
                </TouchableOpacity>
                <Text style={{fontSize:12,color:'gray'}}>{rowData.time}</Text>
            </View>
        );
    },

    render : function() {
        return (
            <View style={{flex:1}}>
                <ListView dataSource={this.state.dataSource} renderRow={this._renderRow}/>
                <View style={{opacity:0,height:0}}>
                    <WebView ref={'webView'} url={"http://www.12306.cn/mormhweb/zxdt/index_zxdt.html"}
                             onNavigationStateChange={this._onNavigationStateChange}
                             injectedJavaScript = {this._injectedJavaScript()}
                             onShouldStartLoadWithRequest={this._onShouldStartLoadWithRequest}
                    />
                </View>

            </View>
        );
    }
});

module.exports = Notice;