
/* JavaScript content from js/controller/updateUserEmail.js in folder common */
(function() {
	var focusArray=[];
	var preView = "";
	jq("#updateUserEmailView").live("pageshow", function() {
		focusArray=[];
	});
	var util = mor.ticket.util;
	//var clearTimeOutFun = "";
	function registerAutoScroll(){	
		util.enableAutoScroll('#updateUserEmailValidationCode',focusArray);
	}
	jq("#updateUserEmailView").live("pageinit", function() {
		registerAutoScroll();
		jq("#updateUserEmailValidationCode").change(function(){
			 var checkNum = /^[0-9]{6}$/;
			 if(!checkNum.test(jq("#updateUserEmailValidationCode").val())){
				 mor.ticket.util.alertMessage("手机校验码不正确");
				 jq("#updateUserEmailValidationCode").val("");
				 return;
			 }
		});
		
		jq("#updateUserEmailBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("my12306.html");
			return false;
		});
		jq("#updateUserEmailValidationCodeBtn").bind("tap", function() {
			var user = mor.ticket.loginUser;
			if(user.is_receive !== "Y"){
				WL.SimpleDialog.show("温馨提示","您的手机号还未核验，请核验手机号后修改邮箱。", [ {
					text : '确定',
					handler : function() {
						mor.ticket.util.changePage("userPhoneVerifyCheck.html");
					}
				} ]);
				return false;
			}
			var checkFormUtil = mor.ticket.checkForm;
			if(util.isNoValue(jq("#email").val())){
				util.alertMessage("请输入邮箱地址");
				return;
			}
			if(util.isNoValue(jq("#passwdInput").val())){
				util.alertMessage("请填写密码");
				return;
			}
			if(!checkFormUtil.isEmail(jq("#email").val())){
				util.alertMessage("请输入正确的邮箱地址");
				return;
			}
			var commonParameters = {
					'email' : jq("#email").val(),
					'busCode': "modifyEmail"
				};
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "getModifyUserMobieCode"
			};
			
			var options =  {
					onSuccess: updateUserEmailValidationCodeRequestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
		});
		jq("#updateUserEmailEnterValidationCodeBtn").bind("tap", function() {
			var checkFormUtil = mor.ticket.checkForm;
			if(util.isNoValue(jq("#email").val())){
				util.alertMessage("请输入邮箱地址");
				return;
			}
			if(util.isNoValue(jq("#passwdInput").val())){
				util.alertMessage("请填写密码");
				return;
			}
			if(!checkFormUtil.isEmail(jq("#email").val())){
				util.alertMessage("请输入正确的邮箱地址");
				return;
			}
			if(util.isNoValue(jq("#updateUserEmailValidationCode").val())){
				util.alertMessage("请填写短信验证码");
				return;
			}
			var checkNum = /^[0-9]{6}$/;
			if(!checkNum.test(jq("#updateUserEmailValidationCode").val())){
				mor.ticket.util.alertMessage("手机校验码不正确");
				jq("#updateUserEmailValidationCode").val("");
				return;
			}
			
			var commonParameters = {
					'newEmail': jq("#email").val(),
					'passWord': hex_md5(jq("#passwdInput").val()),
					'check_code': jq("#updateUserEmailValidationCode").val()
			};
			
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "changeEmail"
			};
			
			var options =  {
					onSuccess: requestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
		});
		});
	jq("#updateUserEmailView").live("pagebeforeshow", function(e,data) {
		preView = data.prevPage.attr("id");
		jq("#updateUserEmailValidationCode").val("");
		jq("#passwdInput").val("");
		if(jq("#updateUserEmailValidationCodeBtn").hasClass("ui-disabled")){
			//clearTimeout(clearTimeOutFun);
			jq("#updateUserEmailValidationCodeBtn").removeClass("ui-disabled");
			jq("#updateUserEmailValidationCodeBtn").html("获取验证码");
		}
		var userInfo = mor.ticket.loginUser;
		jq("#email").val(userInfo.email);
	});
	function updateUserEmailValidationCodeRequestSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;	
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			WL.SimpleDialog.show("温馨提示", "修改邮箱验证码已发送至您的手机,请查收。", [ {
				text : '确定',
				handler : function() {
					updateUserEmailCountDown(jq("#updateUserEmailValidationCodeBtn"),120);
				}
			} ]);
		} else {
			util.alertMessage(invocationResult.error_msg);
		}
	}
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;	
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.userInfo.email = jq("#email").val();
			mor.ticket.loginUser.email = jq("#email").val();
			WL.SimpleDialog.show("温馨提示","您的邮箱已改为"+mor.ticket.userInfo.email+"。", [ {
				text : '确定',
				handler : function() {
					jq.mobile.changePage("my12306.html");
				}
			} ]);
		}else{
			util.alertMessage(invocationResult.error_msg);
		}
	}
	function updateUserEmailCountDown(obj,second){
		 if(second>=0){
	          if(typeof buttonDefaultValue === 'undefined' ){ 
	            buttonDefaultValue =  "获取验证码"; 
	        }
	        obj.addClass("ui-disabled");
	        var objValue = buttonDefaultValue+'('+second+')';
	        obj.html(objValue);
	        second--;
	        //clearTimeOutFun =  setTimeout(function(){countDown(obj,second);},1000); 
	        setTimeout(function(){updateUserEmailCountDown(obj,second);},1000); 
	    }else{
	        obj.removeClass("ui-disabled");
	        obj.html(buttonDefaultValue);
	    }    
	}
})();