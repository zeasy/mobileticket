
/* JavaScript content from js/controller/continuePayment.js in folder common */
(function(){
	var prevPageId = "";
	function continuePaymentFn(){
		mor.ticket.viewControl.tab2_cur_page="continuePayment.html";
		//var proMsg = mor.ticket.cache.promptMsg.confirmOrder;
		//jq("#confirm_OrderTips").html(proMsg);
		var util = mor.ticket.util;
		var cache = mor.ticket.cache;
		var queryOrder = mor.ticket.queryOrder;
		var unFinishedOrder = queryOrder.getCurrentUnfinishedOrder();
		var ticketList = unFinishedOrder.myTicketList;
		var ticket = ticketList[0];
		var str = "单程：";
		jq("#continueSendTicketTotalPriceInfo").hide();
		if(isRoundTripOrder(ticketList)){
			jq("#continuePaymentOrderDetailPre").show();
			var RoundTripOrder = getRoundTripTicketList(ticketList);
			var goTicketList = RoundTripOrder.goList;
			ticket = goTicketList[0];
			jq("#continuePaymentOrderDetailsPromptPre").html("往程：" + util.getLocalDateString2(ticket.train_date));
			jq("#continuePaymentFromStationNamePre").html(cache.getStationNameByCode(ticket.from_station_telecode));
			jq("#continuePaymentTrainStartTimePre").html(util.formateTrainTime(ticket.start_time) + " 出发");
			jq("#continuePaymentTrainCodeNamePre").html(ticket.station_train_code);
			jq("#continuePaymentToStationNamePre").html(cache.getStationNameByCode(ticket.to_station_telecode));
			jq("#continuePaymentTrainReachTimePre").html(util.formateTrainTime(ticket.arrive_time) + " 到达");	
			jq("#continuePaymentBookedTicketDetailsGridPre").html(generateTicketDetailsGrid(goTicketList));
						
			ticketList  = RoundTripOrder.backList;			
			ticket = ticketList[0];
			str = "返程：";
		}else{
			jq("#buyBackTicketId").show();
		}
    	var endTime=mor.ticket.paytimer.endTime;
    	if(mor.ticket.paytimer.intervalId){
			clearInterval(mor.ticket.paytimer.intervalId);
			mor.ticket.paytimer.intervalId = null;
		}
    	
    	var paddingFn = mor.ticket.util.paddingWidth;
    	mor.ticket.paytimer.intervalId=setInterval(function(){
    		//TODO position:abslote
		    var nowTime = mor.ticket.util.getNewDate();
		    var nMS=endTime.getTime() - nowTime.getTime();
		          var myD=Math.floor(nMS/(1000 * 60 * 60 * 24));
		          var myM=paddingFn(Math.floor(nMS/(1000*60)) % 60, 2, 0);
		          var myS=paddingFn(Math.floor(nMS/1000) % 60, 2, 0);
		          if(myD >= 0){
		  			jq("#continuePayTime").html(myM+":"+myS);
		          }else{
			          clearInterval(mor.ticket.paytimer.intervalId);
			          mor.ticket.paytimer.intervalId = null;
			          mor.ticket.viewControl.tab2_cur_page="queryOrder.html";
			          jq("#continuePay_line").css('display','none'); 
			  		  WL.SimpleDialog.show(
								"温馨提示", 
								"支付有效时间已过，席位已自动释放给其他乘客", 
								[ {text : '确定', handler: function() {
									mor.ticket.viewControl.bookMode = "dc";
									jq.mobile.changePage(vPathViewCallBack()+"MobileTicket.html");
								}}]
					  );
		  		 }
	      }, 1000);
		
    	jq("#continuePaymentOrderDetailsPrompt").html(str + util.getLocalDateString2(ticket.train_date));
		jq("#continuePaymentFromStationName").html(cache.getStationNameByCode(ticket.from_station_telecode));
		jq("#continuePaymentTrainStartTime").html(util.formateTrainTime(ticket.start_time) + " 出发");
		jq("#continuePaymentTrainCodeName").html(ticket.station_train_code);
		jq("#continuePaymentToStationName").html(cache.getStationNameByCode(ticket.to_station_telecode));
		jq("#continuePaymentTrainReachTime").html(util.formateTrainTime(ticket.arrive_time) + " 到达");	
		jq("#continuePaymentBookedTicketDetailsGrid").html(generateTicketDetailsGrid(ticketList));
		var myTicketList = unFinishedOrder.myTicketList;
		var ticket_status = myTicketList[0].ticket_status_code;
		if(ticket_status == 'j' || ticket_status == 's'){
			var str = "";
			if(ticket_status == 'j'){
				str = "改签";
			}else{
				str = "变更到站";
			}
			jq("#continuePaymentOrderTotalPriceInfo").hide();
			jq("#buyBackTicketId").hide();
			queryOrder.getOrderPrice();
			if(unFinishedOrder.returnTotalPrice >= 0){
				jq("#continuePaymentOrderDifPrice").html(unFinishedOrder.returnTotalPrice+"元&nbsp;");
				if(unFinishedOrder.returnTotalPrice == 0){
					jq("#continuePaymentOrderReturnCostDiv").hide();
					jq("#continuePaymentOrderReturnRateId").hide();
				}else{
					if(mor.ticket.returnCost.returnCostPrice === undefined || mor.ticket.returnCost.returnCostPrice === ""){
						var returnCost = getReturnCost(myTicketList[0].reserve_time,unFinishedOrder.originTrainDate,unFinishedOrder.returnTotalPrice);
						mor.ticket.returnCost.returnCostPrice = returnCost.returnCostPrice;
						mor.ticket.returnCost.returnFact = returnCost.returnFact;
						mor.ticket.returnCost.returnRate = returnCost.returnRate;
					}
					jq("#continuePaymentOrderReturnCostDiv").show();
					jq("#continuePaymentOrderReturnRateId").show();
					jq("#continuePaymentOrderReturnRate").html(mor.ticket.returnCost.returnRate+"%&nbsp;&nbsp;&nbsp;");
					jq("#continuePaymentOrderReturnFact").html(mor.ticket.returnCost.returnFact+"元 &nbsp;&nbsp;");
					jq("#continuePaymentOrderReturnCost").html(mor.ticket.returnCost.returnCostPrice+"元");
				}
				jq("#continuePaymentOrderDifPriceInfo").show();
				jq("#continuePaymentConfirmPaymentBtn").html("确认"+str+"");
			}else {
				jq("#continuePaymentNewTicketPrice").html(unFinishedOrder.newTotalPrice+"元");
				jq("#continuePaymentOldTicketPrice").html(unFinishedOrder.originTotalPrice+"元");
				jq("#continuePaymentConfirmPaymentBtn").html("立即支付");
				jq("#continuePaymentOrderRepayPriceInfo").show();
			}
			jq("#continuePaymentSendTicketVisitBtn").hide();
			jq("#continuePaymentCancelOrderBtn").html("取消"+str+"");
		}else{
			jq("#continuePaymentOrderTotalPrice").html(unFinishedOrder.ticket_price_all);
			jq("#continuePaymentOrderTotalPrice").show();
			queryExpressTickets(myTicketList[0].sequence_no);
		}
		if(window.ticketStorage.getItem("isSendTicket")!="Y" || window.ticketStorage.getItem("isSendTicket")!=="Y"){
			jq("#continuePaymentSendTicketVisitBtn").hide();
		}
		
		
		jq("#continuePaymentView .ui-content").iscrollview("refresh");
	}
	jq("#continuePaymentView").live("pagebeforeshow", function(e,data){
		prevPageId = data.prevPage.attr("id");	
		if(prevPageId == "insuranceTicketView" && mor.ticket.viewControl.succInsuranceFlag == true){
			var ticketList = mor.ticket.queryOrder.getCurrentUnfinishedOrder().myTicketList;
				for(var j = 0;j<ticketList.length;j++){
						var currTicket = jq("#" + ticketList[j].station_train_code + ticketList[j].ticket_type_code + ticketList[j].seat_no + ticketList[j].passenger_id_type_code + ticketList[j].passenger_id_no);
						currTicket.find("#continueInsuranceTicketFlag").addClass("insuranceTicketFlag").html("保");
			}
				var insuranceTotalPrice = ticketList.length*2;
				jq("#continueInsuranceTotalPrice").html(parseFloat(insuranceTotalPrice).toFixed(2));
				jq("#continueInsuranceTicketTotalPriceInfo").css("display","block");
				jq("#continuePaymentView .ui-content").iscrollview("refresh");
		}
	});
	jq("#continuePaymentView").live("pagehide", function(){
		clearInterval(mor.ticket.paytimer.intervalId);
	});
	
	jq("#continuePaymentView").live("pageinit", function(){
		registerContinuePaymentBackBtnListener();
		registerContinuePaymentCancelOrderBtnLisener(); 
		registercontinuePaymentConfirmPaymentBtnListener();
		registerContinuePaymentBuyBackTicketLisener();
		//送票上门
		sendTicketVistPage();
		//线下支付btn
		//registerOffLinePayConfirmTicketBtnListeners();
		if(mor.ticket.loginUser.isAuthenticated === "Y"){
			continuePaymentFn();
		}else{
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
			} else {
				registerAutoLoginHandler(continuePaymentFn, autologinFailJump);
			}

//			mor.ticket.util.changePage(vPathCallBack()+"loginTicket.html");
		}
		mor.ticket.util.initAppVersionInfo();
	});
	
	//获取改签差价手续费
	function getReturnCost(reserve_time,originTrainDate,returnTotalPrices){
		returnTotalPrice = parseFloat(returnTotalPrices);
		var newTrainStartTime = mor.ticket.util.setMyDate3(reserve_time);
		var oldTrainStartTime = mor.ticket.util.setMyDate3(originTrainDate);
		var diffTime =  oldTrainStartTime.getTime() - newTrainStartTime.getTime();
		if (diffTime > 48*60*60*1000  && diffTime <= 15*24*60*60*1000){	
			if(returnTotalPrice >= 0.00 && returnTotalPrice <= 2.00){
				mor.ticket.returnCost.returnCostPrice = returnTotalPrice.toFixed(2);
			}else if(returnTotalPrice > 2.00 && returnTotalPrice <= 40.00){
				mor.ticket.returnCost.returnCostPrice = 2;
			}else if(returnTotalPrice > 40.00){
				mor.ticket.returnCost.returnCostPrice = (returnTotalPrice/20).toFixed(2);
			}else{
				mor.ticket.returnCost.returnCostPrice = 0;
			};
			mor.ticket.returnCost.returnRate = 5;
		}else if(diffTime > 24*60*60*1000 && diffTime <= 48*60*60*1000){
			if(returnTotalPrice >= 0.00 && returnTotalPrice <= 2.00){
				mor.ticket.returnCost.returnCostPrice = returnTotalPrice.toFixed(2);
			}else if(returnTotalPrice > 2.00 && returnTotalPrice <= 20.00){
				mor.ticket.returnCost.returnCostPrice = 2;
			}else if(returnTotalPrice > 20.00){
				mor.ticket.returnCost.returnCostPrice = (returnTotalPrice/10).toFixed(2);
			}else{
				mor.ticket.returnCost.returnCostPrice = 0;
			};
			mor.ticket.returnCost.returnRate = 10;
		}else if(diffTime <= 24*60*60*1000){
			if(returnTotalPrice >= 0.00 && returnTotalPrice <= 2.00){
				mor.ticket.returnCost.returnCostPrice = returnTotalPrice.toFixed(2);
			}else if(returnTotalPrice > 2.00 && returnTotalPrice <= 10.00){
				mor.ticket.returnCost.returnCostPrice = 2;
			}else if(returnTotalPrice > 10.00){
				mor.ticket.returnCost.returnCostPrice = (returnTotalPrice/5).toFixed(2);
			}else{
				mor.ticket.returnCost.returnCostPrice = 0;
			}
			mor.ticket.returnCost.returnRate = 20;
		}else{
			mor.ticket.returnCost.returnCostPrice = 0;
		}
		
		//手续费小数点后两位计算
		
		mor.ticket.returnCost.returnCostPrice = (parseInt(mor.ticket.returnCost.returnCostPrice*2+0.5)/2).toFixed(2);
		mor.ticket.returnCost.returnFact = (returnTotalPrice - mor.ticket.returnCost.returnCostPrice).toFixed(2);
		return mor.ticket.returnCost;
	}
	
	
	function isRoundTripOrder(order){
		if(order.length > 1){
			var ticket = order[0];
			for(var i=1; i< order.length ; i++){
				if(ticket.from_station_telecode != order[i].from_station_telecode){
					return true;
				}
			}
			return false;
		}else {
			return false;
		}
		
	}
	
	function getRoundTripTicketList(order){
		var list1 = [];
		var list2 = [];
		var ticket = order[0];	
		list1.push(ticket);
		for(var i=1; i< order.length ; i++){
			if(ticket.from_station_telecode == order[i].from_station_telecode){
				list1.push(order[i]);
			} else {
				list2.push(order[i]);
			}
		}
		var list1Date = list1[0].train_date+list1[0].start_time;
		var list2Date = list2[0].train_date+list2[0].start_time;
		if(list1Date < list2Date){
			return {
				goList:list1,
				backList:list2
			};
		}else{
			return {
				goList:list2,
				backList:list1
			};
		}
	}
	
	function registerContinuePaymentBackBtnListener(){
		jq("#continuePaymentBackBtn").bind("tap",function(e){
			mor.ticket.util.changePage("orderList.html");
			return false;
		});
	}
	
	function registerContinuePaymentCancelOrderBtnLisener(){
		jq("#continuePaymentCancelOrderBtn").bind("tap", function(){
			if(mor.ticket.util.isAndroid()){
				WL.SimpleDialog.show(
	    				"温馨提示", 
	    				"一天内3次申请车票成功后取消订单，当日将不能在12306继续购票！", 
	    				[ 
	    				  {text : '确定', handler: function(){cancelOrderProcedure();}},
	    				  {text : '取消', handler: function(){}}
	    				]
	    		);
			}else{
				WL.SimpleDialog.show(
	    				"温馨提示", 
	    				"一天内3次申请车票成功后取消订单，当日将不能在12306继续购票！", 
	    				[ 
	    				  {text : '取消', handler: function(){}},
	    				  {text : '确定', handler: function(){cancelOrderProcedure();}}
	    				 ]
	    		);
			}
			
			return false;
		});
	}
	
	function cancelOrderProcedure(){
		var order = mor.ticket.queryOrder.getCurrentUnfinishedOrder();
		var util = mor.ticket.util;
		var cancelFlag = '0';// 0:取消普通待支付订单;1取消改签待支付订单
		for(var i=0; i< order.myTicketList.length; i++){
			if(order.myTicketList[i].ticket_status_code == 'j' || order.myTicketList[i].ticket_status_code == 's'){//改签待支付
				cancelFlag = '1';
				break;
			}
		}		
		var commonParameters = {			
			'sequence_no': order.myTicketList[0].sequence_no,
			'coach_no': '00',
			'seat_no':'0000',
			'batch_no':'0',
			'cancel_flag':cancelFlag
		};
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "cancelOrder"
		};
		
		var options =  {
				onSuccess: requestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
	}
	
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {	
			//不用更改mode，保持原来模型的订票模式
			mor.ticket.viewControl.bookMode = "dc";
			mor.ticket.viewControl.tab2_cur_page="queryOrder.html";
			mor.ticket.viewControl.isNeedRefreshUnfinishedOrder = true;
			jq.mobile.changePage("queryOrder.html");
			mor.ticket.util.alertMessage("订单取消成功");
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	function registercontinuePaymentConfirmPaymentBtnListener(){
		jq("#continuePaymentConfirmPaymentBtn").bind("tap", function(){
			//mjl
			var str = "改签";
			if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
				str = "变更到站";
			}
			var message="确定要"+str+"吗？";
			if(jq("#continuePaymentConfirmPaymentBtn .ui-btn-text").html()=="确认改签" 
				|| jq("#continuePaymentConfirmPaymentBtn .ui-btn-text").html()=="确认变更到站"){
				if(mor.ticket.util.isAndroid()){
					WL.SimpleDialog.show(
							"温馨提示", 
							message, 
							[
							  {text : '确定', handler: comfirmPayment},
							  {text : '取消', handler: function(){}}
							]
						);
				}else{
					WL.SimpleDialog.show(
							"温馨提示", 
							message, 
							[ 
							  {text : '取消', handler: function(){}},
							  {text : '确定', handler: comfirmPayment}
							  ]
						);
				}
			}else{
				comfirmPayment();
			}
			
			/**
			mor.ticket.viewControl.tab2_cur_page="queryOrder.html";
			var order = mor.ticket.queryOrder.getCurrentUnfinishedOrder();
			if(order.returnTotalPrice == undefined ||order.returnTotalPrice < 0){//普通订单支付，或者改签 低改高
				requestEpayProcedure(order);
			}else{
				changeTicketNormalProcedure(order);	//平改，高改低	
			}
			return false;
			**/
		});
	}
	
	
	function comfirmPayment(){
		mor.ticket.viewControl.tab2_cur_page="queryOrder.html";
		var order = mor.ticket.queryOrder.getCurrentUnfinishedOrder();
		if(order.returnTotalPrice == undefined ||order.returnTotalPrice < 0){//普通订单支付，或者改签 低改高,或者变更到站低改高
			requestEpayProcedure(order);
		}else{
			changeTicketNormalProcedure(order);	//平改，高改低	
		}
		return false;
	}
	
	
	
	function requestEpayProcedure(order){
		var commonParameters = "";
		var util = mor.ticket.util;
		var pay_start = 1;//普通付费
		if(order.myTicketList[0].ticket_status_code == "j" || order.myTicketList[0].ticket_status_code == "s"){
			pay_start = 2;//改签/变更到站付费
		}
		//订单批次号
		var batch_no_list = [];
		for(var i=0;i<order.myTicketList.length;i++){
			batch_no_list.push(order.myTicketList[i].batch_no);
		}
		batch_no_list.sort();
		util.uniq(batch_no_list);
		var batch_no_str = batch_no_list.join("#");
		
		if(pay_start == 1){
			commonParameters = {			
					'sequence_no': order.myTicketList[0].sequence_no,
					'batch_no': batch_no_str,//order.myTicketList[0].batch_no,
					'order_timeout_date':order.myTicketList[0].pay_limit_time,
					'pay_start':pay_start.toString()
				};
		}
		if(pay_start == 2){
			var checkResignExpress;
			if(order.if_deliver == "Y"){
				checkResignExpress = "Y";
			}else{
				checkResignExpress = "N";
			}
			commonParameters = {			
					'sequence_no': order.myTicketList[0].sequence_no,
					'batch_no': batch_no_str,//order.myTicketList[0].batch_no,
					'order_timeout_date':order.myTicketList[0].pay_limit_time,
					'pay_start':pay_start.toString(),
					'checkResignExpress':checkResignExpress
				};
		}
		
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "epay"
		};
		
		var options =  {
				onSuccess: epaySucceededNew,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
	}
	
	function epaySucceededNew(result){
		if(busy.isVisible()){
			busy.hide();
		}	
		var invocationResult = result.invocationResult;
		var orderManager = mor.ticket.orderManager;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				orderManager.paymentOrder = mor.ticket.queryOrder.getCurrentUnfinishedOrder();
				//获取batch no用来在支付完成后过滤对应车票
				var myTicketList = orderManager.paymentOrder.myTicketList;
				mor.ticket.payment.batch_nos = [];
				for(var i=0;i<myTicketList.length;i++){
					var batch_no = myTicketList[i].batch_no;
					mor.ticket.payment.batch_nos.push(batch_no);
				}
				var parameters={
			         "showLocationBar": true,
			         "interfaceName":invocationResult['interfaceName'],
			         "interfaceVersion":invocationResult['interfaceVersion'],
			         "tranData":invocationResult['tranData'],
			         "merSignMsg":invocationResult['merSignMsg'],
			         "appId":invocationResult['appId'],
			         "transType":invocationResult['transType'],
					 "epayurl":invocationResult['epayurl']
				};
				window.plugins.childBrowser.showWebPage(invocationResult['epayurl'], parameters); 
				//mor.ticket.util.changePage("queryOrder.html");
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	
	function changeTicketNormalProcedure(ticketList){//平改、或者高改低时流程
		var currentTicketDetail = ticketList.myTicketList[0];
		var util = mor.ticket.util;
		var pay_mode;
		if(ticketList.returnTotalPrice == 0){
			pay_mode = 'N';
		}else{
			pay_mode = 'T';
		}
		var checkResignExpress;
		if(ticketList.if_deliver == "Y"){
			checkResignExpress = "Y";
		}else{
			checkResignExpress = "N";
		}
		var commonParameters = {			
			'sequence_no': currentTicketDetail.sequence_no,
			'batch_no':currentTicketDetail.batch_no,
			'lose_time':currentTicketDetail.limit_time,
			'pay_type':pay_mode,
			'checkResignExpress':checkResignExpress
		};
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "confirmChangeTicket"
		};
		
		var options =  {
				onSuccess: requestConfirmChangeTicketSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};

		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function  requestConfirmChangeTicketSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			if(invocationResult.isCheckResignExpress == "0"){
				if(mor.ticket.util.isAndroid()){
					WL.SimpleDialog.show(
							"温馨提示", 
							invocationResult.checkResignExpressMsg, 
							[{text : '确定', handler: function(){
								  	var order = mor.ticket.queryOrder.getCurrentUnfinishedOrder();
									var currentTicketDetail = order.myTicketList[0];
									var util = mor.ticket.util;
									var pay_mode;
									if(order.returnTotalPrice == 0){
										pay_mode = 'N';
									}else{
										pay_mode = 'T';
									}
									var commonParameters = {			
										'sequence_no': currentTicketDetail.sequence_no,
										'batch_no':currentTicketDetail.batch_no,
										'lose_time':currentTicketDetail.limit_time,
										'pay_type':pay_mode,
										'checkResignExpress':'N'
									};
									
									var invocationData = {
											adapter: mor.ticket.viewControl.adapterUsed,
											procedure: "confirmChangeTicket"
									};
									
									var options =  {
											onSuccess: requestConfirmChangeTicketSucceeded,
											onFailure: util.creatCommonRequestFailureHandler()
									};

									mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
							  }}, {text : '取消', handler: function(){}}]
					);
				}else{
					WL.SimpleDialog.show(
							"温馨提示", 
							invocationResult.checkResignExpressMsg, 
							[ {text : '取消', handler: function(){}},
							  {text : '确定', handler: function(){
								  	var order = mor.ticket.queryOrder.getCurrentUnfinishedOrder();
									var currentTicketDetail = order.myTicketList[0];
									var util = mor.ticket.util;
									var pay_mode;
									if(order.returnTotalPrice == 0){
										pay_mode = 'N';
									}else{
										pay_mode = 'T';
									}
									var commonParameters = {			
										'sequence_no': currentTicketDetail.sequence_no,
										'batch_no':currentTicketDetail.batch_no,
										'lose_time':currentTicketDetail.limit_time,
										'pay_type':pay_mode,
										'checkResignExpress':'N'
									};
									
									var invocationData = {
											adapter: mor.ticket.viewControl.adapterUsed,
											procedure: "confirmChangeTicket"
									};
									
									var options =  {
											onSuccess: requestConfirmChangeTicketSucceeded,
											onFailure: util.creatCommonRequestFailureHandler()
									};

									mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
							  }}]
					);
				}
				
			}else{
				mor.ticket.viewControl.isNeedRefreshUnfinishedOrder = true;
				mor.ticket.viewControl.bookMode = "dc";
				jq.mobile.changePage("queryOrder.html");
				if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
					mor.ticket.util.alertMessage("变更到站成功！");
				}else{
					mor.ticket.util.alertMessage("改签成功！");
				}
				mor.ticket.changeOrchangeStation.changeOrchangeStation = "";
			}
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	//线下支付--Begin
	//周自昌
	

		function registerOffLinePayConfirmTicketBtnListeners() {
			jq("#offLinePayConfirmTickets").bind("tap",function(){
			var unFinishedOrder = mor.ticket.queryOrder.getCurrentUnfinishedOrder();
			var ticketList = unFinishedOrder.myTicketList;;
				WL.SimpleDialog
						.show(
								"温馨提示",
								"确定要线下支付吗?",
								[
										{
											text : '取消',
											handler : function() {
											}
										},
										{
											text : '确定',
											handler : function() {
												var parameter = {
													'sequence_no' : ticketList[0].sequence_no
												};
												getOffLinePayParameter(parameter);
											}
										} ]);
			});
	}
		
	function getOffLinePayParameter(parameter){
		var util = mor.ticket.util;
		var commonParameters = parameter;
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "offLinePay"
		};
		
		var options =  {
				onSuccess: offLinePaySucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
//		WL.Client.invokeProcedure(invocationData, options);
//		busy.show();
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function offLinePaySucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var orderManager=mor.ticket.orderManager;
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)){
			orderManager.paymentOrder.myTicketList = [];
			var queryOrder = mor.ticket.queryOrder;
			var unFinishedOrder = queryOrder.getCurrentUnfinishedOrder();
			var ticketList = unFinishedOrder.myTicketList;
			for(var i=0; i<ticketList.length; i++){
				var ticket = ticketList[i];
				orderManager.paymentOrder.myTicketList.push(ticket);
			}
			for(var i=0;i<ticketList.length;i++){
			if(ticketList[i].sequence_no!=invocationResult.sequence_no){
				ticketList[i].sequence_no=invocationResult.sequence_no;
				}
			}
			mor.ticket.util.changePage("offLinePayFinish.html");
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	//线下支付--End
	
	//送票上门
	function sendTicketVistPage(){
		jq("#continuePaymentSendTicketVisitBtn").bind("tap", function(e){
			e.stopImmediatePropagation();
			e.preventDefault();
			// 判断送票时间间隔是否超过36小时
			var queryOrder = mor.ticket.queryOrder;
			var unFinishedOrder = queryOrder.getCurrentUnfinishedOrder();
			var ticketList = unFinishedOrder.myTicketList;
			var util = mor.ticket.util;
			// 判断往返程
			if(isRoundTripOrder(ticketList)){
				var RoundTripOrder = getRoundTripTicketList(ticketList);
				var goTicketList = RoundTripOrder.goList;
				var ticket_go = goTicketList[0];
				var ticket_back = RoundTripOrder.backList[0];
				var goTime = ticket_go.train_date;
				var backTime = ticket_back.train_date;
				if(!util.getSendTicketTimeLimit(goTime) && !util.getSendTicketTimeLimit(backTime)){
					util.alertMessage("目前只支持配送距票面记载开车时间超过2天的车票");
					return false;
				}
			}else{
				var ticket = ticketList[0];
				var startTime = ticket.train_date;
				if(!util.getSendTicketTimeLimit(startTime)){
					util.alertMessage("目前只支持配送距票面记载开车时间超过2天的车票");
					return false;
				}
			}
			mor.ticket.viewControl.current_tab = "continuePaymentTab";
			mor.ticket.viewControl.current_tab1 = "continuePaymentTab";
			mor.ticket.util.changePage(vPathCallBack() + "sendTicketVisit.html");
			return false;
		});
//		jq("#continuePaymentinsuranceTicketBtn").bind("tap", function(e){
//			e.stopImmediatePropagation();
//			e.preventDefault();
//			mor.ticket.viewControl.current_tab = "continuePaymentTab";
//			mor.ticket.viewControl.current_tab1 = "continuePaymentTab";
//			mor.ticket.util.changePage(vPathCallBack() + "insuranceTicket.html");
//			return false;
//		});	
	}
	var ticketDetailsGridTemplate =
		"{{~it :ticket:index}}" +
		"<div class='orderDetailList' id='{{=ticket.station_train_code}}{{=ticket.ticket_type_code}}{{=ticket.seat_no}}{{=ticket.passenger_id_type_code}}{{=ticket.passenger_id_no}}'>" +
			"<div class='ui-grid-a'>" +
				"<div class='ui-block-a text-ellipsis'><span id='continueInsuranceTicketFlag'></span><span id='continueSendFlag'></span>{{=ticket.passenger_name}}</div>" +
				"{{ if(ticket.passenger_id_type_code === '1'){ }}"+ 
				"<div class='ui-block-b'>{{=mor.ticket.util.getIdNoStar(ticket.passenger_id_no)}}</div>"+
				"{{ }else{ }}"+
				"<div class='ui-block-b'>{{=ticket.passenger_id_no}}</div>"+
				"{{ } }}"+
				 "<div class='ui-block-c'>{{=mor.ticket.util.getIdTypeName(ticket.passenger_id_type_code)}}</div>"+
			"</div>" +
			"<div class='ui-grid-b'>" +
				"<div class='ui-block-a'>{{=mor.ticket.util.getTicketTypeName(ticket.ticket_type_code)}}</div>" +
				"<div class='ui-block-b'>{{=mor.ticket.util.getSeatTypeName(ticket.seat_type_code,ticket.seat_no)}}</div>" +
				"<div class='ui-block-c'><span class='text_orange'>{{=ticket.coach_name}}</span>车</div>" + 
				"<div class='ui-block-d'><span class='text_orange'>{{=ticket.seat_name}}</span></div>" + 
				"<div class='ui-block-e'><span class='text_orange'>{{=ticket.ticket_price}}元</span></div>" + 
			"</div>" +
		"</div>" +
		"{{~}}";
	var generateTicketDetailsGrid = doT.template(ticketDetailsGridTemplate);
	
	function registerContinuePaymentBuyBackTicketLisener(){
		jq("#buyBackTicketId").off().bind("tap","a",function(){
        	var orderManager = mor.ticket.orderManager;
        	var myTicketList = mor.ticket.queryOrder.getCurrentUnfinishedOrder().myTicketList;
        	  mor.ticket.DcSeatTypeList.result.length = 0;
        	  for(var i = 0;i<myTicketList.length;i++){
				   mor.ticket.DcSeatTypeList.result.push(myTicketList[i].seat_type_code);
				   }
        	var currentTicket = myTicketList[0];
        	var model = mor.ticket.leftTicketQuery;
        	var passengerList = [];
        	for(var i = 0 ;i<myTicketList.length;i++){
        		var passenger = {};
				passenger.id_no = myTicketList[i].passenger_id_no;
				passenger.id_type = myTicketList[i].passenger_id_type_code;
				passenger.mobile_no = myTicketList[i].mobile_no;
				passenger.user_name = myTicketList[i].passenger_name;
				passenger.ticket_type = myTicketList[i].ticket_type_code;
				passenger.seat_type = myTicketList[i].seat_type;
				passengerList[i]=passenger;
        	}
        	model.from_station_telecode = currentTicket.from_station_telecode;
			model.to_station_telecode = currentTicket.to_station_telecode;
			model.train_date_back = "";
			model.train_date = mor.ticket.util.changeDateType(currentTicket.train_date);
			orderManager.orderTicketList = myTicketList;
			orderManager.confirmRoundTripTicketList(passengerList);
			orderManager.setPreTicketDetail(currentTicket);
			mor.ticket.viewControl.bookMode = "fc";
			if(currentTicket.ticket_type_code === "3"){
				model.purpose_codes = "0X";	
			}else{
				model.purpose_codes = "00";	
			}
			mor.ticket.util.changePage(vPathViewCallBack()+"MobileTicket.html");
			return false;
        });
	}
	
	// 查询已保存快递订单信息
	function queryExpressTickets(sequence_no){
		var util = mor.ticket.util;
		var commonParameters = {
					"sequence_no" : sequence_no,
					"query_flag" : '0',
					"pay_flag" : '0'
			};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryExpressTickets"
			};
		var options =  {
				onSuccess: requestQueryExpressTicketsSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestQueryExpressTicketsSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			var expressTickets = invocationResult.listExpressOrderForQueryReturnBean;
			if(expressTickets.length > 0){
				jq("#buyBackTicketId").css("display","none");
				var queryOrder = mor.ticket.queryOrder;
				var unFinishedOrder = queryOrder.getCurrentUnfinishedOrder();
				var ticketList = unFinishedOrder.myTicketList;
				var expressTotalPrice = 0;
				for(var i = 0;i<expressTickets.length;i++){
					var ticket_message = expressTickets[i].ticket_message.split("#");
					var total_price = expressTickets[i].total_price !=="0" ? expressTickets[i].total_price : "1700";
					expressTotalPrice += parseInt(total_price);
					for(var j = 0;j<ticketList.length;j++){
						var temp_message = ticketList[j].sequence_no + "+" +ticketList[j].batch_no + "+" +
						ticketList[j].coach_no + "+" + ticketList[j].seat_no + "+" + ticketList[j].passenger_id_type_code + "+" +
						ticketList[j].passenger_id_no + "+" + ticketList[j].passenger_name;
						if(jq.inArray(temp_message,ticket_message) != -1){
							var currTicket = jq("#" + ticketList[j].station_train_code + ticketList[j].ticket_type_code + ticketList[j].seat_no + ticketList[j].passenger_id_type_code + ticketList[j].passenger_id_no);
							currTicket.find("#continueSendFlag").addClass("sendTicketFlag").html("送");
						}
					}
				}
				jq("#continueExpressTotalPrice").html(parseFloat(expressTotalPrice/100).toFixed(2));
				jq("#continueSendTicketTotalPriceInfo").css("display","block");
				jq("#continuePaymentView .ui-content").iscrollview("refresh");
			}else{
				jq("#continueSendTicketTotalPriceInfo").hide();
			}
		}
	}
})();