/**
 * @providesModule ZZTableViewIOS
 * Created by easy on 15/11/5.
 */

var React = require('react-native');
var ZZTableViewHeaderFooterViewIOS = require('./ZZTableViewHeaderFooterViewIOS');
var ZZTableViewCellIOS = require('./ZZTableViewCellIOS');
var ZZNativeTableView = require('NativeModules').ZZTableViewManager;
var UIManager = require('NativeModules').UIManager;
var ZZNativeTableViewIOSConsts = UIManager.ZZTableView.Constants;

var findNodeHandle = require('findNodeHandle');

var View = require('./../View');
var Interface = require('Interface');

var {
    requireNativeComponent
    } = React;



var ZZNativeTableViewIOS = requireNativeComponent('ZZTableView');




class ZZTableViewIOS extends View {

    constructor() {
        super();
        this.state.reuseHeaderFooterViews = {};
        this.state.reuseCells = {};

<<<<<<< HEAD:Client/MobileTicket/commons/components/TableView/ZZTableViewIOS.js
    }
    componentWillUpdate(nextProps, nextState) {
        this.state.delegate = nextProps.delegate;
        if (this.props && this.props.dataSource && nextProps && nextProps.dataSource) {
            if (this.props.dataSource != nextProps.dataSource) {
                this.reloadData(nextProps.dataSource).then(()=>{});
=======
        let checkAndCallDelegate = (func,... args) => {
            if (typeof func == 'function') {
                func(... args);
>>>>>>> 53e7e05571516cb5d343000bdf7fd1589036391b:Client/MobileTicket/components/TableView/ZZTableViewIOS.js
            }
        }

        this.state.dataSource = {};
        this.state.delegate = {
            onDidSelectedRow : (e) => {
                this._selectedIndexPath = e.nativeEvent.indexPath;
                checkAndCallDelegate(this.props.delegate.didSelectRow,e.nativeEvent.indexPath);
            }
        };
    }

    async deselectRow(indexPath,animated) {
        return Interface.asyncInvoke(ZZNativeTableView.deselectRow,indexPath,animated,findNodeHandle(this));
    }

    async beginUpdates() {
        return Interface.asyncInvoke(ZZNativeTableView.beginUpdates,findNodeHandle(this));
    }

    async endUpdates() {
        return Interface.asyncInvoke(ZZNativeTableView.endUpdates,findNodeHandle(this));
    }

    async reloadRows(indexPaths,rowAnimation) {
        return Interface.asyncInvoke(ZZNativeTableView.reloadRows,indexPaths,rowAnimation,findNodeHandle(this));
    }

    async reloadData() {
        return Interface.asyncInvoke(ZZNativeTableView.reloadData,this.dataSource,findNodeHandle(this));
    }


    componentWillMount() {
        console.log('table view componentWillMount');
        React.Children.forEach(this.props.children,(child)=> {
            if (ZZTableViewCellIOS.isPrototypeOf(child.type) || ZZTableViewCellIOS == child.type) {
                this.state.reuseCells[child.props.reuseIdentifier] = child;
            } else if (ZZTableViewHeaderFooterViewIOS.isPrototypeOf(child.type) || ZZTableViewHeaderFooterViewIOS == child.type) {
                this.state.reuseHeaderFooterViews[child.props.reuseIdentifier] = child;
            }
        });
    }

<<<<<<< HEAD:Client/MobileTicket/commons/components/TableView/ZZTableViewIOS.js
=======
    componentWillUnmount() {
        console.log('componentWillUnmount');
    }

    componentWillUpdate(nextProps,nextState) {
        console.log('componentWillUpdate');
    }
    refAtIndexPath(indexPath) {
        return 'cell_row_'+indexPath.row + '_section_'+indexPath.section;
    }
>>>>>>> 53e7e05571516cb5d343000bdf7fd1589036391b:Client/MobileTicket/components/TableView/ZZTableViewIOS.js

    render() {
        var onReuseCell = (e) => {
            let reuseIdentifier = e.nativeEvent['reuseIdentifier'];
            let reactTag = e.nativeEvent['reactTag'];
            let indexPath = {section:e.nativeEvent['indexPath']['section'],row:e.nativeEvent['indexPath']['row']};
            console.log(indexPath);
            let props = {
                reuseIdentifier:reuseIdentifier,
                indexPath:indexPath,
                rowData:e.nativeEvent['rowData'],
                sectionData:e.nativeEvent['sectionData'],
                ref:(cell)=>{
                    this.refs[this.refAtIndexPath(indexPath)] = cell;
                }
            };
            if (reactTag) {
<<<<<<< HEAD:Client/MobileTicket/commons/components/TableView/ZZTableViewIOS.js
                return;
            }
            let element = this.state.reuseCells[reuseIdentifier];
            if (element) {
                let view = React.cloneElement(element,props);   //can't update cell props,any create new ?
                this.props.children.push(view);//insert a cell view
                this.setState({});//notify tableview insert that cell view
=======
                //UIManager.updateView(reactTag,null,props);

            } else {
                let element = this.state.reuseCells[reuseIdentifier];
                if (element) {
                    let view = React.cloneElement(element,props);
                    this.props.children.push(view);
                    this.setState({});
                }
>>>>>>> 53e7e05571516cb5d343000bdf7fd1589036391b:Client/MobileTicket/components/TableView/ZZTableViewIOS.js
            }
        }

        var onReuseHeaderFooterView = (e) => {  // insert a header view
            let reuseIdentifier = e.nativeEvent['reuseIdentifier'];

            let element = this.state.reuseHeaderFooterViews[reuseIdentifier];

            if (element) {
                let view = React.cloneElement(element,{
                    reuseIdentifier:reuseIdentifier,
                    section:e.nativeEvent['section'],
                    sectionData:e.nativeEvent['sectionData'],
                    isHeader:e.nativeEvent['isHeader']
                });
                this.props.children.push(view);
                this.setState({});
            }
        }


<<<<<<< HEAD:Client/MobileTicket/commons/components/TableView/ZZTableViewIOS.js
        var didSelectRow = (e) => {
            if (this.props.delegate && this.props.delegate.didSelectRow) {
                this.props.delegate.didSelectRow(e.nativeEvent.indexPath);
            }
        }

        var cellForRow = this.props.dataSource && this.props.dataSource.cellForRow;
        if (cellForRow) {
            var cell = cellForRow();
        }
=======
>>>>>>> 53e7e05571516cb5d343000bdf7fd1589036391b:Client/MobileTicket/components/TableView/ZZTableViewIOS.js

        var element = (<ZZNativeTableViewIOS
            onReuseCell={onReuseCell}
            onReuseHeaderFooterView={onReuseHeaderFooterView}
<<<<<<< HEAD:Client/MobileTicket/commons/components/TableView/ZZTableViewIOS.js
            onDidSelectRowAtIndexPath={didSelectRow}

            {...this.props}
            />);

=======
            ref={'tableView'}
>>>>>>> 53e7e05571516cb5d343000bdf7fd1589036391b:Client/MobileTicket/components/TableView/ZZTableViewIOS.js

            {...this.props}
            {... this.state.delegate}
            />);

        return element;
    }
}


ZZTableViewIOS.Cell = ZZTableViewCellIOS;
ZZTableViewIOS.Header = ZZTableViewHeaderFooterViewIOS;
ZZTableViewIOS.Footer = ZZTableViewHeaderFooterViewIOS;

ZZTableViewIOS.Consts = ZZNativeTableViewIOSConsts;


exports = module.exports = ZZTableViewIOS;