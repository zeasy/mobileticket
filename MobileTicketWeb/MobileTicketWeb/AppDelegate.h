//
//  AppDelegate.h
//  MobileTicketWeb
//
//  Created by EASY on 16/1/12.
//  Copyright © 2016年 Z.EASY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

