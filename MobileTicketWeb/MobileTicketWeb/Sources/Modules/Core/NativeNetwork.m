//
//  NativeNetwork.m
//  MobileTicketWeb
//
//  Created by EASY on 16/1/13.
//  Copyright © 2016年 Z.EASY. All rights reserved.
//

#import "NativeNetwork.h"
#import <AFNetworking.h>
#import <RCTConvert.h>

@interface NativeNetwork ()

@property (nonatomic, strong) NSMutableDictionary *tasks;

@end

@implementation NativeNetwork

RCT_EXPORT_MODULE()

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.tasks = [NSMutableDictionary dictionary];
	}
	return self;
}


RCT_EXPORT_METHOD(request:(NSDictionary *) options uuid:(NSString *) uuid success:(RCTResponseSenderBlock) success fail:(RCTResponseErrorBlock) fail) {
	RCTLogInfo(@"%@",options);
	options = [RCTConvert NSDictionary:options];
	uuid = [RCTConvert NSString:options[@"uuid"]];
	NSString *url = [RCTConvert NSString:options[@"url"]];
	NSDictionary *params = [RCTConvert NSDictionary:options[@"params"]];
	NSString *method = [RCTConvert NSString:options[@"method"]];
	NSDictionary *headers = [RCTConvert NSDictionary:options[@"headers"]];
	NSArray *cookies = [RCTConvert NSArray:options[@"cookies"]];
	
	NSString *format = [RCTConvert NSString:options[@"format"]];
	
	
	
	NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
	if ([cookies count]) {
		conf = [NSURLSessionConfiguration ephemeralSessionConfiguration];
		for (int i = 0; i < [cookies count]; i++) {
			NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookies[i]];
			[conf.HTTPCookieStorage setCookie:cookie];
		}
	}
	
	AFHTTPSessionManager *manager =  [[AFHTTPSessionManager alloc] initWithSessionConfiguration:conf];
	manager.securityPolicy.allowInvalidCertificates = YES;
	manager.securityPolicy.validatesDomainName = NO;
	
	manager.requestSerializer = [AFHTTPRequestSerializer serializer];
	
	if ([@"JSON" compare:format options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		manager.responseSerializer = [AFJSONResponseSerializer serializer];
	} else if ([@"DATA" compare:format options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		manager.responseSerializer = [AFHTTPResponseSerializer serializer];
	} else {
		manager.responseSerializer = [AFHTTPResponseSerializer serializer];
	}

	// support timeout
	id timeout = options[@"timeout"];
	if (timeout) {
		manager.requestSerializer.timeoutInterval = [RCTConvert NSTimeInterval:timeout];
	}

	for (id key in [headers allKeys]) {
		id v = headers[key];
		[manager.requestSerializer setValue:v forHTTPHeaderField:key];
	}
	
	void (^s)(NSURLSessionDataTask * , id ) = ^(NSURLSessionDataTask * task, id object){
		if (uuid) {
			[self.tasks removeObjectForKey:uuid];
		}
		
		if ([@"DATA" compare:format options:NSCaseInsensitiveSearch] == NSOrderedSame) {
			object = [object base64EncodedStringWithOptions:0];
		} else if ([object isKindOfClass:[NSData class]]){
			object = [[NSString alloc] initWithData:object encoding:NSUTF8StringEncoding];
		}
		object = object ?: @{};
		if (success) {
			success(@[object]);
		}
	};
	void (^f)(NSURLSessionDataTask * , id ) = ^(NSURLSessionDataTask * task, NSError *err){
		if (uuid) {
			[self.tasks removeObjectForKey:uuid];
		}
		if (fail) {
			fail(err);
		}
	};
	
	NSURLSessionDataTask *task = nil;
	if ([@"POST" compare:method options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		task = [manager POST:url parameters:params progress:nil success:s failure:f];
	} else if ([@"PUT" compare:method options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		task = [manager PUT:url parameters:params success:s failure:f];
	} else if ([@"DELETE" compare:method options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		task = [manager DELETE:url parameters:params success:s failure:f];
	} else {
		task = [manager GET:url parameters:params progress:nil success:s failure:f];
	}
	
	if (uuid && task) {
		self.tasks[uuid] = task;
	}
}

RCT_EXPORT_METHOD(cancel:(NSString *) uuid  success:(RCTResponseSenderBlock) success fail:(RCTResponseErrorBlock) fail) {
	uuid = [RCTConvert NSString:uuid];
	if (uuid) {
		NSURLSessionDataTask *task = self.tasks[uuid];
		[task cancel];
	}
}
@end
