
/* JavaScript content from js/CheckCodePlugin.js in folder common */
var CheckCodePlugin = {
	getCheckCode: function(success, fail, timestamp){
		if (mor.ticket.util.isDebug) {
			var common = mor.ticket.common;
			var check_code=hex_md5('123456'+timestamp);
			
			success(check_code);
			return;
		}
		else {
			return cordova.exec(success, fail, "CheckCodePlugin", "getcheckcode", [timestamp]);
		}
	}
};
var CheckCodePlugin2 = {
	getCheckCode: function(success, fail, timestamp){
		if (mor.ticket.util.isDebug) {
			success(timestamp);
			return;
		}
		else {
			var jsonTime = WLJSX.Object.toJSON(timestamp);
			return cordova.exec(success, fail, "CheckCodePlugin", "getcheckcode", [jsonTime]);
		}
	}
};
var CheckCodePlugin3 = {
		decodeCheckCode: function(success, fail, timestamp){
			return cordova.exec(success, fail, "CheckCodePlugin", "decheckcode", [timestamp]);
		}
};
var CheckCodePlugin4 = {
		getCheckCode: function(success, fail, timestamp){
			if (mor.ticket.util.isDebug) {
				success(timestamp);
				return;
			}
			else {
				return cordova.exec(success, fail, "CheckCodePlugin", "getcheckcode", [timestamp]);
			}
		}
	};
var PushMsgPlugin = {
	encrypt: function(success, fail, timestamp){
		return cordova.exec(success, fail, "PushMsgPlugin", "encrypt", [timestamp]);
	},
    decrypt: function(success, fail, timestamp){
	    return cordova.exec(success, fail, "PushMsgPlugin", "decrypt", [timestamp]);
    }
};
window.CheckCodePlugin = CheckCodePlugin;
window.CheckCodePlugin2 = CheckCodePlugin2;
window.CheckCodePlugin3 = CheckCodePlugin3;
window.CheckCodePlugin4 = CheckCodePlugin4;
window.PushMsgPlugin = PushMsgPlugin;