
/* JavaScript content from js/controller/enterRegist.js in folder common */
(function() {
	jq("#enterRegistView").live("pageshow", function() {
		
	});
	jq("#enterRegistView").live("pageinit", function() {
		jq("#enterValidationCodeBtn").bind("tap",enterRegistSubmit);
		jq("#registValidationCodeBtn").bind("tap",getregistValidationCode);
		jq("#enterRegistBackBtn").bind("tap",function(){
			mor.ticket.currentPagePath.fromPath = "enterRegist.html";
			mor.ticket.util.changePage("regist.html");
			return false;
		});
	});
	
	jq("#enterRegistView").live("pagebeforeshow", function(e, data) {
		jq("#registValidationCode").val("");
	});
	function enterRegistSubmit(){
		var util = mor.ticket.util;
		if(util.isNoValue(jq("#registValidationCode").val())){
			util.alertMessage("请填写短信验证码");
			return;
		}
		var checkNum = /^[0-9]{6}$/;
		if(!checkNum.test(jq("#registValidationCode").val())){
			mor.ticket.util.alertMessage("手机校验码不正确");
			jq("#registValidationCode").val("");
			return;
		}
		var userInfo = mor.ticket.registUserInfo.userInfo;
		var commonParameters = {
			'baseDTO.user_name' : '',
			'user_type': userInfo.user_type,
			'user_name': replaceChar(userInfo.user_name),
			'name': replaceChar(userInfo.name) ,
			'id_type_code': userInfo.id_type_code ,
			'id_no': userInfo.id_no ,
			'password': hex_md5(userInfo.password) ,
			'pwd_question': '' ,
			'pwd_answer': '' ,
			'sex_code': userInfo.sex_code ,
			'born_date': util.processDateCode(userInfo.born_date),
			'country_code': userInfo.country_code,
			'mobile_no': userInfo.mobile_no ,
			'phone_no': '' ,
			'email': userInfo.email ,
			'address': '' ,
			'postalcode': '' ,
			'IVR_passwd': '' ,
			'province_code':  userInfo.province_code,
			'school_code': userInfo.school_code,
			'department': userInfo.department ,
			'school_class': userInfo.school_class ,
			'student_no': userInfo.student_no ,
			'enter_year': userInfo.enter_year,
			'school_system': userInfo.school_system,
			'preference_from_station_code':  userInfo.preference_from_station_code,
			'preference_to_station_code': userInfo.preference_to_station_code,
			'preference_card_no': userInfo.preference_card_no,
			'pass_code': jq("#registValidationCode").val()
		};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "registUser"
		};
		
		var options =  {
				onSuccess: requestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	// 替换特殊字符
	function replaceChar(str) {
		var v = str.replace(/['"<> ?]/g,"");
		return v;
	}
	
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			jq.mobile.changePage(vPathCallBack()+"loginTicket.html");
		} else {
			if(invocationResult.succ_flag == "0"){
				WL.SimpleDialog.show(
    				"温馨提示", 
    				invocationResult.error_msg, 
    				[ 
    				  {text : '返回修改', handler: function(){
    					  mor.ticket.currentPagePath.fromPath = "enterRegist.html";
    					  jq.mobile.changePage(vPathCallBack()+"regist.html");
    					  }},
    				  {text : '确定', handler: function(){}}
    				 ]
				);
			}else{
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}
			
		}
	}
	//手机核验
	function getregistValidationCode(){
		var commonParameters = {
				'user_name' : mor.ticket.registUserInfo.userInfo.user_name,
				'mobile_no': mor.ticket.registUserInfo.userInfo.mobile_no,
				'pass_code': ""
			};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "sendMobieCode4regist"
		};
		var options =  {
				onSuccess: validationCodeRequestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function validationCodeRequestSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;	
		if(mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			WL.SimpleDialog.show("温馨提示", "手机核验验证码已发送至您的手机,请查收。", [ {
				text : '确定',
				handler : function() {
					countDown(jq("#registValidationCodeBtn"),120);
				}
			} ]);
		} else {
			util.alertMessage(invocationResult.error_msg);
		}
	}
	 
	function countDown(obj,second){
		 if(second>=0){
	          if(typeof buttonDefaultValue === 'undefined' ){ 
	            buttonDefaultValue =  "获取验证码"; 
	        }
	        obj.addClass("ui-disabled");
	        var objValue = buttonDefaultValue+'('+second+')';
	        obj.html(objValue);
	        second--;
	        //clearTimeOutFun =  setTimeout(function(){countDown(obj,second);},1000); 
	        setTimeout(function(){countDown(obj,second);},1000); 
	    }else{
	        obj.removeClass("ui-disabled");
	        obj.html(buttonDefaultValue);
	    }    
	}
})();