
/* JavaScript content from js/controller/changePwd.js in folder common */
(function() {
	/*jq("#changePwdView").live("pagecreate", function() {
		mor.ticket.util.androidRemoveIscroll("#changePwdView");
	});*/
	var focusArray=[];
	jq("#changePwdView").live("pageshow", function() {
		focusArray=[];
	});
	function registerAutoScroll(){	
		var util = mor.ticket.util;
		util.enableAutoScroll('#oldPasswdInput',focusArray);
		util.enableAutoScroll('#newPasswdInput',focusArray);
		util.enableAutoScroll('#confirmPasswdInput',focusArray);
	}
	
	jq("#changePwdView").live("pageinit", function() {
		//mjl
		registerAutoScroll();
		
		jq("#changPwdBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("my12306.html");
			return false;
		});
		jq("#changePassValidationCode").change(function(){
			 var checkNum = /^[0-9]{6}$/;
			 if(!checkNum.test(jq("#changePassValidationCode").val())){
				 mor.ticket.util.alertMessage("手机校验码不正确");
				 jq("#changePassValidationCode").val("");
				 return;
			 }
		});
		jq("#changePassValidationCodeBtn").bind("tap", function() {
			var user = mor.ticket.loginUser;
			if(user.is_receive !== "Y"){
				WL.SimpleDialog.show("温馨提示","您的手机号还未核验，请核验手机号后修改密码。", [ {
					text : '确定',
					handler : function() {
						mor.ticket.util.changePage("userPhoneVerifyCheck.html");
					}
				} ]);
				return false;
			}
			var util = mor.ticket.util;
			if(util.isNoValue(jq("#oldPasswdInput").val())){
				util.alertMessage("请填写原密码");
				return;
			}
			if(util.isNoValue(jq("#newPasswdInput").val())){
				util.alertMessage("请填写新密码");
				return;
			}
			
			if(util.isNoValue(jq("#confirmPasswdInput").val())){
				util.alertMessage("请填写确认密码");
				return;
			}
			var newPasswd = jq("#newPasswdInput").val();
			if(newPasswd.length<6){
				util.alertMessage("新密码的长度必须大于6位");
				return;
			}
			if(jq("#confirmPasswdInput").val()!=jq("#newPasswdInput").val()){
				util.alertMessage("确认密码不等于新密码");
				return;
			}
			var commonParameters = {
					'email' : '',
					'busCode': "modifyPwd"
				};
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "getModifyUserMobieCode"
			};
			
			var options =  {
					onSuccess: changePassValidationCodeRequestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
		});
		
		
		
		jq("#changPwdBtn").bind("tap", function() {
			var util = mor.ticket.util;
			if(util.isNoValue(jq("#oldPasswdInput").val())){
				util.alertMessage("请填写原密码");
				return;
			}
			if(util.isNoValue(jq("#newPasswdInput").val())){
				util.alertMessage("请填写新密码");
				return;
			}
			
			if(util.isNoValue(jq("#confirmPasswdInput").val())){
				util.alertMessage("请填写确认密码");
				return;
			}
			var newPasswd = jq("#newPasswdInput").val();
			if(newPasswd.length<6){
				util.alertMessage("新密码的长度必须大于6位");
				return;
			}
			if(jq("#confirmPasswdInput").val()!=jq("#newPasswdInput").val()){
				util.alertMessage("确认密码不等于新密码");
				return;
			}
			 var checkNum = /^[0-9]{6}$/;
			 if(!checkNum.test(jq("#changePassValidationCode").val())){
				 mor.ticket.util.alertMessage("手机校验码不正确");
				 jq("#changePassValidationCode").val("");
				 return;
			 }
			var commonParameters = {
				'oldPassWd': hex_md5(jq("#oldPasswdInput").val()),
				'newPassWd': hex_md5(jq("#newPasswdInput").val()),
				'check_code': jq("#changePassValidationCode").val()
			};
			
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "changePass"
			};
			
			var options =  {
					onSuccess: requestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
		});
	});
	jq("#changePwdView").live("pagebeforeshow", function() {
		mor.ticket.util.initAppVersionInfo();
		jq("#oldPasswdInput").val("");
		jq("#newPasswdInput").val("");
		jq("#confirmPasswdInput").val("");
		var user = mor.ticket.loginUser;
		if (user.isAuthenticated === "Y") {
			mor.ticket.viewControl.tab3_cur_page="changePwd.html";
		} else {
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
				} else {
				registerAutoLoginHandler(function(){mor.ticket.viewControl.tab3_cur_page="changePwd.html";}, autologinFailJump);
			}
		}
		jq("#changePassValidationCode").val("");
		if(jq("#changePassValidationCodeBtn").hasClass("ui-disabled")){
			jq("#changePassValidationCodeBtn").removeClass("ui-disabled");
			jq("#changePassValidationCodeBtn").html("获取验证码");
		}
	});
	function changePassValidationCodeRequestSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;	
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			WL.SimpleDialog.show("温馨提示", "修改密码验证码已发送至您的手机,请查收。", [ {
				text : '确定',
				handler : function() {
					changePassCountDown(jq("#changePassValidationCodeBtn"),120);
				}
			} ]);
		} else {
			util.alertMessage(invocationResult.error_msg);
		}
	}
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		var util = mor.ticket.util;		
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			util.alertMessage(invocationResult.error_msg);
			mor.ticket.loginUser["isAuthenticated"]="N";
			var user =  mor.ticket.loginUser;
			user.password = "";
			clearUserCache();
			jq.mobile.changePage(vPathCallBack()+"loginTicket.html");
		} else {
			util.alertMessage(invocationResult.error_msg);
		}
	}

	function clearUserCache() {
		WL.EncryptedCache.open("wlkey",true,onOpenComplete,onOpenError);
	}

	function onOpenComplete(){ 
		WL.EncryptedCache.write("userPW", "*", 
		function(){
			WL.EncryptedCache.close(function(){
				//WL.Logger.debug("Encrypted Cache Closed.");
			}, 
		function(){/*WL.Logger.debug("Failed to close Encrypted Cache.");*/});},
		function(){/*WL.Logger.debug("Failed to write to ticketStorage");*/});
	}

	function onOpenError(status){
		//WL.Logger.debug("Cannot open encrypted cache");
	}
	function changePassCountDown(obj,second){
		 if(second>=0){
	          if(typeof buttonDefaultValue === 'undefined' ){ 
	            buttonDefaultValue =  "获取验证码"; 
	        }
	        obj.addClass("ui-disabled");
	        var objValue = buttonDefaultValue+'('+second+')';
	        obj.html(objValue);
	        second--;
	        //clearTimeOutFun =  setTimeout(function(){countDown(obj,second);},1000); 
	        setTimeout(function(){changePassCountDown(obj,second);},1000); 
	    }else{
	        obj.removeClass("ui-disabled");
	        obj.html(buttonDefaultValue);
	    }    
	}
})();