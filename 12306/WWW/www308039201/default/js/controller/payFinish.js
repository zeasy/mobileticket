
/* JavaScript content from js/controller/payFinish.js in folder common */
(function(){
	var jqName ="";
	function payFinishFn(){
		var payFinishBlock1 = window.ticketStorage.getItem("payFinishBlock1");
		var payFinishBlock2 = window.ticketStorage.getItem("payFinishBlock2");
		var payFinishBlock3 = window.ticketStorage.getItem("payFinishBlock3");
		var payFinishBlock4 = window.ticketStorage.getItem("payFinishBlock4");
		//payFinishBlock1 ="taxiServiceFlagCss";
		//payFinishBlock2 ="hotelServiceFlagCss";
//		payFinishBlock3 ="taxiServiceFlagCss";
//		payFinishBlock4 ="hotelServiceFlagCss";
		mor.ticket.viewControl.tab2_cur_page="queryOrder.html";
		var payMode = mor.ticket.viewControl.isPayfinishMode;
		if(payFinishBlock1 == "taxiServiceFlagCss"){
			jqName = "taxiService";
		    payFinishBlockInnerHtml(jqName,payFinishBlock1);
		    jq("#taxiService").removeClass("saveToLocaleCss").addClass(payFinishBlock1);
	    }else{
	    	jq("#taxiService").addClass("saveToLocaleCss");
		    jq("#taxiService").html("保存<br/>到本地");
	   }
		if(payFinishBlock2 == "hotelServiceFlagCss"){
			jqName = "hotelService";
			payFinishBlockInnerHtml(jqName,payFinishBlock2);
			jq("#hotelService").removeClass("nearAgencySellTicketCss").addClass(payFinishBlock2);	
		}else{
			jq("#hotelService").addClass("nearAgencySellTicketCss");
			jq("#hotelService").html("周边<br/>代售点");
		}
		if(payFinishBlock3 != "" && payFinishBlock3 != undefined && payFinishBlock3 != null){
			jqName = "shareToFriend";
			payFinishBlockInnerHtml(jqName,payFinishBlock3);
			jq("#shareToFriend").removeClass("shareToFriendCss").addClass(payFinishBlock3);	
		}else{
			jq("#shareToFriend").html("分享<br/>给朋友");
		}
		if(payFinishBlock4 != "" && payFinishBlock4 != undefined && payFinishBlock4 != null){
			jqName = "travel";
			payFinishBlockInnerHtml(jqName,payFinishBlock4);
			jq("#travel").removeClass("travelCss").addClass(payFinishBlock4);	
		}else{
			jq("#travel").html("旅行<br/>休闲");
		}
		jq("#payFinishNavBar .ui-grid-d .ui-block-b," +
		"#payFinishNavBar .ui-grid-d .ui-block-c,#payFinishNavBar .ui-grid-d .ui-block-d,#payFinishNavBar .ui-grid-d .ui-block-e").css("width","25%");
		if(payMode){
			var orderManager=mor.ticket.orderManager;
			var myTicketList = orderManager.paymentOrder.myTicketList;
			var sequence_no = myTicketList[0].sequence_no;
			jq("#sequence_no").html(sequence_no);
			var msg = gettiketmessage(myTicketList);
			jq("#customer_name").html(msg);
			jq("#trainDetailsGridAll").html(payFinishgenerateTicketDetailsGrid(myTicketList));
			var priceSum = 0;
			var passengerTrainList = mor.ticket.passengerTrainlList.passengerTrainList;
			for(var i=0;i<myTicketList.length; i++){
				priceSum += parseFloat(myTicketList[i].ticket_price);
				var passenger_id_no = myTicketList[i].passenger_id_no;
				var birthDay = passenger_id_no.substring(6, 10);
				var sex_code = passenger_id_no.substr(passenger_id_no.length - 2,1);
				var sex_name = "未知";
				if(passenger_id_no.length == 18){
					if(parseInt(sex_code) % 2 == 0){
						sex_name = "女";
					}else{
						sex_name = "男";
					}
				}else{
					sex_name = "未知";
				}
				passengerTrainList[i] = {
						trainDate : mor.ticket.util.changeDateType(myTicketList[i].train_date),
						trainHeader : myTicketList[i].station_train_code.substring(0,1),
						seatTypeName : mor.ticket.cache.getSeatTypeByCode(myTicketList[i].seat_type_code),
						fromStationName : mor.ticket.cache.getStationNameByCode(myTicketList[i].from_station_telecode),
						startTime : mor.ticket.util.formateTrainTime(myTicketList[i].start_time),
						toStationName : mor.ticket.cache.getStationNameByCode(myTicketList[i].to_station_telecode),
						arriveTime : mor.ticket.util.formateTrainTime(myTicketList[i].arrive_time),
						sex : sex_name,
						birthYear : birthDay
				};
			}
			var isSendAdvMessage = window.localStorage.getItem("isSendAdvMessage").toString();
			if("Y" == isSendAdvMessage){
				window.plugins.childBrowser.PrecisionMarketing(JSON.stringify(passengerTrainList), {});
			}
			jq("#payfinishTotalPrice").html(priceSum.toFixed(2) + "元");
			var warmStr = "如您拟于乘车前到车站售票窗口换票，请预留足够的换票时间，以免窗口排队人数较多，耽误您检票乘车。";
			jq("#payFinishtView .tips2 p").html(warmStr);
			jq(".tipWarm").show();
			var ticket_status_code = myTicketList[0].ticket_status_code;
			if(ticket_status_code == "f" || ticket_status_code == "r"){
//				jq("#insuranceBlock").hide();
				jq("#payFinishNavBar .ui-grid-d .ui-block-b," +
    			"#payFinishNavBar .ui-grid-d .ui-block-c,#payFinishNavBar .ui-grid-d .ui-block-d,#payFinishNavBar .ui-grid-d .ui-block-e").css("width","25%");
			}
			registerQueryOrder();
			 //add by yiguo
            mor.ticket.queryOrder.currentQueryOrder = null;
            mor.ticket.queryOrder.queryOrderList = null;
		}else{
			jq("#payFinishtView .ui-header h1").html("退票完成");
			jq(".tipWarm").hide();
			jq("#cancelFinishTips2").show();
			var queryOrder = mor.ticket.queryOrder;
			var ticket = queryOrder.getCancelTicketInfo();
			jq("#payFinish").hide();
//			jq("#insuranceBlock").hide();
			jq("#payFinishNavBar .ui-grid-d .ui-block-b," +
			"#payFinishNavBar .ui-grid-d .ui-block-c,#payFinishNavBar .ui-grid-d .ui-block-d,#payFinishNavBar .ui-grid-d .ui-block-e").css("width","25%");
			jq("#cancelFinish").show();
			jq("#payOrderID").html(ticket.payOrderID);
		    if(ticket.ticket_price ==""){
				jq("#firstTicketReturnPrice").hide();
				jq("#secondTicketReturnPrice").hide();
		    }else{
		    	jq("#firstTicketReturnPrice").show();
				jq("#secondTicketReturnPrice").show();
				jq("#oriTicketPrice").html(ticket.ticket_price);
				jq("#oriTicketReturnCostPrice").html(ticket.return_cost);
//				jq("#sendTicketPrice2").html(ticket.ticket_price);
				
				if(ticket.rate=="-1"){
					jq("#ticketPriceRateId").hide();
				}else{
					jq("#ticketPriceRate").html(ticket.rate);
				}
//				jq("#ticketReturnCost").html(ticket.return_cost);
				jq("#shouldTicketPrice").html((ticket.ticket_price - ticket.return_cost).toFixed(2));
		    }
			var warmStr = 
					"1、应退票款按银行规定时限退还至购票时所使用的银行卡，请注意查收。<br/>" +
					"2、如果需要退票报销凭证，请凭购票所使用的乘车人有效身份证件原件和订单号码在办理退票之日起10日内到车站退票窗口索取。<br/>" +
					"3、退票成功后，将向您注册时提供的有效邮箱和手机发送退票信息，请稍后查询。<br/>" +
					"4、距开车前48小时-15天期间内的车票，改签/变更到站至距开车15天以上的其他列车，又在距开车前15天前退票的，仍核收5%的退票费。<br/>" +
					"5、退票费如下核收：票面乘车站开车时间前48小时以上的按票价5%计，24小时以上、不足48小时的按票价10%计，不足24小时的按票价20%计。上述计算的尾数以5角为单位，尾数小于2.5角的舍去、2.5角以上且小于7.5角的计为5角、7.5角以上的进为1元；最低按2元计收。";
			jq("#cancelFinishTips2").html(warmStr);
			jq("#payFinishTips").hide();
			jq('#queryOrder').attr('href', 'finishedOrderDetail.html');
			jq('#queryOrder').on('tap', function(e) {
				// 如果用户点击. 重新更新原有订单信息.
				e.stopImmediatePropagation();
				e.preventDefault();

				var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "queryOrder"
				};
				var onSuccess = function(result) {
					// 更新原有的订单信息.
					var orderList = result.invocationResult.orderList;
					
					if (orderList && orderList.length > 0) {
						 
						
						var order = orderList[0];
						mor.ticket.queryOrder.replaceOrderBySequenceNo(ticket.sequence_no, order);
						setTimeout(function() {
							jq.mobile.changePage(getViewBasePath('finishedOrderDetail.html'));
						}, 1);
					} else {
						onFailure();
					}
				};
				var onFailure = function() {
					// 如果获取数据出错， 返回到订单查询页面.
					jq.mobile.changePage(getViewBasePath('queryOrder.html'));
				};
				var options =  {
					onSuccess: onSuccess,
				    onFailure: onFailure
				};
				mor.ticket.util.invokeWLProcedure({'sequence_no': ticket.sequence_no},invocationData, options);
			});
			mor.ticket.viewControl.isPayfinishMode = true;
		}
		contentIscrollRefresh();
	}
	
	function registerQueryOrder(){
		jq("#queryOrder").bind("tap",function(e){
			e.stopImmediatePropagation();
			e.preventDefault();
			mor.ticket.viewControl.queryFinishedOrderType = '0';
            mor.ticket.util.changePage(vPathCallBack() + "finishedOrderList.html");
		});
	}
   jq("#payFinishtView").live("pagebeforeshow", function(){
	   var user = mor.ticket.loginUser;
	   mor.ticket.passengerTrainlList.passengerTrainList = [];
		if (user.isAuthenticated === "Y") {
			payFinishFn();
		} else {
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump()
			} else {
				registerAutoLoginHandler(payFinishFn, autologinFailJump);
			}

			// mor.ticket.util.changePage(vPathCallBack()+"loginTicket.html");
		}
	});
   jq("#payFinishtView").live("pageinit", function() {
//	   AddtravelPlanbtnLisener();
	   registerTpcBtnLisener();
	   registerShareBtnClickHandler();
//	   registerScreenShotBtnClickHandler();
	   registerTaxiServiceClickHandle();
	   registerHotelServiceClickHandle();
	});
    

//	function AddtravelPlanbtnLisener() {
//		jq("#addTravelPlanBtn").bind(
//				"tap",
//				function(e) {
//					var name = "";
//					var idType = "";
//					var ticket_type_code = "";
//					e.stopImmediatePropagation();
//					var  travelPlanList = JSON.parse(window.ticketStorage.getItem("travelPlanList"));
//					var orderManager=mor.ticket.orderManager;
//					var myTicketList = orderManager.paymentOrder.myTicketList;
//					for(var i = 0;i<myTicketList.length;i++){
//						 name = name +myTicketList[i].passenger_name +",";
//						 idType = idType + myTicketList[i].passenger_id_type_code+",";
//						 ticket_type_code = ticket_type_code + myTicketList[i].ticket_type_code+",";
//					}
//					myTicketList[0].passenger_name = name;
//					myTicketList[0].passenger_id_type_code = idType;
//					myTicketList[0].ticket_type_code = ticket_type_code;
//					myTicketList[0].checked = 0;
//					if(travelPlanList == null){
//						for(var i = 1; i< myTicketList.length; i++){
//							myTicketList.splice(i);
//						}
//						window.ticketStorage.setItem("travelPlanList",JSON.stringify(myTicketList));	
//					}else{
//								if(myTicketList[0] !=  undefined){
//									travelPlanList.push(myTicketList[0]);
//								}								
//						travelPlanList =uniQueue(travelPlanList);
//						window.ticketStorage.setItem("travelPlanList",JSON.stringify(travelPlanList));
//					}		
//					mor.ticket.util.changePage("travelPlan.html");
//					return false;
//				});
//		
//		
//	}
	
	/*function uniQueue(array){ 
		var arr=[]; 
		var m; 
		while(array.length>0){ 
		m=array[0]; 
		arr.push(m); 
		array=jq.grep(array,function(item,index){
		return item.station_train_code==m.station_train_code 
		&& item.passenger_name == m.passenger_name
		&& item.passenger_id_type_code == m.passenger_id_type_code; 
		},true); 
		} 
		return arr; 
	}*/ 
   function gettiketmessage(myTicketList){
	  var passenger1=[],passenger2=[],passenger3=[],passenger4=[],passenger5=[],passenger6=[];
	  for(var i=0;i<myTicketList.length;i++){
		  var  ticket = myTicketList[i];		  
		// 学生票，提示换票
		  if(ticket.ticket_type_code=='3'){
				  passenger1.push(ticket.passenger_name);
		  }//残军票，提示换票
		  else if(ticket.ticket_type_code=='4'){
			  passenger2.push(ticket.passenger_name);
		  }//电子票
		  else if(ticket.eticket_flag=='Y'){
			  passenger3.push(ticket.passenger_name);
//			  WL.Logger.debug("###################################ticket.eticket_flag###:"+ticket.eticket_flag);
		  }//二代证，不是电子票， 是儿童票的
		  else if(ticket.eticket_flag=='N'&&ticket.passenger_id_type_code=='1'&&ticket.ticket_type_code=='2'){
			  passenger4.push(ticket.passenger_name);
		  }//二代证，不是电子票， 不是儿童票的
		  else if(ticket.eticket_flag=='N'&&ticket.passenger_id_type_code=='1'&&ticket.ticket_type_code=='1'){
			  passenger5.push(ticket.passenger_name);
		  }else if(ticket.eticket_flag=='N'&&ticket.passenger_id_type_code!='1'&&ticket.ticket_type_code!='3'&&ticket.ticket_type_code!='4') {
			  passenger6.push(ticket.passenger_name);
		  }
	  }
	  var msg = "<p style='text-indent:2em;'>本应用将在“订单查询”中显示订单信息，并向您注册时提供的邮箱和手机发送订单信息，请稍后查询。</p>";
//	  WL.Logger.debug("###################################踩踩踩踩踩踩踩踩踩###:"+msg);
	  var msg_one="";
	for(var i=0;i<passenger1.length;i++){
		if(msg_one!=""){
			msg_one += "、"+passenger1[i];
		}else{
			msg_one +=passenger1[i];
		}
	}
	if(msg_one!=""){
		msg_one="<p style='text-indent:2em;'>"+msg_one +"&nbsp;&nbsp;女士/先生请持购票时所使用的有效身份证件、附有“学生火车票优惠卡”的学生证（均为原件）和订单号码尽快到安装有学生火车票优惠卡识别器的车站售票窗口、铁路客票代售点或具有办理学生票功能的自动售取票机换取纸质车票后乘车，换票前请确保学生优惠卡内的优惠区间与网站填写的信息一致。</p>";
	}
	
	
	var msg_two="";
	for(var i=0;i<passenger2.length;i++){
		if(msg_two!=""){
			msg_two += "、"+passenger2[i];
		}else{
			msg_two +=passenger2[i];
		}
	}
	if(msg_two!=""){
		msg_two="<p style='text-indent:2em;'>"+ msg_two + "&nbsp;&nbsp;女士/先生请持购票时所使用的有效身份证件、“中华人民共和国残疾军人证”、“中华人民共和国伤残人民警察证”（均为原件）和订单号码尽快到车站售票窗口换取纸质车票。</p>";
	}
	
	var msg_three="";
	for(var i=0;i<passenger3.length;i++){
		if(msg_three!=""){
			msg_three += "、"+passenger3[i];
		}else{
			msg_three +=passenger3[i];
		}
	}
	if(msg_three!=""){
		msg_three = "<p style='text-indent:2em;'>"+msg_three+ "&nbsp;&nbsp;女士/先生可持购票时所使用的二代居民身份证原件于购票后、列车开车前到车站直接检票乘车。</p>";
	}
	
	var msg_four="";
	for(var i=0;i<passenger4.length;i++){
		if(msg_four!=""){
			msg_four += "、"+passenger4[i];
		}else{
			msg_four +=passenger4[i];
		}
	}
	if(msg_four!=""){
		msg_four = "<p style='text-indent:2em;'>"+msg_four+  "&nbsp;&nbsp;女士/先生请持二代居民身份证原件和订单号尽快到铁路代售点、车站自动售票机或车站售票窗口换取儿童纸质车票。</p>";
	}
	
	
	var msg_five="";
	for(var i=0;i<passenger5.length;i++){
		if(msg_five!=""){
			msg_five += "、"+passenger5[i];
		}else{
			msg_five +=passenger5[i];
		}
	}
	if(msg_five!=""){
		msg_five = "<p style='text-indent:2em;'>"+msg_five+"&nbsp;&nbsp;女士/先生请持二代居民身份证原件和订单号尽快到铁路代售点、车站自动售票机或车站售票窗口换取纸质车票。</p>";
	}
	
	var msg_six="";
	for(var i=0;i<passenger6.length;i++){
		if(msg_six!=""){
			msg_six += "、"+passenger6[i];
		}else{
			msg_six +=passenger6[i];
		}
	}
	if(msg_six!=""){
		msg_six ="<p style='text-indent:2em;'>"+msg_six+ "&nbsp;&nbsp;女士/先生请持购票时所使用的有效身份证件原件和订单号码尽快到车站售票窗口换取纸质车票。</p>";
	}
	msg = msg + msg_one + msg_two + msg_three + msg_four + msg_five + msg_six ;
	return msg;
   }
   function contentIscrollRefresh(){
		if(jq("#payFinishtView .ui-content").attr("data-iscroll")!=undefined){
			jq("#payFinishtView .ui-content").iscrollview("refresh");
		}
	}
	
	function registerTaxiServiceClickHandle(){
		jq("#taxiService").off().on("tap", function(event) {
			event.preventDefault();
			var payFinishBlock1 = window.ticketStorage.getItem("payFinishBlock1");
//			payFinishBlock1 ="taxiServiceFlagCss";
			if(payFinishBlock1 =="taxiServiceFlagCss"){
				taxiServiceClick();
			}else if(payFinishBlock1 == "hotelServiceFlagCss"){
				if(mor.ticket.util.isAndroid()){
					window.plugins.childBrowser.showAdsView("http://m.ctrip.com/webapp/hotel/?allianceid=110185&sid=554014", "携程特惠酒店", {}); 
				}else{
					window.plugins.childBrowser.showAdsView("","http://m.ctrip.com/webapp/hotel/?allianceid=110185&sid=554014",{}); 
				}				
			}else{
				saveImgclick();
			}
			
		});
		}
	  function registerHotelServiceClickHandle(){
		   jq("#hotelService").off().on("tap", function(event) {
				event.preventDefault();
				var payFinishBlock2 = window.ticketStorage.getItem("payFinishBlock2");
//				payFinishBlock2 ="hotelServiceFlagCss";
				if(payFinishBlock2 == "taxiServiceFlagCss"){
					taxiServiceClick();
				}else if(payFinishBlock2 =="hotelServiceFlagCss"){
					if(mor.ticket.util.isAndroid()){
						window.plugins.childBrowser.showTpcWebView("http://m.ctrip.com/webapp/mkt/partner-360?allianceid=110185&sid=554014", "携程特惠酒店", {}); 	
					}else{
						window.plugins.childBrowser.showAdsView("","http://m.ctrip.com/webapp/mkt/partner-360?allianceid=110185&sid=554014",{}); 
					}
					
				}else{
					window.plugins.childBrowser.showTpcWebView("http://map.wap.qq.com/x/index.jsp?sid=AT364KWTF7FdqdK_thITOuyh&welcomeChange=1&poi_table=ticket_outlets&needlocation=1&radius=1000&g_ut=3&referer=12306wxmp_lbscloud_nearby&type=lbscloudnearby&lbskey=JK7BZ-HI4HV-LBNPS-UF4UL-VGU47-KMBSK", "周边代售点", {}); 
				}
				
		   });	
	   }
	  function registerShareBtnClickHandler() {
			jq("#shareToFriend").off().on("tap", function(event) {			
				event.preventDefault();
				var payFinishBlock3 = window.ticketStorage.getItem("payFinishBlock3");
				//payFinishBlock3 ="taxiServiceFlagCss";
				if(payFinishBlock3 =="taxiServiceFlagCss"){
					taxiServiceClick();
				}else if(payFinishBlock3 == "hotelServiceFlagCss"){
					window.plugins.childBrowser.showAdsView("","http://m.ctrip.com/webapp/hotel/?allianceid=110185&sid=554014",{}); 
					//window.plugins.childBrowser.showTpcWebView("http://m.ctrip.com/webapp/mkt/partner-360?allianceid=110185&sid=554014", "携程特惠酒店", {}); 
				}else{
					shareClick();
				}		
			});
		}
		function registerTpcBtnLisener(){
			jq("#travel").off().bind("tap", function(event) {
				event.preventDefault();
				var payFinishBlock4 = window.ticketStorage.getItem("payFinishBlock4");
//				payFinishBlock4 ="hotelServiceFlagCss";
				if(payFinishBlock4 =="taxiServiceFlagCss"){
					taxiServiceClick();
				}else if(payFinishBlock4 == "hotelServiceFlagCss"){
					window.plugins.childBrowser.showAdsView("","http://m.ctrip.com/webapp/hotel/?allianceid=110185&sid=554014",{}); 
					//window.plugins.childBrowser.showTpcWebView("http://m.ctrip.com/webapp/mkt/partner-360?allianceid=110185&sid=554014", "携程特惠酒店", {}); 
				}else{
					showAdsView();
				}
		  });	
		}
		function showAdsView(){
			var passengerTrainList = mor.ticket.passengerTrainlList.passengerTrainList;
			window.plugins.childBrowser.showAdsView(JSON.stringify(passengerTrainList), '',{}); 
		}
	  function shareClick(){
		  showImg();
			var cloneContent = jq("#payFinishtView").clone().insertAfter("#payFinishtView");
			cloneContent.attr("style","padding-top: 0px !important ; padding-bottom: 0px !important ;");
			cloneContent.children("div[data-role='content']").first().replaceWith(jq("#forCopyPayFinishtView").clone().attr("style","min-height : "+jq("div[data-role='content']").height()+"px ;"));		
			cloneContent.attr("id","iscopyPayFinishtView");
			cloneContent.children().removeClass("ui-header-fixed");
			cloneContent.children().removeClass("ui-footer-fixed");
			html2canvas(document.getElementById("iscopyPayFinishtView"), {
				allowTaint : true,
				taintTest : false,
				chinese: true,
				onrendered : function(canvas) {
					// 生成base64图片数据
					var dataUrl = canvas.toDataURL();
//					cordova.exec(callShareSuccess, callFail,
//							"SaveImgPlugin", "saveImg", [ dataUrl,jq("#sequence_no").html() ]);
					cordova.exec(callShareSuccess, callFail,
							"SocialSharing", "share", [ dataUrl,"铁路12306订票成功_"+jq("#sequence_no").html() ]);
					cloneContent.remove();
					//busy.hide();
					//hideLoader();
					hideImg();
				}
			});
	  }
	function taxiServiceClick(){
		var myTicketList = mor.ticket.orderManager.paymentOrder.myTicketList;
		var phone = window.btoa(mor.ticket.loginUser.mobile_no);
		if(myTicketList.length>0 && myTicketList!=undefined){
		var start_pos = mor.ticket.cache.getStationNameByCode(myTicketList[0].from_station_telecode);
		var end_pos = mor.ticket.cache.getStationNameByCode(myTicketList[0].to_station_telecode);
		var start_time = myTicketList[0].train_date.substring(0,4)+"-"
		+myTicketList[0].train_date.substring(4,6)+"-"
		+myTicketList[0].train_date.substring(6,8)+" "
		+myTicketList[0].start_time.substring(0,2)+":"
		+myTicketList[0].start_time.substring(2,4)+":00";
		var end_time = myTicketList[0].arrive_time.substring(0,2)+":"+myTicketList[0].arrive_time.substring(2,4)+":00";
		var seat_rank = mor.ticket.cache.getSeatTypeByCode(myTicketList[0].seat_type_code);

	//	if(mor.ticket.util.isAndroid()){
	        window.plugins.childBrowser.showTpcWebView(encodeURI("http://huodong.yongche.com/app/12306.html"), "接送站服务", {}); 	
	//	}else{
	//		window.plugins.childBrowser.showAdsView("",encodeURI("http://huodong.yongche.com/app/12306.html"),{});
	//	}
		}else{
		//if(mor.ticket.util.isAndroid()){	
		    window.plugins.childBrowser.showTpcWebView(encodeURI("http://huodong.yongche.com/app/12306.html"), "接送站服务", {});
		//	}else{
		 //   window.plugins.childBrowser.showAdsView("",encodeURI("http://huodong.yongche.com/app/12306.html"),{});	
		//	}
	}	
	}
	function saveImgclick(){
		showImg();			
		var cloneContent = jq("#payFinishtView").clone().insertAfter("#payFinishtView");
		cloneContent.attr("style","padding-top: 0px !important ; padding-bottom: 0px !important ;");
		cloneContent.children("div[data-role='content']").first().replaceWith(jq("#forCopyPayFinishtView").clone().attr("style","min-height : "+jq("div[data-role='content']").height()+"px ;"));/*newStyle.css 列车查询结果*/
		cloneContent.attr("id","iscopyPayFinishtView");
		cloneContent.children().removeClass("ui-header-fixed");
		cloneContent.children().removeClass("ui-footer-fixed");
		
		html2canvas(document.getElementById("iscopyPayFinishtView"), {
			allowTaint : true,
			taintTest : false,
			chinese: true,
			onrendered : function(canvas) {
				// canvas.id = "mycanvas";
				// document.body.appendChild(canvas);
				// 生成base64图片数据
				var dataUrl = canvas.toDataURL();
				cordova.exec(callScreenShotSuccess, callFail,
						"SaveImgPlugin", "saveImg", [ dataUrl,"铁路12306订票成功_"+jq("#sequence_no").html() ]);
				cloneContent.remove();
				//busy.hide();
				//hideLoader();
				hideImg();
			}
		});
	}
	function showImg(){
		jq(document).progressDialog.showDialog("请稍候...");
	}
	function hideImg(){
		jq(document).progressDialog.hideDialog();
	}
	function showLoader(){
		jq.mobile.loading('show',{
			text: '截图中...',
			textVisible: true,
			theme: 'd',
			textonly: false,
			html: ""
		});
	}
	
	function hideLoader(){
		jq.mobile.loading('hide');
	}
	
	function callShareSuccess(data) {
		// alert(data);
		/*WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);*/
		openNativePage(data,"Share");
	}
	
	function callScreenShotSuccess(data) {
		// alert(data);
		if (data == "IOS"){
			WL.SimpleDialog.show("温馨提示", "已成功保存至相册", [ {
				text : '确定',
				handler : function() {
				}
			} ]);	
		}else{
			openNativePage(data,"ScreenShot");
		}
	
	}
	
	function callFail(data) {
		// alert(data);
		WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);
	}

	function openNativePage(data,whatBtn) {
		var params = {
				imgPath : data
		};
		if(whatBtn === "Share"){
		WL.NativePage.show('com.MobileTicket.MobileTicketShare',
				backFromNativePage, params);
		}
		else if(whatBtn === "ScreenShot"){
			WL.NativePage.show('com.MobileTicket.ScreenShotSaveImg',
					backFromNativePage, params);
			}
		}


	function backFromNativePage(data) {
		WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);
	}
  
	function payFinishBlockInnerHtml(jqName,block){
		if(block == "hotelServiceFlagCss"){
			jq("#"+jqName).html("特惠<br/>酒店");
			jq("#hotelServiceFreeFlag").addClass("hotelServiceFreeFlag");
		}else if(block == "taxiServiceFlagCss"){
//			var taxiServiceCoupon = window.ticketStorage.getItem("taxiServiceCoupon");
//			if("Y" == taxiServiceCoupon){
				jq("#taxiServiceCouponFlag").addClass("taxiServiceCouponFlag");
//			}else{
//				jq("#taxiServiceCouponFlag").removeClass("taxiServiceCouponFlag");
//			}
			jq("#"+jqName).html("接送站<br/>服务");
			
		}
	}
   
   var payFinishTicketDetailsGridTemplate =
	   "{{~it :orderDetail:index}}" +
		"<li data-index='{{=index}}' style='padding:5px 0px 5px 5px;'>" +
			"<div class='ui-grid-a' >" +
				"<div class='ui-block-a' style='width: 30%;'>{{=mor.ticket.util.changeDateType(orderDetail.train_date)}}</div>" +
				"<div class='ui-block-b' style='width: 70%; overflow: hidden;white-space: nowrap;text-overflow: ellipsis;'>" +
					"<div class='ui-grid-b'>" +
						"<div class='ui-block-a' style='width: 46%;'>{{=mor.ticket.cache.getStationNameByCode(orderDetail.from_station_telecode)}}" +
								"{{=mor.ticket.util.formateTrainTime(orderDetail.start_time)}}</div>" +
						"<div class='ui-block-b trainDetailArrow' style='width: 8%;'></div>" +
						"<div class='ui-block-c' style='width: 46%;'>{{=mor.ticket.cache.getStationNameByCode(orderDetail.to_station_telecode)}}" +
								"{{=mor.ticket.util.formateTrainTime(orderDetail.arrive_time)}}</div>" +
					"</div>" +
				"</div>" +
			"</div>" +
			"<div class='ui-grid-c'>" +
				"<div class='ui-block-a'>{{=orderDetail.station_train_code}}</div>" +
				"<div class='ui-block-b'>{{=mor.ticket.cache.getSeatTypeByCode(orderDetail.seat_type_code)}}</div>" +
				"<div class='ui-block-c'>{{=orderDetail.coach_name}}车</div>" +
				"<div class='ui-block-d'>{{=orderDetail.seat_name}}</div>" +
				"<div class='ui-block-a'>{{=orderDetail.passenger_name}}</div>" +
				"<div class='ui-block-b'>{{=orderDetail.passenger_id_type_name}}</div>" +
				"<div class='ui-block-c'>{{=mor.ticket.util.getTicketTypeName(orderDetail.ticket_type_code)}}</div>" +
				"<div class='ui-block-d'>{{=orderDetail.ticket_price}}元</div>" +
			"</div>" +
			"<div class='ui-grid-a'>" +
			"{{ if(orderDetail.flat_msg != null ){ }}" +
			"<div class='ui-block-a'>{{=orderDetail.flat_msg}}</div>" +
		    "{{ } }}" +
			"</div>" +
		"</div>" +
		"</li>" +
		"{{~}}";
	var payFinishgenerateTicketDetailsGrid = doT.template(payFinishTicketDetailsGridTemplate);
})();