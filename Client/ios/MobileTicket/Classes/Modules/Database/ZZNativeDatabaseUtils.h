//
//  MTDatabaseUtils.h
//  MobileTicket
//
//  Created by EASY on 15/10/28.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZZNativeDatabaseTableSchema.h"

@interface ZZNativeDatabaseUtils : NSObject

+(Class) createClass:(NSString *) className withSchema:(ZZNativeDatabaseTableSchema *) schema;

@end
