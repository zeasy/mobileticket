
/* JavaScript content from js/controller/expressStatusDetail.js in folder common */
(function() {
	jq.extendModule("mor.ticket.expressTrackDetail", {
		expressTrackDetails : [],
		expressTrack_no : ''
	});
	var delete_tracking_no = "";
	var thisBtn = "";
	var expressTrack_no = '';
	jq("#expressStatusDetailView").live("pageinit", function() {
	});
	jq("#expressStatusDetailView").live("pagebeforeshow", function() {
		mor.ticket.util.initAppVersionInfo();
		var user = mor.ticket.loginUser;
		if(user.isAuthenticated === "Y"){
			jq("#expressStatusDetail").html(generateExpressStatusDetailList(mor.ticket.expressOrder.ExpressOrderArray));
			jq("#expressStatusDetailView .ui-content").iscrollview("refresh");
			jq("#expressTrackBtn").bind("tap",queryExpressStatusDetail);
			innitMethod();
		}else{
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
			} else {
				
			}
		}
	});
	
	function queryExpressStatusDetail(){
		var commonParameter = jq(this).parent().attr("id");
		expressTrack_no = commonParameter;
		var util = mor.ticket.util;
		var  commonParameters = {};
		if(mor.ticket.viewControl.queryFinishedOrderType == "4"){
			commonParameters = {
						"trans_no" : commonParameter,
						"query_where": "H"
				};
		}else{
			commonParameters = {
					"trans_no" : commonParameter,
					"query_where": "G"
			};
		}
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryExpressStatusDetail"
			};
			
		var options =  {
				onSuccess: requestQueryExpressStatusDetailSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	function requestQueryExpressStatusDetailSucceeded(result){
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			if(invocationResult.listExpressDeliverDetailBean.length == 0){
				mor.ticket.util.alertMessage("暂无物流状态详细信息");
			}else{
				mor.ticket.expressTrackDetail.expressTrackDetails = [];
				var expressTrackDetailsTimeArray = [];
				for(var i=0;i<invocationResult.listExpressDeliverDetailBean.length;i++){
					expressTrackDetailsTimeArray.push(invocationResult.listExpressDeliverDetailBean[i].deliver_time);
				}
				expressTrackDetailsTimeArray.sort(function(a,b){return a>b;});
				
				for(var i=0;i<expressTrackDetailsTimeArray.length;i++){
					for(var j=0;j<invocationResult.listExpressDeliverDetailBean.length;j++){
						if(expressTrackDetailsTimeArray[i] == invocationResult.listExpressDeliverDetailBean[j].deliver_time){
							mor.ticket.expressTrackDetail.expressTrackDetails.push(invocationResult.listExpressDeliverDetailBean[j]);
						}
					}
				}
				
				mor.ticket.expressTrackDetail.expressTrack_no = expressTrack_no;
				jq.mobile.changePage("expressTrackDetail.html");
			}
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	function innitMethod(){
		jq("#expressStatusDetailViewBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("finishedOrderDetail.html");
			return false;
		});
	deleteExpressStatusDetail();
	}
	//取消已保存送票信息
	function deleteExpressStatusDetail(){
		jq("#expressStatusDetail").on("tap","li #deleteExpressBtn",function(e){
			e.stopImmediatePropagation();
			thisBtn = jq(this);
			var commonParameter = jq(this).parent().attr("id");
			delete_tracking_no = commonParameter.split('+')[4];
			if(mor.ticket.util.isAndroid()){
				WL.SimpleDialog.show("温馨提示", "您确定取消送票吗?", [
	     			{
	     				text : "确认",
	     				handler : function() {
	     					deleteExpressStatus(commonParameter);
	     				}
	     			}, 
	     			{
	     				text : "取消",
	     				handler : function() {
	     					return false;
	     				}
	     			}
	     			]);
			}else{
				WL.SimpleDialog.show("温馨提示", "您确定取消送票吗?", [
	     			{
	     				text : "取消",
	     				handler : function() {
	     					return false;
	     				}
	     			},
	     			{
	     				text : "确认",
	     				handler : function() {
	     					deleteExpressStatus(commonParameter);
	     				}
	     			} 
	     			]);
			}
			
		});
	}
		
	function deleteExpressStatus(commonParameter){
		//thisBtn = jq(this);
		//var commonParameter = jq(this).parent().attr("id");
		//delete_tracking_no = commonParameter.split('+')[4];
		var util = mor.ticket.util;
		var commonParameters = {
					"sequence_no" : commonParameter.split('+')[0],
					"batch_no" : commonParameter.split('+')[1],
					"coach_no" : commonParameter.split('+')[2],
					"seat_no" : commonParameter.split('+')[3],
					"tracking_no" : commonParameter.split('+')[4]
			};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "deleteExpressOrderAferPay"
			};
			
		var options =  {
				onSuccess: requestDeleteExpressOrderAferPaySucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	function requestDeleteExpressOrderAferPaySucceeded(result){
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			for(var i=0;i<mor.ticket.expressOrder.ExpressOrderArray.length;i++){
				if(mor.ticket.expressOrder.ExpressOrderArray[i].tracking_no == delete_tracking_no){
					mor.ticket.expressOrder.ExpressOrderArray.splice(i,1);
				}
			}
			thisBtn.parent().css("display","none");
			jq("#expressStatues"+delete_tracking_no).val("取消送票");
			jq("#expressStatusDetailView .ui-content").iscrollview("refresh");
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	var expressStatusDetailListTemplate =
		"{{~it :expressStatusDetail:index}}" +
				"<li id='{{=expressStatusDetail.tracking_no}}'>" +
				"{{ if(index == 0){ }}" + 
					"<fieldset class='hasLegend' style='margin-top:0px;'>" +
				"{{ }else{ }}" + 
					"<fieldset class='hasLegend' style='margin-top:10px;'>" +
				"{{ } }}" +
				"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
					"<label>物流单号</label>" +
					"<div class='pencil-input' style='width:90px;'>" +
						"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset' style='width:110px;'>" +
							"<input id='attnName' type='text' value='{{=expressStatusDetail.tracking_no}}' readonly/>" +
						"</div>" +
					"</div>" +
					"{{ if(expressStatusDetail.deliver_comment.split('#')[0] == '待派件' || expressStatusDetail.deliver_comment.split('#')[0] == '待制票'){ }}" +
					"<div class='div_bigBtn' style='width: 25%;float:right;margin: 5px 0px;display: inline-table;' id='{{=expressStatusDetail.deleteId}}'><a data-role='button' id='deleteExpressBtn' style='color: #fff;padding:5px;font-size:14px;'>取消送票</a></div>" +
					"{{ }else if(expressStatusDetail.deliver_comment.split('#')[0] == '配送中' || expressStatusDetail.deliver_comment.split('#')[0] == '派件中' || expressStatusDetail.deliver_comment.split('#')[0] == '部分交付' || expressStatusDetail.deliver_comment.split('#')[0] == '交付异常' || expressStatusDetail.deliver_comment.split('#')[0] == '全部交付'){ }}" +
					"<div class='div_bigBtn' style='width: 25%;float:right;margin: 5px 0px;display: inline-table;' id='{{=expressStatusDetail.tracking_no}}'><a data-role='button' id='expressTrackBtn' style='color: #fff;padding:5px;font-size:14px;'>送票跟踪</a></div>" +
					"{{ }else{ }}" +
					"{{ } }}" +
				"</div>" +
				"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
					"<label>收   件  人</label>" +
					"<div class='pencil-input'>" +
						"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset' style='width:100%;'>" +
							"<input id='attnMobileNo' type='text' value='{{=expressStatusDetail.addressee_name}}  {{=expressStatusDetail.mobile_no}}' readonly/>" +
						"</div>" +
					"</div>" +
				"</div>" +
				"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
					"<label>寄   送   至</label>" +
					"<div class='pencil-input'>" +
						"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset' style='width:100%; padding-left:5px;'>" +
							"<span style='font-size:14px;font-family: sans-serif;'>{{=expressStatusDetail.addressee_province}}{{=expressStatusDetail.addressee_city}}{{=expressStatusDetail.addressee_county}}</span>" +
							"<span style='font-size:14px;font-family: sans-serif;'>{{=expressStatusDetail.addressee_town}}{{=expressStatusDetail.addressee_street}}{{=expressStatusDetail.detail_address}}</span>" +
						"</div>" +
					"</div>" +
				"</div>" +
				"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
					"<label>物流企业</label>" +
					"<div class='pencil-input'>" +
						"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'>" +
							"<input id='attnMobileNo' type='text' value='{{=expressStatusDetail.deliver_company}}' readonly/>" +
						"</div>" +
					"</div>" +
				"</div>" +
				"</fieldset>" +
				"<fieldset class='hasLegend' style='margin-top:0px;'>" +
						"{{ for(var i=0;i<expressStatusDetail.ticket_message.split('#').length-1;i++) { }}" +
						"<div class='ui-grid-c departshortline' style='line-height:30px;'>" +
								"<div class='ui-block-a text-ellipsis' style='border:0px; width:20%;text-align: center;'>{{=expressStatusDetail.ticket_message.split('#')[i].split('+')[6]}}</div>" +
								"<div class='ui-block-b' style='border:0px; width:13%;'>{{=expressStatusDetail.station_train_code}}</div>" +
								"<div class='ui-block-c' style='border:0px; width:48%;'>{{=mor.ticket.cache.getStationNameByCode(expressStatusDetail.from_station_telecode)}}---{{=mor.ticket.cache.getStationNameByCode(expressStatusDetail.to_station_telecode)}}</div>" +
								"<div class='ui-block-d' style='border:0px; width:19%;text-align: center;'>{{=expressStatusDetail.ticket_status.split('#')[i]}}</div>" +
								"</div>" +
								"{{ } }}"+
				"</fieldset>" +
				"<fieldset>" +
					"<div data-role='fieldcontain' class='departshortnoline ui-field-contain'>" +
						"<label>送票状态</label>" +
						"<div class='pencil-input'>" +
							"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'>" +
								"<input id='expressStatues{{=expressStatusDetail.tracking_no}}' type='text' value='{{=expressStatusDetail.deliver_comment.split('#')[0]}}' readonly/>" +
							"</div>" +
						"</div>" +
					"</div>" +
				"</fieldset>" +
			"</li>" +
		"{{~}}";
	var generateExpressStatusDetailList = doT.template(expressStatusDetailListTemplate);
})();
	