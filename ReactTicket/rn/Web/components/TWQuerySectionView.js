/**
 * Created by easy on 16/3/22.
 */

import RTView from 'RTView'



import React,{
    View,
    TouchableOpacity,
    Text,
    StyleSheet,
} from 'react-native'

import Style from 'RTStyle'


class TWQuerySectionView extends RTView {


    render() {
        let query = this.props.query || [];

        return (
            <View style={{backgroundColor:'white',height:20}}>
                <View  style={{flex:1,flexDirection:'row',marginLeft:10,marginRight:10}}>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <Text style={{flex:1,alignSelf:'center'}}>车次({query.length})</Text>
                        <Text style={{flex:1,alignSelf:'center'}}>区间</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={{flex:0.7,flexDirection:'row'}}>
                            <Text style={{flex:1,alignSelf:'center'}}>座次</Text>
                            <Text style={{flex:1,marginLeft:25,alignSelf:'center'}}>余票</Text>
                        </View>
                        <Text style={{flex:0.3,textAlign:'right',alignSelf:'center'}}>票价</Text>
                    </View>
                </View>
                <View style={{height:StyleSheet.hairlineWidth,backgroundColor:Style.default,marginLeft:10,marginRight:10}}/>
            </View>

        );
    }
}

module.exports = TWQuerySectionView;