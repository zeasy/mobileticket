/**
 * Created by easy on 16/3/18.
 */

'use strict'

import * as actions from '../constants/ActionTypes'
import RTHttp from 'RTHttp'
import Hud from 'RTHud'


export function clearQueryRequest() {
    return dispatch => {
        dispatch({
            type : actions.TWQueryRequestAction,
            data : {
                loading : false,
                refreshing : false,
                query : []
            }
        });
    };
}

export function queryRequest(from_station,to_station,train_date,isRefresh) {
    return dispatch => {
        //start
        dispatch({
            type : actions.TWQueryRequestAction,
            data : {
                status : 'loading',
                refreshing : !!isRefresh,
                loading : true
            }
        });

        if (!isRefresh) Hud.showLoading();

        return RTHttp.get('https://kyfw.12306.cn/otn/leftTicket/query',[
            {'leftTicketDTO.train_date':train_date},
            {'leftTicketDTO.from_station':from_station},
            {'leftTicketDTO.to_station':to_station},
            {'purpose_codes':'ADULT'}
        ]).then(result=> {
            if (result.status == true) {
                dispatch({
                    type : actions.TWQueryRequestAction,
                    data : {
                        query:result.data,
                        status : 'finish',
                        refreshing : false,
                        loading : false,
                        day : train_date,
                    }
                });
                if (!isRefresh) Hud.dismiss();

            } else {
                let error = new Error(result.messages && JSON.stringify(result.messages),result.status);
                dispatch({
                    type : actions.TWQueryRequestAction,
                    data : {
                        status : 'fail',
                        error : error,
                        refreshing : false,
                        loading : false
                    }
                });
                Hud.showFail((result.messages && result.messages.join('\n')) || '网络错误');
            }
        }).catch(error => {
            dispatch({
                type : actions.TWQueryRequestAction,
                data : {
                    status : 'fail',
                    error : error,
                    refreshing : false,
                    loading : false
                }
            });
            Hud.showFail(error.message);
        });
    };



}