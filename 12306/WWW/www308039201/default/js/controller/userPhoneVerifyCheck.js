
/* JavaScript content from js/controller/userPhoneVerifyCheck.js in folder common */
(function() {
	var focusArray=[];
	var preView = "";
	jq("#userPhoneVerifyCheckView").live("pageshow", function() {
		focusArray=[];
	});
	var util = mor.ticket.util;
	//var clearTimeOutFun = "";
	function registerAutoScroll(){	
		util.enableAutoScroll('#validationCode',focusArray);
	}
	jq("#userPhoneVerifyCheckView").live("pageinit", function() {
		registerAutoScroll();
		jq("#validationCode").change(function(){
			 var checkNum = /^[0-9]{6}$/;
			 if(!checkNum.test(jq("#validationCode").val())){
				 mor.ticket.util.alertMessage("手机校验码不正确");
				 jq("#validationCode").val("");
				 return;
			 }
		});
		jq("#userPhoneVerifyCheckBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("my12306.html");
			return false;
		});
		jq("#validationCodeBtn").bind("tap", function() {
			var checkFormUtil = mor.ticket.checkForm;
			if(util.isNoValue(jq("#phoneNo").val())){
				util.alertMessage("请输入手机号码");
				return;
			}
			if(!checkFormUtil.isMobile(jq("#phoneNo").val())){
				util.alertMessage("请输入正确的手机号码!");
				return;
			}
			var commonParameters = {
					'mobile_no': jq("#phoneNo").val(),
					'pass_code': ""
				};
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "getCheckMobieCode"
			};
			
			var options =  {
					onSuccess: validationCodeRequestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
		});
		jq("#enterValidationCodeBtn").bind("tap", function() {
			var checkFormUtil = mor.ticket.checkForm;
			if(util.isNoValue(jq("#phoneNo").val())){
				util.alertMessage("请输入手机号码");
				return;
			}
			if(!checkFormUtil.isMobile(jq("#phoneNo").val())){
				util.alertMessage("请输入正确的手机号码!");
				return;
			}
			if(util.isNoValue(jq("#validationCode").val())){
				util.alertMessage("请填写短信验证码");
				return;
			}
			var checkNum = /^[0-9]{6}$/;
			if(!checkNum.test(jq("#validationCode").val())){
				mor.ticket.util.alertMessage("手机校验码不正确");
				jq("#validationCode").val("");
				return;
			}
			
			var commonParameters = {
					'mobile_no': jq("#phoneNo").val(),
					'check_code': jq("#validationCode").val(),
					'pass_code': ""
			};
			
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "checkMobileCode"
			};
			
			var options =  {
					onSuccess: requestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
		});
	});
	
	function validationCodeRequestSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;	
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			WL.SimpleDialog.show("温馨提示", "手机核验验证码已发送至您的手机,请查收。", [ {
				text : '确定',
				handler : function() {
					countDown(jq("#validationCodeBtn"),120);
				}
			} ]);
		} else {
			util.alertMessage(invocationResult.error_msg);
		}
	}
	jq("#userPhoneVerifyCheckView").live("pagebeforeshow", function(e,data) {
		preView = data.prevPage.attr("id");
		jq("#validationCode").val("");
		if(jq("#validationCodeBtn").hasClass("ui-disabled")){
			//clearTimeout(clearTimeOutFun);
			jq("#validationCodeBtn").removeClass("ui-disabled");
			jq("#validationCodeBtn").html("获取验证码");
		}
		/*
		if(mor.ticket.userPhone.userPhoneFlag == "phoneUpdate"){
			jq("#headerTitle").html("修改手机号");
			jq("#newPhoneNoDiv").css("display","inline-block");
			jq("#oldPhoneNoLable").html("原&nbsp;手&nbsp;机&nbsp;号");
			var checkFormUtil = mor.ticket.checkForm;
			jq("#newPhoneNo").change(function () { 
				if(util.isNoValue(jq("#regist_mobile").val())){
					util.alertMessage("请输入手机号码");
					return;
				}
				if(!checkFormUtil.isMobile(jq("#regist_mobile").val())){
					util.alertMessage("请输入正确的手机号码!");
					return;
				}
			} );
		}else{
			jq("#headerTitle").html("手机号核验");
			jq("#newPhoneNoDiv").css("display","none");
			jq("#oldPhoneNoLable").html("手&nbsp;&nbsp;&nbsp;机&nbsp;&nbsp;&nbsp;号");
		}*/
		var userInfo = mor.ticket.loginUser;
		jq("#phoneNo").val(userInfo.mobile_no);
		//jq("#phoneNo").addClass("ui-disabled");
	});
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;	
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.loginUser.is_receive = "Y";
			mor.ticket.loginUser.mobile_no = jq("#phoneNo").val();
			mor.ticket.userInfo.mobile_no = jq("#phoneNo").val();
			mor.ticket.loginUser.is_receive = "Y";
			WL.SimpleDialog.show("温馨提示","您的手机号 "+mor.ticket.loginUser.mobile_no+" 核验成功。", [ {
				text : '确定',
				handler : function() {
					jq.mobile.changePage("my12306.html");
				}
			} ]);
		}else{
			util.alertMessage(invocationResult.error_msg);
		}
	}
	function countDown(obj,second){
		 if(second>=0){
	          if(typeof buttonDefaultValue === 'undefined' ){ 
	            buttonDefaultValue =  "获取验证码"; 
	        }
	        obj.addClass("ui-disabled");
	        var objValue = buttonDefaultValue+'('+second+')';
	        obj.html(objValue);
	        second--;
	        //clearTimeOutFun =  setTimeout(function(){countDown(obj,second);},1000); 
	        setTimeout(function(){countDown(obj,second);},1000); 
	    }else{
	        obj.removeClass("ui-disabled");
	        obj.html(buttonDefaultValue);
	    }    
	}
})();