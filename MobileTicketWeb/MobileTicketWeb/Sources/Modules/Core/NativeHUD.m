//
//  NativeHUD.m
//  MobileTicketWeb
//
//  Created by EASY on 16/1/13.
//  Copyright © 2016年 Z.EASY. All rights reserved.
//

#import "NativeHUD.h"
#import <RCTConvert.h>
#import <SVProgressHUD/SVProgressHUD.h>

@implementation NativeHUD

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(show:(NSDictionary *) options s:(RCTResponseSenderBlock) s f:(RCTResponseErrorBlock) f) {
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
		NSString *type = [RCTConvert NSString:options[@"type"]];
		NSString *status = [RCTConvert NSString:options[@"status"]];
		if (options[@"progress"]) {
			[SVProgressHUD showProgress:[RCTConvert float:options[@"progress"]] status:status];
		} else if ([@"info" compare:type options:NSCaseInsensitiveSearch] == NSOrderedSame) {
			[SVProgressHUD showInfoWithStatus:status];
		} else if ([@"error" compare:type options:NSCaseInsensitiveSearch] == NSOrderedSame) {
			[SVProgressHUD showErrorWithStatus:status];
		} else if([@"success" compare:type options:NSCaseInsensitiveSearch] == NSOrderedSame){
			[SVProgressHUD showSuccessWithStatus:status];
		} else {
			[SVProgressHUD showWithStatus:status];
		}
	});
	
}

RCT_EXPORT_METHOD(hide:(RCTResponseSenderBlock) s f:(RCTResponseErrorBlock) f) {
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD dismiss];
	});
}

@end
