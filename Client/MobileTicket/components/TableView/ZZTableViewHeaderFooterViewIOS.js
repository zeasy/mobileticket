/**
 * Created by easy on 15/11/12.
 */

var View = require('./../View');
var React = require('react-native');
var ZZNativeTableViewHeaderFooterViewIOSConsts = require('NativeModules').UIManager.ZZTableViewHeaderFooterView.Constants;

var {
    requireNativeComponent
    } = React;

var ZZNativeTableViewHeaderFooterViewIOS = requireNativeComponent('ZZTableViewHeaderFooterView');

class ZZTableViewHeaderFooterViewIOS extends View {

    componentWillMount() {
        this.state.sectionData = this.props.sectionData || {};
    }

    render() {
        return <ZZNativeTableViewHeaderFooterViewIOS {...this.props} />
    }
}



ZZTableViewHeaderFooterViewIOS.Consts = ZZNativeTableViewHeaderFooterViewIOSConsts;

exports = module.exports = ZZTableViewHeaderFooterViewIOS;