//
//  ZZTableViewHeaderFooterViewManager.m
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZTableViewHeaderFooterViewManager.h"
#import "ZZTableViewHeaderFooterView.h"

@implementation ZZTableViewHeaderFooterViewManager

RCT_EXPORT_MODULE()
RCT_EXPORT_VIEW_PROPERTY(isHeader, BOOL)
RCT_EXPORT_VIEW_PROPERTY(reuseIdentifier, NSString)
RCT_EXPORT_VIEW_PROPERTY(section, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(sectionData, NSDictionary)

-(UIView<RCTComponent> *)view {
	return (UIView<RCTComponent> *)[[ZZTableViewHeaderFooterView alloc] initWithEventDispatcher:self.bridge.eventDispatcher];
}

-(NSDictionary *)constantsToExport {
	return @{};
}

@end
