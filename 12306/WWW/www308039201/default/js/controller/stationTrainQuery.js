
/* JavaScript content from js/controller/stationTrainQuery.js in folder common */
(function(){
	var focusArray=[];
	function registerAutoScroll(){	
		var util = mor.ticket.util;
		util.enableAutoScroll('#trainStartTime',focusArray);
		util.enableAutoScroll('#trainEndTime',focusArray);
	}
	jq("#stationTrainQueryView").live("pageinit", function(){		
		registerStationTrainQueryBtnLisener();
		registerStationTrainQueryBackButtonHandler();
		registerStationTrainHeadersBtnGroupClickHandler();
		registerStationInputClickHandler();
//		registerShareBtnClickHandler();
		registerDatePicker();
		registerAutoScroll();
	});
	
	jq("#stationTrainQueryView")
	.live(
			"pageshow",
			function() {
				mor.ticket.util.initAppVersionInfo();
						jq("#stationTrainQueryView").css("pointer-events", "auto");
						mor.ticket.viewControl.current_tab = "stationTrainQueryTab";
						mor.ticket.viewControl.tab1_cur_page = vPathCallBack() + "stationTrainQuery.html";
						var cache = mor.ticket.cache;
						jq("#stationTrainstationInput").val(cache.getStationNameByCode(mor.ticket.stationTrainQuery.queryStation));
						if(mor.ticket.currentPagePath.calenderDate != ""){
							jq("#stationTrainQueryDateInput").val(mor.ticket.currentPagePath.calenderDate);
						}else{
							var date = mor.ticket.util.getNewDate();
							date.setDate(date.getDate() + 1);
							var train_date = date.format("yyyy-MM-dd");
							jq("#stationTrainQueryDateInput").val(train_date);
						}
						//初始化开车时间段
						var start_time_period = mor.ticket.stationTrainQuery.startTimes;
						var stop_time_period = mor.ticket.stationTrainQuery.stopTimes;
						jq("#trainStartTime option").eq(getIndexOfSelectTimes(start_time_period)).attr("selected",true);
						if(jq("#trainStartTime").val() != "0000--2400"){
							jq("#trainStartTime-button").css("color","#298CCF");
						}else{
							jq("#trainStartTime-button").css("color","black");
						}
						jq("#trainStartTime").selectmenu('refresh', true);
						jq("#trainEndTime option").eq(getIndexOfSelectTimes(stop_time_period)).attr("selected",true);
						if(jq("#trainEndTime").val() != "0000--2400"){
							jq("#trainEndTime-button").css("color","#298CCF");
						}else{
							jq("#trainEndTime-button").css("color","black");
						}
						jq("#trainEndTime").selectmenu('refresh', true);
						//初始化车次类型train_hearder
						if(mor.ticket.stationTrainQuery.trainType == ""){
							jq("#stationTrainHeadersBtnGroup a").eq(0).addClass("ui-btn-active ui-state-persist");
						}else{
							var newtrain_headers = mor.ticket.stationTrainQuery.trainType.split("#");
							var util = mor.ticket.util;
							for ( var i = 0, len = jq("#stationTrainHeadersBtnGroup a").length; i < len; i++) {
								var jq_a = jq("#stationTrainHeadersBtnGroup a:eq("
										+ i + ")");
								if(jq_a.hasClass("ui-btn-active ui-state-persist")){
									jq_a.removeClass("ui-btn-active ui-state-persist");
								}
								if (util.indexOf(newtrain_headers,jq_a.attr("name"))) {
									jq_a.addClass("ui-btn-active ui-state-persist");
								}
							}
						}
			});
	function getIndexOfSelectTimes(time_period_str){
		var index = 0;
		switch(time_period_str){
		case '0000--2400':
			index = 0;
			break;
		case '0000--0600':
			index = 1;
			break;
		case '0600--1200':
			index = 2;
			break;
		case '1200--1800':
			index = 3;
			break;
		case '1800--2400':
			index = 4;
			break;
		default:
			index = 0;
		}
		return index;
	}
	function registerDatePicker(){
		jq("#stationTrainQueryDateInput").bind("tap",function(){
			var current = mor.ticket.currentPagePath;
			var monthValue;
			current.defaultDateValue[0]=jq("#stationTrainQueryDateInput").val().slice(0,4);
			monthValue=parseInt(jq("#stationTrainQueryDateInput").val().slice(5,7));
			if(monthValue == "1"){
				monthValue = "0";
			}else monthValue = monthValue-1;
			if(monthValue < 10){
				current.defaultDateValue[1] = "0"+monthValue;
			}else{
				current.defaultDateValue[1] = monthValue;
			}
			current.defaultDateValue[2]=jq("#stationTrainQueryDateInput").val().slice(8,10);
			var reservePeriod = initReservedPeriod();
			jQuery.extend(jQuery.mobile.datebox.prototype.options, {
				mode:"calbox",
				useInline: true,
				calControlGroup: true,
				defaultValue: current.defaultDateValue,
				showInitialValue: true,
				lockInput: false,
				useFocus: true,
				maxDays: reservePeriod,
				buttonIcon: "grid",
				calUsePickers: true,
				afterToday: true,
				beforeToday: false,
				notToday: false,
				calOnlyMonth: false,
				overrideSlideFieldOrder: ['y','m','d','h','i']
			});
			mor.ticket.currentPagePath.fromPath = "stationTrainQuery.html";
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.pickYears = false;
			mor.ticket.currentPagePath.show = false;
			jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
			mor.ticket.util.changePage(goingPath);
		});
	}
	function registerStationTrainQueryBtnLisener(){
		jq("#stationTrainQueryBtn").bind("tap", sendStationTrainQueryRequest);
		jq("#trainStartTime").bind("change", function() {
			mor.ticket.stationTrainQuery.startTimes = jq(this).val();
			if(jq(this).val() == "0000--2400"){
				jq("#trainStartTime-button").css("color","black");
			}else{
				jq("#trainStartTime-button").css("color","#298CCF");
			}
			return false;
		});
		jq("#trainEndTime").bind("change", function() {
			mor.ticket.stationTrainQuery.stopTimes = jq(this).val();
			if(jq(this).val() == "0000--2400"){
				jq("#trainEndTime-button").css("color","black");
			}else{
				jq("#trainEndTime-button").css("color","#298CCF");
			}
			return false;
		});
	};
	
	function sendStationTrainQueryRequest(e) {
		if(jq("#stationTrainstationInput").val()===""
			|| jq("#stationTrainstationInput").val()===undefined
			|| jq("#stationTrainstationInput").val()===null){
			mor.ticket.util.alertMessage("请填车站");
			mor.ticket.util.changePage(vPathCallBack()+"stationTrainQuery.html");
			return;
		}
		var stationTrainQueryTrain_headers= "";
		for ( var i = 0, len = jq("#stationTrainHeadersBtnGroup a").length; i < len; i++) {
			var jq_a = jq("#stationTrainHeadersBtnGroup a:eq("
					+ i + ")");
			if (jq_a.hasClass("ui-btn-active ui-state-persist")) {
				stationTrainQueryTrain_headers += jq_a.attr("name")+"#";
			}
		}
		if (stationTrainQueryTrain_headers == "") {
			stationTrainQueryTrain_headers = "QB#";
		}
		var util = mor.ticket.util;
		mor.ticket.stationTrainQuery.trainDate = jq("#stationTrainQueryDateInput").val();
		mor.ticket.stationTrainQuery.startTimes = jq("#trainStartTime").val();
		mor.ticket.stationTrainQuery.stopTimes = jq("#trainEndTime").val();
		mor.ticket.stationTrainQuery.trainType = stationTrainQueryTrain_headers.substring(0,stationTrainQueryTrain_headers.length);
		var commonParameters = {
			'train_start_date':  util.processDateCode(jq("#stationTrainQueryDateInput").val()),
			'train_station_code': mor.ticket.stationTrainQuery.queryStation,
			'startTimes':jq("#trainStartTime").val(),
			'stopTimes':jq("#trainEndTime").val(),
			'trainType':stationTrainQueryTrain_headers.substring(0,stationTrainQueryTrain_headers.length)
			
		};
		
		var invocationData = {
			adapter: mor.ticket.viewControl.adapterUsed,
			procedure: "stationTrainQuery"
		};
		
		var options =  {
				onSuccess: requestStationTrainQuerySucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestStationTrainQuerySucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.stationTrainQuery.stationTrainQueryResult = invocationResult.cacheStopTimeDTO;
			jq.mobile.changePage(vPathCallBack() + "stationTrainQueryResults.html");
		}else {	
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	function getWeek(prompt) {
		var desc = [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ];
		if (typeof prompt == "string") {
			var date = mor.ticket.util.setMyDate(prompt);
			return desc[date.getDay()];
		} else {
			return desc[prompt.getDay()];
		}
	}
	//初始化预售期
	function initReservedPeriod() {
		var reservedPeriodType =  '3';
		var reservePeriod = parseInt(window.ticketStorage
				.getItem("reservePeriod_" + reservedPeriodType), 10);
		return reservePeriod;
	}
	function registerStationTrainQueryBackButtonHandler() {
		jq("#stationTrainQueryBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage(vPathCallBack() + "moreOption.html");
			return false;
		});
	}
	function registerStationTrainHeadersBtnGroupClickHandler() {
		jq("#stationTrainHeadersBtnGroup").on("tap", "a", function() {
			var activeCls = "ui-btn-active ui-state-persist";
			var that = jq(this);
			var id = that.attr('id');
			var name = that.attr("name");
			var trainHeaders = mor.ticket.stationTrainQuery.trainHeaders;
			if (that.hasClass(activeCls)) {
				if (id != "stationQB") {
					that.removeClass(activeCls);
					trainHeaders.splice(jq.inArray(name,trainHeaders),1);
					// 判断如果出了全部之外的alink都没有ui-btn-active
					// ui-state-persist，那么给QB添加此属性
					if (!jq("#stationD,#stationZ,#stationT,#stationK,#stationQT").hasClass(activeCls)) {
						jq("#stationQB").addClass(activeCls);
						mor.ticket.stationTrainQuery.trainHeaders = [];
					}
				}
			} else {
				if (id == "stationQB") {
					mor.ticket.stationTrainQuery.trainHeaders = [];
					that.addClass(activeCls).siblings().removeClass(activeCls);
				} else {
					that.addClass(activeCls);
					trainHeaders.push(name);
					if (jq("#stationQB").hasClass(activeCls)) {
						jq("#stationQB").removeClass(activeCls);
					}
				}
			}
			mor.ticket.stationTrainQuery.trainType = "";
			for(var i = 0;i<mor.ticket.stationTrainQuery.trainHeaders.length;i++){
				mor.ticket.stationTrainQuery.trainType += mor.ticket.stationTrainQuery.trainHeaders[i]+"#"; 
			}
			return false;
		});
		
	}
	
	
	
	function registerStationInputClickHandler() {

		jq("#stationTrainstationInput").bind("tap", function(e) {
			e.stopImmediatePropagation();
			jq("#stationTrainQueryView").css("pointer-events", "none");
			mor.ticket.views.selectStation.isFromStation = false;
			mor.ticket.util.changePage(vPathCallBack() + "selectStation.html");
			return false;
		});
	}
	;
})();