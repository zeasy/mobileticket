
/* JavaScript content from js/controller/travelPlan.js in folder common */
/**
 * travelPlan
 */
(function() {
	var travelPlanArry = new Array();
	var cache_planList=[];
	var prevId;
	var index;
	var  travelPlanList = new Array();
	jq("#travelPlanDetailView").live("pagebeforeshow", function(e,data) {
		mor.ticket.util.initAppVersionInfo();
		prevId = data.prevPage.attr("id");
		travelPlanList = JSON.parse(window.ticketStorage.getItem("travelPlanList"));
		if(travelPlanList == null){
			mor.ticket.util.alertMessage("您还没有出行计划！");
			mor.ticket.util.changePage(vPathViewCallBack()+"MobileTicket.html");
			return false;	
		}
		jq("#travelPlanSubmitBtn").show();
		if(prevId == "payFinishtView"){
			jq("#travelPlanSubmitBtn").hide();
		}
		for(var i = 0;i<travelPlanList.length;i++){
			travelPlanList[i].checked = 0;
		}
		jq("#travelPlanListView").html(generateOrdersDetailList(travelPlanList));
		 if(prevId == "passengersView" || prevId == "bookTicketView" ){
			 jq("#travelPlanListView span[class='change']").hide();
			 jq("#deletePlanBtn").hide();
		 }else{
			 jq("#travelPlanListView span[class='change']").show();
			 jq("#deletePlanBtn").show();
		 }
		 if(mor.ticket.loginUser.isAuthenticated !== "Y"){
				mor.ticket.util.keepPageURL();

			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
			} else {
				registerAutoLoginHandler(travelPlanListItemClickHandler(), autologinFailJump);
			}				
		   }else{
			   travelPlanListItemClickHandler();
		   }

	});
	
	jq("#travelPlanDetailView").live("pageinit", function() {
		deleteTravelPlan();
		backBtnListener();
	});
	function travelPlanListItemClickHandler() {
		jq("#travelPlanListView").off().on("tap", "li",function(e) {
			travelPlanArry.length =0;
			 index = jq(this).attr("data-index");
			 var selectedPlan = travelPlanList[index];
			 if(prevId == "passengersView" || prevId == "bookTicketView" ){
				    travelPlanArry.push(travelPlanList[index]);
					searchAndShowResult(travelPlanArry);
			 }else{
				 if (jq(this).find("input").is(":checked")){
						jq(this).find("input").attr("checked", false);
						for(var i=0;i<cache_planList.length;i++){
							if( selectedPlan.station_train_code == cache_planList[i].station_train_code 
									&& selectedPlan.passenger_name == cache_planList[i].passenger_name
									&& selectedPlan.passenger_id_type_code == cache_planList[i].passenger_id_type_code
									&& selectedPlan.checked == cache_planList[i].checked){
								cache_planList.splice(i,1);
								selectedPlan.checked =0;
								travelPlanList[index].checked=0;
							 break;
							}
						}
				 }else{
					 var init_auto=0;
					  for(var ii =0;ii<cache_planList.length;ii++){
						  if(cache_planList[ii].station_train_code== selectedPlan.station_train_code 
								  && selectedPlan.passenger_name == cache_planList[ii].passenger_name
								  && selectedPlan.passenger_id_type_code == cache_planList[ii].passenger_id_type_code 
								  && selectedPlan.checked == cache_planList[ii].checked ){
							  init_auto++;
						  }
					  }
			     if (init_auto > 0) {
					jq(this).find("input").attr("checked",true);
				}else{
				 jq(this).find("input").attr("checked", true);
				 selectedPlan.checked = 1;
				 travelPlanList[index].checked=1;
				 cache_planList.push(selectedPlan);
				 }
				 }
			 }
			return false;
		}).on("taphold", "li",function(e){
					if(jq(this).attr("data-role")!=null&&jq(this).attr("data-role")=='list-divider'){
						jq(this).removeClass("ui-btn-active");
						return false;
					}
					return false;	
				});
	}
	
	function searchAndShowResult(travelPlanArry) {
		var util = mor.ticket.util;
		var purpose_codes ="";
		if(travelPlanArry[0].ticket_type_code =="3"){
			purpose_codes ="0X";
		}else{
			purpose_codes ="00";
		}

			commonParameters = {
					"train_date" : util.processDateCode(mor.ticket.leftTicketQuery.train_date),
					"purpose_codes" : purpose_codes,
					"from_station" : travelPlanArry[0].from_station_telecode,
					"to_station" : travelPlanArry[0].to_station_telecode,
					"station_train_code" : travelPlanArry[0].station_train_code,
					"start_time_begin" : "0000",
					"start_time_end" : "2400",
					"train_headers" : "QB#",
					"train_flag" : "",
					"seat_type" : travelPlanArry[0].seat_type_code,
					"seatBack_Type" : "",
					"ticket_num" : ""
				};
		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "queryLeftTicket"
		};
		var options = {
			onSuccess : requestSucceeded,
			onFailure : util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,
				options, true);
	}
	
	function requestSucceeded(result) {
		mor.ticket.currentTicket.length =0;
		var invocationResult = result.invocationResult;
		var passengerArray  = new Array();
		var idTypeArray = new Array();
		var typeCode = new Array();
		var passArray = new Array();
		var ints = 0; 
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.currentTicket = invocationResult.ticketResult[0];
			var yplist = processYpInfo(mor.ticket.currentTicket.yp_info);
			for(var i = 0; i<yplist.length;i++){
				if(yplist[i].num == 0){
					ints++;
				}
			}
			if(mor.ticket.currentTicket.message !=""){
				mor.ticket.util.alertMessage(mor.ticket.currentTicket.message);
				return;
			}else if(ints == yplist.length){
				mor.ticket.util.alertMessage("很抱歉，当日该车次票已售完");
				return;
			}else{	
			mor.ticket.currentTicket.yplist = yplist;
			var  start_train_date= mor.ticket.currentTicket.start_train_date;
			mor.ticket.currentTicket.train_date = start_train_date.substring(0,4)+"-"+start_train_date.substring(4,6)+"-"+start_train_date.substring(6,8);
			mor.ticket.currentPagePath.calenderDate = mor.ticket.currentTicket.train_date;
			var passengers = mor.ticket.passengersCache.passengers;
			passengerArray = travelPlanArry[0].passenger_name.split(",");
			idTypeArray = travelPlanArry[0].passenger_id_type_code.split(",");
			typeCode = travelPlanArry[0].ticket_type_code.split(",");
			passengerArray.splice(passengerArray.length-1,1);
			idTypeArray.splice(idTypeArray.length-1,1);
			typeCode.splice(typeCode.length-1,1);
			mor.ticket.passengerList.length = 0;
			for(var i =0 ; i<passengerArray.length;i++){
				passArray = jq.grep(passengers,function(item, passengerIndex) {
						return passengerArray[i] == item.user_name
						&& idTypeArray[i] == item.id_type;
					});
				 
				 mor.ticket.passengerList.push(passArray[0]);
			}
			for(var j = 0; j<typeCode.length; j++ ){
				mor.ticket.passengerList[j].ticket_type = typeCode[j];
			}
			mor.ticket.util.changePage("passengers.html");
			return false;
			}
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			return;
		}
	}
	function processYpInfo(ypinfo) {
		var cache = mor.ticket.cache;
		var arrayLength = ypinfo.length / 10;
		var obj = new Array();
		var temp_seat_rate = 50;// 对于没有定义的座位，rate值从50开始递增
		for ( var i = 0, m = 6, n = 10, x = 0, y = 1; i < arrayLength; i++, m = m + 10, n = n + 10, x = x + 10, y = y + 10) {
			var seat_type_id = ypinfo.substring(x, y);
			var seat_type = null;
			var seat_type_rate = null;
			var seat_num = 0;
			if (parseInt(ypinfo.substring(m, m + 1), 10) >= 3) {
				seat_type = "无座";
				seat_type_rate = 100;
				seat_num = parseInt(ypinfo.substring(m, n), 10)-3000;
			} else {
				seat_type = cache.getSeatTypeByCode(seat_type_id);
				seat_type_rate = cache.getSeatTypeRateByCode(seat_type_id);
				if (seat_type_rate === null || seat_type_rate === "undefined") {
					seat_type_rate = temp_seat_rate;
					temp_seat_rate = temp_seat_rate + 1;
				}
				seat_num = parseInt(ypinfo.substring(m, n), 10);
			}
			
			obj[i] = {
				type_id : seat_type_id,
				type : seat_type,
				type_rate : seat_type_rate,// 保存座位rate用于排序
				num : seat_num,
				uclass : getLiClass(i),
				price : (parseFloat(ypinfo.substring(y, m), 10) / 10)
						.toFixed(1)
			};
		}
		var sortedObjs = sortYpInfo(obj);

		for ( var i = 0; i < sortedObjs.length; i++) {
			sortedObjs[i].uclass = getLiClass(i);
		}
		sortedObjs = repalceYpInfoType(sortedObjs);
		return sortedObjs;
	}
	// 把两个字数以上的座位类型改为两位
	function repalceYpInfoType(objs) {
		for ( var i in objs) {
			var type = objs[i].type;
			switch (type) {
			case "商务座":
				objs[i].type = "商务";
				break;
			case "特等座":
				objs[i].type = "特等";
				break;
			case "一等座":
				objs[i].type = "一等";
				break;
			case "二等座":
				objs[i].type = "二等";
				break;
			case "高级软卧":
				objs[i].type = "高软";
				break;
			case "观光座":
				objs[i].type = "观光";
				break;
			}
		}
		return objs;
	}

	// 按照座位的type_rate进行排序，规则如下
	// 商务座、特等座、一等座、二等座、高级软卧、软卧、硬卧、软座、硬座、（...）、无座 的顺序显示；
	// 如果出现其余席别，显示在这些席别的后面，顺序暂不限，其中 无座 总放在最后面。
	function sortYpInfo(ypinfoArray) {
		
		var by = function(name) {
			return function(o, p) {
				var a, b;
				if (typeof o === "object" && typeof p === "object" && o && p) {
					a = o[name];
					b = p[name];
					if (a === b) {
						return 0;
					}
					if (typeof a === typeof b) {
						return a < b ? -1 : 1;
					}
					return typeof a < typeof b ? -1 : 1;
				} else {
					throw ("error");
				}
			};
		};

		ypinfoArray.sort(by("type_rate"));
		return ypinfoArray;

	}
	function getLiClass(index) {
		switch (index % 4) {
		case 0:
			return "ui-block-a";
		case 1:
			return "ui-block-b";
		case 2:
			return "ui-block-c";
		case 3:
			return "ui-block-d";
		default:
			return "ui-block-e";

		}
	}

		function deleteTravelPlan(){
			jq("#deletePlanBtn").bind("tap",function(e) {
				e.stopImmediatePropagation();
				travelPlanList = jq.grep(travelPlanList,function(item, planIndex) {
						return item.checked != 1;	
				});
			cache_planList.length =0;
			window.ticketStorage.setItem("travelPlanList",JSON.stringify(travelPlanList));
			jq("#travelPlanListView").html(generateOrdersDetailList(travelPlanList));
			jq("#travelPlanListView span[class='change']").show();
			});
		}
		function backBtnListener(){
			jq("#travelPlanViewBackBtn").bind("tap",function(e) {
				e.stopImmediatePropagation();
				if(prevId == "payFinishtView"){
					mor.ticket.util.changePage(vPathCallBack() + "payFinish.html");
					return false;
				}else if(prevId == "FinishedOrderDetailView"){
					mor.ticket.util.changePage(vPathCallBack() + "queryOrder.html");
					return false;
				}else{
						mor.ticket.util.changePage(vPathViewCallBack()+"MobileTicket.html");
						return false;
				}
		});
		}		

	var ordersDetailListTemplate =
		"{{~it :travelPlanList:index}}" +
		"<li data-role='list-divider' data-index='{{=index}}'>" +
			"<div class='ui-grid-a' >" +
				"<div class='ui-block-a'>{{=travelPlanList.station_train_code}}</div>" +
				"<div class='ui-block-b'>" +
					"<div class='ui-grid-b'>" +
						"<div class='ui-block-a'>{{=mor.ticket.cache.getStationNameByCode(travelPlanList.from_station_telecode)}}" +
								"<span>{{=mor.ticket.util.formateTrainTime(travelPlanList.start_time)}}</span></div>" +
						"<div class='ui-block-b trainDetailArrow'></div>" +
						"<div class='ui-block-c'>{{=mor.ticket.cache.getStationNameByCode(travelPlanList.to_station_telecode)}}" +
								"<span>{{=mor.ticket.util.formateTrainTime(travelPlanList.arrive_time)}}</span></div>" +
					"</div>" +
				"</div>" +
			"</div>" +
			"<div class='ui-grid-c'>" +
				"<div class='ui-block-a'></div>" +
				"<div class='ui-block-b'>{{=mor.ticket.util.getSeatTypeName(travelPlanList.seat_type_code,travelPlanList.seat_no)}}</div>" +
				"<div class='ui-block-c'></div>" +
				"<div class='ui-block-d'>{{=travelPlanList.ticket_price}}元</div>" +
			"</div>" +
			"<div class='ui-grid-a'>" +
			"<div class='ui-block-a'>{{=travelPlanList.passenger_name}}</div>" +
				"<div class='ui-block-b' style='text-align: right;'>" +
					"<span class='change' style='display:none;'>" +
					"	<input class='changeTicketChkbox' id ='travelPlanChkBox' type='checkbox'/><span></span></span>" +
				"</div>" +
				"</div>" +
		"</li>" +
		"{{~}}";
	var generateOrdersDetailList = doT.template(ordersDetailListTemplate);
	})();