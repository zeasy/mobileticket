
/* JavaScript content from js/controller/ticketPriceQueryResults.js in folder common */
(function() {
	jq("#ticketPriceQueryResultsView").live("pageinit", function() {
		registerPrevDateBtnClickHandler();
		registerNextDateBtnClickHandler();
		registSelectDateChangeHandler();
		registerBackButtonHandler();
	});
	function registSelectDateChangeHandler(){
		jq("#ticketPriceChooseDateBtn").change(function(){
			var date = mor.ticket.util.setMyDate(jq("#ticketPriceChooseDateBtn").val());
			var selectDate = new Date(date.getTime());
			mor.ticket.leftTicketQuery.train_date = selectDate.format("yyyy-MM-dd");
			searchAndShowResult(mor.ticket.leftTicketQuery);
		});
		jq("#ticketPriceDateInputId").bind("tap",function(){
			var current = mor.ticket.currentPagePath;
			var monthValue;
			current.defaultDateValue[0]=jq("#ticketPriceChooseDateBtn").val().slice(0,4);
			monthValue=parseInt(jq("#ticketPriceChooseDateBtn").val().slice(5,7));
			if(monthValue == "1"){
				monthValue = "0";
			}else monthValue = monthValue-1;
			if(monthValue < 10){
				current.defaultDateValue[1] = "0"+monthValue;
			}else{
				current.defaultDateValue[1] = monthValue;
			}
			current.defaultDateValue[2]=jq("#ticketPriceChooseDateBtn").val().slice(8,10);
			var reservePeriod = mor.ticket.history.reservePeriod;
			jQuery.extend(jQuery.mobile.datebox.prototype.options, {
				mode:"calbox",
				useInline: true,
				calControlGroup: true,
				defaultValue: current.defaultDateValue,
				showInitialValue: true,
				lockInput: false,
				useFocus: true,
				maxDays: reservePeriod,
				buttonIcon: "grid",
				calUsePickers: true,
				afterToday: true,
				beforeToday: false,
				notToday: false,
				calOnlyMonth: false,
				overrideSlideFieldOrder: ['y','m','d','h','i']
			});
			mor.ticket.currentPagePath.fromPath = "ticketPriceQueryResults.html";
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.pickYears = false;
			mor.ticket.currentPagePath.show = false;
			jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
			mor.ticket.util.changePage(goingPath);
		});
	}

	jq("#ticketPriceQueryResultsView").live("pagebeforeshow",function(e,data) {
		mor.ticket.util.initAppVersionInfo();
		var model = mor.ticket.leftTicketQuery;
		var prevId = data.prevPage.attr("id");
		if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
			model.train_date = mor.ticket.currentPagePath.calenderDate;
		}
		jq("#ticketPriceChooseDateBtn").val(model.train_date);
		var cache = mor.ticket.cache;
		jq("#ticketPriceFromToStation").html(cache.getStationNameByCode(model.from_station_telecode) 
				+ "-" + cache.getStationNameByCode(model.to_station_telecode));
	});

	jq("#ticketPriceQueryResultsView").live("pageshow",function() {
		mor.ticket.viewControl.tab3_cur_page = "ticketPriceQueryResults.html";
		var query = mor.ticket.leftTicketQuery;
		searchAndShowResult(query);
	});

	function searchAndShowResult(query) {
		var util = mor.ticket.util;
		var commonParameters = null;
		commonParameters = {
			"train_date" : util.processDateCode(query.train_date),
			"from_station" : query.from_station_telecode,
			"to_station" : query.to_station_telecode,
			"startTimes" : query.start_times,
			"trainType" : query.train_headers
		};

		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "leftTicketPrice"
		};
		var options = {
			onSuccess : requestSucceeded,
			onFailure : util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,options, true);
	}

	function requestSucceeded(result) {
		if (busy.isVisible()) {
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			showSearchResults(invocationResult.queryLeftNewReturnDTO);
		} else {
			jq.mobile.changePage("ticketPriceQuery.html");
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			return;
		}
	}

	function showSearchResults(results) {
		for ( var i = 0; i < results.length; i++) {
			if (results[i].list_Price && results[i].list_Price.length > 0) {
				results[i].pricelist = processTicketPriceInfo(results[i].list_Price);
			} else {
				results[i].pricelist = "";
			}
			results[i].lishiHour = results[i].lishi.substring(0, 2);
			results[i].lishiMinute = results[i].lishi.substring(3, 5);
		}
		jq("#ticketPriceQueryResultsList").html(generatePriceListContent(results)).listview("refresh");
		jq("#ticketPriceQueryResultsList").show();
		initDateBtn();
	}

	function processTicketPriceInfo(priceList) {
		var util = mor.ticket.util;
		var obj = new Array();
		for ( var i = 0;i<priceList.length;i++) {
			var price = priceList[i].split("#");
			var seat_type_name = "";
			if(price[0] == 'w' || price[0] == 'W'){
				seat_type_name = '无座';
			}else{
				seat_type_name = util.getSeatTypeName(price[0]);
			}
			var queryTicketPrice = '¥'+(parseFloat(price[2], 10) / 10).toFixed(1);
			if(queryTicketPrice == '¥0.0'){
				queryTicketPrice = "--";
			}
			obj[i] = {
				type :   util.repalceYpInfoType(seat_type_name),
				uclass : getLiClass(i),
				price : queryTicketPrice
			};
		}
		return obj;
	}
	
	function getLiClass(index) {
		switch (index % 4) {
		case 0:
			return "ui-block-a";
		case 1:
			return "ui-block-b";
		case 2:
			return "ui-block-c";
		case 3:
			return "ui-block-d";
		default:
			return "ui-block-e";
		}
	}
	
	function initDateBtn() {
		var date = mor.ticket.util
				.setMyDate(mor.ticket.leftTicketQuery.train_date);
		var nowDate = mor.ticket.util.getNewDate();
		var reservePeriod = mor.ticket.history.reservePeriod; 
		if (date < nowDate) {
			jq("#ticketPricePrevDateBtn").addClass("ui-disabled");
			jq("#ticketPriceNextDateBtn").removeClass("ui-disabled");
		} else if (date - nowDate > (reservePeriod - 1) * 86400000) {
			jq("#ticketPriceNextDateBtn").addClass("ui-disabled");
			jq("#ticketPricePrevDateBtn").removeClass("ui-disabled");
		} else {
			jq("#ticketPricePrevDateBtn").removeClass("ui-disabled");
			jq("#ticketPriceNextDateBtn").removeClass("ui-disabled");
		}
	}
	
	function registerPrevDateBtnClickHandler() {
		jq("#ticketPricePrevDateBtn").off().on("tap",
			function() {
				var date = mor.ticket.util
						.setMyDate(mor.ticket.leftTicketQuery.train_date);
				var nowDate = mor.ticket.util.getNewDate();
				if (date > nowDate) {
					var preDate = new Date(date.getTime() - 24 * 60
							* 60 * 1000);
					mor.ticket.leftTicketQuery.train_date = preDate
							.format("yyyy-MM-dd");
					jq("#ticketPriceChooseDateBtn").val(mor.ticket.leftTicketQuery.train_date);
					searchAndShowResult(mor.ticket.leftTicketQuery);
				}
				return false;
			});
	}

	function registerNextDateBtnClickHandler() {
		jq("#ticketPriceNextDateBtn").off().on("tap",
			function() {
				var date = mor.ticket.util
						.setMyDate(mor.ticket.leftTicketQuery.train_date);
				var nowDate = mor.ticket.util.getNewDate();
				if (date - nowDate < (mor.ticket.history.reservePeriod - 1) * 86400000) {
					var nextDate = new Date(date.getTime() + 24
							* 60 * 60 * 1000);
					mor.ticket.leftTicketQuery.train_date = nextDate
							.format("yyyy-MM-dd");
					jq("#ticketPriceChooseDateBtn").val(mor.ticket.leftTicketQuery.train_date);
					searchAndShowResult(mor.ticket.leftTicketQuery);							
				}
				return false;
			});
	}

	function registerBackButtonHandler() {
		jq("#ticketPriceQueryResultsBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage("ticketPriceQuery.html");
			return false;
		});
	}
	var ticketPriceListTemplate = "{{~it :ticket:index}}"
		+ "<li data-index='{{=index}}' data-iconshadow='false' data-icon='none'>"
		+ "<a><div class='ticket-train-row'>"
		+ "<div class='ticket-train-code-col'>{{=ticket.station_train_code}}</div>"
		+ "<div class='ticket-android-fix'><div class='ticket-train-code-txt'><div class='ui-grid-b'>"
		+ "<div class='ui-block-a'>"
		+ "<div class='ticket-station-div'>"
		+ "{{ if(ticket.from_station_telecode == ticket.start_station_telecode) { }}"
		+ "<div class='ticket-station-div_from_station'>始</div>"
		+ "{{ }else{ }}"
		+"<div class='ticket-station-div_pass_station'>过</div>"
		+ "{{ } }}"
		+ "{{=ticket.from_station_name}}</div>"
		+ "<div class='ticket-time'>{{=ticket.start_time}}</div>"
		+ "</div>"
		+ "<div class='ui-block-b'>"
		+ "<div class='ticket-right-arrow'></div>"
		+ "<div class='ticket-duration'>"
		+ "{{ if(ticket.lishiHour != '00'){ }}"
		+ "{{=ticket.lishiHour}}小时"
		+ "{{ } }}"
		+ "{{=ticket.lishiMinute}}分"
		+ "</div>"
		+ "</div>"
		+ "<div class='ui-block-c'>"
		+ "<div class='ticket-station-div'><span style='text-align:left;display:inline-block;width:80px;vertical-align:top'>"
		+ "{{ if(ticket.to_station_telecode == ticket.end_station_telecode) { }}"
		+"<div class='ticket-station-div_to_station'>终</div>"
		+ "{{ }else{ }}"
		+"<div class='ticket-station-div_pass_station'>过</div>"
		+ "{{ } }}"
		+ "{{=ticket.to_station_name}}</span></div>"
		+ "<div class='ticket-time'>{{=ticket.arrive_time}}</div>"
		+ "</div>"
		+ "</div>"
		+ "</div></div></div>"
		+ "<div class='ui-grid-c ticket-info-grid'>"
		+ "{{ for(var i=0;i<ticket.pricelist.length;i++) { }}"
		+ "<span class='{{=ticket.pricelist[i].uclass}}' ><span>{{=ticket.pricelist[i].type}}:</span>{{=ticket.pricelist[i].price}}</span>"
		+ "{{ } }}"
		+"</div>"
		+ "</a>"
		+ "</li>"
		+ "{{~}}";
	var generatePriceListContent = doT.template(ticketPriceListTemplate);
	
})();