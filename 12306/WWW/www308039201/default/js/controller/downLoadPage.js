
/* JavaScript content from js/controller/downLoadPage.js in folder common */
(function() {
	jq("#downLoadPageView").live("pageshow", function(){
		if(mor.system.update == "Y"){
			var sUserAgent = navigator.userAgent.toLowerCase();
			var isPadOs = sUserAgent.match(/ipad/i) == "ipad";
			var isIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
			var isIPhone = isPadOs || isIphoneOs;
			var isAndroid = sUserAgent.match(/android/i) == "android";
			if(isAndroid){ 
				navigator.app.loadUrl('http://dynamic.12306.cn/otn/appDownload/androiddownload', {openExternal:true});
			}
			if(isIPhone){
				window.open("https://itunes.apple.com/cn/app/tie-lu12306/id564818797?mt=8", "_system");
			}
		}
	});
})();