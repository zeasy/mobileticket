
/* JavaScript content from js/controller/orderManager.js in folder common */
(function(){
	var imageBox = true;
	jq.extendModule("mor.ticket.reportTicketInfo", {
		ticketInfo : ""
	});
	jq.extendModule("mor.ticket.newPassCodeStationTrainCode", {
		stationTrainCode : ""
	});
	jq.extendModule("mor.ticket.orderManager", {
		captchaDefaultSize : {width:293,height:190,radio:1,offset:{x:0,y:30},logoSize:{width:24,height:24},reloadSize:{width:54,height:24}},//图片验证码
		captchaClientSize :{offset:{},logoSize:{},reloadSize:{}},//width height radio offset
		captchaLogoDoms:[],
		captchaImageResult:"",
		capchaNewOrOldFlag:"",
		orderTicketList:[],
		ticketMode:"",
		orderParameters:{},
		ifConfirmedPassenger:false,
		totalTicketNum:0,//欲购买票数
		preTicketNum:0,//往程票数
		queueMessage:'',//需要排队时，队列信息
		paymentOrder:{
			myTicketList:[]
		},
		clearTicketList :function(){
			if(mor.ticket.viewControl.bookMode != "fc"){
				this.orderTicketList=[];
			}		
		},		
		clearFarelist :function(){
			
			
				var passengers = mor.ticket.passengersCache.passengers;
				for(var i =0;i<passengers.length;i++){
					mor.ticket.passengersCache.passengers[i].checked =0;
					
				}
				mor.ticket.passengerList = [];
	
				
		},		
		
		
		
		confirmedTicket:function(){},
		
		addTicket:function(ticket){this.orderTicketList.push(ticket);},
		
		getTicketList:function(){return this.orderTicketList;},
		
		setTotalTicketNum:function(length){			
			if(mor.ticket.viewControl.bookMode === "fc"){
				this.totalTicketNum = this.preTicketNum + length;
			}else {
				this.totalTicketNum = length;
				if(mor.ticket.viewControl.bookMode === "dc"){
					this.preTicketNum = length;
				}
			}
		},
		
		getActualTotalTicketPrice:function(){
			orderTicketList = this.orderTicketList;
			var actualTotalTicketPrice=0;
			for(var i=0; i<orderTicketList.length; i++){
				actualTotalTicketPrice += parseFloat(orderTicketList[i].ticket_price);
			}
			return actualTotalTicketPrice;
		},
		
/*      直接调用confirmPassengerInfo接口，此方法不用了
 * 		getQueueCountMessage:function(parameters){
        	mor.ticket.orderManager.orderParameters=parameters;
			var invocationData = {
				adapter: "CARSMobileServiceAdapterV2",
				procedure: "getQueueCountMessage",
				"parameters": [parameters]
			};
        	var invocationData = {
    				adapter: "CARSMobileServiceAdapterV2",
    				procedure: "getQueueCountMessage"
    			};
			var options = {
					onSuccess: mor.ticket.orderManager.getQueueCountSucceeded,
					onFailure: mor.ticket.orderManager.orderManagerRequestFailure
			};
			
//			WL.Client.invokeProcedure(invocationData, options);
			mor.ticket.util.invokeWLProcedure(parameters, invocationData, options);
//			busy.show();
        },
        
        getQueueCountSucceeded:function(result){
        	if(busy.isVisible()){
        		busy.hide();
        	}
    		var invocationResult = result.invocationResult;
    		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
    			if(invocationResult.queueflag=="1"){
    				WL.SimpleDialog.show(
    	    				"温馨提示", 
    	    				invocationResult.queueMsg, 
    	    				[ {text : '确定', handler: function(){}}]
    	    		);  
    			}else{
    				WL.SimpleDialog.show(
    	    				"温馨提示", 
    	    				invocationResult.queueMsg, 
    	    				[ {text : '取消', handler: function(){}},
    	    				  {text : '确定', handler: function(){mor.ticket.orderManager.confirmPassengerInfo();}} ]
    	    		);  
    			}
    		}else {
    			mor.ticket.orderManager.refreshCaptcha2();
    			mor.ticket.util.alertMessage(invocationResult.error_msg);
    		}	
        },*/
        
       confirmPassengerInfo:function(parameters) {
    		//确认订单
    		var mode = mor.ticket.viewControl.bookMode;
    		var invocationData = null;
    		
			
			
    		if (mode === "wc"){
    			/*invocationData = {
    					adapter: "CARSMobileServiceAdapterV2",
    					procedure: "confirmPassengerInfoGo",
    					"parameters": [parameters]
    			};*/
    			invocationData = {
    					adapter: mor.ticket.viewControl.adapterUsed,
    					procedure: "confirmPassengerInfoGo"
    			};
    		}else if(mode === "fc"){
				/*invocationData = {
						adapter: "CARSMobileServiceAdapterV2",
						procedure: "confirmPassengerInfoBack",
						"parameters": [parameters]
				};*/
    			invocationData = {
						adapter: mor.ticket.viewControl.adapterUsed,
						procedure: "confirmPassengerInfoBack"
				};
    		} else if(mode === "gc"){
    			if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
    				parameters.dynamicProp = "0+0+0+B";
    			}else{
    				parameters.dynamicProp = "0+0+0+0";
    			}
    			/*invocationData = {
    					adapter: "CARSMobileServiceAdapterV2",
    					procedure: "requestChangeTicket",
    					"parameters": [parameters]
    				};*/
    			invocationData = {
    					adapter: mor.ticket.viewControl.adapterUsed,
    					procedure: "requestChangeTicket"
    				};
    		} else {
    			/*invocationData = { 
    					adapter: "CARSMobileServiceAdapterV2",
    					procedure: "confirmPassengerInfoSingle",
    					"parameters": [parameters]
    				};*/
    			invocationData = {
    					adapter: mor.ticket.viewControl.adapterUsed,
    					procedure: "confirmPassengerInfoSingle"
    				};
    		}
    		var failureFunction;
			
    		if(mode === "fc"){
    			failureFunction = mor.ticket.orderManager.FCRequestFailureHandler;
    		}else{
			    failureFunction = mor.ticket.orderManager.orderManagerRequestFailure;	
			}
			
    		var options = {
    				onSuccess: mor.ticket.orderManager.requestOrderSucceeded,
    				onFailure: failureFunction
    		};		
			
			
//    		WL.Client.invokeProcedure(invocationData, options);	
    		mor.ticket.util.invokeWLProcedure(parameters, invocationData, options);
    		
//    		busy.show();
    	},
    	
    	//返程情况下调用失败方法重写
    	FCRequestFailureHandler:function(){    		
    		if(busy.isVisible()){
    			busy.hide();
    		}
    		mor.ticket.viewControl.tab1_cur_page=vPathViewCallBack()+"MobileTicket.html";
			WL.SimpleDialog.show(
				"温馨提示", 
				"哎呀，网络好像有问题，请检查网络连接", 
				[ {text : '确定', handler: function(){jq.mobile.changePage("queryOrder.html");}}]
			);	
    	},
    	
    	requestOrderSucceeded:function(result){
    		if(busy.isVisible()){
    			busy.hide();
    		}
    		var invocationResult = result.invocationResult;
    		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
    			if(!mor.ticket.util.tripConflicts(invocationResult)){
    				return false;
    			}
        		var mode = mor.ticket.viewControl.bookMode;
    			if(invocationResult.queueFlag==0){
    			//购票成功，无需排队 
    				mor.ticket.orderManager.clearTicketList();
					mor.ticket.orderManager.clearFarelist();
					
    				var pay_limit_time;//get支付倒计时
    				if(mode === "gc"){
    					for(var i=0; i<invocationResult.orderList.length; i++){	
        					invocationResult.orderList[i].station_train_code=mor.ticket.currentTicket.station_train_code;	        					
        					mor.ticket.orderManager.addTicket(invocationResult.orderList[i]);
        				}
    					pay_limit_time = mor.ticket.orderManager.orderTicketList[0].myTicketList[0].pay_limit_time;
    				}
    				
    				//对于返程订票支付倒计时，需要显示的是orderTicketList中最后的一张票的pay_limit_time
        			else if(mode == "fc"){
        				for(var i=0; i<invocationResult.ticketResult.length; i++){	
            				invocationResult.ticketResult[i].station_train_code=mor.ticket.currentTicket.station_train_code;
            				invocationResult.ticketResult[i].isRoundTripFlag = true;
            				mor.ticket.orderManager.addTicket(invocationResult.ticketResult[i]);
            			}
        				pay_limit_time = mor.ticket.orderManager.orderTicketList[mor.ticket.orderManager.orderTicketList.length-1].pay_limit_time;
        			}
    				else {
    					for(var i=0; i<invocationResult.ticketResult.length; i++){	
        					invocationResult.ticketResult[i].station_train_code=mor.ticket.currentTicket.station_train_code;
        					mor.ticket.orderManager.addTicket(invocationResult.ticketResult[i]);
        				}
    					//pay_limit_time = mor.ticket.orderManager.orderTicketList[0].myTicketList[0].pay_limit_time;
    					//mod by yiguo
    					pay_limit_time = mor.ticket.orderManager.orderTicketList[0].pay_limit_time;
    					// date_back add zjk
    					var date = mor.ticket.util.setMyDate(mor.ticket.leftTicketQuery.train_date);
    					var date_back = new Date(date.setDate(date.getDate() + 1));
    					mor.ticket.leftTicketQuery.train_date_back = date_back.format("yyyy-MM-dd");
    				}	    				
    				//显示支付倒计时
        			mor.ticket.paytimer.endTime = mor.ticket.util.setMyDate3(pay_limit_time);
        			
    				var payment=mor.ticket.payment;
    				payment['interfaceName'] = invocationResult.interfaceName;
    				payment['interfaceVersion'] = invocationResult.interfaceVersion;
    				payment['tranData'] = invocationResult.tranData;
    				payment['merSignMsg'] = invocationResult.merSignMsg;
    				payment['appId'] = invocationResult.appId;
    				payment['transType'] = invocationResult.transType;
    				payment['epayurl'] = invocationResult.epayurl;
                	
    				jq.mobile.changePage("orderDetails.html");
    			
    			}else if(invocationResult.queueFlag==1&&invocationResult.queueCode==1){
    				mor.ticket.orderManager.queueMessage = invocationResult.queueMessage;
    	            //排队，获取排队时间
    				mor.ticket.orderManager.getWaitTimeProcedure();
    			}else if(invocationResult.queueFlag==1){
    				//排队失败  
    				mor.ticket.orderManager.refreshCaptchaImg2();
    				mor.ticket.util.alertMessage(invocationResult.queueMessage);
    			} 
    		} else {
    			//调用失败
    			if(invocationResult.succ_flag=="-1" && mor.ticket.viewControl.bookMode === "fc") {
    				WL.SimpleDialog.show(
    						"温馨提示", 
    						"哎呀，网络好像有问题，请检查网络连接", 
    						[ {text : '确定', handler: function() {jq.mobile.changePage("queryOrder.html");}}]
    					); 				
        		}else{
        			mor.ticket.orderManager.refreshCaptchaImg2();
        			mor.ticket.util.alertMessage(invocationResult.error_msg);
        		}
    		}
    	
    	},
    	
    	getWaitTimeProcedure:function(){
    		var util = mor.ticket.util;  
    		var viewControl = mor.ticket.viewControl;
    		var mode = viewControl.unFinishedOrderTourFlag;
    		if(!mode){
    			mode = viewControl.bookMode;
    		}
			var parameters = util.prepareRequestCommonParameters({
				tourFlag:mode
			});
			/*var invocationData = {
					adapter: "CARSMobileServiceAdapterV2",
					procedure: "getWaitTime",
					"parameters": [parameters]
				};*/
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "getWaitTime"
				};
				
				var options = {
						onSuccess: mor.ticket.orderManager.getWaitTimeSucceeded,
						onFailure: mor.ticket.orderManager.orderManagerRequestFailure
				};
				
//				busy.show();
//				WL.Client.invokeProcedure(invocationData, options);
				mor.ticket.viewControl.show_busy = false;
				mor.ticket.util.invokeWLProcedure(parameters, invocationData, options);
    	},
    	
    	getWaitTimeSucceeded:function(result){

    		if(busy.isVisible()){
    			busy.hide();
    		}
			
			//mor.ticket.orderManager.clearFarelist()
			var invocationResult = result.invocationResult;
			if (!mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				mor.ticket.util.alertMessage(invocationResult.error_msg);
				return false;
			}
			if(!mor.ticket.util.tripConflicts(invocationResult)){
				return false;
			}
    		var waitTime=invocationResult.waitTime;
    		var interval=30;
    		if(waitTime>=60){
    			interval=30;
    		}else if(waitTime<60&&waitTime>10){
    			interval=interval/2;
    		}else if(waitTime<10){
    			interval=5;
    		}
    		mor.ticket.poll.timer = jq.timer(
    				function() {
    					mor.ticket.orderManager.pollServerForWaitTime();
    				},	interval*1000, true);
    		
    		var util=mor.ticket.util;
    		var viewControl = mor.ticket.viewControl;
    		var mode = viewControl.unFinishedOrderTourFlag;
    		//alert(1);
    		if(!mode){
    			//alert(2);
    			if(util.isHybrid()){
    				//alert(3);
    			var isDisplayGoNoFinishedOrder = window.ticketStorage.getItem("isDisplayGoNoFinishedOrder");
    			var msg = "";
    			var parameters ={};
    			if(waitTime<=5){
    				msg=mor.ticket.orderManager.queueMessage + "\n";
    				parameters={ "message": msg,
    						  "title":"温馨提示"};
    				if(isDisplayGoNoFinishedOrder == "Y"){
    					parameters={ "message": msg,
      						  "buttonText":"进入未完成订单",
      						  "title":"温馨提示"};
    				}
    	          window.plugins.childBrowser.openDialog(parameters); 
    	          mor.ticket.poll.endTime = endTime;
      	    	  mor.ticket.poll.intervalId=setInterval(function(){;}, 1000);     	          
    	          return;
    			}else{
    				msg=mor.ticket.orderManager.queueMessage + "\n最新预估排队等待时间:";
    			}
      			parameters={ "message": msg,
      						"title":"温馨提示"};
      			if(isDisplayGoNoFinishedOrder == "Y"){
					parameters={ "message": msg,
  						  "buttonText":"进入未完成订单",
  						  "title":"温馨提示"};
				}
      	          window.plugins.childBrowser.openDialog(parameters); 
      	          var endTime = new Date(Date.now()+waitTime*1000);
      	          mor.ticket.poll.endTime = endTime;
      	          //var endTime=mor.ticket.poll.endTime;
      	    	  //endTime = new Date(Date.now()+waitTime*1000);
      	    	  mor.ticket.poll.intervalId=setInterval(function(){
      	    		  jq('<div>').each(function(){
	      		          var nowTime = new Date();
	      		          //endTime=mor.ticket.poll.endTime;
	      		          var nMS=mor.ticket.poll.endTime.getTime() - nowTime.getTime() ;
	      		          var myD=Math.floor(nMS/(1000 * 60 * 60 * 24));
	      		          var myM=Math.floor(nMS/(1000*60)) % 60;
	      		          var myS=Math.floor(nMS/1000) % 60;
	      		          var str;
	      		          if(myD>= 0){
                            str = myM+"分"+myS+"秒\n";
	      		  			window.plugins.childBrowser.updateDialog(str);
	      		          }else{
	      		  			str = "";	
	      		  		  }
      	    		  });
      	    	  }, 1000);     	          
	      		}else{
	      			//alert(4);
	      			jq("#counter_line").css('display','block'); 
	      			var endTime=mor.ticket.poll.endTime;
	      			endTime = new Date(Date.now()+waitTime*1000);
	      			mor.ticket.poll.intervalId=setInterval(function(){
	      		    	  jq("#waitTime").each(function(){
	      		          var obj = jq(this);
	      		          var nowTime = new Date();
	      		          var nMS=endTime.getTime() - nowTime.getTime() ;
	      		          var myD=Math.floor(nMS/(1000 * 60 * 60 * 24));
	      		          //var myH=Math.floor(nMS/(1000*60*60)) % 24;
	      		          var myM=Math.floor(nMS/(1000*60)) % 60;
	      		          var myS=Math.floor(nMS/1000) % 60;
	      		         // var myMS=Math.floor(nMS/100) % 10;
	      		          var str;
	      		          if(myD>= 0){
	      		  			str = myM+"分"+myS+"秒\n";
	      		          }else{
	      		  			str = "";	
	      		  		}
	      		  		obj.html(str);});}, 1000);
	      		}
    		} else{
    			//alert(5);
    			jq(".unfinishedOrderWaitTimeContain").show(); 
      			//var endTime=mor.ticket.poll.endTime;
      			//endTime = new Date(Date.now()+waitTime*1000);
    			var endTime = new Date(Date.now()+waitTime*1000);
     	        mor.ticket.poll.endTime = endTime;
      			mor.ticket.poll.intervalId=setInterval(function(){
      		    	  jq(".unfinishedOrderWaitTime").each(function(){
      		          var obj = jq(this);
      		          var nowTime = new Date();
      		          //var nMS=endTime.getTime() - nowTime.getTime() ;
      		          var nMS=mor.ticket.poll.endTime.getTime() - nowTime.getTime() ;
      		          var myD=Math.floor(nMS/(1000 * 60 * 60 * 24));
      		          var myM=Math.floor(nMS/(1000*60)) % 60;
      		          var myS=Math.floor(nMS/1000) % 60;
      		          var str;
      		          if(myD>= 0){
      		  			str = myM+"分"+myS+"秒\n";
      		          }else{
      		  			str = "";	
      		  		}
      		  		obj.html(str);});}, 1000);
    		}    	
    	},
    	
    	pollServerForWaitTime: function(){

    		var util = mor.ticket.util;
    		var viewControl = mor.ticket.viewControl;
    		var mode = viewControl.unFinishedOrderTourFlag;
    		if(!mode){
    			mode = viewControl.bookMode;
    		}
    		var parameters = util.prepareRequestCommonParameters({
    			tourFlag:mode
    		});
    		/*var invocationData = {
    				adapter: "CARSMobileServiceAdapterV2",
    				procedure: "getWaitTime",
    				"parameters": [parameters]
    			};*/
    		
    		var invocationData = {
    				adapter: mor.ticket.viewControl.adapterUsed,
    				procedure: "getWaitTime"
    			};
			var options = {
					onSuccess: mor.ticket.orderManager.pollServerSucceeded,
					onFailure: mor.ticket.orderManager.orderManagerRequestFailure
			};
			
			if(mor.ticket.poll.timer.isActive){
//				WL.Client.invokeProcedure(invocationData, options);
//				busy.show();
				mor.ticket.viewControl.show_busy = false;
				mor.ticket.util.invokeWLProcedure(parameters, invocationData, options);
			}
    	
    	},
        
    	pollServerSucceeded :function(result){
    		if(busy.isVisible()){
    			busy.hide();
    		}
    		if(!mor.ticket.poll.timer.isActive){
    			return false;
    		}
    		var invocationResult = result.invocationResult;
    		if (!mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				mor.ticket.util.alertMessage(invocationResult.error_msg);
				mor.ticket.poll.timer.stop();
                window.plugins.childBrowser.closeDialog();
    			clearInterval(mor.ticket.poll.intervalId);
				return false;
			}
    		//行程冲突了且提示用户举报的情况
			if(!mor.ticket.util.tripConflicts(invocationResult)){
				mor.ticket.poll.timer.stop();
                window.plugins.childBrowser.closeDialog();
    			clearInterval(mor.ticket.poll.intervalId);
				return false;
			}
			
    		var viewControl = mor.ticket.viewControl;
    		var mode = viewControl.unFinishedOrderTourFlag;
    		if(invocationResult.succ_flag !=1){			
    			mor.ticket.poll.timer.stop();
                window.plugins.childBrowser.closeDialog();
    			clearInterval(mor.ticket.poll.intervalId);
    			if(!mode){
    				jq("#counter_line").hide();
    				mor.ticket.orderManager.refreshCaptchaImg2();
        			WL.SimpleDialog.show(
        					"温馨提示", 
        					invocationResult.error_msg, 
        					[ {text : '确定', handler: function(){}}]
        			);
    			}else{
    				mor.ticket.orderManager.unfinishedOrderRequest("");
    			}
    		}else{		
    			if(invocationResult.waitTime==-1){
    				//finally get ticket successfully
    				mor.ticket.poll.timer.stop();

    				//call confirmSingleSuc			
    				var util = mor.ticket.util;
    				var parameters = util.prepareRequestCommonParameters({
    					orderId:invocationResult.orderId
    				});
    				
    				var viewControl = mor.ticket.viewControl;
    	    		var mode = viewControl.unFinishedOrderTourFlag;
    	    		if(!mode){
    	    			mode = viewControl.bookMode;
    	    		}
    				var invocationData;
    				if(mode == "gc"){
    					/*invocationData = {
        						adapter: "CARSMobileServiceAdapterV2",
        						procedure: "confirmResignSuc",
        						"parameters": [parameters]
        				};*/
    					invocationData = {
        						adapter: mor.ticket.viewControl.adapterUsed,
        						procedure: "confirmResignSuc"
        				};
    				}else {
    					/*invocationData = {
        						adapter: "CARSMobileServiceAdapterV2",
        						procedure: "confirmSingleSuccess",
        						"parameters": [parameters]
        				};*/
    					invocationData = {
        						adapter: mor.ticket.viewControl.adapterUsed,
        						procedure: "confirmSingleSuccess"
    					};
    				}				
    					
					var options = {
							onSuccess: mor.ticket.orderManager.confirmSingleSucceeded,
							onFailure: mor.ticket.orderManager.orderManagerRequestFailure 
					};
//					busy.show();
//					WL.Client.invokeProcedure(invocationData, options);
					mor.ticket.viewControl.show_busy = false;
					mor.ticket.util.invokeWLProcedure(parameters, invocationData, options);
    	
    			}else if(invocationResult.waitTime>0){
    				//得到新的等待时间
    				//alert("wait time: "+invocationResult.waitTime);
    				//set new interval and update endtime
    				//endTime = new Date(Date.now()+waitTime*1000);
    				var waitTime=invocationResult.waitTime;
    				var interval=30;
    				if(waitTime>=60){
    					interval=30;
    				}else if(waitTime<60&&waitTime>10){
    					interval=interval/2;
    				}else if(waitTime<10){
    					interval=5;
    				}
    				mor.ticket.poll.timer.set({time:interval*1000});
    				mor.ticket.poll.endTime = new Date(Date.now()+waitTime*1000);
    			}
    		}
    	
    	},
    	
    	confirmSingleSucceeded :function(result){
    		if(busy.isVisible()){
    			busy.hide();
    		}
    		var intervalId = mor.ticket.poll.intervalId;
    		if(intervalId != ""){
    			mor.ticket.poll.timer.stop();
    			clearInterval(intervalId);
    		}
    		var viewControl = mor.ticket.viewControl;
    		var mode = viewControl.unFinishedOrderTourFlag;
    		if(!mode){    			
    			window.plugins.childBrowser.closeDialog();
    		}	
    		var invocationResult = result.invocationResult;		
    		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
    			if(!mode){
    				mode = viewControl.bookMode;

    				mor.ticket.orderManager.clearTicketList();
					mor.ticket.orderManager.clearFarelist();

        			var pay_limit_time;//显示支付倒计时		
        			if(mode == "gc"){
        				for(var i=0; i<invocationResult.orderList.length; i++){	
            				invocationResult.orderList[i].station_train_code=mor.ticket.currentTicket.station_train_code;			
            				mor.ticket.orderManager.addTicket(invocationResult.orderList[i]);            				
            			}
        				pay_limit_time = mor.ticket.orderManager.orderTicketList[0].myTicketList[0].pay_limit_time;
        			}
        			
        			//对于返程订票支付倒计时，需要显示的是orderTicketList中最后的一张票的pay_limit_time
        			else if(mode == "fc"){
        				for(var i=0; i<invocationResult.ticketResult.length; i++){	
            				invocationResult.ticketResult[i].station_train_code=mor.ticket.currentTicket.station_train_code;
            				invocationResult.ticketResult[i].isRoundTripFlag = true;
            				mor.ticket.orderManager.addTicket(invocationResult.ticketResult[i]);   
            			}
        				pay_limit_time = mor.ticket.orderManager.orderTicketList[mor.ticket.orderManager.orderTicketList.length-1].pay_limit_time;
        			}else {
        				for(var i=0; i<invocationResult.ticketResult.length; i++){	
            				invocationResult.ticketResult[i].station_train_code=mor.ticket.currentTicket.station_train_code;			
            				mor.ticket.orderManager.addTicket(invocationResult.ticketResult[i]);            				
            			}
        				pay_limit_time = mor.ticket.orderManager.orderTicketList[0].pay_limit_time;
        				// date_back add zjk
    					var date = mor.ticket.util.setMyDate(mor.ticket.leftTicketQuery.train_date);
    					var date_back = new Date(date.setDate(date.getDate() + 1));
    					mor.ticket.leftTicketQuery.train_date_back = date_back.format("yyyy-MM-dd");
        			}
        			mor.ticket.paytimer.endTime = mor.ticket.util.setMyDate3(pay_limit_time);
        			
        			var payment=mor.ticket.payment;
        			payment['interfaceName'] = invocationResult.interfaceName;
        			payment['interfaceVersion'] = invocationResult.interfaceVersion;
        			payment['tranData'] = invocationResult.tranData;
        			payment['merSignMsg'] = invocationResult.merSignMsg;
        			payment['appId'] = invocationResult.appId;
        			payment['transType'] = invocationResult.transType;
        			payment['epayurl'] = invocationResult.epayurl;
        			mor.ticket.viewControl.isNeedRefreshUnfinishedOrder = true;
        			
        			jq("#counter_line").hide(); 
        			jq.mobile.changePage("orderDetails.html");
    			}else{
    				mor.ticket.orderManager.unfinishedOrderRequest(invocationResult.ticketResult[0].sequence_no);			    				    				
    			}   			
    		}
    	},
    	
    	unfinishedOrderRequest:function(sequence_no){
    		var util = mor.ticket.util;
			var commonParameters = {	
				'sequence_no':sequence_no,
				'query_class': '1'
			};
			
			/*var invocationData = {
					adapter: "CARSMobileServiceAdapterV2",
					procedure: "queryOrder",
					parameters: [commonParameters]
			};*/
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "queryOrder"
			};
			
			var options =  {
					onSuccess: mor.ticket.orderManager.unfinishedOrderRequestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
//			WL.Client.invokeProcedure(invocationData, options);
//			busy.show(); 
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
    	},
    	
    	unfinishedOrderRequestSucceeded:function(result){
    		if(busy.isVisible()){
    			busy.hide();
    		}
    		var invocationResult = result.invocationResult;	
    		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
    			//行程冲突了且提示用户举报的情况
            	mor.ticket.util.tripConflicts(invocationResult);
    			if(invocationResult.orderList.length && invocationResult.orderList[0].order_date != undefined){
    				//init queryOrder queryOrderList
    				mor.ticket.queryOrder.queryOrderList=[];
    				mor.ticket.queryOrder.originPaidQrderList=[];
    				mor.ticket.queryOrder.setUnfinishedOrderList(invocationResult.orderList);
        			mor.ticket.queryOrder.hasChangeTicket();
    				mor.ticket.queryOrder.updateCurrentQueryOrder(invocationResult.orderList[0]);				
        			mor.ticket.viewControl.unFinishedOrderTourFlag = "";
    				jq.mobile.changePage("orderList.html",{reloadPage:true,transition:"none"});
    			}else {//从后台等不到相关数据时，处理。 
    				//mor.ticket.viewControl.isNeedRefreshUnfinishedOrder = true;
    				//mor.ticket.util.changePage("queryOrder.html");
    				mor.ticket.util.alertMessage("未查询到您的订单处理状态！"); 
    				jq.mobile.changePage("queryOrder.html");
    				setTimeout(function(){
    					jq("#refreshUnfinishedorder").trigger("tap");
    				},1000);
    			}
    		}else {
    			mor.ticket.util.alertMessage(invocationResult.error_msg);
    		}
    	},
    	
    	orderManagerRequestFailure:function(){
    		mor.ticket.orderManager.refreshCaptchaImg2();
//			mor.ticket.util.creatCommonRequestFailureHandler();			
    		if(busy.isVisible()){
				busy.hide();
			}
			if(result && result.status==200){
				if(result.invocationResult.responseID=="26"){//调用otsmobile失败
					WL.SimpleDialog.show(
							"温馨提示", 
							"系统忙,请稍后再试。", 
							[ {text : '确定', handler: function() {}}]
						);
			    }else{
			    	 WL.SimpleDialog.show(
								"温馨提示", 
								"哎呀，网络好像有问题，请检查网络连接！", 
								[ {text : '确定', handler: function() {}}]
							);   	
			    }
			}
			else{
				WL.Device.getNetworkInfo(function (networkInfo) {
					if(networkInfo.isNetworkConnected=="false"){
						WL.SimpleDialog.show(
							"温馨提示", 
							"哎呀，您的网络有问题，请检查网络连接。", 
							[ {text : '确定', handler: function() {}}]
						);	
					}else{
						WL.SimpleDialog.show(
							"温馨提示", 
							"哎呀，您的网络好像有问题，请检查网络连接。", 
							[ {text : '确定', handler: function() {}}]
						);	
					}
				});	
			}
		},
		
		//图片验证码--begin
		captchaTabHandle:function(e){
			e.stopImmediatePropagation();
			if(!imageBox){
				return false;
			}
			imageBox = false;
			setTimeout(function(){
				imageBox = true;
			},300);
            var obj = {};
            if (e.offsetX && e.offsetY) {
                obj.X = e.offsetX;
                obj.Y = e.offsetY;
            }else{
                return;
            }
            var clientOffset = mor.ticket.orderManager.captchaClientSize.offset;
            obj.X -= clientOffset.x;
            obj.Y -= clientOffset.y;
            if (obj.X <= 0 || obj.Y <= 0) {
                return;
            }
            if(jq('.captchaLogo').length >= 20){
            	return;
            }
            jq('<img src="../images/captchaLogo.png" alt="ok" class="captchaLogo" data-pos="'+obj.X+','+obj.Y+'" style="position:absolute;left:'+(obj.X - 10)+'px;top:'+(obj.Y  -10 + clientOffset.y)+'px"/>').on('tap',function(){
            	e.stopImmediatePropagation();
    			if(!imageBox){
    				return false;
    			}
    			imageBox = false;
    			setTimeout(function(){
    				imageBox = true;
    			},300);
            	jq(this).remove();
            }).appendTo(jq('#captchaDivNew'));
            
		 },
		 captchaImgReload:function(){
			 jq('.captchaLogo').each(function(i,v){
				 jq(v).remove();
			 });
		 },
		 captchaDataStringify:function(){
			 var logs = jq('.captchaLogo'),result;
			 if(!logs.length){
				 return '';
			 }
			 logs.each(function(i,v){
				 if(i==0){
					 result = jq(v).data('pos');//.remove().data('pos');
				 }else{
					 result += ","+jq(v).data('pos');//.remove().data('pos');
				 }
			 });
			 mor.ticket.orderManager.captchaImageResult = result;
			 return result;
		 },
		captchaImgInit:function(){
			 var clientWidth =document.documentElement.clientWidth,defaultSize = mor.ticket.orderManager.captchaDefaultSize,clientSize = mor.ticket.orderManager.captchaClientSize;   
			 if(clientWidth<defaultSize.width){
		    	clientSize.radio = clientWidth/defaultSize.width;
		    	clientSize.offset.x = parseInt(defaultSize.offset.x * clientSize.radio);
		    	clientSize.offset.y = parseInt(defaultSize.offset.y * clientSize.radio);
		    	clientSize.width = clientWidth;
		    	clientSize.height = parseInt(defaultSize.height * clientSize.radio);
		    	clientSize.logoSize ={width:parseInt(defaultSize.logoSize.width * clientSize.radio),height:parseInt(deafultSize.logoSize.height * clientSize.radio)};
		    	clientSize.reloadSize = {width:parseInt(defaultSize.reloadSize.width * clientSize.radio),height:parseInt(defaultSize.reloadSize.height * clientSize.radio)};
			}else{
				clientSize.width = defaultSize.width;
				clientSize.height = defaultSize.height;
				clientSize.radio = defaultSize.radio;
				clientSize.offset.x = defaultSize.offset.x;
		    	clientSize.offset.y =defaultSize.offset.y;
		    	clientSize.logoSize = defaultSize.logoSize;
		    	clientSize.reloadSize = defaultSize.reloadSize;
			}
			jq('#captchaDivNew').add(jq('#captchaImg1')).css({'width':clientSize.width+"px",'height':clientSize.height+"px"});
			jq('.captchaLogo').css({'width':clientSize.logoSize.width+'px','height':clientSize.logoSize.height+'px'});
			jq('#cpathcaReload').css({'width':clientSize.reloadSize.width+'px','height':clientSize.reloadSize.height+'px'});
		},
		
		refreshCaptchaImg:function(){
			jq("#captchaImg1").hide();
				commonParameters = {
						"rand":"randp",
						"module":"passenger",
						"location_code":mor.ticket.currentTicket.location_code
					};
			
			
			var invocationData = {
    				adapter: mor.ticket.viewControl.adapterUsed,
    				procedure: "getPassCodeNew"
    		};
    		var options = {
    				onSuccess: mor.ticket.orderManager.requestCaptchaImgSucceeded,
    				onFailure: mor.ticket.util.creatCommonRequestFailureHandler()
    		};
//    		busy.show();
//    		WL.Client.invokeProcedure(invocationData, options);
    		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
    		return false;
		},
		requestCaptchaImgSucceeded: function(result){
    		if(busy.isVisible()){
    			busy.hide();
    		}
    		var invocationResult = result.invocationResult;
    		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
    			mor.ticket.orderManager.capchaNewOrOldFlag = invocationResult.model;
    			if(invocationResult.model == "2"){
    				jq("#captchaDivOld").show();
    				jq("#captchaDivNew").hide();
    				jq("#captchaImg").attr("src", "data:image/gif;base64," + invocationResult.passcode);
        			mor.ticket.history.checkCodeImg1="data:image/gif;base64," + invocationResult.passcode;
        			jq("#captchaImg").show();
        			jq("#refreshImgBtn").hide();
    				jq("#captchaImg1").hide();
    			}else{
    				jq("#captchaDivOld").hide();
    				jq("#captchaDivNew").show();
    				mor.ticket.orderManager.captchaImgReload();
    				jq("#captchaImg1").on("click",mor.ticket.orderManager.captchaTabHandle);
    				jq("#captchaImg1").attr("src", "data:image/gif;base64," + invocationResult.passcode);
    				mor.ticket.history.checkCodeImg1="data:image/gif;base64," + invocationResult.passcode;
        			jq("#refreshImgBtn").show();
    				jq("#captchaImg1").show();
    			}
    			if(jq("#passengersView .ui-content").attr("data-iscroll")!=undefined){
    				jq("#passengersView .ui-content").iscrollview("refresh");
    			}
    		} else {
    			mor.ticket.util.alertMessage(invocationResult.error_msg);
    		}
    	},
		
    	refreshCaptchaImg2:function(){
    		jq("#captchaImg1").hide();
			var commonParameters = {
					"rand":"randp",
					"module":"passenger",
					"location_code":mor.ticket.currentTicket.location_code
				};
			var invocationData = {
    				adapter: mor.ticket.viewControl.adapterUsed,
    				procedure: "getPassCodeNew"
    		};
    		var options = {
    				onSuccess: mor.ticket.orderManager.requestCaptchaImgSucceeded2,
    				onFailure: mor.ticket.util.creatCommonRequestFailureHandler()
    		};
//    		busy.show();
//    		WL.Client.invokeProcedure(invocationData, options);
    		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
    		return false;
		},
		requestCaptchaImgSucceeded2: function(result){
    		if(busy.isVisible()){
    			busy.hide();
    		}
    		var invocationResult = result.invocationResult;
    		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
    			mor.ticket.orderManager.capchaNewOrOldFlag = invocationResult.model;
    			if(invocationResult.model == "2"){
    				jq("#captchaDivOld").show();
    				jq("#captchaDivNew").hide();
    				jq("#captchaImg").attr("src", "data:image/gif;base64," + invocationResult.passcode);
        			mor.ticket.history.checkCodeImg1="data:image/gif;base64," + invocationResult.passcode;
        			jq("#captchaImg").show();
        			jq("#refreshImgBtn").hide();
    				jq("#captchaImg1").hide();
    			}else{
    				jq("#captchaDivOld").hide();
    				jq("#captchaDivNew").show();
    				mor.ticket.orderManager.captchaImgReload();
    				jq("#captchaImg1").on("click",mor.ticket.orderManager.captchaTabHandle);
    				jq("#captchaImg1").attr("src", "data:image/gif;base64," + invocationResult.passcode);
    				mor.ticket.history.checkCodeImg1="data:image/gif;base64," + invocationResult.passcode;
        			jq("#refreshImgBtn").show();
    				jq("#captchaImg1").show();
    			}
    			if(jq("#passengersView .ui-content").attr("data-iscroll")!=undefined){
    				jq("#passengersView .ui-content").iscrollview("refresh");
    			}
    		} else {
    			mor.ticket.util.alertMessage(invocationResult.error_msg);
    		}
    	},
    	
		//图形验证码--end
		
    	refreshCaptcha:function (){
            if(jq('#captchaLoading').length>0){
                return false;
            }
    		jq("#captchaInput").val("");
            jq("#captchaInput").parent().after('<span id="captchaLoading">加载中</span>');
            jq("#captchaImg").hide();
    		/*var invocationData = {
    				adapter: "CARSMobileServiceAdapterV2",
    				procedure: "postPassCode",
    				parameters: [mor.ticket.util.prepareRequestCommonParameters()]	
    		};*/
    		var invocationData = {
    				adapter: mor.ticket.viewControl.adapterUsed,
    				procedure: "postPassCode"
    		};
    		var options = {
    				onSuccess: mor.ticket.orderManager.requestCaptchaSucceeded,
    				onFailure: mor.ticket.util.creatCommonRequestFailureHandler()
    		};
//    		busy.show();
//    		WL.Client.invokeProcedure(invocationData, options);
    		mor.ticket.util.invokeWLProcedure(null, invocationData, options);
    		return false;
    	},
    	
    	requestCaptchaSucceeded: function(result){
    		if(busy.isVisible()){
    			busy.hide();
    		}
            jq("#captchaLoading").remove();
    		var invocationResult = result.invocationResult;
    		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
    			jq("#captchaImg").attr("src", "data:image/gif;base64," + invocationResult.passcode);
    			mor.ticket.history.checkCodeImg="data:image/gif;base64," + invocationResult.passcode;
    			jq("#captchaImg").show();
    		} else {
    			mor.ticket.util.alertMessage(invocationResult.error_msg);
    		}
    	},
    	
    	refreshCaptcha2:function (){
            if(jq('#captchaLoading').length>0){
                return false;
            }
    		jq("#captchaInput").val("");
            jq("#captchaInput").parent().after('<span id="captchaLoading">加载中</span>');
            jq("#captchaImg").hide();
    		/*var invocationData = {
    				adapter: "CARSMobileServiceAdapterV2",
    				procedure: "postPassCode",
    				parameters: [mor.ticket.util.prepareRequestCommonParameters()]	
    		};*/
    		var invocationData = {
    				adapter: mor.ticket.viewControl.adapterUsed,
    				procedure: "postPassCode"
    		};
    		var options = {
    				onSuccess: mor.ticket.orderManager.requestCaptchaSucceeded2,
    				onFailure: mor.ticket.util.creatCommonRequestFailureHandler()
    		};
    		mor.ticket.viewControl.show_busy = false;
    		mor.ticket.util.invokeWLProcedure(null, invocationData, options);
//    		WL.Client.invokeProcedure(invocationData, options);
    		return false;
    	},
    	
    	requestCaptchaSucceeded2: function(result){
    		if(busy.isVisible()){
    			busy.hide();
    		}
            jq("#captchaLoading").remove();
    		var invocationResult = result.invocationResult;
    		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				//alert(invocationResult.passcode);
    			jq("#captchaImg").attr("src", "data:image/gif;base64," + invocationResult.passcode);
                mor.ticket.history.checkCodeImg="data:image/gif;base64," + invocationResult.passcode;
                jq("#captchaImg").show();
    		} else {
    			mor.ticket.util.alertMessage(invocationResult.error_msg);
    		}
    	},
    	
    	getRoundTripOrderList:function(){
    		var currentOrderList = [];
    		var preOrderList = [];
    		var orderTicketList = this.orderTicketList;
    		for(var i=0; i<orderTicketList.length; i++){
    			if(!orderTicketList[i].isRoundTripFlag){
    				currentOrderList.push(orderTicketList[i]);
    			} else {
    				preOrderList.push(orderTicketList[i]);
    			}
    		}
    		return {current:currentOrderList,
    			pre : preOrderList};
    	},
    	
    	preTicketDetail:{},
    	setPreTicketDetail:function(ticketDetail){ 
    		for(o in ticketDetail){
    			this.preTicketDetail[o] = ticketDetail[o];
    		}
    	},
		getPreTicketDetail:function(){ return this.preTicketDetail;},
		//确认往返程 往程票务信息:添加购票人电话 以及标记往程票flag
		confirmRoundTripTicketList:function(passengerList){
			var orderTicketList = this.orderTicketList;
			for(var i=0; i < orderTicketList.length; i++){
				for(var j=0; j<passengerList.length; j++){
					if(orderTicketList[i].passenger_id_no == passengerList[j].id_no){
						orderTicketList[i].mobile_no = passengerList[j].mobile_no;
					}
				}
				//orderTicketList[i].isRoundTripFlag = true;
			}
		},
		
		//赋值默认的座位类型
		getConfirmOrderListPassengers:function(passengerList, seat_type){
			var orderTicketList = this.orderTicketList;
			for(var i=0; i<orderTicketList.length; i++){
				var passenger = {};
				passenger.id_no = orderTicketList[i].passenger_id_no;
				passenger.id_type = orderTicketList[i].passenger_id_type_code;
				passenger.mobile_no = orderTicketList[i].mobile_no;
				passenger.user_name = orderTicketList[i].passenger_name;
				passenger.ticket_type = orderTicketList[i].ticket_type_code;
				passenger.seat_type = seat_type;
				passengerList.push(passenger);				
			}
		},
		
		//根据 orderTicketList 获取返程联系人列表。 因为在选择联系人那里重新对联系人的字段进行了修改，
		// 所以这里也只能进行修改.
		getFcPassengers: function(){
			var passengerList = [];
			jq.each(this.orderTicketList, function(i, orderTicket) {
				var passenger = {};
				passenger.id_no = orderTicket.passenger_id_no;
				passenger.id_type = orderTicket.passenger_id_type_code;
				passenger.mobile_no = orderTicket.mobile_no;
				passenger.user_name = orderTicket.passenger_name;
				passenger.ticket_type = orderTicket.ticket_type_code;
				passenger.seat_type = orderTicket.seat_type_code;
				passengerList.push(passenger);
			});
			return passengerList;
		}
	});
})();