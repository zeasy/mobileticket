
/* JavaScript content from js/controller/trainQuery.js in folder common */
(function(){
	jq("#trainQueryView").live("pageinit", function(){		
		registerTrainQueryBtnLisener();
		registerBackButtonHandler();
	});
	jq("#trainQueryView").live("pageshow",function(e, data) {
		mor.ticket.util.initAppVersionInfo();
		var prevId = data.prevPage.attr("id");
		mor.ticket.viewControl.tab1_cur_page = "trainQuery.html";
		if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
			mor.ticket.currentQueryObject.train_date = mor.ticket.currentPagePath.calenderDate;
		}else if(prevId !== "trainQueryResultsView"){
			var today = new Date(mor.ticket.util.getNewDate());
			mor.ticket.currentQueryObject.train_date = today.format("yyyy-MM-dd");
		}
		jq("#trainQueryTrainDateInput").val(mor.ticket.currentQueryObject.train_date);
	    jq("#trainQueryTrainInput").val(mor.ticket.views.trainQueryCode.trainCode);
	});
	function registerTrainQueryBtnLisener(){
		jq("#trainQueryBtn").bind("tap", sendTrainQueryRequest);
		jq("#trainQueryTrainDateInput").bind("tap",function(){
			var current = mor.ticket.currentPagePath;
			var monthValue;
			current.defaultDateValue[0]=jq("#trainQueryTrainDateInput").val().slice(0,4);
			monthValue=parseInt(jq("#trainQueryTrainDateInput").val().slice(5,7));
			if(monthValue == "1"){
				monthValue = "0";
			}else monthValue = monthValue-1;
			if(monthValue < 10){
				current.defaultDateValue[1] = "0"+monthValue;
			}else{
				current.defaultDateValue[1] = monthValue;
			}
			current.defaultDateValue[2]=jq("#trainQueryTrainDateInput").val().slice(8,10);
			var reservedPeriodType = "3";
			var reservePeriod = parseInt(window.ticketStorage
					.getItem("reservePeriod_" + reservedPeriodType), 10);
			jQuery.extend(jQuery.mobile.datebox.prototype.options, {
				mode:"calbox",
				useInline: true,
				calControlGroup: true,
				defaultValue: current.defaultDateValue,
				showInitialValue: true,
				lockInput: false,
				useFocus: true,
				maxDays: reservePeriod,
				buttonIcon: "grid",
				calUsePickers: true,
				afterToday: true,
				beforeToday: false,
				notToday: false,
				calOnlyMonth: false,
				overrideSlideFieldOrder: ['y','m','d','h','i']
			});
			mor.ticket.currentPagePath.fromPath = "trainQuery.html";
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.pickYears = false;
			mor.ticket.currentPagePath.show = false;
			jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
			if(jq("#trainQueryTrainInput").val() != ""){
				mor.ticket.views.trainQueryCode.trainCode = jq("#trainQueryTrainInput").val();
			}
			mor.ticket.util.changePage(goingPath);
		});
	}
	function sendTrainQueryRequest(e) {
		e.stopImmediatePropagation();
		var util = mor.ticket.util;	
		var trainCode = mor.ticket.checkForm.trim(jq("#trainQueryTrainInput").val());
		if(trainCode == '' || !mor.ticket.checkForm.checkTrainCode(trainCode)){
			util.alertMessage("请输入正确的车次号");
			return;
		}
		
		var model = mor.ticket.currentQueryObject;
		model.train_code = jq("#trainQueryTrainInput").val().toLocaleUpperCase();
		model.train_date = jq("#trainQueryTrainDateInput").val();
		mor.ticket.views.trainQueryCode.trainCode = jq("#trainQueryTrainInput").val();
		mor.ticket.util.changePage(vPathCallBack()+"trainQueryResults.html");
		return false;
	}

	function registerBackButtonHandler() {
		jq("#trainQueryBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage(vPathCallBack() + "moreOption.html");
			return false;
		});
	}
})();