/**
 * @providesModule Network
 * Created by easy on 15/11/17.
 */

'use strict';

var Interface = require('Interface');
var NativeNetwork = require('NativeModules').NativeNetwork;

class Network {

    static async asyncRequest(options) {
        return Interface.asyncInvoke(NativeNetwork.request,options);
    }

    static async asyncUpload(options) {
        return Interface.asyncInvoke(NativeNetwork.upload,options);
    }

    static async asyncDownload(options) {
        return Interface.asyncInvoke(NativeNetwork.download,options);
    }

}

exports = module.exports = Network;