/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Navigator
} from 'react-native';


import {Provider} from 'react-redux'
import {combineReducers,applyMiddleware,createStore} from 'redux'

import CCNavigator from 'CCNavigator';

let reducers = combineReducers({
  Web:require('./Web/reducers'),
  Mobile:require('./Mobile/reducers')
});

import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';

const logger = createLogger();
const store = createStore(
    reducers,
    applyMiddleware(thunk, promise, logger)
);

class ReactTicket extends Component {


  renderScene(route,navigator) {
    let Controller = route.component;
    let props = route.passProps;

    return <Controller {...props} navigator={navigator}/>
  }

  render() {
    return (
        <Provider store={store}>
        <CCNavigator initialRoute={{component:require('./containers/RTRootController'),title:'root'}}
    renderScene={this.renderScene.bind(this)}
  />
  </Provider>

  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('ReactTicket', () => ReactTicket);
