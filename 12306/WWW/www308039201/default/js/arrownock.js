
/* JavaScript content from js/arrownock.js in folder common */
var AnPush = function() {
};

AnPush.prototype = {
	native_call: function(method, args, successCallback, errorCallback) {
		cordova.exec(successCallback, errorCallback, "AnPush", method, args);
	},
	
	initialize: function(appKey, callback, options) {
		var params = [appKey];
		if(options) {
			if(options.secureConnection == false) {
				params.push(false);
			} else {
				params.push(true);
			}
			var settings = {};
			if(options.alert) {
				settings.alert = true;
			}
			if(options.badge) {
				settings.badge = true;
			}
			if(options.sound) {
				settings.sound = true;
			}
			if(options.foregroundAlert) {
				settings.foregroundAlert = options.foregroundAlert;
			} else {
				settings.foregroundAlert = "You have a new message";
			}
			params.push(settings);
		} else {
			params.push(true);
			params.push({});
		}
		
		this.native_call('init', params, function(data) {
			callback();	// success
		}, function(e) {
			var error = new Error(e.message);
			error.code = e.error_code;
			callback(error); // error
		});
	},
	
	connect: function(callback) {
		if(device.platform === 'Android') {
			this.native_call('connect', [], function() {
				if(callback) {
					callback();
				}
			}, function(err) {
				if(callback) {
					callback(err);
				}
			});
		}
	},

	disconnect: function(callback) {
		if(device.platform === 'Android') {
			this.native_call('disconnect', [], function() {
				if(callback) {
					callback();
				}
			}, function(err) {
				if(callback) {
					callback(err);
				}
			});
		}
	},
	
	setDeviceId: function(deviceId, callback) {
		this.native_call('setDeviceId', [deviceId], function() {
			if(callback) {
				callback();
			}
		}, function(err) {
			if(callback) {
				callback(err);
			}
		});
	},
	
	register: function(channels, overwrite) {
		var self = this;
		this.native_call('register', [channels, overwrite], function() {}, function(err) {
			if(err) {
				self.onRegistered(err);
			}
		});
	},

	unregister: function(channels) {
		var self = this;
		this.native_call('unregister', [channels], function() {}, function(err) {
			if(err) {
				self.onUnregistered(err);
			}
		});
	},
	
	setMute: function(startHour, startMinute, duration) {
		var self = this;
		if(arguments.length === 3) {
			this.native_call('setScheduledMute', [startHour, startMinute, duration], function() {}, function(err) {
				if(err) {
					self.onSetMute(err);
				}
			});
		} else {
			this.native_call('setMute', [], function() {}, function(err) {
				if(err) {
					self.onSetMute(err);
				}
			});
		}
	},
	
	clearMute: function() {
		var self = this;
		this.native_call('clearMute', [], function() {}, function(err) {
			if(err) {
				self.onClearMute(err);
			}
		});
	},
	
	setSilentPeriod: function(startHour, startMinute, duration, resend) {
		var self = this;
		this.native_call('setSilentPeriod', [startHour, startMinute, duration, resend], function() {}, function(err) {
			if(err) {
				self.onSetSilentPeriod(err);
			}
		});
	},
	
	clearSilentPeriod: function() {
		var self = this;
		this.native_call('clearSilentPeriod', [], function() {}, function(err) {
			if(err) {
				self.onClearSilentPeriod(err);
			}
		});
	},
	
	getAnID: function(callback) {
		this.native_call('getAnID', [], function(anid) {
			if(callback) {
				callback(null, anid);
			}
		}, function(err) {
			if(callback) {
				callback(err);
			}
		});
	},

	enable: function(callback) {
		try {
			this.native_call('enableFetchMode', [], function() {
				if(callback) {
					callback();
				}
			}, function(err) {
				if(callback) {
					callback(err);
				}
			});
		} catch(e) {
			callback();
		}
	},

	disable: function(callback) {
		this.native_call('disable', [], function() {
			if(callback) {
				callback();
			}
		}, function(err) {
			if(callback) {
				callback(err);
			}
		});
	},

	isEnabled: function(callback) {
		if(callback) {
			this.native_call('isEnabled', [], function(isEnabled) {
				callback(null, isEnabled == 1? true : false);	// success
			}, function(err) {
				callback(err);
			});
		}
	},
	
	setFetchingInterval: function(interval, callback) {
		if(device.platform === 'Android') {
			this.native_call('setFetchingInterval', [interval], function() {
				if(callback) {
					callback();
				}
			}, function(err) {
				if(callback) {
					callback(err);
				}
			});
		}
	},
	
	// iOS only
	setApplicationIconBadgeNumber: function(number, callback) {
		if(device.platform === 'iOS') {
			this.native_call('setApplicationIconBadgeNumber', [number], function() {
				if(callback) {
					callback();	// success
				}
			}, function(err) {
				if(callback) {
					callback(err);
				}
			});
		} else {
			if(callback) {
				callback();
			}
		}
	},
	
	getFromLocalStorage: function(key, callback) {
		this.native_call('fetchLocalStore', [key], function(value) {
			if(callback) {
				callback(null, value);	// success
			}
		}, function(err) {
			if(callback) {
				callback(err);
			}
		});
	},
	
	saveToLocalStorage: function(key, value, callback) {
		var params = [key];
		if(value) {
			params.push(value);
		}
		this.native_call('saveToLocalStore', params, function() {
			if(callback) {
				callback();	// success
			}
		}, function(err) {
			if(callback) {
				callback(err);
			}
		});
	},
	
	setServerHosts: function(apiHost, dsHost, callback) {
		if(callback) {
			this.native_call('setServerHosts', [apiHost, dsHost], function() {
				callback();
			}, function(err) {
				callback(err);
			});
		}
	},
	
	showForegroundNotification: function(message, sound, seconds, callback) {
		if(arguments[0] !== undefined 
			&& arguments[1] !== undefined 
			&& arguments[2] !== undefined) {
			this.native_call('showForegroundNotification', [message, sound, seconds], function() {
				callback();
			}, function(err) {
				callback(err);
			});
		} else {
			var error = new Error("Invalid parameters");
			callback(error);
		}
	},
	
	// the event callbacks
	onRegistered: function(err, anid) {},
	onUnregistered: function(err) {},
	onSetMute: function(err) {},
	onClearMute: function(err) {},
	onSetSilentPeriod: function(err) {},
	onClearSilentPeriod: function(err) {},
	onStatusChanged: function(err, status) {},	// android only
	onReceivedPushNotification: function(payloads) {}
};

//DeviceManager
var AnDeviceManager = function() {
};

AnDeviceManager.prototype = {
	native_call: function(method, args, successCallback, errorCallback) {
		cordova.exec(successCallback, errorCallback, "AnDeviceManager", method, args);
	},
	
	setHost: function(host, callback) {
		this.native_call('setHost', [host], function() {
			if(callback) {
				callback();
			}
		}, function(err) {
			if(callback) {
				callback(err);
			}
		});
	}
};

if(!window.plugins) {
	window.plugins = {};
}

var anPushPlugin = new AnPush();
window.plugins.AnPush = anPushPlugin;

var dmPlugin = new AnDeviceManager();
window.plugins.AnDeviceManager = dmPlugin;

function loadPendingPushNotifications() {
	cordova.exec(function() {}, function() {}, "AnPush", "loadPendingPushNotifications", []);
}

document.addEventListener("deviceready", function() {
	if(device.platform === 'iOS') {
		// hacking for the app start event
		cordova.exec(function() {}, function() {}, "AnPush", "onResume", []);
		
		document.addEventListener("resume", function() {
			cordova.exec(function() {}, function() {}, "AnPush", "onResume", []);
			loadPendingPushNotifications();
		});
		
		document.addEventListener("pause", function() {
			cordova.exec(function() {}, function() {}, "AnPush", "onPause", []);
		});
		loadPendingPushNotifications();
	}
});