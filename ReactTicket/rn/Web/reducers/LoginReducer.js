/**
 * Created by easy on 16/3/17.
 */


function reducer(state,action) {

    if (action.type.startsWith('TWLogin')) {
        return {... state,...action.data};
    }

    return {...state};
}

module.exports = reducer;