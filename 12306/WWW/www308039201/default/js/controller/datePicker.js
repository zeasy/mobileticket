
/* JavaScript content from js/controller/datePicker.js in folder common */
(function() {
	var model = mor.ticket.leftTicketQuery;
	var reservedPeriodType = '';
	jq("#datePickerView").live("pageinit", function() {
		registerType();
		registerEvtListener();
		registerCalenderPage();
	});
	jq("#datePickerView").live("pagebeforeshow",function(){
		mor.ticket.util.initAppVersionInfo();
		var model = mor.ticket.leftTicketQuery;
		if(mor.ticket.currentPagePath.fromPath == "advanceQueryOrder.html"){
			if(mor.ticket.viewControl.queryFinishedOrderType == '4'){
				jq("#datePicker").html("选择乘车日期");
			}else{
				if( mor.queryOrder.views.advanceQuery.isSelectTrainDate == false){
					jq("#datePicker").html("选择订票日期");
				}else
				jq("#datePicker").html("选择乘车日期");
			}
//			jq("#bookTicketTab").removeClass("ui-btn-active ui-state-persist");
//			jq("#queryOrderTab").addClass("ui-btn-active ui-state-persist");
		}
		if(mor.ticket.viewControl.bookMode !== "dc"){
			if(model.purpose_codes === '0X'){
				jq("#datePicker").html("选择出发日期(学生)");
			}
		}
		if(model.purpose_codes !== '0X'){
			var currSelectedDate = jq("#datePickerInputHidden").val();
			var date = mor.ticket.util.setMyDate(currSelectedDate);
			var today = mor.ticket.util.getNewDate();
			var todayDate = today.getDate();
			today = today.setDate(todayDate + parseInt(mor.ticket.history.reservePeriod));
			var reserveDay = new Date(today);
			if (date > reserveDay) {
				jq("#datePickerInputHidden").attr("value",reserveDay.format("yyyy-MM-dd"));
			}
		}
		if(mor.ticket.currentPagePath.type == true){
			initPrevAndNextMonth();
		}
	});
	function initPrevAndNextMonth(){
		var currSelectedDate = jq("#datePickerInputHidden").val();
		var selectedDate = mor.ticket.util.setMyDate(currSelectedDate);
			selectedDate.setDate(1);
		var prevMonthDate = new Date(selectedDate.setMonth(selectedDate.getMonth() - 1));
		var nextMonthDate = new Date(selectedDate.setMonth(selectedDate.getMonth() + 2));
		var prevMonthStr = prevMonthDate.format("yyyyMM");
		var nextMonthStr = nextMonthDate.format("yyyyMM");
		var fromPath = mor.ticket.currentPagePath.fromPath;
		if((fromPath == "advanceQueryOrder.html" && mor.queryOrder.views.advanceQuery.isSelectTrainDate == false)||
				(fromPath == "advanceQueryOrder.html" && mor.ticket.viewControl.queryFinishedOrderType == '4')){
				var newDate = mor.ticket.util.getNewDate();
				newDate = newDate.format("yyyyMM");
				if(mor.queryOrder.views.advanceQuery.isFromDateInputShow == true){
					var fromDate = jq("#fromDateInputShow").val().slice(0,4)+jq("#fromDateInputShow").val().slice(5,7);
					if(fromDate == newDate){
						jq(".nextMonth").addClass("ui-disabled");
					}else{
						jq(".nextMonth").removeClass("ui-disabled");
					}
				}
				else{
					var toDate = jq("#toDateInputShow").val().slice(0,4)+jq("#toDateInputShow").val().slice(5,7);
					if(toDate == newDate){
						jq(".nextMonth").addClass("ui-disabled");
					}else{
						jq(".nextMonth").removeClass("ui-disabled");
					}
				}
		}else if((fromPath == "advanceQueryOrder.html" && mor.ticket.viewControl.queryFinishedOrderType == '3'&&mor.queryOrder.views.advanceQuery.isSelectTrainDate == true)){
			if(mor.queryOrder.views.advanceQuery.isFromDateInputShow == true){
				jq(".preMonth").addClass("ui-disabled");
			}else{
				var newDate = mor.ticket.util.getNewDate();
				newDate = newDate.format("yyyyMM");
				var fromDate = jq("#toDateInputShow").val().slice(0,4)+jq("#toDateInputShow").val().slice(5,7);
				if(fromDate == newDate){
					jq(".preMonth").addClass("ui-disabled");
				}else{
					jq(".preMonth").removeClass("ui-disabled");
				}
			}
		}else{
			if(!mor.ticket.util.indexOf(mor.ticket.history.reservePeriodMonth,nextMonthStr)){
				jq(".nextMonth").addClass("ui-disabled");
			}else{
				jq(".nextMonth").removeClass("ui-disabled");
			}
			if(!mor.ticket.util.indexOf(mor.ticket.history.reservePeriodMonth,prevMonthStr)){
				jq(".preMonth").addClass("ui-disabled");
			}else{
				jq(".preMonth").removeClass("ui-disabled");
			}
		}
		
		jq("select[name='pickyar']").parent().addClass("ui-disabled");
		jq("select[name='pickmon']").parent().addClass("ui-disabled");
	};
	function calculateNormalReservedPeriod(){
		reservedPeriodType = '1';
		var reservePeriod = parseInt(window.ticketStorage
				.getItem("reservePeriod_" + reservedPeriodType), 10);
		if(!jq.isNumeric(reservePeriod)){
			reservePeriod = parseInt(window.ticketStorage
		.getItem("reservePeriod_" + '1'), 10);
		}
		normalPeriod = reservePeriod;
		return normalPeriod;
	}
	function calculateStuReservedPeriod(){
		reservedPeriodType = '3';
		var reservePeriod = parseInt(window.ticketStorage
				.getItem("reservePeriod_" + reservedPeriodType), 10);
		if(!jq.isNumeric(reservePeriod)){
			reservePeriod = parseInt(window.ticketStorage
		.getItem("reservePeriod_" + '1'), 10);
		}
		stuPeriod = reservePeriod;
		return stuPeriod;
	}
	//选择类型
	function registerEvtListener(){
		jq("#typeSelectLableNomal").on("tap",function(){
			reservedPeriodType = '1';
			model.purpose_codes = '00';
			var reservePeriod = parseInt(window.ticketStorage
					.getItem("reservePeriod_" + reservedPeriodType), 10);
			if(!jq.isNumeric(reservePeriod)){
				reservePeriod = parseInt(window.ticketStorage
			.getItem("reservePeriod_" + '1'), 10);
			}
			var currSelectedDate = jq("#datePickerInputHidden").val();
			var date = mor.ticket.util.setMyDate(currSelectedDate);
			var today = mor.ticket.util.getNewDate();
			var todayDate = today.getDate();
			today = today.setDate(todayDate + parseInt(reservePeriod));
			var reserveDay = new Date(today);
			if (date > reserveDay) {
				jq("#datePickerInputHidden").attr("value",reserveDay.format("yyyy-MM-dd"));
			}
			var reservePeriodMonth = window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType) == null ? [] : 
				JSON.parse(window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType));
			mor.ticket.history.reservePeriodMonth = reservePeriodMonth;
			mor.ticket.history.reservePeriod = reservePeriod;
			jq("#typeSelect2").css("visibility","hidden");
			jq("#typeSelect1").css("visibility","visible");
			jq("#studentSeatType").addClass("ui-checkbox-off");
			setTimeout(function(){
				jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				initPrevAndNextMonth();
			},300);
		});
		jq("#typeSelectLableStu").on("tap",function(){
			model.purpose_codes = '0X';
			reservedPeriodType = '3';
			var reservePeriod = parseInt(window.ticketStorage
					.getItem("reservePeriod_" + reservedPeriodType), 10);
			if(!jq.isNumeric(reservePeriod)){
				reservePeriod = parseInt(window.ticketStorage
			.getItem("reservePeriod_" + '1'), 10);
			}
			var reservePeriodMonth = window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType) == null ? [] : 
				JSON.parse(window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType));
			mor.ticket.history.reservePeriodMonth = reservePeriodMonth;
			mor.ticket.history.reservePeriod = reservePeriod;
			jq("#typeSelect1").css("visibility","hidden");
			jq("#typeSelect2").css("visibility","visible");
			jq("#studentSeatType").css("visibility","visible");
			jq("#studentSeatType").addClass("ui-checkbox-on");
			setTimeout(function(){
				jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				initPrevAndNextMonth();
			},300);
		});
		jq("#datePickBackBtn").on("tap",function(){
			var fromPath = mor.ticket.currentPagePath.fromPath;
			if(fromPath == "advanceQueryOrder.html"){
				mor.ticket.currentPagePath.advanceQueryDate = jq("#datePickerInputHidden").val();
			}else{
				mor.ticket.currentPagePath.calenderDate = jq("#datePickerInputHidden").val();
			}
			mor.ticket.util.changePage(vPathCallBack()+fromPath );
		});
	}
	//初始化可选类型
	function registerType(){
		if(model.purpose_codes == '0X'){
			jq("#typeSelect2").css("visibility","visible");
			jq("#typeSelect1").css("visibility","hidden");
			
		}else{
			jq("#typeSelect2").css("visibility","hidden");
			jq("#typeSelect1").css("visibility","visible");
		}
	}
	//初始化日历控件
	function registerCalenderPage(){
		var canStudentTicket = window.ticketStorage.getItem("canStudentTicket");
		if(canStudentTicket == true || canStudentTicket == "true"){
			if(mor.ticket.currentPagePath.type == false){
				jq(".typeSelectLable").css("display","none");
				jq(".insertLine").css("display","none");
				jq("#typeSelect1").css("display","none");
				jq("#typeSelect2").css("display","none");
			}else if(mor.ticket.currentPagePath.type == true){
				var normalPeriod = calculateNormalReservedPeriod()+1;
				var stuPeriod = calculateStuReservedPeriod()+1;
				jq("#normalTicketPeriod").html(normalPeriod+"天预售期：成人、儿童、学生和残军票");
				jq("#stuTicketPeriod").html(stuPeriod+"天预售期：仅学生票");
				jq(".typeSelectLable").css("visibility","visible");
				jq(".insertLine").css("visibility","visible");
				if(mor.ticket.viewControl.bookMode !== 'dc'){
					jq(".typeSelectLable").css("display","none");
					jq(".insertLine").css("display","none");
					jq("#typeSelect1").css("display","none");
					jq("#typeSelect2").css("display","none");
				}
			}
			if(mor.ticket.currentPagePath.show == false){
				jq(".typeSelectLable").css("display","none");
				jq(".insertLine").css("display","none");
				jq("#typeSelect1").css("display","none");
				jq("#typeSelect2").css("display","none");
			}
		}else{
			jq(".typeSelectLable").css("display","none");
			jq(".insertLine").css("display","none");
			jq("#typeSelect1").css("display","none");
			jq("#typeSelect2").css("display","none");
		}
	}
	
	var pageUrl = vPathCallBack()+ "datePicker.html";
	jq.mobile.loadPage(pageUrl,{showLoadMsg: false});
})();