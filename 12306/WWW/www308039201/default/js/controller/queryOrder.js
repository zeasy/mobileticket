
/* JavaScript content from js/controller/queryOrder.js in folder common */
(function () {
	var prevPageId = "";
    jq("#queryOrderView").live("pageinit", function (e, data) {
        registerTodayOrderBtnListener();
        registerSevenDaysOrderBtnListener();
        registerAdvanceQueryOrderBtnListener();
        registerNoFinishedOrdersItemListener();
    	registerUnfinishedOrderRefreshButtonListener();
    	registerTransferOrderDisplayBtnListener();
    	registerTravelQueryOrderBtnListener();
    	
    });
    jq("#queryOrderView").live("pagebeforeshow", function () {
    	jq('#queryOrderView .iscroll-wrapper').iscrollview('refresh');
        mor.ticket.viewControl.current_tab = "queryOrderTab";
        mor.ticket.viewControl.tab2_cur_page = "queryOrder.html";
        var user = mor.ticket.loginUser; 
        if (user.isAuthenticated === "Y") {
//			WL.Logger.debug("queryOrderView show");		
        } else {
        	// add by yiguo
			var latestTime = window.ticketStorage.getItem("pwdTime");
			// mod by yiguo
			if (window.ticketStorage.getItem("autologin") == "true"
				&& (!latestTime || (Date.now() - latestTime   < 7*24*3600*1000))
				&& ((user.username!="" && user.password!="" && user.password!="*")
				|| (jq("#usernameInput").val()!="" && jq("#usernameInput").val()!=undefined)
					&& jq("#passwordInput").val()!="" && jq("#passwordInput").val()!=undefined)) {
				AutoSendLoginRequest();
			}else{
				jq.mobile.changePage(vPathCallBack() + "loginTicket.html");
			}
        }
        
    	var date = mor.ticket.util.getNewDate();
    	var weekDay = new Array("星期日","星期一","星期二","星期三","星期四","星期五","星期六")[date.getDay()];
    	var currentDate = date.format("yyyy年MM月dd日") ;
    	date.setDate(date.getDate()-6);
    	var sevenDays = date.format("yyyy年MM月dd日");
    	jq("#todayDiv").html("<span>"+currentDate + " " + weekDay + "</span>");
//    	jq("#sevenDaysDiv").html("<span>" + sevenDays + "--" + currentDate + "</span>");
    	
    });
    jq("#queryOrderView").live("pageshow", function (e, data) {
    	mor.ticket.util.initAppVersionInfo();
        prevPageId = data.prevPage.attr("id");
        if (mor.ticket.viewControl.isNeedRefreshUnfinishedOrder) {
            jq('#noFinishedOrdersDesc').text('未完成订单(0)');
            jq('#noFinishedOrdersQuery').empty();
            mor.ticket.viewControl.isNeedRefreshUnfinishedOrder = false;
        }
	     if(prevPageId === "orderListView") {
	    	jq("#notFinishedOrdersDiv").addClass("qry_order_tab_active");
			jq("#finishedOrdersDiv").removeClass("qry_order_tab_active");
			jq("#finishedOrders").hide();
			jq("#notFinshedOrders").show();
			jq("#noFinishedOrdersQuery").show();
	        displayNoFinishedOrders();
	    }
	     if(prevPageId === "payFinishtView"){
	    	 jq("#todayOrder").tap(); 
	     }
    });

    function registerNoFinishedOrdersItemListener() {
        jq("#noFinishedOrdersQuery").off().on("tap", "li", function (e) {
            e.stopImmediatePropagation();
            var queryOrder = mor.ticket.queryOrder;
            //init queryOrder content
            queryOrder.currentQueryOrder = {};
            queryOrder.currentUnfinishOrderIndex = 0;
            queryOrder.currentUnfinishOrderIndex = jq(this).index();
            mor.ticket.queryOrder.setCurrentUnfinishedOrders(jq(this).index());
            mor.ticket.util.changePage(vPathCallBack() + "orderList.html");
            return false;
        });
    }
    // mod by yiguo
    window.pullOrderData =  initQueryOrder;
    function initQueryOrder(fn, err) {
        var util = mor.ticket.util;
        var commonParameters = {
            'query_class': '1'
        };

        var invocationData = {
            adapter: mor.ticket.viewControl.adapterUsed,
            procedure: "queryOrder"
        };
        // add by yiguo
        var _rqtSuc,_rqtErr;
        if(!fn){
        	_rqtSuc = requestSucceeded;
        	_rqtErr = util.creatCommonRequestFailureHandler();
        }else{
        	_rqtSuc = function(result){
        		//requestSucceeded(result);
        		fn(result);
        	};
        	
        	var _failFn = util.creatCommonRequestFailureHandler();
        	_rqtErr = function(){
        		_failFn(ret);
        		err();
        		
        	};
        }
        var options = {
            onSuccess: _rqtSuc,
            onFailure: _rqtErr
        };
        mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
    }

    function requestSucceeded(result) {
        if (busy.isVisible()) {
            busy.hide();
        }
        var invocationResult = result.invocationResult;
        if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
        	prevPageId = "";
            var queryOrder = mor.ticket.queryOrder;
            //init queryOrder queryOrderList
            queryOrder.queryOrderList = [];
            queryOrder.originPaidQrderList = [];
            queryOrder.setUnfinishedOrderList(invocationResult.orderList);
            displayNoFinishedOrders();
        } else {
            mor.ticket.util.alertMessage(invocationResult.error_msg);
        }
    }

    function displayNoFinishedOrders() {
        var queryOrder = mor.ticket.queryOrder;
        var noFinishedOrderList = queryOrder.queryOrderList;
    	jq("#noFinishedOrdersDesc").html("未完成订单(" + (noFinishedOrderList != null ? noFinishedOrderList.length : "0") + ")");
        if (noFinishedOrderList && noFinishedOrderList[0]) {
            queryOrder.hasChangeTicket();
            jq("#noFinishedOrdersQuery").html(generateUnfinishedOrdersDetailList(noFinishedOrderList)).show().listview("refresh");
            contentIscrollRefresh();
            if(noFinishedOrderList.length == 1 && prevPageId != "orderListView"){
            	jq("#noFinishedOrdersQuery li").tap();
            }
        } else {
            jq("#noFinishedOrdersQuery").hide();
            jq("#noFinishedOrdersQuery").html("").listview("refresh");
        }
    }

    function contentIscrollRefresh() {
        if (jq("#queryOrderView .ui-content").attr("data-iscroll") != undefined) {
            jq("#queryOrderView .ui-content").iscrollview("refresh");
        }
    }

    function registerTodayOrderBtnListener() {
    	jq("#todayOrder").bind("tap", function () {
        	if (!busy.isVisible()) {
				busy.show();
			}
            mor.ticket.viewControl.queryFinishedOrderType = '0';// query today finished order
            mor.ticket.util.changePage(vPathCallBack() + "finishedOrderList.html");
            return false;
        });
    }

    function registerSevenDaysOrderBtnListener() {
        jq("#sevenDaysOrder").bind("tap", function () {
        	if (!busy.isVisible()) {
				busy.show();
			}
            mor.ticket.viewControl.queryFinishedOrderType = '1';// query seven days finished order
            mor.ticket.util.changePage(vPathCallBack() + "finishedOrderList.html");
            return false;
        });
    }

    function registerAdvanceQueryOrderBtnListener() {
        jq("#advanceQuery").bind("tap", function () {
        	if (!busy.isVisible()) {
				busy.show();
			}
            mor.ticket.viewControl.queryFinishedOrderType = '2';//advance query finished order
            mor.ticket.util.changePage(vPathCallBack() + "advanceQueryOrder.html");
            return false;
        });
    }
    
    function registerTravelQueryOrderBtnListener() {
        jq("#noTravelOrder").bind("tap", function () {
        	if (!busy.isVisible()) {
				busy.show();
			}
            mor.ticket.viewControl.queryFinishedOrderType = '3';//not travel query finished order
            mor.ticket.util.changePage(vPathCallBack() + "advanceQueryOrder.html");
            mor.queryOrder.views.advanceQuery.queryOrderTypeFlag = true;
            return false;
        });
        jq("#traveledOrder").bind("tap", function () {
        	if (!busy.isVisible()) {
				busy.show();
			}
            mor.ticket.viewControl.queryFinishedOrderType = '4';//travel finished query finished order
            mor.ticket.util.changePage(vPathCallBack() + "advanceQueryOrder.html");
            mor.queryOrder.views.advanceQuery.queryOrderTypeFlag = true;
            return false;
        });
    }

    function registerUnfinishedOrderRefreshButtonListener() {
        jq("#refreshUnfinishedorder").bind("tap", function () {
            initQueryOrder();
            return false;
        });
    }
    
    function registerTransferOrderDisplayBtnListener(){
    	jq("#finishedOrdersDiv").bind("tap",function(){
    		jq("#notFinishedOrdersDiv").removeClass("qry_order_tab_active");
    		jq("#finishedOrdersDiv").addClass("qry_order_tab_active");
    		jq("#notFinshedOrders").hide();
    		jq("#noFinishedOrdersQuery").hide();
    		jq("#finishedOrders").show();
    	});
    	
    	jq("#notFinishedOrdersDiv").bind("tap",function(){
        	jq("#notFinishedOrdersDiv").addClass("qry_order_tab_active");
    		jq("#finishedOrdersDiv").removeClass("qry_order_tab_active");
    		jq("#finishedOrders").hide();
    		jq("#notFinshedOrders").show();
    		jq("#noFinishedOrdersQuery").show();
    		initQueryOrder();
    		return false;
    	});
    }

    var unfinishedOrdersDetailListTemplate =
    	"{{~it :order:index}}" +
        "<li data-index='{{=index}}'><a class='a_btn'>" +
        "<span>订单时间：{{=mor.ticket.util.getLocalDateString3(order.order_date)}}</span>" +
        "总张数：" +
        "<span style='color:#CF5618;'>{{=order.myTicketList.length}}</span>" +
        "</a></li>" +
        "{{~}}";
    var generateUnfinishedOrdersDetailList = doT.template(unfinishedOrdersDetailListTemplate);
})();