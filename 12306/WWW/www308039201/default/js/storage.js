
/* JavaScript content from js/storage.js in folder common */
/**
 * @discription 对 localstorage 对象封装，加入异常捕获与定制处理功能
 * @author      已过
 */
window.ticketStorage = {
		setItem : function(key, value, fn/*异常处理函数*/){
			try{
				localStorage.setItem(key, value);
			}catch(e){
				//console.log(e.message);
				fn && fn(key, value);
			}
		},
		getItem : function(key, fn/*异常处理函数*/){
			try{
				return localStorage.getItem(key);
			}catch(e){
				//console.log(e.message);
				fn && fn(key);
			}
		},
		removeItem: function(key,fn){
			try{
				return localStorage.removeItem(key);
			}catch(e){
				//console.log(e.message);
				fn && fn(key);
			}
		},
		pushmessageNum:function(){
			var regex = /pushmessage/;
			var storage = window.localStorage;
			var count = 0;
			for(var i =0;i<storage.length;i++){
				if(regex.test(storage.key(i))){
					count++;
				}
			}
			return count;
		}
		/* 原版先不做推送消息的删除，保留所有记录（因为若删除推送的消息，需要为每一个推送的消息按照时间戳进行冒泡排序，并需要根据storage中的键值重新分配，暂时先不做。）
		if(count >=10){
			for(var i = 10;i<count;i++){
				
			}
		}
		*/
};

