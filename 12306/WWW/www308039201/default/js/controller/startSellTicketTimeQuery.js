
/* JavaScript content from js/controller/startSellTicketTimeQuery.js in folder common */
(function(){
	
	jq("#startSellTicketTimeQueryView").live("pageinit", function(){		
		registerstartSellTicketTimeQueryBtnLisener();
		registerStartSellTicketTimeQueryBackButtonHandler();
		registerStationNameInputClickHandler();
	});
	jq("#startSellTicketTimeQueryView")
	.live(
			"pageshow",
			function() {
				jq("#startSellTicketTimeQueryView").css("pointer-events", "auto");
				mor.ticket.viewControl.tab1_cur_page = "startSellTicketTimeQuery.html";
				mor.ticket.viewControl.current_tab = "startSellTicketTimeQueryTab";
				if(mor.ticket.startSellTicketTimeQuery.start_sell_ticket_time_station_telecode !=""){
					var cache = mor.ticket.cache;
					jq("#startSellTicketTimeQueryStationName").val(cache.getStationNameByCode(mor.ticket.startSellTicketTimeQuery.start_sell_ticket_time_station_telecode));
				}
				//预售期提示
				if(mor.ticket.viewControl.tipsFlag == true){
					var date = mor.ticket.util.getNewDate();
			    	var weekDay = new Array("星期日","星期一","星期二","星期三","星期四","星期五","星期六")[date.getDay()];
			    	var currentDate = date.format("yyyy年MM月dd日") ;
			    	var reservePeriod = parseInt(window.ticketStorage
							.getItem("reservePeriod_1" ), 10);
			    	var reservePeriodDate = new Date((+date)+24*60*60*1000*reservePeriod);
			    	var reservePeriodWindowDate = new Date((+date)+24*60*60*1000*(reservePeriod-2));
					var reservePeriodTips = reservePeriodDate.format("MM月dd日");
					var reservePeriodWindowTips = reservePeriodWindowDate.format("MM月dd日");
					jq("#startSellTicketTimeQueryTips").append("<span>今天是"+currentDate + " " + weekDay + ",各渠道预售期如下:</span><br>");
					jq("#startSellTicketTimeQueryTips").append("<span>1.互联网/手机及电话订票: 售至"+reservePeriodTips+".</span><br>");
					jq("#startSellTicketTimeQueryTips").append("<span>2.车站窗口、代售点及自动售票机: 售至"+reservePeriodWindowTips+".</span><br>");
//					jq("#startSellTicketTimeQueryTips").append("<span>3.全国高铁动车组车票C、D字头列车起售时间为11:00.</span><br><span>4.G字头列车起售时间为14:00.</span>");
					mor.ticket.viewControl.tipsFlag = false;
				}
				
			});
	function registerstartSellTicketTimeQueryBtnLisener(){
		jq("#startSellTicketTimeQueryBtn").bind("tap", sendstartSellTicketTimeQueryRequest);
	};
	
	function sendstartSellTicketTimeQueryRequest(e) {
		if(jq("#startSellTicketTimeQueryStationName").val()===""
			|| jq("#startSellTicketTimeQueryStationName").val()===undefined
			|| jq("#startSellTicketTimeQueryStationName").val()===null){
			mor.ticket.util.alertMessage("请填车站");
			mor.ticket.util.changePage(vPathCallBack()+"startSellTicketTimeQuery.html");
			return;
		}
		var util = mor.ticket.util;
		var cache = mor.ticket.cache;
		var station_name = cache.getStationNameByCode(mor.ticket.startSellTicketTimeQuery.start_sell_ticket_time_station_telecode);
		var commonParameters = {
			'train_station_name': station_name,
			'station_telecode': mor.ticket.startSellTicketTimeQuery.start_sell_ticket_time_station_telecode,
			'train_start_date': util.getNewDate().format("yyyy-MM-dd")
		};
		
		var invocationData = {
			adapter: mor.ticket.viewControl.adapterUsed,
			procedure: "startSellTicketTimeQuery"
		};
		
		var options =  {
				onSuccess: requestStartSellTicketTimeQuerySucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestStartSellTicketTimeQuerySucceeded(result) {
		
			var invocationResult = result.invocationResult;
			if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				var newCacheQssDTO=invocationResult.cacheQssDTO.sort(function(a,b){
					if(a.time < b.time) return -1;
					if(a.time > b.time) return 1;
					return 0;
				});
				jq("#sellTicketTimeList").html(generateSellTicketTimeListContent(newCacheQssDTO));
				jq("#sellInfoNum").html(invocationResult.cacheQssDTO.length);
			}else {	
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}
		}
	function registerStartSellTicketTimeQueryBackButtonHandler() {
		jq("#startSellTicketTimeQueryBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage(vPathCallBack() + "moreOption.html");
			return false;
		});
	}
	function registerStationNameInputClickHandler() {

		jq("#startSellTicketTimeQueryStationName").bind("tap", function(e) {
			e.stopImmediatePropagation();
			jq("#startSellTicketTimeQueryView").css("pointer-events", "none");
			mor.ticket.util.changePage(vPathCallBack() + "selectStation.html");
			return false;
		});
	}
	;
	generateSellTicketTimeListContent
	var sellTicketTimeListTemplate = "<div class='sellInfo'>"
		+ "<div></div>"
		+ "<div>共查到&nbsp<span id='sellInfoNum'></span>&nbsp条起售信息</div>"
		+ "<div></div>"
		+ "</div>"
		+ "{{~it :sellTicketTimeArray:index}}"
		+ "<div>"
		+ "<span class='sellTimeStationName'>{{=sellTicketTimeArray.station_name +' :'}}</span>"
		+ "<span class='sellTime'>{{=sellTicketTimeArray.time}}</span>"
		+ "</div>"
		+ "{{~}}";
	var generateSellTicketTimeListContent = doT.template(sellTicketTimeListTemplate);
	
})();