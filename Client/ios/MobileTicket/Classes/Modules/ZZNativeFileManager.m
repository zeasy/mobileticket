//
//  ZZNativeFileManager.m
//  MobileTicket
//
//  Created by EASY on 15/11/17.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZNativeFileManager.h"

@implementation ZZNativeFileManager

RCT_EXPORT_MODULE(NativeFileManager)


-(NSString *) cacheDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
}

-(NSString *) documentDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}

-(NSString *) temporaryDirectory {
	return NSTemporaryDirectory();
}

RCT_EXPORT_METHOD(cacheDirectory:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	NSString *dir = [self cacheDirectory];
	success(@[dir]);
}

RCT_EXPORT_METHOD(documentDirectory:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	NSString *dir = [self documentDirectory];
	success(@[dir]);
}

RCT_EXPORT_METHOD(temporaryDirectory:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	NSString *dir = [self temporaryDirectory];
	success(@[dir]);
}

RCT_EXPORT_METHOD(mkdir:(NSString *) path successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	NSError *error = nil;
	BOOL result = [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:@{} error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[@(result)]);
	}
}

RCT_EXPORT_METHOD(rmdir:(NSString *) path successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	NSError *error = nil;
	BOOL result = [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[@(result)]);
	}
}

RCT_EXPORT_METHOD(exists:(NSString *) path successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	NSError *error = nil;
	BOOL result = [[NSFileManager defaultManager] fileExistsAtPath:path];
	if (error) {
		fail(error);
	} else {
		success(@[@(result)]);
	}
}

RCT_EXPORT_METHOD(copy:(NSString *) src dest:(NSString *) dest successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	NSError *error = nil;
	BOOL result = [[NSFileManager defaultManager] copyItemAtPath:src toPath:dest error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[@(result)]);
	}
}

RCT_EXPORT_METHOD(move:(NSString *) src dest:(NSString *) dest successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	NSError *error = nil;
	BOOL result = [[NSFileManager defaultManager] moveItemAtPath:src toPath:dest error:&error];
	if (error) {
		fail(error);
	} else {
		success(@[@(result)]);
	}
}

@end
