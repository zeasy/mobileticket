/**
 * @providesModule CCNavigationBar
 * Created by easy on 16/3/12.
 */
import React,{View,Text,TouchableOpacity,Navigator,NavigatorIOS,StatusBar,Platform} from 'react-native';
let iOS = Platform.OS === 'ios';

let CCNavigationBarItem = React.createClass({

    onLeftButtonPress : function() {
        if (this.props.leftButtonTitle && this.props.onLeftButtonPress) {
            this.props.onLeftButtonPress();
        } else if (this.props.backButtonTitle && this.props.onBackButtonPress) {
            this.props.onBackButtonPress();
        }
    },

    onRightButtonPress : function() {
        if (this.props.rightButtonTitle && this.props.onRightButtonPress) {
            this.props.onRightButtonPress();
        }
    },

    render : function() {
        let hidden = this.props.navigationBarHidden != undefined ? this.props.navigationBarHidden : false;

        let height = iOS ? 64 : 44;
        let marginTop = iOS ? 20 : 0;
        let bar = (
            <View style={{flexDirection:'row',height:height,backgroundColor:this.props.barTintColor||'white'}}>
                <TouchableOpacity  style={{flex:1,justifyContent:'center',marginTop:marginTop,marginLeft:10}}
                                   onPress={this.onLeftButtonPress}
                                   activeOpacity={0.7}
                >
                    <Text style={{alignSelf:'flex-start',fontSize:16,color:this.props.tintColor||'blue'}}>{this.props.leftButtonTitle||this.props.backButtonTitle}</Text>
                </TouchableOpacity>

                <View style={{flex:1,justifyContent:'center',marginTop:marginTop}}>
                    <Text style={{alignSelf:'center',fontSize:16,fontWeight:'bold',color:this.props.titleTextColor||'black'}}>{this.props.title}</Text>
                </View>

                <TouchableOpacity  style={{flex:1,justifyContent:'center',marginTop:marginTop,marginRight:10}}
                                   onPress={this.onRightButtonPress}
                                   activeOpacity={0.7}
                >
                    <Text style={{alignSelf:'flex-end',fontSize:16,color:this.props.tintColor||'blue'}}>{this.props.rightButtonTitle}</Text>
                </TouchableOpacity>
            </View>
        );
        return (
            <View {...this.props}>
                {!hidden && bar}
            </View>
        );
    }
});
module.exports = CCNavigationBarItem;