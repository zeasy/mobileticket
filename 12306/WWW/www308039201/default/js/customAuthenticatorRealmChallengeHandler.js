
/* JavaScript content from js/customAuthenticatorRealmChallengeHandler.js in folder common */
var customAuthenticatorRealmChallengeHandler = WL.Client.createWLChallengeHandler("morCustomRealm");

customAuthenticatorRealmChallengeHandler.handleChallenge = function(obj){
	cordova.exec(function (token){
			customAuthenticatorRealmChallengeHandler.submitChallengeAnswer(token);
		}, 
		function (){alert("Authentication failed");
		}, 
		"Auth", "getToken", [obj["WL-Challenge-Data"]]);
	return true;
};

customAuthenticatorRealmChallengeHandler.handleFailure = function(err) {
    WL.SimpleDialog.show(WL.ClientMessages.wlclientInitFailure, WL.ClientMessages.authenticityFailure, [ {
        text : WL.ClientMessages.close,
        handler : function() {
        }
    } ]);
};