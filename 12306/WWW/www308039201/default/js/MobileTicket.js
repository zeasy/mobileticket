
/* JavaScript content from js/MobileTicket.js in folder common */
/*
 * Licensed Materials - Property of IBM
 * 5725-G92 (C) Copyright IBM Corp. 2006, 2012. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

// Worklight comes with the jQuery 1.8.1 framework bundled inside. If you do not want to use it, please comment out the line below.
//window.$ = window.jQuery = WLJQ;
var busy = new WL.BusyIndicator(null, {
	text : "加载中...",
	opacity : 0.5,
	fullScreen : false,
	boxLength : 2.0
});

function wlCommonInit() {

	/*
	 * Application is started in offline mode as defined by a connectOnStartup
	 * property in initOptions.js file. In order to begin communicating with
	 * Worklight Server you need to either:
	 *
	 * 1. Change connectOnStartup property in initOptions.js to true. This will
	 * make Worklight framework automatically attempt to connect to Worklight
	 * Server as a part of application start-up. Keep in mind - this may
	 * increase application start-up time.
	 *
	 * 2. Use WL.Client.connect() API once connectivity to a Worklight Server is
	 * required. This API needs to be called only once, before any other
	 * WL.Client methods that communicate with the Worklight Server. Don't
	 * forget to specify and implement onSuccess and onFailure callback
	 * functions for WL.Client.connect(), e.g:
	 *
	 * WL.Client.connect({ onSuccess: onConnectSuccess, onFailure:
	 * onConnectFailure });
	 *
	 */

	var util = mor.ticket.util;
	util.userDef = jq.Deferred();
	util.syncDef = util.syncDef || jq.Deferred();

	// Common initialization code goes here
//	if (!busy.isVisible()) {
//		busy.show();
//	}

	// 摇一摇
	if (util.isAndroid()) {
		registEventForWave();
	}
	document.addEventListener(WL.Events.WORKLIGHT_IS_DISCONNECTED,
			handleConnectionDown, false);
	initCommonData();

	prepareCacheMap();

	syncLocalCache();
	registerTabListener();
	innitLoginUserUserNameAndPassWord();
	handleAndriodBackbutton();
	handleNativeCodeCallBack();

	jq.when(util.userDef, util.syncDef).done(function() {
		if (busy.isVisible()) {
			// WL.Logger.debug('******* Hide busy indicator in wlCommonInit.');
			busy.hide();
		}
	});

	setTimeout(function() {
		// WL.Client.__hideBusy();
		if (util.userDef.state() != 'resolved') {
			// WL.Logger.debug('------------------- reslove util.userDef.');
			util.userDef.resolve();
		}
		if (util.syncDef.state() != 'resolved') {
			// WL.Logger.debug('------------------- reslove util.syncDef.');
			util.syncDef.resolve();
		}
		if (busy.isVisible()) {
			// WL.Logger.debug('------------------- Hide busy indicator in
			// timer.');
			busy.hide();
		}
	}, 6000);

	// 自动登录
	//registerAutoLoginHandler();

	jq("#content").css("pointer-events", "auto");
}

// authenticity challenge failed callback
if (wl_authenticityChallengeHandler.handleFailure) {
	wl_authenticityChallengeHandler.handleFailure = function(err) {
		WL.SimpleDialog.show(WL.ClientMessages.wlclientInitFailure,
				WL.ClientMessages.authenticityFailure, [ {
					text : WL.ClientMessages.exit,
					handler : function() {
						// exit App if authenticity chanllenge failed.
						WL.App.close();
					}
				} ]);
	};
}

function registEventForWave() {
	if (window.DeviceMotionEvent) {
		window.addEventListener('devicemotion', deviceMotionHandler, true);
	}
	// var options={frequency:100};
	// navigator.accelerometer.watchAcceleration(deviceMotionHandler, null,
	// options);
}

var SHAKE_THRESHOLD = 10;
var last_update = 0;
var last_x, last_y, last_z;
function deviceMotionHandler(eventData) {
	var curTime = new Date().getTime();
	// different time
	if ((curTime - last_update) > 100) {
		var acceleration = eventData.accelerationIncludingGravity;
		var x = acceleration.x;
		var y = acceleration.y;
		var z = acceleration.z;
		var diffTime = curTime - last_update;
		var rate = Math.abs(x + y + z - last_x - last_y - last_z) / 3 / diffTime * 100;
		if (rate > SHAKE_THRESHOLD) {
			waveHandler();
		}
		last_update = curTime;
		last_x = x;
		last_y = y;
		last_z = z;
	}
}

function waveHandler() {
	if (document.activeElement.nodeName == 'INPUT'
			|| document.activeElement.nodeName == 'TEXTAREA') {
		navigator.notification.vibrate(300);
		if(mor.ticket.util.isAndroid()){
			WL.SimpleDialog.show("温馨提示", "是否确认清空所有输入?", [ 
        		  {
        			text : "确认",
        			handler : function() {
        				document.activeElement.value = "";
        			}
        		},
        		{
        			text : "取消",
        			handler : function() {
        			}
        		}
        		]);
		}
		else{
			WL.SimpleDialog.show("温馨提示", "是否确认清空所有输入?", [ 
        		 {
        			text : "取消",
        			handler : function() {
        			}
        		}, {
        			text : "确认",
        			handler : function() {
        				document.activeElement.value = "";
        			}
        		} ]);
		}
		
	}
}

function handleConnectionDown() {
	if (mor.ticket.util.isPreview) {
		return;
	}

	WL.Device.getNetworkInfo(function(networkInfo) {
		if (busy.isVisible()) {
			busy.hide();
		}
		if (networkInfo.isNetworkConnected == "false") {
			WL.SimpleDialog.show("温馨提示", "哎呀，您的网络有问题，请检查网络连接。", [ {
				text : '确定',
				handler : function() {
				}
			} ]);
		} else {
			WL.SimpleDialog.show("温馨提示", "哎呀，您的网络好像有问题，请检查网络连接。", [ {
				text : '确定',
				handler : function() {
				}
			} ]);
		}
	});
}

function handleNativeCodeCallBack() {
	window.plugins.childBrowser.onCounterDialogOk = mor.ticket.payment.onCounterDialogOk;
	window.plugins.childBrowser.onOrderList = mor.ticket.payment.orderList;
	window.plugins.childBrowser.onOrderComplete = mor.ticket.payment.orderComplete;
	window.orderComplete = mor.ticket.payment.orderComplete;
}

function vPathCallBack() {

	var pat = RegExp('views');
	var path = null;
	var Hpath = window.location;
	if (pat.test(Hpath)) {
		path = "../views/";
	} else {
		path = "views/";
	}
	return (path);
}

function vPathViewCallBack() {

	var pat = RegExp('views');
	var path = null;
	var Hpath = window.location;
	if (pat.test(Hpath)) {
		path = "../";
	} else {
		path = "";
	}
	return (path);
}

// 1. 根据当前路径返回基准路径.
// 2. 如果提供目标路径，返回完整的目标路径地址。
function getViewBasePath(target) {
	var base = '';
	if (/views/.test(window.location)) {
		base = '../';
	} else {
		base = '';
	}
	if(!target) {
		return base;
	} else {
		if (/^MobileTicket\.html$/.test(target)) {
			return base + target;
		} else {
			return base + 'views/' + target;
		}
	}
}


function registerTabListener() {	
	var footTabMapping = {
	    'bookTicketTab': {
	    	needLogin: false,
	    	url: 'MobileTicket.html'
	    },
	    'queryOrderTab': {
	    	needLogin: true,
	    	url: 'views/queryOrder.html'
	    },
	    'my12306Tab': {
	    	needLogin: true,
	    	url: 'views/my12306.html'
	    },
	    'moreOptionsTab': {
	    	needLogin: false,
	    	url: 'views/moreOption.html'
	    }
	};
	var footTabIds = [];
	jq.each(footTabMapping, function(key) {
		footTabIds.push(key);
	});
	
	jq('.footer_bar .f_nav ul li a').live("tap", function(e) {
		//e.stopImmediatePropagation();
		var tab = jq(this);
		var id = tab.attr('id');
		if (id === 'bookTicketTab') {
			jq("#bookTicketTab").addClass("ui-btn-active ui-state-persist");	
		}
		if (!id || footTabIds.indexOf(id) < 0) return;
		
		var tabObj = footTabMapping[id];

		if (mor.ticket.viewControl.current_tab == id) {
			return false;
		}
	
		mor.ticket.viewControl.current_tab = id;
		resetTab();
		
		if (tabObj.needLogin) {
			if (mor.ticket.loginUser.isAuthenticated != "Y" && window.ticketStorage.getItem("autologin") != "true") {
				mor.ticket.viewControl.session_out_page= tabObj.url;
				mor.ticket.util.changePage(getViewBasePath() + "views/loginTicket.html");
				return;
			} 
		}
	    mor.ticket.util.changePage(getViewBasePath() + tabObj.url);
		return false;
	});
	
}

function resetTab() {
	var activeClass = 'ui-btn-active ui-state-persist';
	//jq("#bookTicketTab").removeClass(activeClass);
	jq("#queryOrderTab").removeClass(activeClass);
	jq("#my12306Tab").removeClass(activeClass);
	jq("#moreOptionsTab").removeClass(activeClass);

}
// invoke when press android's back button

function handleAndriodBackbutton() {
	WL.App.overrideBackButton(backFunc);
}

function backFunc() {
	// 按退出键隐藏键盘,fix issue for android2.3

	if (mor.ticket.util.isAndroid()) {
		if (jq.mobile.activePage.find("input").is(":focus")) {
			jq("input:focus")[0].blur();
			// jq.mobile.activePage.find("input").blur();
			return false;
		}
	}
	
	var currentPage = jq.mobile.activePage;
	var backButton = currentPage.find(".ui-header").find(".ui-btn-left");
	if(backButton.length != 0 && backButton.css("display") != "none"){
		backButton.tap();
	}else{
		WL.SimpleDialog.show("温馨提示", "您是要退出应用吗?", [ 
		{
			text : "确认",
			handler : quitHandler
		},
		{
			text : "取消",
			handler : function() {
			}
		}
		]);
	}
}
function quitHandler() {
	WL.Logger.debug("quit button pressed");
	WL.Client.logout();
	WL.App.close();
}

//初始化mor.ticket.loginUser.username/password
function innitLoginUserUserNameAndPassWord(){
	if(window.ticketStorage.getItem("username")==null
			|| window.ticketStorage.getItem("username")==undefined
			|| window.ticketStorage.getItem("username")==""){
		mor.ticket.loginUser.username = "";
	}else{
		mor.ticket.loginUser.username = window.ticketStorage.getItem("username");
	}
	if(mor.ticket.loginUser.isKeepUserPW === "true" || mor.ticket.loginUser.isKeepUserPW == true){
		WL.EncryptedCache.open("wlkey", true,function(){
			WL.EncryptedCache.read("userPW", function(value) {
				if(value=="*"){
					value="";
				}
				mor.ticket.loginUser.password = value;
			},function() {
//				fail();
				WL.Logger.debug("Failed to read to ticketStorage");
				WL.Logger.debug('*******  resolve util.userDef in read failed.');
				// mor.ticket.util.userDef.resolve();
			});
		},function(){
			WL.Logger.debug("Failed to open to ticketStorage");
			WL.Logger.debug('*******  resolve util.userDef in open failed.');
		});
		
		WL.EncryptedCache.close(function() {}, function() {});
	}
}
function prepareCacheMap() {
	// init station map
	var jvFmt = mor.ticket.util.jsVersionFormat;
	if (window.ticketStorage.getItem("stations") != null && jvFmt(mor.ticket.cache.station_version) < jvFmt(window.ticketStorage.getItem("station_version"))) {
		mor.ticket.cache.stations = JSON.parse(window.ticketStorage
				.getItem("stations"));
	}
	var stations = mor.ticket.cache.stations;
	var stationMap = mor.cache.stationMap;
	for ( var i = 0; i < stations.length; i++) {
		stationMap[stations[i].id] = stations[i].value;
	}
	;
	// init province id value
	var provinceList = mor.ticket.cache.province;
	var provinceMap = mor.cache.provinceMap;
	for ( var y = 0; y < provinceList.length; y++) {
		provinceMap[provinceList[y].id] = provinceList[y].value;
	}
	;

}

function initCommonData() {

	// fetch device info
	var common = mor.ticket.common;
	common["baseDTO.os_type"] = deviceDetection();
	if (mor.ticket.util.isPreview) {
		common["baseDTO.device_no"] = '1213123123';
	} else {
		common["baseDTO.device_no"] = device.uuid;
	}
	// alert("device.version"+device.version);
	// convertCity();
	// common["baseDTO.mobile_no"]=device.? to do , can not get user mobile num
	// now
}

function syncLocalCache() {
	// WL.Logger.debug("sysCache called");
	var util = mor.ticket.util;
	/*
	 * var commonParameters = util.prepareRequestCommonParameters(); var
	 * invocationData = { adapter: "CARSMobileServiceAdapterV2", procedure:
	 * "sysCache", parameters: [commonParameters] };
	 */
	var invocationData = {
		adapter : mor.ticket.viewControl.adapterUsed,
		procedure : "sysCache"
	};

	var options = {
		onSuccess : requestSucceeded,
		onFailure : requestSysCacheFailure
	};
	mor.ticket.viewControl.show_busy = true;

	mor.ticket.util.invokeWLProcedure(null, invocationData, options);
	// WL.Client.invokeProcedure(invocationData, options);

}
function requestSysCacheFailure(){
	var util = mor.ticket.util;
	var invocationData = {
		adapter : mor.ticket.viewControl.adapterUsed,
		procedure : "sysCache"
	};
	var options = {
		onSuccess : requestSucceeded,
		onFailure : requestsysCacheFailure2
	};
	mor.ticket.viewControl.show_busy = false;
	mor.ticket.util.invokeWLProcedure(null, invocationData, options);
}

function requestsysCacheFailure2(result) {
	if (busy.isVisible()) {busy.hide();}
	var canStudentTicket = window.ticketStorage.getItem("canStudentTicket");
	if(canStudentTicket == undefined || canStudentTicket == null){
		window.ticketStorage.setItem("summer_day", "0601-0930");
		window.ticketStorage.setItem("winter_day", "1201-0331");
		window.ticketStorage.setItem("canStudentTicket", "true");
	}
	
	var isAdverStart = window.ticketStorage.getItem("isAdverStart");
	if(isAdverStart == undefined || isAdverStart == null){
		window.ticketStorage.setItem("isAdverStart","Y");
	}
	var payFinishBlock1 = window.ticketStorage.getItem("payFinishBlock1");
	if(payFinishBlock1 == undefined || payFinishBlock1 == null){
		window.ticketStorage.setItem("payFinishBlock1","taxiServiceCss");
	}
	var payFinishBlock2 = window.ticketStorage.getItem("payFinishBlock2");
	if(payFinishBlock2 == undefined || payFinishBlock2 == null){
		window.ticketStorage.setItem("payFinishBlock2","hotelServiceCss");
	}
	var payFinishBlock3 = window.ticketStorage.getItem("payFinishBlock3");
	if(payFinishBlock3 == undefined || payFinishBlock3 == null){
		window.ticketStorage.setItem("payFinishBlock3","");
	}
	var payFinishBlock4 = window.ticketStorage.getItem("payFinishBlock4");
	if(payFinishBlock4 == undefined || payFinishBlock4 == null){
		window.ticketStorage.setItem("payFinishBlock4","");
	}
	var taxiServiceCoupon = window.ticketStorage.getItem("taxiServiceCoupon");
	if(taxiServiceCoupon == undefined || taxiServiceCoupon == null){
		window.ticketStorage.setItem("taxiServiceCoupon","Y");
	}
	var reservePeriodOri = window.ticketStorage.getItem("reservePeriod_1");
	if(reservePeriodOri != undefined && reservePeriodOri != null){
		return;
	}
	
	// 预售期
	var today = mor.ticket.util.getNewDate();
	var from_period = mor.ticket.util.getNewDate().format("yyyy-MM-dd hh:mm:ss");
	today.setDate(today.getDate() + 59);
	var to_period = today.format("yyyy-MM-dd hh:mm:ss");
	reservePeriodList = [{ticket_type:"1",from_period:from_period,to_period:to_period},
	                     {ticket_type:"3",from_period:from_period,to_period:to_period}];
		
	for ( var i = 0; i < reservePeriodList.length; i++) {
		var ticket_type = reservePeriodList[i].ticket_type;
		if (ticket_type == "1" || ticket_type == "2" || ticket_type == "3" || ticket_type == "n") {// 只获取到成人（ticket_type=1）的时间
			var util = mor.ticket.util;
			var startDate = reservePeriodList[i].from_period;
			var endDate = reservePeriodList[i].to_period;
			var from_date = util.setMyDate(startDate);
			var to_date = util.setMyDate(endDate);
			var reservePeriod = ((to_date - from_date) / 86400000);
			var from_date_array = startDate.split("-");
			var to_date_array = endDate.split("-");
			var fromMons = parseInt(from_date_array[0],10) * 12 + parseInt(from_date_array[1],10);
			var toMons = parseInt(to_date_array[0],10) * 12 + parseInt(to_date_array[1],10);
			var reserveMons = [];
			var monLen = toMons - fromMons;
			window.ticketStorage.setItem("reservePeriod_" + ticket_type,
						reservePeriod);
			reserveMons.push(from_date.format("yyyyMM"));
			from_date.setDate(1);
			for(var j = 0;j < monLen;j++){
				var newDate = new Date(from_date.setMonth(from_date.getMonth() + 1));
				reserveMons.push(newDate.format("yyyyMM"));
			}
			window.ticketStorage.setItem("reservePeriodMonth_" + ticket_type,
					JSON.stringify(reserveMons));
		}
	}
	var channels = window.ticketStorage.getItem("channels");
	var apiServer = window.ticketStorage.getItem("apiServer");
	var dsServer = window.ticketStorage.getItem("dsServer");
	var recServer = window.ticketStorage.getItem("recServer");
	var appKey = window.ticketStorage.getItem("appKey");
	var isPush = window.ticketStorage.getItem("isPush");
	
	if(channels ==undefined ||apiServer ==undefined ||dsServer ==undefined ||recServer ==undefined ||appKey ==undefined||isPush ==undefined){
		return;
	}
	if(isPush == "N"){//不推送的时候disable
		sdk.disable(function(err){
			if(err) {
				//alert(err.message);
			} 
		});
	}else{
		//初始化推送参数
		window.plugins.AnDeviceManager.setHost(recServer, null);
		var conType = false;
		if(mor.ticket.util.isAndroid()){
			conType = true;
		}
		sdk.initialize(appKey, function(err) {
			if(err) {
				//alert(err);
			} else {
				sdk.setServerHosts(apiServer, dsServer, function(err) {
					if(err) {
						//alert(err);
					}
					//注册广播频道
					registerChannelsall();
				});
			}
		}, {secureConnection:conType,alert: true, badge: true, sound: true});

	}
}
//系统更新下载
function downLoadPage(){
	var sUserAgent = navigator.userAgent.toLowerCase();
	var isIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
	var isPadOs = sUserAgent.match(/ipad/i) == "ipad";
	var isAndroid = sUserAgent.match(/android/i) == "android";
	if(isAndroid){
		return navigator.app.loadUrl('http://dynamic.12306.cn/otn/appDownload/androiddownload', {openExternal:true});
	}
	if(isIphoneOs || isPadOs){
		return window.open("https://itunes.apple.com/cn/app/tie-lu12306/id564818797?mt=8", "_system");
	}
}
//系统更新提示
function downLoadNewSystem(){
	if(mor.ticket.util.isAndroid()){
		WL.SimpleDialog.show(
				"温馨提示", 
				"新版本已发布，欢迎下载。", 
				[{text : '下载', handler: function(){downLoadPage();}},
				 {text : '取消', handler: function(){}}]
		);
	}else{
		WL.SimpleDialog.show(
				"温馨提示", 
				"新版本已发布，欢迎下载。", 
				[{text : '取消', handler: function(){}},
				 {text : '下载', handler: function(){downLoadPage();}}]
		);
	}
}
function requestSucceeded(result) {
    //add by yiguo
	var jvFmt = mor.ticket.util.jsVersionFormat;
	

	var serverCache = result.invocationResult;
	var common = mor.ticket.common;
	common["baseDTO.time_offset"] = Date.now()
			- new Date(serverCache.serverTime);
	// check app version 
	if(mor.ticket.util.isIPhone()){
		common["baseDTO.os_type"] = 'i';
	}
	if(mor.ticket.util.isAndroid()){
		common["baseDTO.os_type"] = 'a';
	}
	var os_type = common["baseDTO.os_type"];
	var app_version_no = WL.StaticAppProps.APP_VERSION == null ? "0" 
			: WL.StaticAppProps.APP_VERSION;
	// android app
	if(os_type === 'a'){
		var android_version_no = serverCache.versionNewAndNo;
		common["baseDTO.version_no_new"] = serverCache.versionNewAndNo;
		if(android_version_no > app_version_no){
			common["baseDTO.app_need_update"] = "true";
			common["baseDTO.app_download_url"] = serverCache.versionNewAndUrl;
			//弹出提示更新信息
			downLoadNewSystem();
		}
	}
	// ios app
	if(os_type === 'i'){
		var ios_version_no = serverCache.versionNewIosNo;
		common["baseDTO.version_no_new"] = serverCache.versionNewIosNo;
		if(ios_version_no > app_version_no){
			common["baseDTO.app_need_update"] = "true";
			common["baseDTO.app_download_url"] = serverCache.versionNewIosUrl;
			//弹出提示更新信息
			downLoadNewSystem();
		}
	}

	window.ticketStorage.setItem("isSendTicket", serverCache.isSendTicket);
	window.ticketStorage.setItem("isInsurance", serverCache.isInsurance);
    //动卧启动
	window.ticketStorage.setItem("isDwUseFlag", serverCache.isDwUseFlag);
	// 自动登录
	//mod by yigou
	//registerAutoLoginHandler();

	window.ticketStorage.setItem("serverTime", serverCache.serverTime);
	// 预售期
	var reservePeriodList = serverCache.reservePeriodList;
	// 如果获取服务器预售期失败时，默认设置预售期60天
	if(reservePeriodList == undefined || reservePeriodList == null){
		var today = mor.ticket.util.getNewDate();
		var from_period = mor.ticket.util.getNewDate().format("yyyy-MM-dd hh:mm:ss");
		today.setDate(today.getDate() + 59);
		var to_period = today.format("yyyy-MM-dd hh:mm:ss");
		reservePeriodList = [{ticket_type:"1",from_period:from_period,to_period:to_period},
		                     {ticket_type:"3",from_period:from_period,to_period:to_period}];
	}
	var lastStudentDay;
	for ( var i = 0; i < reservePeriodList.length; i++) {
		var ticket_type = reservePeriodList[i].ticket_type;
		if (ticket_type == "1" || ticket_type == "2" || ticket_type == "3" || ticket_type == "4") {// 只获取到成人（ticket_type=1）的时间
			var util = mor.ticket.util;
			var startDate = reservePeriodList[i].from_period;
			var endDate = reservePeriodList[i].to_period;
			var from_date = util.setMyDate(startDate);
			var to_date = util.setMyDate(endDate);
			if(ticket_type == "3"){
				lastStudentDay = to_date.format("yyyymmdd");
			}
			var reservePeriod = ((to_date - from_date) / 86400000);
			var from_date_array = startDate.split("-");
			var to_date_array = endDate.split("-");
			var fromMons = parseInt(from_date_array[0],10) * 12 + parseInt(from_date_array[1],10);
			var toMons = parseInt(to_date_array[0],10) * 12 + parseInt(to_date_array[1],10);
			var reserveMons = [];
			var monLen = toMons - fromMons;
			window.ticketStorage.setItem("reservePeriod_" + ticket_type,
						reservePeriod);
			reserveMons.push(from_date.format("yyyyMM"));
			from_date.setDate(1);
			for(var j = 0;j < monLen;j++){
				var newDate = new Date(from_date.setMonth(from_date.getMonth() + 1));
				reserveMons.push(newDate.format("yyyyMM"));
			}
			window.ticketStorage.setItem("reservePeriodMonth_" + ticket_type,
					JSON.stringify(reserveMons));
		}
	}
	
	// 学生票寒暑假区间
	var summer_day = serverCache.summer_day;
	window.ticketStorage.setItem("summer_day",summer_day);
	var winter_day = serverCache.winter_day;
	window.ticketStorage.setItem("winter_day",winter_day);
	var currYear = util.getNewDate().format("yyyy");
	var currDay = util.getNewDate().format("yyyymmdd");
	var summer_day_begin = currYear+summer_day.substr(0,4);
	var summer_day_end = currYear+summer_day.substr(5,4);
	var currMonth = util.getNewDate().getMonth();
	var winter_day_begin,winter_day_end;
	if(currMonth<7){
		winter_day_begin = (parseInt(currYear) - 1)+ winter_day.substr(0,4);
		winter_day_end = currYear+winter_day.substr(5,4);
	}else{
		winter_day_begin = currYear+winter_day.substr(0,4);
		winter_day_end = (parseInt(currYear) + 1)+winter_day.substr(5,4);
	}
	var canStudentTicket = false;
    if(currDay >= summer_day_begin && currDay <= summer_day_end) canStudentTicket = true;
    if(lastStudentDay >= summer_day_begin && lastStudentDay <= summer_day_end) canStudentTicket = true;
    if(currDay >= winter_day_begin && currDay <= winter_day_end) canStudentTicket = true;
    if(lastStudentDay >= winter_day_begin && lastStudentDay <= winter_day_end) canStudentTicket = true;
/*	var studentPeriodMonth = window.ticketStorage.getItem("reservePeriodMonth_3") == null ? [] : 
		JSON.parse(window.ticketStorage.getItem("reservePeriodMonth_3"));
	var currYear = util.getNewDate().format("yyyy");
	// 暑假区间
	if(summer_day != null && summer_day != ""){
		window.ticketStorage.setItem("summer_day", summer_day);
		var summer_months = summer_day.split("-");
		var summer_begin = currYear + summer_months[0].substr(0,2);
		var summer_end = currYear + summer_months[1].substr(0,2);
		for(var i = 0;i<studentPeriodMonth.length;i++){
			if(studentPeriodMonth[i] >= summer_begin && 
					studentPeriodMonth[i] <= summer_end){
				canStudentTicket = true;
				break;
			}
		}
	}
		// 寒假区间
	if(winter_day != null && winter_day != ""){
		window.ticketStorage.setItem("winter_day", winter_day);
		var nextYear = parseInt(currYear) + 1;
		var winter_months = winter_day.split("-");
		var beginMonth = winter_months[0].substr(0,2);
		var winter_begin = currYear + beginMonth;
		if(beginMonth < "12" ){
			winter_begin = nextYear + beginMonth;
		}
		var winter_end = nextYear + winter_months[1].substr(0,2);
		for(var i = 0;i<studentPeriodMonth.length;i++){
			if(studentPeriodMonth[i] >= winter_begin && 
					studentPeriodMonth[i] <= winter_end){
				canStudentTicket = true;
				break;
			}
		}
	}
	*/

	window.ticketStorage.setItem("canStudentTicket", canStudentTicket);
	// 订单确认页面提交时是否显示进入未完成订单配置项
	var isDisplayGoNoFinishedOrder = serverCache.isDisplayGoNoFinishedOrder;
	if(null != isDisplayGoNoFinishedOrder && "" != isDisplayGoNoFinishedOrder){
		window.ticketStorage.setItem("isDisplayGoNoFinishedOrder",isDisplayGoNoFinishedOrder);
	}
	// 支付通道是否启用支付宝新支付通道
	var isNewAlipaySdk = serverCache.isNewAlipaySdk;
	if(null != isNewAlipaySdk && "" != isNewAlipaySdk){
		window.ticketStorage.setItem("isNewAlipaySdk",isNewAlipaySdk);
	}
	// 车票改签新规启用规则 启用日期
	var newRuleStartDate = serverCache.newRuleStartDate;
	if(null != newRuleStartDate && "" != newRuleStartDate){
		window.ticketStorage.setItem("newRuleStartDate",newRuleStartDate);
	}
	// 车票改签新规启用规则 时间间隔
	var newRuleIntervalHours = serverCache.newRuleIntervalHours;
	if(null != newRuleIntervalHours && "" != newRuleIntervalHours){
		window.ticketStorage.setItem("newRuleIntervalHours",newRuleIntervalHours);
	}
	//关于系统通知
	var systemNotice = serverCache.isSystemNotice;
	if(null != systemNotice && "" != systemNotice){
		window.ticketStorage.setItem("systemNotice",systemNotice);
	}
	//重庆北提示
	var  isCUW= serverCache.isCUW;
	if(null != isCUW && "" != isCUW){
		window.ticketStorage.setItem("isCUW",isCUW);
		window.ticketStorage.setItem("cuwMessage",serverCache.cuwMessage);
	}
	//动卧车次
	if(serverCache.dw_train !== undefined  && serverCache.dw_train.length > 0){
		for(var i=0;i<serverCache.dw_train.length;i++){
			mor.ticket.dw_ticket.station_train_code.push(serverCache.dw_train[i]);
		}
	}
	//开车前半小时到两小时之间不能改签退票的车站
	var confirm_order__station_code = serverCache.stop_station_info;
	if(confirm_order__station_code !== undefined  && confirm_order__station_code !== null && confirm_order__station_code !== ""){
		window.ticketStorage.setItem("confirm_order__station_code",confirm_order__station_code);
	}else{
		window.ticketStorage.setItem("confirm_order__station_code","***#*@***#*");
	}
	////开车前半小时到两小时之间不能改签退票的车站的提示信息
	var  confirm_order_station_code_message= serverCache.stop_station_msg;
	if(null !== confirm_order_station_code_message && "" !== confirm_order_station_code_message){
		window.ticketStorage.setItem("confirm_order_station_code_message",confirm_order_station_code_message);
	}else{
		window.ticketStorage.setItem("confirm_order_station_code_message","");
	}
	
	//购票成功后的温馨提示
	var saleTicketMessage = serverCache.saleTicketMessage;
	if(saleTicketMessage.length>0){
		window.ticketStorage.setItem("saleTicketMessage",saleTicketMessage);
	}
	// init seat Type
	var seatTypeMap = mor.cache.seatTypeMap;
	var seatsType = serverCache.seatTypeList;
	for ( var i = 0; i < seatsType.length; i++) {
		seatTypeMap[seatsType[i].seat_type_code] = seatsType[i].seat_type_name;
	}
	;

	// init ticket type
	var ticketType = serverCache.ticketTypeList;
	var ticketTypeMap = mor.cache.ticketTypeMap;
	for ( var i = 0; i < ticketType.length; i++) {
		ticketTypeMap[ticketType[i].ticket_type_code] = ticketType[i].ticket_type_name;
	}
	;

	// init id type
	var idTypeList = serverCache.cardTypeList;
	var idTypeMap = mor.cache.idTypeMap;
	for ( var i = 0; i < idTypeList.length; i++) {
		idTypeMap[idTypeList[i].card_type_code] = idTypeList[i].card_type_name;
	}
	;
	// init tipsMessage
	var tipsMessage = serverCache.tipsMessage;
	var tipsDate = serverCache.tipsDate;
	var tipsExpDate = serverCache.tipsExpDate;
	window.ticketStorage.setItem("tipsMessage",tipsMessage);
	window.ticketStorage.setItem("tipsDate",tipsDate);
	window.ticketStorage.setItem("tipsExpDate",tipsExpDate);
	//init push Setting
	var channels = serverCache.channels;
	var apiServer = serverCache.apiServer;
	var dsServer = serverCache.dsServer;
	var recServer = serverCache.recServer;
	var appKey = serverCache.appKey;
	var isPush = "";
	if(serverCache.isPush === undefined || serverCache.isPush === "undefined" || serverCache.isPush === "" || serverCache.isPush === null){
		isPush = "N";
	}else{
		isPush = serverCache.isPush;
	}
	window.ticketStorage.setItem("channels",channels);
	window.ticketStorage.setItem("apiServer",apiServer);
	window.ticketStorage.setItem("dsServer",dsServer);
	window.ticketStorage.setItem("recServer",recServer);
	window.ticketStorage.setItem("appKey",appKey);
	window.ticketStorage.setItem("isPush",isPush);
	if(isPush === "N"){//不推送的时候disable
		sdk.disable(function(err){
			if(err) {
				//alert(err.message);
			} 
		});
	}else{
		//初始化推送参数
		window.plugins.AnDeviceManager.setHost(recServer, null);
		var conType = false;
		if(mor.ticket.util.isAndroid()){
			conType = true;
		}
		sdk.initialize("iSihvjP8UVmhiOgkoO0OBjlZWkzIsVFX", function(err) {
			if(err) {
				//alert(err);
			} else {
				sdk.setServerHosts(apiServer, dsServer, function(err) {
					if(err) {
						//alert(err);
					}
					//注册广播频道
					registerChannelsall();
				});
			}
		}, {secureConnection:conType,alert: true, badge: true, sound: true});
		
	}
	

	// 旅行休闲是否启用标识
	var isAdverStart = serverCache.isAdverStart;
	if(null != isAdverStart && "" != isAdverStart){
		window.ticketStorage.setItem("isAdverStart",isAdverStart);
	}
	var payFinishBlock1 = serverCache.payFinishBlock1;
	if(null != payFinishBlock1 && undefined != payFinishBlock1){
		window.ticketStorage.setItem("payFinishBlock1",payFinishBlock1);
	}
	var payFinishBlock2 = serverCache.payFinishBlock2;
	if(null != payFinishBlock2 && undefined != payFinishBlock2){
		window.ticketStorage.setItem("payFinishBlock2",payFinishBlock2);
	}
	var payFinishBlock3 = serverCache.payFinishBlock3;
	if(null != payFinishBlock3 && undefined != payFinishBlock3){
		window.ticketStorage.setItem("payFinishBlock3",payFinishBlock3);
	}
	var payFinishBlock4 = serverCache.payFinishBlock4;
	if(null != payFinishBlock4 && undefined != payFinishBlock4){
		window.ticketStorage.setItem("payFinishBlock4",payFinishBlock4);
	}
	var taxiServiceCoupon = serverCache.taxiServiceCoupon;
	if(null != taxiServiceCoupon && "" != taxiServiceCoupon){
		window.ticketStorage.setItem("taxiServiceCoupon",taxiServiceCoupon);
	}
	// 旅行休闲版本号
	var adver_version = window.ticketStorage.getItem("adver_version")
		|| mor.ticket.cache.adver_version.toString();
	if(serverCache.adver_version > adver_version){
		window.ticketStorage.setItem("adver_version_no",serverCache.adver_version);
		window.ticketStorage.setItem("adver_new",true);	
	}
	//接送站服务new
	var taxi_new = window.ticketStorage.getItem("taxi_new");
	if(taxi_new == undefined || taxi_new ==""){
		window.ticketStorage.setItem("taxi_new",true);	
	}
	//携程酒店new
	var hotel_new = window.ticketStorage.getItem("hotel_new");
	if(hotel_new == undefined || hotel_new ==""){
		window.ticketStorage.setItem("hotel_new",true);	
	}
	// can or not get passengers info 
	var isSendAdvMessage = serverCache.isSendAdvMessage;
	if(null != isSendAdvMessage && isSendAdvMessage != ""){
		window.localStorage.setItem("isSendAdvMessage", isSendAdvMessage);
	}
	// check station version
	var station_version = window.ticketStorage.getItem("station_version")
			|| mor.ticket.cache.station_version.toString();
	//TODO remove it
	if(jvFmt(mor.ticket.cache.station_version) > jvFmt(station_version)){
		station_version = mor.ticket.cache.station_version;
	}
	if (jvFmt(serverCache.station_version_no) > jvFmt(station_version)) {
		mor.ticket.cache["needSync"] = "Y";
		mor.ticket.cache["syncList"] += "station";
		mor.ticket.cache["syncVersionList"] = station_version;
	} else {
		// match, load data from local storage
		if (window.ticketStorage.getItem("stations") != null && jvFmt(mor.ticket.cache.station_version) < jvFmt(window.ticketStorage.getItem("station_version"))) {
			mor.ticket.cache.stations = JSON.parse(window.ticketStorage
					.getItem("stations"));
		}
		prepareCacheMap();
	}

	// check hot station version and init hot station
	var hotstation_version = window.ticketStorage.getItem("hotstation_version")
			|| mor.ticket.cache.hotStation_version.toString();
	// check if verion match
	if (serverCache.hotstation_version != hotstation_version) {
		// not match, need sync
		mor.ticket.cache["needSync"] = "Y";
		mor.ticket.cache["syncList"] += "|hotstation";
		mor.ticket.cache["syncVersionList"] += "|" + hotstation_version;
	} else {
		// match, load data from local storage
		if (window.ticketStorage.getItem("hot_station") != null) {
			mor.ticket.cache.hotStations = JSON.parse(window.ticketStorage
					.getItem("hot_station"));
		}
	}
	WL.JSONStore.closeAll();
	var citySearchFields = {valueSM : 'string',city_code:'string',city_name : 'string',pinyin : 'string'};
	initJSONStore('city',citySearchFields);
	var city_version_no = window.ticketStorage.getItem("city_version")
						|| mor.ticket.cache.city_version.toString();
	if (jvFmt(serverCache.city_version_no) > jvFmt(city_version_no)) {
		window.ticketStorage.setItem("city_update",true);
	}else{
		window.ticketStorage.setItem("city_update",false);
	}

	var universitySearchFields = {province_code : 'string',university_code:'string',university_name : 'string',pinyin : 'string'};
	initJSONStore('university',universitySearchFields);
	var university_version_no = window.ticketStorage.getItem("university_version")
								|| mor.ticket.cache.university_version.toString();
	if (jvFmt(serverCache.university_version_no) > jvFmt(university_version_no)) {
		window.ticketStorage.setItem("university_update",true);
	}else{
		window.ticketStorage.setItem("university_update",false);
	}

	var expressAddressSearchFields = {province : 'string',city:'string',country:'string',town:'string',street:'string'};
	initJSONStore('expressAddress',expressAddressSearchFields);
	var kuaidi_version_no = window.ticketStorage.getItem("expressAddress_version")
						|| mor.ticket.cache.expressAddress_version.toString();
	if (jvFmt(serverCache.kuaidi_version) > jvFmt(kuaidi_version_no)) {
		window.ticketStorage.setItem("expressAddress_update",true);
	}else{
		window.ticketStorage.setItem("expressAddress_update",false);
	}

	// sync with server with unmatched version
	if (mor.ticket.cache["needSync"] == "Y") {
		var util = mor.ticket.util;
		/*
		 * var commonParameters = util.prepareRequestCommonParameters({
		 * 'syncList': mor.ticket.cache["syncList"], 'syncVersionList':
		 * mor.ticket.cache["syncVersionList"], }); var invocationData = {
		 * adapter: "CARSMobileServiceAdapterV2", procedure: "syncCache",
		 * parameters: [commonParameters] };
		 */
		var commonParameters = {
			'syncList' : mor.ticket.cache["syncList"],
			'syncVersionList' : mor.ticket.cache["syncVersionList"],
		};
		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "syncCache"
		};
		var options = {
			onSuccess : syncSucceeded,
			onFailure : util.creatCommonRequestFailureHandler()
		};
		mor.ticket.viewControl.show_busy = true;
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,
				options, true);
		// WL.Client.invokeProcedure(invocationData, options);
	} else {
		// WL.Logger.debug('******* resolve util.syncDef in adapter1.');
		mor.ticket.util.syncDef.resolve();
	}
}

function syncSucceeded(result) {

	// WL.Logger.debug(result);
	// prepare station and pinying
	if (result.invocationResult.station) {
		var stations = result.invocationResult.station;
		var version_no = result.invocationResult.station_v;
		mor.ticket.cache.stations = [];
		var cache_station = mor.ticket.cache.stations;
		for ( var i = 0; i < stations.length; i++) {
			cache_station[i] = {};
			cache_station[i]["id"] = stations[i].id;
			cache_station[i]["value"] = stations[i].value;
			jq("#py").val(stations[i].value);
			var pingyin = jq("#py").toPinyin();
			cache_station[i].pinyin = pingyin.toLowerCase();
			cache_station[i]["valueSM"] = generatePinYing(stations[i].value);
		}
		window.ticketStorage.setItem("stations", JSON.stringify(cache_station));
		window.ticketStorage.setItem("station_version", version_no);
		// WL.Logger.debug("staion list synced");
		prepareCacheMap();
	}
	// prepare hot station and sort
	if (result.invocationResult.hotStation) {
		var hotStations = result.invocationResult.hotStation;
		var version_no = result.invocationResult.hotStation_v;
		hotStations.sort(sortHotStationfunction);
		mor.ticket.cache.hotStations = [];
		var cache_hotStation = mor.ticket.cache.hotStations;
		var cache = mor.ticket.cache;
		for ( var i = 0; i < hotStations.length; i++) {
			cache_hotStation[i] = {};
			cache_hotStation[i]["id"] = hotStations[i].tel_code;
			cache_hotStation[i]["value"] = cache.getStationNameByCode(hotStations[i].tel_code);
		}

		window.ticketStorage.setItem("hot_station", JSON.stringify(cache_hotStation));
		window.ticketStorage.setItem("hotstation_version", version_no);
		// WL.Logger.debug("hot staion list synced");
	}
	// prepare city and pinying
	if (result.invocationResult.city) {
		var city = result.invocationResult.city;
		var version_no = result.invocationResult.city_v;
		mor.ticket.cache.city = [];
		var cache_city = mor.ticket.cache.city;
		for ( var i = 0; i < city.length; i++) {
			cache_city[i] = {};
			cache_city[i]["city_code"] = city[i].city_code;
			cache_city[i]["city_name"] = city[i].city_name;
			jq("#py").val(city[i].city_name);
			var pingyin = jq("#py").toPinyin();
			cache_city[i].pinyin = pingyin.toLowerCase();
			cache_city[i]["valueSM"] = generatePinYing(city[i].city_name);
		}
		window.ticketStorage.setItem("city", JSON.stringify(cache_city));
		window.ticketStorage.setItem("city_version", version_no);
		// WL.Logger.debug("city list synced");
	}
	// prepare university and pinying
	if (result.invocationResult.university) {
		var university = result.invocationResult.university;
		var version_no = result.invocationResult.university_v;
		mor.ticket.cache.university = [];
		var cache_university = mor.ticket.cache.university;
		
		for ( var i = 0; i < university.length; i++) {
			cache_university[i] = {};
			cache_university[i]["province_code"] = university[i].province_code;
			cache_university[i]["university_code"] = university[i].university_code;
			cache_university[i]["university_name"] = university[i].university_name;
			jq("#py").val(university[i].university_name);
			var pingyin = jq("#py").toPinyin();
			cache_university[i].pinyin = pingyin.toLowerCase();
		}
		window.ticketStorage.setItem("university", JSON
				.stringify(cache_university));
		window.ticketStorage.setItem("university_version", version_no);
		// WL.Logger.debug("university list synced");
	}

	mor.ticket.cache["needSync"] = "N";
	// WL.Logger.debug('******* resolve util.syncDef in adapter2.');
	mor.ticket.util.syncDef.resolve();
}

function sortHotStationfunction(x, y) {
	if (x.hot_class == y.hot_class) {
		return x.tel_code.charCodeAt(0) - y.tel_code.charCodeAt(0);
	} else {
		return y.hot_class - x.hot_class;
	}

}

function generatePinYing(input) {
	var reg = /^[A-Z]+/g;
	jq("#py").val(input);
	var pingyin = jq("#py").toPinyin();
	var sm = '';
	for ( var index = 0; index < pingyin.length; index++) {
		if (pingyin.charAt(index).match(reg)) {
			sm += pingyin.charAt(index);
		}
	}
	return sm;
}

function deviceDetection() {
	switch (WL.Client.getEnvironment()) {
	case "android":
		return "a";
		break;
	case "iphone":
		return "i";
		break;
	default:
		return "a";
	}
}

// 以下为自动登录

// 自动登录

function registerAutoLoginHandler(suc, fail) {
	var user = mor.ticket.loginUser;
	var latestTime = window.ticketStorage.getItem("pwdTime");
	if (window.ticketStorage.getItem("autologin") == "true"
		&& (!latestTime || (Date.now() - latestTime   < 7*24*3600*1000))
		&& ((user.username!="" && user.password!="" && user.password!="*")
		|| (jq("#usernameInput").val()!="" && jq("#usernameInput").val()!=undefined)
			&& jq("#passwordInput").val()!="" && jq("#passwordInput").val()!=undefined)) {
		AutoSendLoginRequest(suc, fail);
	}else{
		jq.mobile.changePage(vPathCallBack() + "loginTicket.html");
	}
	return;

}

function AutoSendLoginRequest(suc, fail) {
	var _cfail = mor.ticket.util.creatCommonRequestFailureHandler();
	function _suc(result){
		AutoRequestSucceeded(result,suc,fail);
	}

	function _fail(result){
		_cfail(result);
		fail();
	}
	
	
	var commonParameters = {};
	if(mor.ticket.loginUser.password  == "*" 
		|| mor.ticket.loginUser.password  == null 
		|| mor.ticket.loginUser.password == ""
		|| mor.ticket.loginUser.username == ""
		|| mor.ticket.loginUser.username == null){
		commonParameters = {
				'baseDTO.user_name' : jq("#usernameInput").val(),
				'password' : hex_md5(jq("#passwordInput").val())
			};
//		}
	}else{
		commonParameters = {
				'baseDTO.user_name' : mor.ticket.loginUser.username,
				'password' : hex_md5(mor.ticket.loginUser.password)
			};
	}
	var invocationData = {
		adapter : mor.ticket.viewControl.adapterUsed,
		procedure : "login"
	};

	var options = {
		onSuccess : _suc,
		onFailure:	_fail
	};

	mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
}

function AutoRequestSucceeded(result,suc, fail) {

	if (busy.isVisible()) {
		busy.hide();
	}

	var invocationResult = result.invocationResult;
	if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
		//单点登录
		if(invocationResult.notice_one_session == "Y"){
			mor.ticket.util.alertMessage(invocationResult.notice_one_sessionMsg);
		}
		mor.ticket.loginUser["isAuthenticated"] = "Y";
		mor.ticket.loginUser.is_receive = invocationResult.is_receive;
		mor.ticket.loginUser.start_receive = invocationResult.start_receive;
		if (mor.ticket.loginUser["username"] == null
				|| mor.ticket.loginUser["username"] == '') {
			mor.ticket.loginUser["username"] = window.ticketStorage
					.getItem("username");
		}

		var loginUser = mor.ticket.loginUser;
		loginUser.accountName = invocationResult.user_name;
		loginUser.realName = invocationResult.name;
		loginUser.id_type = invocationResult.id_type_code;
		loginUser.id_no = invocationResult.id_no;
		loginUser.user_status = invocationResult.user_status;
		loginUser.check_id_flag = invocationResult.check_id_flag;
		loginUser.display_control_flag = invocationResult.display_control_flag;
		loginUser.mobile_no = invocationResult.mobileNo;
		loginUser.user_type = invocationResult.user_type;
		loginUser.email = invocationResult.email;
		loginUser.activeUser = invocationResult.is_active;
		// init passengerinfo
		if(mor.ticket.autoLoginChangePage.autoLoginChangePageFlag == "moreOptionView"){
			jq("#logoutOptionDesc").html('<span class="loginName">'+loginUser.realName+"</span>");
			jq("#logoutOption").show();
			jq("#moreOptionView .iscroll-wrapper").iscrollview("refresh");
			mor.ticket.autoLoginChangePage.autoLoginChangePageFlag == "";
		}
		if (invocationResult.passengerResult) {

			mor.ticket.passengersCache.passengers = [];


			mor.ticket.passengersCache.passengers = invocationResult.passengerResult;

			mor.ticket.passengersCache.sortPassengers();
			// WL.Logger.debug("Successfully 5");
		}
		//session过期，登录passengersCache添加儿童
		var NewPassengerList = mor.ticket.passengerList;
		var NewPassengersCache = mor.ticket.passengersCache.passengers;
		for(var i=0;i<NewPassengerList.length;i++){
			var isErTongPassenger = 0; 
			for(var j=0;j<NewPassengersCache.length;j++){
				var new_id_type = mor.ticket.passengersCache.passengers[j].id_type;
				var new_id_no = mor.ticket.passengersCache.passengers[j].id_no;
				var new_user_name = mor.ticket.passengersCache.passengers[j].user_name;
				var new_user_type = mor.ticket.passengersCache.passengers[j].user_type;
				if((new_id_type === NewPassengerList[i].id_type) && (new_id_no === NewPassengerList[i].id_no) && (new_user_name === NewPassengerList[i].user_name) && (new_user_type == NewPassengerList[i].user_type) && NewPassengersCache[j].checked!=1){
					mor.ticket.passengersCache.passengers[j].checked=1;
					if(mor.ticket.passengersCache.passengers[j].user_type=="2"){
						isErTongPassenger = 1;
					}
					break;
				}
			}
			if(NewPassengerList[i].user_type=="2" && isErTongPassenger!=1){
				for(var w=0;w<NewPassengersCache.length;w++){
					var new_id_type_cache = mor.ticket.passengersCache.passengers[w].id_type;
					var new_id_no_cache = mor.ticket.passengersCache.passengers[w].id_no;
					var new_user_name_cache = mor.ticket.passengersCache.passengers[w].user_name;
					if((new_id_type_cache == NewPassengerList[i].id_type) && (new_id_no_cache == NewPassengerList[i].id_no) && (new_user_name_cache == NewPassengerList[i].user_name) && NewPassengersCache[w].user_type!="2"){
						var newErTongPassengerCache = {};
						newErTongPassengerCache['checked']   = 1;
						newErTongPassengerCache['seat']      = "";
						newErTongPassengerCache['user_name'] = NewPassengerList[i].user_name;
						newErTongPassengerCache['id_type'] = NewPassengerList[i].id_type;
						newErTongPassengerCache['id_no'] = NewPassengerList[i].id_no;
						newErTongPassengerCache['mobile_no'] = NewPassengerList[i].mobile_no;
						newErTongPassengerCache['user_type'] = NewPassengerList[i].user_type;
						newErTongPassengerCache['total_times'] = mor.ticket.passengersCache.passengers[w].total_times;
						ewErTongPassengerCache['two_totaltimes'] = mor.ticket.passengersCache.passengers[w].two_totaltimes;
						newErTongPassengerCache['other_totaltimes'] = mor.ticket.passengersCache.passengers[w].other_totaltimes;
						newErTongPassengerCache['p_str'] = NewPassengerList[i].p_str;
						newErTongPassengerCache['user_nameSM'] = mor.ticket.passengersCache.passengers[w].user_nameSM;
						NewPassengersCache.splice(w+1,0,newErTongPassengerCache);
					}
				}
			}
		}
		if(invocationResult.display_control_flag==="2"){
			if(mor.ticket.loginUser.id_type == "1"){
				 WL.SimpleDialog.show(
							"温馨提示", 
							"身份信息经过核验但未通过，需修改12306所填写的身份信息内容与二代居民身份证原件完全一致，保存后状态仍显示“待核验”时，需持二代居民身份证原件到车站售票窗口或铁路客票代售点办理核验，详见《铁路互联网购票身份核验须知》。", 
							[ {text : '确定', handler: function() {
							}}]
				  );
			}else{
				 WL.SimpleDialog.show(
							"温馨提示", 
							"您的身份信息核验未通过，详见《铁路互联网购票身份核验须知》。", 
							[ {text : '确定', handler: function() {
							}}]
				  );
			}	
		}
		if(invocationResult.display_control_flag==="3"){
			 WL.SimpleDialog.show(
						"温馨提示", 
						"您的身份信息未经核验，需持二代居民身份证原件到车站售票窗口或铁路客票代售点办理核验，详见《铁路互联网购票身份核验须知》。", 
						[ {text : '确定', handler: function() {
						}}]
			  );
		}
		if(invocationResult.display_control_flag==="4"){
			 WL.SimpleDialog.show(
						"温馨提示", 
						"本网站不再支持一代居民身份证,请更改为二代居民身份证.", 
						[ {text : '确定', handler: function() {
						}}]
			  );
		}
		if(invocationResult.display_control_flag==="5"){
			 WL.SimpleDialog.show(
						"温馨提示", 
						"您的身份信息未经核验，需持在12306填写的有效身份证件原件到车站售票窗口办理预核验，详见《铁路互联网购票身份核验须知》。", 
						[ {text : '确定', handler: function() {
						}}]
			  );
		}
		if(invocationResult.is_active==="N"){
			 WL.SimpleDialog.show(
						"温馨提示", 
						"如果您要接收12306的服务邮件，请到我的12306验证邮箱。", 
						[ {text : '确定', handler: function() {
						}}]
			  );
		}
		suc && suc();
		return;
	}
	jq.mobile.changePage(vPathCallBack() + "loginTicket.html");
	mor.ticket.util.alertMessage(invocationResult.error_msg);
	fail && fail();
	return;

}
//add by yiguo
function autologinFailJump(){
		jq.mobile.changePage(vPathCallBack() + "loginTicket.html");
}
//以上为自动登录 另外定制

//check network connection. timeout=30s
if(WL.Utils.wlCheckReachability){
    WL.Utils.wlCheckReachability = function() {
        var isCheckDone = false;
        // iOS's isReachable does not check a server's availability. Rather, it merely checks is a socket can be opened.
        if (typeof navigator.network != "undefined" && navigator.network.connection.type == 'NONE') {
            WL.Utils.dispatchWLEvent(__WL.InternalEvents.REACHABILITY_TEST_FAILURE);
            isCheckDone = true;
        }

        var reachabilityUrl = WL.Client.getAppProperty(WL.AppProp.APP_SERVICES_URL) + "reach";
        if(typeof WL.AppProp.WLCLIENT_TIMEOUT_IN_MILLIS === 'undefined'){
            WL.AppProp.WLCLIENT_TIMEOUT_IN_MILLIS = 30000;
        }
        var timeout = wlInitOptions.timeout || WL.AppProp.WLCLIENT_TIMEOUT_IN_MILLIS;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", reachabilityUrl, true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                clearTimeout(xhrTimeout);
                WL.Utils.dispatchWLEvent(__WL.InternalEvents.REACHABILITY_TEST_SUCCESS);
            }
        };

        xhr.send("");
        var xhrTimeout = setTimeout(function() {
            if (!isCheckDone) {
                xhr.abort();
                WL.Utils.dispatchWLEvent(__WL.InternalEvents.REACHABILITY_TEST_FAILURE);
            }
        }, timeout); // by default timeout is 30s
    };
}
function initJSONStore(collectionName,searchFields){
	var collections = {};
	collections[collectionName] = {
			searchFields : searchFields
	};
	try{
		WL.JSONStore.init(collections).
		then(function () {
		}).
		fail(function (errorObject) {
			WL.Logger.info(errorObject.msg);
		});
	}catch (e) {
		WL.Logger.error(e.message);
	};	
};

/* JavaScript content from js/MobileTicket.js in folder iphone */
/**
 *  @license
 *  Licensed Materials - Property of IBM
 *  5725-G92 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
// This method is invoked after loading the main HTML and successful initialization of the Worklight runtime.
function wlEnvInit(){
    wlCommonInit();
    // Environment initialization code goes here
}