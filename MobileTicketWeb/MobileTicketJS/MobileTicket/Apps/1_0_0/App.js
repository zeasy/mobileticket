/**
 * @providesModule App_1_0_0
 * Created by easy on 16/1/12.
 */
'use strict';

require('react-redux');

var React = require('react-native');
var LoginScene = require('Login_1_0_0');
var SelectCityScene = require('SelectCity_1_0_0');

var Icon = require('react-native-vector-icons/FontAwesome')


var {
    View,
    ScrollView,
    TouchableOpacity,
    Text,
    NavigatorIOS
    } = React;

let Tools = React.createClass({
    render : function () {
        return (
            <ScrollView style={{flex:1}}>
                <TouchableOpacity onPress={()=> this.props.navigator.push(
                    {component:LoginScene,passProps:{onSuccess:()=>{
                        this.props.navigator.pop();
                    }}}
                    )
                    }>
                    <Text>Login</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=> this.props.navigator.push(
                    {component:SelectCityScene,passProps:{onSuccess:()=>{
                        this.props.navigator.pop();
                    }}}
                    )}>
                    <Text>SelectCity</Text>
                </TouchableOpacity>


                <TouchableOpacity onPress={()=> this.props.navigator.push(
                    {component:require('Notice12306_1_0_0'),passProps:{onSuccess:()=>{
                        this.props.navigator.pop();
                    }}}
                    )}>

                    <Icon name="truck" color="red" size={30}>
                        <Text>Notice12306</Text>
                    </Icon>

                </TouchableOpacity>

                <TouchableOpacity onPress={()=> {

                }}>

                    <Icon name="truck" color="red" size={30}>
                        <Text>Test</Text>
                    </Icon>

                </TouchableOpacity>
            </ScrollView>
        );
    }
});

let reducer = (state) => {
    return state;
}


let App = React.createClass({

    render : function () {
        return (

                <NavigatorIOS
                    ref="nav"
                    style={{flex:1}}
                    initialRoute={{
                        component: Tools,
                        title: 'Tools',
                    }}
                />

        );
    }
});

module.exports = App;