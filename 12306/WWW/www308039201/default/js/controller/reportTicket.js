
/* JavaScript content from js/controller/reportTicket.js in folder common */
(function() {
	var reportPrePageId = "";
	jq("#reportTicketView").live("pagebeforeshow", function(e,data) {
		reportPrePageId = data.prevPage.attr("id");
		jq("#reportName").html(mor.ticket.loginUser.realName);
		jq("#reportIdNo").html(mor.ticket.loginUser.id_no);
		jq("#reportPhoneNo").html(mor.ticket.loginUser.mobile_no);
		jq("#reportTicketInfoMsg").html("　　本人确认举报身份信息被他人冒用购买"+mor.ticket.reportTicketInfo.ticketInfo +"次车票。本人承诺本次举报及购票所提交的身份信息属实，并对举报后果负责。<br/>"
				+"　　铁路部门郑重提醒，将在车站和列车对该车票进行重点查验。根据国务院颁布的《铁路安全管理条例》，对该车票所记载身份信息与所持身份证件或者真实身份不符的持票人,"
                +"铁路部门有权拒绝其进站乘车。同时，公安机关将依法调查。<br/>"
                +"　　铁路部门将对您提供的个人信息严格保密。");
	});
	jq("#reportTicketView").live("pageinit", function(){
		reprotTicketBackBtnListener();
		submitReportTicketBtnListener();
	});
	function reprotTicketBackBtnListener(){
		jq("#reprotTicketBackBtn").bind("tap",function(e){
			if(reportPrePageId == "passengersView"){
				mor.ticket.util.changePage("passengers.html");
			}else if(reportPrePageId == "orderListView"){
				mor.ticket.util.changePage("queryOrder.html");
			}
			return false;
		});
	}
	function submitReportTicketBtnListener(){
		jq("#submitReportTicketBtn").bind("tap",function(e){
			var util = mor.ticket.util;
			var commonParameters = {};
			
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "report608Info"
			};
			
			var options =  {
					onSuccess: requestReport608InfoSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
		});
	};
	function requestReport608InfoSucceeded(result){
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				mor.ticket.util.alertMessage(invocationResult.error_msg);
				if(reportPrePageId == "passengersView"){
					mor.ticket.util.changePage("passengers.html");
				}else if(reportPrePageId == "orderListView"){
					mor.ticket.util.changePage("queryOrder.html");
				}
				return false;
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			return false;
		}
	}
})();