/**
 * @providesModule ActionTypes
 * Created by easy on 16/1/15.
 */


export const REQUEST_USER_LOGIN = 'REQUEST_USER_LOGIN';
export const REQUEST_USER_LOGIN_SUCCESS = 'REQUEST_USER_LOGIN_SUCCESS';
export const REQUEST_USER_LOGIN_FAIL = 'REQUEST_USER_LOGIN_FAIL';