
/* JavaScript content from js/controller/advanceQueryOrder.js in folder common */
(function() {	
	/*jq("#AdvanceQueryOrderView").live("pagecreate", function() {
		mor.ticket.util.androidRemoveIscroll("#AdvanceQueryOrderView");
	});*/
	var focusArray=[];
	var flag = 0;
	jq("#AdvanceQueryOrderView").live("pageshow", function() {
		focusArray=[];
	});
	function registerAutoScroll(){	
		var util = mor.ticket.util;
		util.enableAutoScroll('#trainCode',focusArray);
		util.enableAutoScroll('#passengerName',focusArray);
		util.enableAutoScroll('#sequenceNo',focusArray);
	}
	jq("#AdvanceQueryOrderView").live("pageinit", function() {
		registerAutoScroll();
		registerFromDateInputChangeListener();
		registerToDateInputChangeListener();
		registerAdvancedQueryOrderBtnListener();
		registerBtnGroupListener();
		jq("#advanceQueryOrderViewBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("queryOrder.html");
			mor.queryOrder.views.advanceQuery.isSelectTrainDate = false;
		});
		checkFormAQ();
	});
	
	function advanceQueryOrderFn(){
		if(mor.ticket.viewControl.queryFinishedOrderType == '4'){
			jq("#advanceQueryTips").hide();
			jq("#advanceTips").html("历史订单");
			var advanceQuery = mor.queryOrder.views.advanceQuery;
//			if(advanceQuery.fromDate == "" || advanceQuery.toDate == "" ||mor.ticket.currentPagePath.fromPath == "queryOrder.html"){
			if(mor.queryOrder.views.advanceQuery.queryOrderTypeFlag == true ){
				var date= mor.ticket.util.getNewDate();
				
				var toDate = new Date((+date)-24*60*60*1000*1);
				var dateStrTo = toDate.format("yyyy-MM-dd");
				var fromdate = new Date((+date)-24*60*60*1000*30);
				var dateStrFrom = fromdate.format("yyyy-MM-dd");
				jq("#toDateInputShow").val(dateStrTo);
				jq("#fromDateInputShow").val(dateStrFrom);
				mor.queryOrder.views.advanceQuery.fromDate = dateStrFrom;
				mor.queryOrder.views.advanceQuery.toDate = dateStrTo;
				advanceQuery.isSelectTrainDate = false;
				mor.queryOrder.views.advanceQuery.queryOrderTypeFlag = false;
			}
			else{
				if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == false){
					advanceQuery.fromDate = mor.queryOrder.views.advanceQuery.fromDate;
					advanceQuery.toDate = mor.queryOrder.views.advanceQuery.toDate;
					jq("#fromDateInputShow").val(advanceQuery.fromDate);
					jq("#toDateInputShow").val(advanceQuery.toDate);
					advanceQuery.isSelectTrainDate = false;
				}else if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == true){
					advanceQuery.trainFromDate = mor.queryOrder.views.advanceQuery.trainFromDate;
					advanceQuery.trainToDate = mor.queryOrder.views.advanceQuery.trainToDate;
					jq("#fromDateInputShow").val(advanceQuery.trainFromDate);
					jq("#toDateInputShow").val(advanceQuery.trainToDate);
					advanceQuery.isSelectTrainDate = true;
				}
			}
			jq('#trainCode').val(advanceQuery.trainCode);
			jq('#passengerName').val(advanceQuery.passengerName);
			jq('#sequenceNo').val(advanceQuery.sequenceNo);	
		}else{
			jq("#advanceTips").html("未出行订单");
			var advanceQuery = mor.queryOrder.views.advanceQuery;
//			if(advanceQuery.fromDate == "" || advanceQuery.toDate == "" ||mor.ticket.currentPagePath.fromPath == "queryOrder.html" ){
			if(mor.queryOrder.views.advanceQuery.queryOrderTypeFlag == true){
				var date= mor.ticket.util.getNewDate();
				var dateStr = date.format("yyyy-MM-dd");
				
				var fromdate = new Date((+date)-24*60*60*1000*30);
				var dateStrFrom = fromdate.format("yyyy-MM-dd");
				jq("#toDateInputShow").val(dateStr);
				jq("#fromDateInputShow").val(dateStrFrom);
				mor.queryOrder.views.advanceQuery.fromDate = dateStrFrom;
				mor.queryOrder.views.advanceQuery.toDate = dateStr;
				advanceQuery.isSelectTrainDate = false;
				mor.queryOrder.views.advanceQuery.queryOrderTypeFlag = false;
			}
			else {
				if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == false){
					advanceQuery.fromDate = mor.queryOrder.views.advanceQuery.fromDate;
					advanceQuery.toDate = mor.queryOrder.views.advanceQuery.toDate;
					jq("#fromDateInputShow").val(advanceQuery.fromDate);
					jq("#toDateInputShow").val(advanceQuery.toDate);
					advanceQuery.isSelectTrainDate = false;
				}else if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == true){
					advanceQuery.trainFromDate = mor.queryOrder.views.advanceQuery.trainFromDate;
					advanceQuery.trainToDate = mor.queryOrder.views.advanceQuery.trainToDate;
					jq("#fromDateInputShow").val(advanceQuery.trainFromDate);
					jq("#toDateInputShow").val(advanceQuery.trainToDate);
					advanceQuery.isSelectTrainDate = true;
				}	
				if(!advanceQuery.isSelectTrainDate){
					jq("#searchBookDate").addClass("ui-btn-active ui-state-persist");
					jq("#searchTrainDate").removeClass("ui-btn-active ui-state-persist");
				}else{
					jq("#searchTrainDate").addClass("ui-btn-active ui-state-persist");
					jq("#searchBookDate").removeClass("ui-btn-active ui-state-persist");
				}
			}
			jq('#trainCode').val(advanceQuery.trainCode);
			jq('#passengerName').val(advanceQuery.passengerName);
			jq('#sequenceNo').val(advanceQuery.sequenceNo);	
		}
	}
	jq("#AdvanceQueryOrderView").live("pagebeforeshow", function() {
		if(mor.ticket.loginUser.isAuthenticated !== "Y"){
			if (window.ticketStorage.getItem("autologin") != "true") {
					autologinFailJump();
				} else {
					registerAutoLoginHandler(advanceQueryOrderFn, autologinFailJump);
				}
		}else{
			advanceQueryOrderFn();
		}			
	});
	
	jq("#AdvanceQueryOrderView").live("pageshow", function() {
		mor.ticket.util.initAppVersionInfo();
		if(busy.isVisible()){
			busy.hide();
		}
	});
	function checkFormAQ(){
		var util = mor.ticket.util;
		var checkFormUtil = mor.ticket.checkForm;
		jq("#trainCode").off('blur').on('blur',function () { 
			if(jq("#trainCode").val()!==''&&!checkFormUtil.checkTrainCode(jq("#trainCode").val())){
				util.alertMessage("请输入正确的车次号");
				return;
			}
		});
		jq("#passengerName").off('blur').on('blur',function () { 
			if(jq("#passengerName").val()!==''&&!checkFormUtil.checkChar(jq("#passengerName").val())){
				util.alertMessage("姓名只能包含中文或者英文，如有生僻字或繁体字参见12306姓名填写规则进行填写！");
				return;
			}
		});
		jq("#sequenceNo").off('blur').on('blur',function () { 
			if(jq("#sequenceNo").val()!==''&&!checkFormUtil.checkSequenceNo(jq("#sequenceNo").val())){
				util.alertMessage("请输入正确的订单号!");
				return;
			}
		});
	}
	function initAdvanceQuerySelectScroller(){		
		jq('#fromDateInput,#toDateInput').scroller({
	        preset: 'date',
	        theme: 'ios',
	        yearText:'年',
	        monthText:'月',
	        dayText:'日',
	        setText:'确定',
	        cancelText:'取消',
	        display: 'modal',
	        mode: 'scroller',
	        dateOrder: 'yy mm dd',
	        dateFormat: 'yy-mm-dd',
	        height:40,
	        showLabel:true
		});
	}
	
	function registerBtnGroupListener(){
		var advanceQuery = mor.queryOrder.views.advanceQuery;
		jq("#searchBookDate").bind("tap",function(){
			jq(this).addClass("ui-btn-active ui-state-persist")
				.siblings().removeClass("ui-btn-active ui-state-persist");
			advanceQuery.isSelectTrainDate = false;
			jq("#fromDateInputShow").val(mor.queryOrder.views.advanceQuery.fromDate);
			jq("#toDateInputShow").val(mor.queryOrder.views.advanceQuery.toDate);
			return false;
		});
		
		jq("#searchTrainDate").bind("tap",function(){
			jq(this).addClass("ui-btn-active ui-state-persist")
				.siblings().removeClass("ui-btn-active ui-state-persist");
			mor.queryOrder.views.advanceQuery.isSelectTrainDate = true;
			if(mor.ticket.viewControl.queryFinishedOrderType == '3'){
				var date= mor.ticket.util.getNewDate();
				var dateStr = date.format("yyyy-MM-dd");
				jq("#fromDateInputShow").val(dateStr);
				var reservePeriod = parseInt(window.ticketStorage
						.getItem("reservePeriod_3"), 10);
				var date1 = new Date(date.setDate(date.getDate()+reservePeriod));
				var date1Str = date1.format("yyyy-MM-dd");
				jq("#toDateInputShow").val(date1Str);
				mor.queryOrder.views.advanceQuery.trainFromDate = dateStr;
				mor.queryOrder.views.advanceQuery.trainToDate = date1Str;
			}else if(mor.ticket.viewControl.queryFinishedOrderType == '4'){
				var date= mor.ticket.util.getNewDate();
				var dateStr = date.format("yyyy-MM-dd");
				var fromdate = new Date((+date)-24*60*60*1000*30);
				var dateStrFrom = fromdate.format("yyyy-MM-dd");
				jq("#toDateInputShow").val(dateStr);
				jq("#fromDateInputShow").val(dateStrFrom);
				mor.queryOrder.views.advanceQuery.trainFromDate = dateStrFrom;
				mor.queryOrder.views.advanceQuery.trainToDate = dateStr;
				flag++;
			}
			return false;
		});
	}
	
	function registerFromDateInputChangeListener(){
		var advanceQuery = mor.queryOrder.views.advanceQuery;
		jq("#fromDateInputShow").bind("tap",function(){
			mor.queryOrder.views.advanceQuery.isFromDateInputShow = true;
			var str = "advanceQueryOrder.html";
			mor.ticket.currentPagePath.fromPath = str;
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.show = false;
			mor.queryOrder.views.advanceQuery.flag = "fromDateInput";
			//未出行订单
			if(mor.ticket.viewControl.queryFinishedOrderType == '3'){
				if(advanceQuery.isSelectTrainDate == false){
					//订票日期查询
					var defaultDateValue = jq("#fromDateInputShow").val();
					var current = mor.ticket.currentPagePath;
					var monthValue;
					current.defaultDateValue[0]=defaultDateValue.slice(0,4);
					monthValue=parseInt(defaultDateValue.slice(5,7));
					if(monthValue == "1"){
						monthValue = "0";
					}else monthValue = monthValue-1;
					if(monthValue < 10){
						current.defaultDateValue[1] = "0"+monthValue;
					}else{
						current.defaultDateValue[1] = monthValue;
					}
					current.defaultDateValue[2]=defaultDateValue.slice(8,10);
					jQuery.extend(jQuery.mobile.datebox.prototype.options, {
						mode:"calbox",
						useInline: true,
						calControlGroup: true,
						defaultValue: current.defaultDateValue,
						showInitialValue: true,
						lockInput: false,
						useFocus: true,
						maxDays: false,
						buttonIcon: "grid",
						calUsePickers: true,
						afterToday: false,
						beforeToday: true,
						notToday: false,
						calOnlyMonth: true,
						overrideSlideFieldOrder: ['y','m','d','h','i']
					});
					jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				}else{
					var reservePeriod = parseInt(window.ticketStorage
							.getItem("reservePeriod_3"), 10);
					var defaultDateValue = jq("#fromDateInputShow").val();
					var current = mor.ticket.currentPagePath;
					var monthValue;
					current.defaultDateValue[0]=defaultDateValue.slice(0,4);
					monthValue=parseInt(defaultDateValue.slice(5,7));
					if(monthValue == "1"){
						monthValue = "0";
					}else monthValue = monthValue-1;
					if(monthValue < 10){
						current.defaultDateValue[1] = "0"+monthValue;
					}else{
						current.defaultDateValue[1] = monthValue;
					}
					current.defaultDateValue[2]=defaultDateValue.slice(8,10);
					jQuery.extend(jQuery.mobile.datebox.prototype.options, {
						mode:"calbox",
						useInline: true,
						calControlGroup: true,
						defaultValue: current.defaultDateValue,
						showInitialValue: true,
						lockInput: false,
						useFocus: true,
						maxDays: false,
						buttonIcon: "grid",
						calUsePickers: true,
						afterToday: true,
						beforeToday: false,
						notToday: false,
						calOnlyMonth: true,
						overrideSlideFieldOrder: ['y','m','d','h','i']
					});
					jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				}
			}else if(mor.ticket.viewControl.queryFinishedOrderType == '4'){
				//历史订单
				if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == false){
					//订票日期查询
					var defaultDateValue = jq("#fromDateInputShow").val();
					var current = mor.ticket.currentPagePath;
					var monthValue;
					current.defaultDateValue[0]=defaultDateValue.slice(0,4);
					monthValue=parseInt(defaultDateValue.slice(5,7));
					if(monthValue == "1"){
						monthValue = "0";
					}else monthValue = monthValue-1;
					if(monthValue < 10){
						current.defaultDateValue[1] = "0"+monthValue;
					}else{
						current.defaultDateValue[1] = monthValue;
					}
					current.defaultDateValue[2]=defaultDateValue.slice(8,10);
					jQuery.extend(jQuery.mobile.datebox.prototype.options, {
						mode:"calbox",
						useInline: true,
						calControlGroup: true,
						defaultValue: current.defaultDateValue,
						showInitialValue: true,
						lockInput: false,
						useFocus: true,
						maxDays: false,
						buttonIcon: "grid",
						calUsePickers: true,
						afterToday: false,
						notToday: false,
						beforeToday: true,
						calOnlyMonth: true,
						
						overrideSlideFieldOrder: ['y','m','d','h','i']
					});
					jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				}else{
					var defaultDateValue = jq("#fromDateInputShow").val();
					var current = mor.ticket.currentPagePath;
					var monthValue;
					current.defaultDateValue[0]=defaultDateValue.slice(0,4);
					monthValue=parseInt(defaultDateValue.slice(5,7));
					if(monthValue == "1"){
						monthValue = "0";
					}else monthValue = monthValue-1;
					if(monthValue < 10){
						current.defaultDateValue[1] = "0"+monthValue;
					}else{
						current.defaultDateValue[1] = monthValue;
					}
					current.defaultDateValue[2]=defaultDateValue.slice(8,10);
					jQuery.extend(jQuery.mobile.datebox.prototype.options, {
						mode:"calbox",
						useInline: true,
						calControlGroup: true,
						defaultValue: current.defaultDateValue,
						showInitialValue: true,
						lockInput: false,
						useFocus: true,
						maxDays: false,
						buttonIcon: "grid",
						calUsePickers: true,
						afterToday: false,
						beforeToday:true,
						notToday: false,
						calOnlyMonth: true,
						overrideSlideFieldOrder: ['y','m','d','h','i']
					});
					jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				}
			}
			mor.ticket.util.changePage(goingPath);	
			return false;
		});
	}
	
	function registerToDateInputChangeListener(){
		var advanceQuery = mor.queryOrder.views.advanceQuery;
		jq("#toDateInputShow").bind("tap",function(){
			mor.queryOrder.views.advanceQuery.isFromDateInputShow = false;
			var str = "advanceQueryOrder.html";
			mor.ticket.currentPagePath.fromPath = str;
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.show = false;
			mor.queryOrder.views.advanceQuery.flag = "toDateInput";

			if(mor.ticket.viewControl.queryFinishedOrderType == '3'){
				//未出行订单
				if(advanceQuery.isSelectTrainDate == false){
					//订票日期查询
					var defaultDateValue = jq("#toDateInputShow").val();
					var current = mor.ticket.currentPagePath;
					var monthValue;
					current.defaultDateValue[0]=defaultDateValue.slice(0,4);
					monthValue=parseInt(defaultDateValue.slice(5,7));
					if(monthValue == "1"){
						monthValue = "0";
					}else monthValue = monthValue-1;
					if(monthValue < 10){
						current.defaultDateValue[1] = "0"+monthValue;
					}else{
						current.defaultDateValue[1] = monthValue;
					}
					current.defaultDateValue[2]=defaultDateValue.slice(8,10);
					jQuery.extend(jQuery.mobile.datebox.prototype.options, {
						mode:"calbox",
						useInline: true,
						calControlGroup: true,
						defaultValue: current.defaultDateValue,
						showInitialValue: true,
						lockInput: false,
						useFocus: true,
						maxDays: false,
						buttonIcon: "grid",
						calUsePickers: true,
						afterToday: false,
						beforeToday: true,
						notToday: false,
						calOnlyMonth: true,
						overrideSlideFieldOrder: ['y','m','d','h','i']
					});
					jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				}else{	
					//乘车日期查询
					var defaultDateValue = jq("#toDateInputShow").val();
					var current = mor.ticket.currentPagePath;
					var monthValue;
					current.defaultDateValue[0]=defaultDateValue.slice(0,4);
					monthValue=parseInt(defaultDateValue.slice(5,7));
					if(monthValue == "1"){
						monthValue = "0";
					}else monthValue = monthValue-1;
					if(monthValue < 10){
						current.defaultDateValue[1] = "0"+monthValue;
					}else{
						current.defaultDateValue[1] = monthValue;
					}
					current.defaultDateValue[2]=defaultDateValue.slice(8,10);
					var reservePeriod = parseInt(window.ticketStorage
							.getItem("reservePeriod_3"), 10);
					jQuery.extend(jQuery.mobile.datebox.prototype.options, {
						mode:"calbox",
						useInline: true,
						calControlGroup: true,
						defaultValue: current.defaultDateValue,
						showInitialValue: true,
						lockInput: false,
						useFocus: true,
						maxDays: false,
						buttonIcon: "grid",
						calUsePickers: true,
						afterToday: true,
						beforeToday:false,
						notToday: false,
						calOnlyMonth: true,
						overrideSlideFieldOrder: ['y','m','d','h','i']
					});
					jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				}
			}else if(mor.ticket.viewControl.queryFinishedOrderType == '4'){
				//历史订单
				if(mor.queryOrder.views.advanceQuery.isSelectTrainDate == false){
					//订票日期查询
					var defaultDateValue = jq("#toDateInputShow").val();
					var current = mor.ticket.currentPagePath;
					var monthValue;
					current.defaultDateValue[0]=defaultDateValue.slice(0,4);
					monthValue=parseInt(defaultDateValue.slice(5,7));
					if(monthValue == "1"){
						monthValue = "0";
					}else monthValue = monthValue-1;
					if(monthValue < 10){
						current.defaultDateValue[1] = "0"+monthValue;
					}else{
						current.defaultDateValue[1] = monthValue;
					}
					current.defaultDateValue[2]=defaultDateValue.slice(8,10);
//					var reservePeriod = parseInt(window.ticketStorage
//							.getItem("reservePeriod_1"), 10);
					jQuery.extend(jQuery.mobile.datebox.prototype.options, {
						mode:"calbox",
						useInline: true,
						calControlGroup: true,
						defaultValue: current.defaultDateValue,
						showInitialValue: true,
						lockInput: false,
						useFocus: true,
						maxDays: false,
						buttonIcon: "grid",
						calUsePickers: true,
						afterToday: false,
						beforeToday: true,
						notToday: true,
						calOnlyMonth: true,
						overrideSlideFieldOrder: ['y','m','d','h','i']
					});
					jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				}else{
					var defaultDateValue = jq("#toDateInputShow").val();
					var current = mor.ticket.currentPagePath;
					var monthValue;
					current.defaultDateValue[0]=defaultDateValue.slice(0,4);
					monthValue=parseInt(defaultDateValue.slice(5,7));
					if(monthValue == "1"){
						monthValue = "0";
					}else monthValue = monthValue-1;
					if(monthValue < 10){
						current.defaultDateValue[1] = "0"+monthValue;
					}else{
						current.defaultDateValue[1] = monthValue;
					}
					current.defaultDateValue[2]=defaultDateValue.slice(8,10);
					jQuery.extend(jQuery.mobile.datebox.prototype.options, {
						mode:"calbox",
						useInline: true,
						calControlGroup: true,
						defaultValue: current.defaultDateValue,
						showInitialValue: true,
						lockInput: false,
						useFocus: true,
						maxDays: false,
						buttonIcon: "grid",
						calUsePickers: true,
						afterToday: false,
						beforeToday:true,
						notToday: true,
						calOnlyMonth: true,
						overrideSlideFieldOrder: ['y','m','d','h','i']
					});
					jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
				}
			}
			mor.ticket.util.changePage(goingPath);
			return false;
		});
	}
	function registerAdvancedQueryOrderBtnListener(){
		jq('#advancedQueryOrderBtn').off('tap').on("tap",function(){
			var advanceQuery = mor.queryOrder.views.advanceQuery;
			var util = mor.ticket.util;
			var checkFormUtil = mor.ticket.checkForm;
			if(jq("#trainCode").val()!==''&&!checkFormUtil.checkTrainCode(jq("#trainCode").val())){
				util.alertMessage("车次首字符为英文字母或者数字，后面最多4位数字!");
				return false;
			}
			if(jq("#passengerName").val()!==''&&!checkFormUtil.checkChar(jq("#passengerName").val())){
				util.alertMessage("姓名只能包含中文或者英文，如有生僻字或繁体字参见12306姓名填写规则进行填写！");
				return false;
			}
			if(jq("#sequenceNo").val()!==''&&!checkFormUtil.checkSequenceNo(jq("#sequenceNo").val())){
				util.alertMessage("请输入正确的订单号!");
				return false;
			}

			if(advanceQuery.isSelectTrainDate == false){
				if(util.setMyDate(advanceQuery.toDate) < util.setMyDate(advanceQuery.fromDate)){
					util.alertMessage("订票起始日期不能在截止日期之后！");
					return false;
				}
			}
			else if(advanceQuery.isSelectTrainDate == true){
				if(util.setMyDate(advanceQuery.trainToDate) < util.setMyDate(advanceQuery.trainFromDate)){
					util.alertMessage("乘车起始日期不能在截止日期之后！");
					return false;
				}
			}
			
			advanceQuery.trainCode = jq('#trainCode').val();
			advanceQuery.passengerName = jq('#passengerName').val();
			advanceQuery.sequenceNo = jq('#sequenceNo').val();
			mor.ticket.util.changePage("finishedOrderList.html");
			
			return false;
		});
	}
	
})();