
/* JavaScript content from js/controller/trainQueryResults.js in folder common */
(function() {
	
	jq("#trainQueryResultsView").live("pageinit", function() {
		registerBackButtonHandler();
		registerPrevDateBtnClickHandler();
		registerNextDateBtnClickHandler();
		registSelectDateChangeHandler();
		registerDatePicker();
	});

	jq("#trainQueryResultsView").live("pagebeforeshow",function(e,data) {
		mor.ticket.util.initAppVersionInfo();
		var prevId = data.prevPage.attr("id");
		if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
			mor.ticket.currentQueryObject.train_date = mor.ticket.currentPagePath.calenderDate;
		}
		jq("#priceTicketResult").bind("tap",searchAndShowResult);
	});
	function registerDatePicker(){
		jq("#trainQueryResultInputId").bind("tap",function(){
			var current = mor.ticket.currentPagePath;
			var monthValue;
			current.defaultDateValue[0]=jq("#trainQueryChooseDateBtn").val().slice(0,4);
			monthValue=parseInt(jq("#trainQueryChooseDateBtn").val().slice(5,7));
			if(monthValue == "1"){
				monthValue = "0";
			}else monthValue = monthValue-1;
			if(monthValue < 10){
				current.defaultDateValue[1] = "0"+monthValue;
			}else{
				current.defaultDateValue[1] = monthValue;
			}
			current.defaultDateValue[2]=jq("#trainQueryChooseDateBtn").val().slice(8,10);
			var reservePeriod = mor.ticket.history.reservePeriod;
			jQuery.extend(jQuery.mobile.datebox.prototype.options, {
				mode:"calbox",
				useInline: true,
				calControlGroup: true,
				defaultValue: current.defaultDateValue,
				showInitialValue: true,
				lockInput: false,
				useFocus: true,
				maxDays: reservePeriod,
				buttonIcon: "grid",
				calUsePickers: true,
				afterToday: true,
				beforeToday: false,
				notToday: false,
				calOnlyMonth: false,
				overrideSlideFieldOrder: ['y','m','d','h','i']
			});
			mor.ticket.currentPagePath.fromPath = "trainQueryResults.html";
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.pickYears = false;
			mor.ticket.currentPagePath.show = true;
			jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
			mor.ticket.util.changePage(goingPath);
		});
	}
	jq("#trainQueryResultsView").live("pageshow",function() {
		mor.ticket.viewControl.tab3_cur_page = "trainQueryResultsView.html";
		var query = mor.ticket.currentQueryObject;
		jq("#trainQueryChooseDateBtn").val(query.train_date);
		searchAndShowResult(query);
	});

	function searchAndShowResult(query) {
		var util = mor.ticket.util;
		var commonParameters = null;
			commonParameters = {
				"start_train_date" : util.processDateCode(query.train_date),
				"train_code" : query.train_code
			};

		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "queryTrainInfo"
		};
		var options = {
			onSuccess : requestSucceeded,
			onFailure : util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,options);
	}

	function requestSucceeded(result) {
		if (busy.isVisible()) {
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			showSearchResults(invocationResult);
		} else {
			jq.mobile.changePage("trainQuery.html");
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			return;
		}
	};

	function showSearchResults(result) {
		var results = result.list;
		//var ticketPriceList = new Array();
		var util = mor.ticket.util;
		//var prices = result.list_price;
		var lishi = "";
		for ( var i = 0; i < results.length; i++) {
			if (i == 0) {
				jq("#fromStationName_tq").html(results[i].start_station_name);
				jq("#trainStartTime_tq").html(results[i].start_time + " 始发");
				jq("#trainCodeName_tq").html(results[i].station_train_code);
				jq("#trainDurationTime_tq").html(results[i].lishi);
				jq("#toStationName_tq").html(results[i].end_station_name);
				results[i].stopover_time = '----';
			} else {
				if(i == results.length-1){
					jq("#trainReachTime_tq").html(results[i].arrive_time + " 终到");
					results[i].stopover_time = '----';
					lishi = results[i].running_time;
				}else{
					results[i].stopover_time = util.limitTrainTime(results[i].arrive_time,results[i].start_time) + '分钟';
				}
			}
		}
		jq("#trainDurationTime_tq").html(util.getLiShiStr(lishi));
		jq("#trainQueryStopStationList").html(generateTrainStationListContent(results));
		jq("#trainTicketDetail").show();
		jq("#trainQueryStopStationList").show();
		jq("#trainQueryResultsView .iscroll-wrapper").iscrollview("refresh");
		initDateBtn();
	}
	
	function getLiClass(index) {
		switch (index % 4) {
		case 0:
			return "ui-block-a";
		case 1:
			return "ui-block-b";
		case 2:
			return "ui-block-c";
		case 3:
			return "ui-block-d";
		default:
			return "ui-block-e";
		}
	}
	
	function registerBackButtonHandler() {
		jq("#trainQueryResultsBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage("trainQuery.html");
			return false;
		});
	}

	function initDateBtn() {
		var date = mor.ticket.util
				.setMyDate(mor.ticket.currentQueryObject.train_date);
		var nowDate = mor.ticket.util.getNewDate();
		if (date < nowDate) {
			jq("#trainQueryPrevDateBtn").addClass("ui-disabled");
			jq("#trainQueryNextDateBtn").removeClass("ui-disabled");
		} else if (date - nowDate > (mor.ticket.util.getReservedPeriod() - 1) * 86400000) {
			jq("#trainQueryNextDateBtn").addClass("ui-disabled");
			jq("#trainQueryPrevDateBtn").removeClass("ui-disabled");
		} else {
			jq("#trainQueryPrevDateBtn").removeClass("ui-disabled");
			jq("#trainQueryNextDateBtn").removeClass("ui-disabled");
		}
	}

	function registSelectDateChangeHandler(){
		jq("#trainQueryChooseDateBtn").change(function(){
			var date = mor.ticket.util.setMyDate(jq("#trainQueryChooseDateBtn").val());
			var selectDate = new Date(date.getTime());
			mor.ticket.currentQueryObject.train_date = selectDate.format("yyyy-MM-dd");
			searchAndShowResult(mor.ticket.currentQueryObject);
		});
	}
	
	function registerPrevDateBtnClickHandler() {
		jq("#trainQueryPrevDateBtn").off().on("tap",
			function() {
				var date = mor.ticket.util
						.setMyDate(mor.ticket.currentQueryObject.train_date);
				var nowDate = mor.ticket.util.getNewDate();
				if (date > nowDate) {
					var preDate = new Date(date.getTime() - 24 * 60
							* 60 * 1000);
					mor.ticket.currentQueryObject.train_date = preDate.format("yyyy-MM-dd");
					jq("#trainQueryChooseDateBtn").val(preDate.format("yyyy-MM-dd"));
					searchAndShowResult(mor.ticket.currentQueryObject);
					mor.ticket.util.contentIscrollTo(0, 0, 0);
				}
				return false;
			});
	}

	function registerNextDateBtnClickHandler() {
		jq("#trainQueryNextDateBtn").off().on("tap",
			function() {
				var date = mor.ticket.util
						.setMyDate(mor.ticket.currentQueryObject.train_date);
				var nowDate = mor.ticket.util.getNewDate();
				if (date - nowDate < (mor.ticket.util.getReservedPeriod() - 1) * 86400000) {
					var nextDate = new Date(date.getTime() + 24
							* 60 * 60 * 1000);
					mor.ticket.currentQueryObject.train_date = nextDate.format("yyyy-MM-dd");
					jq("#trainQueryChooseDateBtn").val(nextDate.format("yyyy-MM-dd"));
					searchAndShowResult(mor.ticket.currentQueryObject);							
					mor.ticket.util.contentIscrollTo(0, 0, 0);
				}
				return false;
			});
	}
	var trainStationListTemplate = "<table class='stopStationTable'>"
		+"<tr class='hearders'>"
			+"<td style='width:20%'>站序</td>"
			+"<td style='width:20%'>站名</td>"
			+"<td style='width:20%'>到时</td>"
			+"<td style='width:20%'>发时</td>"
			+"<td style='width:20%'>停留</td>"
		+"</tr>"
		+"{{~it :station:index}}"
		+"<tr>"
			+"<td style='width:20%'>{{=station.station_no}}</td>"
			+"<td style='width:20%'>{{=station.station_name}}</td>"
			+"<td style='width:20%'>{{=station.arrive_time}}</td>"
			+"{{ if(station.start_time == station.arrive_time){ }}"
			+"<td style='width:20%;'>----</td>"
			+"{{ }else{ }}"
			+"<td style='width:20%;'>{{=station.start_time}}</td>"
			+"{{ } }}"
			+"<td style='width:20%'>{{=station.stopover_time}}</td>"
		+"</tr>"
		+"{{~}}"
		+"</table>";
	var generateTrainStationListContent = doT.template(trainStationListTemplate);

})();