
/* JavaScript content from js/jquery.processing.plugin.js in folder common */
/**
 * progress-Dialog for jQuery
 * Written by vakin Jiang (mailto: chiang.www@gmail.com)
 * Date: 2010/7/30
 * @author vakin
 * @version 1.0
 * 
 * @example
 * jq(document).progressDialog.showDialog();
 * 
 *  jq.ajax({
	  .....
	  complete:function(data){
	    jq(document).progressDialog.hideDialog();
	    //do something
	  }
	});
 **/
(function(jq) {
	jq.fn.progressDialog = function() {

	};

	jq.fn.progressDialog.showDialog = function(text) {
		text = text || "Loading,Please wait..."
		createElement(text);
		setPosition();
		waterfall.appendTo("body");
		jq(window).bind('resize', function() {
			setPosition();
		});
	}

	jq.fn.progressDialog.hideDialog = function(text) {
		waterfall.remove();
	}

	function createElement(text) {
		if (!waterfall) {
			waterfall = jq(document.createElement("div"));
			waterfall.attr("id", "waterfall");
			waterfall.css( {
				"height" : "100%",
				"width" : "100%",
				"filter" : "alpha(opacity = 50)",
				"-moz-opacity" : "0.5",
				"opacity" : "0.5",
				"background-color" : "#CCCCCC",
				"position" : "absolute",
				"left" : "0px",
				"top" : "0px"
			});
		}
		if (!loadDiv) {
			loadDiv = document.createElement("div");
		}
		jq(loadDiv).appendTo(waterfall);
		
		var content = " <div style='width:" +width+ "px; height:" +Height+ "px;'><div style='width:100%; height:30px; line-height:31px;padding-left:15px;text-align:center;font-weight:bolder; color:black;'></div><div class='processingImg'></div></div>";
		jq(loadDiv).html(content);
	}

	function setPosition() {
		var leftOffset = (jq(document).width() - width) / 2;
		var topOffset = (jq(document).height() - Height) / 2;
		jq(loadDiv).css( {
			"position" : "absolute",
			"height" : Height + "px",
			"width" : width + "px",
			"left" : leftOffset + "px",
			"top" : topOffset + "px"
		});
	}

	var waterfall;
	var loadDiv;
	var width = 290;
	var Height = 60;
})(jQuery);