//
//  ViewController.m
//  MobileTicketWeb
//
//  Created by EASY on 16/1/12.
//  Copyright © 2016年 Z.EASY. All rights reserved.
//

#import "ViewController.h"
#import <RCTRootView.h>

@interface ViewController ()

@property (nonatomic, strong) RCTRootView *rootView;

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	NSURL *url = [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle?platform=ios&dev=true"];
//	RCTBridge *bridge = [[RCTBridge alloc] initWithBundleURL:url moduleProvider:nil launchOptions:nil];
	
//	NSURL *url = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
	self.rootView = [[RCTRootView alloc] initWithBundleURL:url moduleName:@"App_1_0_0" initialProperties:@{} launchOptions:@{}];
	self.rootView.frame = self.view.bounds;
	self.rootView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[self.view addSubview:self.rootView];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
