
/* JavaScript content from js/controller/ticketPriceQuery.js in folder common */
(function(){
	var prevId = "";
	var focusArray=[];
	function registerAutoScroll(){	
		var util = mor.ticket.util;
		util.enableAutoScroll('#ticketPriceQueryTimePeriodSelect',focusArray);
	}
	jq("#ticketPriceQueryView").live("pageinit", function(){		
		registerTicketPriceQueryFromStationInputClickHandler();
		registerTicketPriceQueryToStationInputClickHandler();
		registerTicketPriceQueryBackButtonHandler();
		registerTicketPriceQueryChangeSeatClickHandler();
		registerTicketPriceHeadersBtnGroupClickHandler();
		registerTicketPriceQueryBtnLisener();
		registerDatePicker();
		registerAutoScroll();
	});
	
	jq("#ticketPriceQueryView").live("pagebeforeshow",function(e,data) {
		mor.ticket.util.initAppVersionInfo();
		prevId = data.prevPage.attr("id");
		jq("#ticketPriceQueryView").css("pointer-events", "auto");
		mor.ticket.viewControl.current_tab = "ticketPriceQueryTab";
		mor.ticket.viewControl.tab3_cur_page = "ticketPriceQuery.html";
		var util = mor.ticket.util;
		util.syncDef = util.syncDef || jq.Deferred();
		util.syncDef.then(function(serverCache) {
			var st = new Date(window.ticketStorage.getItem('serverTime'));
			var ct = new Date();
			// 如果系统时间和服务时间不一致， 需要重新渲染。
			var reservePeriod = calReservedPeriod();
			if (st.getFullYear() !== ct.getFullYear()
					|| st.getMonth() != ct.getMonth()
					|| st.getDate() != ct.getDate() || reservePeriod !== mor.ticket.history.reservePeriod) {
				// 因为在初始化的时候， 这个内容已经被赋值了，所以有可能是错误的值.
				var currDate = ct.format("yyyy-MM-dd");
				var sysDate = st.format("yyyy-MM-dd");
				var reservePeriodMonth = mor.ticket.history.reservePeriodMonth;
				if(!util.indexOf(reservePeriodMonth,ct.format("yyyyMM"))){
					mor.ticket.leftTicketQuery.train_date = sysDate;
					jq("#ticketPriceQueryDateInput").val(mor.ticket.leftTicketQuery.train_date);
				}else{
					var today = mor.ticket.util.getNewDate();
					var todayDate = today.getDate();
					today = today.setDate(todayDate + parseInt(reservePeriod,10) - 1);
					var reserveDay = new Date(today).format("yyyy-MM-dd"); 
					if(sysDate > currDate || currDate > reserveDay){
						mor.ticket.leftTicketQuery.train_date = sysDate;
						jq("#ticketPriceQueryDateInput").val(mor.ticket.leftTicketQuery.train_date);
					}
				}
			}
			jq("#ticketPriceQueryView .iscroll-wrapper").iscrollview('refresh');
		});
		iniTticketPriceQueryViewData();
	});
	function registerTicketPriceQueryFromStationInputClickHandler() {
		jq("#ticketPriceQueryFromStationInput").bind("tap", function(e) {
			e.stopImmediatePropagation();
			jq("#ticketPriceQueryView").css("pointer-events", "none");
			mor.ticket.views.selectStation.isTicketPriceQueryFromStation = true;
			mor.ticket.util.changePage(vPathCallBack() + "selectStation.html");
			return false;
		});
	}

	function registerTicketPriceQueryToStationInputClickHandler() {
		jq("#ticketPriceQueryToStationInput").bind("tap", function(e) {
			e.stopImmediatePropagation();
			jq("#ticketPriceQueryView").css("pointer-events", "none");
			mor.ticket.views.selectStation.isTicketPriceQueryFromStation = false;
			mor.ticket.util.changePage(vPathCallBack() + "selectStation.html");
			return false;
		});
	}

	function iniTticketPriceQueryViewData() {
		/*
		 * 初始化查询车站
		 */
		var model = mor.ticket.leftTicketQuery;
		if (!model.from_station_telecode) {
			model.from_station_telecode = (window.ticketStorage
					.getItem("set_from_station_telecode") == null ? "BJP"
					: window.ticketStorage.getItem("set_from_station_telecode"));
		}
		if (!model.to_station_telecode) {
			model.to_station_telecode = (window.ticketStorage
					.getItem("set_to_station_telecode") == null ? "SHH"
					: window.ticketStorage.getItem("set_to_station_telecode"));
		}
		if (!model.train_date) {
			if (window.ticketStorage.getItem("set_train_date_type") != null) {
				var date = mor.ticket.util.getNewDate();
				var queryDate = new Date(date.setDate(date.getDate()
						+ parseInt(window.ticketStorage
								.getItem("set_train_date_type"))));
				model.train_date = queryDate.format("yyyy-MM-dd");
			} else {
				var date = mor.ticket.util.getNewDate();
				date.setDate(date.getDate() + 1);
				model.train_date = date.format("yyyy-MM-dd");
			}
		}
		//初始化车次类型train_hearder
		if(mor.ticket.leftTicketQuery.train_headers == ""){
			jq("#ticketPriceQueryHeadersBtnGroup a").eq(0).addClass("ui-btn-active ui-state-persist");
		}else{
			var newtrain_headers = mor.ticket.leftTicketQuery.train_headers.split("#");
			var util = mor.ticket.util;
			for ( var i = 0, len = jq("#ticketPriceQueryHeadersBtnGroup a").length; i < len; i++) {
				var jq_a = jq("#ticketPriceQueryHeadersBtnGroup a:eq("
						+ i + ")");
				if(jq_a.hasClass("ui-btn-active ui-state-persist")){
					jq_a.removeClass("ui-btn-active ui-state-persist");
				}
				if (util.indexOf(newtrain_headers,jq_a.attr("name"))) {
					jq_a.addClass("ui-btn-active ui-state-persist");
				}
			}
		}
		//初始化开车时间段
		var new_time_period = mor.ticket.leftTicketQuery.time_period;
		if(mor.ticket.viewControl.bookMode == "fc"){
			new_time_period = mor.ticket.leftTicketQuery.time_period_back;
		}
		jq("#ticketPriceQueryTimePeriodSelect option").eq(parseInt(new_time_period)).attr("selected",true);
		if(jq("#ticketPriceQueryTimePeriodSelect").val() != "0"){
			jq("#ticketPriceQueryTimePeriodSelect-button").css("color","#298CCF");
		}else{
			jq("#ticketPriceQueryTimePeriodSelect-button").css("color","black");
		}
		jq("#ticketPriceQueryTimePeriodSelect").selectmenu('refresh', true);
		/*
		 * 页面初始化出发地,目的地,日期
		 */
		var cache = mor.ticket.cache;
		jq("#ticketPriceQueryFromStationInput").val(
				cache.getStationNameByCode(model.from_station_telecode));
		jq("#ticketPriceQueryToStationInput").val(
				cache.getStationNameByCode(model.to_station_telecode));
		if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
			model.train_date = mor.ticket.currentPagePath.calenderDate;
		}
		jq("#ticketPriceQueryDateInput").val(model.train_date);
	}
	
	function calReservedPeriod(){
		var reservedPeriodType = '3';
		var model = mor.ticket.leftTicketQuery;
		if(model.purpose_codes == '00'){
			reservedPeriodType = '1';
		}else if(model.purpose_codes == '0X'){
			reservedPeriodType = '3';
		}
		var reservePeriod = parseInt(window.ticketStorage
				.getItem("reservePeriod_" + reservedPeriodType), 10);
		var reservePeriodMonth = window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType) == null ? [] : 
			JSON.parse(window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType));
		mor.ticket.history.reservePeriodMonth = reservePeriodMonth;
		mor.ticket.history.reservePeriod = reservePeriod;
		return reservePeriod;
	}
	
	function registerDatePicker(){
		jq("#ticketPriceQueryDateInput").bind("tap",function(){
			var current = mor.ticket.currentPagePath;
			var monthValue;
			current.defaultDateValue[0]=jq("#ticketPriceQueryDateInput").val().slice(0,4);
			monthValue=parseInt(jq("#ticketPriceQueryDateInput").val().slice(5,7));
			if(monthValue == "1"){
				monthValue = "0";
			}else monthValue = monthValue-1;
			if(monthValue < 10){
				current.defaultDateValue[1] = "0"+monthValue;
			}else{
				current.defaultDateValue[1] = monthValue;
			}
			current.defaultDateValue[2]=jq("#ticketPriceQueryDateInput").val().slice(8,10);
			var reservedPeriodType = "3";
			var reservePeriod = parseInt(window.ticketStorage
					.getItem("reservePeriod_" + reservedPeriodType), 10);
			jQuery.extend(jQuery.mobile.datebox.prototype.options, {
				mode:"calbox",
				useInline: true,
				calControlGroup: true,
				defaultValue: current.defaultDateValue,
				showInitialValue: true,
				lockInput: false,
				useFocus: true,
				maxDays: reservePeriod,
				buttonIcon: "grid",
				calUsePickers: true,
				beforeToday: false,
				afterToday: true,
				notToday: false,
				calOnlyMonth: false,
				overrideSlideFieldOrder: ['y','m','d','h','i']
			});
			mor.ticket.currentPagePath.fromPath = "ticketPriceQuery.html";
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.pickYears = false;
			mor.ticket.currentPagePath.show = false;
			var reservePeriodMonth = window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType) == null ? [] : 
				JSON.parse(window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType));
			mor.ticket.history.reservePeriodMonth = reservePeriodMonth;
			jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
			mor.ticket.util.changePage(goingPath);
		});
	}
	function registerTicketPriceQueryBackButtonHandler() {
		jq("#ticketPriceQueryBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage(vPathCallBack() + "moreOption.html");
			return false;
		});
	}
	function registerTicketPriceHeadersBtnGroupClickHandler() {
		jq("#ticketPriceQueryHeadersBtnGroup").on("tap", "a", function(e) {
			e.stopImmediatePropagation();
			if(!mor.ticket.datebox.calbox){
				return false;
			}
			var activeCls = "ui-btn-active ui-state-persist";
			var that = jq(this);
			var name = that.attr('name');
			var trainHeaders = mor.ticket.searchResultScreen.trainHeaders;
			if (that.hasClass(activeCls)) {
				if (name != "QB") {
					trainHeaders.splice(jq.inArray(name,trainHeaders),1);
					that.removeClass(activeCls);
					if (!jq("#ticketPriceQueryD,#ticketPriceQueryZ,#ticketPriceQueryT,#ticketPriceQueryK,#ticketPriceQueryQT").hasClass(activeCls)) {
						jq("#ticketPriceQueryQB").addClass(activeCls);
						mor.ticket.searchResultScreen.trainHeaders = [];
					}
				}
			} else {
				if (name == "QB") {
					mor.ticket.searchResultScreen.trainHeaders = [];
					that.addClass(activeCls).siblings().removeClass(activeCls);
				} else {
					that.addClass(activeCls);
					trainHeaders.push(name);
					if (jq("#ticketPriceQueryQB").hasClass(activeCls)) {
						jq("#ticketPriceQueryQB").removeClass(activeCls);
					}
				}
			}
			mor.ticket.leftTicketQuery.train_headers = "";
			for(var i = 0;i<mor.ticket.searchResultScreen.trainHeaders.length;i++){
				mor.ticket.leftTicketQuery.train_headers += mor.ticket.searchResultScreen.trainHeaders[i]+"#"; 
			}
			return false;
		});	
	}
	/*
	 * 出发地和目的地交换数据
	 */
	function registerTicketPriceQueryChangeSeatClickHandler() {
		jq("#ticketPriceQueryChangeSeat").live("tap", function(e) {
			e.stopImmediatePropagation();
			var model = mor.ticket.leftTicketQuery;
			var fromStationInput = jq("#ticketPriceQueryToStationInput").val();
			var toStationInput = jq("#ticketPriceQueryFromStationInput").val();
			jq("#ticketPriceQueryToStationInput").val(toStationInput);
			jq("#ticketPriceQueryFromStationInput").val(fromStationInput);
			
			var from_station = model.to_station_telecode;
			var to_station = model.from_station_telecode;
			model.from_station_telecode = from_station;
			model.to_station_telecode = to_station;
			return false;
		});
	}
	function registerTicketPriceQueryBtnLisener(){
		jq("#ticketPriceQueryTimePeriodSelect").bind("change", function() {
			if(mor.ticket.viewControl.bookMode == 'fc'){
				mor.ticket.leftTicketQuery.time_period_back = jq(this).val();
			}else{
				mor.ticket.leftTicketQuery.time_period = jq(this).val();
			}
			if(jq(this).val() == "0"){
				jq("#ticketPriceQueryTimePeriodSelect-button").css("color","black");
			}else{
				jq("#ticketPriceQueryTimePeriodSelect-button").css("color","#298CCF");
			}
			return false;
		});
		jq("#ticketPriceQueryBtn").bind("tap", sendticketPriceQueryRequest);
	};
	
	function sendticketPriceQueryRequest(e) {
		e.stopImmediatePropagation();
		var ticketPriceQuery_headers= "";
		for ( var i = 0, len = jq("#ticketPriceQueryHeadersBtnGroup a").length; i < len; i++) {
			var jq_a = jq("#ticketPriceQueryHeadersBtnGroup a:eq("
					+ i + ")");
			if (jq_a.hasClass("ui-btn-active ui-state-persist")) {
				ticketPriceQuery_headers += jq_a.attr("name")+"#";
			}
		}
		if (ticketPriceQuery_headers == "") {
			ticketPriceQuery_headers = "QB#";
		}
		var model = mor.ticket.leftTicketQuery;
		var fromStationInput = model.from_station_telecode;
		var toStationInput = model.to_station_telecode;
		if (fromStationInput == '') {
			mor.ticket.util.alertMessage("出发地不能为空");
			return false;
		} 
		if (toStationInput == '') {
			mor.ticket.util.alertMessage("目的地不能为空");
			return false;
		}
		model.train_date = jq("#ticketPriceQueryDateInput").val();
		var time_period_str = jq("#ticketPriceQueryTimePeriodSelect").val();
		model.time_period = time_period_str;
		switch(time_period_str){
		case '0':
			model.start_times = "0000--2400";
			break;
		case '1':
			model.start_times = "0000--0600";
			break;
		case '2':
			model.start_times = "0600--1200";
			break;
		case '3':
			model.start_times = "1200--1800";
			break;
		case '4':
			model.start_times = "1800--2400";
			break;
		default:
			model.start_times = "0000--2400";
		}
		model.train_headers = ticketPriceQuery_headers;
		mor.ticket.util.changePage("ticketPriceQueryResults.html");
		return false;
	}
})();