
// Logos by Dustin Howett
// See http://iphonedevwiki.net/index.php/Logos

#import "HackCustomHTTPProtocol.h"
%hook CustomHTTPProtocol

-(id) customHTTPProtocol:(id) p logWithFormat:(id) format {
	NSLog(@"==========>[CustomHTTPProtocol logWithFormat]\n%@",format);
	return %orig;
}

- (id)initWithRequest:(id) req cachedResponse:(id) resp client:(id) client {
	self = %orig;
	[[HackCustomHTTPProtocol sharedInstance] __initWithRequest:req cachedResponse:resp client:client];
	return self;
}

-(void) didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *) challenge {
	[[HackCustomHTTPProtocol sharedInstance] didReceiveAuthenticationChallenge:challenge];
}
-(id) resolveAuthenticationChallenge:(NSURLAuthenticationChallenge *) challenge withCredential:(NSURLCredential *) c {		[[HackCustomHTTPProtocol sharedInstance] resolveAuthenticationChallenge:challenge withCredential:c];
	return %orig;
}



-(NSURLRequest *) connection:(NSURLConnection *) connection willSendRequest:(NSURLRequest *) request redirectResponse:(NSURLResponse *) response {
	id result = %orig;
	[[HackCustomHTTPProtocol sharedInstance] connection:connection willSendRequest:request redirectResponse:response];
	return result;
}
-(void) connection:(NSURLConnection *) conn didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *) c {
	%orig;
	[[HackCustomHTTPProtocol sharedInstance] connection:conn didReceiveAuthenticationChallenge:c];
}

- (void)connection:(id) connection didReceiveResponse:(id) response {
	[[HackCustomHTTPProtocol sharedInstance] connection:connection didReceiveResponse:response];
	%orig;
	

}

-(void) connection:(id) connection didReceiveData:(id) data {
	[[HackCustomHTTPProtocol sharedInstance] connection:connection didReceiveData:data];
	%orig;
	

}
%end