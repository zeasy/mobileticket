/**
 * Created by easy on 15/11/4.
 */

var React = require('react-native');
var Components = require('Components');

var {
    StyleSheet,
    AlertIOS,
    View
    } = React;

var {
    Controller
    } = Components;

var TestController = require('./TestController');

class MoreController extends Controller {
    constructor() {
        super();
        this.route.title = '更多';
        this.route.rightButtonTitle = 'Edit';
        this.route.onRightButtonPress = () => {
            this.props.navigator.push({
                component : MoreController,
                title : 'title'

            });
        };
    }
}

exports = module.exports = MoreController;
