/**
 * Created by easy on 16/3/19.
 */

'use strict'

import React,{
    View,
    TouchableOpacity,
    Text,
    StyleSheet,
} from 'react-native'

import Style from 'RTStyle'

import RTView from 'RTView'
import moment from  'moment'
import YpInfo from 'RTYpInfo'

class TWQueryCell extends RTView {

    constructor(props,context) {
        super(props,context);


    }

    renderColumnOne() {
        let firstCodeColor = ['G','D'].indexOf(this.firstCode) >= 0 ? Style.info : Style.primary;
        let from = this.query.start_station_telecode == this.query.from_station_telecode ? '起' : '过';
        let to = this.query.end_station_telecode == this.query.to_station_telecode ? '终' : '过';

        return (
            <View style={{flex:1}}>
                {/*"第一列"*/}
                <View style={{flexDirection:'row'}}>
                    <Text style={{alignSelf:'center',textAlign:'center',borderRadius:7,
                            backgroundColor:firstCodeColor,color:'white',fontSize:14,width:14,height:14,lineHeight:15
                            }}>{this.firstCode}</Text>
                    <Text style={{marginLeft:2,alignSelf:'center',textAlign:'center',color:Style.info,fontSize:14}}>{this.otherCode}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{alignSelf:'center',textAlign:'center',borderRadius:7,backgroundColor:Style.danger,color:'white',fontSize:10,lineHeight:11,width:14,height:14}}>{from}</Text>
                    <Text style={{marginLeft:2,alignSelf:'center',color:Style.inverse,fontSize:14}}>{this.query.start_time}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{alignSelf:'center',textAlign:'center',borderRadius:7,backgroundColor:Style.primary,color:'white',fontSize:10,lineHeight:11,width:14,height:14}}>{to}</Text>
                    <Text style={{marginLeft:2,color:Style.inverse,fontSize:14}}>{this.query.arrive_time}</Text>
                </View>
            </View>
        );
    }

    renderColumnTwo() {
        return (
            <View style={{flex:1}}>
                {/*"第二列"*/}
                <View style={{flexDirection:'row'}}>
                    <Text style={{color:Style.inverse,fontSize:14}}>{this.lishi}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{color:Style.inverse,fontSize:14}}>{this.query.from_station_name}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{color:Style.inverse,fontSize:14}}>{this.query.to_station_name}</Text>
                </View>
            </View>
        );
    }

    renderColumnThree() {
        let size = 3;
        let infos = this.sortInfos.filter(info=>info.num > 0).slice(0,size);
        let noNumInfos = this.sortInfos.filter(info=>info.num == 0).slice(0,size);
        infos = infos.concat(noNumInfos).slice(0,3);

        let views = [];
        for (let i = 0;i < infos.length;i++) {
            let info = infos[i];
            let color = Style.inverse;

            if(info.num < 50 && info.num > 20) {
                color = Style.warning;
            } else if (info.num < 20 && info.num > 0) {
                color = Style.danger;
            } else if(info.num == 0) {
                color = Style.disabled;
            }

            let otherColor = info.num > 0 ? Style.inverse:Style.disabled;

            let view = (
                <View style={{flexDirection:'row'}} key={i}>
                    <View style={{flex:1}}>
                        <Text style={{color:otherColor,fontSize:14}}>{info.type.substring(0,3)}</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <Text style={{color:otherColor,fontSize:14}}> - </Text>
                        <Text style={{color:color,fontSize:14}}>{info.num}</Text>
                    </View>
                </View>
            );
            views.push(view);
        }

        return <View style={{flex:0.7}}>
                {/*"第三列"*/}
            {views}
            </View>;
    }

    renderColumnFour() {
        let size = 3;
        let infos = this.sortInfos.filter(info=>info.num > 0).slice(0,size);
        let noNumInfos = this.sortInfos.filter(info=>info.num == 0).slice(0,size);
        infos = infos.concat(noNumInfos).slice(0,3);


        let views = [];
        for (let i = 0;i < infos.length;i++) {
            let info = infos[i];
            let color = info.num > 0 ? Style.inverse : Style.disabled;
            let view = (
                <View style={{}} key={i}>
                    <Text style={{color:color,fontSize:14,textAlign:'right'}}>{info.price}</Text>
                </View>
            );
            views.push(view);
        }

        return<View style={{flex:0.3}}>
                {/*"第四列"*/}
                {views}
            </View>;
    }

    renderOther() {
        let other = false;
        return (
            <View style={{flexDirection:'row'}}>
                {other && <Text style={{color:Style.inverse,fontSize:14,textAlign:'right'}}>{this.props.query.buttonTextInfo}</Text>}
            </View>
        );
    }

    render() {

        this.query = (this.props.query && this.props.query.queryLeftNewDTO) || {};
        this.station_train_code = this.query.station_train_code || '';
        this.firstCode = this.station_train_code.substr(0,1);
        this.otherCode = this.station_train_code.substr(1);

        if (this.firstCode < 'A' || this.firstCode > 'Z') {
            this.firstCode = '普';
            this.otherCode = this.station_train_code;
        }

        this.lishi = moment(this.query.lishi,'HHmm').format('H时mm分');

        this.infos = YpInfo.processYpInfo(this.query.yp_info) || [];
        this.sortInfos = (YpInfo.sortYpInfo(this.infos) || []).reverse();

        return (
            <View>
                <TouchableOpacity activeOpacity={0.6} style={{flexDirection:'row',marginLeft:10,marginRight:10,marginTop:8,marginBottom:8}}>
                    <View style={{flex:1,flexDirection:'row'}}>
                        {this.renderColumnOne()}
                        {this.renderColumnTwo()}
                    </View>

                    <View style={{flex:1}}>
                        <View style={{flex:1,flexDirection:'row'}}>
                            {this.renderColumnThree()}
                            {this.renderColumnFour()}
                        </View>
                        <View>
                            {this.renderOther()}
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{flex:1,height:StyleSheet.hairlineWidth,backgroundColor:Style.default,marginLeft:10,marginRight:10}}/>
            </View>
        );
    }
}

module.exports = TWQueryCell;