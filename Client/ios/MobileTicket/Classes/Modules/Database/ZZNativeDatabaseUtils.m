//
//  MTDatabaseUtils.m
//  MobileTicket
//
//  Created by EASY on 15/10/28.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZNativeDatabaseUtils.h"
#import "ZZNativeDatabaseTableModel.h"
#import <BZObjectStoreModelInterface.h>
#import <objc/runtime.h>

@implementation ZZNativeDatabaseUtils

+(objc_property_attribute_t) typePropertyWithTableType:(NSString *) type isId:(BOOL) isId {
	
	NSMutableString *builder = [NSMutableString string];
	[builder appendString:@"@\""];
	if ([@"string" compare:type options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		[builder appendString:NSStringFromClass([NSString class])];
	} else if ([@"number" compare:type options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		[builder appendString:NSStringFromClass([NSNumber class])];
	}
	if (isId) {
		[builder appendFormat:@"<%@>",NSStringFromProtocol(@protocol(OSIdenticalAttribute))];
	}
	[builder appendString:@"\""];
	objc_property_attribute_t attr = {"T",[builder UTF8String]};
	return attr;
}

+(Class) createClass:(NSString *) className withSchema:(ZZNativeDatabaseTableSchema *) schema {
	if (![className length]) {
		return nil;
	}
	
	Class clazz = NSClassFromString(className);
	if (clazz) {
		return clazz;
	}
	
	clazz = objc_allocateClassPair([ZZNativeDatabaseTableModel class], [className UTF8String], 0);

	for (ZZNativeDatabaseTableColumnSchema *columnSchema in schema.columns) {
		objc_property_attribute_t typeAttr = [self typePropertyWithTableType:columnSchema.type isId:columnSchema.isId];
		objc_property_attribute_t copyAttr = {"C",""};
		
		const objc_property_attribute_t attributes[] = {typeAttr,copyAttr};
		class_addProperty(clazz, [columnSchema.name UTF8String], attributes, 2);
		
		
	}
	
	
	objc_registerClassPair(clazz);
	return clazz;
}

@end
