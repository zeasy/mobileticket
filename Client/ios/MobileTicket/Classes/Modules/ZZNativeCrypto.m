//
//  ZZNativeCrypto.m
//  MobileTicket
//
//  Created by EASY on 15/11/17.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZNativeCrypto.h"
#import <RCTConvert.h>
#import <CocoaSecurity.h>

@implementation ZZNativeCrypto

RCT_EXPORT_MODULE(NativeCrypto)

RCT_EXPORT_METHOD(crypt:(NSString *) type args:(NSArray *) args resultType:(NSString *) resultType successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	CocoaSecurityResult *result = nil;
	id arg0 = [args count]>0?args[0]:nil;
	id arg1 = [args count]>1?args[1]:nil;
	id arg2 = [args count]>2?args[2]:nil;
	arg0 = [RCTConvert NSString:arg0];
	arg1 = [RCTConvert NSString:arg1];
	arg2 = [RCTConvert NSString:arg2];
	if ([@"aes_encrypt" isEqualToString:type]) {
		result = [CocoaSecurity aesEncrypt:arg0 key:arg1];
	}
	else if ([@"aes_key_iv_encrypt" isEqualToString:type]) {
		result = [CocoaSecurity aesEncrypt:arg0 hexKey:arg1 hexIv:arg2];
	}
	else if ([@"aes_decrypt" isEqualToString:type]) {
		result = [CocoaSecurity aesDecryptWithBase64:arg0 key:arg1];
	}
	else if ([@"aes_key_iv_decrypt" isEqualToString:type]) {
		result = [CocoaSecurity aesDecryptWithBase64:arg0 hexKey:arg1 hexIv:arg2];
	}
	else if ([@"md5" isEqualToString:type]) {
		result = [CocoaSecurity md5:arg0];
	}
	else if ([@"hmac_md5" isEqualToString:type]) {
		result =[CocoaSecurity hmacMd5:arg0 hmacKey:arg1];
	}
	else if ([@"sha1" isEqualToString:type]) {
		result = [CocoaSecurity sha1:arg0];
	}
	else if ([@"hmac_sha1" isEqualToString:type]) {
		result = [CocoaSecurity hmacSha1:arg0 hmacKey:arg1];
	}
	else if ([@"sha224" isEqualToString:type]) {
		result = [CocoaSecurity sha224:arg0];
	}
	else if ([@"hmac_sha224" isEqualToString:type]) {
		result = [CocoaSecurity hmacSha224:arg0 hmacKey:arg1];
	}
	else if ([@"sha256" isEqualToString:type]) {
		result = [CocoaSecurity sha256:arg0];
	}
	else if ([@"hmac_sha256" isEqualToString:type]) {
		result = [CocoaSecurity hmacSha256:arg0 hmacKey:arg1];
	}
	else if ([@"sha384" isEqualToString:type]) {
		result = [CocoaSecurity sha384:arg0];
	}
	else if ([@"hmac_sha384" isEqualToString:type]) {
		result = [CocoaSecurity hmacSha384:arg0 hmacKey:arg1];
	}
	else if ([@"sha512" isEqualToString:type]) {
		result = [CocoaSecurity sha512:arg0];
	}
	else if ([@"hmac_sha512" isEqualToString:type]) {
		result = [CocoaSecurity hmacSha512:arg0 hmacKey:arg1];
	}
	if (result) {
		if ([@"utf8" isEqualToString:resultType]) {
			success(@[result.utf8String?:@""]);
		}
		else if ([@"hex" isEqualToString:resultType]) {
			success(@[result.hexLower?:@""]);
		}
		else if ([@"base64" isEqualToString:resultType]) {
			success(@[result.base64?:@""]);
		}
		else {
			fail(nil);
		}
	} else {
		fail(nil);
	}
}


@end
