
/* JavaScript content from js/controller/agencySellTicketQuery.js in folder common */
(function(){
	var focusArray=[];
	function registerAutoScroll(){	
		var util = mor.ticket.util;	
		if(util.isIPhone()){
			util.enableAutoScroll('#agencyName',focusArray);
			util.enableAutoScroll('#province',focusArray);
			util.enableAutoScroll('#city',focusArray);
			util.enableAutoScroll('#county',focusArray);
		}
	}
	jq("#agencySellTicketQueryView").live("pageinit", function(){	
		registerAutoScroll();
		registerTrainQueryBtnLisener();
		registerBackButtonHandler();
		mor.ticket.util.initAppVersionInfo();
		mor.ticket.viewControl.tab1_cur_page = "agencySellTicketQuery.html";
		jq("#mapsearchBtn").bind("tap",function(){
			window.plugins.childBrowser.showTpcWebView("http://map.wap.qq.com/x/index.jsp?sid=AT364KWTF7FdqdK_thITOuyh&welcomeChange=1&poi_table=ticket_outlets&needlocation=1&radius=1000&g_ut=3&referer=12306wxmp_lbscloud_nearby&type=lbscloudnearby&lbskey=JK7BZ-HI4HV-LBNPS-UF4UL-VGU47-KMBSK", "周边代售点", {}); 
		});
	});
	
	function registerTrainQueryBtnLisener(){
		jq("#province").change("tap", queryCityRequest);
		jq("#city").change("tap", queryCountyRequest);
		jq("#QueryDetailsSellBtn").click("tap",queryDetailsSellRequest);
	};
	
	function queryCityRequest(e) {
		var util = mor.ticket.util;	
		var commonParameters = {
			'province': jq("#province").val().trim()
		};
		
		var invocationData = {
			adapter: mor.ticket.viewControl.adapterUsed,
			procedure: "queryAgentSellCity"
		};
		
		var options =  {
				onSuccess: requestCitySucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.viewControl.show_busy = false;
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestCitySucceeded(result) {
			jq("#city").empty();
			jq("#city").append("<option value='city' selected='selected'>请选择市</option>");
			jq("#county").empty();
			jq("#county").append("<option value='county' selected='selected'>请选择区/县</option>");
			jq("#county").selectmenu('refresh', true);
			var invocationResult = result.invocationResult;
			if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				for ( var i = 0; i < invocationResult.ofqAgencyCacheResult.length; i++) {
					var htmlStr = "<option value='" + invocationResult.ofqAgencyCacheResult[i] + "'>" + invocationResult.ofqAgencyCacheResult[i] + "</option>";
					var jq_select_city = jq("#city");
					jq_select_city.append(htmlStr);
					jq("#city").selectmenu('refresh', true);
				}
			}else {	
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}
		}
	function queryCountyRequest(e) {
		var util = mor.ticket.util;	
		var commonParameters = {
			'province': jq("#province").val().trim(),
			'city': jq("#city").val().trim()
		};
		
		var invocationData = {
			adapter: mor.ticket.viewControl.adapterUsed,
			procedure: "queryAgentSellCounty"
		};
		
		var options =  {
				onSuccess: requestCountySucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.viewControl.show_busy = false;
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestCountySucceeded(result) {
			jq("#county").empty();
			jq("#county").append("<option value='county' selected='selected'>请选择区/县</option>");
			var invocationResult = result.invocationResult;
			if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				for ( var i = 0; i < invocationResult.ofqAgencyCacheResult.length; i++) {
					var htmlStr = "<option value='" + invocationResult.ofqAgencyCacheResult[i] + "'>" + invocationResult.ofqAgencyCacheResult[i] + "</option>";
					var jq_select_county = jq("#county");
					jq_select_county.append(htmlStr);
					jq("#county").selectmenu('refresh', true);
				}
			}else {	
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}
		}
	
	function queryDetailsSellRequest(e) {
	    var province = jq("#province").val();
	    var county = jq("#county").val();
	    var city = jq("#city").val();
		if(province == "province"){
			 WL.SimpleDialog.show(
						"温馨提示", 
						"请选择省", 
						[ {text : '确定', handler: function() {
						}}]
			  );
			return;
		}
		if(city == "city"){
			 WL.SimpleDialog.show(
						"温馨提示", 
						"请选择市", 
						[ {text : '确定', handler: function() {
						}}]
			  );
			return;
		}
		if(county == "county"){
			 WL.SimpleDialog.show(
						"温馨提示", 
						"请选择区/县", 
						[ {text : '确定', handler: function() {
						}}]
			  );
			return;
		}
		var agencySellTicketQuery = mor.ticket.views.agencySellTicketQuery;
			agencySellTicketQuery.province = province;
			agencySellTicketQuery.city = city;
			agencySellTicketQuery.county = county;
			agencySellTicketQuery.agency_name = jq("#agencyName").val();
			mor.ticket.util.changePage("agencySellTicketList.html");
		return false;
	}
	
	function registerBackButtonHandler() {
		jq("#agencySellTicketQueryBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage(vPathCallBack() + "moreOption.html");
			return false;
		});
	}
})();