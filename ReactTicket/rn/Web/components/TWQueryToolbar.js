/**
 * Created by easy on 16/3/22.
 */


import RTView from 'RTView'

import React,{
    View,
    TouchableOpacity,
    Text,
    StyleSheet,
} from 'react-native'

import Style from 'RTStyle'
var Icon = require('react-native-vector-icons/Ionicons');
import moment from 'moment'

class TWQueryToolbar extends RTView {
    componentWillUpdateProps() {}

    onPrevious() {
        if (!this.previousDisabled)
            this.props.onPrevious && this.props.onPrevious();
    }

    onNext() {
        if (!this.nextDisabled)
            this.props.onNext && this.props.onNext();
    }

    render() {
        this.day = this.props.day;
        this.previousDisabled = this.props.previousDisabled || this.day <= moment().format('YYYY-MM-DD');
        this.nextDisabled = this.props.nextDisabled; // || 超过预售期

        let previousColor = this.previousDisabled?Style.disabled:Style.inverse;
        let nextColor = this.nextDisabled?Style.disabled:Style.inverse;
        let enabled = this.props.enabled || true;
        if (enabled) {
            return (
                <View style={{height:44,backgroundColor:'white'}}>
                    <View style={{height:StyleSheet.hairlineWidth,backgroundColor:Style.default}}/>
                    <View style={{flex:1,flexDirection:'row',marginLeft:40,marginRight:40}}>
                        <TouchableOpacity style={{flexDirection:'row',justifyContent:'center'}} activeOpacity={this.previousDisabled?1:0.6} onPress={this.onPrevious.bind(this)}>
                            <Icon name="ios-arrow-left" size={20} color={previousColor} style={{alignSelf:'center'}}/>
                            <Text style={{alignSelf:'center',marginLeft:5,marginTop:-2,color:previousColor}}>前一天</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{flex:1,justifyContent:'center'}}>
                            <Text style={{alignSelf:'center',marginTop:-1}}>{this.day}</Text>
                        </TouchableOpacity>


                        <TouchableOpacity style={{flexDirection:'row',justifyContent:'center'}} activeOpacity={this.nextDisabled?1:0.6} onPress={this.onNext.bind(this)}>
                            <Text style={{alignSelf:'center',marginRight:5,marginTop:-2,color:nextColor}}>后一天</Text>
                            <Icon name="ios-arrow-right" size={20} color={nextColor} style={{alignSelf:'center'}}/>
                        </TouchableOpacity>

                    </View>

                </View>
            );
        } else {
            return (<View/>);
        }

    }
}

module.exports = TWQueryToolbar;