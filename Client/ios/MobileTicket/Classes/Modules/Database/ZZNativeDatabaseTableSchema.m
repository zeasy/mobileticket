//
//  MTDatabaseTableSchema.m
//  MobileTicket
//
//  Created by EASY on 15/10/28.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZNativeDatabaseTableSchema.h"
#import <RCTConvert.h>

@implementation ZZNativeDatabaseTableColumnSchema



-(instancetype)initWithDictionary:(NSDictionary *) dict name:(NSString *) name{
	self = [super init];
	if (self) {
		_name = name;
		_type = [RCTConvert NSString:dict[@"type"]];
		_isId = [RCTConvert BOOL:dict[@"id"]];
	}
	return self;
}

@end

@implementation ZZNativeDatabaseTableSchema

-(instancetype)initWithDictionary:(NSDictionary *) dict {
	self = [super init];
	if (self) {
				NSMutableArray *columns = [NSMutableArray array];
		
		for (id k in [dict allKeys]) {
			NSString *key = [RCTConvert NSString:k];
			id v = dict[k];
			if ([v isKindOfClass:[NSString class]]) {
				v = @{@"type":v};
			}
			if ([v isKindOfClass:[NSDictionary class]]) {
				ZZNativeDatabaseTableColumnSchema *column = [[ZZNativeDatabaseTableColumnSchema alloc] initWithDictionary:v name:key];
				[columns addObject:column];
			}
		}
		
		_columns = columns;
	}
	return self;
}

@end
