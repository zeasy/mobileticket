//
//  ZZTableViewCell.h
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RCTEventDispatcher.h>
#import <RCTComponent.h>

@class _ZZTableViewCell;

@interface ZZTableViewCell : UIView

- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher;

@property (nonatomic, strong) NSString *reuseIdentifier;
@property (nonatomic, assign) UITableViewCellStyle cellStyle;
@property (nonatomic, assign) NSIndexPath *indexPath;
@property (nonatomic, strong) NSDictionary *rowData;
@property (nonatomic, strong) NSDictionary *sectionData;


@property (nonatomic, strong) RCTDirectEventBlock onSelected;
@property (nonatomic, strong) RCTDirectEventBlock onHighlighted;
@property (nonatomic, strong) RCTDirectEventBlock onEditing;


@property (nonatomic, weak) _ZZTableViewCell *cell;

@end


@interface _ZZTableViewCell : UITableViewCell

@property (nonatomic, strong) ZZTableViewCell *cell;

@end