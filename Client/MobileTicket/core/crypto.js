/**
 * @providesModule Crypto
 * Created by easy on 15/11/17.
 */

"use strict";
var ReactPropTypes = require('ReactPropTypes');
var NativeCrypto = require('NativeModules').NativeCrypto;
var Interface = require('Interface');

class Crypto {

    static aesEncrypt(data = '',key = '') {
        return crypto_crypt('aes_encrypt',[data,key]);
    }

    static aesEncryptKeyIv(data = '',hexKey = '',hexIv = '') {
        return crypto_crypt('aes_key_iv_encrypt',[data,hexKey,hexIv]);
    }

    static aesDecrypt(base64Data = '',key = '') {
        return crypto_crypt('aes_decrypt',[base64Data,key]);
    }

    static aesDecryptKeyIv(base64Data = '',hexKey = '',hexIv = '') {
        return crypto_crypt('aes_key_iv_decrypt',[base64Data,hexKey,hexIv]);
    }

    static md5(data = '') {
        return crypto_crypt('md5',[data]);
    }

    static hmacMd5(data = '',key = '') {
        return crypto_crypt('hmac_md5',[data,key]);
    }

    static sha1(data = '') {
        return crypto_crypt('sha1',[data]);
    }

    static hmacSha1(data = '',key = '') {
        return crypto_crypt('hmac_sha1',[data,key]);
    }

    static sha224(data = '') {
        return crypto_crypt('sha224',[data]);
    }

    static hmacSha224(data = '',key = '') {
        return crypto_crypt('hmac_sha224',[data,key]);
    }

    static sha256(data = '') {
        return crypto_crypt('sha256',[data]);
    }

    static hmacSha256(data = '',key = '') {
        return crypto_crypt('hmac_sha256',[data,key]);
    }

    static sha384(data = '') {
        return crypto_crypt('sha384',[data]);
    }

    static hmacSha384(data = '',key = '') {
        return crypto_crypt('hmac_sha384',[data,key]);
    }

    static sha512(data = '') {
        return crypto_crypt('sha512',[data]);
    }

    static hmacSha512(data = '',key = '') {
        return crypto_crypt('hmac_sha512',[data,key]);
    }
}

Crypto.Types = ReactPropTypes.oneOf(
    [
        'aes_encrypt' /*default*/, 'aes_key_iv_encrypt',
        'aes_decrypt', 'aes_key_iv_decrypt',
        'md5', 'hmac_md5',
        'sha1', 'hmac_sha1',
        'sha224', 'hmac_sha224',
        'sha256', 'hmac_sha256',
        'sha384','hmac_sha384',
        'sha512','hmac_sha512'
    ]
);

var crypto_crypt = (type : Crypto.Types,args : Array) => {
    return new Result(type,args);
}


class Result {
    constructor(type,args) {
        this._type = type;
        this._args = args;
    }

    async asyncUtf8 () {
        return result_crypt(this._type,this._args,'utf8');
    }

    async asyncHex () {
        return result_crypt(this._type,this._args,'hex');
    }

    async asyncBase64 () {
        return result_crypt(this._type,this._args,'base64');
    }
}

Result.Types = ReactPropTypes.oneOf(
    [
        'utf8' /*default*/,
        'hex',
        'base64'
    ]
);

var result_crypt = async (type,args,resultType) => {
    return Interface.asyncInvoke(NativeCrypto.crypt,type,args,resultType);
}

Crypto.Result = Result;
exports = module.exports = Crypto;