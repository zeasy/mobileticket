/**
 * @providesModule Analytics
 * Created by easy on 15/11/17.
 */

"use strict";
var NativeAnalytics = require('NativeModules').NativeAnalytics;
var Interface = require('Interface');

class Analytics {
    static async asyncBeginPageView(pageView) {
        return Interface.asyncInvoke(NativeAnalytics.beginPageView,pageView);
    }

    static async asyncEndPageView(pageView) {
        return Interface.asyncInvoke(NativeAnalytics.endPageView,pageView);
    }

    static async asyncEvent(event,attrs) {
        return Interface.asyncInvoke(NativeAnalytics.event,event,attrs);
    }

    static async asyncUpdateOnlineConfigs() {
        return Interface.asyncInvoke(NativeAnalytics.updateOnlineConfigs);
    }

    static async asyncOnlineConfig(key) {
        return Interface.asyncInvoke(NativeAnalytics.onlineConfigParam,key);
    }
}

exports = module.exports = Analytics;