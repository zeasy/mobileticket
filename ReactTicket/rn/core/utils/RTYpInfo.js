/**
 * @providesModule RTYpInfo
 * 余票信息
 * Created by easy on 16/3/22.
 */

'use strict'

import cache from 'RTCacheMap'

class RTYpInfo {

    static processYpInfo(ypinfo) {
        if (!ypinfo || ypinfo.length == 0) {
            return [];
        }
        var arrayLength = ypinfo.length / 10;
        var obj = new Array();
        var seat_type = '';
        var temp_seat_rate = 50;// 对于没有定义的座位，rate值从50开始递增
        for ( var i = 0, m = 6, n = 10, x = 0, y = 1; i < arrayLength; i++, m = m + 10, n = n + 10, x = x + 10, y = y + 10) {
            var seat_type_id = ypinfo.substring(x, y);
            var seat_num = 0;
            var seat_type_rate = null;
            if (parseInt(ypinfo.substring(m, m + 1), 10) >= 3) {
                seat_type = "无座";
                seat_type_rate = 100;
                seat_num = parseInt(ypinfo.substring(m, n), 10)-3000;
            } else {
                seat_type = cache.getSeatTypeByCode(seat_type_id);
                seat_type_rate = cache.getSeatTypeRateByCode(seat_type_id);
                if (seat_type_rate === null || seat_type_rate === "undefined") {
                    seat_type_rate = temp_seat_rate;
                    temp_seat_rate = temp_seat_rate + 1;
                }
                seat_num = parseInt(ypinfo.substring(m, n), 10);
            }
            var fareNewPrice = ''+(parseFloat(ypinfo.substring(y, m), 10) / 10).toFixed(1);
            if(fareNewPrice == '0.0'){
                fareNewPrice = '';
            }
            obj[i] = {
                type_id : seat_type_id,
                type : seat_type,
                type_rate : seat_type_rate,
                num : seat_num,
                price : fareNewPrice
            };
        }
        return RTYpInfo.sortYpInfo(obj);
    }

    static sortYpInfo(ypinfos) {

        var by = function(name) {
            return function(o, p) {
                var a, b;
                if (typeof o === "object" && typeof p === "object" && o && p) {
                    a = o[name];
                    b = p[name];
                    if (a === b) {
                        return 0;
                    }
                    if (typeof a === typeof b) {
                        return a < b ? -1 : 1;
                    }
                    return typeof a < typeof b ? -1 : 1;
                } else {
                    throw ("error");
                }
            };
        };

        ypinfos.sort(by("type_rate"));
        return ypinfos;

    }
}

module.exports = RTYpInfo;