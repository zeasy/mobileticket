//
//  ZZTableDataSource.h
//  MobileTicket
//
//  Created by EASY on 15/11/12.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZZTableDataSource : NSObject

@property (nonatomic, strong) NSMutableArray *sections;

@end


@interface ZZTableRow : NSObject

@property (nonatomic, strong) NSString *reuseIdentifier;
@property (nonatomic, strong) NSDictionary *contents;

@end

@interface ZZTableSectionHeaderFooter : NSObject

@property (nonatomic, strong) NSString *reuseIdentifier;
@property (nonatomic, strong) NSDictionary *contents;

@end

@interface ZZTableSection : NSObject

@property (nonatomic, strong) NSMutableArray *rows;
@property (nonatomic, strong) ZZTableSectionHeaderFooter *header;
@property (nonatomic, strong) ZZTableSectionHeaderFooter *footer;

@property (nonatomic, strong) NSString *indexTitle;
@property (nonatomic, strong) NSDictionary *contents;

@end