/**
 * Created by easy on 15/11/3.
 */

var React = require('react-native');
var Analytics = require('../core').Analytics;

class Controller extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.state || {};

        this.class = this.constructor;
        this.title = this.class.name;

        this.route = {};
        this.route.component = this.class;

        this.route.title = '';      //set your component title
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    }

    componentWillMount() {
        Analytics.asyncBeginPageView(this.analyticsTitle()).then();
        if (this.props.navigator) {
            this.props.navigator.replace(this.route);
        }
    }

    componentWillUnmount() {
        Analytics.asyncEndPageView(this.analyticsTitle()).then();
    }

    event(attrs) {
        Analytics.asyncEvent(this.analyticsTitle(),attrs).then();
    }

    analyticsTitle () {
        return this.title || this.route.title;
    }

    render() {
        return (<React.View />);
    }
}

exports = module.exports = Controller;
