/**
 * @providesModule KeyValueStore
 * Created by easy on 15/11/17.
 */

'use strict';

var Database = require('Database');

class KeyValueStore {
    constructor(database) {
        this.database = database;
        this.tableSchema = {
            key : {
                type : 'string',
                id : true
            },
            value : 'string'
        };
        this.tableName = 'KeyValueStore';
        this.table = this.database.table(this.tableName,this.tableSchema);
    }

    async asyncSet (key, value) {
        return this.table.asyncSave([{
            key : key,
            value: value
        }]);
    }

    async asyncGet (key) {
        return this.table.asyncFetch({
            where : "key = '"+ key + "'"
        }).then((results)=>{
            return results[0]?results[0].value : null;
        });
    }

    async asyncRemove (key) {
        return this.table.asyncRemove({
            where : "key = '"+ key + "'"
        });
    }

    static defaultStore () {
        return DefaultStore;
    }
}

var DefaultStore = new KeyValueStore(Database.defaultDatabase());

exports = module.exports = KeyValueStore;