
/* JavaScript content from js/controller/orderTrackDetail.js in folder common */
(function() {		
	jq("#OrderTrackDetailView").live("pageinit", function() {
		jq("#orderTrackBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("finishedOrderDetail.html");
			return false;
		});
	});
	jq("#OrderTrackDetailView").live("pagebeforeshow",function(){
		mor.ticket.util.initAppVersionInfo();
		var user = mor.ticket.loginUser;		
		if(user.isAuthenticated === "Y"){
			orderTrackDetailFn();
		}else{
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
			} else {
				registerAutoLoginHandler(orderTrackDetailFn, autologinFailJump);
			}
		}
	});
	function orderTrackDetailFn(){
		var orderTrackData = mor.ticket.finishOrderTrack;
		if(orderTrackData.finishOrderTicketInfo.length > 0){
			jq("#sequenceNo").html(orderTrackData.sequence_no);
			jq("#orderDate").html(orderTrackData.order_date);
			jq("#orderTrackList").html(generateOrderTrackList(orderTrackData.finishOrderTicketInfo)).listview("refresh");
		}
		jq("#OrderTrackDetailView .ui-content").iscrollview("refresh");
	}
	
	var orderTrackListTemplate =
		"{{~it :ticket:index}}" +
		"<li data-index='{{=index}}' style='padding:5px 0px;'>" +
			"<div class='ui-grid-a'>" +
				"<div class='ui-block-a'><span style='margin-left:5px'>{{=index + 1}}</span></div>" +
				"<div class='ui-block-b departlongline'>" +
				"{{=ticket.ticketInfo.passenger_name}}&nbsp;&nbsp;" +
				"{{=mor.ticket.util.changeDateType(ticket.ticketInfo.train_date)}}&nbsp;&nbsp;" +
				"{{=ticket.ticketInfo.board_train_code}}&nbsp;&nbsp;" +
				"{{=ticket.ticketInfo.from_station_name}}→" +
				"{{=ticket.ticketInfo.to_station_name}}" +
				"</div>" +
			"</div>" +
			" {{ for(var i = 0;i<ticket.operateTimeStatues.length;i++){ }}" +
			"<div class='ui-grid-b'>" +
				"<div class='ui-block-a'>{{=ticket.operateTimeStatues[i].split('#')[0]}}&nbsp;&nbsp;" +
				"{{=ticket.operateTimeStatues[i].split('#')[1]}}&nbsp;&nbsp;" +
				"({{=ticket.operateTimeStatues[i].split('#')[2]}})</div>" +
			"</div>" +
			 "{{ } }}" +
		"</li>" +
		"{{~}}";
	var generateOrderTrackList = doT.template(orderTrackListTemplate);
})();