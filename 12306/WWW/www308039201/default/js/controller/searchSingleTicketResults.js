
/* JavaScript content from js/controller/searchSingleTicketResults.js in folder common */
(function() {
	/* 
	 * byLeftOrPriceSortFlag：byLeft--余票结果排序;byPrice--票价结果排序
	 * isCheckedLeftOrPriceFlag：  0--余票选中；1--票价选中
	 * byLiShiSortUpDownTicketResultFlag:1--历时升序，0--历时降序
	 * byStartTimeSortUpDownTicketResultFlag：3--出发时间升序，2--出发时间降序
	 * byArriveTimeSortUpDownTicketResultFlag：5--到达时间升序，4--到达时间降序
	 * sortImageFlag：01--向下排序箭头图标为蓝色选中，02--向上排序箭头图标为蓝色选中
	 * sortImageColor：1--点击历时排序，2--点击出发时间排序，3--点击到达时间排序
	 * 
	 */
	var byLeftOrPriceSortFlag = "byLeft";
	var isCheckedLeftOrPriceFlag = "0";
	var byLiShiSortUpDownTicketResultFlag  = '1';
	var byStartTimeSortUpDownTicketResultFlag  = '3';
	var byArriveTimeSortUpDownTicketResultFlag  = '5';
	var sortImageFlag  = '00';
	var sortImageColor  = "0";
	var newTicketrusult = [];
	var tapholding = false;
	var changeStationing = false;
	var prevPage;
	var sortUpDownPreId = "default";
	var util = mor.ticket.util;
	var saveImgName = "";
	var shareImageName = "";
	var popupWindow = null;
	jq("#searchSingleTicketResultsView").live("pageinit", function() {
		registerChangestationFromorTo();
		registerTicketListItemClickHandler();
		registerPrevDateBtnClickHandler();
		registerNextDateBtnClickHandler();
		registerrefreshsearchorderRefreshButtonListener();
		registSelectDateChangeHandler();
		registerTicketSortListener();
		regiterTicketYpAndPriceListener();
		registerBackButtonHandler();
		registerMoreStopStationListener();
		toDatePicker();
		registerShareBtnClickHandler();
		registerScreenShotBtnClickHandler();
		registerMoreBtnClickHandler();
		registerRefreshBtnClickHandler();
		jq("#searchSingleTicketResultList_List_content", this).bind( {"iscroll_onpulldown" : onPullDown});
		if(mor.ticket.searchResultList.leftOrPriceFlag == "1"){
			byLeftOrPriceSortFlag = "byLeft";
			isCheckedLeftOrPriceFlag = "0";
			byLiShiSortUpDownTicketResultFlag  = '1';
			byStartTimeSortUpDownTicketResultFlag  = '3';
			byArriveTimeSortUpDownTicketResultFlag  = '5';
			sortImageFlag  = '00';
			sortImageColor  = "0";
			sortUpDownPreId = "byStartTimeSortTicketResult";
		}	
	});
	//右上角菜单
	function registerMoreBtnClickHandler(){
		popupWindow = new mor.ticket.util.simplePopup({
	        fireBtn: jq("#moreBtn"),
	        mask:    jq("#popupBasic-screen"),
	        pop:     jq("#popupBasic")
	    });
	}
	
	//added by qdy
	function toDatePicker(){
	jq("#trainDateInputId").bind("tap",function(){
		var reservePeriod;
		var str = "searchSingleTicketResults.html";
		mor.ticket.currentPagePath.fromPath = str;
		var goingPath = vPathCallBack()+ "datePicker.html";
		mor.ticket.currentPagePath.type = true;
		mor.ticket.currentPagePath.show = false;
		//重设日期，刷新控件
		var monthValue;
		var current = mor.ticket.currentPagePath;
		current.defaultDateValue[0]=jq("#chooseDateBtn").val().slice(0,4);
		monthValue=parseInt(jq("#chooseDateBtn").val().slice(5,7));
		if(monthValue == "1"){
			monthValue = "0";
		}else monthValue = monthValue-1;
		if(monthValue < 10){
			current.defaultDateValue[1] = "0"+monthValue;
		}else{
			current.defaultDateValue[1] = monthValue;
		}
		current.defaultDateValue[2]=jq("#chooseDateBtn").val().slice(8,10);
		reservePeriod = mor.ticket.history.reservePeriod;
		/*if(mor.ticket.viewControl.bookMode === "gc"){
			if(mor.ticket.leftTicketQuery.isTwoday == true){
				reservePeriod = mor.ticket.leftTicketQuery.dayLimit;
			}
		}*/
		jQuery.extend(jQuery.mobile.datebox.prototype.options, {
			mode:"calbox",
			useInline: true,
			calControlGroup: true,
			defaultValue: current.defaultDateValue,
			showInitialValue: true,
			lockInput: false,
			useFocus: true,
			maxDays: reservePeriod,
			buttonIcon: "grid",
			calUsePickers: true,
			afterToday: true,
			beforeToday: false,
			notToday: false,
			calOnlyMonth: false,
			overrideSlideFieldOrder: ['y','m','d','h','i']
		});
		jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
		mor.ticket.util.changePage(goingPath);
		jq(".ui-datebox-container .ui-btn-b").css({"background-color":"white","border-color":"white"});
	});
	}
	//往下拉是重新刷新
	function onPullDown(event, data){
		tapholding = true;
		gotPullDownData(event, data); 
		setTimeout(function(){
			tapholding = false;
		}, 2000);
	}
	
	function  gotPullDownData(event, data){
		var query = mor.ticket.leftTicketQuery;
		mor.ticket.viewControl.show_busy = false;
		searchAndShowResult(query);	
	}
	function registSelectDateChangeHandler(){
		jq("#chooseDateBtn").change(function(){
			var date = mor.ticket.util.setMyDate(jq("#chooseDateBtn").val());
			var selectDate = new Date(date.getTime());
			if (mor.ticket.viewControl.bookMode == "fc") {
				mor.ticket.leftTicketQuery.train_date_back = selectDate.format("yyyy-MM-dd");
				var from_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode_back);
				var to_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode_back);
				var trainBackDate = mor.ticket.leftTicketQuery.train_date_back.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date_back.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date_back.substring(8,10)+"日";
				saveImgName = trainBackDate+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
				shareImageName = mor.ticket.leftTicketQuery.train_date_back+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
			}else{
				mor.ticket.leftTicketQuery.train_date = selectDate.format("yyyy-MM-dd");
				var from_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode);
				var to_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode);
				var trainDate = mor.ticket.leftTicketQuery.train_date.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date.substring(8,10)+"日";
				saveImgName = trainDate+"_"+from_station_name+"-"+to_station_name+"_车次余票";
				shareImageName = mor.ticket.leftTicketQuery.train_date+"_"+from_station_name+"-"+to_station_name+"_车次余票";
			}
			searchAndShowResult(mor.ticket.leftTicketQuery);
		});
	}
	//new add by zzc end
	/*
	 * 返回日期的星期
	 *new add by zzc begin
	 */
	
	function getWeek(prompt) {
		var desc = [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ];
		if (typeof prompt == "string") {
			var date = mor.ticket.util.setMyDate(prompt);
			return desc[date.getDay()];
		} else {
			return desc[prompt.getDay()];
		}
	}
	/*
	 * 返回日期的星期
	 *new add by zzc end
	 */
	jq("#searchSingleTicketResultsView")
			.live(
					"pagehide",
					function(e, data) {
						if (mor.ticket.viewControl.bookMode == "fc") {
							if (data.nextPage.attr("id") != "bookTicketView"
								&& data.nextPage.attr("id") != "searchResultScreenView"
									&& data.nextPage.attr("id") != "passengersView"
									&& data.nextPage.attr("id") != "datePickerView") {
								mor.ticket.viewControl.bookMode = "dc";
								mor.ticket.viewControl.tab1_cur_page = vPathViewCallBack()
										+ "MobileTicket.html";
							}
						}
					});

	jq("#searchSingleTicketResultsView")
			.live(
					"pagebeforeshow",
					function(e,data) {
						var model = mor.ticket.leftTicketQuery;
						var cache = mor.ticket.cache;
						var from_station_telecode = model.from_station_telecode;
						var to_station_telecode = model.to_station_telecode;
						prevPage = data.prevPage.attr("id");
						if(mor.ticket.viewControl.bookMode === "fc"){
							jq("#fromToStation").html(cache.getStationNameByCode(to_station_telecode) + "--" + cache.getStationNameByCode(from_station_telecode) + "(返程)");
							if(prevPage == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
								model.train_date_back = mor.ticket.currentPagePath.calenderDate;
							}
							jq("#chooseDateBtn").val(model.train_date_back);
						}else if(mor.ticket.viewControl.bookMode === "gc"){
							var str = "";
							if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
								str = "变更到站";
							}else{
								str = "改签";
							}
							jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode) + "--" + cache.getStationNameByCode(to_station_telecode) + "("+str+")");
							if(prevPage == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
								model.train_date = mor.ticket.currentPagePath.calenderDate;
							}
							jq("#chooseDateBtn").val(model.train_date);
						}else{
							registerChangeStation();
							jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode) + "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
							if(prevPage == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
								model.train_date = mor.ticket.currentPagePath.calenderDate;
							}
							jq("#chooseDateBtn").val(model.train_date);
						}
						if (model.purpose_codes === "0X") {
							if (jq("#fromToStation").children("b")
									.size() < 1) {
							jq("#fromToStation").append("(学生)");
							}
						}

						if(isCheckedLeftOrPriceFlag == "1"){
							jq("#priceTicketResultBtn").css("font-size","16px");
							jq("#priceTicketResultBtn").css("color","#3289c9");
							jq("#leftTicketResultBtn").css("font-size","15px");
							jq("#leftTicketResultBtn").css("color","white");
							byLeftOrPriceSortFlag = "byPrice";
						}
						if(isCheckedLeftOrPriceFlag == "0"){
							jq("#priceTicketResultBtn").css("font-size","15px");
							jq("#priceTicketResultBtn").css("color","white");
							jq("#leftTicketResultBtn").css("font-size","16px");
							jq("#leftTicketResultBtn").css("color","#3289c9");
							byLeftOrPriceSortFlag = "byLeft";
						}
						if (mor.ticket.viewControl.bookMode == "fc") {
							var from_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode_back);
							var to_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode_back);
							var trainBackDate = mor.ticket.leftTicketQuery.train_date_back.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date_back.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date_back.substring(8,10)+"日";
							saveImgName = trainBackDate+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
							shareImageName = mor.ticket.leftTicketQuery.train_date_back+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
						}else{
							var from_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode);
							var to_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode);
							var trainDate = mor.ticket.leftTicketQuery.train_date.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date.substring(8,10)+"日";
							saveImgName = trainDate+"_"+from_station_name+"-"+to_station_name+"_车次余票";
							shareImageName = mor.ticket.leftTicketQuery.train_date+"_"+from_station_name+"-"+to_station_name+"_车次余票";
						}
					});
	
	jq("#searchSingleTicketResultsView")
	.live(
			"pageshow",
			function() {
				mor.ticket.viewControl.tab1_cur_page = "searchSingleTicketResults.html";
				var query = mor.ticket.leftTicketQuery;
				if(prevPage === "searchResultScreenView" || prevPage === "passengersView"){
					if(query.result.length == 0){
						return false;
					}
					var tipMessage = "";
					var map = [];
					if (mor.ticket.searchResultScreen.chooseQueryFlag == true){
						mor.ticket.searchResultScreen.filterResult();
						map = mor.ticket.searchResultScreen.result;
						if(prevPage == "searchResultScreenView"){
							if(!mor.ticket.searchResultScreen.isHasFilterCondition()){
								tipMessage = "<span>共查询到"+query.result.length+"趟列车</span>";
							}else{
								tipMessage = "<span>共查询到"+query.result.length+"趟列车,筛选后有"+map.length+"趟列车</span>";
							}
							jq("#trainCount").html(tipMessage);
							jq("#trainCount").show();
							jq("#trainCount").fadeOut(3000);
						}
					}else if(mor.ticket.searchResultScreen.exitFlag == true){
						mor.ticket.searchResultScreen.chooseQueryFlag = true;
						map = mor.ticket.searchResultScreen.result;
						if(map.length == 0){
							map = newTicketrusult;
						}
						if(!mor.ticket.searchResultScreen.isHasFilterCondition()){
							map = query.result;
							tipMessage = "<span>共查询到"+query.result.length+"趟列车</span>";
						}else{
							tipMessage = "<span>共查询到"+query.result.length+"趟列车,筛选后有"+map.length+"趟列车</span>";
						}
						jq("#trainCount").html(tipMessage);
						jq("#trainCount").show();
						jq("#trainCount").fadeOut(3000);
					}else{
						mor.ticket.searchResultScreen.result = [];
						mor.ticket.searchResultScreen.chooseQueryFlag = true;
						map = newTicketrusult;
						if(!mor.ticket.searchResultScreen.isHasFilterCondition()){
							tipMessage = "<span>共查询到"+query.result.length+"趟列车</span>";
						}else{
							tipMessage = "<span>共查询到"+query.result.length+"趟列车,筛选后有"+map.length+"趟列车</span>";
						}
						jq("#trainCount").html(tipMessage);
						jq("#trainCount").show();
						jq("#trainCount").fadeOut(3000);
					}
					newTicketrusult = map;
					if(sortUpDownPreId != ""){
						var id = sortUpDownPreId;
						sortUpDownPreId = "back";
						jq("#" + id).click();
					}
					jq("#iscroll-pulldown").css("visibility","visible");
					return false;
				}
				if(prevPage == "datePickerView"){
					jq("#chooseDateBtn").change();
					return false;
				}
				
				if (query.result.length == 0) {
					if (mor.ticket.viewControl.bookMode === "fc") {
						if (query.from_station_telecode_back == '') {
							mor.ticket.util.alertMessage("出发地不能为空");
							jq.mobile.changePage(vPathViewCallBack()
									+ "MobileTicket.html");
							return false;
						} else if (query.to_station_telecode_back == '') {
							mor.ticket.util.alertMessage("目的地不能为空");
							jq.mobile.changePage(vPathViewCallBack()
									+ "MobileTicket.html");
							return false;
						}
						;
					} else {
						if (query.from_station_telecode == '') {
							mor.ticket.util.alertMessage("出发地不能为空");
							jq.mobile.changePage(vPathViewCallBack()
									+ "MobileTicket.html");
							return false;
						} else if (query.to_station_telecode == '') {
							mor.ticket.util.alertMessage("目的地不能为空");
							jq.mobile.changePage(vPathViewCallBack()
									+ "MobileTicket.html");
							return false;
						}
					}
					searchAndShowResult(query);
				}
			});
	
	function registerTicketSortListener(){
		//按历时排序
		jq("#byLiShiSortTicketResult").click(function(){
			if(sortUpDownPreId != "back"){
				showImg("请稍候...");
			}
			sortImageColor = "1";
			jq("#train_start_time_text span").css("color","#fff");
			jq("#train_arrive_time_text span").css("color","#fff");
			jq("#train_li_shi_text span").css("color","#3289c9");
			var currId = jq(this).attr("id");
			if(currId == sortUpDownPreId){
				if(byLiShiSortUpDownTicketResultFlag == "0"){
					byLiShiSortUpDownTicketResultFlag = "1";
				}else{
					byLiShiSortUpDownTicketResultFlag = "0";
				}
			}
			sortUpDownPreId = currId;
			if(byLiShiSortUpDownTicketResultFlag == '1'){
				sortTicketResult(newTicketrusult,"1","lishi");
				if(byLeftOrPriceSortFlag == "byLeft"){
					showSearchResults(newTicketrusult,"left");
				}
				if(byLeftOrPriceSortFlag == "byPrice"){
					showSearchResults(newTicketrusult,"price");
				}
				//mor.ticket.util.contentIscrollTo(0, 0, 0);
				jq("#train_li_shi_sort_down").css("display","none");
				jq("#train_li_shi_sort_up").css("background-image","url(../images/up_blue.png)");
				jq("#train_li_shi_sort_up").css("display","block");
				
				sortImageFlag = "01";
				selectSortImageColor();
				return;
			}
			if(byLiShiSortUpDownTicketResultFlag == '0'){
				sortTicketResult(newTicketrusult,"0","lishi");
				if(byLeftOrPriceSortFlag == "byLeft"){
					showSearchResults(newTicketrusult,"left");
				}
				if(byLeftOrPriceSortFlag == "byPrice"){
					showSearchResults(newTicketrusult,"price");
				}
				//mor.ticket.util.contentIscrollTo(0, 0, 0);
				jq("#train_li_shi_sort_up").css("display","none");
				jq("#train_li_shi_sort_down").css("background-image","url(../images/down_blue.png)");
				jq("#train_li_shi_sort_down").css("display","block");
				sortImageFlag = "02";
				selectSortImageColor();
				return;
			}
		});
		//按发时排序
		jq("#byStartTimeSortTicketResult").click(function(){
			if(sortUpDownPreId != "back"){
				showImg("请稍候...");
			}
			sortImageColor = "2";
			jq("#train_start_time_text span").css("color","#3289c9");
			jq("#train_arrive_time_text span").css("color","#fff");
			jq("#train_li_shi_text span").css("color","#fff");
			var currId = jq(this).attr("id");
			if(currId == sortUpDownPreId){
				if(byStartTimeSortUpDownTicketResultFlag == "2"){
					byStartTimeSortUpDownTicketResultFlag = "3";
				}else{
					byStartTimeSortUpDownTicketResultFlag = "2";
				}
			}
			sortUpDownPreId = currId;
			if(byStartTimeSortUpDownTicketResultFlag == '3'){
				sortTicketResult(newTicketrusult,"3","start_time");
				if(byLeftOrPriceSortFlag == "byLeft"){
					showSearchResults(newTicketrusult,"left");
				}
				if(byLeftOrPriceSortFlag == "byPrice"){
					showSearchResults(newTicketrusult,"price");
				}
				//mor.ticket.util.contentIscrollTo(0, 0, 0);
				jq("#train_start_time_sort_down").css("display","none");
				jq("#train_start_time_sort_up").css("display","block");
				jq("#train_start_time_sort_up").css("background-image","url(../images/up_blue.png)");
				sortImageFlag = "01";
				selectSortImageColor();
				return;
			}
			if(byStartTimeSortUpDownTicketResultFlag == '2'){
				sortTicketResult(newTicketrusult,"2","start_time");
				if(byLeftOrPriceSortFlag == "byLeft"){
					showSearchResults(newTicketrusult,"left");
				}
				if(byLeftOrPriceSortFlag == "byPrice"){
					showSearchResults(newTicketrusult,"price");
				}
				//mor.ticket.util.contentIscrollTo(0, 0, 0);	
				jq("#train_start_time_sort_up").css("display","none");
				jq("#train_start_time_sort_down").css("background-image","url(../images/down_blue.png)");
				jq("#train_start_time_sort_down").css("display","block");
				sortImageFlag = "02";
				selectSortImageColor();
				return;
			}
		});
		//按到时排序
		jq("#byArriveTimeSortTicketResult").click(function(){
			if(sortUpDownPreId != "back"){
				showImg("请稍候...");
			}
			sortImageColor = "3";
			jq("#train_arrive_time_text span").css("color","#3289c9");
			jq("#train_start_time_text span").css("color","#fff");
			jq("#train_li_shi_text span").css("color","#fff");
			var currId = jq(this).attr("id");
			if(currId == sortUpDownPreId){
				if(byArriveTimeSortUpDownTicketResultFlag == "4"){
					byArriveTimeSortUpDownTicketResultFlag = "5";
				}else{
					byArriveTimeSortUpDownTicketResultFlag = "4";
				}
			}
			sortUpDownPreId = currId;
			if(byArriveTimeSortUpDownTicketResultFlag == '5'){
				sortTicketResult(newTicketrusult,"5","arrive_time");
				if(byLeftOrPriceSortFlag == "byLeft"){
					showSearchResults(newTicketrusult,"left");
				}
				if(byLeftOrPriceSortFlag == "byPrice"){
					showSearchResults(newTicketrusult,"price");
				}
				//mor.ticket.util.contentIscrollTo(0, 0, 0);
				jq("#train_arrive_time_sort_down").css("display","none");
				jq("#train_arrive_time_sort_up").css("background-image","url(../images/up_blue.png)");
				jq("#train_arrive_time_sort_up").css("display","block");
				sortImageFlag = "01";
				selectSortImageColor();
				return;
			}
			if(byArriveTimeSortUpDownTicketResultFlag == '4'){
				sortTicketResult(newTicketrusult,"4","arrive_time");
				if(byLeftOrPriceSortFlag == "byLeft"){
					showSearchResults(newTicketrusult,"left");
				}
				if(byLeftOrPriceSortFlag == "byPrice"){
					showSearchResults(newTicketrusult,"price");
				}
				//mor.ticket.util.contentIscrollTo(0, 0, 0);
				jq("#train_arrive_time_sort_up").css("display","none");
				jq("#train_arrive_time_sort_down").css("background-image","url(../images/down_blue.png)");
				jq("#train_arrive_time_sort_down").css("display","block");
				sortImageFlag = "02";
				selectSortImageColor();
				return;
			}
		});
	}
	
	function selectSortImageColor(){
		if(sortImageColor == "1"){
			if(byStartTimeSortUpDownTicketResultFlag == '3'){
				jq("#train_start_time_sort_up").css("background-image","url(../images/up.png)");
				jq("#train_start_time_sort_up").css("display","block");
				jq("#train_start_time_sort_down").css("display","none");
			}
			if(byStartTimeSortUpDownTicketResultFlag == '2'){
				jq("#train_start_time_sort_down").css("background-image","url(../images/down.png)");
				jq("#train_start_time_sort_down").css("display","block");
				jq("#train_start_time_sort_up").css("display","none");
			}
			if(byArriveTimeSortUpDownTicketResultFlag == '5'){
				jq("#train_arrive_time_sort_up").css("background-image","url(../images/up.png)");
				jq("#train_arrive_time_sort_up").css("display","block");
				jq("#train_arrive_time_sort_down").css("display","none");
			}
			if(byArriveTimeSortUpDownTicketResultFlag == '4'){
				jq("#train_arrive_time_sort_down").css("background-image","url(../images/down.png)");
				jq("#train_arrive_time_sort_down").css("display","block");
				jq("#train_arrive_time_sort_up").css("display","none");
			}
		}
		if(sortImageColor == "2"){
			if(byLiShiSortUpDownTicketResultFlag == '1'){
				jq("#train_li_shi_sort_up").css("background-image","url(../images/up.png)");
				jq("#train_li_shi_sort_up").css("display","block");
				jq("#train_li_shi_sort_down").css("display","none");
			}
			if(byLiShiSortUpDownTicketResultFlag == '0'){
				jq("#train_li_shi_sort_down").css("background-image","url(../images/down.png)");
				jq("#train_li_shi_sort_down").css("display","block");
				jq("#train_li_shi_sort_up").css("display","none");
			}
			if(byArriveTimeSortUpDownTicketResultFlag == '5'){
				jq("#train_arrive_time_sort_up").css("background-image","url(../images/up.png)");
				jq("#train_arrive_time_sort_up").css("display","block");
				jq("#train_arrive_time_sort_down").css("display","none");
			}
			if(byArriveTimeSortUpDownTicketResultFlag == '4'){
				jq("#train_arrive_time_sort_down").css("background-image","url(../images/down.png)");
				jq("#train_arrive_time_sort_down").css("display","block");
				jq("#train_arrive_time_sort_up").css("display","none");
			}
		}
		if(sortImageColor == "3"){
			if(byLiShiSortUpDownTicketResultFlag == '1'){
				jq("#train_li_shi_sort_up").css("background-image","url(../images/up.png)");
				jq("#train_li_shi_sort_up").css("display","block");
				jq("#train_li_shi_sort_down").css("display","none");
			}
			if(byLiShiSortUpDownTicketResultFlag == '0'){
				jq("#train_li_shi_sort_down").css("background-image","url(../images/down.png)");
				jq("#train_li_shi_sort_down").css("display","block");
				jq("#train_li_shi_sort_up").css("display","none");
			}
			if(byStartTimeSortUpDownTicketResultFlag == '3'){
				jq("#train_start_time_sort_up").css("background-image","url(../images/up.png)");
				jq("#train_start_time_sort_up").css("display","block");
				jq("#train_start_time_sort_down").css("display","none");
			}
			if(byStartTimeSortUpDownTicketResultFlag == '2'){
				jq("#train_start_time_sort_down").css("background-image","url(../images/down.png)");
				jq("#train_start_time_sort_down").css("display","block");
				jq("#train_start_time_sort_up").css("display","none");
			}
		}
	}
	function regiterTicketYpAndPriceListener(){
//		var model = mor.ticket.leftTicketQuery;
		jq("#priceAndLeftTicketResultBtn").click(function(){
			if(sortUpDownPreId == "byStartTimeSortTicketResult"){
				jq("#train_start_time_text span").css("color","#3289c9");
				jq("#train_arrive_time_text span").css("color","#fff");
				jq("#train_li_shi_text span").css("color","#fff");
			}else if(sortUpDownPreId == "byLiShiSortTicketResult"){
				jq("#train_li_shi_text span").css("color","#3289c9");
				jq("#train_arrive_time_text span").css("color","#fff");
				jq("#train_start_time_text span").css("color","#fff");
			}else if(sortUpDownPreId == "byArriveTimeSortTicketResult"){
				jq("#train_arrive_time_text span").css("color","#3289c9");
				jq("#train_start_time_text span").css("color","#fff");
				jq("#train_li_shi_text span").css("color","#fff");
			}
			if(isCheckedLeftOrPriceFlag == "0"){
				isCheckedLeftOrPriceFlag = "1";
				byLeftOrPriceSortFlag = "byPrice";
				jq("#priceTicketResultBtn").css("font-size","16px");
				jq("#priceTicketResultBtn").css("color","#3289c9");
				jq("#leftTicketResultBtn").css("font-size","15px");
				jq("#leftTicketResultBtn").css("color","white");
				showSearchResults(newTicketrusult,"price");
			}else{
				isCheckedLeftOrPriceFlag = "0";
				byLeftOrPriceSortFlag = "byLeft";
				jq("#priceTicketResultBtn").css("font-size","15px");
				jq("#priceTicketResultBtn").css("color","white");
				jq("#leftTicketResultBtn").css("font-size","16px");
				jq("#leftTicketResultBtn").css("color","#3289c9");
				showSearchResults(newTicketrusult,"left");
			}
		});
		
		jq("#searchSingleTicketResultList_List_content").bind("swipeleft",function(e){
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			tapholding = true;
			jq("#nextDateBtn").tap();
			setTimeout(function(){
				tapholding = false;
			}, 2000);
			return false;
		});
		jq("#searchSingleTicketResultList_List_content").bind("swiperight",function(e){
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			tapholding = true;
			jq("#prevDateBtn").tap();
			setTimeout(function(){
				tapholding = false;
			}, 2000);
			return false;
		});
		jq("#resultScreenBtn").bind("tap", function() {
			mor.ticket.util.changePage("searchResultScreen.html");
			return false;
		});
	}
	function registerChangeStation(){
		jq("#changetext").live("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			var model = mor.ticket.leftTicketQuery;
			var cache = mor.ticket.cache;
			var from_station_telecode = model.from_station_telecode;
			var to_station_telecode = model.to_station_telecode;
			changeStationing = true;
			if(mor.ticket.util.isAndroid()){
				WL.SimpleDialog.show("温馨提示", "确定交换发到站吗?", [
   				    {
   				         text : "确认",
   				         handler : function() {
   				        	 var m = model.to_station_telecode;
   								to_station_telecode = model.from_station_telecode;
   								from_station_telecode = m;
   								
   								jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode) + "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
   								if (model.purpose_codes === "0X") {
   									if (jq("#fromToStation").children("b")
   											.size() < 1) {
   									jq("#fromToStation").append("(学生)");
   									}
   								}
   								var query = mor.ticket.leftTicketQuery;
   								mor.ticket.leftTicketQuery.from_station_telecode = from_station_telecode;
   								mor.ticket.leftTicketQuery.to_station_telecode = to_station_telecode;
   								if (mor.ticket.viewControl.bookMode == "fc") {
   									var from_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode_back);
   									var to_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode_back);
   									var trainBackDate = mor.ticket.leftTicketQuery.train_date_back.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date_back.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date_back.substring(8,10)+"日";
   									saveImgName = trainBackDate+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
   									shareImageName = mor.ticket.leftTicketQuery.train_date_back+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
   								}else{
   									var from_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode);
   									var to_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode);
   									var trainDate = mor.ticket.leftTicketQuery.train_date.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date.substring(8,10)+"日";
   									saveImgName = trainDate+"_"+from_station_name+"-"+to_station_name+"_车次余票";
   									shareImageName = mor.ticket.leftTicketQuery.train_date+"_"+from_station_name+"-"+to_station_name+"_车次余票";
   								}
   								mor.ticket.searchResultScreen.chooseQueryFlag = false;
   								//mor.ticket.searchResultScreen.clear();
   								searchAndShowResult(query);	
   								setTimeout(function() {changeStationing = false;}, 1000);	
   				            }
   				    },
   				    {
   					    text : "取消",
   					    handler : function() {
   					   	 	setTimeout(function() {changeStationing = false;}, 1000);												
   					    }
   					}
   				]);
			}
			else{
				WL.SimpleDialog.show("温馨提示", "确定交换发到站吗?", [
					{
					    text : "取消",
					    handler : function() {
					    	setTimeout(function() {changeStationing = false;}, 1000);												
					    }
					},
				    {
				         text : "确认",
				         handler : function() {
				        	 var m = model.to_station_telecode;
								to_station_telecode = model.from_station_telecode;
								from_station_telecode = m;
								
								jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode) + "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
								if (model.purpose_codes === "0X") {
   									if (jq("#fromToStation").children("b")
   											.size() < 1) {
   									jq("#fromToStation").append("(学生)");
   									}
   								}
								var query = mor.ticket.leftTicketQuery;
								mor.ticket.leftTicketQuery.from_station_telecode = from_station_telecode;
								mor.ticket.leftTicketQuery.to_station_telecode = to_station_telecode;
								mor.ticket.searchResultScreen.chooseQueryFlag = false;
								//mor.ticket.searchResultScreen.clear();
								searchAndShowResult(query);	
								setTimeout(function() {changeStationing = false;}, 1000);	
				            }
				    }
				]);
			}
			return false;
		});
		
		
	}
	function searchAndShowResult(query) {
		var mode = mor.ticket.viewControl.bookMode;
		var commonParameters = null;
		if (mode === "fc") {
			commonParameters = {
				"train_date" : util.processDateCode(query.train_date_back),
				"purpose_codes" : query.purpose_codes,
				"from_station" : query.from_station_telecode_back,
				"to_station" : query.to_station_telecode_back,
				"station_train_code" : query.station_train_code_back,
				"start_time_begin" : '0000',
				"start_time_end" : '2400',
				"train_headers" : "QB#",
				"train_flag" : query.train_flag,
				"seat_type" : '',
				"seatBack_Type" : '',
				"ticket_num" : ""
			};
		} else {
			commonParameters = {
				"train_date" : util.processDateCode(query.train_date),
				"purpose_codes" : query.purpose_codes,
				"from_station" : query.from_station_telecode,
				"to_station" : query.to_station_telecode,
				"station_train_code" : query.station_train_code,
				"start_time_begin" : '0000',
				"start_time_end" : '2400',
				"train_headers" : "QB#",
				"train_flag" : query.train_flag,
				"seat_type" : '',
				"seatBack_Type" : '',
				"ticket_num" : ""
			};
		}

		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "queryLeftTicket"
		};
		var options = {
			onSuccess : requestSucceeded,
			onFailure : requestFailureHandler
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,
				options, true);
		storeUserRecentSearchStation(query.from_station_telecode,
				query.to_station_telecode);
		jq("#searchSingleTicketResultList").hide();
	}

	function requestFailureHandler(result){
		mor.ticket.leftTicketQuery.result = [];
		newTicketrusult = [];
		if (busy.isVisible()) {
			busy.hide();
		}
		if (result && result.status == 200) {
			if (result.invocationResult.responseID == "26") {// 调用otsmobile失败
				WL.SimpleDialog.show("温馨提示",
						"系统忙,请稍后再试。", [ {
							text : '确定',
							handler : function() {
							}
						} ]);
			} else {
				WL.SimpleDialog.show("温馨提示",
						"哎呀，网络好像有问题，请检查网络连接！", [ {
							text : '确定',
							handler : function() {
							}
						} ]);
			}
		} else {
			WL.Device
					.getNetworkInfo(function(
							networkInfo) {
						if (networkInfo.isNetworkConnected == "false") {
							WL.SimpleDialog
									.show(
											"温馨提示",
											"哎呀，您的网络有问题，请检查网络连接。",
											[ {
												text : '确定',
												handler : function() {
												}
											} ]);
						} else {
							WL.SimpleDialog
									.show(
											"温馨提示",
											"哎呀，您的网络好像有问题，请检查网络连接。",
											[ {
												text : '确定',
												handler : function() {
												}
											} ]);
						}
					});
		}
	}
	function requestSucceeded(result) {
		if (busy.isVisible()) {
			busy.hide();
		}
		//var mode = mor.ticket.viewControl.bookMode;
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			var query = mor.ticket.leftTicketQuery;
			window.ticketStorage.setItem("set_train_date_type",mor.ticket.leftTicketQuery.train_date);
			query.result = [];
			jq("#iscroll-pulldown").css("visibility","visible");
			// 针对高铁、动卧过滤普通车次
			/*if(mode == "gc" && mor.ticket.dw_ticket.dw_flag == true){
				for(var i=0;i<invocationResult.ticketResult.length;i++){
					var temp_ticket = invocationResult.ticketResult[i];
					var temp_train_header = temp_ticket.station_train_code.substr(0,1);
					if(temp_train_header != "G" && temp_train_header != "D"){
						invocationResult.ticketResult.splice(i,1);
						i--;
					}
				}
			}*/
			mor.ticket.leftTicketQuery.result = filterSeatTypeForYp(invocationResult.ticketResult);
			mor.ticket.searchResultScreen.filterResult();
			newTicketrusult = mor.ticket.searchResultScreen.result;
			var tipMessage = "";
			if(!mor.ticket.searchResultScreen.isHasFilterCondition()){
				tipMessage = "<span>共查询到"+mor.ticket.leftTicketQuery.result.length+"趟列车</span>";
			}else{
				tipMessage = "<span>共查询到"+mor.ticket.leftTicketQuery.result.length+"趟列车,筛选后有"+newTicketrusult.length+"趟列车</span>";
			}
			jq("#trainCount").html(tipMessage);
			jq("#trainCount").show();
			jq("#trainCount").fadeOut(3000);
			if(byLeftOrPriceSortFlag == "byPrice"){
				if(sortImageColor !="0"){
					prevDateBtnAndNextDateBtnAndSelectDateChangeTicketResults(newTicketrusult);
				}
				showSearchResults(newTicketrusult,"price");
			}
			if(byLeftOrPriceSortFlag == "byLeft"){
				if(sortImageColor !="0"){
					prevDateBtnAndNextDateBtnAndSelectDateChangeTicketResults(newTicketrusult);
				}
				showSearchResults(newTicketrusult,"left");
			}
			
		} else {
			jq("#iscroll-pulldown").css("visibility","hidden");
			//jq.mobile.changePage(vPathViewCallBack() + "MobileTicket.html");
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			return;
		}
	};
	function storeUserRecentSearchStation(from_station_telecode,
			to_station_telecode) {
		var recentStationCodeList = window.ticketStorage
				.getItem("recentStation") == null ? [] : JSON
				.parse(ticketStorage.getItem("recentStation"));
		for ( var i = 0, l = recentStationCodeList.length; i < l; i++) {
			if (recentStationCodeList[i] == from_station_telecode) {
				recentStationCodeList.splice(i, 1);
				i--;
				continue;
			}
			if (recentStationCodeList[i] == to_station_telecode) {
				recentStationCodeList.splice(i, 1);
				i--;
				continue;
			}
		}
		recentStationCodeList.push(to_station_telecode);
		recentStationCodeList.push(from_station_telecode);
		if (recentStationCodeList.length > 10)
			recentStationCodeList.shift();
		window.ticketStorage.setItem("recentStation", JSON
				.stringify(recentStationCodeList));
		// 保存常用线路
		var mode = mor.ticket.viewControl.bookMode;
		if(mode === "fc"){
			storeUserRecentSearchStationLine(to_station_telecode,from_station_telecode);
		}else{
			storeUserRecentSearchStationLine(from_station_telecode,to_station_telecode);
		}
	}
	// 常用线路保存至本地缓存
	function storeUserRecentSearchStationLine(from_station_telecode,to_station_telecode){
		var recentStationLineList = window.ticketStorage
				.getItem("recentStationLine") == null ? [] : JSON
				.parse(ticketStorage.getItem("recentStationLine"));
		for ( var i = 0, n = recentStationLineList.length; i < n; i++) {
			var stationLine = recentStationLineList[i].split("-");
			if ((stationLine[0] === from_station_telecode) 
					&& (stationLine[1] === to_station_telecode)) {
				recentStationLineList.splice(i, 1);
				i--;
				n--;
				continue;
			}
		}
		recentStationLineList.push(from_station_telecode + "-" + to_station_telecode);
		if (recentStationLineList.length > 5){
			recentStationLineList.shift();
		}
		window.ticketStorage.setItem("recentStationLine", JSON
				.stringify(recentStationLineList));
	}
	
	function prevDateBtnAndNextDateBtnAndSelectDateChangeTicketResults(ticketResult){
		if(sortImageColor == "1"){
			if(byLiShiSortUpDownTicketResultFlag == '1'){
				sortTicketResult(ticketResult,"1","lishi");
			}else{
				sortTicketResult(ticketResult,"0","lishi");
			}
		}
		if(sortImageColor == "2"){
			if(byStartTimeSortUpDownTicketResultFlag == '3'){
				sortTicketResult(ticketResult,"3","start_time");
			}else{
				sortTicketResult(ticketResult,"2","start_time");
			}
		}
		if(sortImageColor == "3"){
			if(byArriveTimeSortUpDownTicketResultFlag == '5'){
				sortTicketResult(ticketResult,"5","arrive_time");
			}else{
				sortTicketResult(ticketResult,"4","arrive_time");
			}
		}
	}
	
	function filterSeatTypeForYp(results){
		var seatType = "";
		var mode = mor.ticket.viewControl.bookMode;
		if (mode === "fc") {
			seatType = mor.ticket.leftTicketQuery.seatBack_Type;
		} else {
			seatType = mor.ticket.leftTicketQuery.seat_Type;
		}
		for ( var i = 0; i < results.length; i++) {
			var number = 0;
			if (results[i].yp_info && results[i].yp_info.length > 0) {
				results[i].yplist = processYpInfo(results[i].yp_info);
				
			} else {
				results[i].yplist = "";
			}
			//席别优惠判断
			var isTicketNum = "false";//车次余票是否为0,false不为0,true为0
			for(var k=0;k<results[i].yplist.length;k++){
				 if(results[i].yplist[k].num>0){
					 isTicketNum = "true";	
				 }
				 results[i].yplist[k].discount = "0";
				 if (results[i].yp_ex&&results[i].yp_ex.length > 0) {
					 for(var di = 0;di<results[i].yp_ex.length;di=di+2){
						 if(results[i].yp_ex.substr(di+1,1) === '1'){
							 results[i].seatType = results[i].yp_ex.substr(di,1);
							 if(results[i].yp_ex.substr(di,1) == results[i].yplist[k].type_id){
								 results[i].yplist[k].discount = "1";
							 }
						 }
					 }
				 }else{
					 results[i].yplist[k].discount = '0';
				 }
			}
			results[i].isTicketNum = isTicketNum;
			// 判断内部是否有优惠票
			 if (results[i].yplist&&results[i].yplist.length > 0) {
				 results[i].discount = '0';
				 for(var di = 0;di<results[i].yplist.length;di++){
					 if(results[i].yplist[di].discount === '1'){
						 results[i].discount = '1';
						 break;
					 }
				 }
			 }else{
				 results[i].discount = '0';
			 }
			results[i].lishiHour = results[i].lishi.substring(0, 2);
			results[i].lishiMinute = results[i].lishi.substring(3, 5);
			/*
			 * 选择席位后从结果集里挑选合适的席位
			 */
//			if (seatType != "") {
//				if (results[i].yplist.length === 0) {
//					results.splice(i, 1);
//					--i;
//				} else {
//					for ( var j = 0; j < results[i].yplist.length; j++) {
//						// 席位条件符合后计数
//						if (results[i].yplist[j].type_id === seatType) {
//							number++;
//							// 如果合适席位的席位数位0删除
//							if (results[i].yplist[j].num > 0) {
//								break;
//							}else{
//								results.splice(i, 1);
//								--i;
//								break;
//							}
//						}
//					}
//					// 计数后如果席位不包含选择席位，删除
//					if (number === 0) {
//						results.splice(i, 1);
//						--i;
//					}
//				}
//
//			}
		}
		return results;
	}
	function showSearchResults(results,leftOrPrice) {
		results = filterSeatTypeForYp(results);
		var model = mor.ticket.leftTicketQuery;
		var cache = mor.ticket.cache;
		var mode = mor.ticket.viewControl.bookMode;
		if (mode === "fc") {
			train_date = model.train_date_back;
			from_station_telecode = model.from_station_telecode_back;
			to_station_telecode = model.to_station_telecode_back;
		}
//		results = jq.grep(results,
//						function(item, index) {
//							return cache.getStationNameByCode(item.from_station_telecode) === undefined
//									|| cache.getStationNameByCode(item.to_station_telecode) === undefined;
//						}, true);
		if(leftOrPrice == "left" || leftOrPrice == "byLeft"){
			jq("#searchSingleTicketResultList").html(generateListContent(results)).listview("refresh");
		}
		if(leftOrPrice == "price" || leftOrPrice == "byPrice"){
			jq("#searchSingleTicketResultList").html(generatePriceListContent(results)).listview("refresh");
		}
		jq("#searchSingleTicketResultList").show();
		if(jq("#waterfall") != undefined){
			setTimeout(hideImg,300);
		}
		initDateBtn();
	}
	var newIndex = "";
	function registerTicketListItemClickHandler() {
		jq("#searchSingleTicketResultList")
				.on(
						"tap",
						"a[name='ticket']",
						function(e) {
							e.stopImmediatePropagation();
							if(tapholding)
							return false;																						
//							var query = mor.ticket.leftTicketQuery;
							mor.ticket.currentTicket = newTicketrusult[jq(this).parent().parent().parent().index()];
							var ticket = mor.ticket.currentTicket;
							ticketListItemClick(ticket);
							return false;
						});		

			
	};
	//guruqin更换发到站开始	
	function registerChangestationFromorTo(){			
		//变更到站时更改到站	
				jq("#searchSingleTicketResultList .stopStationTable tr").live("tap", function (){
					jq(".noclicktr").prevAll(".pass_arrive_station").attr("class","Passed_station");
					jq(".noclicktr").nextAll(".pass_arrive_station").attr("class","Not_arrive_station");						
					var model = mor.ticket.leftTicketQuery;
					var Tdclick=jq(this).children().eq(1).html();					
					if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站" && mor.ticket.viewControl.bookMode === "gc"){																			
					    if(jq(this).hasClass("station_on")||jq(this).hasClass("Not_arrive_station")){
					    	if(mor.ticket.util.isAndroid()){
								WL.SimpleDialog.show("温馨提示", "您是否要将"+(Tdclick)+"更换为到达站？", 
										[{
									    	text : "确认",
									    	handler : function() {
				                                      jq("#fromStationInput").val(Tdclick);
									    			  jq.each(mor.ticket.cache.stations,function(i,n){
									    			    	if(n.value == Tdclick){	
									    			    		if(n.id == model.from_station_telecode){
									    			    			mor.ticket.util.alertMessage("出发站不能和到达站相同");
									    			    			return false;			 		 	  	    	
									    			    		}else
									    			    		  {
									    			    		   model.to_station_telecode = n.id;	
									    			    		  }			 						 		 	  	    
									    			    		}
									    			   });
									    			   var cache = mor.ticket.cache;
									    			   var from_station_telecode = model.from_station_telecode;
									    			   var to_station_telecode = model.to_station_telecode;
									    			   jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode)+ "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));				    			
									    			    onPullDown();
									    			    
				                                                }
									    			   },
									    {
									       text : "取消",
									       handler : function() {
									    			return false;
									    			             }
									   } 
						             ]);                   
					         }else{
					    		WL.SimpleDialog.show("温馨提示", "您是否要将"+(Tdclick)+"更换为到达站？",
										[{
										       text : "取消",
										       handler : function() {
										    			return false;
										    			             }
										},
										 {
							    	text : "确认",
							    	handler : function() {
		                                      jq("#fromStationInput").val(Tdclick);
							    			  jq.each(mor.ticket.cache.stations,function(i,n){
							    			    	if(n.value == Tdclick){	
							    			    		if(n.id == model.from_station_telecode){
							    			    			mor.ticket.util.alertMessage("出发站不能和到达站相同");
							    			    			return false;			 		 	  	    	
							    			    		}else
							    			    		  {
							    			    		   model.to_station_telecode = n.id;	
							    			    		  }			 						 		 	  	    
							    			    		}
							    			   });
							    			   var cache = mor.ticket.cache;
							    			   var from_station_telecode = model.from_station_telecode;
							    			   var to_station_telecode = model.to_station_telecode;
							    			   jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode)+ "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));				    			
							    			    onPullDown();
							    			    
		                                                }
							    			   },
								             ]);  
					    	}			    	
					    	}
					}
//车票为单程票时更换发到站					
				    else if(mor.ticket.viewControl.bookMode === "dc"){	
								if(jq(this).hasClass("Passed_station")){
									if(mor.ticket.util.isAndroid()){
										WL.SimpleDialog.show("温馨提示", "您是否要将"+(Tdclick)+"更换为出发站？", 
											  [{
													  text : "确认",
													    	handler : function() {
													    			    		jq("#fromStationInput").val(Tdclick);
													    			    		jq.each(mor.ticket.cache.stations,function(i,n){
													    			    			if(n.value == Tdclick){	
													    			    			 	if(n.id == model.to_station_telecode){
													    			    			 		mor.ticket.util.alertMessage("出发站不能和到达站相同");
													    			    			 		return false;			 		 	  	    	
													    			    			 		}
													    			    			 	 else{
													    			    			 		model.from_station_telecode = n.id;	
													    			    			 		 }			 						 		 	  	    
													    			    			 					  }
													    			    		});
													    			    		var cache = mor.ticket.cache;
													    			    		var from_station_telecode = model.from_station_telecode;
													    			    		var to_station_telecode = model.to_station_telecode;
													    			    		jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode)+ "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
													    			    		onPullDown();
													    			                  }
													    			                 },
													    	{
													    		text : "取消",
													    		handler : function() {
													    		return false;
													    		                     }
													    	} 
													  ]);
			                       }else{
									WL.SimpleDialog.show("温馨提示", "您是否要将"+(Tdclick)+"更换为出发站？", 
											  [	{
													    		text : "取消",
													    		handler : function() {
													    		return false;
													    		                     }
													    	}, 
											  {
													  text : "确认",
													    	handler : function() {
													    			    		jq("#fromStationInput").val(Tdclick);
													    			    		jq.each(mor.ticket.cache.stations,function(i,n){
													    			    			if(n.value == Tdclick){	
													    			    			 	if(n.id == model.to_station_telecode){
													    			    			 		mor.ticket.util.alertMessage("出发站不能和到达站相同");
													    			    			 		return false;			 		 	  	    	
													    			    			 		}
													    			    			 	 else{
													    			    			 		model.from_station_telecode = n.id;	
													    			    			 		 }			 						 		 	  	    
													    			    			 					  }
													    			    		});
													    			    		var cache = mor.ticket.cache;
													    			    		var from_station_telecode = model.from_station_telecode;
													    			    		var to_station_telecode = model.to_station_telecode;
													    			    		jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode)+ "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
													    			    		onPullDown();
													    			                  }
													    			                 },
													  ]);
			                         }										  
											}
								if(jq(this).hasClass("Not_arrive_station")){
									if(mor.ticket.util.isAndroid()){
												WL.SimpleDialog.show("温馨提示", "您是否要将"+(Tdclick)+"更换为到达站？", 
														[{
													    	text : "确认",
													    	handler : function() {
					                                                  jq("#fromStationInput").val(Tdclick);
													    			  jq.each(mor.ticket.cache.stations,function(i,n){
													    			    	if(n.value == Tdclick){	
													    			    		if(n.id == model.from_station_telecode){
													    			    			mor.ticket.util.alertMessage("出发站不能和到达站相同");
													    			    			return false;			 		 	  	    	
													    			    		}else
													    			    		  {
													    			    		   model.to_station_telecode = n.id;	
													    			    		  }			 						 		 	  	    
													    			    		}
													    			   });
													    			   var cache = mor.ticket.cache;
													    			   var from_station_telecode = model.from_station_telecode;
													    			   var to_station_telecode = model.to_station_telecode;
													    			   jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode)+ "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
													    			    onPullDown();
					                                                            }
													    			   },
													    {
													       text : "取消",
													       handler : function() {
													    			return false;
													    			             }
													   } 
										             ]);
													 }else{
												WL.SimpleDialog.show("温馨提示", "您是否要将"+(Tdclick)+"更换为到达站？", 
														[{
													       text : "取消",
													       handler : function() {
													    			return false;
													    			             }
													   }, 
														{
													    	text : "确认",
													    	handler : function() {
					                                                  jq("#fromStationInput").val(Tdclick);
													    			  jq.each(mor.ticket.cache.stations,function(i,n){
													    			    	if(n.value == Tdclick){	
													    			    		if(n.id == model.from_station_telecode){
													    			    			mor.ticket.util.alertMessage("出发站不能和到达站相同");
													    			    			return false;			 		 	  	    	
													    			    		}else
													    			    		  {
													    			    		   model.to_station_telecode = n.id;	
													    			    		  }			 						 		 	  	    
													    			    		}
													    			   });
													    			   var cache = mor.ticket.cache;
													    			   var from_station_telecode = model.from_station_telecode;
													    			   var to_station_telecode = model.to_station_telecode;
													    			   jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode)+ "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
													    			    onPullDown();
					                                                            }
													    			   },	
										             ]);										 
													 }
											}						
								if(jq(this).hasClass("station_on")){
												WL.SimpleDialog.show("温馨提示", "您是否将"+(Tdclick)+"更换为出发站/到达站？", 
														[{
												    		text : "换到站",
												    		handler : function() {	    			    			 					    			
												    			    jq("#toStationInput").val(Tdclick);
												    			    jq.each(mor.ticket.cache.stations,function(i,n){
												    			    		if(n.value == Tdclick){	
												    			    			if(n.id == model.from_station_telecode){
												    			    			 	mor.ticket.util.alertMessage("出发站不能和到达站相同");
												    			    			 	return false;			 		 	  	    	
												    			    			 }else
												    			    			 {
												    			    				model.to_station_telecode = n.id;	
												    			    			 }			 						 		 	  	    
												    			    			}
												    			   });
												    			    		var cache = mor.ticket.cache;
												    			    		var from_station_telecode = model.from_station_telecode;
												    			    		var to_station_telecode = model.to_station_telecode;
												    			    		jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode)+ "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
												    			    			onPullDown();
					                                                             }
												    			                },
												    	{
												    		text : "换发站",
												    		handler : function() {
					                                                  jq("#fromStationInput").val(Tdclick);
												    			      jq.each(mor.ticket.cache.stations,function(i,n){
												    			    		if(n.value == Tdclick){	
												    			    			if(n.id == model.to_station_telecode){
												    			    			 	mor.ticket.util.alertMessage("出发站不能和到达站相同");
												    			    			 	return false;			 		 	  	    	
												    			    		} else
												    			    		{
												    			    		       model.from_station_telecode = n.id;	
												    			    		}			 						 		 	  	    
												    			    	 }
												    			      });
												    			    		var cache = mor.ticket.cache;
												    			    		var from_station_telecode = model.from_station_telecode;
												    			    		var to_station_telecode = model.to_station_telecode;
												    			    		jq("#fromToStation").html(cache.getStationNameByCode(from_station_telecode)+ "<a id='changetext'>\<\></a>" + cache.getStationNameByCode(to_station_telecode));
												    			    			onPullDown();
												    			    			
												    			                       }
												    			                    },
												    	{
												    		text : "取消",
												    		handler : function() {
												    			    return false;
												    			                 }
												    	                   } 								    			                        					
										        ]);	
											}			
										
				}else{
					  jq("#searchSingleTicketResultList .stopStationTable tr").die("tap");
					 }												
				    });
	}
	//guruqin更换发到站结束
	
	
		
	function ticketListItemClick(ticket){
		if (mor.ticket.viewControl.bookMode === "fc") {
			ticket.train_date = mor.ticket.leftTicketQuery.train_date_back;
			ticket.pre_train_date = mor.ticket.leftTicketQuery.train_date;
		} else {
			ticket.train_date = mor.ticket.leftTicketQuery.train_date;
		}
		/*if(mor.ticket.viewControl.bookMode === "gc" && mor.ticket.dw_ticket.dw_flag == true){
			var trainHeader = ticket.station_train_code.substr(0,1);
			if(trainHeader !== "D" && trainHeader != "G"){
				var str = "";
				if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
					str = "变更到站";
				}if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "改签"){
					str = "改签";
				}
				mor.ticket.util.alertMessage("当前"+str+"的列车类型，只能选择高铁或动车。");
				return false;
			}
		}*/
		if (ticket.flag == "0") {
			mor.ticket.util.alertMessage(ticket.message);
			return false;
		}
		var hasticket = ticket.yplist.length;
		for ( var i = 0; i < ticket.yplist.length; i++) {
			if (!ticket.yplist[i].num) {
				hasticket--;
			}
		}
		if (hasticket) {
			if (ticket.yplist[0].price == "NaN"
					|| ticket.yplist[0].price == null
					|| ticket.yplist[0].price === "undefined") {
				var util = mor.ticket.util;
				var parameters = util
						.prepareRequestCommonParameters({
							"train_date" : util
									.processDateCode(ticket.train_date),
							"from_station" : ticket.from_station_telecode,
							"to_station" : ticket.to_station_telecode,
							"station_train_code" : ticket.station_train_code,
							"yp_info" : ticket.yp_info
						});
				var invocationData = {
					adapter : mor.ticket.viewControl.adapterUsed,
					procedure : "queryLeftTicketDetail",
				};
				var options = {
					onSuccess : requestYPInfoSucceeded,
					onFailure : util
							.creatCommonRequestFailureHandler()
				};
				mor.ticket.util
						.invokeWLProcedure(parameters,
								invocationData, options);
			} else {
				var trainDate = ticket.train_date.substr(0,4)+ticket.train_date.substr(5,2)+ticket.train_date.substr(8,2)+
				ticket.start_time.substr(0,2) + ticket.start_time.substr(3,2)+"00";
				var endTime= mor.ticket.util.setMyDate3(trainDate);
				var nowTime = mor.ticket.util.getNewDate();
				var diffTime = endTime.getTime() - nowTime.getTime();
				if (diffTime <= 2*60*60*1000){
					  WL.SimpleDialog.show(
								"温馨提示", 
								"您选择的车票距开车时间很近了，请确保有足够的时间抵达车站，并办理换取纸质车票、安全检查、实名制验证及检票等手续，以免耽误您的旅行。", 
								[ {text : '确定', handler: function() {
								}}]
							);
				}
				jq.mobile.changePage(vPathCallBack()
						+ "passengers.html");
			}
		} else {
			mor.ticket.util.alertMessage("很抱歉，当日该车次票已售完");
		}
	}
	function registerMoreStopStationListener(){
		jq("#searchSingleTicketResultList").on("tap",
				"div[name='stopStation']",
				function(e){
			e.stopImmediatePropagation();
			var that = jq(this);
//			var query = mor.ticket.leftTicketQuery;
			newIndex = that.parent().parent().index();
			if( !jq("#stop_"+newIndex).is(":visible") && jq("#stop_"+newIndex).html()!=""){
				jq("#stop_"+newIndex).css("display","block");
				that.next().children("a").removeClass("moreDownStation");
				that.next().children("a").addClass("moreUpStation");
				jq("#searchSingleTicketResultList").listview("refresh");
				return false;								
			}
			if( jq("#stop_"+newIndex).is(":visible")){
				jq("#stop_"+newIndex).hide();
				that.next().children("a").removeClass("moreUpStation");
				that.next().children("a").addClass("moreDownStation");
				jq("#searchSingleTicketResultList").listview("refresh");		
				return false;
			}
			that.next().children("a").removeClass("moreDownStation");
			that.next().children("a").addClass("moreUpStation");
			mor.ticket.currentTicket = newTicketrusult[newIndex];
			var ticket = mor.ticket.currentTicket;
			var commonParameters = {
					'depart_date': ticket.start_train_date,
					'from_station_telecode': ticket.from_station_telecode,
					'to_station_telecode':ticket.to_station_telecode,
					'train_no':ticket.train_no
					
				};
				
				var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "stopStationQuery"
				};
				
				var options =  {
						onSuccess: requestStopStationQuerySucceeded,
						onFailure: util.creatCommonRequestFailureHandler()
				};
				mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
				return false;
		});
		
	}	
	 function registerrefreshsearchorderRefreshButtonListener() {
	        jq("#refreshsearchorder").bind("tap", function () {
	        	searchAndShowResult(mor.ticket.leftTicketQuery);
	            return false;
	        });
	    }
	function requestYPInfoSucceeded(result) {
		if (busy.isVisible()) {
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			var ticket = mor.ticket.currentTicket;
			ticket.yp_info = invocationResult.yp_info;
			ticket.yplist = processYpInfo(ticket.yp_info);
			jq.mobile.changePage(vPathCallBack() + "passengers.html");
		} else {

			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	;
	function requestStopStationQuerySucceeded(result) {
		
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			jq("#stop_"+newIndex).html(generateStopStationListContent(invocationResult.list));
			jq("#stop_"+newIndex).show();		
			jq("#searchSingleTicketResultList").listview("refresh");
		}else {	
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	function registerBackButtonHandler() {
		jq("#searchSingleBackBtn").off().on("tap", function(e) {
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			mor.ticket.util.changePage(vPathViewCallBack() + "MobileTicket.html");
			return false;
		});
	}
	function processYpInfo(ypinfo) {
		var cache = mor.ticket.cache;
		var arrayLength = ypinfo.length / 10;
		var obj = new Array();
		var temp_seat_rate = 50;// 对于没有定义的座位，rate值从50开始递增
		for ( var i = 0, m = 6, n = 10, x = 0, y = 1; i < arrayLength; i++, m = m + 10, n = n + 10, x = x + 10, y = y + 10) {
			var seat_type_id = ypinfo.substring(x, y);
			var seat_type = null;
			var seat_type_rate = null;
			var seat_num = 0;
			if (parseInt(ypinfo.substring(m, m + 1), 10) >= 3) {
				seat_type = "无座";
				seat_type_rate = 100;
				seat_num = parseInt(ypinfo.substring(m, n), 10)-3000;
			} else {
				seat_type = cache.getSeatTypeByCode(seat_type_id);
				seat_type_rate = cache.getSeatTypeRateByCode(seat_type_id);
				if (seat_type_rate === null || seat_type_rate === "undefined") {
					seat_type_rate = temp_seat_rate;
					temp_seat_rate = temp_seat_rate + 1;
				}
				seat_num = parseInt(ypinfo.substring(m, n), 10);
			}
			
			var newPrice = '¥'+(parseFloat(ypinfo.substring(y, m), 10) / 10).toFixed(1);
			if(newPrice == '¥0.0'){
				newPrice = "--";
			}
			obj[i] = {
				type_id : seat_type_id,
				type : seat_type,
				type_rate : seat_type_rate,// 保存座位rate用于排序
				num : seat_num,
				uclass : getLiClass(i),
				price : newPrice
			};
		}
		var sortedObjs = sortYpInfo(obj);

		for ( var i = 0; i < sortedObjs.length; i++) {
			sortedObjs[i].uclass = getLiClass(i);
		}
		sortedObjs = repalceYpInfoType(sortedObjs);
		return sortedObjs;
	}
	// 把两个字数以上的座位类型改为两位
	function repalceYpInfoType(objs) {
		for ( var i in objs) {
			var type = objs[i].type;
			switch (type) {
			case "商务座":
				objs[i].type = "商务";
				break;
			case "特等座":
				objs[i].type = "特等";
				break;
			case "一等座":
				objs[i].type = "一等";
				break;
			case "二等座":
				objs[i].type = "二等";
				break;
			case "高级软卧":
				objs[i].type = "高软";
				break;
			case "观光座":
				objs[i].type = "观光";
				break;
			}
		}
		return objs;
	}

	// 按照座位的type_rate进行排序，规则如下
	// 商务座、特等座、一等座、二等座、高级软卧、软卧、硬卧、软座、硬座、（...）、无座 的顺序显示；
	// 如果出现其余席别，显示在这些席别的后面，顺序暂不限，其中 无座 总放在最后面。
	function sortYpInfo(ypinfoArray) {
		
		var by = function(name) {
			return function(o, p) {
				var a, b;
				if (typeof o === "object" && typeof p === "object" && o && p) {
					a = o[name];
					b = p[name];
					if (a === b) {
						return 0;
					}
					if (typeof a === typeof b) {
						return a < b ? -1 : 1;
					}
					return typeof a < typeof b ? -1 : 1;
				} else {
					throw ("error");
				}
			};
		};

		ypinfoArray.sort(by("type_rate"));
		return ypinfoArray;

	}

	function initDateBtn() {
		var date = mor.ticket.util
				.setMyDate(mor.ticket.leftTicketQuery.train_date);
		var nowDate = mor.ticket.util.getNewDate();
		if (mor.ticket.viewControl.bookMode == "fc") {
			var date_back = mor.ticket.util
					.setMyDate(mor.ticket.leftTicketQuery.train_date_back);
			if (date_back <= date) {
				jq("#prevDateBtn").addClass("ui-disabled");
				jq("#nextDateBtn").removeClass("ui-disabled");
			} else if (date_back - nowDate > (initReservedPeriod() - 1) * 86400000) {
				jq("#nextDateBtn").addClass("ui-disabled");
				jq("#prevDateBtn").removeClass("ui-disabled");
			} else {
				jq("#prevDateBtn").removeClass("ui-disabled");
				jq("#nextDateBtn").removeClass("ui-disabled");
			}
		}else{
			if (date < nowDate) {
				jq("#prevDateBtn").addClass("ui-disabled");
				jq("#nextDateBtn").removeClass("ui-disabled");
				jq("#trainDateInputId").removeClass("ui-disabled");
			}else if (date - nowDate > (initReservedPeriod() - 1) * 86400000) {
				jq("#nextDateBtn").addClass("ui-disabled");
				jq("#prevDateBtn").removeClass("ui-disabled");
				jq("#trainDateInputId").removeClass("ui-disabled");
				swipeLeftFlag  = true;
			}else {
				jq("#prevDateBtn").removeClass("ui-disabled");
				jq("#nextDateBtn").removeClass("ui-disabled");
				jq("#trainDateInputId").removeClass("ui-disabled");
			}
		}
	}
	
	function registerPrevDateBtnClickHandler() {
		jq("#prevDateBtn")
				.off()
				.on(
						"tap",
						function() {
							var date = mor.ticket.util
									.setMyDate(mor.ticket.leftTicketQuery.train_date);
							var nowDate = mor.ticket.util.getNewDate();
							if (mor.ticket.viewControl.bookMode == "fc") {
								date = mor.ticket.util
										.setMyDate(mor.ticket.leftTicketQuery.train_date_back);
								var orderManager=mor.ticket.orderManager;
								var preTicket = orderManager.getPreTicketDetail();
								// 返程开始日期
								nowDate = mor.ticket.util.setMyDate2(preTicket.train_date.replace(/-/g, ''));
							}
//							var nowDate = mor.ticket.util.getNewDate();
							if (date > nowDate) {
								var preDate = new Date(date.getTime() - 24 * 60
										* 60 * 1000);
								if (mor.ticket.viewControl.bookMode == "fc") {
									mor.ticket.leftTicketQuery.train_date_back = preDate
											.format("yyyy-MM-dd");
									//new add by zzc
									jq("#chooseDateBtn").val(mor.ticket.leftTicketQuery.train_date_back);
								} else {
									mor.ticket.leftTicketQuery.train_date = preDate
											.format("yyyy-MM-dd");
									//new add by zzc
									jq("#chooseDateBtn").val(mor.ticket.leftTicketQuery.train_date);
								}
								if (mor.ticket.viewControl.bookMode == "fc") {
									var from_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode_back);
									var to_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode_back);
									var trainBackDate = mor.ticket.leftTicketQuery.train_date_back.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date_back.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date_back.substring(8,10)+"日";
									saveImgName = trainBackDate+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
									shareImageName = mor.ticket.leftTicketQuery.train_date_back+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
								}else{
									var from_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode);
									var to_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode);
									var trainDate = mor.ticket.leftTicketQuery.train_date.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date.substring(8,10)+"日";
									saveImgName = trainDate+"_"+from_station_name+"-"+to_station_name+"_车次余票";
									shareImageName = mor.ticket.leftTicketQuery.train_date+"_"+from_station_name+"-"+to_station_name+"_车次余票";
								}
								searchAndShowResult(mor.ticket.leftTicketQuery);
							}
							return false;
						});
	}

	function registerNextDateBtnClickHandler() {
		jq("#nextDateBtn")
				.off()
				.on(
						"tap",
						function() {
							var date = mor.ticket.util
									.setMyDate(mor.ticket.leftTicketQuery.train_date);
							if (mor.ticket.viewControl.bookMode == "fc") {
								date = mor.ticket.util
										.setMyDate(mor.ticket.leftTicketQuery.train_date_back);
							}
							var nowDate = mor.ticket.util.getNewDate();
							if (date - nowDate < (initReservedPeriod() - 1) * 86400000) {
								var nextDate = new Date(date.getTime() + 24
										* 60 * 60 * 1000);

								if (mor.ticket.viewControl.bookMode == "fc") {
									mor.ticket.leftTicketQuery.train_date_back = nextDate
											.format("yyyy-MM-dd");
									//new add by zzc
									jq("#chooseDateBtn").val(mor.ticket.leftTicketQuery.train_date_back);
								} else {
									mor.ticket.leftTicketQuery.train_date = nextDate
											.format("yyyy-MM-dd");
									//new add by zzc
								jq("#chooseDateBtn").val(mor.ticket.leftTicketQuery.train_date);
								}
								if (mor.ticket.viewControl.bookMode == "fc") {
									var from_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode_back);
									var to_station_back_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode_back);
									var trainBackDate = mor.ticket.leftTicketQuery.train_date_back.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date_back.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date_back.substring(8,10)+"日";
									saveImgName = trainBackDate+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
									shareImageName = mor.ticket.leftTicketQuery.train_date_back+"_"+from_station_back_name+"-"+to_station_back_name+"_车次余票";
								}else{
									var from_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.from_station_telecode);
									var to_station_name = mor.ticket.cache.getStationNameByCode(mor.ticket.leftTicketQuery.to_station_telecode);
									var trainDate = mor.ticket.leftTicketQuery.train_date.substring(0,4)+"年"+mor.ticket.leftTicketQuery.train_date.substring(5,7)+"月"+mor.ticket.leftTicketQuery.train_date.substring(8,10)+"日";
									saveImgName = trainDate+"_"+from_station_name+"-"+to_station_name+"_车次余票";
									shareImageName = mor.ticket.leftTicketQuery.train_date+"_"+from_station_name+"-"+to_station_name+"_车次余票";
								}
								searchAndShowResult(mor.ticket.leftTicketQuery);								

							}
							return false;
						});
	}

	function getLiClass(index) {
		switch (index % 4) {
		case 0:
			return "ui-block-a";
		case 1:
			return "ui-block-b";
		case 2:
			return "ui-block-c";
		case 3:
			return "ui-block-d";
		default:
			return "ui-block-e";

		}
	}
	function initReservedPeriod() {
		var model = mor.ticket.leftTicketQuery;
		var reservePeriod;
		var reservedPeriodType = '';
		if (model.purpose_codes == '0X') {
			reservedPeriodType = '3';
		} else if (model.purpose_codes == '1F') {
			reservedPeriodType = 'n';
		} else {
			reservedPeriodType = '1';
		}
		reservePeriod = parseInt(window.ticketStorage
				.getItem("reservePeriod_" + reservedPeriodType), 10);
		return reservePeriod;
	}
	/*对TicketResult进行排序
	 * sortFlag:1,3,5 正序
	 * sortFlag:0,2,4倒序
	 * sortColValue:lishi 按历时排序
	 * sortColValue: start_time按发时排序
	 * sortColValue:arrive_time 按到时排序
	 * */
	function sortTicketResult(ticketResult,sortFlag,sortColValue){
		var by = function(name)
		{
			return function(o, p)
			{
				var a, b;
				if (typeof o === "object" && typeof p === "object" && o && p) 
				{
					a = o[name];
					b = p[name];
					
					if(sortFlag=="0" || sortFlag=="2" || sortFlag=="4"){
						if (a === b) {return 0;}
						if (typeof a === typeof b) { return a < b ? 1 : -1;}
						return typeof a < typeof b ? 1 : -1;
					}
					if(sortFlag=="1" || sortFlag=="3" || sortFlag=="5"){
						if (a === b) {return 0;}
						if (typeof a === typeof b) { return a > b ? 1 : -1;}
						return typeof a > typeof b ? 1 : -1;
					}
					
				}
				else {throw ("error"); }
			};
		};
		if(sortColValue == "lishi"){
			ticketResult.sort(by("lishi"));
		}
		if(sortColValue == "start_time"){
			ticketResult.sort(by("start_time"));
		}
		if(sortColValue == "arrive_time"){
			ticketResult.sort(by("arrive_time"));
		}
		return ticketResult;
	}
	
	function registerRefreshBtnClickHandler(){
		jq("#refreshBtn").off().on("tap", function(event,data) {			
			event.preventDefault();
			jq("#popupBasic-screen").click();
			onPullDown(event, data);
		});
	}
	/*xuyi*/
	function registerShareBtnClickHandler() {
		jq("#shareBtn").off().on("tap", function(event) {			
			event.preventDefault();
			popupWindow.close();
			//jq( "div[data-role='popup']" ).popup( "close" );
			//busy.show();
			//showLoader();
			showImg("请稍候...");
			jq("#searchSingleTicketResultsViewFooter").css("bottom","-1px!important;");
			var cloneContent = jq("#searchSingleTicketResultsView").clone().insertAfter("#searchSingleTicketResultsView");
			cloneContent.attr("style","padding-top: 0px !important ; padding-bottom: 0px !important ;");
			cloneContent.children("#searchSingleTicketResultList_List_content").first().replaceWith(jq("#forCopysearchSingleTicketResultsView").clone().attr("style","margin-top: 35px !important ; min-height : "+jq("div[data-role='content']").height()+"px ;" ));/*newStyle.css 列车查询结果*/
			cloneContent.children(".ui-popup-screen").remove();
			cloneContent.children(".ui-popup-container").remove();
			cloneContent.attr("id","iscopySearchSingleTicketResultsView");
			cloneContent.children().removeClass("ui-header-fixed");
			cloneContent.children().removeClass("ui-footer-fixed");
			html2canvas(document.getElementById("iscopySearchSingleTicketResultsView"), {
				allowTaint : true,
				taintTest : false,
				chinese : true,
				onrendered : function(canvas) {
					// canvas.id = "mycanvas";
					// document.body.appendChild(canvas);
					// 生成base64图片数据
					var dataUrl = canvas.toDataURL();
//					cordova.exec(callShareSuccess, callFail,
//							"SaveImgPlugin", "saveImg", [ dataUrl,"Screenshot_"+shareImageName ]);
					cordova.exec(callShareSuccess, callFail,
							"SocialSharing", "share", [ dataUrl,"Screenshot_"+shareImageName ]);
					cloneContent.remove();
					//jq("#searchSingleTicketResultsViewFooter").css("bottom","0px!important;");
					//busy.hide();
					//hideLoader();
					hideImg();
				}
			});

		});
	}
	
	function registerScreenShotBtnClickHandler() {
		jq("#screenShortBtn").off().on("tap", function(event) {
			event.preventDefault();
			popupWindow.close();
			//jq( "div[data-role='popup']" ).popup( "close" );
			//busy.show();
			//showLoader();
			showImg("请稍候...");
			jq("#searchSingleTicketResultsViewFooter").css("bottom","-1px!important;");
			var cloneContent = jq("#searchSingleTicketResultsView").clone().insertAfter("#searchSingleTicketResultsView");
			cloneContent.attr("style","padding-top: 0px !important ; padding-bottom: 0px !important ;");
			cloneContent.children("#searchSingleTicketResultList_List_content").first().replaceWith(jq("#forCopysearchSingleTicketResultsView").clone().attr("style","margin-top: 35px !important ; min-height : "+jq("div[data-role='content']").height()+"px ;"));/*newStyle.css 列车查询结果*/
			cloneContent.children(".ui-popup-screen").remove();
			cloneContent.children(".ui-popup-container").remove();
			cloneContent.attr("id","iscopySearchSingleTicketResultsView");
			cloneContent.children().removeClass("ui-header-fixed");
			cloneContent.children().removeClass("ui-footer-fixed");
			cloneContent.children().removeClass("top-prompt");
			html2canvas(document.getElementById("iscopySearchSingleTicketResultsView"), {
				allowTaint : true,
				taintTest : false,
				chinese : true,
				onrendered : function(canvas) {
					// canvas.id = "mycanvas";
					// document.body.appendChild(canvas);
					// 生成base64图片数据
					var dataUrl = canvas.toDataURL();
					cordova.exec(callScreenShotSuccess, callFail,
							"SaveImgPlugin", "saveImg", [ dataUrl,saveImgName]);
					cloneContent.remove();
					//jq("#searchSingleTicketResultsViewFooter").css("bottom","0px!important;");
					//jq("#searchSingleTicketResultsView").remove();
					//busy.hide();
					//hideLoader();
					hideImg();
				}
			});

		});
	}
	
	function showImg(text){
		jq(document).progressDialog.showDialog(text);
	}
	function hideImg(){
		jq(document).progressDialog.hideDialog();
	}
	function showLoader(){
		jq.mobile.loading('show',{
			text: '截图中...',
			textVisible: true,
			theme: 'd',
			textonly: false,
			html: ""
		});
	}
	
	function hideLoader(){
		jq.mobile.loading('hide');
	}
	
	function callShareSuccess(data) {
		// alert(data);
		/*WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);*/
		openNativePage(data,"Share");
	}
	
	function callScreenShotSuccess(data) {
		// alert(data);
		if (data == "IOS"){
			WL.SimpleDialog.show("温馨提示", "已成功保存至相册", [ {
				text : '确定',
				handler : function() {
				}
			} ]);	
		}else{
			openNativePage(data,"ScreenShot");
		}
	
	}

	function callFail(data) {
		// alert(data);
		WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);
	}
	
	function openNativePage(data,whatBtn) {
		var params = {
				imgPath : data
		};
		
		if(whatBtn === "Share"){
		WL.NativePage.show('com.MobileTicket.MobileTicketShare',
				backFromNativePage, params);
		
		}else if(whatBtn === "ScreenShot"){
			WL.NativePage.show('com.MobileTicket.ScreenShotSaveImg',
					backFromNativePage, params);
			}
		}


	function backFromNativePage(data) {
		WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);
	}
	
	
	
	var ticketListTemplate = "{{~it :ticket:index}}"
			+ "<li data-index='{{=index}}' data-iconshadow='false' data-icon='none'>"
			+ "{{ if(ticket.isTicketNum == 'false') { }}"
			+ "<div class='ui-grid-a' style='background:#F0F0F0;'><div class='ui-block-a'>"
			+ "<a name='ticket' class='ui-link'>"
			+ "<div class='ticket-train-row'>"
			+ "<div class='ticket-train-code-col' style='color:grey;'>{{=ticket.station_train_code}}</div>"
			+ "<div class='ticket-android-fix'><div class='ticket-train-code-txt'><div class='ui-grid-b'>"
			+ "<div class='ui-block-a'>"
			+ "<div class='ticket-station-div' style='color:grey;'>"
			+ "{{ if(ticket.from_station_telecode == ticket.start_station_telecode) { }}"
			+ "<div class='ticket-station-div_from_station'>始</div>"
			+ "{{ }else{ }}"
			+"<div class='ticket-station-div_pass_station'>过</div>"
			+ "{{ } }}"
			+ "{{ if(mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode) == undefined || mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode) == 'undefined'){ }}"
			+ "{{=ticket.from_station_name}}</div>"
			+ "{{ }else{ }}"
			+ "{{=mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode)}}</div>"
			+ "{{ } }}"
			+ "<div class='ticket-time'>{{=ticket.start_time}}</div>"
			+ "</div>"
			+ "<div class='ui-block-b'>"
			+ "{{ if(ticket.is_support_card == '1'){ }}"
			+ "<div class='el-ticket-arrow'></div>"
			+ "{{ } }}"
			+ "<div class='ticket-right-arrow'></div>"
			+ "<div class='ticket-duration'>"
			+ "{{ if(ticket.lishiHour != '00'){ }}"
			+ "{{=ticket.lishiHour}}小时"
			+ "{{ } }}"
			+ "{{=ticket.lishiMinute}}分"
			+ "</div>"
			+ "</div>"
			+ "<div class='ui-block-c'>"
			+ "<div class='ticket-station-div' style='color:grey;'><span style='text-align:left;display:inline-block;width:80px;vertical-align:top'>"
			+ "{{ if(ticket.to_station_telecode == ticket.end_station_telecode) { }}"
			+"<div class='ticket-station-div_to_station'>终</div>"
			+ "{{ }else{ }}"
			+"<div class='ticket-station-div_pass_station'>过</div>"
			+ "{{ } }}"
			+ "{{ if(mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode) == undefined || mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode) == 'undefined'){ }}"
			+ "{{=ticket.to_station_name}}</span></div>"
			+ "{{ }else{ }}"
			+ "{{=mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode)}}</span></div>"
			+ "{{ } }}"
			+ "<div class='ticket-time'>{{=ticket.arrive_time}}</div>"
			+ "</div>"
			+ "</div>"
			+ "</div></div></div>"
			+ "<div class='ui-grid-c ticket-info-grid'>"
			+ "{{ for(var i=0;i<ticket.yplist.length;i++) { }}"
			+ "{{ if(ticket.flag != '0'){if(ticket.yplist[i].num ){if(ticket.yplist[i].num <= 20) { }}"
			+ "<span class='{{=ticket.yplist[i].uclass}} s-span-black' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].num}}</span>张</span>"
			+ "{{ }else{ }}"
			+ "<span class='{{=ticket.yplist[i].uclass}} s-span-green' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span class='s-span-black'>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].num}}</span>张</span>"
			+ "{{ }  }}"
			+ "{{ }else{ }}"
			+ "<span class='{{=ticket.yplist[i].uclass}} s-span-grey' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span>{{=ticket.yplist[i].type}}{{ } }}</span>:无</span>"
			+ "{{ } }}"
			+ "{{ }else{ }}"
			+ "<span class='{{=ticket.yplist[i].uclass}} s-span-grey' ><span class='s-span-black'>{{=ticket.yplist[i].type}}:</span><span class='ticket-icon-presell'>*</span></span>"
			+ "{{ } }}" + "{{ } }}" + "</div>"
			+ "{{ if(ticket.flag == '0'){ }}"
			+ "<div class='ticket-info-presell'>{{=ticket.message}}</div>"
			+ "{{ } }}"
			+ "</a>"
			+ "</div>"
			+ "{{ if(ticket.discount == '1') { }}"
			+ "<div class='ui-block-b triangle-topright' name='stopStation'>"
			+ "</div>" 
			+ "<div id='folder'>"
			+ "<a class='stopStation moreDownStation'></a>"
			+ "</div>"
			+ "<div class='discountDiv'></div>"
			+ "{{ }else{ }}"
			+ "<div class='ui-block-b triangle-topright-nodiscount' name='stopStation'>" 
			+ "</div>" 
			+ "<div id='folder'>"
			+ "<span>" 
			+ "<div></div>"
			+ "</span>" 
			+ "<a class='stopStation moreDownStation'></a></div>"
			+ "{{ } }}"
			+ "</div>"
			+ "{{ }else{ }}"
			+ "<div class='ui-grid-a'><div class='ui-block-a'>"
			+ "<a name='ticket' class='ui-link'>"
			+ "<div class='ticket-train-row'>"
			+ "<div class='ticket-train-code-col'>{{=ticket.station_train_code}}</div>"
			+ "<div class='ticket-android-fix'><div class='ticket-train-code-txt'><div class='ui-grid-b'>"
			+ "<div class='ui-block-a'>"
			+ "<div class='ticket-station-div'>"
			+ "{{ if(ticket.from_station_telecode == ticket.start_station_telecode) { }}"
			+ "<div class='ticket-station-div_from_station'>始</div>"
			+ "{{ }else{ }}"
			+"<div class='ticket-station-div_pass_station'>过</div>"
			+ "{{ } }}"
			+ "{{ if(mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode) == undefined || mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode) == 'undefined'){ }}"
			+ "{{=ticket.from_station_name}}</div>"
			+ "{{ }else{ }}"
			+ "{{=mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode)}}</div>"
			+ "{{ } }}"
			+ "<div class='ticket-time'>{{=ticket.start_time}}</div>"
			+ "</div>"
			+ "<div class='ui-block-b'>"
			+ "{{ if(ticket.is_support_card == '1'){ }}"
			+ "<div class='el-ticket-arrow'></div>"
			+ "{{ } }}"
			+ "<div class='ticket-right-arrow'></div>"
			+ "<div class='ticket-duration'>"
			+ "{{ if(ticket.lishiHour != '00'){ }}"
			+ "{{=ticket.lishiHour}}小时"
			+ "{{ } }}"
			+ "{{=ticket.lishiMinute}}分"
			+ "</div>"
			+ "</div>"
			+ "<div class='ui-block-c'>"
			+ "<div class='ticket-station-div'><span style='text-align:left;display:inline-block;width:80px;vertical-align:top'>"
			+ "{{ if(ticket.to_station_telecode == ticket.end_station_telecode) { }}"
			+"<div class='ticket-station-div_to_station'>终</div>"
			+ "{{ }else{ }}"
			+"<div class='ticket-station-div_pass_station'>过</div>"
			+ "{{ } }}"
			+ "{{ if(mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode) == undefined || mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode) == 'undefined'){ }}"
			+ "{{=ticket.to_station_name}}</span></div>"
			+ "{{ }else{ }}"
			+ "{{=mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode)}}</span></div>"
			+ "{{ } }}"
			+ "<div class='ticket-time'>{{=ticket.arrive_time}}</div>"
			+ "</div>"
			+ "</div>"
			+ "</div></div></div>"
			+ "<div class='ui-grid-c ticket-info-grid'>"
			+ "{{ for(var i=0;i<ticket.yplist.length;i++) { }}"
			+ "{{ if(ticket.flag != '0'){if(ticket.yplist[i].num ){if(ticket.yplist[i].num <= 20) { }}"
			+ "<span class='{{=ticket.yplist[i].uclass}} s-span-black' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].num}}</span>张</span>"
			+ "{{ }else{ }}"
			+ "<span class='{{=ticket.yplist[i].uclass}} s-span-green' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span class='s-span-black'>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].num}}</span>张</span>"
			+ "{{ }  }}"
			+ "{{ }else{ }}"
			+ "<span class='{{=ticket.yplist[i].uclass}} s-span-grey' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span>{{=ticket.yplist[i].type}}{{ } }}</span>:无</span>"
			+ "{{ } }}"
			+ "{{ }else{ }}"
			+ "<span class='{{=ticket.yplist[i].uclass}} s-span-grey' ><span class='s-span-black'>{{=ticket.yplist[i].type}}:</span><span class='ticket-icon-presell'>*</span></span>"
			+ "{{ } }}" + "{{ } }}" + "</div>"
			+ "{{ if(ticket.flag == '0'){ }}"
			+ "<div class='ticket-info-presell'>{{=ticket.message}}</div>"
			+ "{{ } }}"
			+ "</a>"
			+ "</div>"
			+ "{{ if(ticket.discount == '1') { }}"
			+ "<div class='ui-block-b triangle-topright' name='stopStation'>"
			+ "</div>" 
			+ "<div id='folder'>"
			+ "<a class='stopStation moreDownStation'></a>"
			+ "</div>"
			+ "<div class='discountDiv'></div>"
			+ "{{ }else{ }}"
			+ "<div class='ui-block-b triangle-topright-nodiscount' name='stopStation'>" 
			+ "</div>" 
			+ "<div id='folder'>"
			+ "<span>" 
			+ "<div></div>"
			+ "</span>" 
			+ "<a class='stopStation moreDownStation'></a></div>"
			+ "{{ } }}"
			+ "</div>"
			+ "{{ } }}"
			+ "<div id='stop_{{=index}}' style='display:none;'>"

			+"</div>"
			+ "</li>"
			+ "{{~}}";
	var generateListContent = doT.template(ticketListTemplate);
	
	
	
	var ticketPriceListTemplate = "{{~it :ticket:index}}"
		+ "<li data-index='{{=index}}' data-iconshadow='false' data-icon='none'>"
		+ "{{ if(ticket.isTicketNum == 'false') { }}"
		+ "<div class='ui-grid-a' style='background:#F0F0F0;'><div class='ui-block-a'>"
		+ "<a name='ticket' class='ui-link'>"
		+ "<div class='ticket-train-row'>"
		+ "<div class='ticket-train-code-col' style='color:grey;'>{{=ticket.station_train_code}}</div>"
		+ "<div class='ticket-android-fix'><div class='ticket-train-code-txt'><div class='ui-grid-b'>"
		+ "<div class='ui-block-a'>"
		+ "<div class='ticket-station-div' style='color:grey;'>"
		+ "{{ if(ticket.from_station_telecode == ticket.start_station_telecode) { }}"
		+ "<div class='ticket-station-div_from_station'>始</div>"
		+ "{{ }else{ }}"
		+"<div class='ticket-station-div_pass_station'>过</div>"
		+ "{{ } }}"
		+ "{{ if(mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode) == undefined || mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode) == 'undefined'){ }}"
		+ "{{=ticket.from_station_name}}</div>"
		+ "{{ }else{ }}"
		+ "{{=mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode)}}</div>"
		+ "{{ } }}"
		+ "<div class='ticket-time'>{{=ticket.start_time}}</div>"
		+ "</div>"
		+ "<div class='ui-block-b'>"
		+ "{{ if(ticket.is_support_card == '1'){ }}"
		+ "<div class='el-ticket-arrow'></div>"
		+ "{{ } }}"
		+ "<div class='ticket-right-arrow'></div>"
		+ "<div class='ticket-duration'>"
		+ "{{ if(ticket.lishiHour != '00'){ }}"
		+ "{{=ticket.lishiHour}}小时"
		+ "{{ } }}"
		+ "{{=ticket.lishiMinute}}分"
		+ "</div>"
		+ "</div>"
		+ "<div class='ui-block-c'>"
		+ "<div class='ticket-station-div' style='color:grey;'><span style='text-align:left;display:inline-block;width:80px;vertical-align:top'>"
		+ "{{ if(ticket.to_station_telecode == ticket.end_station_telecode) { }}"
		+"<div class='ticket-station-div_to_station'>终</div>"
		+ "{{ }else{ }}"
		+"<div class='ticket-station-div_pass_station'>过</div>"
		+ "{{ } }}"
		+ "{{ if(mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode) == undefined || mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode) == 'undefined'){ }}"
		+ "{{=ticket.to_station_name}}</span></div>"
		+ "{{ }else{ }}"
		+ "{{=mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode)}}</span></div>"
		+ "{{ } }}"
		+ "<div class='ticket-time'>{{=ticket.arrive_time}}</div>"
		+ "</div>"
		+ "</div>"
		+ "</div></div></div>"
		+ "<div class='ui-grid-c ticket-info-grid'>"
		+ "{{ for(var i=0;i<ticket.yplist.length;i++) { }}"
		+ "{{ if(ticket.flag != '0'){if(ticket.yplist[i].num ){if(ticket.yplist[i].num <= 20) { }}"
		+ "<span class='{{=ticket.yplist[i].uclass}} s-span-black' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].price}}</span></span>"
		+ "{{ }else{ }}"
		+ "<span class='{{=ticket.yplist[i].uclass}} s-span-green' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span class='s-span-black'>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].price}}</span></span>"
		+ "{{ }  }}"
		+ "{{ }else{ }}"
		+ "<span class='{{=ticket.yplist[i].uclass}} s-span-grey' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].price}}</span></span>"
		+ "{{ } }}"
		+ "{{ }else{ }}"
		+ "<span class='{{=ticket.yplist[i].uclass}} s-span-grey' ><span>{{=ticket.yplist[i].type}}:</span><span class='numberFont'>{{=ticket.yplist[i].price}}</span></span>"
		+ "{{ } }}" + "{{ } }}" + "</div>"
		+ "{{ if(ticket.flag == '0'){ }}"
		+ "<div class='ticket-info-presell'>{{=ticket.message}}</div>"
		+ "{{ } }}"
		+ "</a>"
		+ "</div>"
		+ "{{ if(ticket.discount == '1') { }}"
		+ "<div class='ui-block-b triangle-topright' name='stopStation'>" 
		+ "</div>" 
		+ "<div id='folder'>"
		+ "<a class='stopStation moreDownStation'></a>"
		+ "</div>"
		+ "<div class='discountDiv'></div>"
		+ "{{ }else{ }}"
		+ "<div class='ui-block-b triangle-topright-nodiscount' name='stopStation'>" 
		+ "</div>" 
		+ "<div id='folder'>"
		+ "<span>" 
		+ "<div></div>"
		+ "</span>" 
		+ "<a class='stopStation moreDownStation'></a>" 
		+ "</div>"
		+ "{{ } }}"
		+ "</div>"
		+ "{{ }else{ }}"
		+ "<div class='ui-grid-a'><div class='ui-block-a'>"
		+ "<a name='ticket' class='ui-link'>"
		+ "<div class='ticket-train-row'>"
		+ "<div class='ticket-train-code-col'>{{=ticket.station_train_code}}</div>"
		+ "<div class='ticket-android-fix'><div class='ticket-train-code-txt'><div class='ui-grid-b'>"
		+ "<div class='ui-block-a'>"
		+ "<div class='ticket-station-div'>"
		+ "{{ if(ticket.from_station_telecode == ticket.start_station_telecode) { }}"
		+ "<div class='ticket-station-div_from_station'>始</div>"
		+ "{{ }else{ }}"
		+"<div class='ticket-station-div_pass_station'>过</div>"
		+ "{{ } }}"
		+ "{{ if(mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode) == undefined || mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode) == 'undefined'){ }}"
		+ "{{=ticket.from_station_name}}</div>"
		+ "{{ }else{ }}"
		+ "{{=mor.ticket.cache.getStationNameByCode(ticket.from_station_telecode)}}</div>"
		+ "{{ } }}"
		+ "<div class='ticket-time'>{{=ticket.start_time}}</div>"
		+ "</div>"
		+ "<div class='ui-block-b'>"
		+ "{{ if(ticket.is_support_card == '1'){ }}"
		+ "<div class='el-ticket-arrow'></div>"
		+ "{{ } }}"
		+ "<div class='ticket-right-arrow'></div>"
		+ "<div class='ticket-duration'>"
		+ "{{ if(ticket.lishiHour != '00'){ }}"
		+ "{{=ticket.lishiHour}}小时"
		+ "{{ } }}"
		+ "{{=ticket.lishiMinute}}分"
		+ "</div>"
		+ "</div>"
		+ "<div class='ui-block-c'>"
		+ "<div class='ticket-station-div'><span style='text-align:left;display:inline-block;width:80px;vertical-align:top'>"
		+ "{{ if(ticket.to_station_telecode == ticket.end_station_telecode) { }}"
		+"<div class='ticket-station-div_to_station'>终</div>"
		+ "{{ }else{ }}"
		+"<div class='ticket-station-div_pass_station'>过</div>"
		+ "{{ } }}"
		+ "{{ if(mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode) == undefined || mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode) == 'undefined'){ }}"
		+ "{{=ticket.to_station_name}}</span></div>"
		+ "{{ }else{ }}"
		+ "{{=mor.ticket.cache.getStationNameByCode(ticket.to_station_telecode)}}</span></div>"
		+ "{{ } }}"
		+ "<div class='ticket-time'>{{=ticket.arrive_time}}</div>"
		+ "</div>"
		+ "</div>"
		+ "</div></div></div>"
		+ "<div class='ui-grid-c ticket-info-grid'>"
		+ "{{ for(var i=0;i<ticket.yplist.length;i++) { }}"
		+ "{{ if(ticket.flag != '0'){if(ticket.yplist[i].num ){if(ticket.yplist[i].num <= 20) { }}"
		+ "<span class='{{=ticket.yplist[i].uclass}} s-span-black' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].price}}</span></span>"
		+ "{{ }else{ }}"
		+ "<span class='{{=ticket.yplist[i].uclass}} s-span-green' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span class='s-span-black'>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].price}}</span></span>"
		+ "{{ }  }}"
		+ "{{ }else{ }}"
		+ "<span class='{{=ticket.yplist[i].uclass}} s-span-grey' >{{ if(ticket.yplist[i].discount == '1'){ }}<span style='color:red;'>{{=ticket.yplist[i].type}}{{ }else{ }}<span>{{=ticket.yplist[i].type}}{{ } }}</span>:<span class='numberFont'>{{=ticket.yplist[i].price}}</span></span>"
		+ "{{ } }}"
		+ "{{ }else{ }}"
		+ "<span class='{{=ticket.yplist[i].uclass}} s-span-grey' ><span>{{=ticket.yplist[i].type}}:</span><span class='numberFont'>{{=ticket.yplist[i].price}}</span></span>"
		+ "{{ } }}" + "{{ } }}" + "</div>"
		+ "{{ if(ticket.flag == '0'){ }}"
		+ "<div class='ticket-info-presell'>{{=ticket.message}}</div>"
		+ "{{ } }}"
		+ "</a>"
		+ "</div>"
		+ "{{ if(ticket.discount == '1') { }}"
		+ "<div class='ui-block-b triangle-topright' name='stopStation'>" 
		+ "</div>" 
		+ "<div id='folder'>"
		+ "<a class='stopStation moreDownStation'></a>"
		+ "</div>"
		+ "<div class='discountDiv'></div>"
		+ "{{ }else{ }}"
		+ "<div class='ui-block-b triangle-topright-nodiscount' name='stopStation'>" 
		+ "</div>" 
		+ "<div id='folder'>"
		+ "<span>" 
		+ "<div></div>"
		+ "</span>" 
		+ "<a class='stopStation moreDownStation'></a>" 
		+ "</div>"
		+ "{{ } }}"
		+ "</div>"
		+ "{{ } }}"
		+ "<div id='stop_{{=index}}' style='display:none;'>"
		+ "</div>"
		+ "</li>"
		+ "{{~}}";
	var generatePriceListContent = doT.template(ticketPriceListTemplate);
	
	
	
	var stopStationListTemplate = "<table class='stopStationTable'>"
		+"<tr class='hearders' id='trclick'>"
		+"<td style='width:20%'>站序</td><td style='width:20%'>站名</td><td style='width:20%'>到时</td><td style='width:20%'>发时</td><td style='width:20%'>停留</td>"
		+"</tr>"		
		+"{{~it :StationList:index}}"
		+"{{ if(mor.ticket.viewControl.bookMode === 'dc' || (mor.ticket.viewControl.bookMode === 'gc' && mor.ticket.changeOrchangeStation.changeOrchangeStation === '变更到站')){ }}"
		+"{{ if(StationList.isEnabled == 'true'){ }}"
		
		+"{{ if(StationList.station_name == mor.ticket.cache.getStationNameByCode(mor.ticket.currentTicket.to_station_telecode) || StationList.station_name == mor.ticket.cache.getStationNameByCode(mor.ticket.currentTicket.from_station_telecode)){ }}"
		+"<tr class='noclicktr'>"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>{{=StationList.station_no}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.station_name}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.arrive_time}}</td><td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.station_no}}</td><td style='width:20%;'>{{=StationList.station_name}}</td><td style='width:20%;'>{{=StationList.arrive_time}}</td><td style='width:20%;'>{{=StationList.start_time}}</td>"
		+"{{ } }}"
		+"{{ if(StationList.stopover_time == '----'){ }}"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>----</td>"
		+"{{ } }}"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.stopover_time}}</td>"
		+"{{ } }}"
		+"</tr>"		
		+"{{ }else{ }}"
		+"<tr class='station_on'>"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>{{=StationList.station_no}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.station_name}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.arrive_time}}</td><td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.station_no}}</td><td style='width:20%;'>{{=StationList.station_name}}</td><td style='width:20%;'>{{=StationList.arrive_time}}</td><td style='width:20%;'>{{=StationList.start_time}}</td>"
		+"{{ } }}"
		+"{{ if(StationList.stopover_time == '----'){ }}"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>----</td>"
		+"{{ } }}"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.stopover_time}}</td>"
		+"{{ } }}"
		+"</tr>"

		+"{{ } }}"		
				
		+"{{ }else{ }}"
		+"<tr style='color:#999;' class='pass_arrive_station'>"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>{{=StationList.station_no}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.station_name}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.arrive_time}}</td><td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.station_no}}</td><td style='width:20%;'>{{=StationList.station_name}}</td><td style='width:20%;'>{{=StationList.arrive_time}}</td><td style='width:20%;'>{{=StationList.start_time}}</td>"
		+"{{ } }}"
		+"{{ if(StationList.stopover_time == '----'){ }}"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>----</td>"
		+"{{ } }}"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.stopover_time}}</td>"
		+"{{ } }}"
		+"</tr>"
		+"{{ } }}"
				
		+"{{ }else{ }}"
		+"{{ if(StationList.isEnabled == 'true'){ }}"			
		+"<tr>"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>{{=StationList.station_no}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.station_name}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.arrive_time}}</td><td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.station_no}}</td><td style='width:20%;'>{{=StationList.station_name}}</td><td style='width:20%;'>{{=StationList.arrive_time}}</td><td style='width:20%;'>{{=StationList.start_time}}</td>"
		+"{{ } }}"
		+"{{ if(StationList.stopover_time == '----'){ }}"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>----</td>"
		+"{{ } }}"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.stopover_time}}</td>"
		+"{{ } }}"
		+"</tr>"
		+"{{ }else{ }}"
		+"<tr style='color:#999;'>"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>{{=StationList.station_no}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.station_name}}</td><td style='width:20%;border-bottom:none;'>{{=StationList.arrive_time}}</td><td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.station_no}}</td><td style='width:20%;'>{{=StationList.station_name}}</td><td style='width:20%;'>{{=StationList.arrive_time}}</td><td style='width:20%;'>{{=StationList.start_time}}</td>"
		+"{{ } }}"
		+"{{ if(StationList.stopover_time == '----'){ }}"
		+"{{ if(StationList.start_time == StationList.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>----</td>"
		+"{{ } }}"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=StationList.stopover_time}}</td>"
		+"{{ } }}"
		+"</tr>"
		+"{{ } }}"	
		+"{{ } }}"			
		+"{{~}}";
		+"</table>";			
		
	var generateStopStationListContent = doT.template(stopStationListTemplate);
	
})();