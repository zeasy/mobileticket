
/* JavaScript content from js/controller/my12306.js in folder common */
(function() {
	var clickCount = 0;
	jq.extendModule("mor.ticket.userPhone", {
		userPhoneFlag : ""
	});
	jq("#my12306View").live("pageinit", function() {
		clickCount = 0;
		//init country id value
		mor.ticket.userPhone.userPhoneFlag = "";
		var countryMap = mor.cache.countryMap;
		if(countryMap){
			var countryList = mor.ticket.cache.country;
			for(var i = 0; i < countryList.length; i++){
				countryMap[countryList[i].id] = countryList[i].value;
			};
		}
		jq("#favoriteContacts").off().bind("tap", function() {
			if(clickCount != 0){
				mor.ticket.util.changePage("passengerList.html");
				clickCount += 1;
			}
			return false;
		});
		
		jq("#userInformation").bind("tap", function() {
			mor.ticket.util.changePage("userInfo.html");
			return false;
		});
		
		jq("#ticketAddressMessage").off().bind("tap",function(){
			mor.ticket.viewControl.current_tab = "ticketAddressMessageTab";
			mor.ticket.util.changePage("sendTicketAddress.html");
			return false;
		});
		
		jq("#changPasswd").bind("tap", function() {
			mor.ticket.util.changePage("changePwd.html");
			return false;
		});
		
		jq("#phoneBinding").bind("tap", function() {
			mor.ticket.util.changePage("userPhoneVerifyCheck.html");
			return false;
		});
		
		jq("#updateUserEmail").bind("tap", function() {
			mor.ticket.util.changePage("updateUserEmail.html");
			return false;
		});
		
		jq("#my12306BackBtn").off().bind("tap",function(){
			mor.ticket.util.changePage("moreOption.html");
			return false;
		});
		jq("#sendEmailEmail").bind("tap",sendEmail);
		registerLogoutBtnLisener();
	});
	
	jq("#my12306View").live("pagebeforeshow", function() {
		mor.ticket.util.initAppVersionInfo();
		mor.ticket.viewControl.tab3_cur_page="";
		var user = mor.ticket.loginUser;
		if (user.isAuthenticated === "Y") {
			var user = mor.ticket.loginUser;
			var name = user.realName;
			jq("#userNameDesc").html(name);
			setTimeout(function(){
				clickCount = 1;
			},500);
		} else {
			var latestTime = window.ticketStorage.getItem("pwdTime");
			// mod by yiguo
			if (window.ticketStorage
					.getItem("autologin") == "true" && (!latestTime || (Date.now() - latestTime < 7*24*3600*1000))) {
				registerAutoLoginHandler(function() {
				}, autologinFailJump);
			}else{
				autologinFailJump();
			}
		}
		if(mor.ticket.loginUser.activeUser === "Y"){
			jq("#sendEmailEmail").hide();
			jq("#updateUserEmail").removeClass("departshortline");
			jq("#updateUserEmail").addClass("departshortnoline");
		}else{
			jq("#sendEmailEmail").show();
			jq("#updateUserEmail").removeClass("departshortnoline");
			jq("#updateUserEmail").addClass("departshortline");
		}
		/*if(mor.ticket.loginUser.start_receive == "Y" && mor.ticket.loginUser.is_receive == "N"){
			jq("#phoneBindingCircleId").css("display","block");
		}else{
			jq("#phoneBindingCircleId").css("display","none");
		}*/
	});
	function registerLogoutBtnLisener(){
		jq("#logoutBtn").off().bind("tap", sendLogoutRequest);
	};
	
	function sendLogoutRequest(e) {
		mor.ticket.viewControl.bookMode = "dc";
		var util = mor.ticket.util;
		var user = mor.ticket.loginUser;
		var commonParameters = {
			'baseDTO.user_name': user["username"]
		};
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "logout"
		};
		
		var options =  {
				onSuccess: requestSucceeded,
				onFailure: requestSucceeded
		};
		
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestFailure(result){
		//clean session time out page url
		mor.ticket.viewControl.session_out_page = "";
		mor.ticket.util.creatCommonRequestFailureHandler(result);
	}
	
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}	
		var passengers = mor.ticket.passengersCache.passengers;
		for(var i =0;i<passengers.length;i++){
			mor.ticket.passengersCache.passengers[i].checked =0;	
		}
		mor.ticket.passengerList = [];
		//window.ticketStorage.setItem("autologin",false);
		
		//jq("#autologinChkbox").attr("checked", false);
		//clean session time out page url
		mor.ticket.viewControl.session_out_page = "";
		mor.ticket.loginUser["isAuthenticated"]="N";
		//mor.ticket.util.alertMessage("您已成功退出");
		mor.ticket.viewControl.tab1_cur_page="";
		mor.ticket.viewControl.tab2_cur_page="";
		mor.ticket.viewControl.tab3_cur_page="";
		mor.ticket.viewControl.tab4_cur_page="";
		mor.ticket.viewControl.current_tab="";
		mor.ticket.leftTicketQuery.train_date = "";
		mor.ticket.leftTicketQuery.from_station_telecode = "";
		mor.ticket.leftTicketQuery.to_station_telecode = "";
		mor.ticket.loginUser.password="";	
		mor.ticket.loginUser['password']="";
		//push 用户注销
		unregisterChannelsUser(mor.ticket.loginUser.accountName);
		WL.EncryptedCache.open("wlkey",true,onOpenComplete,onOpenError);
		clearUserCache();
		jq("#selectPassengerView").remove();
		jq("#queryOrderView").remove();
		jq.mobile.changePage(vPathCallBack()+"loginTicket.html");
	}
	function clearUserCache() {
				//window.ticketStorage.setItem("password", user.password);
				WL.EncryptedCache.open("wlkey",true,onOpenComplete,onOpenError);
	}

	function onOpenComplete(){ 
		WL.EncryptedCache.write("userPW", "*", 
				function(){
//					WL.Logger.debug("Successfully write to storage");
					WL.EncryptedCache.close(function(){
//						WL.Logger.debug("Encrypted Cache Closed.");
					}, 
				function(){/*WL.Logger.debug("Failed to close Encrypted Cache.");*/});},
				function(){/*WL.Logger.debug("Failed to write to ticketStorage");*/});
	}
	
	function onOpenError(status){
//		WL.Logger.debug("Cannot open encrypted cache");
	}
	function sendEmail(){
		var commonParameters = {};
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "reSendEmail"
			};
			
			var options =  {
					onSuccess: sendEmailRequestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
	}
	function sendEmailRequestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			jq("#sendEmailEmail").hide();
			jq("#updateUserEmail").removeClass("departshortline");
			jq("#updateUserEmail").addClass("departshortnoline");
			WL.SimpleDialog.show("温馨提示", "验证邮件已发送，请您登录邮箱"+mor.ticket.loginUser.email+"完成验证。", [ {
				text : '确定',
				handler : function() {}
			} ]);
		} else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
})();