/**
 * @providesModule Login_1_0_0
 * Created by easy on 16/1/12.
 */
'use strict';
var React = require('react-native');
let ZZRandCodeView = require('ZZRandCodeView');
let ZZNetwork = require('ZZNetwork');
let ZZHUD = require('ZZHUD');


var {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Alert,
    ScrollView,
    TextInput,
    } = React;

let Login = React.createClass({

    account : '',
    password : '',

    getInitialState: function () {
        return ({
        });
    },

    onSubmitLogin : function() {
        let points = this.refs['randCode'].getPoints();

        if (this.account.length === 0) {
            Alert.alert('错误','请输入正确格式的12306.cn账号');
            return;
        }
        if (this.password.length === 0) {
            Alert.alert('错误','请输入正确格式的12306.cn密码');
            return;
        }
        if (points.length == 0) {
            Alert.alert('错误','请选择验证码');
            return;
        }

        this.requestCheckRandCode(points);
    },

    onLoginSuccess : function() {
        this.props.onSuccess && this.props.onSuccess();
    },

    requestCheckRandCode : function(points) {
        let pts = points.map((v)=>{
            return Math.ceil(v.x) + ',' + Math.ceil(v.y);
        }).join(',');

        let params = {
            randCode:pts,
            rand:'sjrand'
        };

        ZZHUD.show('正在校验验证码');
        ZZNetwork.post('https://kyfw.12306.cn/otn/passcodeNew/checkRandCodeAnsyn',params).then((object)=> {
            if (object.data.result === '1') {
                ZZHUD.show('正在校验密码');
                ZZNetwork.post('https://kyfw.12306.cn/otn/login/loginAysnSuggest',{
                    'loginUserDTO.user_name' : this.account,
                    'userDTO.password' : this.password,
                    'randCode' : pts
                }).then((obj)=>{
                    if (obj['data'] && obj['data']['loginCheck'] === 'Y') {
                        ZZHUD.hide();
                        this.onLoginSuccess();
                    } else {
                        this.requestRandCode();
                        ZZHUD.showError((obj['messages']||[]).join('\n'));
                    }
                }).catch((err)=>{
                    console.log(err);
                    this.requestRandCode();
                    ZZHUD.showError(err.message);
                });
            } else {
                this.requestRandCode();
                ZZHUD.showError('校验码错误');
            }
        }).catch((err)=>{
            this.requestRandCode();
            ZZHUD.showError(err.message);
        });
    },

    requestRandCode : function() {
        this.refs['randCode'].refresh();
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.first === false) {
            this.requestRandCode();
        }
    },

    componentDidMount: function() {
        this.props.navigator.replace({component:Login,title:'登录',passProps:{...this.props,first:false}});
    },

    render: function () {
        return <View style={styles.container}>
            <ScrollView style={styles.scrollContainer}>
                <View style={styles.userName}>
                    <Text style={styles.userNameText}>账号</Text>
                    <TextInput
                        ref = 'accountInput'
                        style = {styles.userNameInput}
                        clearButtonMode="while-editing"
                        placeholderTextColor="gray"
                        placeholder=" 12306.cn 用户名 / 邮箱 / 手机号码"
                        onChangeText={(text)=>{this.account = text;}}
                    />
                </View>
                <View  style={styles.password}>
                    <Text style={styles.passwordText}>密码</Text>
                <TextInput
                    ref = 'passwordInput'
                    style={styles.passwordInput}
                    clearButtonMode="while-editing"
                    placeholderTextColor="gray"
                    placeholder=" 12306.cn 密码"
                    secureTextEntry={true}
                    onChangeText={(text)=>{this.password = text;}}/>
                </View>
                <ZZRandCodeView style={styles.randCode} ref='randCode'/>

                <TouchableOpacity style={styles.submitButton} onPress={this.onSubmitLogin} activeOpacity = {0.65}>
                    <Text style={styles.submitText}>登 录</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>;
    }
});

let styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContainer: {
        flex: 1
    },
    userName: {
        flexDirection : 'row',
        marginTop: 20,
        height: 30,

        marginLeft: 20,
        marginRight: 20
    },
    userNameText : {
        marginTop : 8,
        width : 40
    },
    userNameInput :  {
        flex:1,
        borderColor: 'lightGray',
        borderWidth: 0.5,
        borderRadius: 5,
        fontSize : 14,

        height : 30
    },
    password :  {
        flexDirection : 'row',
        marginTop: 10,
        height: 30,
        marginLeft: 20,
        marginRight: 20
    },
    passwordText : {
        marginTop : 8,
        width : 40
    },
    passwordInput: {
        flex:1,
        borderColor: 'lightGray',
        borderWidth: 0.5,
        borderRadius: 5,
        fontSize : 14,

        height : 30
    },
    randCode: {
        marginTop : 10,
        width: 293,
        height: 190,
        alignSelf: 'center',
    },
    submitButton: {
        borderRadius: 5,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#3498db',

        height: 40
    },
    submitText: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        height: 20,
        fontSize:16,
        marginTop: 11
    }
});

module.exports = Login;