////
////  MTCloudBridge.m
////  MobileTicket
////
////  Created by EASY on 15/10/30.
////  Copyright (c) 2015年 Facebook. All rights reserved.
////
//
//#import "MTCloudBridge.h"
//#import <LeanCloud/AVOSCloud.h>
//#import <RCTConvert.h>
//
//@implementation MTCloudBridge
//
//RCT_EXPORT_MODULE(ZZCloud)
//
//#pragma mark - App
//RCT_EXPORT_METHOD(setAppKeys:(NSArray *) keys) {
//  
//  NSString *key1 = [keys count]?keys[0]:nil;
//  NSString *key2 = [keys count]>1?keys[1]:nil;
//  [AVOSCloud setApplicationId:key1 clientKey:key2];
//}
//
//#pragma mark - Function
//RCT_EXPORT_METHOD(callFunction:(NSString *) func parameters:(NSDictionary *) parameters successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
//  
//  func = [RCTConvert NSString:func];
//  parameters = [RCTConvert NSDictionary:parameters];
//  [AVCloud callFunctionInBackground:func withParameters:parameters block:^(id object, NSError *error) {
//    if (error) {
//      fail(error);
//    } else {
//      success(@[object]);
//    }
//  }];
//}
//
//#pragma mark - AVAnalytics
//
//RCT_EXPORT_METHOD(analyticsBeginPageView:(NSString *) pageName) {
//  [AVAnalytics beginLogPageView:pageName];
//}
//
//RCT_EXPORT_METHOD(analyticsEndPageView:(NSString *) pageName) {
//  [AVAnalytics endLogPageView:pageName];
//}
//
//RCT_EXPORT_METHOD(analyticsEvent:(NSString *) event attributes:(id) attrs) {
//  if ([attrs isKindOfClass:[NSString class]]) {
//    [AVAnalytics event:event label:(NSString *)attrs];
//  } else if ([attrs isKindOfClass:[NSDictionary class]]) {
//    [AVAnalytics event:event attributes:attrs];
//  } else if ([attrs isKindOfClass:[NSNumber class]]) {
//    [AVAnalytics event:event durations:[attrs intValue]];
//  }
//}
//
//RCT_EXPORT_METHOD(analyticsUpdateOnlineConfigs:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
//  [AVAnalytics updateOnlineConfigWithBlock:^(NSDictionary *dict, NSError *error) {
//    if (error) {
//      fail(error);
//    } else {
//      dict = dict?: @{};
//      success(@[dict]);
//    }
//  }];
//}
//
//RCT_EXPORT_METHOD(analyticsOnlineConfigParam:(NSString *) key successed:(RCTResponseSenderBlock) success) {
//  key = [RCTConvert NSString:key];
//  NSString *string = [AVAnalytics getConfigParams:key];
//  string = string ?: @"";
//  success(@[string]);
//}
//
//@end
