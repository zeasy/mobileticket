
/* JavaScript content from js/controller/moreOption.js in folder common */
(function(){
	jq.extendModule("mor.ticket.autoLoginChangePage", {
		"autoLoginChangePageFlag" : "",
	});
	function moreOptionRegistFn(){
		jq("#registOption").off();
		jq("#registView").remove();
		mor.ticket.registInfo=[];
		if (!busy.isVisible()) {
			busy.show();
		}
		mor.ticket.util.changePage("regist.html");
		setTimeout(function(){jq("#registOption").off().on("tap",moreOptionRegistFn);},1000);
		return false;
	}
	jq("#moreOptionView").live("pageinit", function(){
		mor.ticket.viewControl.tab1_cur_page = "moreOptionView";
		registerAdsBtnLisener();
		registerUpdateData();
		registerBtnListener();
		jq("#registOption").off().on("tap",moreOptionRegistFn);
		
		jq("#loginDoOption").off().bind("tap",function(){
			//busy.show();
			mor.ticket.viewControl.session_out_page="";
			mor.ticket.util.changePage(getViewBasePath() + "views/loginTicket.html");
			return false;
		});	
	});
	function moreOptionFn(){
		var user = mor.ticket.loginUser;
		var name = user.realName;
		jq("#logoutOptionDesc").html('<span class="loginName">'+name+"</span>");
		jq("#logoutOption").show();
		jq("#moreOptionView .iscroll-wrapper").iscrollview("refresh");
	}
	jq("#moreOptionView").live("pagebeforeshow", function(){
		var user = mor.ticket.loginUser;
		jq("#moreOptionView .iscroll-wrapper").iscrollview("refresh");
		if (user.isAuthenticated === "Y") {
			moreOptionFn();
		}else {
			var latestTime = window.ticketStorage.getItem("pwdTime");
			if (window.ticketStorage.getItem("autologin") == "true"
				&& (!latestTime || (Date.now() - latestTime   < 7*24*3600*1000))
				&& ((user.username!="" && user.password!="" && user.password!="*")
				|| (jq("#usernameInput").val()!="" && jq("#usernameInput").val()!=undefined)
					&& jq("#passwordInput").val()!="" && jq("#passwordInput").val()!=undefined)) {
				mor.ticket.autoLoginChangePage.autoLoginChangePageFlag = "moreOptionView";
				AutoSendLoginRequest();
			}else{
				jq("#loginOption").show();
			}
		}
		setUpdateShow();
		// 旅行休闲是否启用
		var isAdverStart = window.ticketStorage.getItem("isAdverStart");
		var payFinishBlock1 = window.ticketStorage.getItem("payFinishBlock1");
		var payFinishBlock2 = window.ticketStorage.getItem("payFinishBlock2");
		if(isAdverStart == "Y"&& payFinishBlock1=="taxiServiceFlagCss"&& payFinishBlock2=="hotelServiceFlagCss"){
			jq("#adsview").addClass("a_btn departshortline arrow-right");
			jq("#Hotelview").addClass("a_btn departshortline arrow-right");
			jq("#Taxiview").addClass("a_btn departshortnoline arrow-right");
		}else if(isAdverStart != "Y"&& payFinishBlock1=="taxiServiceFlagCss"&& payFinishBlock2=="hotelServiceFlagCss"){
			jq("#adsview").hide();
			jq("#Hotelview").addClass("a_btn departshortline arrow-right");
			jq("#Taxiview").addClass("a_btn departshortnoline arrow-right");
		}else if(isAdverStart != "Y"&& payFinishBlock1 !="taxiServiceFlagCss"&& payFinishBlock2=="hotelServiceFlagCss"){
			jq("#adsview").hide();
			jq("#Taxiview").hide();
			jq("#Hotelview").addClass("a_btn departshortnoline arrow-right");
		}else if(isAdverStart != "Y"&& payFinishBlock1 =="taxiServiceFlagCss"&& payFinishBlock2!="hotelServiceFlagCss"){
			jq("#adsview").hide();
			jq("#Hotelview").hide();
			jq("#Taxiview").addClass("a_btn departshortnoline arrow-right");
		}else if(isAdverStart == "Y"&& payFinishBlock1 !="taxiServiceFlagCss"&& payFinishBlock2!="hotelServiceFlagCss"){
			jq("#Taxiview").hide();
			jq("#Hotelview").hide();
			jq("#adsview").addClass("a_btn departshortnoline arrow-right");
		}else if(isAdverStart == "Y"&& payFinishBlock1 =="taxiServiceFlagCss"&& payFinishBlock2!="hotelServiceFlagCss"){
			jq("#Hotelview").hide();
			jq("#adsview").addClass("a_btn departshortline arrow-right");
			jq("#Taxiview").addClass("a_btn departshortnoline arrow-right");
		}else if(isAdverStart == "Y"&& payFinishBlock1 !="taxiServiceFlagCss"&& payFinishBlock2 =="hotelServiceFlagCss"){
			jq("#Taxiview").hide();
			jq("#adsview").addClass("a_btn departshortline arrow-right");
			jq("#Hotelview").addClass("a_btn departshortnoline arrow-right");
		}else{
			jq("#marketDiv").hide();
		}
		initAppVersion();
	});
	function registerBtnListener(){
		jq("#logoutOptionDesc").live("tap",function(){
			mor.ticket.util.changePage("my12306.html");
			return false;
		});
		jq("#more_trainQuery").bind("tap",function(){
			mor.ticket.util.changePage("trainQuery.html");
			return false;
		});
		jq("#more_ticketPriceQuery").bind("tap",function(){
			mor.ticket.util.changePage("ticketPriceQuery.html");
			return false;
		});
		jq("#more_agencySellTicketQuery").bind("tap",function(){
			mor.ticket.util.changePage("agencySellTicketQuery.html");
			return false;
		});
		jq("#more_stationTrainQuery").bind("tap",function(){
			mor.ticket.util.changePage("stationTrainQuery.html");
			return false;
		});
		jq("#more_startSellTicketTimeQuery").bind("tap",function(){
			mor.ticket.util.changePage("startSellTicketTimeQuery.html");
			return false;
		});
		jq("#more_settings").bind("tap",function(){
			mor.ticket.util.changePage("settings.html");
			return false;
		});
		jq("#aboutId").bind("tap",function(){
			mor.ticket.util.changePage("about.html");
			return false;
		});
	}
	function setUpdateShow(){
		mor.ticket.util.initAppVersionInfo();
		var university_update = window.ticketStorage.getItem("university_update");
		var city_update = window.ticketStorage.getItem("city_update");
		var kuaidi_update = window.ticketStorage.getItem("expressAddress_update");
		if((university_update == "true" || university_update == true)
				|| (city_update == "true" || city_update == true)
				|| (kuaidi_update == "true" || kuaidi_update == true)){
			jq("#dataUpdate").show();
		}else{
			jq("#dataUpdate").hide();
		}
		jq("#moreOptionView .iscroll-wrapper").iscrollview("refresh");
	}
	
	function registerAdsBtnLisener(){
		jq("#adsview").off().bind("tap", showAdsView);
		jq("#Taxiview").off().bind("tap", showTaxiView);
		jq("#TaoBaoview").off().bind("tap", showTaoBaoView);
		jq("#Hotelview").off().bind("tap", showHotelView);
	};
	
	function showAdsView(){
		var passengerTrainList = mor.ticket.passengerTrainlList.passengerTrainList;
		var adver_new = window.ticketStorage.getItem("adver_new");
		if(adver_new == "true" || adver_new == true){
			var version_no = window.ticketStorage.getItem("adver_version_no");
			window.ticketStorage.setItem("adver_version",version_no);
			window.ticketStorage.setItem("adver_new",false);
			jq("#adsview .newVersion").hide();
		}
		window.plugins.childBrowser.showAdsView(JSON.stringify(passengerTrainList), "", {});
	}
	function showTaoBaoView(){
		window.plugins.childBrowser.showTaoBaoView("http://h5.m.taobao.com/trip/groupbuy/grouplist.html?from=12306&ttid=703497@travel_h5_4.0&showdownloader=true", "淘宝旅行", {}); 
	}
	function showTaxiView(){
		var taxi_new = window.ticketStorage.getItem("taxi_new");
		var phone = window.btoa(mor.ticket.loginUser.mobile_no);
		if(taxi_new == "true" || taxi_new == true){
			window.ticketStorage.setItem("taxi_new",false);
			jq("#Taxiview .newVersion").hide();
		}
		if(mor.ticket.loginUser.isAuthenticated == "Y"){
			//if(mor.ticket.util.isAndroid()){
				window.plugins.childBrowser.showTpcWebView(encodeURI("http://huodong.yongche.com/app/12306.html"), "接送站服务", {});
			//}else{
			//	window.plugins.childBrowser.showAdsView("",encodeURI("http://sandbox.yongche.org/bd/12306.html"),{}); 
			//}		
		}else{
		//	if(mor.ticket.util.isAndroid()){
				window.plugins.childBrowser.showTpcWebView("http://huodong.yongche.com/app/12306.html", "接送站服务", {}); 	
		//	}else{
		//		window.plugins.childBrowser.showAdsView("","http://sandbox.yongche.org/bd/12306.html",{});
		//	}
			
		}	
	}
	function showHotelView(){
		var hotel_new = window.ticketStorage.getItem("hotel_new");
		if(hotel_new == "true" || hotel_new == true){
			window.ticketStorage.setItem("hotel_new",false);
			jq("#Hotelview .newVersion").hide();
		}
		if(mor.ticket.util.isAndroid()){
			window.plugins.childBrowser.showTpcWebView("http://m.ctrip.com/webapp/mkt/partner-360/?allianceid=110185&sid=554014", "携程特惠酒店", {}); 
		}else{
			window.plugins.childBrowser.showAdsView("","http://m.ctrip.com/webapp/mkt/partner-360?allianceid=110185&sid=554014",{}); 
		}	
	}
	function registerUpdateData(){
		jq("#dataUpdate").live("tap","a",function(e){
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			var university_update = window.ticketStorage.getItem("university_update");
			var city_update = window.ticketStorage.getItem("city_update");
			var kuaidi_update = window.ticketStorage.getItem("expressAddress_update");
			
			if(university_update == "true" || university_update == true){
				updateUniversity();
			}else if(city_update == "true" || city_update == true){
				updateCity();
			}else if(kuaidi_update == "true" || kuaidi_update == true){
				updateExpress();
			}
		});
	}
	
	function updateUniversity(){
		var university_version = window.ticketStorage.getItem("university_version") == null ? 
				"" : window.ticketStorage.getItem("university_version");
		requestUpdateJsonstore("university",university_version);
		
		var city_update = window.ticketStorage.getItem("city_update");
		var kuaidi_update = window.ticketStorage.getItem("expressAddress_update");
		if(city_update == "true" || city_update == true){
			setTimeout(updateCity, 1000);
		}else if(kuaidi_update == "true" || kuaidi_update == true){
			setTimeout(updateExpress, 1000);
		}
	}
	
	function updateCity(){
		var city_version = window.ticketStorage.getItem("city_version") == null ? 
			"" : window.ticketStorage.getItem("city_version");
		requestUpdateJsonstore("city",city_version);
		var kuaidi_update = window.ticketStorage.getItem("expressAddress_update");
		if(kuaidi_update == "true" || kuaidi_update == true){
			setTimeout(updateExpress, 1000);
		}
	}
	function updateExpress(){
			if(!busy.isVisible()){
				busy.show();
			}
			var util = mor.ticket.util;
			var commonParameters = {
					'version_no' : ""
				};
			var invocationData = {
					adapter : mor.ticket.viewControl.adapterUsed,
					procedure : "getExpressAddressCache"
				};
			var options = {
				onSuccess : syncKuaiDiSucceeded,
				onFailure : util.creatCommonRequestFailureHandler()
			};
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,options);
			return false;
	}
	function KuaiDiProvinceCityContryTownStreetCache(provinceList,mapCountryList,mapTownList,mapCityList,mapStreetList){
		this.provinceList = provinceList;
		this.mapCountryList = mapCountryList;
		this.mapTownList = mapTownList;
		this.mapCityList = mapCityList;
		this.mapStreetList = mapStreetList;
	}
	
	function syncKuaiDiSucceeded(result){
		if (result.invocationResult.provinceList && result.invocationResult.mapCityList 
				&& result.invocationResult.mapCountryList && result.invocationResult.mapStreetList
				&& result.invocationResult.mapTownList) {
			if(busy.isVisible()){
				busy.hide();
			}
			var expressAddress = new KuaiDiProvinceCityContryTownStreetCache();
			expressAddress.provinceList = result.invocationResult.provinceList;
			expressAddress.mapCountryList = result.invocationResult.mapCountryList;
			expressAddress.mapTownList = result.invocationResult.mapTownList;
			expressAddress.mapCityList = result.invocationResult.mapCityList;
			expressAddress.mapStreetList = result.invocationResult.mapStreetList;
			var expressAddress_version = result.invocationResult.version_no;
			try {
				var query = {};
				var options = {push:false};
				WL.JSONStore.get("expressAddress").remove(query,options)
				.then(function(){
					updateKuaiDiData(expressAddress,expressAddress_version);
				})
				.fail(function(errorObject){
					WL.Logger.info(errorObject.msg);
				});
			}catch (e) {
				WL.Logger.error(e.message);
			}	
		}
		function updateKuaiDiData(expressAddress,expressAddress_version){
			WL.JSONStore.get("expressAddress").add(expressAddress)
			.then(function () {
				window.ticketStorage.setItem("expressAddress_version",expressAddress_version);
				window.ticketStorage.setItem("expressAddress_update",false);
				setUpdateShow();
				/*WL.SimpleDialog.show("温馨提示", 
						"基础数据更新已完成", [
						{
							text : "确定",
							handler : function() {
								setUpdateShow();
							}
						}
						]);*/
			})
			.fail(function (errorObject) {
				WL.Logger.info(errorObject.msg);
				setUpdateShow();
				/*WL.SimpleDialog.show("温馨提示", 
						"基础数据更新失败", [
						{
							text : "确定",
							handler : function() {
								setUpdateShow();
							}
						}
						]);*/
			});
			
		}
	}
	
	function requestUpdateJsonstore(collectionName,version_no){
		if(!busy.isVisible()){
			busy.show();
		}
		var util = mor.ticket.util;
		var commonParameters = {
				'syncList' : collectionName,
				'syncVersionList' : version_no
			};
			var invocationData = {
				adapter : mor.ticket.viewControl.adapterUsed,
				procedure : "syncCache"
			};
			var options = {
				onSuccess : syncSucceeded,
				onFailure : util.creatCommonRequestFailureHandler()
			};
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,
					options, true);
	}
	
	function syncSucceeded(result){
		if (result.invocationResult.university) {
			if(busy.isVisible()){
				busy.hide();
			}
			var university = result.invocationResult.university;
			var version_no = result.invocationResult.university_v;
			try {
				for ( var i = 0; i < university.length; i++) {
					jq("#py").val(university[i].university_name);
					var pinyin = jq("#py").toPinyin().toLowerCase();
					university[i].pinyin=pinyin;					
				}
				var query = {};
				var options = {push:false};
				WL.JSONStore.get("university").remove(query,options)
				.then(function(){
					updateUniversityData(university,version_no);
				})
				.fail(function(errorObject){
					WL.Logger.info(errorObject.msg);
				});
			}catch (e) {
				WL.Logger.error(e.message);
			}	
				
		}
		
		function updateUniversityData(university,version_no){
			WL.JSONStore.get("university").add(university)
			.then(function () {
				window.ticketStorage.setItem("university_version",version_no);
				window.ticketStorage.setItem("university_update",false);
				setUpdateShow();
				/*WL.SimpleDialog.show("温馨提示", 
						"基础数据更新已完成", [
						{
							text : "确定",
							handler : function() {
								setUpdateShow();
							}
						}
						]);*/
			})
			.fail(function (errorObject) {
				WL.Logger.info(errorObject.msg);
				setUpdateShow();
				/*WL.SimpleDialog.show("温馨提示", 
						"基础数据更新失败", [
						{
							text : "确定",
							handler : function() {
								setUpdateShow();
							}
						}
						]);*/
			});	
		}
		if (result.invocationResult.city) {
			if(busy.isVisible()){
				busy.hide();
			}
			var city = result.invocationResult.city;
			var city_version_no = result.invocationResult.city_v;
			try {
				for ( var i = 0; i < city.length; i++) {
					jq("#py").val(city[i].city_name);
					var pinyin = jq("#py").toPinyin().toLowerCase();
					city[i].pinyin=pinyin;	
					city[i].valueSM = generatePinYing(city[i].city_name);
				}
				var query = {};
				var options = {push:false};
				WL.JSONStore.get("university").remove(query,options)
				.then(function(){
					updateCityData(city,city_version_no);
				})
				.fail(function(errorObject){
					WL.Logger.info(errorObject.msg);
				});
			}catch (e) {
				WL.Logger.error(e.message);
			}	
		}
		function updateCityData(city,city_version_no){
			WL.JSONStore.get("city").add(city)
			.then(function () {
				window.ticketStorage.setItem("city_version",city_version_no);
				window.ticketStorage.setItem("city_update",false);
				setUpdateShow();
				/*WL.SimpleDialog.show("温馨提示", 
						"基础数据更新已完成", [
						{
							text : "确定",
							handler : function() {
								setUpdateShow();
							}
						}
						]);*/
			})
			.fail(function (errorObject) {
				WL.Logger.info(errorObject.msg);
				setUpdateShow();
				/*WL.SimpleDialog.show("温馨提示", 
						"基础数据更新失败", [
						{
							text : "确定",
							handler : function() {
								setUpdateShow();
							}
						}
						]);*/
			});
			
		}
	}
	
	function generatePinYing(input) {
		var reg = /^[A-Z]+/g;
		jq("#py").val(input);
		var pingyin = jq("#py").toPinyin();
		var sm = '';
		for ( var index = 0; index < pingyin.length; index++) {
			if (pingyin.charAt(index).match(reg)) {
				sm += pingyin.charAt(index);
			}
		}
		return sm;
	}
	
	function initAppVersion(){
		var common = mor.ticket.common;
		var noReadNum = 0;
		var count = window.ticketStorage.pushmessageNum();
		for(var i = 0; i < count ; i++){
			var pushmessage = eval("(" + window.ticketStorage.getItem("pushmessage"+i) + ")");
			if(pushmessage.readOrNo === false){
				noReadNum++;
			}
			if(noReadNum > 0 ){
				break;
			}
		}
		var adver_new = window.ticketStorage.getItem("adver_new");
		var taxi_new = window.ticketStorage.getItem("taxi_new");
		var hotel_new = window.ticketStorage.getItem("hotel_new");
		if(adver_new == "true" || adver_new == true){
			jq("#adsview .newVersion").show();
		} else {
			jq("#adsview .newVersion").hide();
		}
		if(taxi_new == "true" || taxi_new == true){
			jq("#Taxiview .newVersion").show();
		} else {
			jq("#Taxiview .newVersion").hide();
		}
		if(hotel_new == "true" || hotel_new == true){
			jq("#Hotelview .newVersion").show();
		} else {
			jq("#Hotelview .newVersion").hide();
		}
		if(common["baseDTO.app_need_update"] == "true" || common["baseDTO.app_need_update"] == true || noReadNum > 0){
			jq("#aboutId .newVersion").show();
		}else{
			jq("#aboutId .newVersion").hide();
		}
	}
})();