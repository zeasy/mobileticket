/**
 * @providesModule ZZNetwork
 * Created by easy on 16/1/13.
 */


'use strict';
let Async = require('ZZAsync');
let NativeNetwork = require('NativeModules').NativeNetwork;

class ZZNetwork {
    static defaultHeaders() {
        return {

            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36',


        };
    }

    static async request(options,uuid) {
        return Async.invoke(NativeNetwork.request,options,uuid+'');
    }

    static async cancel(uuid) {
        return Async.invoke(NativeNetwork.cancel,uuid+'');
    }

    static async get(url,params,uuid) {
        return ZZNetwork.request({url,params,method:'GET',format:'JSON',timeout:30000,headers:ZZNetwork.defaultHeaders()},uuid);
    }

    static async post(url,params,uuid) {
        return ZZNetwork.request({url,params,method:'POST',format:'JSON',timeout:30000,headers:ZZNetwork.defaultHeaders()},uuid);
    }
}

module.exports = ZZNetwork;