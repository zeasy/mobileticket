//
//  ZZTableViewHeaderFooterView.m
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZTableViewHeaderFooterView.h"


@interface ZZTableViewHeaderFooterView ()

@end

@implementation ZZTableViewHeaderFooterView


- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher
{
	self = [super init];
	if (self) {
	}
	return self;
}


@end


@implementation _ZZTableViewHeaderFooterView

-(void)setHeaderFooterView:(ZZTableViewHeaderFooterView *)headerFooterView {
	if (_headerFooterView != headerFooterView) {
		[self.contentView addSubview:headerFooterView];
		[_headerFooterView removeFromSuperview];
			_headerFooterView = headerFooterView;
		_headerFooterView.frame = self.contentView.bounds;
		[self setNeedsLayout];
	}
}

-(void)layoutSubviews {
	[super layoutSubviews];
	_headerFooterView.frame = self.contentView.bounds;
}


@end