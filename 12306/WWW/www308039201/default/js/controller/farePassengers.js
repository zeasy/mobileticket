
/* JavaScript content from js/controller/farePassengers.js in folder common */
(function(){
	// var changeSeatTypeIndex = 0;
	/*    
	 * jq("#passengersView").live("pagecreate", function() {
	 * //mor.ticket.util.androidRemoveIscroll("#passengersView"); });
	 */
	var chooseSeatType = 0;
	var prevId;
	var focusArray=[];
	var canSubmit = true;
	var canotSubmitMessage = "本次列车您选择的席别尚有余票0张，请您选择其他席别或车次。";
	var tapHolding = false;
	//查询停靠站缓存标志,0--后台查询;1--直接显示缓存
	var queryStopStationFlag = "0";
	var util = mor.ticket.util;
	jq("#passengersView").live("pageshow", function() {
		queryStopStationFlag = "0";
		mor.ticket.util.initAppVersionInfo();
		focusArray=[];
		if(prevId == "datePickerView"){
			jq("#passengersChooseDateBtn").change();
		}
		if(prevId == "reportTicketView"){
			//mor.ticket.orderManager.refreshCaptcha2();
			mor.ticket.orderManager.refreshCaptchaImg2();
		}
		if(prevId === "loginView"){
			mor.ticket.orderManager.refreshCaptchaImg2();
		}
		if(prevId !== "searchSingleTicketResultsView" && prevId !== "reportTicketView" && prevId !== "loginView"){
			if(mor.ticket.orderManager.capchaNewOrOldFlag == "2"){
				jq("#captchaDivOld").show();
				jq("#captchaDivNew").hide();
				jq("#refreshImgBtn").hide();
				jq("#captchaImg1").hide();
			}else if(mor.ticket.orderManager.capchaNewOrOldFlag == "1"){
				jq("#captchaDivOld").hide();
				jq("#captchaDivNew").show();
				jq("#refreshImgBtn").show();
				jq("#captchaImg1").show();
			}else{
				jq("#captchaDivOld").hide();
				jq("#captchaDivNew").hide();
				jq("#refreshImgBtn").hide();
				jq("#captchaImg1").hide();
			}
		}
	});
	function registerAutoScroll(){
		util.enableAutoScroll('#captchaInput',focusArray);
		if(util.isIPhone()){
			util.enableAutoScroll('.changeSeatType',focusArray);
			util.enableAutoScroll('.changeyplistType',focusArray);
		}
	}

	jq("#passengersView").live("pagehide", function(e, data){
		jq("#passengersView").css("pointer-events","auto");
		var intervalId = mor.ticket.poll.intervalId;
		if(intervalId != ""){
			mor.ticket.poll.timer.stop();
			clearInterval(mor.ticket.poll.intervalId);
		}
		if(mor.ticket.viewControl.bookMode == "fc"){
			if(data.nextPage.attr("id") != "searchSingleTicketResultsView" && data.nextPage.attr("id") != "orderDetailsView"
				&& data.nextPage.attr("id") != "datePickerView"){
				mor.ticket.viewControl.bookMode = "dc";
				mor.ticket.viewControl.tab1_cur_page=vPathViewCallBack()+"MobileTicket.html";
			}
		}
	});
	jq("#passengersView").live("pageinit", function(){
		if(mor.ticket.util.isAndroid()&& (parseFloat(device.version) > 3.0)){
		   	jq("#passengersView .iscroll-wrapper").addClass("smoothSlide");
		}
		registerDatePicker();
		ticketDeletePassengerBtnListeners();
		ticketAddPassengerBtnListeners();
		registerFormChangeListeners();
		registerPassengersButtonsListener();
		registerAutoScroll();
		registershowAllpassengerInfoTipsListeners();
		registerPrevDateBtnClickHandler();
		registerNextDateBtnClickHandler();
		registSelectDateChangeHandler();
		initDateBtn();
	});
	function registerDatePicker(){
		jq("#passengersChooseDateInputId").on("tap",function(){
			var reservePeriod;
			var str = "passengers.html";
			mor.ticket.currentPagePath.fromPath = str;
			var goingPath = vPathCallBack()+ "datePicker.html";
			mor.ticket.currentPagePath.type = true;
			mor.ticket.currentPagePath.show = false;
			// 判断勾选的乘客
			var mode = mor.ticket.viewControl.bookMode;
			if(mode == "fc" || mode == "gc"){
				var passengers = getPassengerList();
				for(var i = 0;i<passengers.length;i++){
					var people = jq(".car-peop-list:eq("+i+")");
					if (!people.find("input[type='checkbox']").attr("checked")){
						passengers[i].checked = "false";
					}
				}
				
			}
			//重设日期，刷新控件
			var monthValue;
			var current = mor.ticket.currentPagePath;
			mor.ticket.currentTicket.old_train_date = jq("#passengersChooseDateBtn").val();
			current.defaultDateValue[0]=jq("#passengersChooseDateBtn").val().slice(0,4);
			monthValue=parseInt(jq("#passengersChooseDateBtn").val().slice(5,7));
			if(monthValue == "1"){
				monthValue = "0";
			}else monthValue = monthValue-1;
			if(monthValue < 10){
				current.defaultDateValue[1] = "0"+monthValue;
			}else{
				current.defaultDateValue[1] = monthValue;
			}
			current.defaultDateValue[2]=jq("#passengersChooseDateBtn").val().slice(8,10);
			reservePeriod = mor.ticket.history.reservePeriod;
			/*if(mor.ticket.viewControl.bookMode === "gc"){
				if(mor.ticket.leftTicketQuery.isTwoday == true){
					reservePeriod = mor.ticket.leftTicketQuery.dayLimit;
				}
			}*/
			jQuery.extend(jQuery.mobile.datebox.prototype.options, {
				mode:"calbox",
				useInline: true,
				calControlGroup: true,
				defaultValue: current.defaultDateValue,
				showInitialValue: true,
				lockInput: false,
				useFocus: true,
				maxDays: reservePeriod,
				buttonIcon: "grid",
				calUsePickers: true,
				afterToday: true,
				beforeToday: false,
				notToday: false,
				calOnlyMonth: false,
				overrideSlideFieldOrder: ['y','m','d','h','i']
			});
			jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
			mor.ticket.util.changePage(goingPath);
			jq(".ui-datebox-container .ui-btn-b").css({"background-color":"white","border-color":"white"});
		});
	}
	function farePsgFn(e,data){
		var showFlags = false;
		var DcShowFlags = false;
		var ticket = mor.ticket.currentTicket;
		// 提前预处理。
		processSeatTypeInCurrentTicket(ticket);
		var query = mor.ticket.leftTicketQuery;
		var cache = mor.ticket.cache;
		
		regsiterYpListFunHandher();
		mor.ticket.viewControl.tab1_cur_page="passengers.html";
		idTypeControl();
		jq("#bookTicketModifyPassengerDiv").hide();
		jq("#showAllpassengerInfoTips").show();
		jq("#passengerInfoTips2").hide();
		if(showFlags==false){
			jq("#preferentialAgreeInfo").hide();	
		}
		var mode = mor.ticket.viewControl.bookMode;
		if(data.prevPage.attr("id") === "searchSingleTicketResultsView"){
			//mor.ticket.orderManager.refreshCaptcha2();
			mor.ticket.orderManager.refreshCaptchaImg2();
			mor.ticket.history.inputyzm='';
			mor.ticket.currentPassenger = {};
			// mor.ticket.passengerList=[];
			mor.ticket.util.contentIscrollTo(0,0,0);
		}
		var default_seat_type = ticket.getDefaultSeatType();
		var str="单程:";
		if(mode === "fc") {
			str="返程:";
			showPassengersFunHandher('fc');
			updateButtonState();
			jq("#passengersView .ui-header>h1").text("确认订单(返程)");
			jq("#choosePassenger").hide();
			jq(".deletePassenger").hide();
			jq(".addContact").hide();
			jq(".changeTicket").hide();
			jq('#TicketAddPassengerBtn').parent().hide();
			if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
				ticket.train_date = mor.ticket.currentPagePath.calenderDate;
			}
			jq("#passengersChooseDateBtn").val(ticket.train_date);
			// jq(".changeSeatType").show();
			// 动卧优惠是否启用
			var isDwUseFlag = window.ticketStorage.getItem("isDwUseFlag");
			if(isDwUseFlag =="Y" && mor.ticket.DcSeatTypeList.result.length != 0){
				for(var i=0;i<mor.ticket.DcSeatTypeList.result.length;i++){
					var seat_code = mor.ticket.DcSeatTypeList.result[i];
					if(seat_code != "A" && seat_code != "F"){
						DcShowFlags = true;
						break;
					}
				}
			    for(var a=0;a<ticket.seat_types.length;a++){
					if(ticket.seat_types[a]=="F" || ticket.seat_types[a]=="A"){
						showFlags = true;
						break;
					}
				}
				
				if(showFlags == true && DcShowFlags==false){
					jq("#preferentialAgreeInfo").show();	
				}else{
					jq("#preferentialAgreeInfo").hide();
				}
			}else{
				jq("#preferentialAgreeInfo").hide();
			}		
			jq(".bookBackTicket").show();
		}else if(mode === "gc"){
			if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
				str = "变更到站";
			}if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "改签"){
				str = "改签";
			}
			jq("#passengersView .ui-header>h1").text("确认订单("+str+")");
			// 获取到改签车票的坐席编号
			showPassengersFunHandher('gc');
			updateButtonState();
			jq("#choosePassenger").hide();
			jq(".deletePassenger").hide();
			jq(".addContact").hide();
			jq(".bookBackTicket").hide();
			if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
				ticket.train_date = mor.ticket.currentPagePath.calenderDate;
			}
			jq("#passengersChooseDateBtn").val(ticket.train_date);
			// jq(".changeSeatType").show();
			jq(".changeTicket").show();
			jq("#submitOrderBtn").parent().hide();
			jq("#submitChangeTicketBtn").parent().show();
			jq('#TicketAddPassengerBtn').parent().hide();
		}else {
			var currentPassenger=mor.ticket.currentPassenger;
			currentPassenger.seat_type = default_seat_type;
			jq("#passengersView .ui-header>h1").text("确认订单");
			updateForm(currentPassenger);
			if(default_seat_type == "A" || default_seat_type == "F"){
				jq("#passengerInfoTipsForAF").show();
			}else{
				jq("#passengerInfoTipsForAF").hide();
			}
			jq("#choosePassenger").show();
			jq(".deletePassenger").show();
			jq(".addContact").show();
			if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
				ticket.train_date = mor.ticket.currentPagePath.calenderDate;
			}
			if(prevId =="travelPlanDetailView"){
				jq("#passengersChooseDateBtn").val(mor.ticket.currentPagePath.calenderDate);
			}else{
				jq("#passengersChooseDateBtn").val(ticket.train_date);
			}
			jq(".changeTicket").hide();
			// jq(".changeSeatType").hide();
			jq(".bookBackTicket").hide();
			jq("#submitOrderBtn").parent().show();
			jq("#submitChangeTicketBtn").parent().hide();
			jq('#TicketAddPassengerBtn').parent().show();

			showPassengersFunHandher();
			updateButtonState();
		}
		
		if(query.purpose_codes==="0X"){
			jq("#passengersView .ui-header>h1").append("(学生)");
		}else if(query.purpose_codes==="1F"){
			jq("#passengersView .ui-header>h1").append("(农民工)");
		}
		// mjl fix席别选不中
		if(mor.ticket.util.isIPhone()){
			mor.ticket.util.enableAutoScroll("#passengersView select.changeSeatType",focusArray);
		}
		// init prompt list
		var util = mor.ticket.util;
		/*初始化日期控件数据*/
		var reservePeriod = util.getReservedPeriod(query.purpose_codes);
		mor.ticket.history.reservePeriod = reservePeriod;
        jq("#pfromStationName").html(cache.getStationNameByCode(ticket.from_station_telecode));
		jq("#ptrainStartTime").html(ticket.start_time + " 出发");
		jq("#ptrainCodeName").html(ticket.station_train_code);
		jq("#ptrainDurationTime").html(util.getLiShiStr(ticket.lishi));
		if(ticket.is_support_card === "1"){
			jq("#el_ticket_div").show();
		}else{
			jq("#el_ticket_div").hide();
		}
		jq("#ptoStationName").html(cache.getStationNameByCode(ticket.to_station_telecode));
		jq("#ptrainReachTime").html(ticket.arrive_time + " 到达");
		if(ticket.start_station_telecode && ticket.from_station_telecode != ticket.start_station_telecode){
			jq("#pstartStationName").css("display","inline-block");
			jq("#innerStartStation").html(cache.getStationNameByCode(ticket.start_station_telecode));
		}else{
			jq("#pstartStationName").css("display","none");
		}
		
		if(ticket.end_station_telecode && ticket.to_station_telecode != ticket.end_station_telecode){
			jq("#pendStationName").css("display","inline-block");
			jq("#innerEndStation").html(cache.getStationNameByCode(ticket.end_station_telecode));
		}else{
			jq("#pendStationName").css("display","none");
		}
		
		if(mor.ticket.history.inputyzm && mor.ticket.history.inputyzm!==''&&mor.ticket.orderManager.capchaNewOrOldFlag == "2"){
			jq('#captchaInput').val(mor.ticket.history.inputyzm);
		}
		//mod by yiguo
		ticketEditSeatBtnListeners();
		if(!canSubmit){
			jq("#submitOrderBtn").addClass("ui-disabled");
		}else if(jq("#submitOrderBtn").hasClass("ui-disabled")){
			jq("#submitOrderBtn").removeClass("ui-disabled");
		}
		 
		if((prevId == "searchSingleTicketResultsView" || prevId == "loginView") 
			&& mode === "dc" && mor.ticket.passengerList.length == 0){
			setTimeout(function(){
				jq("#TicketAddPassengerBtn").tap();
			},100);
		}
	}
	jq("#passengersView").live("pagebeforeshow", function(e,data){
		jq("#captchaDivNew").hide();
		jq("#refreshImgBtn").hide();
		jq("#captchaImg1").hide();
		jq("#captchaDivOld").hide();
		//图片验证
		if(mor.ticket.orderManager.capchaNewOrOldFlag !== "2"){
			jq("#captchaImg1").on("click",mor.ticket.orderManager.captchaTabHandle);
			mor.ticket.orderManager.captchaImgInit();
		}
		
		prevId = data.prevPage.attr("id");
		//add by yiguo
		mor.ticket.viewControl.unFinishedOrderTourFlag = "";
		var util = mor.ticket.util;
		// 此事件用于改签与返程选择联系人时触发
		var mode = mor.ticket.viewControl.bookMode;
		if(mode == "fc" || mode == "gc"){
			registerSelectPassengerListener();
		}
		jq('#passengersView .iscroll-wrapper').iscrollview('refresh');
		if(mor.ticket.loginUser.isAuthenticated === "Y"){
			farePsgFn(e,data);
			}else{
				util.keepPageURL();
				var ticket = mor.ticket.currentTicket;
				// 提前预处理。
				processSeatTypeInCurrentTicket(ticket);
				var cache = mor.ticket.cache;
				jq("#pfromStationName").html(cache.getStationNameByCode(ticket.from_station_telecode));
				jq("#ptrainStartTime").html(ticket.start_time + " 出发");
				jq("#ptrainCodeName").html(ticket.station_train_code);
				jq("#ptrainDurationTime").html(util.getLiShiStr(ticket.lishi));
				jq("#ptoStationName").html(cache.getStationNameByCode(ticket.to_station_telecode));
				jq("#ptrainReachTime").html(ticket.arrive_time + " 到达");
				if(ticket.start_station_telecode && ticket.from_station_telecode != ticket.start_station_telecode){
					jq("#pstartStationName").css("display","inline-block");
					jq("#innerStartStation").html(cache.getStationNameByCode(ticket.start_station_telecode));
				}else{
					jq("#pstartStationName").css("display","none");
				}
				
				if(ticket.end_station_telecode && ticket.to_station_telecode != ticket.end_station_telecode){
					jq("#pendStationName").css("display","inline-block");
					jq("#innerEndStation").html(cache.getStationNameByCode(ticket.end_station_telecode));
				}else{
					jq("#pendStationName").css("display","none");
				}
				if (window.ticketStorage.getItem("autologin") != "true") {
					autologinFailJump();
			} else {
				registerAutoLoginHandler(function(){farePsgFn(e,data);}, autologinFailJump);
			}
		}
	});

	// 购票用户身份控制
	function idTypeControl(){
		if(mor.ticket.loginUser.id_type == "1" || mor.ticket.loginUser.id_type == "2"){
			jq("#idTypeSelect").html('<option value="1" selected>二代身份证</option>'+
          		'<option value="2">一代身份证</option>'+
         		'<option value="C">港澳通行证</option>'+
          		'<option value="G">台湾通行证</option>'+
          		'<option value="B">护照</option>');
			    //'<option value="H">外国人居留证</option>');
		}else{
			jq("#idTypeSelect").html('<option value="C" selected>港澳通行证</option>'+
	          		'<option value="G">台湾通行证</option>'+
	          		'<option value="B">护照</option>');
					//'<option value="H">外国人居留证</option>');
		}

	}

	function getPassengerList(type) {
		type = type || mor.ticket.viewControl.bookMode;
		var ticket = mor.ticket.currentTicket;

		if (type === 'gc') {
			return ticket.gcPassengerList || (ticket.gcPassengerList = mor.ticket.queryOrder.getGcPassengers());
		}

		if (type === 'fc') {
			return ticket.fcPassengerList || (ticket.fcPassengerList = mor.ticket.orderManager.getFcPassengers());
		}
//		if(mor.ticket.passengerList.length>0){
//			if(chooseSeatType != 1 ){
//				for(var a=0;a<mor.ticket.passengerList.length;a++){
//					mor.ticket.passengerList[a]['seat_type'] = ticket.default_seat_type;
//				}
//			}
//		}
		return mor.ticket.passengerList;
	}
	// 展示用户席位选择列表
	function showPassengersFunHandher(type){
		type = type || mor.ticket.viewControl.bookMode;
		var passengerList = getPassengerList(type);
		var ticket = mor.ticket.currentTicket;
	    var util = mor.ticket.util;
	    var temp = '';
	    jq.each(passengerList, function(i, passenger) {
			var ticketTypeName = util.getTicketTypeName(passenger.ticket_type);
			passenger.seat_type = ticket.getDefaultSeatType(passenger.seat_type);
			var seat_name = util.repalceYpInfoType(ticket.getTypeName(ticket.default_seat_type));
			temp+='<div class="car-peop-list" style="overflow:hidden" data-index="'+i+'">';
			temp+='<div class="car-peop-list-name text-ellipsis" style="width:70px">' + (passenger.user_name) + '</div>';
			if(passenger.id_type == "1"){
				temp+='<div class="car-peop-list-numb">' + (mor.ticket.util.getIdNoStar(passenger.id_no)) + '</div>';
			}else{
					temp+='<div class="car-peop-list-numb">' + (passenger.id_no) + '</div>';
			}
			if(chooseSeatType != 1){
			temp+= getPassengerSeatElem(type, seat_name);
		  }else{
			temp+= getPassengerSeatElem(type, util.repalceYpInfoType(ticket.getTypeName(passenger.seat_type)));
		}
			var displayStyle = ' style="filter: Alpha(Opacity=0);cursor: pointer;-webkit-appearance: none;opacity: 0;" ';
			var disableStyle = ' style="filter: Alpha(Opacity=0);-webkit-appearance: none;opacity: 0;" ';
			var seatTypes = ticket.seat_types;
			var xbDisabled = ' '; // 席别 disabled
			var xbStyle = displayStyle;
			var zwDisabled = ' '; // 座位 disabled
			var zwStyle = displayStyle;
			// 如果
			if (type == 'fc') {
				zwDisabled = ' disabled ';
				zwStyle = disableStyle;
			}

			if (type == 'gc') {
				xbDisabled = ' disabled ';
				zwDisabled = ' disabled ';
//				zwStyle = disableStyle;
//				xbStyle = disableStyle;
                zwStyle = xbStyle = ' style="display:none;"';
			}

			temp+='<select id="'+i+'" data-index="'+i+'"' + xbDisabled + 'class="changeyplistType" ' + xbStyle + ' name="席别">';
			jq.each(seatTypes, function(index, typeId) {
				temp += '<option ';
				if (passenger.seat_type == typeId) {
					temp += 'selected="selected"';
				}
				temp += ' value="'+ typeId +'">'+util.repalceYpInfoType(ticket.getTypeName(typeId))+'</option>';

			});
			temp+='</select>';
			
			temp+='</div>';
			temp+='<div class="car-peop-list-pz">';

			if (zwStyle == disableStyle) {
				temp += ticketTypeName;
			} else {
                var ticketTypeColor= (type == 'gc') ? ' style="color:#000;" ' :"";
				temp += '<span class="seat text-ellipsis" '+ ticketTypeColor +'>'+ticketTypeName+'</span>';
			}

			temp+='<select data-index="'+i+'"' + zwDisabled + 'class="changeSeatType" ' + zwStyle + ' name="车票类型">"';
			temp+='';
			
			//["1","成人票"],["2","儿童票"],["3","学生票"],["4","残军票"]
			var ticketTypeList = [];
			var ticketTypeList1 = ["1","成人票"];
			var ticketTypeList2 = ["2","儿童票"];
			var ticketTypeList3 = ["3","学生票"];
			var ticketTypeList4 = ["4","残军票"];
			ticketTypeList.push(ticketTypeList1);
			ticketTypeList.push(ticketTypeList2);
			ticketTypeList.push(ticketTypeList3);
			ticketTypeList.push(ticketTypeList4);

			if (passenger.ticket_type!=3){
				ticketTypeList.splice(2,1);
			}

			jq.each(ticketTypeList, function(index, ticketType) {
				temp += '<option ';
				if (passenger.ticket_type == ticketType[0]) {
					temp += 'selected="selected"'  ;
				}
				temp += ' value="' + ticketType[0]+'">' +ticketType[1] + '</option>';
			});

			temp += '</select>';
			temp += '</div>';

			if (type === 'fc') {
				temp+='<div class="car-peop-list-input" data-index="'+i+'" ><input id=\"list_'+i+'\" type=\"checkbox\"  class="bookBackTicketCheckBox" '; 
				if(!passenger.checked){
					temp+=' checked ';
				}
				temp+= 'value=\"'+i+'\"></div>';
			} else if (type === 'gc') {
				temp+='<div class="car-peop-list-input" style="position:relative;" data-index="'+i+'" >' +
                    '<label style="position: absolute;top:0;right:0;bottom:0;left:0;z-index: 10;" for=\"list_'+i+'\"></label>'+
                    '<input id=\"list_'+i+'\" type=\"checkbox\"  class="changeTicketCheckBox" ';
					if(!passenger.checked){
						temp+=' checked ';
					}
                    temp+=' value=\"'+i+'\"></div>';
			} else {
				temp+='<div class="car-peop-list-cz" data-index="'+i+'" ><a></a></div>';
			}

			temp+='</div>';
		});
		jq(".car-peop").html(temp);
		setTimeout(function(){refrechSelect();contentIscrollRefresh();},20);

		return;
	}
	// add by yiguo 用来在选择框生成后为其绑定相应的滚动功能，方式键盘呼出时被navbar被顶上去
	function refrechSelect(){
		// add by yiguo 2013.11.18
		if(util.isIPhone()){
			util.enableAutoScroll('.changeSeatType', focusArray);
			util.enableAutoScroll('.changeyplistType', focusArray);
		}
	}

	// 主要是用于改签的时候进行区分， 改签的时候座位不可改。
	function getPassengerSeatElem(type, seat_name) {
		var temp = '';
		temp+='<div class="car-peop-list-zx"><span class="seat text-ellipsis"';
		if (type === 'gc') {
			 temp += ' style="color:black;width:35px"';
		} else {
			temp += ' style="width:35px"';
		}
		return temp + '>' + seat_name +'</span>';;
	}
	// 删除
	function ticketDeletePassengerBtnListeners(){
		jq(".car-peop").off('tap.deletePassenger').on("tap.deletePassenger", ".car-peop-list-cz",function(e){
			var that = jq(this);
			var data_index = that.attr("data-index");
			if (data_index!=undefined ||  data_index!=''){
					if(mor.ticket.util.isAndroid()){
						WL.SimpleDialog.show("温馨提示", "确定删除该乘客吗?",
							[
							 {
								  text: "确认", handler: function(){	
									 for(var i=0;i<mor.ticket.passengersCache.passengers.length;i++){			
										if((mor.ticket.passengersCache.passengers[i].id_type==mor.ticket.passengerList[parseInt(data_index)].id_type) && 
													  (mor.ticket.passengersCache.passengers[i].id_no==mor.ticket.passengerList[parseInt(data_index)].id_no) &&
													  (mor.ticket.passengersCache.passengers[i].user_name==mor.ticket.passengerList[parseInt(data_index)].user_name) && 
													  mor.ticket.passengersCache.passengers[i].user_type==mor.ticket.passengerList[parseInt(data_index)].user_type && 
													  mor.ticket.passengersCache.passengers[i].checked==1){
												 mor.ticket.passengersCache.passengers[i].checked=0;
												 break;
										 }
									 }
									 mor.ticket.passengerList.splice(data_index,1);
									 showPassengersFunHandher();
									 updateButtonState();
									 return;
								  }
							 },
							 {
								  text: "取消", handler: function(){
									  return false;
								  } 
							  }
							]);
					}
					else{
						WL.SimpleDialog.show("温馨提示", "确定删除该乘客吗?",
							 [
							  {
								  text: "取消", handler: function(){
									  return false;
								  } 
							  },
							  {
								  text: "确认", handler: function(){	
									 for(var i=0;i<mor.ticket.passengersCache.passengers.length;i++){			
										if((mor.ticket.passengersCache.passengers[i].id_type==mor.ticket.passengerList[parseInt(data_index)].id_type) && 
													  (mor.ticket.passengersCache.passengers[i].id_no==mor.ticket.passengerList[parseInt(data_index)].id_no) &&
													  (mor.ticket.passengersCache.passengers[i].user_name==mor.ticket.passengerList[parseInt(data_index)].user_name) && 
													  mor.ticket.passengersCache.passengers[i].user_type==mor.ticket.passengerList[parseInt(data_index)].user_type && 
													  mor.ticket.passengersCache.passengers[i].checked==1){
												 mor.ticket.passengersCache.passengers[i].checked=0;
												 break;
										 }
									 }
									 mor.ticket.passengerList.splice(data_index,1);
									 showPassengersFunHandher();
									 updateButtonState();
									 return;
								  }
							 }
							]);
					}
				 }
			});
		
		//添加儿童乘车人
		jq(".car-peop").off("taphold").on("taphold",".car-peop-list", function(e){	
			var that = jq(this);
			var data_index = that.attr("data-index");
			if (data_index!=undefined ||  data_index!=''){
				var erTong = mor.ticket.passengerList[data_index];
				var util = mor.ticket.util;
				if(erTong.ticket_type === "2" || erTong.ticket_type === "3"){
					return;
				}else{
					if(mor.ticket.passengerList.length>=5){
						util.alertMessage("乘车人最多添加5人");
						return;
					}else{
						var newErTongPassenger = [];
						var temp_ticket_type = '2';
						var temp_user_type = "2";
						var temp_copy_child = 1;
						var seat_type = mor.ticket.passengerList[data_index].seat_type;
						// 针对动卧与高级动卧处理
						if(seat_type == "F" || seat_type == "A"){
							temp_ticket_type = '1';
							temp_user_type = "1";
							temp_copy_child = 2;
						}
						newErTongPassenger['checked']     = 1;
						newErTongPassenger['id_no']     = mor.ticket.passengerList[data_index].id_no;
						newErTongPassenger['id_type']     = mor.ticket.passengerList[data_index].id_type;
						newErTongPassenger['length']       = mor.ticket.passengerList[data_index].length;
						newErTongPassenger['mobile_no']         = mor.ticket.passengerList[data_index].mobile_no;
						newErTongPassenger['p_str']     = mor.ticket.passengerList[data_index].p_str;
						newErTongPassenger['seat_type']     = mor.ticket.passengerList[data_index].seat_type;
						newErTongPassenger['ticket_type']     = temp_ticket_type;
						newErTongPassenger['user_name']     = mor.ticket.passengerList[data_index].user_name;
						newErTongPassenger['user_type']     = temp_user_type;
						newErTongPassenger['copy_child']     = temp_copy_child;
						if(seat_type == "F" || seat_type == "A"){
							newErTongPassenger['news']	= "new";
						}
						mor.ticket.passengerList.splice(parseInt(data_index)+1,0,newErTongPassenger);
						chooseSeatType = 1;
						showPassengersFunHandher();
						updateButtonState();
					}
				}
			}
		});
		// 选择人员类型
		jq(".changeSeatType").live("change", function(e){
			  e.stopImmediatePropagation();
			  var data_index  = jq(this).attr("data-index");
			  var data_value  = jq(this).attr("value");
			  if (data_index!=undefined && data_index!=''){
				  getPassengerList()[data_index]['ticket_type'] = data_value;
				  var that = this;
				  jq(that).trigger("blur");
				  setTimeout(function(){
					  jq(that).parent().find('.seat').html(that.options[that.selectedIndex].innerHTML);
					  //contentIscrollRefresh();

				  }, 20);
				return;
			 }
		});

		// 选择座席
		jq(".changeyplistType").live("change", function(e){
			  e.stopImmediatePropagation();
	   		  var data_index  = jq(this).attr("data-index");
			  var data_value  = jq(this).attr("value");
			  if (data_index!=undefined && data_index!=''){
				//mor.ticket.passengerList[data_index].seat_type = data_value;
				getPassengerList()[data_index]['seat_type'] = data_value;
				var that = this;
				  jq(that).trigger("blur");
				setTimeout(function(){
					jq(that).parent().find(".seat").html(that.options[that.selectedIndex].innerHTML);
					//contentIscrollRefresh();
                },20);
				return;
			 }
		});
	}

	//add by yiguo 
	function  psgViewAddPsg(){
		jq("#TicketAddPassengerBtn").off();
	    jq("#selectFarePassengerView").remove();
		mor.ticket.history.url='fareList';
		if(mor.ticket.orderManager.capchaNewOrOldFlag == "2"){
			mor.ticket.history.inputyzm=jq('#captchaInput').val();
		}
		mor.ticket.util.keepPageURL();
		mor.ticket.util.changePage(vPathCallBack()+"fareList.html");
		setTimeout(function(){
	       	 jq("#TicketAddPassengerBtn").off().on("tap",psgViewAddPsg);
	        },1000);
		return false;
	}
	
	// 跳转
	function ticketAddPassengerBtnListeners(){
		jq("#TicketAddPassengerBtn").off().on("tap", psgViewAddPsg);
	}

	function registerSelectPassengerListener(){
		jq(".car-peop-list-input").live("tap",function(e){
			e.stopImmediatePropagation();
            e.stopPropagation();
            e.preventDefault();
			var checkbox = jq(this).find("input[type='checkbox']");
			if(checkbox.is(":checked")){
				checkbox.attr("checked",false);
			}else{
				checkbox.attr("checked",true);
			}
		});
	}
	
	// 监控修改座席类型
	function ticketEditSeatBtnListeners(){
		jq(".editBtnSeat").bind("tap", function(e){
			if(tapHolding){
				return false;
			}
			var that = jq(this);
			if (that.hasClass('disabled')) return;
			var data_val = that.attr("data-val");
			jq('.editBtnSeat').removeClass('hover');
			that.addClass('hover');
			if(mor.ticket.viewControl.bookMode == "dc"){
				for(var i = mor.ticket.passengerList.length-1;i>=0;i--){
					if(mor.ticket.passengerList[i].news != undefined && mor.ticket.passengerList[i].news == "new"){
						mor.ticket.passengerList.splice(i,1);
					}
				}
				
				setTimeout(function() {
					showPassengersFunHandher();
					updateButtonState();
				}, 200);
			}
			jq.each(getPassengerList(), function(i, passenger) {
			    passenger.seat_type = data_val;
			    passenger.seat_type_code = "";
			});
			changeDefaultSeatType(data_val);
		});
		jq(".editBtnSeat").on("taphold", function(e){
			if(!jq(this).hasClass("hover")){
				return false;
			}
			e.stopPropagation();
			e.preventDefault();
			tapHolding = true;
			setTimeout(function(){
				tapHolding = false;
			},1000);
			if(jq(this).attr("data-val") == "F"  || jq(this).attr("data-val") == "A"){
				if(mor.ticket.passengerList.length>=4){
					return false;
				}else{
					 if(jq(this).attr("data-val") == "F"){
						 	var passengersLength = mor.ticket.passengerList.length;
							if(passengersLength < 4){
								for(var i=0;i<(4-passengersLength);i++){
									var newErTongPassenger = [];
									var data_index= mor.ticket.passengerList.length-1;
									newErTongPassenger['checked']     = 1;
									newErTongPassenger['id_no']     = mor.ticket.passengerList[data_index].id_no;
									newErTongPassenger['id_type']     = mor.ticket.passengerList[data_index].id_type;
									newErTongPassenger['length']       = mor.ticket.passengerList[data_index].length;
									newErTongPassenger['mobile_no']         = mor.ticket.passengerList[data_index].mobile_no;
									newErTongPassenger['p_str']     = mor.ticket.passengerList[data_index].p_str;
									newErTongPassenger['seat_type']     = mor.ticket.passengerList[data_index].seat_type;
									newErTongPassenger['ticket_type']     = "1";
									newErTongPassenger['user_name']     = mor.ticket.passengerList[data_index].user_name;
									newErTongPassenger['user_type']     = "1";
									newErTongPassenger['copy_child']     = 2;
									newErTongPassenger['news']	= "new";
									mor.ticket.passengerList.splice(-1,0,newErTongPassenger);
								}
							}
						}else{
							if(mor.ticket.passengerList.length == 1 || mor.ticket.passengerList.length == 3){
									var newErTongPassenger = [];
									var data_index= mor.ticket.passengerList.length-1;
									newErTongPassenger['checked']     = 1;
									newErTongPassenger['id_no']     = mor.ticket.passengerList[data_index].id_no;
									newErTongPassenger['id_type']     = mor.ticket.passengerList[data_index].id_type;
									newErTongPassenger['length']       = mor.ticket.passengerList[data_index].length;
									newErTongPassenger['mobile_no']         = mor.ticket.passengerList[data_index].mobile_no;
									newErTongPassenger['p_str']     = mor.ticket.passengerList[data_index].p_str;
									newErTongPassenger['seat_type']     = mor.ticket.passengerList[data_index].seat_type;
									newErTongPassenger['ticket_type']     = "1";
									newErTongPassenger['user_name']     = mor.ticket.passengerList[data_index].user_name;
									newErTongPassenger['user_type']     = "1";
									newErTongPassenger['copy_child']     = 2;
									newErTongPassenger['news']	= "new";
									mor.ticket.passengerList.splice(-1,0,newErTongPassenger);
								}
						}
					chooseSeatType = 1;
					showPassengersFunHandher();
					updateButtonState();
				}
			}else{
				return false;
			}
		});
	}

	// 修改乘客系别类型.
	function changeDefaultSeatType(seat_type) {
		jq('.car-peop .changeyplistType').val(seat_type);
		var ticket = mor.ticket.currentTicket;
		var util = mor.ticket.util;
		setTimeout(function() {
			jq('.car-peop-list-zx .seat').html(util.repalceYpInfoType(ticket.getTypeName(seat_type)));
		}, 20);

		ticket.default_seat_type = seat_type;
		mor.ticket.default_seat_type = seat_type;
	}

	function registershowAllpassengerInfoTipsListeners(){
		jq("#showAllpassengerInfoTips").bind("tap", function(){
			jq("#passengerInfoTips2").show();
			jq("#showAllpassengerInfoTips").hide();
			jq('#passengersView .iscroll-wrapper').iscrollview('refresh');
			return false;
		});
	}

	function registerFormChangeListeners(){
		var currentPassenger=mor.ticket.currentPassenger;
		var util = mor.ticket.util;
		jq("#passengerNameInput").bind("change", function(){
			currentPassenger.user_name = jq(this).val();

			// 判断如果当前的模式是针对于修改联系人的情况，不应该显示追加联系人按钮
			if(!jq("#bookTicketModifyPassengerDiv").is(":visible")){
				if(currentPassenger.user_name != ""){
					jq("#bookTicketAddPassengerBtn").parent().show();
				}
			}
			return false;
		});
		util.bindSelectFocusBlurListener("#idTypeSelect");
		jq("#idTypeSelect").bind("change", function(){
			currentPassenger.id_type = jq(this).val();
			return false;
		});
		jq("#idNoInput").bind("change", function(){
			currentPassenger.id_no = jq(this).val();
			// 判断如果当前的模式是针对于修改联系人的情况，不应该显示追加联系人按钮
			if(!jq("#bookTicketModifyPassengerDiv").is(":visible")){

				if(currentPassenger.id_no != ""){
					jq("#bookTicketAddPassengerBtn").parent().show();
				}
			}
			return false;
		});
		jq("#mobileNoInput").bind("change", function(){
			currentPassenger.mobile_no = jq(this).val();
			return false;
		});
		util.bindSelectFocusBlurListener("#ticketTypeSelect");
		jq("#ticketTypeSelect").bind("change", function(){
			// 判断当前的旅客类型是否和车票类型相符，只有学生才能买学生票
			// currentPassenger变量不要在其他业务逻辑中使用
			if(mor.ticket.currentPassenger.ticket_type!='3'&&jq(this).val()=='3'&&mor.ticket.currentPassenger.ticket_type!=undefined){
				// util.alertMessage("tt"+mor.ticket.currentPassenger.ticket_type);
				jq("#ticketTypeSelect option[value="+mor.ticket.currentPassenger.ticket_type+"]").attr("selected","selected");
				jq("#ticketTypeSelect").selectmenu('refresh', true);
				util.alertMessage("只有学生类型的常用联系人可以购买学生票！");

			}else{
				currentPassenger.ticket_type = jq(this).val();
			}
			return false;
		});
		util.bindSelectFocusBlurListener("#seatTypeSelect");
		jq("#seatTypeSelect").bind("change", function(){
			currentPassenger.seat_type = jq(this).val();
			return false;
		});
	}

	function registerPassengersButtonsListener(){
		jq("#passengersBackBtn").off().bind("tap",function(e){
            e.stopImmediatePropagation();
            e.stopPropagation();
            e.preventDefault();
            if(prevId =="travelPlanDetailView"){
            	mor.ticket.util.changePage("travelPlan.html");
    			return false;
            }else{
            	mor.ticket.searchResultList.leftOrPriceFlag = "2";
            	mor.ticket.searchResultScreen.chooseQueryFlag = false;
     			mor.ticket.util.changePage("searchSingleTicketResults.html");
     			return false;
            	}
		});



		jq("#refreshCaptcha").off().bind("tap", mor.ticket.orderManager.refreshCaptchaImg);

		jq("#refreshImgBtn").off().bind("tap", mor.ticket.orderManager.refreshCaptchaImg);
		
		jq("#waitTimeOKbtn").off().on("tap",function(){
			jq("#counter_line").hide();
			mor.ticket.viewControl.tab1_cur_page = vPathViewCallBack()+"MobileTicket.html";

			var intervalId = mor.ticket.poll.intervalId;
			if(intervalId != ""){
				mor.ticket.poll.timer.stop();
				clearInterval(mor.ticket.poll.intervalId);
			}
			window.plugins.childBrowser.closeDialog();
			//如果当前的mode为gc那么在改签提交订单后更改mode为dc
			if(mor.ticket.viewControl.bookMode == "gc"){
				mor.ticket.viewControl.bookMode = "dc";
			}

			mor.ticket.viewControl.tab1_cur_page = vPathViewCallBack()+"MobileTicket.html";
			//mod by yiguo
			//mor.ticket.util.changePage("queryOrder.html");
			
			if (!busy.isVisible()) {
				busy.show();
			}

			window.pullOrderData(function(result){
				if(busy.isVisible()){
					busy.hide();
				}
				//mod by yiguo

				//TODO get current order index & jump to orderlist.html
				 var invocationResult = result.invocationResult;
			        if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			        	if (invocationResult.orderList.length == 0) {
			        		jq.mobile.changePage("queryOrder.html");
			     			setTimeout(function(){
			     				jq("#refreshUnfinishedorder").trigger("tap");
			     			},1000);
			        		return;
			        	}
			            var queryOrder = mor.ticket.queryOrder;
			            //init queryOrder queryOrderList
			            queryOrder.queryOrderList = [];
			            queryOrder.originPaidQrderList = [];

			            queryOrder.setUnfinishedOrderList(invocationResult.orderList);
			            //displayNoFinishedOrders();


			            var noFinishedOrderList = queryOrder.queryOrderList;


			            if (noFinishedOrderList && noFinishedOrderList[0]) {

			                queryOrder.hasChangeTicket();

			            }



			            	// init queryOrder content
			            	queryOrder.currentQueryOrder = {};
			            	queryOrder.currentUnfinishOrderIndex = noFinishedOrderList.length -1;
			            	mor.ticket.queryOrder.setCurrentUnfinishedOrders(queryOrder.currentUnfinishOrderIndex);
			            	jq.mobile.changePage("orderList.html",{reloadPage:true,transition:"none"});



			            //mor.ticket.util.changePage("orderList.html",{reloadPage:true});
			        } else {
			            mor.ticket.util.alertMessage(invocationResult.error_msg);
			        }


				//mor.ticket.util.changePage(vPathCallBack() + "orderList.html");

			},
			function(){
				if(busy.isVisible()){
					busy.hide();
				}
				util.alertMessage("订单状态获取失败，暂时无法转入订单查询页面！");
			});


			return false;
		});


		jq("#submitOrderBtn").off().on("tap", function(){
			//判断学生票区间
			var existStudent = false;
			var passengers = getPassengerList();
			for (var i = 0; i < passengers.length; i ++ ){
				if(passengers[i].ticket_type =='3'){
					existStudent = true;
					break;
				}
			}
			if(existStudent){
				var stuPurchaseInterval = false;
				var summer_day = window.ticketStorage.getItem("summer_day");
				var winter_day = window.ticketStorage.getItem("winter_day");
				var selectDate = mor.ticket.currentTicket.train_date.replace(/\-/g,"");
				var selectYear = selectDate.substr(0,4);
				var summer_day_begin = selectYear+summer_day.substr(0,4);
				var summer_day_end = selectYear+summer_day.substr(5,4);
				var selectMonth = selectDate.substr(4,2);
				var winter_day_begin,winter_day_end;
				if(selectMonth<7){
					winter_day_begin = (parseInt(selectYear) - 1)+ winter_day.substr(0,4);
					winter_day_end = selectYear+winter_day.substr(5,4);
				}else{
					winter_day_begin = selectYear+winter_day.substr(0,4);
					winter_day_end = (parseInt(selectYear) + 1)+winter_day.substr(5,4);
				}
			    if(selectDate >= summer_day_begin && selectDate <= summer_day_end) stuPurchaseInterval = true;
			    if(selectDate >= winter_day_begin && selectDate <= winter_day_end) stuPurchaseInterval = true;
			    if(!stuPurchaseInterval){
			    	mor.ticket.util.alertMessage("学生票的乘车时间为每年的暑假"+summer_day.slice(0,2)+"月"+summer_day.slice(2,4)+"日至"+
							summer_day.slice(5,7)+"月"+summer_day.slice(7,9)+"日"+"、寒假"+winter_day.slice(0,2)+"月"+winter_day.slice(2,4)+"日至"+
							winter_day.slice(5,7)+"月"+winter_day.slice(7,9)+"日，目前不办理学生票业务！");
					return;
			    }
			}

			// mjl
			// by yiguo
			jq("#captchaInput").blur();
			// 判断当前用户是否为激活用户，未激活用户不可以提交订单
			/*if (mor.ticket.loginUser.activeUser != 'Y') {
				mor.ticket.util.alertMessage("当前用户未激活，请到您的注册邮箱收取12306激活邮件后按提示激活用户后再登录！");
				return false;
			}*/
			if(mor.ticket.viewControl.bookMode=="fc"&&
				jq('#passengersView .bookBackTicketCheckBox:visible:checked').length==0){
				mor.ticket.util.alertMessage("请选择需要购买返程车票的乘车人！");
				return false;
			}
			if(getPassengerList().length>=1 ){
				if(mor.ticket.orderManager.capchaNewOrOldFlag == "2"){
					if(jq("#captchaInput").val() === ""){
						mor.ticket.util.alertMessage("请输入验证码!");
						return false;
					}
				}
				if(validationForSubmit() && checkPassengerValid()){
					var is_confirm_order__station_code = false;
					var confirm_order__station_code = window.ticketStorage.getItem("confirm_order__station_code").split("@");
					for(var j=0;j<confirm_order__station_code.length;j++){
						if(confirm_order__station_code[j].split("#")[0].indexOf(prepareSubmitOrderParameters().from_station)!=-1 && confirm_order__station_code[j].split("#")[1].indexOf(prepareSubmitOrderParameters().station_train_code.substring(0,1)) != -1){
							is_confirm_order__station_code = true;
							break;
						}
					}
					if(is_confirm_order__station_code == true){
							if(mor.ticket.util.isAndroid()){
								WL.SimpleDialog.show("温馨提示",window.ticketStorage.getItem("confirm_order_station_code_message"),
									[ 
									  {
										  text : '确定', handler: function(){
													confirmPassengerBeforeAlertMessage();
									  		}
									  },
									  {text : '取消', handler: function(){}}
									]);
							}else{
								WL.SimpleDialog.show("温馨提示",window.ticketStorage.getItem("confirm_order_station_code_message"),
									[ 
									  {text : '取消', handler: function(){}},
									  {
										  text : '确定', handler: function(){
												confirmPassengerBeforeAlertMessage();
									  		}
									  }
									]);
								}
						}else{
							confirmPassengerBeforeAlertMessage();
				}
			}
		}else{
			mor.ticket.util.alertMessage("请至少添加一位乘客!");
		}
			return false;
		});
		//购买往返优惠票的旅客，不得单独办理往程车票的退票业务提示
		function DiscountTicketNoRefund(){
			   var orderManager=mor.ticket.orderManager;
			   orderManager.clearTicketList();
			   var length = getPassengerList().length;
			   orderManager.setTotalTicketNum(length);
			   var parameters=prepareSubmitOrderParameters();
				var passengersLists = getPassengerList();
			   if(mor.ticket.viewControl.bookMode=="dc"){
				   mor.ticket.DcSeatTypeList.result.length = 0;
				   for(var i = 0;i<passengersLists.length;i++){
				   mor.ticket.DcSeatTypeList.result.push(passengersLists[i].seat_type);
				   }
			   }
			   var tipMessage ="购买往返优惠票的旅客，不得单独办理往程车票的退票业务。";
			   if(mor.ticket.viewControl.bookMode=="fc"&&jq("#preferentialAgreeInfo input[name='agree']:checkbox").is(":checked")){
				   if(mor.ticket.util.isAndroid()){
					   WL.SimpleDialog.show("温馨提示",tipMessage,
								[ 
								  {
									  text : '确定', handler: function(){
											  parameters.dynamicProp="0+0+G+0";
											  orderManager.confirmPassengerInfo(parameters);
											  return;							
								  		}
								  },
								  {text : '取消', handler: function(){
									  jq("#preferentialAgreeInfo input[name='agree']:checkbox").attr("checked",false);	
									  return;
								  }}
								 
								]);
				   }else{
					   WL.SimpleDialog.show("温馨提示",tipMessage,
								[ 
								 {text : '取消', handler: function(){
									  jq("#preferentialAgreeInfo input[name='agree']:checkbox").attr("checked",false);	
									  return;
								  }},
								  {
									  text : '确定', handler: function(){
											  parameters.dynamicProp="0+0+G+0";
											  orderManager.confirmPassengerInfo(parameters);
											  return;							
								  		}
								  }
								]);
				   }									
			   }else{
					  parameters.dynamicProp="0+0+0+0";
				      orderManager.confirmPassengerInfo(parameters);
			   }
		}
		
		function confirmPassengerBeforeAlertMessage(){
			var mode = mor.ticket.viewControl.bookMode;
			var parameters=prepareSubmitOrderParameters();
			if(mode === "gc"){
				parameters = prepareChangeTicketParameters();
				parameters.from_station = parameters.from_station_telecode;
				parameters.to_station = parameters.to_station_telecode;
				parameters.passenger_id_nos = parameters.ori_passenger_id_nos;
				parameters.passenger_id_types = parameters.ori_passenger_id_type_codes;
				parameters.passenger_names = parameters.ori_passenger_names;
				parameters.ticket_types = parameters.ticket_type_codes;
				parameters.passenger_names = parameters.ori_passenger_names;
			}
			
			//6月1号禁大烟
			var passengers = mor.ticket.passengersCache.passengers;
			var is_smoker = false;
			var is_smoker_name = "";
			for(var i=0;i<passengers.length;i++){
				if( parameters.passenger_id_nos.split(",").indexOf(passengers[i].id_no) !=-1 && passengers[i].is_smoker == "Y"){
					if(is_smoker_name.indexOf(passengers[i].user_name+"、") != 0){
						is_smoker_name += passengers[i].user_name+"、";
					}
					is_smoker = true;
				}
			}
			if(is_smoker){
				var newSmokers = is_smoker_name.substring(0,is_smoker_name.length-1)+" ";
				var is_smoker_message ="根据有关部门提供的信息，"+newSmokers+"曾因违反国务院颁布的《铁路安全管理条例》规定，在动车组列车上吸烟被处罚。" +
						"为了确保公共安全，请自觉遵守有关法律规定，不要在动车组列车以及其他禁烟区域吸烟。" +
						"请点击“确认”继续购票。";
				if(mor.ticket.util.isAndroid()){
					WL.SimpleDialog.show("温馨提示",is_smoker_message,
							[ 
							  {
								  text : '确定', handler: function(){
									  AlertBeforeConfirm(0);				
							  		}
							  },
							  {text : '取消', handler: function(){
								  return false;
							  }}
							]);
				}else{
					 WL.SimpleDialog.show("温馨提示",is_smoker_message,
								[ 
								 {text : '取消', handler: function(){
									 return false;
								  }},
								  {
									  text : '确定', handler: function(){
										  AlertBeforeConfirm(0);							
								  		}
								  }
							]);
				}
			}else{
				AlertBeforeConfirm(0);
			}
		}
		var util = mor.ticket.util;
		var nowDate = util.getNewDate();
		function AlertBeforeConfirm(i){
			var saleTicketMessge = JSON.parse(window.ticketStorage.getItem("saleTicketMessage"));
			if(saleTicketMessge.length>0){
				if(i>=saleTicketMessge.length){
					if(mor.ticket.viewControl.bookMode == "gc"){
						// 增加验证码的校验
						if(mor.ticket.orderManager.capchaNewOrOldFlag == "2"){
							if(jq("#captchaInput").val() != "" ){
								var parameters=prepareChangeTicketParameters();
								var orderManager=mor.ticket.orderManager;
								orderManager.confirmPassengerInfo(parameters);
							}else{
								mor.ticket.util.alertMessage("请输入验证码!");
							}
						}else{
							var parameters=prepareChangeTicketParameters();
							var orderManager=mor.ticket.orderManager;
							orderManager.confirmPassengerInfo(parameters);
						}
					}else{
						DiscountTicketNoRefund();
					}
					return false;
				}
				var mode = mor.ticket.viewControl.bookMode;
				var parameters=prepareSubmitOrderParameters();
				if(mode === "gc"){
					parameters = prepareChangeTicketParameters();
					parameters.from_station = parameters.from_station_telecode;
					parameters.to_station = parameters.to_station_telecode;
					parameters.passenger_id_nos = parameters.ori_passenger_id_nos;
					parameters.passenger_id_types = parameters.ori_passenger_id_type_codes;
					parameters.passenger_names = parameters.ori_passenger_names;
					parameters.ticket_types = parameters.ticket_type_codes;
					parameters.passenger_names = parameters.ori_passenger_names;
				}
				var passengerArray = parameters.passenger_id_types.split(",");
				var isIdType = false;
				for(var j=0;j<passengerArray.length;j++){
					if(saleTicketMessge[i].id_type.indexOf(passengerArray[j]) !=-1){
						isIdType = true;
						break;
					}
				}
		  	    if(saleTicketMessge[i].message_stop_date === undefined || saleTicketMessge[i].message_stop_date === null || saleTicketMessge[i].message_stop_date === ""){
		  	    	saleTicketMessge[i].message_stop_date = "20991231";
		  	    }
		  	    if(saleTicketMessge[i].train_date_begin === undefined || saleTicketMessge[i].train_date_begin === null || saleTicketMessge[i].train_date_begin === ""){
		  	    	saleTicketMessge[i].train_date_begin = "20150624";
		  	    }
		  	    if(saleTicketMessge[i].train_date_end === undefined || saleTicketMessge[i].train_date_end === null || saleTicketMessge[i].train_date_end === ""){
		  	    	saleTicketMessge[i].train_date_end = "20991231";
		  	    }
		  	    var messageStopAlertTime = util.setMyDate2(saleTicketMessge[i].message_stop_date);
		  	    var train_date_begin = util.setMyDate2(saleTicketMessge[i].train_date_begin);
		  	    var train_date_end = util.setMyDate2(saleTicketMessge[i].train_date_end);
				var train_date_time = util.setMyDate2(parameters.train_date);
				if(((isIdType && saleTicketMessge[i].to_station.indexOf(parameters.to_station)!= -1 )
						|| (saleTicketMessge[i].train_station_code.indexOf(util.getBeginTrain(parameters.train_no)) != -1 
					     )) && saleTicketMessge[i].confirm_before_behind == "before" 
					        && messageStopAlertTime >= nowDate
					        && train_date_time >= train_date_begin
					        && train_date_time <= train_date_end){
					if(mor.ticket.util.isAndroid()){
						WL.SimpleDialog.show("温馨提示",saleTicketMessge[i].message,
								[ 
								  {
									  text : '确定', handler: function(){
										  AlertBeforeConfirm(i+1);
								  		}
								  },
								  {text : '取消', handler: function(){
									  return false;
								  }}
								]);
					}else{
						 WL.SimpleDialog.show("温馨提示",saleTicketMessge[i].message,
									[ 
									 {text : '取消', handler: function(){
										 return false;
									  }},
									  {
										  text : '确定', handler: function(){
											  AlertBeforeConfirm(i+1);
									  		}
									  }
								]);
					}
				}else{
					AlertBeforeConfirm(i+1); 
				}
			}else{
				DiscountTicketNoRefund();
			}
		}
		function checkPassengerValid(){
			// 针对动卧与高级动卧特殊处理
			var ticket = mor.ticket.currentTicket;
			var mode = mor.ticket.viewControl.bookMode;
			var passengerList = getPassengerList();
			var id_no_list = [];
			for(var i=0;i<passengerList.length;i++){
				var passenger = passengerList[i];
				if(mode == "dc" && passenger.ticket_type != '2' 
					&& passenger.seat_type != "A"
					&& passenger.seat_type != "F"){
					var passenger_id = passenger.id_type + passenger.id_no;
					if(jq.inArray(passenger_id,id_no_list) != -1){
						mor.ticket.util.alertMessage("乘车人信息重复，请重新选择乘车人。");
						return false;
					}else{
						id_no_list.push(passenger_id);
					}
				}
			}
			return true;
		}
		jq("#submitChangeTicketBtn").off().on("tap",function(){
			//判断学生票区间
			var existStudent = false;
			var passengerList = getPassengerList();
			for (var i = 0; i < passengerList.length; i ++ ){
				if(passengerList[i].ticket_type =='3'){
					existStudent = true;
					break;
				}
			}
			if(existStudent){
				var stuPurchaseInterval = false;
				var summer_day = window.ticketStorage.getItem("summer_day");
				var winter_day = window.ticketStorage.getItem("winter_day");
				var selectDate = mor.ticket.currentTicket.train_date.replace(/\-/g,"");
				var selectYear = selectDate.substr(0,4);
				var summer_day_begin = selectYear+summer_day.substr(0,4);
				var summer_day_end = selectYear+summer_day.substr(5,4);
				var selectMonth = selectDate.substr(4,2);
				var winter_day_begin,winter_day_end;
				if(selectMonth<7){
					winter_day_begin = (parseInt(selectYear) - 1)+ winter_day.substr(0,4);
					winter_day_end = selectYear+winter_day.substr(5,4);
				}else{
					winter_day_begin = selectYear+winter_day.substr(0,4);
					winter_day_end = (parseInt(selectYear) + 1)+winter_day.substr(5,4);
				}
			    if(selectDate >= summer_day_begin && selectDate <= summer_day_end) stuPurchaseInterval = true;
			    if(selectDate >= winter_day_begin && selectDate <= winter_day_end) stuPurchaseInterval = true;
			    if(!stuPurchaseInterval){
			    	mor.ticket.util.alertMessage("学生票的乘车时间为每年的暑假"+summer_day.slice(0,2)+"月"+summer_day.slice(2,4)+"日至"+
							summer_day.slice(5,7)+"月"+summer_day.slice(7,9)+"日"+"、寒假"+winter_day.slice(0,2)+"月"+winter_day.slice(2,4)+"日至"+
							winter_day.slice(5,7)+"月"+winter_day.slice(7,9)+"日，目前不办理学生票业务！");
					return;
			    }
			}
			// by yiguo
			// 判断当前用户是否为激活用户，未激活用户不可以提交订单
			/*if (mor.ticket.loginUser.activeUser != 'Y') {
				mor.ticket.util.alertMessage("当前用户未激活，请到您的注册邮箱收取12306激活邮件后按提示激活用户后再登录！");
				return false;
			}*/
			// 必须勾选一个改签票
			if(jq('#passengersView .changeTicketCheckBox:visible:checked').length==0){
				mor.ticket.util.alertMessage("请勾选需要改签的车票！");
				return false;
			}

			// 改签时必须选择相同座位
			var seat_array = [];

			jq.each(passengerList, function(i, passenger) {
				if (jq(".car-peop-list:eq("+i+") .changeTicketCheckBox").attr("checked") == "checked"){
					seat_array.push(passenger.seat_type);
				}
			});
			jq.unique(seat_array);
			if(seat_array.length>1){
				mor.ticket.util.alertMessage("改签时，必须选择相同席别！");
				return false;
			}
			var model = mor.ticket.leftTicketQuery;
			// 判断改签选择的订票类型是否一致
			for(var i =0;i<passengerList.length;i++){
				if(model.purpose_codes == '0X' && passengerList[i].ticket_type !='3'){
					mor.ticket.util.alertMessage("您选择的是学生票类型,请重新选择乘车人。");
					return false;
				}
			} 
			/**
			 * 改签时，不过滤儿童票
			 * 2014-12-16
			var passengers = getPassengerList();
			for (var i = 0; i < passengers.length; i ++ ){
				var passenger = passengers[i];
				var childFlag = false;
				var adultFlag = false;
				var people = jq(".car-peop-list:eq("+i+")");
				if (people.find(".changeTicketCheckBox").attr("checked") == "checked"){
					var temp_ticket_type = people.find(".changeSeatType").val();
					if(temp_ticket_type == "2"){
						childFlag = true;
					}
					else if( passenger.ticket_type == "1"){
						adultFlag = true;
						break;
					}
				}
				if(childFlag&&!adultFlag){
					util.alertMessage("儿童不能单独旅行，请与成人票一同购买");
					return false;
				}
			}		
			**/
			//提示信息
			confirmPassengerBeforeAlertMessage();
			return false;
		});
		
		/*获取当前车次的停靠站信息*/
		jq("#queryStopStationBtn").off().on("tap",function(e){
			if(jq("#queryStopStationBtn").hasClass("arrow-up")){
				jq("#currentStopStationInfo").hide();
				jq("#queryStopStationBtn").removeClass("arrow-up");
				jq("#queryStopStationBtn").addClass("arrow-down");
				return;
			}
			if(queryStopStationFlag === "1"){
				jq("#currentStopStationInfo").show();
				jq("#queryStopStationBtn").removeClass("arrow-down");
				jq("#queryStopStationBtn").addClass("arrow-up");
				return;
			}
			var ticket = mor.ticket.currentTicket;
			var commonParameters = {
					'depart_date': ticket.start_train_date,
					'from_station_telecode': ticket.from_station_telecode,
					'to_station_telecode':ticket.to_station_telecode,
					'train_no':ticket.train_no
					
				};
				
				var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "stopStationQuery"
				};
			var options = {
				onSuccess : requestSucceeded,
				onFailure : util.creatCommonRequestFailureHandler()
			};
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,options);
		});
	}

	function requestSucceeded(result) {
		if (busy.isVisible()) {
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			showSearchResults(invocationResult);
		} else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			return;
		}
	};

	function showSearchResults(result) {
		var invocationResult = result;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			queryStopStationFlag = "1";
			jq("#currentStopStationInfo").html(generateTrainStationListContent(invocationResult.list));
			jq("#currentStopStationInfo").show();
			jq("#queryStopStationBtn").removeClass("arrow-down");
			jq("#queryStopStationBtn").addClass("arrow-up");
			jq('#passengersView .iscroll-wrapper').iscrollview('refresh');
		}else {	
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	// 提前的预处理方法。
	// 对无座票进行处理，不显示无座，按照id显示座位类型.也就是显示硬座或者一等座/二等座
	// 同时在数组中删除无座的成员，无座的情况按照rate = 100判断。
	// 如果只有无座的票，那么按照真实的席别显示
	// TODO 确定每次页面进入时， currentTicket 能够初始化确定的值.
	function processSeatTypeInCurrentTicket(ticket){
		// 只处理一次.
		var cache = mor.ticket.cache;
		var yplist = ticket.yplist;

		// 通过 type_id type_rate 来把type_id 给映射为中文信息
		// 通过再把无座的 type_id 后面添加一位？然后所有需要通过 type_id 来计算的都需要经过这个
		// 映射才能进行后续的处理.
		var seatTypeMapping = ticket.seatTypeMapping = {};
		var type_ids = ticket.seat_types = [];// 用于储存席别数组

		jq.each(yplist, function(i, yp) {
			var type_id = yp.type_id.replace(/_none/, '');

			yp.type = cache.getSeatTypeByCode(type_id);

			if (yp.type_rate == 100) {
				yp.type_alias = '无座';
				// 保存无票的标示.
				if (!/_none/.test(yp.type_id)) {
					yp.type_id = yp.type_id + '_none';
				}
			}
			seatTypeMapping[yp.type_id] = {
			    type: yp.type,
			    alias: yp.type_alias
			};

			if(yp.num){
				type_ids.push(yplist[i].type_id);
			}
		});

		// 获取系统类型
		ticket.getSeatType = function(id) {
			return id.replace('_none', '');
		};

		// 获取票的详细信息.
		ticket.getType = function(id) {
			return seatTypeMapping[id];
		};

		ticket.getTypeName = function(id) {
			var type = ticket.getType(id);
			return type.alias || type.type;
		};

		var default_seat_type;// 获取默认的席别编号
		var mode = mor.ticket.viewControl.bookMode;
		if(mode === "gc"||mode === "fc"){
			var passengerList = getPassengerList();
			default_seat_type = passengerList[0].seat_type;
		}else{
			default_seat_type = mor.ticket.leftTicketQuery.seat_Type;
		}

		ticket.getDefaultSeatType = function(id) {
			// 如果用户指定的类型存在用户可以用的席别列表，那就返回.
			if (id && type_ids.indexOf(id) > -1) {
				return id;
			} else if (ticket.default_seat_type) {
				// 如果存在默认的系别列表就返回默认的。
				return ticket.default_seat_type;
			} else {
				// 最后倒序选择第一个非无座的座位.
				var firstNoneSeat = type_ids[type_ids.length - 1];
				var sid;
				for (var len = type_ids.length - 1; len > -1; len--) {
					sid = type_ids[len];
					if (!/_none/.test(sid)) {
						firstNoneSeat = sid;
						break;
					}
				}
				return firstNoneSeat;
			}
		};

		// default_seat_type 用来保存用户选中的席别
		ticket.default_seat_type = ticket.getDefaultSeatType(ticket.default_seat_type || default_seat_type);
	}


	function updateButtonState(){
		var submitDiv = jq("#orderSubmitDiv"), tip = jq('#passengersView .tips3');
		if (getPassengerList().length > 0 ){
		 	submitDiv.show();
		 	tip.show();
		 } else {
		 	tip.hide();
		 }
		if(typeof mor.ticket.history.checkCodeImg1 === 'undefined'){
			//提交频繁
			//mor.ticket.orderManager.refreshCaptchaImg2();
		}else{
			if(mor.ticket.orderManager.capchaNewOrOldFlag == "2"){
				jq('#captchaImg').attr('src',mor.ticket.history.checkCodeImg1);
			}else{
				jq('#captchaImg1').attr('src',mor.ticket.history.checkCodeImg1);
			}
		}
		contentIscrollRefresh();
	}


	function validationForSubmit(){
		var util = mor.ticket.util;
		var passengers = getPassengerList();
		var mode = mor.ticket.viewControl.bookMode;
		var model = mor.ticket.leftTicketQuery;
		// 一笔订单，不允许都是小孩票，必须至少绑定一张成人票
		if( mode == "dc" || mode == "wc" || mode == "fc")  {
			var childFlag = false;
			var adultFlag = false;
			for (var i = 0; i < passengers.length; i ++ ){
				var passenger = passengers[i];
				if(model.purpose_codes == '0X' && passenger.ticket_type != "3"){
					util.alertMessage("您选择的是学生票类型,请重新选择乘车人。");
					return false;
				}
				if(model.purpose_codes == '1F' && passenger.ticket_type == "3"){
					util.alertMessage("您选择的是农民工类型,请重新选择乘车人。");
					return false;
				}
				if(model.purpose_codes == '1F' && passenger.id_type != "1"&& passenger.id_type != "2"){
					util.alertMessage("您选择的是农民工类型,乘车人证件只能是一,二代身份证。");
					return false;
				}
				if(mode == "fc"){
					var people = jq(".car-peop-list:eq("+i+")");
					if (people.find(".bookBackTicketCheckBox").attr("checked") == "checked"){
						var temp_ticket_type = people.find(".changeSeatType").val();
						if(temp_ticket_type == "2"){
							childFlag = true;
						}else if( passenger.ticket_type == "1"){
							adultFlag = true;
							break;
						}
					}
				}else{
					if( passenger.ticket_type == "1"){
						adultFlag = true;
						break;
					}
				}
				if(passenger.ticket_type == "2"){
					childFlag = true;
				}

			}
			if(childFlag&&!adultFlag){
				util.alertMessage("儿童不能单独旅行，请与成人票一同购买");
				return false;
			}
		}
		return true;
	}
	function resetForm(){
		jq("#passengerNameInput, #idNoInput, #mobileNoInput").val("");
	}

	function updateForm(passenger){
		jq("#passengerNameInput").val(passenger.user_name);
		jq("#idNoInput").val(passenger.id_no);
		jq("#mobileNoInput").val(passenger.mobile_no);
		jq("#idTypeSelect option[value="+passenger.id_type+"]").attr("selected","selected");
		jq("#idTypeSelect").selectmenu('refresh', true);
		jq("#ticketTypeSelect option[value="+passenger.ticket_type+"]").attr("selected","selected");
		jq("#ticketTypeSelect").selectmenu('refresh', true);

	}

	function fillPasserger(modifypassenger){
		updateForm(modifypassenger);
		jq("#seatTypeSelect option[value="+modifypassenger.seat_type+"]").attr("selected","selected");
		jq("#seatTypeSelect").selectmenu('refresh', true);
		jq("#bookTicketAddPassengerDiv").hide();
		jq("#bookTicketModifyPassengerDiv").show();
	}

	function contentIscrollRefresh(){
		if(jq("#passengersView .ui-content").attr("data-iscroll")!=undefined){
			jq("#passengersView .ui-content").iscrollview("refresh");
		}
	}

	function contentIscrollTo(x,y,time){
		if(jq("#passengersView .ui-content").attr("data-iscroll")!=undefined){
			jq("#passengersView .ui-content").jqmData('iscrollview').iscroll.scrollTo(x,y,time);
		}
	}

	function prepareSubmitOrderParameters(){
		if(mor.ticket.orderManager.capchaNewOrOldFlag !== "2"){
			mor.ticket.orderManager.captchaDataStringify();
		}
		var util = mor.ticket.util;
		var ticket = mor.ticket.currentTicket;

		var mode = mor.ticket.viewControl.bookMode;
		var train_date = ticket.train_date;
		var pass_code_new = "";
		if(mor.ticket.orderManager.capchaNewOrOldFlag == "2"){
			pass_code_new = jq("#captchaInput").val();
		}else{
			pass_code_new = mor.ticket.orderManager.captchaImageResult;
		}
		var parameters = {
			train_date: util.processDateCode(train_date),
			station_train_code: ticket.station_train_code,
			from_station: ticket.from_station_telecode,
			to_station: ticket.to_station_telecode,
			pass_code: pass_code_new,
			location_code:ticket.location_code,
			yp_info: ticket.yp_info,
			train_no : ticket.train_no
		};

		var seatTypes = [];
		var ticketTypes = [];
		var passenger_id_types = [];
		var passenger_id_nos = [];
		var passenger_names = [];
		var mobile_nos = [];
		var save_passenger_flag = [];

		jq.each(getPassengerList(), function(i, passenger) {
			var seat_code = passenger.seat_type_code;
			if(seat_code == undefined || seat_code == ''){
				seat_code = passenger.seat_type;
			}
			var seat_type = ticket.getSeatType(seat_code);

			if(mode === "fc") {
				if (jq(".car-peop-list:eq("+i+") .bookBackTicketCheckBox").attr("checked") == "checked"){
					seatTypes.push(seat_type);
					ticketTypes.push(passenger.ticket_type);
					passenger_id_types.push(passenger.id_type);
					passenger_id_nos.push(passenger.id_no);
					passenger_names.push(passenger.user_name);
				}
			}else {
				seatTypes.push(seat_type);
				ticketTypes.push(passenger.ticket_type);
				passenger_id_types.push(passenger.id_type);
				passenger_id_nos.push(passenger.id_no);
				passenger_names.push(passenger.user_name);
				mobile_nos.push(passenger.mobile_no);
				save_passenger_flag.push("Y");
			}
		});

		jq.extend(parameters, {
			"seat_type_codes": seatTypes.join(","),
			"ticket_types": ticketTypes.join(","),
			"passenger_id_types": passenger_id_types.join(","),
			"passenger_id_nos": passenger_id_nos.join(","),
			"passenger_names": passenger_names.join(","),
			"ticket_type_order_num": mor.ticket.passengerList.length.toString(),
			"bed_level_order_num": "0", // 无用参数
			"start_time" : ticket.start_time,
			"tour_flag" : mode
		});

		if(mode === "fc"){
			jq.extend(parameters, {
				"sequence_no" :mor.ticket.orderManager.orderTicketList[0].sequence_no
			});
		} else {
			jq.extend(parameters, {
				"mobile_nos": mobile_nos.join(","),
				"save_passenger_flag": save_passenger_flag.join(",")
			});
		}

		return parameters;
	}

	function prepareChangeTicketParameters(){
		if(mor.ticket.orderManager.capchaNewOrOldFlag !== "2"){
			mor.ticket.orderManager.captchaDataStringify();
		}
		var util = mor.ticket.util;
		var ticket = mor.ticket.currentTicket;

		var seatTypes = [];
		var ticketTypes = [];
		var passenger_id_types = [];
		var passenger_id_nos = [];
		var passenger_names = [];

		var ori_coach_nos=[];
		var ori_seat_nos=[];
		var ori_batch_nos=[];
		var pass_code_new = "";
		if(mor.ticket.orderManager.capchaNewOrOldFlag == "2"){
			pass_code_new = jq("#captchaInput").val();
		}else{
			pass_code_new = mor.ticket.orderManager.captchaImageResult;
		}
		var parameters = {
			train_date: util.processDateCode(ticket.train_date),
			station_train_code: ticket.station_train_code,
			from_station_telecode: ticket.from_station_telecode,
			to_station_telecode: ticket.to_station_telecode,
			pass_code: pass_code_new,
			location_code:ticket.location_code,
			start_time: ticket.start_time,
			yp_info: ticket.yp_info,
			tour_flag : "gc",
			train_no : ticket.train_no
		};

		jq.each(getPassengerList(), function(i, passenger) {
			if (jq(".car-peop-list:eq("+i+") .changeTicketCheckBox").attr("checked") == "checked"){
				var seat_type = ticket.getSeatType(passenger.seat_type);
				seatTypes.push(seat_type);
				ticketTypes.push(passenger.ticket_type);
				passenger_id_types.push(passenger.id_type);
				passenger_id_nos.push(passenger.id_no);
				passenger_names.push(passenger.user_name);
			}
		});

		var originTicket = mor.ticket.queryOrder.changeTicketOrderList;

		for(var i=0;i<originTicket.length;i++){
			if (jq(".car-peop-list:eq("+i+") .changeTicketCheckBox").attr("checked") == "checked"){
				ori_coach_nos.push(originTicket[i].coach_no);
				ori_seat_nos.push(originTicket[i].seat_no);
				ori_batch_nos.push(originTicket[i].batch_no);
			}
		}


		jq.extend(parameters, {
			"seat_type_codes": seatTypes.join(","),
			"ticket_type_codes":ticketTypes.join(","),
			"ori_passenger_id_type_codes": passenger_id_types.join(","),
			"ori_passenger_id_nos": passenger_id_nos.join(","),
			"ori_passenger_names": passenger_names.join(","),
			"sequence_no" : originTicket[0].sequence_no,
			"ori_batch_nos" : ori_batch_nos.join(","),
			"ori_coach_nos" : ori_coach_nos.join(","),
			"ori_seat_nos" : ori_seat_nos.join(",")
		});

		return parameters;
	}

	// 生成用户系别选择列表
	function regsiterYpListFunHandher(){
		var boxTemp = "";
		var ticket = mor.ticket.currentTicket;

		var yplist = ticket.yplist;
		var defaultSeatType = ticket.getDefaultSeatType(ticket.default_seat_type);
		var nCount = 0;
		
		for (var i = 0, len = yplist.length ; i < len ; i++) {
			var yp = yplist[i],
				color = 'black',
				ticketNum = '无',
				num = yp.num,
				price = yp.price,
				disabled = ' disabled',
				seat_class = ' editBtnSeat';

	        if (defaultSeatType === yp.type_id) {
	        	seat_class = ' editBtnSeat hover';
	        }

			if (num > 0) {
				disabled = '';
				nCount = nCount + num;
				if (num <= 20) {
					ticketNum = num +"张";
					color="black";
				} else {
					ticketNum = num +"张";
					color="green";
				}
			}
			yp.discount = "0";
			//判断优惠席别
			for(var di = 0;di<ticket.yp_ex.length;di=di+2){
				if(ticket.yp_ex.substr(di+1,1) === '1'){
					if(yp.type_id === ticket.yp_ex.substr(di,1)){
						yp.discount = "1";
					}
				}
			}
			// 获取实际的 type 名称，包括无座.
			var typeName = ticket.getTypeName(yp.type_id);
			// 简化显示信息.
			var typeInfo = mor.ticket.util.repalceYpInfoType(typeName); // 用于获取车票信息简称
			/**
			 * if (typeName === '无座') { ticketNum = yp.type + ticketNum; }
			 */

			var lastDivClass;
			if (i === len -1) {
				lastDivClass = ' car-box-last';
			} else {
				lastDivClass = ' ';
			}

			var firstDivClass;
			if (i === 0) {
				firstDivClass = ' car-box-first ';
			} else {
				firstDivClass = ' ';
			}
			// 生成席位切换区域.
			if(yp.discount === "1"){
				boxTemp += '<div class="'+lastDivClass+firstDivClass+' car-box-five car-box-' + len  +
				'"><a data-val="' + yp.type_id + '"  class="'+ seat_class + lastDivClass + firstDivClass + disabled +
				'" ><div class="seatTypeDisCount">折</div><span class="car-txt-zw text-ellipsis" style="width:40px">'+ typeInfo +
				'</span><br /><span class="car-txt-num '+color+'">' + ticketNum +
				'</span><br /><span class="car-txt-jg">'+price.substr(0, price.length)+'</span></a></div>';
			}else{
				boxTemp += '<div class="'+lastDivClass+firstDivClass+' car-box-five car-box-' + len  +
				'"><a data-val="' + yp.type_id + '"  class="'+ seat_class + lastDivClass + firstDivClass + disabled +
				'" ><span class="car-txt-zw text-ellipsis" style="width:40px">'+ typeInfo +
				'</span><br /><span class="car-txt-num '+color+'">' + ticketNum +
				'</span><br /><span class="car-txt-jg">'+price.substr(0, price.length)+'</span></a></div>';
			}
			
		 }
		if(nCount != 0){
			canSubmit = true;
		}
		 jq(".car-box-list").html(boxTemp);
		 return;
    }
	
	var trainStationListTemplate = "<table class='stopStationTable'>"
		+"<tr class='hearders'>"
			+"<td style='width:15%'>站序</td>"
			+"<td style='width:25%'>站名</td>"
			+"<td style='width:20%'>到时</td>"
			+"<td style='width:20%'>发时</td>"
			+"<td style='width:20%'>停留</td>"
		+"</tr>"
		+"{{~it :station:index}}"
		+"{{ if(station.isEnabled == 'true'){ }}"
		+"<tr>"
		+"{{ if(station.start_time == station.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>{{=station.station_no}}</td><td style='width:20%;border-bottom:none;'>{{=station.station_name}}</td><td style='width:20%;border-bottom:none;'>{{=station.arrive_time}}</td><td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=station.station_no}}</td><td style='width:20%;'>{{=station.station_name}}</td><td style='width:20%;'>{{=station.arrive_time}}</td><td style='width:20%;'>{{=station.start_time}}</td>"
		+"{{ } }}"
		+"{{ if(station.stopover_time == '----'){ }}"
		+"{{ if(station.start_time == station.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>----</td>"
		+"{{ } }}"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=station.stopover_time}}</td>"
		+"{{ } }}"
		+"</tr>"
		+"{{ }else{ }}"
		+"<tr style='color:#999;'>"
		+"{{ if(station.start_time == station.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>{{=station.station_no}}</td><td style='width:20%;border-bottom:none;'>{{=station.station_name}}</td><td style='width:20%;border-bottom:none;'>{{=station.arrive_time}}</td><td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=station.station_no}}</td><td style='width:20%;'>{{=station.station_name}}</td><td style='width:20%;'>{{=station.arrive_time}}</td><td style='width:20%;'>{{=station.start_time}}</td>"
		+"{{ } }}"
		+"{{ if(station.stopover_time == '----'){ }}"
		+"{{ if(station.start_time == station.arrive_time){ }}"
		+"<td style='width:20%;border-bottom:none;'>----</td>"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>----</td>"
		+"{{ } }}"
		+"{{ }else{ }}"
		+"<td style='width:20%;'>{{=station.stopover_time}}</td>"
		+"{{ } }}"
		+"</tr>"
		+"{{ } }}"
		+"{{~}}"
		+"</table>";
	var generateTrainStationListContent = doT.template(trainStationListTemplate);
	
	/*选择日期*/
	function registSelectDateChangeHandler(){
		jq("#passengersChooseDateBtn").change(function(){
			var date = mor.ticket.util.setMyDate(jq("#passengersChooseDateBtn").val());
			var selectDate = new Date(date.getTime());
			mor.ticket.currentTicket.train_date = selectDate.format("yyyy-MM-dd");
			searchAndShowLeftTicketResult(mor.ticket.currentTicket);
		});
	}
	/*判断按钮是否可选*/
	function initDateBtn() {
		var util = mor.ticket.util;
		var date = util.setMyDate(mor.ticket.currentTicket.train_date);
		var nowDate = mor.ticket.util.getNewDate();
		var reservePeriod;
		reservePeriod = util.getReservedPeriod(mor.ticket.currentTicket.purpose_codes);
		/*if(mor.ticket.viewControl.bookMode === "gc"){
			if(mor.ticket.leftTicketQuery.isTwoday == true){
				reservePeriod = mor.ticket.leftTicketQuery.dayLimit;
			}
		}*/
		if (mor.ticket.viewControl.bookMode == "fc") {
			var orderManager=mor.ticket.orderManager;
			var preTicket = orderManager.getPreTicketDetail();
			// 返程开始日期
			var train_date_back = mor.ticket.util.setMyDate2(preTicket.train_date.replace(/-/g, ''));
			if (train_date_back >= date) {
				jq("#passengersPrevDateBtn").addClass("ui-disabled");
				jq("#passengersNextDateBtn").removeClass("ui-disabled");
			} else if (date - nowDate > (reservePeriod - 1) * 86400000) {
				jq("#passengersNextDateBtn").addClass("ui-disabled");
				jq("#passengersPrevDateBtn").removeClass("ui-disabled");
			} else {
				jq("#passengersPrevDateBtn").removeClass("ui-disabled");
				jq("#passengersNextDateBtn").removeClass("ui-disabled");
			}
		}else{
			if (date <= nowDate) {
				jq("#passengersPrevDateBtn").addClass("ui-disabled");
				jq("#passengersNextDateBtn").removeClass("ui-disabled");
			} else if (date - nowDate > (reservePeriod - 1) * 86400000) {
				jq("#passengersNextDateBtn").addClass("ui-disabled");
				jq("#passengersPrevDateBtn").removeClass("ui-disabled");
			} else {
				jq("#passengersPrevDateBtn").removeClass("ui-disabled");
				jq("#passengersNextDateBtn").removeClass("ui-disabled");
			}
		}
	}
	
	function registerPrevDateBtnClickHandler() {
		jq("#passengersPrevDateBtn").off().on("tap",
			function() {
				var date = mor.ticket.util
						.setMyDate(mor.ticket.currentTicket.train_date);
				mor.ticket.currentTicket.old_train_date = mor.ticket.currentTicket.train_date;
				var nowDate = mor.ticket.util.getNewDate();
				if (date > nowDate) {
					var preDate = new Date(date.getTime() - 24 * 60
							* 60 * 1000);
						mor.ticket.currentTicket.train_date = preDate
								.format("yyyy-MM-dd");
						jq("#passengersChooseDateBtn").val(mor.ticket.currentTicket.train_date);
						searchAndShowLeftTicketResult(mor.ticket.currentTicket);
				}
				return false;
			});
	}

	function registerNextDateBtnClickHandler() {
		jq("#passengersNextDateBtn").off().on("tap",
		function() {
			var date = mor.ticket.util.setMyDate(mor.ticket.currentTicket.train_date);
			mor.ticket.currentTicket.old_train_date = mor.ticket.currentTicket.train_date;
			var nowDate = mor.ticket.util.getNewDate();
			var reservePeriod;
			reservePeriod = mor.ticket.util.getReservedPeriod(mor.ticket.currentTicket.purpose_codes);
			/*if(mor.ticket.viewControl.bookMode === "gc"){
				if(mor.ticket.leftTicketQuery.isTwoday == true){
					reservePeriod = mor.ticket.leftTicketQuery.dayLimit;
				}
			}*/
			if (date - nowDate < (reservePeriod - 1) * 86400000) {
				var nextDate = new Date(date.getTime() + 24
						* 60 * 60 * 1000);
					mor.ticket.currentTicket.train_date = nextDate.format("yyyy-MM-dd");
				jq("#passengersChooseDateBtn").val(mor.ticket.currentTicket.train_date);	
				searchAndShowLeftTicketResult(mor.ticket.currentTicket);
			}
			return false;
		});
	}
	
	/*查询当前车次上一天或下一天车票信息*/
	function searchAndShowLeftTicketResult(query) {
		var util = mor.ticket.util;
		var commonParameters = null;
		var leftTicketQuery = mor.ticket.leftTicketQuery;
		commonParameters = {
			"train_date" : util.processDateCode(query.train_date),
			"purpose_codes" : leftTicketQuery.purpose_codes,
			"from_station" : query.from_station_telecode,
			"to_station" : query.to_station_telecode,
			"station_train_code" : query.station_train_code,
			"start_time_begin" : util.getStartTimeBeginCode(leftTicketQuery.time_period),
			"start_time_end" : util.getStartTimeEndCode(leftTicketQuery.time_period),
			"train_headers" : 'QB',
			"train_flag" : query.train_flag,
			"seat_type" : mor.ticket.leftTicketQuery.seat_Type,
			"seatBack_Type" : mor.ticket.leftTicketQuery.seatBack_Type,
			"ticket_num" : ""
		};

		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "queryLeftTicket"
		};
		var options = {
			onSuccess : requestLeftTicketSucceeded,
			onFailure : util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,options);
	}
	
	function requestLeftTicketSucceeded(result) {
		if (busy.isVisible()) {
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			canSubmit = true;
			queryStopStationFlag = "0";
			var tempTicket = invocationResult.ticketResult[0];
			tempTicket.yplist = processYpInfo(tempTicket.yp_info);
			var ticket = mor.ticket.currentTicket;
			ticket.arrive_time = tempTicket.arrive_time;
			ticket.end_station_telecode = tempTicket.end_station_telecode;
			ticket.from_station_telecode = tempTicket.from_station_telecode;
			ticket.lishi = tempTicket.lishi;
			ticket.location_code = tempTicket.location_code;
			ticket.start_station_telecode = tempTicket.start_station_telecode;
			ticket.start_time = tempTicket.start_time;
			ticket.start_train_date = tempTicket.start_train_date;
			ticket.station_train_code = tempTicket.station_train_code;
			ticket.to_station_telecode = tempTicket.to_station_telecode;
			ticket.train_no = tempTicket.train_no;
			ticket.yp_info = tempTicket.yp_info;
			ticket.yplist = tempTicket.yplist;
			//当有余票 刷新验证码
			if(tempTicket.yplist.length>=0){
				for(var i=0;i<tempTicket.yplist.length;i++){
					if(tempTicket.yplist[i].num>0){
						mor.ticket.orderManager.refreshCaptchaImg();
						break;
					}
				}
			}
			var message = tempTicket.message === "" ? "很抱歉，当日该车次票已售完" : tempTicket.message;
			// ticket num
			var hasticket = ticket.yplist.length;
			for ( var i = 0; i < ticket.yplist.length; i++) {
				if (!ticket.yplist[i].num) {
					hasticket--;
				}
			}
			if (!hasticket) {
				canSubmit = false;
				jq("#submitOrderBtn").addClass("ui-disabled");
				mor.ticket.util.alertMessage(message);
			}
			else if(jq("#submitOrderBtn").hasClass("ui-disabled")){
				jq("#submitOrderBtn").removeClass("ui-disabled");
			}
			// 提前预处理。
			processSeatTypeInCurrentTicket(ticket);
			var cache = mor.ticket.cache;
			regsiterYpListFunHandher();
			jq("#pfromStationName").html(cache.getStationNameByCode(ticket.from_station_telecode));
			jq("#ptrainStartTime").html(ticket.start_time + " 出发");
			jq("#ptrainCodeName").html(ticket.station_train_code);
			jq("#ptrainDurationTime").html(util.getLiShiStr(ticket.lishi));
			jq("#ptoStationName").html(cache.getStationNameByCode(ticket.to_station_telecode));
			jq("#ptrainReachTime").html(ticket.arrive_time + " 到达");
			if(ticket.start_station_telecode && ticket.from_station_telecode != ticket.start_station_telecode){
				jq("#pstartStationName").css("display","inline-block");
				jq("#innerStartStation").html(cache.getStationNameByCode(ticket.start_station_telecode));
			}else{
				jq("#pstartStationName").css("display","none");
			}
			
			if(ticket.end_station_telecode && ticket.to_station_telecode != ticket.end_station_telecode){
				jq("#pendStationName").css("display","inline-block");
				jq("#innerEndStation").html(cache.getStationNameByCode(ticket.end_station_telecode));
			}else{
				jq("#pendStationName").css("display","none");
			}
			ticketEditSeatBtnListeners();
			jq("#currentStopStationInfo").hide();
			jq("#queryStopStationBtn").removeClass("arrow-up");
			jq("#queryStopStationBtn").addClass("arrow-down");
			initDateBtn();
		} else {
			var errorMessage = invocationResult.error_msg;
			if(errorMessage.indexOf("该车次当天不开行")!=-1){
				errorMessage = "该车次当天已停止售票";
			}
			mor.ticket.currentTicket.train_date = mor.ticket.currentTicket.old_train_date;
			jq("#passengersChooseDateBtn").val(mor.ticket.currentTicket.train_date);
			initDateBtn();
			mor.ticket.util.alertMessage(errorMessage);
			return false;
		}
	};
	
	function processYpInfo(ypinfo) {
		var arrayLength = ypinfo.length / 10;
		var obj = new Array();
		var cache = mor.ticket.cache;
		var temp_seat_rate = 50;// 对于没有定义的座位，rate值从50开始递增
		for ( var i = 0, m = 6, n = 10, x = 0, y = 1; i < arrayLength; i++, m = m + 10, n = n + 10, x = x + 10, y = y + 10) {
			var seat_type_id = ypinfo.substring(x, y);
			var seat_num = 0;
			var seat_type_rate = null;
			if (parseInt(ypinfo.substring(m, m + 1), 10) >= 3) {
				seat_type = "无座";
				seat_type_rate = 100;
				seat_num = parseInt(ypinfo.substring(m, n), 10)-3000;
			} else {
				seat_type = cache.getSeatTypeByCode(seat_type_id);
				seat_type_rate = cache.getSeatTypeRateByCode(seat_type_id);
				if (seat_type_rate === null || seat_type_rate === "undefined") {
					seat_type_rate = temp_seat_rate;
					temp_seat_rate = temp_seat_rate + 1;
				}
				seat_num = parseInt(ypinfo.substring(m, n), 10);
			}
			var fareNewPrice = '¥'+(parseFloat(ypinfo.substring(y, m), 10) / 10).toFixed(1);
			if(fareNewPrice == '¥0.0'){
				fareNewPrice = "--";
			}
			obj[i] = {
				type_id : seat_type_id,
				type : seat_type,
				type_rate : seat_type_rate,
				num : seat_num,
				price : fareNewPrice
			};
		}
		return sortYpInfo(obj);
	}
	
	function sortYpInfo(ypinfoArray) {
		
		var by = function(name) {
			return function(o, p) {
				var a, b;
				if (typeof o === "object" && typeof p === "object" && o && p) {
					a = o[name];
					b = p[name];
					if (a === b) {
						return 0;
					}
					if (typeof a === typeof b) {
						return a < b ? -1 : 1;
					}
					return typeof a < typeof b ? -1 : 1;
				} else {
					throw ("error");
				}
			};
		};

		ypinfoArray.sort(by("type_rate"));
		return ypinfoArray;

	}
})();