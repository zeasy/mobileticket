
/* JavaScript content from js/controller/searchResultScreenView.js in folder common */
(function() {
	var focusArray = [];
	jq("#searchResultScreenView").live("pagebeforeshow", function() {
		
		jq("#fromStationInfo").html("");
		jq("#toStationInfo").html("");
		jq("#trainSeatType").html("");
		jq("#trainSeatTypeNext").html("");
		jq("#trainSeatTypeEnd").html("");
		focusArray = [];
		clearFilterCondition("0");
		mor.ticket.searchResultScreen.chooseQueryFlag = false;
		mor.ticket.searchResultScreen.exitFlag = false;
		mor.ticket.searchResultScreen.setQueryCondition();
		setTimeout(renderAll(mor.ticket.searchResultScreen.from_station_telecode,1), 100);
		setTimeout(renderAll(mor.ticket.searchResultScreen.to_station_telecode,2), 100);
		setTimeout(renderseatType(mor.ticket.searchResultScreen.seat_type_list,1), 100);
		var query = mor.ticket.leftTicketQuery;
		//初始化选中的train_hearder
		var train_hearders_array = mor.ticket.searchResultScreen.trainHeaders;
		var util = mor.ticket.util;
		for ( var i = 0, len = jq("#searchResultScreenHeadersBtnGroup a").length; i < len; i++) {
			var jq_a = jq("#searchResultScreenHeadersBtnGroup a:eq("
					+ i + ")");
			if (jq.inArray(jq_a.attr("name"),train_hearders_array) != -1) {
				jq_a.addClass("ui-btn-active ui-state-persist");
			}else{
				jq_a.removeClass("ui-btn-active ui-state-persist");
			}
		}
		if(train_hearders_array.length == 0){
			jq("#searchResultScreenQB").tap();
		}
		var start_time = mor.ticket.searchResultScreen.start_time;
		jq("#SetOutTimeSelect option").eq(parseInt(getOutTimeSelect(start_time))).attr("selected",true);
		if(start_time != "0024"){
			jq("#SetOutTimeSelect-button").css("color","#298CCF");
		}else{
			jq("#SetOutTimeSelect-button").css("color","#000");
		}
		var arrive_time = mor.ticket.searchResultScreen.arrive_time;
		jq("#ArriveTimeSelect option").eq(parseInt(getOutTimeSelect(arrive_time))).attr("selected",true);
		if(arrive_time == "0024"){
			jq("#ArriveTimeSelect-button").css("color","#000");
		}else{
			jq("#ArriveTimeSelect-button").css("color","#298CCF");
		}
		jq("#SetOutTimeSelect").selectmenu('refresh', true);
		jq("#ArriveTimeSelect").selectmenu('refresh', true);
		if(!mor.ticket.searchResultScreen.begin_station_flag){
			jq("#beginStation").removeClass("ui-btn-active ui-state-persist");
		}else{
			jq("#beginStation").addClass("ui-btn-active ui-state-persist");
		}
		if(!mor.ticket.searchResultScreen.end_station_flag){
			jq("#endStation").removeClass("ui-btn-active ui-state-persist");
		}else{
			jq("#endStation").addClass("ui-btn-active ui-state-persist");
		}
		if(!mor.ticket.searchResultScreen.only_ticket){
			jq("#showTicket").removeClass("ui-btn-active ui-state-persist");
		}else{
			jq("#showTicket").addClass("ui-btn-active ui-state-persist");
		}
//		showTrainHeaders();
		var width_btn_end = Math.ceil(document.documentElement.clientWidth*0.1551+50);
		jq("#showTicket").css('width',width_btn_end+"px");
	});
	function getOutTimeSelect(start_time){
		switch (start_time) {
		case '0024':
			return '0';
			break;
		case '0006':
			return '1';
			break;
		case '0612':
			return '2';
			break;
		case '1218':
			return '3';
			break;
		case '1824':
			return '4';
			break;
		default:
			console.log("Invalid start_time: " + start_time);
			return '0';
		}
	}
	function getStartTimes(time_period) {
		switch (time_period) {
		case '0':
			return "0024";
			break;
		case '1':
			return "0600";
			break;
		case '2':
			return "1200";
			break;
		case '3':
			return "1800";
			break;
		case '4':
			return "2400";
			break;
		default:
			console.log("Invalid time_period: "
					+ time_period);
			return "0024";
		}
	}
	
	
	jq("#searchResultScreenView").live("pageinit", function() {
		mor.ticket.searchResultList.leftOrPriceFlag = "2";
		jq("#searchResultScreenBackBtn").off().bind("tap", function() {
			mor.ticket.searchResultScreen.exitFlag = true;
			mor.ticket.util.changePage("searchSingleTicketResults.html");
			return false;
		});
		registerTrainHeadersBtnGroupClickHandler();
		screenFromBtnGroupClickHandler();
		screenBeginAndEndBtnGroupClickHandler();
		resultSrceenBtnClickHandler();
		registerSelectSeatTypeListener();
		registerResetBtnListener();
		registerAutoScroll();
		registerSetOutTimeSelectChangeHandler();
		registerArriveTimeSelectChangeHandler();
	});
	function registerSetOutTimeSelectChangeHandler() {
		mor.ticket.util.bindSelectFocusBlurListener("#SetOutTimeSelect");
		jq("#SetOutTimeSelect").bind("change", function() {
			if(jq(this).val() == "0024"){
				jq("#SetOutTimeSelect-button").css("color","black");
			}else{
				jq("#SetOutTimeSelect-button").css("color","#298CCF");
			}
			return false;
		});
	};
	function registerArriveTimeSelectChangeHandler() {
		mor.ticket.util.bindSelectFocusBlurListener("#ArriveTimeSelect");
		jq("#ArriveTimeSelect").bind("change", function() {
			if(jq(this).val() == "0024"){
				jq("#ArriveTimeSelect-button").css("color","black");
			}else{
				jq("#ArriveTimeSelect-button").css("color","#298CCF");
			}
			return false;
		});
	};
	function registerAutoScroll() {
		var util = mor.ticket.util;
		if (util.isIPhone()) {
			util.enableAutoScroll('#ArriveTimeSelect', focusArray);
			util.enableAutoScroll('#SetOutTimeSelect',focusArray);
		}
	}
	
	function clearFilterCondition(direction){
		if(!mor.ticket.searchResultScreen.chooseQueryFlag){
			// 车票预订直接跳转到筛选页面标志 0
			if(direction !== "0"){
				mor.ticket.searchResultScreen.trainHeaders = [];
				mor.ticket.searchResultScreen.seat_type_checked = [];
				mor.ticket.searchResultScreen.start_time = "0024";
				jq("#SetOutTimeSelect-button").css("color","#000");
				jq("#searchResultScreenQB").tap();
				jq("#SetOutTimeSelect option").eq(0).attr("selected",true);
			}
			mor.ticket.searchResultScreen.begin_station_flag = false;
			mor.ticket.searchResultScreen.end_station_flag = false;
			mor.ticket.searchResultScreen.only_ticket = false;
			mor.ticket.searchResultScreen.from_station_checked = [];
			mor.ticket.searchResultScreen.to_station_checked = [];
			mor.ticket.searchResultScreen.arrive_time = "0024";
			jq("#ArriveTimeSelect-button").css("color","#000");
			jq("#ArriveTimeSelect option").eq(0).attr("selected",true);
			if(jq("#beginStation,#endStation,#showTicket").hasClass("ui-btn-active ui-state-persist")){
				jq("#beginStation").removeClass("ui-btn-active ui-state-persist");
				jq("#endStation").removeClass("ui-btn-active ui-state-persist");
				jq("#showTicket").removeClass("ui-btn-active ui-state-persist");
			}
		}
		mor.ticket.searchResultScreen.from_station_telecode = [];
		mor.ticket.searchResultScreen.to_station_telecode = [];
		mor.ticket.searchResultScreen.seat_type_list = [];
		mor.ticket.searchResultScreen.seat_type_enabled = ["X"];
		jq("#SetOutTimeSelect").selectmenu('refresh', true);
		jq("#ArriveTimeSelect").selectmenu('refresh', true);
	}
	
	function registerResetBtnListener(){
		jq("#resetBtn").on("tap",function(e){
			e.stopImmediatePropagation();
			jq("#searchResultScreenQB").tap();
			setTimeout(renderAll(mor.ticket.searchResultScreen.from_station_telecode, 3), 100);
			setTimeout(renderAll(mor.ticket.searchResultScreen.to_station_telecode, 4), 100);
			setTimeout(renderseatType(mor.ticket.searchResultScreen.seat_type_list,0), 100);
			if(jq("#beginStation,#endStation,#showTicket").hasClass("ui-btn-active ui-state-persist")){
				jq("#beginStation").removeClass("ui-btn-active ui-state-persist");
				jq("#endStation").removeClass("ui-btn-active ui-state-persist");
				jq("#showTicket").removeClass("ui-btn-active ui-state-persist");
			}
			jq("#ArriveTimeSelect-button").css("color","#000");
			jq("#ArriveTimeSelect option").eq(0).attr("selected",true);
			jq("#SetOutTimeSelect-button").css("color","#000");
			jq("#SetOutTimeSelect option").eq(0).attr("selected",true);
			jq("#SetOutTimeSelect").selectmenu('refresh', true);
			jq("#ArriveTimeSelect").selectmenu('refresh', true);
		});
	}
	function resultSrceenBtnClickHandler() {
		jq("#resultSrceenBtn")
				.bind(
						"tap",
						function(e) {
							e.stopImmediatePropagation();
							mor.ticket.searchResultScreen.result.length = 0;
							var checkedCount = 0;
							mor.ticket.searchResultScreen.from_station_checked = [];
							mor.ticket.searchResultScreen.to_station_checked = [];
							mor.ticket.searchResultScreen.seat_type_checked = [];
							mor.ticket.searchResultScreen.begin_station_flag = false;
							mor.ticket.searchResultScreen.end_station_flag = false;
							mor.ticket.searchResultScreen.only_ticket = false;
							mor.ticket.searchResultScreen.trainHeaders = [];
							for ( var i = 0, len = jq("#searchResultScreenHeadersBtnGroup a").length; i < len; i++) {
								var jq_a = jq("#searchResultScreenHeadersBtnGroup a:eq("
										+ i + ")");
								if (jq_a
										.hasClass("ui-btn-active ui-state-persist")) {
									mor.ticket.searchResultScreen.trainHeaders.push(jq_a.attr("name"));
								}
							}
							mor.ticket.leftTicketQuery.train_headers = "";
							for(var i=0;i<mor.ticket.searchResultScreen.trainHeaders.length;i++){
								mor.ticket.leftTicketQuery.train_headers += mor.ticket.searchResultScreen.trainHeaders[i]+"#";
							}
							if(mor.ticket.searchResultScreen.trainHeaders.length > 0){
								checkedCount++;
							}
							for ( var i = 0, len = jq("#fromStationInfo a").length; i < len; i++) {
								var jq_a = jq("#fromStationInfo a:eq(" + i
										+ ")");
								if (jq_a
										.hasClass("ui-btn-active ui-state-persist")) {
									mor.ticket.searchResultScreen.from_station_checked.push(jq_a.attr("id")) ;
									checkedCount++;
								}
							}
							for ( var i = 0, len = jq("#toStationInfo a").length; i < len; i++) {
								var jq_a = jq("#toStationInfo a:eq(" + i + ")");
								if (jq_a
										.hasClass("ui-btn-active ui-state-persist")) {
									mor.ticket.searchResultScreen.to_station_checked.push(jq_a.attr("id"));
									checkedCount++;
								}
							}
							for ( var i = 0, len = jq("#trainSeatType a").length; i < len; i++) {
								var jq_a = jq("#trainSeatType a:eq(" + i + ")");
								if (jq_a
										.hasClass("ui-btn-active ui-state-persist")) {
									mor.ticket.searchResultScreen.seat_type_checked.push(jq_a.attr("id"));
									checkedCount++;
								}
							}
							for ( var i = 0, len = jq("#trainSeatTypeNext a").length; i < len; i++) {
								var jq_a = jq("#trainSeatTypeNext a:eq(" + i + ")");
								if (jq_a
										.hasClass("ui-btn-active ui-state-persist")) {
									mor.ticket.searchResultScreen.seat_type_checked.push(jq_a.attr("id"));
									checkedCount++;
								}
							}
							for ( var i = 0, len = jq("#trainSeatTypeEnd a").length; i < len; i++) {
								var jq_a = jq("#trainSeatTypeEnd a:eq(" + i + ")");
								if (jq_a
										.hasClass("ui-btn-active ui-state-persist")) {
									mor.ticket.searchResultScreen.seat_type_checked.push(jq_a.attr("id"));
									checkedCount++;
								}
							}
							
							mor.ticket.leftTicketQuery.seat_Type = "0";
							
							mor.ticket.searchResultScreen.start_time = jq("#SetOutTimeSelect").val();
							mor.ticket.leftTicketQuery.time_period = "";
							var start_time_period = getOutTimeSelect(mor.ticket.searchResultScreen.start_time);
							if(mor.ticket.viewControl.bookMode == "fc"){
								mor.ticket.leftTicketQuery.time_period_back =  start_time_period;
							}else{
								mor.ticket.leftTicketQuery.time_period = start_time_period;
							}
							
							if(mor.ticket.searchResultScreen.start_time != "0024"){
								checkedCount++;
							}
							mor.ticket.searchResultScreen.arrive_time = jq("#ArriveTimeSelect").val();
							if(mor.ticket.searchResultScreen.arrive_time != "0024"){
								checkedCount++;
							}
							if(jq("#beginStation").hasClass("ui-btn-active ui-state-persist")){
								mor.ticket.searchResultScreen.begin_station_flag = true;
								checkedCount++;
							}
							if(jq("#endStation").hasClass("ui-btn-active ui-state-persist")){
								mor.ticket.searchResultScreen.end_station_flag = true;
								checkedCount++;
							}
							if(jq("#showTicket").hasClass("ui-btn-active ui-state-persist")){
								mor.ticket.searchResultScreen.only_ticket = true;
								checkedCount++;
							}
							if(checkedCount > 0){
								mor.ticket.searchResultScreen.chooseQueryFlag = true;
							}else{
								mor.ticket.searchResultScreen.chooseQueryFlag = false;
							}
							mor.ticket.searchResultScreen.checkedCount = checkedCount;
							mor.ticket.util.changePage("searchSingleTicketResults.html");
							return false;
						});

	}
	
	function registerTrainHeadersBtnGroupClickHandler() {
		jq("#searchResultScreenHeadersBtnGroup")
				.on(
						"tap",
						"a",
						function(e) {
							e.stopImmediatePropagation();
							if(!mor.ticket.datebox.calbox){
								return false;
							}
							var trainHeaders = mor.ticket.leftTicketQuery.train_headers.split("#");
							trainHeaders.splice(trainHeaders.length-1,1);
							var util = mor.ticket.util;
							var activeCls = "ui-btn-active ui-state-persist";
							var that = jq(this);
							var name = that.attr('name');
							if (that.hasClass(activeCls)) {
								if (name != "QB") {
									var nCount = jq("#searchResultScreenHeadersBtnGroup a.ui-btn-active").length;
									if(!util.indexOf(trainHeaders,"QB")){
										if(nCount == 1){
											util.alertMessage("请至少选择一个车次类型");
											return false;
										}
									}
									that.removeClass(activeCls);
									if (!jq(
											"#searchResultScreenD,#searchResultScreenZ,#searchResultScreenT,#searchResultScreenK,#searchResultScreenQT")
											.hasClass(activeCls)) {
										jq("#searchResultScreenQB").addClass(
												activeCls);
									}
								}
							} else {
								if (name == "QB") {
									that.addClass(activeCls).siblings()
											.removeClass(activeCls);
								} else {
									that.addClass(activeCls);
									if (jq("#searchResultScreenQB").hasClass(
											activeCls)) {
										jq("#searchResultScreenQB")
												.removeClass(activeCls);
									}
								}
							}
							return false;
						});
	}

	function screenFromBtnGroupClickHandler() {
		jq("#fromStationInfo,#toStationInfo").on("tap", "a", function(e) {
			e.stopImmediatePropagation();
			var activeCls = "ui-btn-active ui-state-persist";
			var that = jq(this);
			if (that.hasClass(activeCls)) {
				that.removeClass(activeCls);
			} else {
				that.addClass(activeCls);
			}
			return false;
		});
	}
	
	function screenBeginAndEndBtnGroupClickHandler() {
		jq("#beginStation,#endStation,#showTicket").on("tap", function(e) {
			e.stopImmediatePropagation();
			var activeCls = "ui-btn-active ui-state-persist";
			var that = jq(this);
			if (that.hasClass(activeCls)) {
				that.removeClass(activeCls);
			} else {
				that.addClass(activeCls);
			}
			return false;
		});
	}

	function registerSelectSeatTypeListener() {
		jq("#trainSeatType,#trainSeatTypeNext,#trainSeatTypeEnd").on("tap", "a", function(e) {
			e.stopImmediatePropagation();
			var activeCls = "ui-btn-active ui-state-persist";
			var that = jq(this);
			if(that.hasClass("ui-disabled")){
				return false;
			}
			if (that.hasClass(activeCls)) {
				that.removeClass(activeCls);
			} else {
				that.addClass(activeCls);
			}
			return false;
		});
	}
	
//	function showTrainHeaders(){
//		var trainHeaders = mor.ticket.leftTicketQuery.train_headers.split("#");
//		trainHeaders.splice(trainHeaders.length-1,1);
//		var checkedTrainHeaders = mor.ticket.searchResultScreen.trainHeaders;
//		var util = mor.ticket.util;
//		var activeCls = "ui-btn-active ui-state-persist";
//		if(!util.indexOf(trainHeaders,"QB")){
//			if(jq("#searchResultScreenQB").hasClass(activeCls)){
//				jq("#searchResultScreenQB").removeClass(activeCls);
//			}
//			for ( var i = 0, len = jq("#searchResultScreenHeadersBtnGroup a").length; i < len; i++) {
//				var jq_a = jq("#searchResultScreenHeadersBtnGroup a:eq("+ i + ")");
//				if(jq_a.hasClass("ui-disabled")){
//					jq_a.removeClass("ui-disabled");
//				}
//				if(!util.indexOf(trainHeaders,jq_a.attr("name"))){
//					jq_a.addClass("ui-disabled");
//				}else if(util.indexOf(checkedTrainHeaders,jq_a.attr("name"))){
//					jq_a.addClass(activeCls);
//				}else{
//					jq_a.removeClass(activeCls);
//				}
//			}
//		}else{
//			for ( var i = 0, len = jq("#searchResultScreenHeadersBtnGroup a").length; i < len; i++) {
//				var jq_a = jq("#searchResultScreenHeadersBtnGroup a:eq("+ i + ")");
//				if(jq_a.hasClass("ui-disabled")){
//					jq_a.removeClass("ui-disabled");
//				}
//			}
//		}
//	}
	function renderAll(datas, flag) {
		var jq_ScreenTrain_info = '';
		var cls = "";
		var station_checked = [];
		if(flag == 1){
			station_checked = mor.ticket.searchResultScreen.from_station_checked;
		}else if(flag == 2){
			station_checked = mor.ticket.searchResultScreen.to_station_checked;
		}
		var util = mor.ticket.util;
		for ( var i = 0; i < datas.length; i++) {
			if(station_checked.length > 0 && jq.inArray(datas[i].split(",")[0],station_checked) != -1){
				cls = "ui-btn-active ui-state-persist";
			}else{
				cls = "";
			}
			if(mor.ticket.cache.getStationNameByCode(datas[i].split(",")[0]) == undefined || mor.ticket.cache.getStationNameByCode(datas[i].split(",")[0]) == "undefined"){
				jq_ScreenTrain_info = jq_ScreenTrain_info
				+ '<a name="stationName" id='
				+ datas[i].split(",")[0]
				+ ' href="#" data-role="button" class="ui-link ui-btn '+cls+'">'
				+ datas[i].split(",")[1] + '</a>';
			}else{
				jq_ScreenTrain_info = jq_ScreenTrain_info
				+ '<a name="stationName" id='
				+ datas[i].split(",")[0]
				+ ' href="#" data-role="button" class="ui-link ui-btn '+cls+'">'
				+ mor.ticket.cache.getStationNameByCode(datas[i].split(",")[0]) + '</a>';
			}
		}
		for ( var j = datas.length; j < 3; j++) {
			jq_ScreenTrain_info = jq_ScreenTrain_info
					+ '<a href="#" data-role="button" class="ui-link ui-btn ui-disabled"></a>';
		}
		if (jq_ScreenTrain_info != '') {
			if (flag == 1 || flag == 3) {
				jq_ScreenTrain_info = '<div class="ui-controlgroup-controls"> <a href="#" data-role="button" class="ui-link ui-btn ui-disabled ui-first-child">发站</a>'
						+ jq_ScreenTrain_info + "</div>";
				jq("#fromStationInfo").html(jq_ScreenTrain_info);
			}else if (flag == 2 || flag == 4){
				jq_ScreenTrain_info = '<div class="ui-controlgroup-controls"> <a href="#" data-role="button" class="ui-link ui-btn ui-disabled ui-first-child">到站</a>'
						+ jq_ScreenTrain_info + "</div>";
				jq("#toStationInfo").html(jq_ScreenTrain_info);
			}
		}
		jq("#searchResultScreenView .iscroll-wrapper").iscrollview("refresh");
	}
	function renderseatType(seatTypeIdData,flag) {
		var jq_ScreenSeatType_info = '';
		var jq_ScreenSeatTypeNext_info = '';
		var cls = "";
		var util = mor.ticket.util;
		var seat_type_checked = mor.ticket.searchResultScreen.seat_type_checked;
		if(flag == 0){
			seat_type_checked = [];
		}
		if(seatTypeIdData.length > 10){
			showOverTenSeatType(seatTypeIdData,flag);
		}
		else if (seatTypeIdData.length > 5) {
			jq("#trainSeatTypeEnd").hide();
			jq("#trainSeatTypeNext").show();
			for ( var i = 0; i < 5; i++) {
				if(seat_type_checked.length > 0 && jq.inArray(seatTypeIdData[i],seat_type_checked) != -1){
					cls = "ui-btn-active ui-state-persist";
				}else{
					cls = "";
				}
				jq_ScreenSeatType_info = jq_ScreenSeatType_info
						+ '<a name="seatTypeName" id='
						+ seatTypeIdData[i]
						+ ' href="#" data-role="button" class="ui-link ui-btn '+cls+'">'
						+  repalceYpInfoType(util.getSeatTypeName(seatTypeIdData[i])) + '</a>';
			}
			if (jq_ScreenSeatType_info != '') {
				jq_ScreenSeatType_info = '<div class="ui-controlgroup-controls"> <a href="#" data-role="button" class="ui-link ui-btn ui-disabled ui-first-child">席别</a>'
					+ jq_ScreenSeatType_info + "</div>";				
				jq("#trainSeatType").html(jq_ScreenSeatType_info);
			}
			for ( var i = 5; i < seatTypeIdData.length; i++) {
				if(seat_type_checked.length > 0 && jq.inArray(seatTypeIdData[i],seat_type_checked) != -1){
					cls = "ui-btn-active ui-state-persist";
				}else{
					cls = "";
				}
				jq_ScreenSeatTypeNext_info = jq_ScreenSeatTypeNext_info
				+ '<a name="seatTypeName" id='
				+ seatTypeIdData[i]
				+ ' href="#" data-role="button" class="ui-link ui-btn '+cls+'">'
				+ repalceYpInfoType(util.getSeatTypeName(seatTypeIdData[i])) + '</a>';
			}
			for(var j = seatTypeIdData.length;j<10;j++){
				jq_ScreenSeatTypeNext_info = jq_ScreenSeatTypeNext_info
					+ '<a href="#" data-role="button" class="ui-link ui-btn ui-disabled"></a>';
			}
			if (jq_ScreenSeatTypeNext_info != '') {
				jq_ScreenSeatTypeNext_info = '<div class="ui-controlgroup-controls"> <a href="#" data-role="button" class="ui-link ui-btn ui-disabled ui-first-child"></a>'
					+ jq_ScreenSeatTypeNext_info + "</div>";
				jq("#trainSeatTypeNext").html(jq_ScreenSeatTypeNext_info);
			}
		}else{
			jq("#trainSeatTypeNext").hide();
			jq("#trainSeatTypeEnd").hide();
			for ( var i = 0; i < seatTypeIdData.length; i++) {
				if(seat_type_checked.length > 0 && jq.inArray(seatTypeIdData[i],seat_type_checked) != -1){
					cls = "ui-btn-active ui-state-persist";
				}else{
					cls = "";
				}
				jq_ScreenSeatType_info = jq_ScreenSeatType_info
						+ '<a name="seatTypeName" id='
						+ seatTypeIdData[i]
						+ ' href="#" data-role="button" class="ui-link ui-btn '+cls+'">'
						+ repalceYpInfoType(util.getSeatTypeName(seatTypeIdData[i])) + '</a>';
			}
			for(var j = seatTypeIdData.length;j<5;j++){
				jq_ScreenSeatType_info = jq_ScreenSeatType_info
					+ '<a href="#" data-role="button" class="ui-link ui-btn ui-disabled"></a>';
			}
			if (jq_ScreenSeatType_info != '') {
				jq_ScreenSeatType_info = '<div class="ui-controlgroup-controls"> <a href="#" data-role="button" class="ui-link ui-btn ui-disabled ui-first-child">席别</a>'
					+ jq_ScreenSeatType_info + "</div>";	
				jq("#trainSeatType").html(jq_ScreenSeatType_info);
			}
		}
		jq("#searchResultScreenView .iscroll-wrapper").iscrollview("refresh");
	}
	function showOverTenSeatType(seatTypeIdData,flag){
		var jq_ScreenSeatType_info = "";
		var jq_ScreenSeatTypeNext_info = "";
		var jq_ScreenSeatTypeEnd_info = "";
		var seat_type_checked = mor.ticket.searchResultScreen.seat_type_checked;
		if(flag == 0){
			seat_type_checked = [];
		}
		var cls = "";
		var util = mor.ticket.util;
		for ( var i = 0; i < 5; i++) {
			if(seat_type_checked.length > 0 && jq.inArray(seatTypeIdData[i],seat_type_checked) != -1){
				cls = "ui-btn-active ui-state-persist";
			}else{
				cls = "";
			}
			jq_ScreenSeatType_info = jq_ScreenSeatType_info
					+ '<a name="seatTypeName" id='
					+ seatTypeIdData[i]
					+ ' href="#" data-role="button" class="ui-link ui-btn '+cls+'">'
					+  repalceYpInfoType(util.getSeatTypeName(seatTypeIdData[i])) + '</a>';
		}
		if (jq_ScreenSeatType_info != '') {
			jq_ScreenSeatType_info = '<div class="ui-controlgroup-controls"> <a href="#" data-role="button" class="ui-link ui-btn ui-disabled ui-first-child">席别</a>'
				+ jq_ScreenSeatType_info + "</div>";				
			jq("#trainSeatType").html(jq_ScreenSeatType_info);
		}
		for ( var i = 5; i < 10; i++) {
			if(seat_type_checked.length > 0 && util.indexOf(seat_type_checked,seatTypeIdData[i])){
				cls = "ui-btn-active ui-state-persist";
			}else{
				cls = "";
			}
			jq_ScreenSeatTypeNext_info = jq_ScreenSeatTypeNext_info
			+ '<a name="seatTypeName" id='
			+ seatTypeIdData[i]
			+ ' href="#" data-role="button" class="ui-link ui-btn '+cls+'">'
			+ repalceYpInfoType(util.getSeatTypeName(seatTypeIdData[i])) + '</a>';
		}
		if (jq_ScreenSeatTypeNext_info != '') {
			jq_ScreenSeatTypeNext_info = '<div class="ui-controlgroup-controls"> <a href="#" data-role="button" class="ui-link ui-btn ui-disabled ui-first-child"></a>'
				+ jq_ScreenSeatTypeNext_info + "</div>";
			jq("#trainSeatTypeNext").html(jq_ScreenSeatTypeNext_info);
		}
		for ( var i = 10; i < seatTypeIdData.length; i++) {
			if(seat_type_checked.length > 0 && jq.inArray(seatTypeIdData[i],seat_type_checked) != -1){
				cls = "ui-btn-active ui-state-persist";
			}else{
				cls = "";
			}
			jq_ScreenSeatTypeEnd_info = jq_ScreenSeatTypeEnd_info
					+ '<a name="seatTypeName" id='
					+ seatTypeIdData[i]
					+ ' href="#" data-role="button" class="ui-link ui-btn '+cls+'">'
					+ repalceYpInfoType(util.getSeatTypeName(seatTypeIdData[i])) + '</a>';
		}
		for(var j = seatTypeIdData.length;j<15;j++){
			jq_ScreenSeatTypeEnd_info = jq_ScreenSeatTypeEnd_info
				+ '<a href="#" data-role="button" class="ui-link ui-btn ui-disabled"></a>';
		}
		if (jq_ScreenSeatTypeEnd_info != '') {
			jq_ScreenSeatTypeEnd_info = '<div class="ui-controlgroup-controls"> <a href="#" data-role="button" class="ui-link ui-btn ui-disabled ui-first-child"></a>'
				+ jq_ScreenSeatTypeEnd_info + "</div>";	
			jq("#trainSeatTypeEnd").html(jq_ScreenSeatTypeEnd_info);
		}
		jq("#trainSeatTypeNext").show();
		jq("#trainSeatTypeEnd").show();
	}
	
	function repalceYpInfoType(objs) {
			switch (objs) {
			case "商务座":
				objs = "商务";
				break;
			case "特等座":
				objs = "特等";
				break;
			case "一等座":
				objs = "一等";
				break;
			case "二等座":
				objs = "二等";
				break;
			case "高级软卧":
				objs = "高软";
				break;
			case "观光座":
				objs = "观光";
				break;
			case "一等包座":
				objs = "一等包";
				break;
			case "混编硬座":
				return "混编";
			}
		return objs;
	}
})();