//
//  ZZNativeFunction.m
//  MobileTicket
//
//  Created by EASY on 15/11/17.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZNativeFunction.h"
#import <RCTConvert.h>
#import <AVCloud.h>

@implementation ZZNativeFunction

RCT_EXPORT_MODULE(NativeFunction)



#pragma mark - Function
RCT_EXPORT_METHOD(call:(NSString *) func parameters:(NSDictionary *) parameters successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	
	func = [RCTConvert NSString:func];
	parameters = [RCTConvert NSDictionary:parameters];
	[AVCloud callFunctionInBackground:func withParameters:parameters block:^(id object, NSError *error) {
		if (error) {
			fail(error);
		} else {
			success(@[object]);
		}
	}];
}


@end
