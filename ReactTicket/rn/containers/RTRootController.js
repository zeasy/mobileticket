/**
 * Created by easy on 16/3/17.
 */


import React,{
    Component,
    View,
    TouchableOpacity,
    Text,
    StyleSheet
} from 'react-native';



class RTRootController extends Component {

    componentWillMount() {

        this.props.navigator.replace({
            component : require('../Web/containers/TWRootController')
        });
    }

    onMobilePress() {
        this.props.navigator.push({
            component : require('../Mobile').rootController()
        });
    }

    onWebPress() {
        this.props.navigator.push({
            component : require('../Web').rootController()
        });
    }

    render() {
        return (
            <View style={styles.container}>
            <TouchableOpacity onPress={this.onMobilePress.bind(this)} style={{height:40}}>
                <Text>移动版</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.onWebPress.bind(this)} style={{height:40}}>
                <Text>Web版</Text>
            </TouchableOpacity>
        </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});

module.exports = RTRootController;