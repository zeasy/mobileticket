
/* JavaScript content from js/controller/sendTicketVisit.js in folder common */
(function(){
	
	jq.extendModule("mor.ticket.sendTicketAddress", {
	       sendTicketList : [],
	       sendOrderList : [],
		   thisAddress : '',
		   selected : false
		});
	
	jq.extendModule("mor.ticket.sendTicketOrderInfo", {
		order_no : 0,
		address_name : '',
		address_detail : '',
		address_addr : '',
		address_tel : '',
		address_date : '',
		ticketList : []
	});
	jq.extendModule("mor.ticket.sendTicketAddressLists", {
		
	});
	var util = mor.ticket.util;
	var addSendTicketAdressCount = 1;
	var sendTicketLists = []; // 当前待支付订单所有车票列表
	var unSendTicketLists = [];// 未保存送票的车票列表
	var addUnSendTicketLists = [];// 未勾选的车票列表
	var sendOrderList = [];// 订单列表
	var expressTickets = [];// 已保存送票订单列表
	var selectSendTicketLists = [];
	var cancelUpdataSendTicket = [];
	var cancelUpdataSendTicketSave = [];
	//var sendTicketAddressLists = [];
	var thisSaveUpateBtn = "";
	var thisSaveBtn = "";
	var thisUpdateBtn = "";
	var thisDeleteBtn = "";
	var thisCancelUpdateBtn ="";
	var sendAddressLists = [];
	var deleteExpressTickets = [];
	var sendAddressDirection = "0";// 查询送票地址来源 0-首次进入送票上门 1-其他情况
	jq("#sendTicketVisitView").live("pageinit", function(e,data){
		addSendTicketAdressCount = 1;
		unSendTicketLists = [];
		addUnSendTicketLists = [];
		selectSendTicketLists = [];
		deleteExpressTickets = [];
		thisSaveBtn = "";
		thisUpdateBtn = "";
		thisDeleteBtn = "";
		thisSaveUpateBtn = "";
	});
	
	jq("#sendTicketVisitView").live("pagebeforeshow", function(e,data){
		if(data.prevPage.attr("id") !== "sendTicketAddressView"){
			sendTicketLists = [];
			sendOrderList = [];
			expressTickets = [];
			mor.ticket.sendTicketAddress.thisAddress = '';
			mor.ticket.sendTicketAddress.sendTicketList = [];
			mor.ticket.sendTicketAddress.sendOrderList = [];
		}
		initSendTicketAddress();
		sendTicketAdressQuery();// 更换送票地址信息
		addExpressTickets();// 保存送票信息
		deleteOneSendTicket(); // 勾选或取消选择车票
		sendTicketVisitBackPage();
		deleteExpressTicket();// 删除送票信息
		saveUpdateExpressTickets(); // 保存修改送票信息
		updateSendTicket(); // 点击修改送票信息
		cancelUpdateSendTicket();// 取消修改送票信息
	});

	function initSendTicketAddress(){
		if(sendTicketLists.length == 0){
			initSendTicketList();
		} else {
			showOrderInfoList();
		}
	}

	function setAddressHeight(){
		jq.grep(jq("li"),function(address,data_index){
			var obj_a_div = jq("#address_" + data_index).parent();
			var obj_address = obj_a_div.parent();
			obj_address.find("#address").css("margin-top",(obj_a_div.height()-20)/2);
		});
	}
	function initSendTicketList(){
		if(mor.ticket.viewControl.current_tab == "orderDetailsTab" 
			|| mor.ticket.viewControl.current_tab1 == "orderDetailsTab"){
			mor.ticket.viewControl.current_tab1 = "orderDetailsTab";
			mor.ticket.viewControl.current_tab2 = "orderDetailsTab"; 
			var ticket = mor.ticket.currentTicket;		
			var orderManager = mor.ticket.orderManager;
			var ticketList = orderManager.getTicketList();
			var mode = mor.ticket.viewControl.bookMode;
			var isFC = true,isDC = true;
			if(mode == "fc"){
				// 若是返程票，则判断往程票是否可送
				var preTicket = orderManager.getPreTicketDetail();
				var goTime = preTicket.train_date;
				isFC = util.getSendTicketTimeLimit(goTime);
			}
			var backTime = ticket.train_date;
			isDC = util.getSendTicketTimeLimit(backTime);
			
			for(var i=0;i<ticketList.length;i++){
				if(ticketList[i].ticket_type_code !== "3"){
					// 判断往程票
					if(ticketList[i].isRoundTripFlag != true && isFC){
						sendTicketLists.push(ticketList[i]);
					}else if(ticketList[i].isRoundTripFlag == true && isDC){ // 判断返程票
						sendTicketLists.push(ticketList[i]);
					}
				}	
			}
			
			for(var i=0;i<sendTicketLists.length;i++){
				if(sendTicketLists[i].isRoundTripFlag != true && mode == "fc"){
					sendTicketLists[i].train_date = ticket.pre_train_date;
					sendTicketLists[i].station_train_code = preTicket.station_train_code;
					sendTicketLists[i].from_station_telecode = preTicket.from_station_telecode;
					sendTicketLists[i].to_station_telecode = preTicket.to_station_telecode;
					sendTicketLists[i].start_time = preTicket.start_time;
					sendTicketLists[i].arrive_time = preTicket.arrive_time;
				}else{
					sendTicketLists[i].train_date = ticket.train_date;
					sendTicketLists[i].station_train_code = ticket.station_train_code;
					sendTicketLists[i].from_station_telecode = ticket.from_station_telecode;
					sendTicketLists[i].to_station_telecode = ticket.to_station_telecode;
					sendTicketLists[i].start_time = ticket.start_time;
					sendTicketLists[i].arrive_time = ticket.arrive_time;
				}
			}
		}
		if(mor.ticket.viewControl.current_tab == "continuePaymentTab" || mor.ticket.viewControl.current_tab1 == "continuePaymentTab"){
			mor.ticket.viewControl.current_tab1 = "continuePaymentTab";
			mor.ticket.viewControl.current_tab2 = "continuePaymentTab"; 
			var queryOrder = mor.ticket.queryOrder;
			var unFinishedOrder = queryOrder.getCurrentUnfinishedOrder();
			var ticketList = unFinishedOrder.myTicketList;
			// 判断往返程
			if(isRoundTripOrder(ticketList)){
				var RoundTripOrder = getRoundTripTicketList(ticketList);
				var goTicketList = RoundTripOrder.goList;
				var backTicketList = RoundTripOrder.backList;
				var goTime = goTicketList[0].train_date;
				var backTime = backTicketList[0].train_date;
				if(util.getSendTicketTimeLimit(goTime)){
					for(var i=0;i<ticketList.length;i++){
						if(ticketList[i].ticket_type_code !== "3"){
							sendTicketLists.push(ticketList[i]);
						}
					}
				}else if(util.getSendTicketTimeLimit(backTime)){
					for(var i=0;i<backTicketList.length;i++){
						if(ticketList[i].ticket_type_code !== "3"){
							sendTicketLists.push(backTicketList[i]);
						}
					}
				}
			}else{
				for(var i=0;i<ticketList.length;i++){
					if(ticketList[i].ticket_type_code !== "3"){
						sendTicketLists.push(ticketList[i]);
					}
				}
			}
		}
		// 查询已保存物流数据
		if(sendTicketLists.length > 0){
			queryExpressTickets();
		}else{
			mor.ticket.util.alertMessage("没有查询到相关车票信息");
			jq("#sendTicketVisitBackBtn").tap();
			return false;
		}
	}
	
	function isRoundTripOrder(order){
		if(order.length > 1){
			var ticket = order[0];
			for(var i=1; i< order.length ; i++){
				if(ticket.from_station_telecode != order[i].from_station_telecode){
					return true;
				}
			}
			return false;
		}else {
			return false;
		}	
	}
	
	function getRoundTripTicketList(order){
		var list1 = [];
		var list2 = [];
		var ticket = order[0];	
		list1.push(ticket);
		for(var i=1; i< order.length ; i++){
			if(ticket.from_station_telecode == order[i].from_station_telecode){
				list1.push(order[i]);
			} else {
				list2.push(order[i]);
			}
		}
		var list1Date = list1[0].train_date+list1[0].start_time;
		var list2Date = list2[0].train_date+list2[0].start_time;
		if(list1Date < list2Date){
			return {
				goList:list1,
				backList:list2
			};
		}else{
			return {
				goList:list2,
				backList:list1
			};
		}
	}
	
	function showOrderInfoList(){
		jq("#sendTicket").html(generateOrderInfoListContent(sendOrderList));
		addSendTicketAdressCount = sendOrderList.length;
		jq("#sendTicket").listview("refresh");
		jq("#sendTicket").show();
		setTimeout(showSendTicketAddress, 100);
	}
	
	function showSendTicketAddress(){
		var sendAddress = mor.ticket.sendTicketAddress;
		if(sendAddress.selected == "true" || sendAddress.selected == true){
			var address = jq("#address_" + sendAddress.thisAddress);
			address.find("#addressee").html(sendAddress.addressee_name);
			address.find("#addresseeTelephone").html(sendAddress.mobile_no);
			address.find("#addresseeAddress").html(sendAddress.addressee_province+""+sendAddress.addressee_city+""+sendAddress.addressee_county);
			address.find("#addresseeDetailsAddress").html(sendAddress.detail_address);
		}else{
			jq("#address_"+sendAddress.thisAddress).find("#addImg").show();
		}
		setAddressHeight();
	}
	
	function sendTicketVisitBackPage(){
		jq("#sendTicketVisitBackBtn,#sendTicketVisitFinishBtn").bind("tap",function(e){
			e.stopImmediatePropagation();
			e.preventDefault();
			if(mor.ticket.viewControl.current_tab2 == "continuePaymentTab"){
				mor.ticket.util.changePage(vPathCallBack() + "continuePayment.html");
				return false;
			}
			if(mor.ticket.viewControl.current_tab2 == "orderDetailsTab"){
				mor.ticket.util.changePage(vPathCallBack() + "orderDetails.html");
				return false;
			}
			return false;
		});
	}
	
	function OrderInfo(track_no,order_no,ticketList,address_name,address_tel,
			address_addr,address_detail,address_date,send_time,status,deliver_company){
		this.track_no = track_no;
		this.order_no = order_no;
		this.ticketList = ticketList;
		this.address_name = address_name;
		this.address_tel = address_tel;
		this.address_addr = address_addr;
		this.address_detail = address_detail;
		this.address_date = address_date;
		this.deliver_company = deliver_company;
		this.send_time = send_time;
		this.status = status;
	}
	function sendTicketAdressQuery(){
		jq("a[id^='address']").live("tap",function(e){
			e.stopImmediatePropagation();
			var that = jq(this);
			mor.ticket.sendTicketAddress.thisAddress = that.parent().parent().parent().attr("id");
			mor.ticket.viewControl.current_tab = "sendTicketVisitTab";
			// 保存配送信息
			sendOrderList = [];
			for(var i = 1;i <= addSendTicketAdressCount;i++){
				var orderInfo = new OrderInfo();
				var ticketList = [];
				orderInfo.order_no = i;
				var liDiv = jq("#" + i);
				var expressId = liDiv.children("div:first").attr("id");
				if(expressId == undefined){
					expressId = "";
				}
				var editStatus = liDiv.find("#saveUpdateSendTicket").css("display");
				if(expressId != '' &&  editStatus != "none"){
					orderInfo.status = 'edit';
				}else if(expressId != '' && editStatus == "none"){
					orderInfo.status = 'unEdit';
				}else{
					orderInfo.status = '';
				}
				var tr = liDiv.find("tr");
				for(var t = tr.length - 1;t>=0;t--){
					var trVal = tr.eq(t).attr("id");
					for(var j = 0;j < sendTicketLists.length;j++){
						if(trVal == sendTicketLists[j].index_flag){
							ticketList.push(sendTicketLists[j]);
						}
					}
				}
				orderInfo.track_no = expressId;
				orderInfo.ticketList = ticketList;
				orderInfo.address_name = liDiv.find("#addressee").text();
				orderInfo.address_tel = liDiv.find("#addresseeTelephone").text();
				orderInfo.address_addr = liDiv.find("#addresseeAddress").text();
				orderInfo.address_detail = liDiv.find("#addresseeDetailsAddress").text();
				orderInfo.deliver_company = liDiv.find("#logisticsInfo").val();
				orderInfo.address_date = liDiv.find("#sendTicketDate").val();
				orderInfo.send_time = liDiv.find("#sendTicketTime").val();
				sendOrderList.push(orderInfo);
			}
			mor.ticket.sendTicketAddress.sendTicketList = sendTicketLists;
			mor.ticket.sendTicketAddress.sendOrderList = sendOrderList;
			mor.ticket.util.changePage("sendTicketAddress.html");
			return false;
		});
	}
	
	function deleteOneSendTicket(){
		jq("#sendTicket").on("tap","tr",function(e){
			e.stopImmediatePropagation();
			if(jq(this).find("input[type='checkbox']").is(":disabled")){
				return;
			}
			var valu = jq(this).find("input[type='checkbox']").is(":checked");
			var thisUnSendTicketindex = jq(this).parent().parent().parent().attr("id");
			var new_delete_one_index_flag = jq(this).attr("id");
			if(valu){// 取消车票
				jq(this).find("input[type='checkbox']").attr("checked",false);
				var unCheckedTicketList = [];
				for(var i=0;i<sendTicketLists.length;i++){
					if(sendTicketLists[i].index_flag == new_delete_one_index_flag){
						sendTicketLists[i].send_flag = "0";
						unCheckedTicketList.push(sendTicketLists[i]);
						unSendTicketLists.push(sendTicketLists[i]);
						addUnSendTicketLists.push(sendTicketLists[i]);
						// 同时取消关联儿童票或成人票
						for(var j=0;j<sendTicketLists.length;j++){
							if(sendTicketLists[i].passenger_id_no == sendTicketLists[j].passenger_id_no &&
									sendTicketLists[i].batch_no == sendTicketLists[j].batch_no &&
									sendTicketLists[i].index_flag != sendTicketLists[j].index_flag){
								sendTicketLists[j].send_flag = "0";
								unCheckedTicketList.push(sendTicketLists[j]);
								unSendTicketLists.push(sendTicketLists[j]);
								addUnSendTicketLists.push(sendTicketLists[j]);
								jq("#"+sendTicketLists[j].index_flag).find("input[type='checkbox']").attr("checked",false);
							}
						}
						break;
					}
				}
				// 将取消车票追加都其他订单列表
				for(var j=1;j<=addSendTicketAdressCount;j++){
					if(jq("#"+j).find("#saveUpdateSendTicket").css("display") != "none" || jq("#"+j).find("#saveSendTicket").css("display") != "none"){
						var unSendTicketListTrHtml = "";
						for(var w=0;w<unCheckedTicketList.length;w++){
								unSendTicketListTrHtml = "<tr id='"+unCheckedTicketList[w].index_flag+"'>"
						  		+ "<td style='width:10%'>"+unCheckedTicketList[w].station_train_code+"</td>"
								+ "<td style='width:20%'>"+unCheckedTicketList[w].passenger_name+"</td>"
								+ "<td style='width:20%'>"+mor.ticket.util.getTicketTypeName(unCheckedTicketList[w].ticket_type_code)+"</td>"
								+ "<td style='width:40%'>"+mor.ticket.util.getSeatTypeName(unCheckedTicketList[w].seat_type_code)+''+unCheckedTicketList[w].coach_name+'车'+unCheckedTicketList[w].seat_name+"</td>"
								+ "<td style='width:5%' style='pointer:none;'>"
								+ "<input type='checkbox' name='checkbox' id='checkbox_"+unCheckedTicketList[w].index_flag+"' style='pointer:none;'>"
								+ "</td>"
								+ "</tr>";
								if(thisUnSendTicketindex != j){
									jq("#"+j).find("tbody").append(unSendTicketListTrHtml);
								}
						}
					}
				}
			}else{ // 勾选车票
				var checkedTicketList = [];
				for(var i=0;i<sendTicketLists.length;i++){
					if(sendTicketLists[i].index_flag == new_delete_one_index_flag){
						checkedTicketList.push(sendTicketLists[i]);
						// 关联儿童票或成人票
						for(var j=0;j<sendTicketLists.length;j++){
							if(sendTicketLists[i].passenger_id_no == sendTicketLists[j].passenger_id_no &&
									sendTicketLists[i].batch_no == sendTicketLists[j].batch_no &&
									sendTicketLists[i].index_flag != sendTicketLists[j].index_flag){
								checkedTicketList.push(sendTicketLists[j]);
							}
						}
						break;
					}
				}
				var oldTrLenth = jq(this).parent().find("input[type='checkbox']:checked").length;
				var newTrLenth = oldTrLenth + checkedTicketList.length;
				if(newTrLenth > 5) {
					mor.ticket.util.alertMessage("一个送票地址最多只可选择5张车票");
					return false;
				} else { 
					jq(this).find("input[type='checkbox']").attr("checked",true);
					var liIndex = jq(this).parent().parent().parent().attr("id");
					for(var i=0;i<checkedTicketList.length;i++){
						jq("#"+liIndex).find("tr[id='"+checkedTicketList[i].index_flag+"']").find("input[type='checkbox']").attr("checked",true);
						for(var j=0;j<sendTicketLists.length;j++){
							if(checkedTicketList[i].index_flag == sendTicketLists[j].index_flag){
								sendTicketLists[j].send_flag = "1";
							}
						}
						for(var k=0;k<unSendTicketLists.length;k++){
							if(checkedTicketList[i].index_flag == unSendTicketLists[k].index_flag){
								unSendTicketLists.splice(k,1);
								break;
							}
						}
						for(var w=0;w<addUnSendTicketLists.length;w++){
							if(checkedTicketList[i].index_flag == addUnSendTicketLists[w].index_flag){
								addUnSendTicketLists.splice(w,1);
								break;
							}
						}
					}
				}
				// 从其他订单中移除当前已勾选的车票
				var nCount = 0;
				for(var j=1;j<=addSendTicketAdressCount;j++){
					if(thisUnSendTicketindex != j){
						var trLeng = jq("#"+j).find("tr").length;
						for(var t=trLeng-1;t>=0;t--){
							var trValue = jq("#"+j).find("tr").eq(t).attr("id");
							for(var i=0;i<checkedTicketList.length;i++){
								if(trValue == checkedTicketList[i].index_flag){
									jq("#"+j).find("tr").eq(t).remove();
								}
							}
						}
						var trLen = jq("#"+j).find("tr").length;
						if(trLen == 0){
							jq("#" + j).remove();
							nCount++;
						}
					}
				}
				addSendTicketAdressCount = addSendTicketAdressCount - nCount;
			}
			// 可添加送票的车票列表
			cancelUpdataSendTicket = [];
			for(var i=0;i<unSendTicketLists.length;i++){
				cancelUpdataSendTicket.push(unSendTicketLists[i]);
			}
			jq("#sendTicket").listview("refresh");
			return false;
		});
	}
	
	function addExpressTickets(){
		jq("#sendTicket").on("tap","li #saveSendTicket",function(e){
			e.stopImmediatePropagation();
			var tickets_all_message = "";
			var train_time_info = "";
			var send_ticket_from_station_telecode = "";
			thisSaveBtn = jq(this);
			var liDiv = thisSaveBtn.parent().parent();
			thisUpdateBtn = liDiv.find("#updateSendTicket");
			thisDeleteBtn = liDiv.find("#deleteSendTicket");
			var sendTicketDate = mor.ticket.util.getNewDate().format("yyyyMMdd");
			var sendTicketTime = "08:00~12:00";
			var save_send_ticket_lenght = liDiv.find("tr").length;
			if(save_send_ticket_lenght == 0){
				mor.ticket.util.alertMessage("请选择需要送票的车票信息");
				return;
			}
			var saveSendTicketList = [];
			for(var i=0;i<save_send_ticket_lenght;i++){
				var save_send_ticket_from_station_telecode_index_flag = liDiv.find("tr").eq(i).attr("id");
				for(var j=0;j<sendTicketLists.length;j++){
					var new_save_index_flag = sendTicketLists[j].index_flag;
					if(save_send_ticket_from_station_telecode_index_flag == new_save_index_flag && sendTicketLists[j].send_flag == "1"){
						saveSendTicketList.push(sendTicketLists[j]);
					}
				}
			}
			
			if(saveSendTicketList.length == 0){
				mor.ticket.util.alertMessage("请选择需要送票的车票信息");
				return;
			}
			send_ticket_from_station_telecode = saveSendTicketList[0].from_station_telecode;
			var isChild = "";
			for(var i=0;i<saveSendTicketList.length;i++){
					if(saveSendTicketList[i].ticket_type_code != "2"){
						isChild = "true";
						break;
					}
					if(saveSendTicketList[i].ticket_type_code == "2"){
						isChild = "false";
					}
			}
			if(isChild == "false"){
				mor.ticket.util.alertMessage("儿童票不能单独送票");
				return;
			}
			for(var i=0;i<saveSendTicketList.length;i++){
				tickets_all_message += saveSendTicketList[i].sequence_no + "~" +saveSendTicketList[i].batch_no + "~" +
				saveSendTicketList[i].coach_no + "~" + saveSendTicketList[i].seat_no + "~" + saveSendTicketList[i].passenger_id_type_code + "~" +
				saveSendTicketList[i].passenger_id_no + "~" + saveSendTicketList[i].passenger_name + "~" + saveSendTicketList[i].ticket_type_code + "#";
				train_time_info += saveSendTicketList[i].train_date.split("-").join("")+""+saveSendTicketList[i].start_time.split(":").join("")+"#";
			}
			
			
			// 送票地址信息
			var nIndex = -1;
			var sendTicketAddressLists = mor.ticket.sendTicketAddressLists;
			for(var i = 0;i<sendTicketAddressLists.length;i++){
				if((liDiv.find("#addressee").html() == sendTicketAddressLists[i].addressee_name)
						&& (liDiv.find("#addresseeTelephone").html() == sendTicketAddressLists[i].mobile_no)
						&& (liDiv.find("#addresseeAddress").html() == (sendTicketAddressLists[i].addressee_province+""+sendTicketAddressLists[i].addressee_city+""+sendTicketAddressLists[i].addressee_county))
						&& (liDiv.find("#addresseeDetailsAddress").html() == sendTicketAddressLists[i].detail_address))
				{
					nIndex = i;
					break;
				}
			}
			
			if(nIndex == -1){
				mor.ticket.util.alertMessage("请添加收货人地址信息！");
				return false;
			}
			
			var commonParameters = {
					"region_code" : sendTicketAddressLists[nIndex].region_code,
					"deliver_company" : sendTicketAddressLists[nIndex].deliver_company,
					"addressee_name" : sendTicketAddressLists[nIndex].addressee_name,
					"addressee_province" : sendTicketAddressLists[nIndex].addressee_province,
					"addressee_city" : sendTicketAddressLists[nIndex].addressee_city,
					"addressee_county" : sendTicketAddressLists[nIndex].addressee_county,
					"addressee_town" : sendTicketAddressLists[nIndex].addressee_town,
					"addressee_street" : sendTicketAddressLists[nIndex].addressee_street,
					"detail_address" : sendTicketAddressLists[nIndex].detail_address,
					"mobile_no" : sendTicketAddressLists[nIndex].mobile_no,
					"phone_no" : sendTicketAddressLists[nIndex].phone_no,
					"service_type" : sendTicketAddressLists[nIndex].service_type,
					"pay_type" : '',
					"deliver_mode" : '',
					"receive_date_flag" : '',
					"invoice_flag" : '',
					"from_tele_code" : send_ticket_from_station_telecode,
					"tickets_message" : tickets_all_message,
					"deliverDate" : sendTicketDate + " " + sendTicketTime,
					"train_time_info" :	train_time_info
			};
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "addExpressTickets"
				};
			
			var options =  {
					onSuccess: requestAddExpressTicketsSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
			return false;
		});
	}
	
	function requestAddExpressTicketsSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			thisSaveBtn.css("display","none");
			thisUpdateBtn.css("display","");
			thisDeleteBtn.css("display","");
			var liDiv = thisUpdateBtn.parent().parent();
			liDiv.find(".sendTicketBtn").attr("id",invocationResult.tracking_no+sendTicketLists[0].sequence_no);
			liDiv.find("#status").html("");
			var thisCheckboxLength =  liDiv.find("input[type='checkbox']").length;
			for(var i=0;i<thisCheckboxLength;i++){
				if(liDiv.find("input[type='checkbox']").eq(i).is(":checked") == false){
					liDiv.find("input[type='checkbox']").eq(i).parent().parent().remove();
					thisCheckboxLength = thisCheckboxLength - 1;
					i = i - 1;
				}else{
					liDiv.find("input[type='checkbox']").eq(i).attr("disabled",true);
				}
			}
			liDiv.find("a[id^='address']").addClass("ui-disabled");
			liDiv.find("#sendTicketDate").addClass("ui-disabled");
			liDiv.find("#sendTicketTime").addClass("ui-disabled");
			var agent_fee = invocationResult.agent_fee !== "0" ? invocationResult.agent_fee : 1700;
			liDiv.find("#deliver_company").find("#logisticsInfo").val(invocationResult.deliver_company + "  ¥" + parseFloat(agent_fee/100).toFixed(2) + " 元");
			liDiv.find("#deliver_company").show();
			queryExistedExpressList();
			setTimeout(function(){
				addNewExpress(0);
			}, 1000);
			jq("#sendTicket").listview("refresh");
		}else {	
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	// 查询已保存快递订单信息
	function queryExpressTickets(){
		var commonParameters = {
					"sequence_no" : sendTicketLists[0].sequence_no,
					"query_flag" : '0',
					"pay_flag" : '0'
			};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryExpressTickets"
			};
			
		var options =  {
				onSuccess: requestQueryExpressTicketsSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestQueryExpressTicketsSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			expressTickets = invocationResult.listExpressOrderForQueryReturnBean;
			if(expressTickets.length>0){
				for(var i=0;i<sendTicketLists.length;i++){
					sendTicketLists[i].index_flag = "index_"+i;
					sendTicketLists[i].send_flag = "0";
				}
				var ticketList = []; 
				addSendTicketAdressCount = expressTickets.length;
				// 遍历快递订单列表
				for(var j=0;j<expressTickets.length;j++){
					var trackInfo = expressTickets[j];
					selectSendTicketLists = [];
					ticketList = trackInfo.ticket_message.split("#");
					expressTickets[j].total_price = expressTickets[j].total_price !== "0" ? expressTickets[j].total_price : "1700";
					for(var w=0;w<sendTicketLists.length;w++){
						var temp_message = 
							sendTicketLists[w].sequence_no + "+" + 
							sendTicketLists[w].batch_no + "+" +
							sendTicketLists[w].coach_no + "+" +
							sendTicketLists[w].seat_no + "+" +
							sendTicketLists[w].passenger_id_type_code + "+" +
							sendTicketLists[w].passenger_id_no + "+" +
							sendTicketLists[w].passenger_name;
					
						if(jq.inArray(temp_message,ticketList) != -1){
							sendTicketLists[w].send_flag = "1";
							selectSendTicketLists.push(sendTicketLists[w]);
						}
					}	
					expressTickets[j].sendTicketList = selectSendTicketLists;
				}
				expressTickets.reverse();
				jq("#sendTicket").html(generateQuerySendTicketListContent(expressTickets));
				jq("#sendTicket").listview("refresh");
				jq("#sendTicket").show();
				
				addUnSendTicketLists = [];
				unSendTicketLists = [];
				for(var i=0;i<sendTicketLists.length;i++){
					if(sendTicketLists[i].send_flag != "1"){
						addUnSendTicketLists.push(sendTicketLists[i]);
					}
				}
				setAddressHeight();
				queryDefaultSendTicketAddress("1");
				if(addUnSendTicketLists.length == 0){
					return false;
				}
				var addUnSendTicketListsLength = addUnSendTicketLists.length;
				if(addUnSendTicketListsLength<6){
					for(var j=0;j<addUnSendTicketLists.length;j++){
						for(var i=0;i<sendTicketLists.length;i++){
							if(addUnSendTicketLists[j].index_flag == sendTicketLists[i].index_flag){
								sendTicketLists[i].send_flag = "1";
							}
						}
						addUnSendTicketLists[j].send_flag = "1";
					}
				}
				if(addUnSendTicketListsLength>5){
					for(var j=0;j<addUnSendTicketLists.length;j++){
						unSendTicketLists.push(addUnSendTicketLists[j]);
					}
				}
				addNewSendTicket();
			}
		} else {
			initDefaultSendTicketInfo();
		}
	}
	// 查询已经存在的物流单
	function queryExistedExpressList(){
		var commonParameters = {
					"sequence_no" : sendTicketLists[0].sequence_no,
					"query_flag" : '0',
					"pay_flag" : '0'
			};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryExpressTickets"
			};
			
		var options =  {
				onSuccess: queryExistedExpressSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function queryExistedExpressSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			expressTickets = invocationResult.listExpressOrderForQueryReturnBean;
			for(var i=0;i<expressTickets.length;i++){
				var trackInfo = expressTickets[i];
				selectSendTicketLists = [];
				var ticketList = trackInfo.ticket_message.split("#");
				for(var w=0;w<sendTicketLists.length;w++){
					var temp_message = 
							sendTicketLists[w].sequence_no + "+" + 
							sendTicketLists[w].batch_no + "+" +
							sendTicketLists[w].coach_no + "+" +
							sendTicketLists[w].seat_no + "+" +
							sendTicketLists[w].passenger_id_type_code + "+" +
							sendTicketLists[w].passenger_id_no + "+" + 
							sendTicketLists[w].passenger_name;
					
					if(jq.inArray(temp_message,ticketList) != -1){
						sendTicketLists[w].send_flag = "1";
						selectSendTicketLists.push(sendTicketLists[w]);
					}
				}	
				expressTickets[i].sendTicketList = selectSendTicketLists;
			}
			for(var i=0;i<sendTicketLists.length;i++){
				if(sendTicketLists[i].send_flag != "1"){
					if(addUnSendTicketLists.length == 0){
						unSendTicketLists.push(sendTicketLists[i]);
						addUnSendTicketLists.push(sendTicketLists[i]);
					}else{
						var isExisted = false;
						for(var j = 0;j<addUnSendTicketLists.length;j++){
							if(sendTicketLists[i].index_flag == addUnSendTicketLists[j].index_flag){
								isExisted = true;
								break;
							}
						}
						if(!isExisted){
							unSendTicketLists.push(sendTicketLists[i]);
							addUnSendTicketLists.push(sendTicketLists[i]);
						}
					}
				}
			}
		}
	}
	
	function initDefaultSendTicketInfo(){
		if(sendTicketLists.length<=5){
			for(var i=0;i<sendTicketLists.length;i++){
				sendTicketLists[i].index_flag = "index_"+i;
				sendTicketLists[i].send_flag = "1";
			}
		}else{
			for(var i=0;i<sendTicketLists.length;i++){
				sendTicketLists[i].index_flag = "index_"+i;
				sendTicketLists[i].send_flag = "0";
			}
		}
		jq("#sendTicket").append("<li id='1'></li>");
		jq("#1").html(generateSendTicketListContent(sendTicketLists));
		jq("#"+addSendTicketAdressCount).find("#Num").html('1');
		jq("#sendTicket").listview("refresh");
		jq("#sendTicket").show();
		setTimeout(function(){
			queryDefaultSendTicketAddress("0");
		},100);
	}
	function queryDefaultSendTicketAddress(sendAddressFrom){
		//查询默认送票地址
		var commonParameters = "";

		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "queryDeliverAddress"
		};
		sendAddressDirection = sendAddressFrom;
		var options = {
			onSuccess : queryDeliverSucceeded,
			onFailure : util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,options);
	}
	
	function queryDeliverSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.sendTicketAddressLists = invocationResult.deliverAddressList;
			var sendTicketAddressLists = mor.ticket.sendTicketAddressLists;
			if(sendAddressDirection == "0"){ // 首次进入送票上门页面
				if(sendTicketAddressLists.length>0){
					// 首次进入送票上门，没有物流配送单
					if(mor.ticket.sendTicketAddress.thisAddress == ''){
						var defaultAddress = false;
						for(var i=0;i<sendTicketAddressLists.length;i++){
							// 判断默认送票地址
							if(sendTicketAddressLists[i].default_address === '0'){
								jq("#addressee").html(sendTicketAddressLists[i].addressee_name);
								jq("#addresseeTelephone").html(sendTicketAddressLists[i].mobile_no);
								jq("#addresseeAddress").html(sendTicketAddressLists[i].addressee_province+""+sendTicketAddressLists[i].addressee_city+""+sendTicketAddressLists[i].addressee_county);
								jq("#addresseeDetailsAddress").html(sendTicketAddressLists[i].detail_address);
								defaultAddress = true;
								break;
							}
						}
						// 没有默认送票地址
						if(!defaultAddress){
							jq("#addImg").show();
						}else{
							setAddressHeight();
						}
					}
				}else{//没有添加送票地址
					jq("#addImg").show();
				}
			}
		} else {
			jq("#addImg").show();
		}
	}
	//保存修改物流单
	function saveUpdateExpressTickets(){
		jq("#sendTicket").on("tap","li #saveUpdateSendTicket",function(e){
			e.stopImmediatePropagation();
			var tickets_all_message = "";
			var train_time_info = "";
			
			thisSaveUpateBtn = jq(this);
			var liDiv = jq(this).parent().parent();
			thisSaveBtn = liDiv.find("#saveSendTicket");
			thisUpdateBtn = liDiv.find("#updateSendTicket");
			thisCancelUpdateBtn = liDiv.find("#cancelUpdateSendTicket");
			thisDeleteBtn = liDiv.find("#deleteSendTicket");
			var save_send_ticket_lenght = liDiv.find("tr").length;
			
			if(save_send_ticket_lenght == 0){
				mor.ticket.util.alertMessage("请选择需要送票的车票信息");
				return false;
			}
			var saveUpdateSendTicketList = [];
			for(var i=0;i<save_send_ticket_lenght;i++){
				var save_send_ticket_from_station_telecode_index_flag = liDiv.find("tr").eq(i).attr("id");
				for(var j=0;j<sendTicketLists.length;j++){
					var new_save_index_flag = sendTicketLists[j].index_flag;
					if(save_send_ticket_from_station_telecode_index_flag == new_save_index_flag && sendTicketLists[j].send_flag == "1"){
						saveUpdateSendTicketList.push(sendTicketLists[j]);
					}
				}
			}
			
			if(saveUpdateSendTicketList.length == 0){
				mor.ticket.util.alertMessage("请选择需要送票的车票信息");
				return;
			}
			var isChild = "";
			for(var i=0;i<saveUpdateSendTicketList.length;i++){
					if(saveUpdateSendTicketList[i].ticket_type_code != "2"){
						isChild = "true";
						break;
					}
					if(saveUpdateSendTicketList[i].ticket_type_code == "2"){
						isChild = "false";
					}
			}
			if(isChild == "false"){
				mor.ticket.util.alertMessage("儿童票不能单独送票");
				return;
			}
			
			for(var i=0;i<saveUpdateSendTicketList.length;i++){
					tickets_all_message += saveUpdateSendTicketList[i].sequence_no + "~" +saveUpdateSendTicketList[i].batch_no + "~" +
					saveUpdateSendTicketList[i].coach_no + "~" + saveUpdateSendTicketList[i].seat_no + "~" + saveUpdateSendTicketList[i].passenger_id_type_code +
					"~" + saveUpdateSendTicketList[i].passenger_id_no + "~" + saveUpdateSendTicketList[i].passenger_name+ "~" + saveUpdateSendTicketList[i].ticket_type_code+"#";
					train_time_info += saveUpdateSendTicketList[i].train_date.split("-").join("")+""+saveUpdateSendTicketList[i].start_time.split(":").join("")+"#";
			}
			saveUpdateSendTicketList = [];
			
			var trackingNoAndSequenceNo = liDiv.find(".sendTicketBtn").attr("id");
			var saveUpdateExpressTickets = [];
			for(var i=0;i<expressTickets.length;i++){
				var expressTrackingNoAndSequenceNo = expressTickets[i].tracking_no + expressTickets[i].ticket_message.split("+")[0];
				if(trackingNoAndSequenceNo == expressTrackingNoAndSequenceNo){
					saveUpdateExpressTickets.push(expressTickets[i]);
				}
			}
			
			// 送票地址信息
			var nIndex = 0;
			var sendTicketAddressLists = mor.ticket.sendTicketAddressLists;
			for(var i = 0;i<sendTicketAddressLists.length;i++){
				if((liDiv.find("#addressee").html() == sendTicketAddressLists[i].addressee_name)
						&& (liDiv.find("#addresseeTelephone").html() == sendTicketAddressLists[i].mobile_no)
						&& (liDiv.find("#addresseeAddress").html() == (sendTicketAddressLists[i].addressee_province+""+sendTicketAddressLists[i].addressee_city+""+sendTicketAddressLists[i].addressee_county))
						&& (liDiv.find("#addresseeDetailsAddress").html() == sendTicketAddressLists[i].detail_address))
				{
					nIndex = i;
					break;
				}
			}
			
			var commonParameters = {
						"tracking_no" : saveUpdateExpressTickets[0].tracking_no,
						"train_time_info" : train_time_info,
						"ticket_message" : tickets_all_message,
						"from_tele_code" : saveUpdateExpressTickets[0].sendTicketList[0].from_station_telecode,
						"region_code" : saveUpdateExpressTickets[0].region_code,
						"deliver_company" : saveUpdateExpressTickets[0].deliver_company,
						"addressee_name" : sendTicketAddressLists[nIndex].addressee_name,
						"addressee_province" : sendTicketAddressLists[nIndex].addressee_province,
						"addressee_city" : sendTicketAddressLists[nIndex].addressee_city,
						"addressee_county" : sendTicketAddressLists[nIndex].addressee_county,
						"addressee_town" : sendTicketAddressLists[nIndex].addressee_town,
						"addressee_street" : sendTicketAddressLists[nIndex].addressee_street,
						"detail_address" : sendTicketAddressLists[nIndex].detail_address,
						"mobile_no" : saveUpdateExpressTickets[0].mobile_no,
						"phone_no" : saveUpdateExpressTickets[0].phone_no,
						"service_type" : saveUpdateExpressTickets[0].service_type,
						"pay_type" : saveUpdateExpressTickets[0].pay_type,
						"deliver_mode" : saveUpdateExpressTickets[0].deliver_mode,
						"start_date" : saveUpdateExpressTickets[0].start_date,
						"stop_date" : saveUpdateExpressTickets[0].stop_date,
						"receive_date_flag" : saveUpdateExpressTickets[0].receive_date_flag,
						"start_time" : saveUpdateExpressTickets[0].start_time,
						"stop_time": saveUpdateExpressTickets[0].stop_time,
						"invoice_flag" : saveUpdateExpressTickets[0].invoice_flag,
						"invoice_title" : saveUpdateExpressTickets[0].invoice_title,
						"invoice_name" : saveUpdateExpressTickets[0].invoice_name,
						"flag" : saveUpdateExpressTickets[0].flag,
						"stop_hour" : saveUpdateExpressTickets[0].stop_hour,
						"agent_fee" : saveUpdateExpressTickets[0].agent_fee,
						"addition_cost" : saveUpdateExpressTickets[0].addition_cost,
						"alien_fee" : saveUpdateExpressTickets[0].alien_fee,
						"deliver_comment" : saveUpdateExpressTickets[0].deliver_comment,
						"total_price" : saveUpdateExpressTickets[0].total_price,
						"sequeueNo" : saveUpdateExpressTickets[0].sendTicketList[0].sequence_no,
						"isAfterPay" : "N",
						"isDelete" : "false"
				};
				var invocationData = {
						adapter: mor.ticket.viewControl.adapterUsed,
						procedure: "updateAndDeleteExpressTickets"
					};
				
				var options =  {
						onSuccess: requestSaveUpdateExpressTicketsSucceeded,
						onFailure: util.creatCommonRequestFailureHandler()
				};
				mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
				return false;
		});
	}
	
	function requestSaveUpdateExpressTicketsSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.util.alertMessage("修改送票信息成功");
			thisSaveBtn.css("display","none");
			thisSaveUpateBtn.css("display","none");
			thisCancelUpdateBtn.css("display","none");
			thisUpdateBtn.css("display","");
			thisDeleteBtn.css("display","");
			var liDiv = thisSaveUpateBtn.parent().parent();
			liDiv.find("a[id^='address']").addClass("ui-disabled");
			var thisCheckboxLength =  liDiv.find("input[type='checkbox']").length;
			for(var i=0;i<thisCheckboxLength;i++){
				if(liDiv.find("input[type='checkbox']").eq(i).is(":checked") == false){
					liDiv.find("input[type='checkbox']").eq(i).parent().parent().remove();
					thisCheckboxLength = thisCheckboxLength - 1;
					i = i - 1;
				}else{
					liDiv.find("input[type='checkbox']").eq(i).attr("disabled",true);
				}
			}
			var agent_fee = invocationResult.total_price !== "0" ? invocationResult.total_price : "1700";
			liDiv.find("#deliver_company").find("#logisticsInfo").val(invocationResult.deliver_company + "  ¥" + parseFloat(parseInt(agent_fee)/100).toFixed(2) + " 元");
			addNewExpress(liDiv.attr("id"));
			jq("#sendTicket").listview("refresh");
			queryExistedExpressList();
		}else {	
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	//删除已保存送票信息
	function deleteExpressTicket(){
		jq("#sendTicket").on("tap","li #deleteSendTicket",function(e){
			e.stopImmediatePropagation();
			thisDeleteBtn = jq(this);
			if(mor.ticket.util.isAndroid()){
				WL.SimpleDialog.show("温馨提示", "您确定取消送票服务吗?", [
	     			{
	     				text : "确认",
	     				handler : function() {
	     					deleteExistedExpress(thisDeleteBtn);
	     				}
	     			}, 
	     			{
	     				text : "取消",
	     				handler : function() {
	     					return false;
	     				}
	     			}
	     			]);
			}else{
				WL.SimpleDialog.show("温馨提示", "您确定取消送票服务吗?", [
	     			{
	     				text : "取消",
	     				handler : function() {
	     					return false;
	     				}
	     			},
	     			{
	     				text : "确认",
	     				handler : function() {
	     					deleteExistedExpress(thisDeleteBtn);
	     				}
	     			} 
	     			]);
			}
			
		});
	}
	
	function deleteExistedExpress(thisBtn){
		var trackingNoAndSequenceNo = thisDeleteBtn.parent().parent().find(".sendTicketBtn").attr("id");
		deleteExpressTickets = [];
		for(var i=0;i<expressTickets.length;i++){
			var expressTrackingNoAndSequenceNo = expressTickets[i].tracking_no + expressTickets[i].ticket_message.split("+")[0];
			if(trackingNoAndSequenceNo == expressTrackingNoAndSequenceNo){
				deleteExpressTickets.push(expressTickets[i]);
			}
		}
		var commonParameters = {
				"tracking_no" : deleteExpressTickets[0].tracking_no,
				"ticket_message" : '',
				"from_tele_code" : deleteExpressTickets[0].sendTicketList[0].from_station_telecode,
				"region_code" : deleteExpressTickets[0].region_code,
				"deliver_company" : deleteExpressTickets[0].deliver_company,
				"addressee_name" : deleteExpressTickets[0].addressee_name,
				"addressee_province" : deleteExpressTickets[0].addressee_province,
				"addressee_city" : deleteExpressTickets[0].addressee_city,
				"addressee_county" : deleteExpressTickets[0].addressee_county,
				"addressee_town" : deleteExpressTickets[0].addressee_town,
				"addressee_street" : deleteExpressTickets[0].addressee_street,
				"detail_address" : deleteExpressTickets[0].detail_address,
				"mobile_no" : deleteExpressTickets[0].mobile_no,
				"phone_no" : deleteExpressTickets[0].phone_no,
				"service_type" : deleteExpressTickets[0].service_type,
				"pay_type" : deleteExpressTickets[0].pay_type,
				"deliver_mode" : deleteExpressTickets[0].deliver_mode,
				"start_date" : deleteExpressTickets[0].start_date,
				"stop_date" : deleteExpressTickets[0].stop_date,
				"receive_date_flag" : deleteExpressTickets[0].receive_date_flag,
				"start_time" : deleteExpressTickets[0].start_time,
				"stop_time" : deleteExpressTickets[0].stop_time,
				"invoice_flag" : deleteExpressTickets[0].invoice_flag,
				"invoice_title" : deleteExpressTickets[0].invoice_title,
				"invoice_name" : deleteExpressTickets[0].invoice_name,
				"flag" : deleteExpressTickets[0].flag,
				"stop_hour" : deleteExpressTickets[0].stop_hour,
				"agent_fee" : deleteExpressTickets[0].agent_fee,
				"addition_cost" : deleteExpressTickets[0].addition_cost,
				"alien_fee" : deleteExpressTickets[0].alien_fee,
				"Deliver_comment" : deleteExpressTickets[0].Deliver_comment,
				"total_price" : deleteExpressTickets[0].total_price,
				"deliverAddress" : deleteExpressTickets[0].deliverAddress,
				"sequeueNo" : deleteExpressTickets[0].sendTicketList[0].sequence_no,
				"isAfterPay" : "N",
				"isDelete" : "true"
		};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "updateAndDeleteExpressTickets"
			};
		
		var options =  {
				onSuccess: requestDeleteExpressTicketsSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestDeleteExpressTicketsSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.util.alertMessage("删除送票信息成功");
			var thisLiDiv = thisDeleteBtn.parent().parent();
			thisLiDiv.find("#status").html("(待确认)");
			thisLiDiv.find(".sendTicketBtn").attr("id","");
			var thisTrLength = thisLiDiv.find("input[type='checkbox']").length;
			for(var i=0;i<thisTrLength;i++){
				thisLiDiv.find("input[type='checkbox']").eq(i).attr("disabled",false);
			}
			
			if(unSendTicketLists.length>0){
				for(var j=0;j<unSendTicketLists.length;j++){
					var unSendTicketListTrHtml = "<tr id='"+unSendTicketLists[j].index_flag+"'>"
				  	+ "<td style='width:10%'>"+unSendTicketLists[j].station_train_code+"</td>"
					+ "<td style='width:20%'>"+unSendTicketLists[j].passenger_name+"</td>"
					+ "<td style='width:20%'>"+mor.ticket.util.getTicketTypeName(unSendTicketLists[j].ticket_type_code)+"</td>"
					+ "<td style='width:40%'>"+mor.ticket.util.getSeatTypeName(unSendTicketLists[j].seat_type_code)+''+unSendTicketLists[j].coach_name+'车'+unSendTicketLists[j].seat_name+"</td>"
					+ "<td style='width:5%' style='pointer:none;'>"
					+ "<input type='checkbox' name='checkbox' id='checkbox_"+unSendTicketLists[j].index_flag+"' style='pointer:none;'>"
					+ "</td>"
					+ "</tr>";
					thisLiDiv.find("tbody").append(unSendTicketListTrHtml);
				}
			}

			thisLiDiv.find("#saveUpdateSendTicket").css("display","none");
			thisLiDiv.find("#cancelUpdateSendTicket").css("display","none");
			thisLiDiv.find("#deleteSendTicket").css("display","none");
			
			thisLiDiv.find("#saveSendTicket").css("display","");
			thisLiDiv.find("#updateSendTicket").css("display","none");
			thisLiDiv.find("a[id^='address']").removeClass("ui-disabled");
			jq("#sendTicket").listview("refresh");
			queryExistedExpressList();
		}else {	
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	
	function updateSendTicket(){
		jq("#sendTicket").on("tap","li #updateSendTicket",function(e){
			e.stopImmediatePropagation();
			if(!mor.ticket.datebox.calbox){
				return false;
			}
			mor.ticket.datebox.calbox = false;
			setTimeout(function(){
				mor.ticket.datebox.calbox = true;
			},500);
			jq(this).css("display","none");
			var liDiv = jq(this).parent().parent();
			liDiv.find("#saveUpdateSendTicket").css("display","");
			liDiv.find("#cancelUpdateSendTicket").css("display","");
			liDiv.find("#deleteSendTicket").css("display","none");
			liDiv.find("a[id^='address']").removeClass("ui-disabled");
			liDiv.find("#sendTicketDate").removeClass("ui-disabled");
			liDiv.find("#sendTicketTime").removeClass("ui-disabled");
			var thisTrLength = liDiv.find("input[type='checkbox']").length;
			for(var i=0;i<thisTrLength;i++){
				liDiv.find("input[type='checkbox']").eq(i).attr("disabled",false);
			}
			var thisLiIdValue = liDiv.attr("id");
			if(unSendTicketLists.length>0){
				var unSendTicketListTrHtml = "";
				cancelUpdataSendTicket = [];
				for(var i=0;i<unSendTicketLists.length;i++){
					cancelUpdataSendTicket.push(unSendTicketLists[i]);
					cancelUpdataSendTicketSave.push(unSendTicketLists[i]);
					unSendTicketListTrHtml = "<tr id='"+unSendTicketLists[i].index_flag+"'>"
				  	+ "<td style='width:10%'>"+unSendTicketLists[i].station_train_code+"</td>"
					+ "<td style='width:20%'>"+unSendTicketLists[i].passenger_name+"</td>"
					+ "<td style='width:20%'>"+mor.ticket.util.getTicketTypeName(unSendTicketLists[i].ticket_type_code)+"</td>"
					+ "<td style='width:40%'>"+mor.ticket.util.getSeatTypeName(unSendTicketLists[i].seat_type_code)+''+unSendTicketLists[i].coach_name+'车'+unSendTicketLists[i].seat_name+"</td>"
					+ "<td style='width:5%' style='pointer:none;'>"
					+ "<input type='checkbox' name='checkbox' id='checkbox_"+unSendTicketLists[i].index_flag+"' style='pointer:none;'>"
					+ "</td>"
					+ "</tr>";
					jq("#"+thisLiIdValue).find("tbody").append(unSendTicketListTrHtml);
				}
				jq("#sendTicket").listview("refresh");
			}
			return false;
		});
	}
	function cancelUpdateSendTicket(){
		jq("#sendTicket").on("tap","li #cancelUpdateSendTicket",function(e){
			e.stopImmediatePropagation();
			if(!mor.ticket.datebox.calbox){
				return false;
			}
			mor.ticket.datebox.calbox = false;
			setTimeout(function(){
				mor.ticket.datebox.calbox = true;
			},500);
			thisCancelUpdateBtn = jq(this);
			var liDiv = thisCancelUpdateBtn.parent().parent();
			var trackingNoAndSequenceNo = liDiv.find(".sendTicketBtn").attr("id");
			var updateExpressTicket = null;
			for(var i=0;i<expressTickets.length;i++){
				var expressTrackingNoAndSequenceNo = expressTickets[i].tracking_no + expressTickets[i].ticket_message.split("+")[0];
				if(trackingNoAndSequenceNo == expressTrackingNoAndSequenceNo){
					updateExpressTicket = expressTickets[i];
					break;
				}
			}
			var editFlag = false;// 修改标志
			// 原送票信息车票列表
			var expressTicketList = updateExpressTicket.sendTicketList;
			// 修改过程车票列表
			var currTicketList = liDiv.find("input[type='checkbox']:checked");
			if(expressTicketList.length != currTicketList.length){
				editFlag = true;
			}else{
				// 首先，判断车票是否一致
				for(var i = 0;i<currTicketList.length;i++){
					var check_box = currTicketList[i];
					var currTicket_index = check_box.getAttribute("id").substring(9);
					var isExisted = false;
					for(var j = 0;i<expressTicketList.length;j++){
						if(currTicket_index == expressTicketList[j].index_flag){
							isExisted = true;
							break;
						}
					}
					if(!isExisted){
						editFlag = true;
						break;
					}
				}
				// 车票没有修改，则判断地址是否一致
				if(!editFlag){
					if((liDiv.find("#addressee").html() != updateExpressTicket.addressee_name)
							|| (liDiv.find("#addresseeTelephone").html() != updateExpressTicket.mobile_no)
							|| (liDiv.find("#addresseeAddress").html() != (updateExpressTicket.addressee_province+""+updateExpressTicket.addressee_city+""+updateExpressTicket.addressee_county))
							|| (liDiv.find("#addresseeDetailsAddress").html() != updateExpressTicket.detail_address)){
						editFlag = true;
					}
				}
			}
			if(!editFlag){
				cancelUpdateSendTicketBtn();
			}
			else if(mor.ticket.util.isAndroid()){
				WL.SimpleDialog.show("温馨提示", "您确定放弃修改送票服务吗?", [
	     			{
	     				text : "确认",
	     				handler : function() {
	     					cancelUpdateSendTicketBtn();
	     				}
	     			}, 
	     			{
	     				text : "取消",
	     				handler : function() {
	     					return false;
	     				}
	     			}
	     			]);
			}else{
				WL.SimpleDialog.show("温馨提示", "您确定放弃修改送票服务吗?", [
	     			{
	     				text : "取消",
	     				handler : function() {
	     					return false;
	     				}
	     			},
	     			{
	     				text : "确认",
	     				handler : function() {
	     					cancelUpdateSendTicketBtn();
	     				}
	     			} 
	     			]);
			}
			
		});
	} 
	function cancelUpdateSendTicketBtn(){
			thisCancelUpdateBtn.css("display","none");
			var currLi = thisCancelUpdateBtn.parent().parent();
			var thisLiIdValue = currLi.attr("id");
			currLi.find("#saveUpdateSendTicket").css("display","none");
			currLi.find("#updateSendTicket").css("display","");
			currLi.find("#deleteSendTicket").css("display","");
			currLi.find("a[id^='address']").addClass("ui-disabled");
			currLi.find("#sendTicketDate").addClass("ui-disabled");
			currLi.find("#sendTicketTime").addClass("ui-disabled");
			var trackingNo = currLi.find(".sendTicketBtn").attr("id").substring(0,10);
			
			var thisExpressTicketsSendTicketList = [];
			var expressTicketsSendTicketList = "";
			var thisExpress = null;
			// 当前物流单的车票列表
			for(var i=0;i<expressTickets.length;i++){
				if(trackingNo == expressTickets[i].tracking_no){
					thisExpress = expressTickets[i];
					expressTicketsSendTicketList = expressTickets[i].sendTicketList;
					break;
				}
			}
			// 遍历当前物流单中车票信息
			for(var k=0;k<expressTicketsSendTicketList.length;k++){
				// 在所有送票列表中标识当前车票为已送票
				for(var w=0;w<sendTicketLists.length;w++){
					if(expressTicketsSendTicketList[k].index_flag == sendTicketLists[w].index_flag){
						sendTicketLists[w].send_flag = "1";
						thisExpressTicketsSendTicketList.push(expressTicketsSendTicketList[k]);
					}
				}
				// 从当前未添加送票(未勾选)列表中移除当前车票信息
				for(var z=0;z<unSendTicketLists.length;z++){
					if(expressTicketsSendTicketList[k].index_flag == unSendTicketLists[z].index_flag){
						unSendTicketLists.splice(z,1);
						z--;
					}
				}
				// 从当前已添加送票（已勾选）列表中移除当前车票信息
				for(var t=0;t<addUnSendTicketLists.length;t++){
					if(expressTicketsSendTicketList[k].index_flag == addUnSendTicketLists[t].index_flag){
						addUnSendTicketLists.splice(t,1);
						t--;
					}
				}
			}
			
			var unSaveCancelSendTicketList = currLi.find("tr");
			// 当前取消修改的物流单车票信息
			if(unSaveCancelSendTicketList.length>0){
				// 循环遍历当前配送单中车票信息
				for(var j=0;j<unSaveCancelSendTicketList.length;j++){
					var count = 0;
					// 判断当前配送单中的车票信息是否为原物流单中车票信息
					for(var k=0;k<expressTicketsSendTicketList.length;k++){
						if(unSaveCancelSendTicketList.eq(j).attr("id") == expressTicketsSendTicketList[k].index_flag){
							count ++;
						}	
					}
					// 若是当前配送单中车票信息（且已勾选）不在原物流单中，则添加到未送票信息列表中并标识为未送票
					if(count == 0 && unSaveCancelSendTicketList.eq(j).find("input[type='checkbox']").is(":checked")==true){
						for(var g=0;g<sendTicketLists.length;g++){
							if(unSaveCancelSendTicketList.eq(j).attr("id") == sendTicketLists[g].index_flag){
								sendTicketLists[g].send_flag = "0";
								unSendTicketLists.push(sendTicketLists[g]);
								addUnSendTicketLists.push(sendTicketLists[g]);
							}
						}
					}
				}
		}
		// 遍历其他未保存或修改中的配送单（除去当前配送单）	
		for(var h=1;h<=addSendTicketAdressCount;h++){
			var currExpress = jq("#"+h);
			if(thisLiIdValue != h && ((currExpress.find("#saveUpdateSendTicket").css("display") != "none" || currExpress.find("#saveSendTicket").css("display") != "none"))){
				var trLeng = currExpress.find("tr").length;
				for(var j = trLeng-1; j>=0;j--){
					var currTr = currExpress.find("tr").eq(j);
					// 移除原物流单中车票信息
					for(var n = 0;n<expressTicketsSendTicketList.length;n++){
						if(currTr.attr("id") == expressTicketsSendTicketList[n].index_flag){
							currTr.remove();
							break;
						}
					}
				}
			}
		}	
		// 将原物流单的车票信息添加当前配送单
		currLi.find("tbody").empty();
		var saveSendTicketListTrHtml = "";
		for(var j=0;j<thisExpressTicketsSendTicketList.length;j++){
			saveSendTicketListTrHtml = "<tr id='"+thisExpressTicketsSendTicketList[j].index_flag+"'>"
				+ "<td style='width:10%'>"+thisExpressTicketsSendTicketList[j].station_train_code+"</td>"
				+ "<td style='width:20%'>"+thisExpressTicketsSendTicketList[j].passenger_name+"</td>"
				+ "<td style='width:20%'>"+mor.ticket.util.getTicketTypeName(thisExpressTicketsSendTicketList[j].ticket_type_code)+"</td>"
				+ "<td style='width:40%'>"+mor.ticket.util.getSeatTypeName(thisExpressTicketsSendTicketList[j].seat_type_code)+''+thisExpressTicketsSendTicketList[j].coach_name+'车'+thisExpressTicketsSendTicketList[j].seat_name+"</td>"
				+ "<td style='width:5%' style='pointer:none;'>"
				+ "<input type='checkbox' name='checkbox' id='checkbox_"+thisExpressTicketsSendTicketList[j].index_flag+"' style='pointer:none;' checked disabled>"
				+ "</td>"
				+ "</tr>";
				jq("#"+thisLiIdValue).find("tbody").append(saveSendTicketListTrHtml);
		}
		// 送票地址回复原来的地址
		currLi.find("#addressee").html(thisExpress.addressee_name);
		currLi.find("#addresseeTelephone").html(thisExpress.mobile_no);
		currLi.find("#addresseeAddress").html(thisExpress.addressee_province+""+thisExpress.addressee_city+""+thisExpress.addressee_county);
		currLi.find("#addresseeDetailsAddress").html(thisExpress.detail_address);
		setAddressHeight();
		addNewExpress(thisLiIdValue);
		
		addUnSendTicketLists = [];
		unSendTicketLists = [];
		for(var i=0;i<sendTicketLists.length;i++){
			if(sendTicketLists[i].send_flag != "1"){
				addUnSendTicketLists.push(sendTicketLists[i]);
			}
		}
		if(addUnSendTicketLists.length == 0){
			return false;
		}
		var addUnSendTicketListsLength = addUnSendTicketLists.length;
		if(addUnSendTicketListsLength>5){
			for(var j=0;j<addUnSendTicketLists.length;j++){
				unSendTicketLists.push(addUnSendTicketLists[j]);
			}
		}
		return false;
	}
	
function addNewExpress(currExpressId){
	var isNeedAddExpress = true;
	isNeedAddExpress = addUnSendTicketToEdit(currExpressId);
	// 判断是否添加新的配送单
	if(isNeedAddExpress){
		addNewSendTicket();
	}
	jq("#sendTicket").listview("refresh");
}

function addNewSendTicket(){
	if(addUnSendTicketLists.length > 0){
		addSendTicketAdressCount = addSendTicketAdressCount + 1;
		jq("#sendTicket").append("<li id='"+addSendTicketAdressCount+"'></li>");
		jq("#"+addSendTicketAdressCount).html(generateSendTicketListContent(addUnSendTicketLists));
		jq("#"+addSendTicketAdressCount).find("#Num").html(addSendTicketAdressCount);
		jq("#"+addSendTicketAdressCount).show();
		jq("#"+addSendTicketAdressCount+" #addImg").show();
	}
	jq("#sendTicket").listview("refresh");
}
//将未标识送票的车票分散到其他处于修改或未保存的配送单	
function addUnSendTicketToEdit(currExpressId){
	var isNeedAddExpress = true;
	for(var i=1;i<=addSendTicketAdressCount;i++){
		var currExpress = jq("li#"+i);
		if(currExpressId != i && (currExpress.find("#saveUpdateSendTicket").css("display") != "none" 
			|| currExpress.find("#saveSendTicket").css("display") != "none")){
			for(var j=0;j<unSendTicketLists.length;j++){
				var trLeng = currExpress.find("tr").length;
				var isContinue = false;
				for(var t = 0; t < trLeng;t++){
					var currTr = currExpress.find("tr").eq(t);
					if(currTr.attr("id") == unSendTicketLists[j].index_flag){
						isContinue = true;
						break;
					}
				}
				if(isContinue){
					continue;
				}
				var unSendTicketListTrHtml = "<tr id='"+unSendTicketLists[j].index_flag+"'>"
				+ "<td style='width:10%'>"+unSendTicketLists[j].station_train_code+"</td>"
				+ "<td style='width:20%'>"+unSendTicketLists[j].passenger_name+"</td>"
				+ "<td style='width:20%'>"+mor.ticket.util.getTicketTypeName(unSendTicketLists[j].ticket_type_code)+"</td>"
				+ "<td style='width:40%'>"+mor.ticket.util.getSeatTypeName(unSendTicketLists[j].seat_type_code)+''+unSendTicketLists[j].coach_name+'车'+unSendTicketLists[j].seat_name+"</td>"
				+ "<td style='width:5%' style='pointer:none;'>"
				+ "<input type='checkbox' name='checkbox' id='checkbox_"+unSendTicketLists[j].index_flag+"' style='pointer:none;'>"
				+ "</td>"
				+ "</tr>";
				jq("#"+i).find("tbody").append(unSendTicketListTrHtml);
				}
			isNeedAddExpress = false;
		}
	}
	jq("#sendTicket").listview("refresh");
	return isNeedAddExpress;
}
 var sendTicketListTemplate = "<div class='sendTicketBtn' id=''>"
	 	+ "送票信息-<span id='Num'></span><span id='status'>(待确认)</span>"
	  	+ "</div>"
	 	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all' style='margin-top:-32px;'>"
		+ "		<a id='updateSendTicket' data-role='button' class='sendTicketButton_right' style='display:none;'><img src='../images/express_close.png'/></a>"
		+ "		<a id='cancelUpdateSendTicket' data-role='button' class='sendTicketButton_right' style='display:none;'><img src='../images/express_open.png'/></a>"
	  	+ "</div>"
  		+ "<table id='sendTicketList_1' style='text-align: center; border-spacing:0px; width: 100%;margin-right: 5px;'>"	
  		+ "{{~it :sendTicketList:index}}"
  		+ "<tr id='{{=sendTicketList.index_flag}}'>"
  		+ "<td style='width:10%'>{{=sendTicketList.station_train_code}}</td>"
		+ "<td style='width:20%'><span class='text-ellipsis'>{{=sendTicketList.passenger_name}}</span></td>"
		+ "<td style='width:20%'>{{=mor.ticket.util.getTicketTypeName(sendTicketList.ticket_type_code)}}</td>"
		+ "<td style='width:40%'>{{=mor.ticket.util.getSeatTypeName(sendTicketList.seat_type_code)}}{{=sendTicketList.coach_name}}车{{=sendTicketList.seat_name}}</td>"
		+ "<td style='width:5%' style='pointer:none;'>"
		+ "{{ if(sendTicketList.send_flag == '1'){ }}"
		+ "<input type='checkbox' name='checkbox' id='checkbox_{{=sendTicketList.index_flag}}' checked style='pointer:none;'>"
		+ "{{ }else{ }}"
		+ "<input type='checkbox' name='checkbox' id='checkbox_{{=sendTicketList.index_flag}}' style='pointer:none;'>"
		+ "{{ } }}"
		+ "</td>"
		+ "</tr>"
		+ "{{~}}"
  		+ "</table>"
	    + "<div class='departlongline ui-field-contain' data-role='fieldcontain'>"
	    + "<label id='address'>送票地址</label>"
	    + "<div class='pencil-input'  style='margin-top:2px;'>"
	    + "<a name='address' id='address_{{=index}}'><span id='addressee' style='display: inline-block; line-height: 20px;'></span><img id='addImg' src='../images/add.png' style='width:25px;height:25px;margin-left:30%;margin-bottom:-16px;display:none;'/><span id='addresseeTelephone'></span><br>"
	    + "<span id='addresseeAddress' style='display: inline-block; line-height: 20px;'></span>"
	    + "<span id='addresseeDetailsAddress' style='display: inline; line-height: 20px;'></span></a>"
	    + "</div>"
	    +"</div>"
	    + "<div class='departlongline ui-field-contain' data-role='fieldcontain' id='deliver_company' style='display:none;'>"
	    + "<label>送票服务</label>"
	    + "<div class='pencil-input'>"
	    + 	"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'>"
	    + 		"<input id='logisticsInfo' style='font-size:14px;' value='' disabled>"
	    + 	"</div>"
	    + "</div>"
	    + "</div>"
	 	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all'>"
		+ "		<a id='saveUpdateSendTicket' data-role='button' class='sendTicketButton_center' style='display:none;'>确认</a>"
		+ "		<a id='saveSendTicket' data-role='button' class='sendTicketButton_center' style='display:;'>确认</a>"
		+ "		<a id='deleteSendTicket' data-role='button' class='sendTicketButton_center_gray' style='display:none;'>取消</a>"
	  	+ "</div>";
 var generateSendTicketListContent = doT.template(sendTicketListTemplate);
 
 
 var querySendTicketListTemplate ="{{~it :expressTickets:index}}"
	 	+ "<li id='{{=index+1}}'>"
	 	+ "<div class='sendTicketBtn' id='{{=expressTickets.tracking_no}}{{=expressTickets.sendTicketList[0].sequence_no}}'>"
	 	+ "		送票信息-<span id='Num'>{{=index+1}}</span><span id='status'></span>"
	 	+ "</div>"
	 	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all' style='margin-top:-32px;'>"
		+ "		<a id='updateSendTicket' data-role='button' class='sendTicketButton_right' style='display:;'><img src='../images/express_close.png'/></a>"
		+ "		<a id='cancelUpdateSendTicket' data-role='button' class='sendTicketButton_right' style='display:none;'><img src='../images/express_open.png'/></a>"
	  	+ "</div>"
		+ "<table id='sendTicketList_1' style='text-align: center; border-spacing:0px; width: 100%;margin-right: 5px;'>"
		+ "{{ for(var t=0;t<expressTickets.sendTicketList.length;t++) { }}"
		+ "<tr id='{{=expressTickets.sendTicketList[t].index_flag}}'>"
		+ "<td style='width:10%'>{{=expressTickets.sendTicketList[t].station_train_code}}</td>"
		+ "<td style='width:20%'><span class='text-ellipsis'>{{=expressTickets.sendTicketList[t].passenger_name}}</span></td>"
		+ "<td style='width:20%'>{{=mor.ticket.util.getTicketTypeName(expressTickets.sendTicketList[t].ticket_type_code)}}</td>"
		+ "<td style='width:40%'>{{=mor.ticket.util.getSeatTypeName(expressTickets.sendTicketList[t].seat_type_code)}}{{=expressTickets.sendTicketList[t].coach_name}}车{{=expressTickets.sendTicketList[t].seat_name}}</td>"
		+ "<td style='width:5%' style='pointer:none;'>"
		+ "<input type='checkbox' name='checkbox' id='checkbox_{{=expressTickets.sendTicketList[t].index_flag}}' checked style='pointer:none;' disabled>"
		+ "</td>"
		+ "</tr>"
		+ "{{ } }}"
		+ "</table>"
	    + "<div class='departlongline ui-field-contain' data-role='fieldcontain'>"
	    + "<label id='address'>送票地址</label>"
	    + "<div class='pencil-input' style='margin-top:2px;'>"
	    + "<a name='address' id='address_{{=index+1}}' class='ui-disabled'><span id='addressee' style='display: inline-block; line-height: 20px;'>{{=expressTickets.addressee_name}}</span>&nbsp;<span id='addresseeTelephone'>{{=expressTickets.mobile_no}}</span><br>"
	    + "<span id='addresseeAddress' style='display: inline-block; line-height: 20px;'>{{=expressTickets.addressee_province+''+expressTickets.addressee_city+''+expressTickets.addressee_county}}</span>"
	    + "<span id='addresseeDetailsAddress' style='display: inline-block; line-height: 20px;'>{{=expressTickets.detail_address}}</span></a>"
	    + "</div>"
	    +"</div>"
	    + "<div class='departlongline  ui-field-contain' data-role='fieldcontain' id='deliver_company' style='display:;'>"
	    + "<label>送票服务</label>"
	    + "<div class='pencil-input'>"
	    + 	"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'>"
	    + 		"<input id='logisticsInfo' style='font-size:14px;' value='{{=expressTickets.deliver_company}} ¥{{=parseFloat(parseInt(expressTickets.total_price)/100).toFixed(2)}} 元' disabled>"
	    + 	"</div>"
	    + "</div>"
	    + "</div>"
	 	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all'>"
		+ "		<a id='saveUpdateSendTicket' data-role='button' class='sendTicketButton_center' style='display:none;'>确认</a>"
		+ "		<a id='saveSendTicket' data-role='button' class='sendTicketButton_center' style='display:none;'>确认</a>"
		+ "		<a id='deleteSendTicket' data-role='button' class='sendTicketButton_center_gray' style='display:;'>取消</a>"
	  	+ "</div>"
	    + "</li>"
	    + "{{~}}";
var generateQuerySendTicketListContent = doT.template(querySendTicketListTemplate);

var savedExpressInfo = "<span id='status'></span></div>"
	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all' style='margin-top:-32px;'>"
	+ "		<a id='updateSendTicket' data-role='button' class='sendTicketButton_right' style='display:;'><img src='../images/express_close.png'/></a>"
	+ "		<a id='cancelUpdateSendTicket' data-role='button' class='sendTicketButton_right' style='display:none;'><img src='../images/express_open.png'/></a>"
  	+ "</div>"
	+ "<table id='sendTicketList_{{=order.order_no}}' style='text-align: center; border-spacing:0px; width: 100%;margin-right: 5px;'>"
	+ "{{ for(var t=0;t<order.ticketList.length;t++) { }}"
	+ "<tr id='{{=order.ticketList[t].index_flag}}'>"
	+ "<td style='width:10%'>{{=order.ticketList[t].station_train_code}}</td>"
	+ "<td style='width:20%'><span class='text-ellipsis'>{{=order.ticketList[t].passenger_name}}</span></td>"
	+ "<td style='width:20%'>{{=mor.ticket.util.getTicketTypeName(order.ticketList[t].ticket_type_code)}}</td>"
	+ "<td style='width:40%'>{{=mor.ticket.util.getSeatTypeName(order.ticketList[t].seat_type_code)}}{{=order.ticketList[t].coach_name}}车{{=order.ticketList[t].seat_name}}</td>"
	+ "<td style='width:5%' style='pointer:none;'>"
	+ "<input type='checkbox' name='checkbox' id='checkbox_{{=order.ticketList[t].index_flag}}' checked style='pointer:none;' disabled>"
	+ "</td>"
	+ "</tr>"
	+ "{{ } }}"
	+ "</table>"
    + "<div class='departlongline ui-field-contain' data-role='fieldcontain'>"
    + "<label id='address'>送票地址</label>"
    + "<div class='pencil-input' style='margin-top:2px;'>"
    + "<a name='address' id='address_{{=index+1}}' class='ui-disabled'><span id='addressee' style='display: inline-block; line-height: 20px;'>{{=order.address_name}}</span>&nbsp;<span id='addresseeTelephone'>{{=order.address_tel}}</span><br>"
    + "<span id='addresseeAddress' style='display: inline-block; line-height: 20px;'>{{=order.address_addr}}</span>"
    + "<span id='addresseeDetailsAddress' style='display: inline-block; line-height: 20px;'>{{=order.address_detail}}</span></a>"
    + "</div>"
    +"</div>"
    + "<div class='departlongline ui-field-contain' data-role='fieldcontain' id='deliver_company' style='display:;'>"
    + "<label>送票服务</label>"
    + "<div class='pencil-input'>"
    + 	"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'>"
    + 		"<input id='logisticsInfo' style='font-size:14px;' value='{{=order.deliver_company}}' disabled>"
    + 	"</div>"
    + "</div>"
    + "</div>"
	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all'>"
	+ "		<a id='saveUpdateSendTicket' data-role='button' class='sendTicketButton_center' style='display:none;'>确认</a>"
	+ "		<a id='saveSendTicket' data-role='button' class='sendTicketButton_center' style='display:none;'>确认</a>"
	+ "		<a id='deleteSendTicket' data-role='button' class='sendTicketButton_center_gray' style='display:;'>取消</a>"
  	+ "</div>";

var editExpressInfo = "<span id='status'></span></div>"
	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all' style='margin-top:-32px;'>"
	+ "		<a id='updateSendTicket' data-role='button' class='sendTicketButton_right' style='display:none;'><img src='../images/express_close.png'/></a>"
	+ "		<a id='cancelUpdateSendTicket' data-role='button' class='sendTicketButton_right' style='display:;'><img src='../images/express_open.png'/></a>"
  	+ "</div>"
	+ "<table id='sendTicketList_{{=order.order_no}}' style='text-align: center; border-spacing:0px; width: 100%;margin-right: 5px;'>"
	+ "{{ for(var t=0;t<order.ticketList.length;t++) { }}"
	+ "<tr id='{{=order.ticketList[t].index_flag}}'>"
	+ "<td style='width:10%'>{{=order.ticketList[t].station_train_code}}</td>"
	+ "<td style='width:20%'><span class='text-ellipsis'>{{=order.ticketList[t].passenger_name}}</span></td>"
	+ "<td style='width:20%'>{{=mor.ticket.util.getTicketTypeName(order.ticketList[t].ticket_type_code)}}</td>"
	+ "<td style='width:40%'>{{=mor.ticket.util.getSeatTypeName(order.ticketList[t].seat_type_code)}}{{=order.ticketList[t].coach_name}}车{{=order.ticketList[t].seat_name}}</td>"
	+ "<td style='width:5%' style='pointer:none;'>"
	+ "<input type='checkbox' name='checkbox' id='checkbox_{{=order.ticketList[t].index_flag}}' " +
			"{{ if(order.ticketList[t].send_flag == '1'){ }}" +
			"checked " +
			"{{ } }}" +
			"style='pointer:none;'>" 
	+ "</td>"
	+ "</tr>"
	+ "{{ } }}"
	+ "</table>"
    + "<div class='departlongline ui-field-contain' data-role='fieldcontain'>"
    + "<label id='address'>送票地址</label>"
    + "<div class='pencil-input' style='margin-top:2px;'>"
    + "<a name='address' id='address_{{=index+1}}'><span id='addressee' style='display: inline-block; line-height: 20px;'>{{=order.address_name}}</span>&nbsp;<span id='addresseeTelephone'>{{=order.address_tel}}</span><br>"
    + "<span id='addresseeAddress' style='display: inline-block; line-height: 20px;'>{{=order.address_addr}}</span>"
    + "<span id='addresseeDetailsAddress' style='display: inline-block; line-height: 20px;'>{{=order.address_detail}}</span></a>"
    + "</div>"
    +"</div>"
    + "<div class='departlongline ui-field-contain' data-role='fieldcontain' id='deliver_company' style='display:;'>"
    + "<label>送票服务</label>"
    + "<div class='pencil-input'>"
    + 	"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'>"
    + 		"<input id='logisticsInfo' style='font-size:14px;' value='{{=order.deliver_company}}' disabled>"
    + 	"</div>"
    + "</div>"
    + "</div>"
	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all'>"
	+ "		<a id='saveUpdateSendTicket' data-role='button' class='sendTicketButton_center' style='display:;'>确认</a>"
	+ "		<a id='saveSendTicket' data-role='button' class='sendTicketButton_center' style='display:none;'>确认</a>"
	+ "		<a id='deleteSendTicket' data-role='button' class='sendTicketButton_center_gray' style='display:none;'>取消</a>"
	+ "</div>";

var orderInfo = "<span id='status'>(待确认)</span></div>"
 	+ "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all' style='margin-top:-32px;'>"
	+ "		<a id='updateSendTicket' data-role='button' class='sendTicketButton_right' style='display:none;'><img src='../images/express_close.png'/></a>"
	+ "		<a id='cancelUpdateSendTicket' data-role='button' class='sendTicketButton_right' style='display:none;'><img src='../images/express_open.png'/></a>"
  	+ "</div>" 	
	+ "<table id='sendTicketList_{{=order.order_no}}' style='text-align: center; border-spacing:0px; width: 100%;margin-right: 5px;'>"
	+ "{{ for(var t=0;t<order.ticketList.length;t++) { }}"
	+ "<tr id='{{=order.ticketList[t].index_flag}}'>"
	+ "<td style='width:10%'>{{=order.ticketList[t].station_train_code}}</td>"
	+ "<td style='width:20%'><span class='text-ellipsis'>{{=order.ticketList[t].passenger_name}}</span></td>"
	+ "<td style='width:20%'>{{=mor.ticket.util.getTicketTypeName(order.ticketList[t].ticket_type_code)}}</td>"
	+ "<td style='width:40%'>{{=mor.ticket.util.getSeatTypeName(order.ticketList[t].seat_type_code)}}{{=order.ticketList[t].coach_name}}车{{=order.ticketList[t].seat_name}}</td>"
	+ "<td style='width:5%' style='pointer:none;'>"
	+ "<input type='checkbox' name='checkbox' id='checkbox_{{=order.ticketList[t].index_flag}}' " +
	  "{{ if(order.ticketList[t].send_flag == '1'){ }}" +
	  "checked " +
	  "{{ } }} " +
	  "style='pointer:none;'>" 
	+ "</td>"
	+ "</tr>"
	+ "{{ } }}"
	+ "</table>"
    + "<div class='departlongline ui-field-contain' data-role='fieldcontain'>"
    + "<label id='address'>送票地址</label>"
    + "<div class='pencil-input' style='margin-top:2px;'>"
    + "<a name='address' id='address_{{=index+1}}'><span id='addressee' style='display: inline-block; line-height: 20px;'>{{=order.address_name}}</span>&nbsp;<img id='addImg' src='../images/add.png' style='width:25px;height:25px;margin-left:30%;margin-bottom:-16px;display:none;'/><span id='addresseeTelephone'>{{=order.address_tel}}</span><br>"
    + "<span id='addresseeAddress' style='display: inline-block; line-height: 20px;'>{{=order.address_addr}}</span>"
    + "<span id='addresseeDetailsAddress' style='display: inline-block; line-height: 20px;'>{{=order.address_detail}}</span></a>"
    + "</div>"
    + "</div>"
    + "<div class='departlongline ui-field-contain' data-role='fieldcontain' id='deliver_company' style='display:none;'>"
    + "<label>送票服务</label>"
    + "<div class='pencil-input'>"
    + 	"<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'>"
    + 		"<input id='logisticsInfo' style='font-size:14px;' value='' disabled>"
    + 	"</div>"
    + "</div>"
    + "</div>"
    + "<div data-role='controlgroup' data-type='horizontal' class='sendTicketButton_all'>"
	+ "		<a id='saveUpdateSendTicket' data-role='button' class='sendTicketButton_center' style='display:none;'>确认</a>"
	+ "		<a id='saveSendTicket' data-role='button' class='sendTicketButton_center' style='display:;'>确认</a>"
	+ "		<a id='deleteSendTicket' data-role='button' class='sendTicketButton_center_gray' style='display:none;'>取消</a>"
  	+ "</div>";

var showOrderInfoListTemplate ="{{~it :order:index}}"
 	+ "<li id='{{=order.order_no}}'>"
	+ "<div class='sendTicketBtn' id='{{=order.track_no}}'>"
 	+ "		送票信息-<span id='Num'>{{=order.order_no}}</span>"
	+ "{{ if(order.status == 'edit') { }}"
	+  editExpressInfo
	+ "{{ }else if(order.status == 'unEdit'){ }}"
	+  savedExpressInfo
	+ "{{ }else{ }}"
	+  orderInfo
	+ "{{ } }}"
    + "</div>"
    + "</li>"
    + "{{~}}";
var generateOrderInfoListContent = doT.template(showOrderInfoListTemplate);
})();