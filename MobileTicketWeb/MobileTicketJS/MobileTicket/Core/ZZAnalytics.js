/**
 * @providesModule ZZAnalytics
 * Created by easy on 16/1/13.
 */


'use strict';
let Async = require('ZZAsync');
let CloudAnalytics = require('NativeModules').CloudAnalytics;

class ZZAnalytics {
    static async beginPageView(pageView) {
        return Async.invoke(CloudAnalytics.beginPageView,pageView);
    }

    static async endPageView(pageView) {
        return Async.invoke(CloudAnalytics.endPageView,pageView);
    }

    static async event(event,label,attributes,duration) {
        if (typeof label == 'function' && typeof attributes != 'function') {
            attributes = label;
            label = undefined;
        }
        return Async.invoke(CloudAnalytics.event,{event,label,attributes,duration});
    }

    static async config(key) {
        return Async.invoke(CloudAnalytics.config,key);
    }
}

module.exports = ZZAnalytics;