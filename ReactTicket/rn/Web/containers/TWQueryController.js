/**
 * Created by easy on 16/3/19.
 */


import React,{
    Component,
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet,
    ListView,
    RefreshControl
} from 'react-native'

import moment from 'moment'
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux'
import * as actions from '../actions/QueryActions'

import {MKButton,MKColor,MKTextField} from 'react-native-material-kit'

import RTController from 'RTController'

import TWQueryCell from '../components/TWQueryCell'
import TWQuerySectionView from '../components/TWQuerySectionView'
import TWQueryToolbar from '../components/TWQueryToolbar'

class TWQueryController extends RTController {

    static defaultProps = {
        query : [],
        refreshing : false,
        loading : false,
    };

    constructor(props,context) {
        super(props,context);
    }

    componentDidMount() {
        setTimeout(_=>{
            this.onRequestQuery();
        },250);
    }

    componentWillUnmount() {
        this.props.actions.clearQueryRequest();
    }

    renderRow(rowData) {
        //return (<View style={{height:40}}/>);
        return (
            <TWQueryCell query={rowData}/>
        );
    }

    renderSectionHeader(sectionData) {
        return <TWQuerySectionView query={sectionData}/>
    }



    render() {
        let section = 'section';

        let datas = {
            [section] : this.props.query
        };


        let sectionIds = [section];
        let rowIds = [];
        this.props.query.forEach((t,index) => {
            rowIds.push('row'+index);
            datas[section+':row'+index] = t;
        });
        rowIds = [rowIds];

        let getSectionData = (data,sectionId)=> {
            return data[sectionId];
        }
        let getRowData = (data,sectionId,rowId) => {
            return data[sectionId+':'+rowId];
        }

        let ds = new ListView.DataSource({
            getSectionData : getSectionData,
            getRowData : getRowData,
            rowHasChanged: (r1, r2) => r1 !== r2,
            sectionHeaderHasChanged:(s1, s2) => s1 !== s2}
        );
        let dataSource = ds.cloneWithRowsAndSections(datas,sectionIds,rowIds);
        let refreshControl = <RefreshControl onRefresh={this.onRefresh.bind(this)} refreshing={this.props.refreshing}/>
        return (
            <View style={{flex:1}}>
                <ListView initialListSize={8}
                          renderRow={this.renderRow.bind(this)}
                          renderSectionHeader={this.renderSectionHeader.bind(this)}
                          dataSource={dataSource}
                          style={{flex:1,backgroundColor:'white'}}

                          refreshControl={refreshControl}/>
                <TWQueryToolbar onNext={this.onNextDay.bind(this)} onPrevious={this.onPreviousDay.bind(this)} day={this.props.day}/>
            </View>
        );
    }



    onPreviousDay() {
        let previousDay = moment(this.props.day).add(-1,'days').format('YYYY-MM-DD');
        this.props.actions.queryRequest('BJP','SHH',previousDay);
    }

    onNextDay() {
        let nextDay = moment(this.props.day).add(1,'days').format('YYYY-MM-DD');
        this.props.actions.queryRequest('BJP','SHH',nextDay);
    }

    onRequestQuery() {
        this.props.actions.queryRequest('BJP','SHH',this.props.day);
    }

    onRefresh() {
        this.props.actions.queryRequest('BJP','SHH',this.props.day,true);
    }
}

let mapStateToProps = (state,props) => {
    return {...state.Web.Query}
}

let mapDispatchToProps = (dispatch,ownProps) => {
    return {
        actions : bindActionCreators(actions,dispatch)
    };
}

module.exports = connect(mapStateToProps,mapDispatchToProps)(TWQueryController);