//
//  ZZControl.h
//  MobileTicket
//
//  Created by EASY on 15/11/13.
//  Copyright (c) 2015年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RCTComponent.h>
#import <RCTEventDispatcher.h>

@interface ZZControl : UIControl

- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher;

@property (nonatomic, strong) RCTDirectEventBlock onTouch;

@end
