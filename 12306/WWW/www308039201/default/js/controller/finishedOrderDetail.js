
/* JavaScript content from js/controller/finishedOrderDetail.js in folder common */
(function() {
	jq.extendModule("mor.ticket.expressOrder", {
		ExpressOrderArray : []
	});
	jq.extendModule("mor.ticket.changeOrchangeStation", {
		changeOrchangeStation : ""
	});
	jq.extendModule("mor.ticket.finishOrderTrack", {
		finishOrderTicketInfo : [],
		sequence_no : "",
		order_date : "",
		queryRefundInfoParameters : []
	});
	jq.extendModule("mor.ticket.orderRefundInfo", {
		info : []
	});
	var methodFinishFlag = '';
	var popupWindow = null;
	var changeOrChangeStation = "";
	jq("#FinishedOrderDetailView").live("pageinit", function() {
		registerChangeTrainListener();
		registerCancelTrainListener();
		registerChangeTicketCheckedBoxListener();
		registerChangeTicketBtnListener();
		registerChangeTostationTrainListener();
		registerCancelTicketBtnListener();
		registerSaveAndShareBtnClickHandler();
		registerMoreOperationClickHandler();
		jq("#FinishedOrderDetailViewBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("finishedOrderList.html");
			return false;
		});
		jq("#expressStatusDetailBtn").bind("tap",queryExpressStatusDetail);
		jq("#insuranceDetailBtn").bind("tap",queryInsuranceDetail);
		jq("#finishOrderTrackBtn").bind("tap",queryfinishOrderTrack);
		jq("#queryRefundInfoBtn").bind("tap",queryRefundInfo);
		jq("#orderTrackBtn").bind("tap",function(){
			mor.ticket.util.changePage("expressStatusDetail.html");
			return false;
		});
//		travelPlanbtnLisener();
		mor.ticket.util.initAppVersionInfo();
	});
	
	//右上角菜单
	function registerMoreOperationClickHandler(){
		popupWindow = new mor.ticket.util.simplePopup({
	        fireBtn: jq("#moreOperation"),
	        mask:    jq("#popupNested-screen"),
	        pop:     jq("#popupNested")
	    });
	}
	
	function queryInsuranceDetail(){
		mor.ticket.util.changePage("insuranceStatusDetail.html");
		return false;
	}
	
	function queryExpressStatusDetail(){
		var util = mor.ticket.util;
		var commonParameters = {};
		if(mor.ticket.viewControl.queryFinishedOrderType == "4"){
			commonParameters = {
					"sequence_no" : jq("#ticketSquenceNo").html(),
					"query_flag" : '2',
					"pay_flag" : '1'
			};
		}else{
			commonParameters = {
					"sequence_no" : jq("#ticketSquenceNo").html(),
					"query_flag" : '1',
					"pay_flag" : '1'
			};
		}
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryExpressTickets"
			};
			
		var options =  {
				onSuccess: requestExpressStatusDetailSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	function requestExpressStatusDetailSucceeded(result){
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			if(result.invocationResult.listExpressOrderForQueryReturnBean.length == 0){
				mor.ticket.util.alertMessage("您没有对应的快递信息");
			}else{
				mor.ticket.expressOrder.ExpressOrderArray = [];
				var orderDetailsList = mor.ticket.queryOrder.getCurrentFinishedOrdersMyTicketList();
				for(var i=0;i<result.invocationResult.listExpressOrderForQueryReturnBean.length;i++){
					var ticket_message = result.invocationResult.listExpressOrderForQueryReturnBean[i].ticket_message.split("#");
					for(var j = 0;j<orderDetailsList.length;j++){
						var temp_ticket_message = orderDetailsList[j].sequence_no + "+" + 
							orderDetailsList[j].batch_no + "+" +
							orderDetailsList[j].coach_no + "+" +
							orderDetailsList[j].seat_no + "+" +
							orderDetailsList[j].passenger_id_type_code + "+" +
							orderDetailsList[j].passenger_id_no + "+" +
							orderDetailsList[j].passenger_name;
						if(jq.inArray(temp_ticket_message,ticket_message) != -1){
							result.invocationResult.listExpressOrderForQueryReturnBean[i].station_train_code = orderDetailsList[j].station_train_code;
							result.invocationResult.listExpressOrderForQueryReturnBean[i].to_station_telecode = orderDetailsList[j].to_station_telecode;
							result.invocationResult.listExpressOrderForQueryReturnBean[i].from_station_telecode = orderDetailsList[j].from_station_telecode;
							break;
						}
					}
					result.invocationResult.listExpressOrderForQueryReturnBean[i].deleteId = result.invocationResult.listExpressOrderForQueryReturnBean[i].ticket_message.split('#')[0].split('+')[0]+"+"+
					result.invocationResult.listExpressOrderForQueryReturnBean[i].ticket_message.split('#')[0].split('+')[1]+"+"+
					result.invocationResult.listExpressOrderForQueryReturnBean[i].ticket_message.split('#')[0].split('+')[2]+"+"+
					result.invocationResult.listExpressOrderForQueryReturnBean[i].ticket_message.split('#')[0].split('+')[3]+"+"+
					result.invocationResult.listExpressOrderForQueryReturnBean[i].tracking_no;
					mor.ticket.expressOrder.ExpressOrderArray.push(result.invocationResult.listExpressOrderForQueryReturnBean[i]);
				}
				WL.Logger.error("expressDetailList=" + mor.ticket.expressOrder.ExpressOrderArray);
				jq.mobile.changePage("expressStatusDetail.html");
			}
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	function queryfinishOrderTrack(){
		jq("#popupNested-screen").click();
		var util = mor.ticket.util;
		if(mor.ticket.viewControl.queryFinishedOrderType == "4"){
			commonParameters = {
					"sequence_no" : jq("#ticketSquenceNo").html(),
					"query_where" : 'H',
			};
		}else{
			commonParameters = {
					"sequence_no" : jq("#ticketSquenceNo").html(),
					"query_where" : 'G',
			};
		}
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryFinishOrderTrack"
			};
			
		var options =  {
				onSuccess: requestFinishOrderTrackSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestFinishOrderTrackSucceeded(result){
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			var ticketInfoArray = [];
			mor.ticket.finishOrderTrack.finishOrderTicketInfo = [];
			mor.ticket.finishOrderTrack.sequence_no = jq("#ticketSquenceNo").html();
			mor.ticket.finishOrderTrack.order_date = jq("#ticketBookedDate").html();
			result.invocationResult.list.sort();
			if(methodFinishFlag == 'yes'){
				var trade_nos = "";
				var trade_modes = "";
				var start_times = "";
				var stop_times = "";
				for(var i=0;i<result.invocationResult.list.length;i++){
					if(result.invocationResult.list[i].trade_no != ""){
						trade_nos += result.invocationResult.list[i].trade_no+"#";
						trade_modes += result.invocationResult.list[i].trade_mode+"#";
						start_times +=	result.invocationResult.list[i].start_time.substring(0,8)+"#";
						stop_times	+=	result.invocationResult.list[i].stop_time.substring(0,8)+"#";
						ticketInfoArray.push(result.invocationResult.list[i].board_train_code+result.invocationResult.list[i].coach_no+result.invocationResult.list[i].seat_no);
					}
				}
				if(trade_nos != ""  && trade_modes != "" && start_times != "" && stop_times != ""){
					mor.ticket.finishOrderTrack.queryRefundInfoParameters = [];
					mor.ticket.finishOrderTrack.queryRefundInfoParameters.push(trade_nos);
					mor.ticket.finishOrderTrack.queryRefundInfoParameters.push(trade_modes);
					mor.ticket.finishOrderTrack.queryRefundInfoParameters.push(start_times);
					mor.ticket.finishOrderTrack.queryRefundInfoParameters.push(stop_times);
				}
			}else{
				for(var i=0;i<result.invocationResult.list.length;i++){
					//if(result.invocationResult.list[i].trade_no != ""){
						ticketInfoArray.push(result.invocationResult.list[i].board_train_code+result.invocationResult.list[i].coach_no+result.invocationResult.list[i].seat_no);
					//}
				}
				ticketInfoArray.sort();
				mor.ticket.util.uniq(ticketInfoArray);
				for(var j=0;j<ticketInfoArray.length;j++){
					if(ticketInfoArray[j] == ""){
						ticketInfoArray.splice(j,1);
					}
				}
			}
			for(var j=0;j<ticketInfoArray.length;j++){
				var oneNewTicketInfo = {};
				oneNewTicketInfo.operateTimeStatues = [];
				for(var i=0;i<result.invocationResult.list.length;i++){
					var ticketInfo2 = result.invocationResult.list[i].board_train_code+result.invocationResult.list[i].coach_no+result.invocationResult.list[i].seat_no;	
					if(methodFinishFlag == 'yes'){
						if(ticketInfoArray[j]==ticketInfo2){
							oneNewTicketInfo.ticketInfo = result.invocationResult.list[i];
						}
					}else{
						if(ticketInfoArray[j]==ticketInfo2){
							oneNewTicketInfo.ticketInfo = result.invocationResult.list[i];
							var channel = "窗口";
							if(result.invocationResult.list[i].office_name.indexOf("网售")!=-1){
								channel = "互联网";
							}else{
								channel = result.invocationResult.list[i].office_name;
							}
							var status = result.invocationResult.list[i].status;
							var state = "支付成功";
							if(status == "已支付"){
								state = '支付成功';
							}else if(status == "已改签"){
								state = '办理改签';
							}else if(status == "已退票"){
								state = '办理退票';
							}else if(status == "改签票"){
								state = '改签成功';
							}else if(status == "已出票"){
								state = '制票成功';
							}
							oneNewTicketInfo.operateTimeStatues.push(result.invocationResult.list[i].operate_time+"#"+state+"#"+channel);
						}
					}
				}
				if(methodFinishFlag != 'yes'){
					if(oneNewTicketInfo.operateTimeStatues.length>1){
						if(oneNewTicketInfo.operateTimeStatues[0].split("#")[0] > oneNewTicketInfo.operateTimeStatues[1].split("#")[0]){
							var aa = oneNewTicketInfo.operateTimeStatues[0];
							var bb = oneNewTicketInfo.operateTimeStatues[1];
							oneNewTicketInfo.operateTimeStatues[0] =  bb;
							oneNewTicketInfo.operateTimeStatues[1] =  aa;
						}
					}
				}
				mor.ticket.finishOrderTrack.finishOrderTicketInfo.push(oneNewTicketInfo);
			}
			if(methodFinishFlag == 'yes'){
				getQueryRefundInfo(mor.ticket.finishOrderTrack.queryRefundInfoParameters);
			}else{
				if(mor.ticket.finishOrderTrack.finishOrderTicketInfo.length > 0){
					mor.ticket.finishOrderTrack.queryRefundInfoParameters = [];
					jq.mobile.changePage("orderTrackDetail.html");
				}else{
					mor.ticket.util.alertMessage("没有查询到相关订单跟踪信息");
				}
			}
			return false;
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	function queryRefundInfo(){
		methodFinishFlag = 'yes';
		queryfinishOrderTrack();
	}
	function getQueryRefundInfo(queryRefundInfoParameters){
		if(queryRefundInfoParameters.length == 0){
			methodFinishFlag = '';
			mor.ticket.util.alertMessage("没有退款详情");
			return false;
		}
		var util = mor.ticket.util;
		var commonParameters = {
				"trade_nos" : queryRefundInfoParameters[0],
				"trade_modes" : queryRefundInfoParameters[1],
				"start_times" : queryRefundInfoParameters[2],
				"stop_times" : queryRefundInfoParameters[3],

			};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryRefundInfo"
			};
			
		var options =  {
				onSuccess: requestQueryRefundInfoSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function uniqArray(array) {
		for ( var i = 1; i < array.length; i++) {
			if (array[i].trade_no == array[i - 1].trade_no) {
				array.splice(i--, 1);
			}
		}
		return array;
	}
	
	
	function requestQueryRefundInfoSucceeded(result){
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			mor.ticket.orderRefundInfo.info = [];
			uniqArray(invocationResult.list);
			for(var i=0;i<invocationResult.list.length;i++){
				var passenger_name = "";
				for(var k=0;k<mor.ticket.finishOrderTrack.finishOrderTicketInfo.length;k++){
					if(invocationResult.list[i].trade_no == mor.ticket.finishOrderTrack.finishOrderTicketInfo[k].ticketInfo.trade_no){
						passenger_name += mor.ticket.finishOrderTrack.finishOrderTicketInfo[k].ticketInfo.passenger_name+"  ";
					}
				}
				for(var j=0;j<mor.ticket.finishOrderTrack.finishOrderTicketInfo.length;j++){
					if(invocationResult.list[i].trade_no == mor.ticket.finishOrderTrack.finishOrderTicketInfo[j].ticketInfo.trade_no){
						invocationResult.list[i].origin_name = mor.ticket.finishOrderTrack.finishOrderTicketInfo[j].ticketInfo.origin_name;
						invocationResult.list[i].passenger_name = passenger_name;
						invocationResult.list[i].status = mor.ticket.finishOrderTrack.finishOrderTicketInfo[j].ticketInfo.status;
						var transAmount = parseInt(invocationResult.list[i].transAmount);
						transAmount = parseFloat(transAmount/100);
						invocationResult.list[i].transAmount = transAmount.toFixed(2);
						mor.ticket.orderRefundInfo.info.push(invocationResult.list[i]);
						break;
					}
				}
			}
			methodFinishFlag = '';
			jq.mobile.changePage("queryRefundInfo.html");
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	
/*	function travelPlanbtnLisener() {
		jq("#travelPlan").bind(
				"tap",
				function(e) {
					var name = "";
					var idType = "";
					var ticket_type_code = "";
					e.stopImmediatePropagation();
					jq("#popupNested-screen").click();
					var  travelPlanList = JSON.parse(window.ticketStorage.getItem("travelPlanList"));
					var finishOrder = mor.ticket.queryOrder.getCurrentFinishedOrdersMyTicketList();
					finishOrder = jq.grep(finishOrder,function(item, planIndex) {
						return item.station_train_code == finishOrder[0].station_train_code;	
				});
					for(var i = 0;i<finishOrder.length;i++){
						 name = name +finishOrder[i].passenger_name +",";
						 idType = idType + finishOrder[i].passenger_id_type_code+",";
						 ticket_type_code = ticket_type_code + finishOrder[i].ticket_type_code+",";
					}
					
					finishOrder[0].passenger_name = name;
					finishOrder[0].passenger_id_type_code = idType;
					finishOrder[0].ticket_type_code = ticket_type_code;
					finishOrder[0].checked = 0;
					if(travelPlanList == null){
						for(var i = 1; i< finishOrder.length; i++){
							finishOrder.splice(i);
						}
						window.ticketStorage.setItem("travelPlanList",JSON.stringify(finishOrder));	
					}else{
								if(finishOrder[0] !=  undefined){
									travelPlanList.push(finishOrder[0]);
								}								
						travelPlanList =uniQueue(travelPlanList);
						window.ticketStorage.setItem("travelPlanList",JSON.stringify(travelPlanList));
					}		
					mor.ticket.util.changePage("travelPlan.html");
					return false;
				});

	}*/

/*	function uniQueue(array){ 
		var arr=[]; 
		var m; 
		while(array.length>0){ 
		m=array[0]; 
		arr.push(m); 
		array=jq.grep(array,function(item,index){
		return item.station_train_code==m.station_train_code 
		&& item.passenger_name == m.passenger_name
		&& item.passenger_id_type_code == m.passenger_id_type_code; 
		},true); 
		} 
		return arr; 
	} */
	
	//var resignWoPuNum=0;//用来记录保存卧铺车票改签数量
	var WoPuSeats = ["3","4","5","6","A","C","F","L"];
	function finishedOrderDetailsFn(){		
		mor.ticket.viewControl.current_tab="queryOrderTab";
		if(mor.ticket.viewControl.queryFinishedOrderType == '0'){
			jq("#FinishedOrderDetailViewBackBtn .ui-btn-text").html("");
			jq("#FinishedOrderDetailViewBackBtn").removeClass("head-btn-long");
		} else {
			if(mor.ticket.viewControl.queryFinishedOrderType == '1') {
				jq("#FinishedOrderDetailViewBackBtn .ui-btn-text").html("");
				jq("#FinishedOrderDetailViewBackBtn").removeClass("head-btn-long");
			} else {
				jq("#FinishedOrderDetailViewBackBtn .ui-btn-text").html("");
				jq("#FinishedOrderDetailViewBackBtn").removeClass("head-btn-long");
			}
		}			
		var orderDetailList = mor.ticket.queryOrder.getCurrentFinishedOrdersMyTicketList();
		jq("#ticketSquenceNo").html(orderDetailList[0].sequence_no);
		jq("#ticketBookedDate").html(mor.ticket.util.getLocalDateString2(orderDetailList[0].reserve_time));
		jq("#finishedOrderDetails").html(generateOrdersDetailList(orderDetailList)).listview("refresh");	
		for(var i=0;i<orderDetailList.length;i++){
			if(orderDetailList[i].resign_flag=='4' && orderDetailList[i].return_flag == 'N'){
				jq("#finishedOrderDetails li").eq(i).addClass("ui-disabled");
			}
		}
		initContentHeight();
		jq("#FinishedOrderDetailView .iscroll-wrapper").iscrollview("refresh");
	}
	jq("#FinishedOrderDetailView").live("pagebeforeshow", function() {
		var user = mor.ticket.loginUser;		
		if(user.isAuthenticated === "Y"){
			finishedOrderDetailsFn();
		}else{
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
			} else {
				registerAutoLoginHandler(finishedOrderDetailsFn, autologinFailJump);
			}
		}
	});
	
	function initContentHeight(){
		//TODO .outerHeight
		var height = jq("#FinishedOrderDetailView .ui-content").height();
		height -= jq("#regulationInfo").height();
		jq("#FinishedOrderDetailView .ui-content").height(height);
	}
	
	function contentIscrollRefresh(){
		jq("#FinishedOrderDetailView .iscroll-wrapper").iscrollview("refresh");
	}
	
	function registerChangeTrainListener(){
		jq("#changeTicket").bind("tap",function(){
			var checkedList = jq(".changeTicketChkbox:checked");
			if(checkedList.length>0){
				for(var i=0;i<checkedList.length;i++){
					checkedList[i].checked = false;
				}
			}
			changeOrChangeStation = "change";
			jq(this).addClass("ui-btn-active ui-state-persist")
			.siblings().removeClass("ui-btn-active ui-state-persist");
			jq(".cancelTicketBtn").hide();
			jq("#changeTostationTicketBtn").hide();
			var ticketList = mor.ticket.queryOrder.getCurrentFinishedOrdersMyTicketList();
			ChangeTicketStatuse(ticketList,"change");
			contentIscrollRefresh();
			return false;
		});
		
		jq("#hidePopupScreen").bind("tap",function(){
			jq("#popupNested-screen").click();
			contentIscrollRefresh();
		});
	}
	
	function registerChangeTostationTrainListener(){
		jq("#changeTostationTicket").bind("tap",function(){
			var checkedList = jq(".changeTicketChkbox:checked");
			if(checkedList.length>0){
				for(var i=0;i<checkedList.length;i++){
					checkedList[i].checked = false;
				}
			}
			changeOrChangeStation = "changeStation";
			jq(this).addClass("ui-btn-active ui-state-persist")
			.siblings().removeClass("ui-btn-active ui-state-persist");
			jq(".cancelTicketBtn").hide();
			var ticketList = mor.ticket.queryOrder.getCurrentFinishedOrdersMyTicketList();
			ChangeTicketStatuse(ticketList,"changeTostationTicket");
			contentIscrollRefresh();
			return false;
		});
		
		jq("#hidePopupScreen").bind("tap",function(){
			jq("#popupNested-screen").click();
			contentIscrollRefresh();
		});
	}
	function ChangeTicketStatuse(ticketList,changeTostationOrChange){
		var num = 0;
		if(ticketList.length == 1){
			if(changeTostationOrChange === "change"){
				if(ticketList[0].resign_flag === "1" || ticketList[0].resign_flag === "2"){
					jq(".change").eq(0).show();
					num ++;
				}else{
					jq(".change").eq(0).hide();
				}
			}
			if(changeTostationOrChange === "changeTostationTicket"){
				if(ticketList[0].resign_flag === "1" || ticketList[0].resign_flag === "3"){
					jq(".change").eq(0).show();
					num ++;
				}else{
					jq(".change").eq(0).hide();
				}
			}
			
		}else{
			if(changeTostationOrChange === "change"){
				for(var i=0; i<ticketList.length; i++){
					if(ticketList[i].resign_flag === "1" || ticketList[i].resign_flag === "2"){
						jq(".change").eq(i).show();
						num ++;
					}else{
						jq(".change").eq(i).hide();
					}
				}	
			}
			if(changeTostationOrChange === "changeTostationTicket"){
				for(var i=0; i<ticketList.length; i++){
					if(ticketList[i].resign_flag === "1" || ticketList[i].resign_flag === "3"){
						jq(".change").eq(i).show();
						num ++;
					}else{
						jq(".change").eq(i).hide();
					}
				}	
			}
			
		}
		if(changeTostationOrChange === "change"){
			if(num){
				jq("#changeTicketBtn").html(getChangeTicketBtnText("change"));		
				jq("#changeTicketBtn").parent().css("visibility","visible");
				jq("#changeTicketBtn").show();
			}else{
				jq("#changeTicket").removeClass("ui-btn-active ui-state-persist");
				jq("#changeTicketBtn").hide();
			}
		}
		if(changeTostationOrChange === "changeTostationTicket"){
			if(num){
				jq("#changeTicketBtn").html(getChangeTicketBtnText("changeTostationTicket"));		
				jq("#changeTicketBtn").parent().css("visibility","visible");
				jq("#changeTicketBtn").show();
			}else{
				jq("#changeTostationTicket").removeClass("ui-btn-active ui-state-persist");
				jq("#changeTicketBtn").hide();
			}
		}
	}
	
	function registerCancelTrainListener(){
		jq("#cancleTicket").bind("tap",function(){
			jq(this).addClass("ui-btn-active ui-state-persist")
				.siblings().removeClass("ui-btn-active ui-state-persist");
			jq(".change").hide();
			var ticketList = mor.ticket.queryOrder.getCurrentFinishedOrdersMyTicketList();
			CancelTicketStatuse(ticketList);
			jq("#changeTicketBtn").parent().css("visibility","hidden");
			jq("#changeTicketBtn").html(getChangeTicketBtnText());
			contentIscrollRefresh();
			return false;
		});
	}
	
	function CancelTicketStatuse(ticketList){
		var num = 0;
		for(var i=0; i<ticketList.length; i++){
			if(ticketList[i].return_flag === "Y"){
				jq(".cancelTicketBtn").eq(i).show();
				num ++;
			}
		}
		if(!num){
			jq("#cancleTicket").removeClass("ui-btn-active ui-state-persist");
		}
	}
	
	function registerChangeTicketCheckedBoxListener(){
		jq("#finishedOrderDetails").on("change", ".changeTicketChkbox", function(e){
			e.stopImmediatePropagation();
			var checkedList = jq(".changeTicketChkbox:checked");
			if(checkedList.length > 1){
				var resignWoPuNum=0;//用来记录保存卧铺车票改签数量
				var batchNos = [];
				var ticketList = mor.ticket.queryOrder.getCurrentFinishedOrdersMyTicketList();	
				var currentChangeTicketOrder = ticketList[jq(this).parents('li').index()];
				//如果同时改签两张卧铺票，弹出提示信息不允许
				for(var i = 0;i<checkedList.length;i++){
					
					var seatTypeCode = ticketList[jq(checkedList[i]).parents('li').index()].seat_type_code;
					if(jq.inArray(seatTypeCode,WoPuSeats)!=-1){
						resignWoPuNum++;
					}
					batchNos.push(ticketList[jq(checkedList[i]).parents('li').index()].batch_no);
				}
				
				jq.unique(batchNos);
				if(batchNos.length!=1){
					if(changeOrChangeStation === "change"){
						mor.ticket.util.alertMessage("  您选择的车票【" + currentChangeTicketOrder.station_train_code + "车次，"+
							currentChangeTicketOrder.coach_name + "车，"+currentChangeTicketOrder.seat_name +"】不能和其他车票同时改签，" +
							"请不要勾选。请选择同一订单、同一日期、同一车次、相同发到站、相同席别的车票进行变更到站。");
					}
					if(changeOrChangeStation === "changeStation"){
						mor.ticket.util.alertMessage("  您选择的车票【" + currentChangeTicketOrder.station_train_code + "车次，"+
								currentChangeTicketOrder.coach_name + "车，"+currentChangeTicketOrder.seat_name +"】不能和其他车票同时变更到站，" +
								"请不要勾选。请选择同一订单、同一日期、同一车次、相同发到站、相同席别的车票进行变更到站。");
					}
					jq(this).removeAttr("checked");
					
				}
				//如果同时改签两张卧铺票，弹出提示信息不允许
				else if(resignWoPuNum>1){
					if(changeOrChangeStation === "change"){
						mor.ticket.util.alertMessage("  您选择的车票【" + currentChangeTicketOrder.station_train_code + "车次，"+
								currentChangeTicketOrder.coach_name + "车，"+currentChangeTicketOrder.seat_name +"】不能和其他卧铺车票同时改签，" +
								"请不要勾选。请一次改签一张卧铺车票。");
					}
					if(changeOrChangeStation === "changeStation"){
						mor.ticket.util.alertMessage("  您选择的车票【" + currentChangeTicketOrder.station_train_code + "车次，"+
								currentChangeTicketOrder.coach_name + "车，"+currentChangeTicketOrder.seat_name +"】不能和其他卧铺车票同时变更到站，" +
								"请不要勾选。请一次变更到站一张卧铺车票。");
					}
					jq(this).removeAttr("checked");
				}else{
					if(changeOrChangeStation === "changeStation"){
						jq("#changeTicketBtn").html(getChangeTicketBtnText("changeTostationTicket"));
						jq("#changeTicketBtn").show();
					}
					if(changeOrChangeStation === "change"){
						jq("#changeTicketBtn").html(getChangeTicketBtnText("change"));
						jq("#changeTicketBtn").show();
					}
					
				}
			}
			else{
				if(changeOrChangeStation === "changeStation"){
					jq("#changeTicketBtn").html(getChangeTicketBtnText("changeTostationTicket"));
					jq("#changeTicketBtn").show();
				}
				if(changeOrChangeStation === "change"){
					jq("#changeTicketBtn").html(getChangeTicketBtnText("change"));
					jq("#changeTicketBtn").show();
				}	
			}
			contentIscrollRefresh();
			return false;
		});
	}
	
	function registerChangeTicketBtnListener(){
		jq("#changeTicketBtn").bind("tap",function(){
			if(countCheckedBoxNum()) {
				if(changeOrChangeStation === "change"){
					mor.ticket.changeOrchangeStation.changeOrchangeStation = "改签";
				}
				if(changeOrChangeStation === "changeStation"){
					mor.ticket.changeOrchangeStation.changeOrchangeStation = "变更到站";
				}
				mor.ticket.viewControl.bookMode = "gc";
				mor.ticket.leftTicketQuery.isToday = false;
				mor.ticket.leftTicketQuery.isTwoday = false;
				var queryOrder = mor.ticket.queryOrder;
				var ticketList = queryOrder.getCurrentFinishedOrdersMyTicketList();
				queryOrder.initChangeTicketOrderList();
				for(var i=0;i<ticketList.length;i++){
					if (jq(".changeTicketChkbox:eq("+i+")").attr("checked") == "checked"){
						queryOrder.setChangeTicketOrderList(ticketList[i]);
					}
				}
				initChangeTicketInfo(queryOrder.changeTicketOrderList);
				var newTrain_date = mor.ticket.util.changeDateType(queryOrder.changeTicketOrderList[0].train_date);
				mor.ticket.leftTicketQuery.train_date = newTrain_date;
				var newRuleStartDate = window.ticketStorage.getItem("newRuleStartDate");
				if(newRuleStartDate <= newTrain_date){
					var today = mor.ticket.util.getNewDate();
					var  reserveDay= new Date(today).format("yyyy-MM-dd");
					var newRuleIntervalHours = window.ticketStorage.getItem("newRuleIntervalHours");
					var newDate = queryOrder.changeTicketOrderList[0].train_date+" "+queryOrder.changeTicketOrderList[0].start_time.substr(0,2)+":"+queryOrder.changeTicketOrderList[0].start_time.substr(2,2);
					if(reserveDay == newTrain_date){
						mor.ticket.leftTicketQuery.today_train_date = newTrain_date;
						mor.ticket.leftTicketQuery.isToday = true;
					}else if(mor.ticket.util.getSendTicketHoursTimeLimit(newDate,newRuleIntervalHours)){
							mor.ticket.leftTicketQuery.today_train_date = newTrain_date;
							var currDate = mor.ticket.util.setMyDate2(mor.ticket.util.getNewDate().format("yyyyMMdd"));
							var startDate = mor.ticket.util.setMyDate2(queryOrder.changeTicketOrderList[0].train_date);
							var nDays = parseInt(Math.abs(startDate - currDate)/1000/60/60/24);
							mor.ticket.leftTicketQuery.dayLimit = nDays;
							mor.ticket.leftTicketQuery.isTwoday = true;
					}else{
						mor.ticket.leftTicketQuery.train_date = newTrain_date;
					}
				}
				mor.ticket.util.changePage(vPathViewCallBack()+"MobileTicket.html");							
			}
			else {
				var str = "";
				if(changeOrChangeStation === "change"){
					str = "改签";
				}
				if(changeOrChangeStation === "changeStation"){
					str = "变更到站";
				}
				mor.ticket.util.alertMessage("请勾选需要"+str+"的车票！");
			}
			return false;
		});
	}
	
	function countCheckedBoxNum(){
		return jq(".changeTicketChkbox:checked").length;
	}
	
	function getChangeTicketBtnText(changeTostationOrChange){
		var changeTicketChkboxNum = countCheckedBoxNum();
		var str = "";
		if(changeTicketChkboxNum){
			if(changeTostationOrChange === "changeTostationTicket"){
				str = "变更到站 (" + changeTicketChkboxNum + ")";
			}
			if(changeTostationOrChange === "change"){
				str = "改签 (" + changeTicketChkboxNum + ")";
			}
		}else{
				if(changeTostationOrChange === "changeTostationTicket"){
					str = "变更到站";
				}
				if(changeTostationOrChange === "change"){
					str = "改签";
				}
		}		
		return str;
	}
	
	//48小时  改签/变更到站
	function ticketChangeTostation(){
		var queryOrder = mor.ticket.queryOrder;
		var ticketList = queryOrder.getCurrentFinishedOrdersMyTicketList();
		queryOrder.initChangeTicketOrderList();
		for(var i=0;i<ticketList.length;i++){
			if (jq(".changeTicketChkbox:eq("+i+")").attr("checked") == "checked"){
				queryOrder.setChangeTicketOrderList(ticketList[i]);
			}
		}
		initChangeTicketInfo(queryOrder.changeTicketOrderList);
		var trainDate = queryOrder.changeTicketOrderList[0].train_date+queryOrder.changeTicketOrderList[0].start_time+"00";;
		var endTime= mor.ticket.util.setMyDate3(trainDate);
		var nowTime = mor.ticket.util.getNewDate();
		var diffTime = endTime.getTime() - nowTime.getTime();
		if (diffTime > 48*60*60*1000){	
			return true;
		}else{
			return false;
		}
	}
	
	
	function initChangeTicketInfo(ticketInfoList){
		var typeFlag = new Array();
		var model = mor.ticket.leftTicketQuery;
		model.from_station_telecode = ticketInfoList[0].from_station_telecode;
		model.to_station_telecode = ticketInfoList[0].to_station_telecode;
		var date = mor.ticket.util.getNewDate();
		var setTrainDate = window.ticketStorage.getItem("set_train_date_type")==null ? "1":window.ticketStorage.getItem("set_train_date_type");
		var dateNew = new Date(date.setDate(date.getDate()+parseInt(setTrainDate,10)));
		model.train_date = dateNew.format("yyyy-MM-dd");
		model.time_period = 0;
		typeFlag = jq.grep(ticketInfoList,function(item, planIndex) {
			return item.ticket_type_code != "3";	
	});
		if(typeFlag.length >0){
			model.purpose_codes = "00";	
		}else{
			model.purpose_codes = "0X";	
		}
		/*var station_train_code = mor.ticket.dw_ticket.station_train_code;
		if(jq.inArray(ticketInfoList[0].station_train_code,station_train_code) != -1){
			mor.ticket.dw_ticket.dw_flag = true;
		}else{
			mor.ticket.dw_ticket.dw_flag = false;
		}*/
	}
	
	function registerCancelTicketBtnListener(){
		jq("#finishedOrderDetails").on("tap", ".cancelTicketBtn", function(){	
			mor.ticket.viewControl.cancelTicketIndex = jq(this).parents("li").index();
			requestCancleChangeTicket();
			
			/** 去掉对话框
			WL.SimpleDialog.show(
					"温馨提示", 
					"确定要退票吗？", 
					[ {text : '确定', handler: requestCancleChangeTicket},
					  {text : '取消', handler: function(){}}]
				);
				
			**/
		});
	}

	function requestCancleChangeTicket(){		
		var ticketList = mor.ticket.queryOrder.getCurrentFinishedOrdersMyTicketList();	
		var index = mor.ticket.viewControl.cancelTicketIndex;
		var currentTicketDetail = ticketList[index];
		var util = mor.ticket.util;
		var commonParameters = {			
			'sequence_no': currentTicketDetail.sequence_no,
			'batch_no':currentTicketDetail.batch_no,
			'coach_no':currentTicketDetail.coach_no,
			'seat_no':currentTicketDetail.seat_no
		};
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "refundTicketRequest"
		};
		
		var options =  {
				onSuccess: requestCancleTicketSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function  requestCancleTicketSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {		
			mor.ticket.queryOrder.setCancelTicketInfo(invocationResult);
			jq.mobile.changePage("cancelOrder.html");
		} else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}		
	}
	
	
	/*xuyi*/
	function registerSaveAndShareBtnClickHandler() {
		jq("#shareForFinishedOrderBtn,#saveImgForFinishedOrderBtn").off().on("tap",function(e){
			e.preventDefault();
			popupWindow.close();
			//jq( "div[data-role='popup']" ).popup( "close" );
			showImg();
			var thisBtn = jq(this).attr("id");
			var cloneContent = jq("#FinishedOrderDetailView").clone().insertAfter("#FinishedOrderDetailView");
			cloneContent.attr("style","padding-top: 1px !important ; padding-bottom: 0px !important ;");
			cloneContent.children("div[data-role='content']").first().replaceWith(jq("#forCopyFinishedOrderDetailView").clone().attr("style","margin-top: 30px !important ; min-height : "+jq("div[data-role='content']").height()+"px ;"));		
			cloneContent.children(".ui-popup-screen").remove();
			cloneContent.children(".ui-popup-container").remove();
			cloneContent.attr("id","iscopyFinishedOrderDetailView");
			cloneContent.find("#changeTicket").css("margin","0px");
			cloneContent.find("#changeTicket").css("font-size","16px");
			cloneContent.find("#cancleTicket").css("margin","0px");
			cloneContent.find("#cancleTicket").css("font-size","16px");
			cloneContent.find("#changeTostationTicket").css("margin","0px");
			cloneContent.find("#changeTostationTicket").css("font-size","16px");
			cloneContent.find("#moreOptionsTab .circle").css("border-radius","50px");
			cloneContent.find("#moreOptionsTab .circle").css("-webkit-border-radius","50px");
			cloneContent.children().removeClass("ui-header-fixed");
			cloneContent.children().removeClass("ui-footer-fixed");
			html2canvas(document.getElementById("iscopyFinishedOrderDetailView"), {
				allowTaint : true,
				taintTest : false,
				chinese : true,
				onrendered : function(canvas) {
					// 生成base64图片数据
					var dataUrl = canvas.toDataURL();
					if(thisBtn == "shareForFinishedOrderBtn"){
						cordova.exec(callShareSuccess, callFail,
								"SocialSharing", "share", [ dataUrl,"铁路12306订单_"+jq("#ticketSquenceNo").html() ]);
		//				cordova.exec(callShareSuccess, callFail,
		//						"SaveImgPlugin", "saveImg", [ dataUrl,jq("#ticketSquenceNo").html() ]);
					}else{
						cordova.exec(callScreenShotSuccess, callFail,
								"SaveImgPlugin", "saveImg", [ dataUrl,"铁路12306订单_"+jq("#ticketSquenceNo").html() ]);
					}
					cloneContent.remove();
					hideImg();
				}
			});
		});
	}
	
	function showImg(){
		jq(document).progressDialog.showDialog("请稍候...");
	}
	function hideImg(){
		jq(document).progressDialog.hideDialog();
	}
	function showLoader(){
		jq.mobile.loading('show',{
			text: '截图中...',
			textVisible: true,
			theme: 'd',
			textonly: false,
			html: ""
		});
	}
	
	function hideLoader(){
		jq.mobile.loading('hide');
	}
	
	function callShareSuccess(data) {
		// alert(data);
		/*WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);*/
		openNativePage(data,"Share");
	}
	
	function callScreenShotSuccess(data) {
		// alert(data);
		if (data == "IOS"){
			WL.SimpleDialog.show("温馨提示", "已成功保存至相册", [ {
				text : '确定',
				handler : function() {
				}
			} ]);	
		}else{
			openNativePage(data,"ScreenShot");
		}
	
	}

	function callFail(data) {
		// alert(data);
		WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);
	}

	function openNativePage(data,whatBtn) {
		var params = {
				imgPath : data
		};
		if(whatBtn === "Share"){
		WL.NativePage.show('com.MobileTicket.MobileTicketShare',
				backFromNativePage, params);
		}
		else if(whatBtn === "ScreenShot"){
			WL.NativePage.show('com.MobileTicket.ScreenShotSaveImg',
					backFromNativePage, params);
			}
		}


	function backFromNativePage(data) {
		WL.SimpleDialog.show("温馨提示", data, [ {
			text : '确定',
			handler : function() {
			}
		} ]);
	}
	
	
	var ordersDetailListTemplate =
		"{{~it :orderDetail:index}}" +
		"<li data-index='{{=index}}'>" +
			"<div class='ui-grid-a' >" +
				"<div class='ui-block-a' style='width:80px;'>{{=mor.ticket.util.changeDateType(orderDetail.train_date)}}</div>" +
				"<div class='ui-block-b' style='width:60px;'>{{=mor.ticket.util.formateTrainTime(orderDetail.start_time)}}开</div>" +
				"<div class='ui-block-c' style='width:145px;'>" +
					"<div class='ui-grid-b'>" +
						"<div class='ui-block-a'>{{=mor.ticket.cache.getStationNameByCode(orderDetail.from_station_telecode)}}</div>" +
						"<div class='ui-block-b trainDetailArrow'></div>" +
						"<div class='ui-block-c'>{{=mor.ticket.cache.getStationNameByCode(orderDetail.to_station_telecode)}}</div>" +
					"</div>" +
				"</div>" +
			"</div>" +
			"<div class='ui-grid-c'>" +
				"<div class='ui-block-a' style='width:80px;'>{{=orderDetail.station_train_code}}</div>" +
				"<div class='ui-block-b'>{{=mor.ticket.util.getSeatTypeName(orderDetail.seat_type_code,orderDetail.seat_no)}}</div>" +
				"<div class='ui-block-c'>{{=orderDetail.coach_name}}车</div>" +
				"<div class='ui-block-d'>{{=orderDetail.seat_name}}</div>" +
				"<div class='ui-block-a' style='width:80px;'>{{=orderDetail.passenger_name}}</div>" +
				"<div class='ui-block-b'>{{=orderDetail.passenger_id_type_name}}</div>" +
				"<div class='ui-block-c'>{{=mor.ticket.util.getTicketTypeName(orderDetail.ticket_type_code,orderDetail.seat_no)}}</div>" +
				"<div class='ui-block-d'>{{=orderDetail.ticket_price}}元</div>" +
			"</div>" +
			"<div class='ui-grid-a'>" +
				"<div class='ui-block-a' style='width:60%;'>车票状态 ：" +
				"{{ if(orderDetail.ticket_status_code =='c'){ }}" +//退票状态下判断
					"{{=orderDetail.ticket_status_name.substring(0,3)}}" +
				"{{ } else { }}" +
					"{{=orderDetail.ticket_status_name}}" +
				"{{ } }}" +
				"</div>" +
				"<div class='ui-block-b' style='text-align: right;width:40%;'>" +
					"<span class='change' style='display:none;'>" +
					"	<input class='changeTicketChkbox' type='checkbox'/><span></span></span>" +
					"<a data-role='button' class='cancelTicketBtn' style='display:none;'>退票</a>" +
				"</div>" +
				"{{ if(orderDetail.ticket_status_code =='c' && orderDetail.ticket_status_name.substring(3) != null ){ }}" +//退票状态下判断
					"<div class='ui-block-a' style='width:100%'>{{=orderDetail.ticket_status_name.substring(3)}}</div>" +
				"{{ } }}" +
			"</div>" +
		"</li>" +
		"{{~}}";
	var generateOrdersDetailList = doT.template(ordersDetailListTemplate);
})();
	