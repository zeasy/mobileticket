/**
 * @providesModule RTHttp
 * Created by easy on 16/3/19.
 */

'use strict'

import Network from 'ZZNetwork'

export default class RTHttp {

    static headers(headers) {
        return headers || {
            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36',
        }
    }

    static format(format) {
        return format || 'json';
    }

    static get(url,params,headers,format) {
        return Network.request({
            method : 'GET',
            url : url,
            params : params,
            headers : this.headers(headers),
            format : this.format(format),
        }).then(result=> {
            if ('json' == this.format(format).toLowerCase()) {
                return Promise.resolve(JSON.parse(result));
            }
            return Promise.resolve(result);
        });
    }

    static post(url,params,headers,format) {
        return Network.request({
            method : 'POST',
            url : url,
            params : params,
            headers : this.headers(headers),
            format : this.format(format),
        }).then(result=> {
            if (!format || 'json' == format.toLowerCase()) {
                return Promise.resolve(JSON.parse(result));
            }
            return Promise.resolve(result);
        });
    }
}