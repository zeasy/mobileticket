
/* JavaScript content from js/controller/finishedOrderList.js in folder common */
(function() {		
	var page_no = 0; 
	var showMore =false;
	jq("#FinishedOrderListView").live("pageinit", function() {
		registerFinishedOrderListItemClickHandler();
		jq("#orderList_content", this).bind( { 
	        "iscroll_onpulldown" : onPullDown,    
	        "iscroll_onpullup"   : onPullUp});
		jq("#finishedOrderListViewBackBtn").bind("tap",function(){
			if(mor.ticket.viewControl.queryFinishedOrderType == '2'||mor.ticket.viewControl.queryFinishedOrderType == '3'||
					mor.ticket.viewControl.queryFinishedOrderType == '4'){
				mor.ticket.util.changePage("advanceQueryOrder.html");
//				mor.queryOrder.views.advanceQuery.fromDate = "";
//				mor.queryOrder.views.advanceQuery.toDate = "";
			} 
			else{
				mor.ticket.util.changePage("queryOrder.html");
			}
			return false;
		});			
	});
	

	//往下拉是重新刷新
	function onPullDown(event, data){
		setTimeout(function fakeRetrieveDataTimeout() { 
		      gotPullDownData(event, data); }, 
		      1500); 
	};
	
	function  gotPullDownData(event, data){
		page_no = 0;
		showMore =false;
		if(mor.ticket.viewControl.queryFinishedOrderType == '0'){
			//今日已完成订单
			initQueryTodayOrder();
		} else {
			initQueryOrder();
		}	
	  data.iscrollview.refresh();
	}
	//往上拉是加载更多
	function onPullUp(){
		++page_no;
		showMore =true;
		if(mor.ticket.viewControl.queryFinishedOrderType == '0'){
			//今日已完成订单
			initQueryTodayOrder();
		} else {
			initQueryOrder();
		}		
	};
	
	function finishedOrderListFn(e, data){
		var user = mor.ticket.loginUser;
		page_no = 0;
		showMore = false;
		var prePage;
		if(mor.ticket.viewControl.queryFinishedOrderType == '0'){//今日订单查询
			//如果从finishedOrderDetails页面返回，那么不重新查询订单
			prePage = data.prevPage.attr("id");
			if(prePage !="FinishedOrderDetailView"){
				initQueryTodayOrder();
			}else{
				var orders = mor.ticket.queryOrder.finishedOrderList;	
				if(orders){//查询订单成功
					if(orders.length){			
						//判断是否有改签的车票，若有，则删除
						mor.ticket.queryOrder.spliceChangingTicketList();
						//对orders数组按照时间进行排序
						var sortedOrders = sortOrdersByDate(orders);
						jq("#orderLength").html(sortedOrders.length);
						jq("#orderPrompt").show();
						nameNoChoose(sortedOrders);
						jq("#finishedOrderList").html(generateFinishedOrdersList(sortedOrders)).listview("refresh");
						jq("#orderList-pullup").show();
						jq("#iscroll-pulldown").show();
					}else {//订单为空
						mor.ticket.util.alertMessage("没有已完成订单。");
						jq("#orderList-pullup").hide();
					}			
				} else {//查询订单失败
					mor.ticket.util.alertMessage(invocationResult.error_msg);
				}	
			}
			jq("#finishedOrderListHeader").html("今日已完成订单");
			jq("#finishedOrderListViewBackBtn .ui-btn-text").html("");
		} else {
			
			//如果从finishedOrderDetails页面返回，那么不重新查询订单
			prePage = data.prevPage.attr("id");
			if(prePage !="FinishedOrderDetailView"){
				initQueryOrder();
			}else{
				var orders = mor.ticket.queryOrder.finishedOrderList;	
				if(orders){//查询订单成功
					if(orders.length){			
						//判断是否有改签的车票，若有，则删除
						mor.ticket.queryOrder.spliceChangingTicketList();
						//对orders数组按照时间进行排序
						var sortedOrders = sortOrdersByDate(orders);
						jq("#orderLength").html(sortedOrders.length);
						jq("#orderPrompt").show();
						nameNoChoose(sortedOrders);
						jq("#finishedOrderList").html(generateFinishedOrdersList(sortedOrders)).listview("refresh");
						jq("#orderList-pullup").show();
						jq("#iscroll-pulldown").show();
					}else {//订单为空
						if(mor.ticket.viewControl.queryFinishedOrderType == '3'){
							mor.ticket.util.alertMessage("没有未出行订单。");	
						}else if (mor.ticket.viewControl.queryFinishedOrderType == '4'){
							mor.ticket.util.alertMessage("没有历史订单。");
						}else{
							mor.ticket.util.alertMessage("没有已完成订单。");
						}
						jq("#orderList-pullup").hide();
					}			
				} else {//查询订单失败
					mor.ticket.util.alertMessage(invocationResult.error_msg);
				}	
			}
			if(mor.ticket.viewControl.queryFinishedOrderType == '1') {//7日内订单查询
				jq("#finishedOrderListHeader").html("7日内已完成订单");
				jq("#finishedOrderListViewBackBtn .ui-btn-text").html("");
			} else {//高级查询
				if(mor.ticket.viewControl.queryFinishedOrderType == '3'){
					jq("#finishedOrderListHeader").html("未出行订单");
					jq("#finishedOrderListViewBackBtn .ui-btn-text").html("");
				}else if(mor.ticket.viewControl.queryFinishedOrderType == '4'){
					jq("#finishedOrderListHeader").html("历史订单");
					jq("#finishedOrderListViewBackBtn .ui-btn-text").html("");
				}
				
			}
		}			

	}
	jq("#FinishedOrderListView").live("pageshow", function(e, data) {
		//jq("FinishedOrderListView .iscroll-pulldown").hide();
		mor.ticket.util.initAppVersionInfo();
		var user = mor.ticket.loginUser;
		
		

		if(user.isAuthenticated === "Y"){
			finishedOrderListFn(e, data);
			jq("#orderSelectTip").fadeOut(3000);
		}else{
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
				} else {
				registerAutoLoginHandler(function(){finishedOrderListFn(e, data)}, autologinFailJump);
				}

			//mor.ticket.util.changePage(vPathCallBack()+"loginTicket.html");
		}
	});
	
	function initQueryTodayOrder(){//今天已完成订单
		var util = mor.ticket.util;
		var today = mor.ticket.util.getNewDate();
		var todayStr = today.format("yyyyMMdd");
		var commonParameters = {
			'from_reserve_date':todayStr,
			'to_reserve_date':todayStr,
			'page_no':page_no.toString(),
			'rows_number':'8',
			'query_class': '2'
		};
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryOrder"
		};
		
		var options =  {
				onSuccess: showMore?requestAddMoreSucceeded:requestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
	}
	
	function prepareQueryOrderPrompt(){
		var jsonPrompt;
		var util = mor.ticket.util;
		if(mor.ticket.viewControl.queryFinishedOrderType == '1'){
			//七日内订单
			var today = mor.ticket.util.getNewDate();
			var todayStr = today.format("yyyyMMdd");
			var sevenDayStr = new Date(today.getTime() - 6*24*60*60*1000).format("yyyyMMdd");
			jsonPrompt = {
				'from_reserve_date':sevenDayStr,
				'to_reserve_date':todayStr,
				'page_no':page_no.toString(),
				'rows_number':'8',
				'query_class': '2' //全部已完成订单
			};
		}else if(mor.ticket.viewControl.queryFinishedOrderType == "4"){
			jsonPrompt = {
					'from_train_date':util.processDateCode(mor.queryOrder.views.advanceQuery.fromDate),
					'to_train_date':util.processDateCode(mor.queryOrder.views.advanceQuery.toDate),
					'train_code':mor.queryOrder.views.advanceQuery.trainCode,
					'passenger_name':mor.queryOrder.views.advanceQuery.passengerName,
					'sequence_no':mor.queryOrder.views.advanceQuery.sequenceNo,
					'page_no':page_no.toString(),
					'rows_number':'8',
					'query_class': '2'//全部已完成订单
			};
		}else {	
			//高级查询    车次日期查询
			if(mor.queryOrder.views.advanceQuery.isSelectTrainDate){
				jsonPrompt = {
						'from_train_date':util.processDateCode(mor.queryOrder.views.advanceQuery.trainFromDate),
						'to_train_date':util.processDateCode(mor.queryOrder.views.advanceQuery.trainToDate),
						'train_code':mor.queryOrder.views.advanceQuery.trainCode,
						'passenger_name':mor.queryOrder.views.advanceQuery.passengerName,
						'sequence_no':mor.queryOrder.views.advanceQuery.sequenceNo,
						'page_no':page_no.toString(),
						'rows_number':'8',
						'query_class': '2'//全部已完成订单
				};
			} else {// 高级查询    订单日期查询				
				jsonPrompt = {
						'from_reserve_date':util.processDateCode(mor.queryOrder.views.advanceQuery.fromDate),
						'to_reserve_date':util.processDateCode(mor.queryOrder.views.advanceQuery.toDate),
						'train_code':mor.queryOrder.views.advanceQuery.trainCode,
						'passenger_name':mor.queryOrder.views.advanceQuery.passengerName,
						'sequence_no':mor.queryOrder.views.advanceQuery.sequenceNo,
						'page_no':page_no.toString(),
						'rows_number':'8',
						'query_class': '2'//全部已完成订单
				};
			}
		}
		return jsonPrompt;
	}
	
	function initQueryOrder(){
		var util = mor.ticket.util;
		var commonParameters = prepareQueryOrderPrompt();
		var invocationData = {};
		if(mor.ticket.viewControl.queryFinishedOrderType == '4'){
			invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "queryHistoryOrder"
			};
		}else{
			invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryOrderTwo"
			};
		}
		
		
		var options =  {
				onSuccess: showMore?requestAddMoreSucceeded:requestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
	}
	
	function requestSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)){
			var orders = mor.ticket.queryOrder.setFinishedOrderList(invocationResult.orderList);
			if(orders){//查询订单成功
				if(orders.length){			
					//判断是否有改签的车票，若有，则删除
					mor.ticket.queryOrder.spliceChangingTicketList();
					//对orders数组按照时间进行排序
					var sortedOrders = sortOrdersByDate(orders);
					jq("#orderLength").html(sortedOrders.length);
					jq("#orderPrompt").show();
					nameNoChoose(sortedOrders);
					jq("#finishedOrderList").html(generateFinishedOrdersList(sortedOrders)).listview("refresh");
					jq("#orderList-pullup").show();
					jq("#iscroll-pulldown").show();
					jq("#orderSelectTip").fadeOut(3000);
				}else {//订单为空
					if(mor.ticket.viewControl.queryFinishedOrderType == '3'){
						mor.ticket.util.alertMessage("没有未出行订单。");	
					}else if (mor.ticket.viewControl.queryFinishedOrderType == '4'){
						mor.ticket.util.alertMessage("没有历史订单。");
					}else{
						mor.ticket.util.alertMessage("没有已完成订单。");
					}
					
					jq("#orderList-pullup").hide();
				}			
			} else {//查询订单失败
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}	
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	//对订单按照时间进行排序
	function sortOrdersByDate(orders){
		var sortedOrders = orders;
		var by = function(name)
		{
			return function(o, p)
			{
				var a, b;
				if (typeof o === "object" && typeof p === "object" && o && p) 
				{
					a = o[name];
					b = p[name];
					if (a === b) {return 0;}
					if (typeof a === typeof b) { return a < b ? 1 : -1;}
					return typeof a < typeof b ? 1 : -1;
				}
				else {throw ("error"); }
			};
		};

		orders.sort(by("order_date"));
		return orders;
	}
	
	function  nameNoChoose(sortedOrders){
		for(var i =0;i<sortedOrders.length;i++){ 
			    var nameFlag = 0;
				for(var j =0;j<sortedOrders[i].myTicketList.length;j++){
					if(sortedOrders[i].myTicketList[0].station_train_code == sortedOrders[i].myTicketList[j].station_train_code && 
							sortedOrders[i].myTicketList[0].ticket_status_code == sortedOrders[i].myTicketList[j].ticket_status_code){
						nameFlag++;
					}
				}
				sortedOrders[i].nameFlag  = nameFlag;
			}
	}
	
	function requestAddMoreSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)){
			var orders = invocationResult.orderList;
			if(orders){//查询订单成功
				if(orders.length){			
					//判断是否有改签的车票，若有，则删除
					orders = mor.ticket.queryOrder.pushFinishedOrderList(invocationResult.orderList);
					mor.ticket.queryOrder.spliceChangingTicketList();
					//对orders数组按照时间进行排序
					var sortedOrders = sortOrdersByDate(orders);
					jq("#orderLength").html(sortedOrders.length);
					jq("#orderPrompt").show();
					nameNoChoose(sortedOrders);
					jq("#finishedOrderList").html(generateFinishedOrdersList(sortedOrders));
					//jq("#orderList-pullup").show();
					jq("#orderSelectTip").fadeOut(3000);
				}else {//订单为空
					mor.ticket.util.alertMessage("已经是最后一页了。");
					jq("#orderList-pullup").hide();
				}
				jq("#FinishedOrderListView .ui-content").iscrollview("refresh");
			} else {//查询订单失败
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}	
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	var finishedOrdersListTemplate =
		"{{~it :order:index}}" +
		"<li data-index='{{=index}}'><a class='finishedOrderBlock ui-btn ui-btn-icon-right ui-icon-carat-r'>" +
			"<div class='ui-grid-a'>" +
				"<div class='ui-block-a'>订&nbsp;单&nbsp;号&nbsp;&nbsp;：<span>{{=order.sequence_no}}&nbsp;&nbsp;</span></div>" +
				"<div class='ui-block-b' style='text-align: center;'>"+
				"{{ if(order.nameFlag > 1){ }}" +
				"<span style='color: black;'>{{=order.myTicketList[0].passenger_name}}&nbsp;等{{=order.nameFlag}}人</span>" +
			    "{{ } else { }}" +
				"<span style='color: black;'>{{=order.myTicketList[0].passenger_name}}&nbsp;</span>" +
			    "{{ } }}" +
			    "</div>" +
				"<div class='ui-block-a'>" +
				//"车次信息：<span>{{=mor.ticket.util.getLocalDateString2(order.myTicketList[0].train_date).substr(5,8)}}&nbsp;{{=mor.ticket.cache.getStationNameByCode(order.myTicketList[0].from_station_telecode)}}{{=mor.ticket.util.formateTrainTime(order.myTicketList[0].start_time)}}—{{=mor.ticket.cache.getStationNameByCode(order.myTicketList[0].to_station_telecode)}}{{=mor.ticket.util.formateTrainTime(order.myTicketList[0].arrive_time)}}</span>" +
				"车次信息：<span>{{=mor.ticket.util.getLocalDateString2(order.myTicketList[0].train_date).substr(5,8)}}&nbsp;{{=mor.ticket.util.formateTrainTime(order.myTicketList[0].start_time)}}开&nbsp;{{=mor.ticket.cache.getStationNameByCode(order.myTicketList[0].from_station_telecode)}}—{{=mor.ticket.cache.getStationNameByCode(order.myTicketList[0].to_station_telecode)}}</span>" +
				"</div>" +
				"<div class='ui-block-a' style='width: 73%;'>订单时间：<span style='color:#000;'>{{=mor.ticket.util.getLocalDateString3(order.myTicketList[0].reserve_time)}}</span></div>" +
				"<div class='ui-block-b' style='width: 27%;'>总张数：<span>{{=order.myTicketList.length}}</span></div>" +
			"</div></a>" +
		"</li>" +
		"{{~}}";
	var generateFinishedOrdersList = doT.template(finishedOrdersListTemplate);
	
	function registerFinishedOrderListItemClickHandler(){
		jq("#finishedOrderList").off().on("tap", "li", function(e){
			e.stopImmediatePropagation();
			mor.ticket.queryOrder.setCurrentFinishedOrders(jq(this).index());
			mor.ticket.util.changePage("finishedOrderDetail.html");			
			return false;
		});
	}
})();