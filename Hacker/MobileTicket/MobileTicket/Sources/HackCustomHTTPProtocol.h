//
//  HackCustomHTTPProtocol.h
//  
//
//  Created by EASY on 15/10/30.
//
//

#import <Foundation/Foundation.h>

@interface HackCustomHTTPProtocol : NSURLProtocol <NSURLConnectionDelegate,NSURLConnectionDataDelegate,NSURLConnectionDownloadDelegate>

+(instancetype) sharedInstance;

-(void) __initWithRequest:(NSURLRequest *)request cachedResponse:(NSCachedURLResponse *)cachedResponse client:(id<NSURLProtocolClient>)client ;
-(void) didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *) challenge;
-(void) resolveAuthenticationChallenge:(NSURLAuthenticationChallenge *) challenge withCredential:(NSURLCredential *) c;


-(NSURLRequest *) connection:(NSURLConnection *) connection willSendRequest:(NSURLRequest *) request redirectResponse:(NSURLResponse *) response;
-(void) connection:(NSURLConnection *) conn didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *) c;
-(void) connection:(NSURLConnection *) conn didReceiveResponse:(NSURLResponse *) response;
-(void)  connection:(NSURLConnection *) conn didReceiveData:(NSData *) data;
@end
