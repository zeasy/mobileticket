
/* JavaScript content from js/controller/regist.js in folder common */
(function() {
	var prevPage;
	var focusArray=[];
	jq.extendModule("mor.ticket.registUserInfo", {
		userInfo : {}
	});
	jq("#registView").live("pageshow", function() {
		focusArray=[];
		jq("#registView .ui-content").iscrollview("refresh");
		if(busy.isVisible()){
			busy.hide();
		}
	});
	
	function registerAutoScroll(){	
		var util = mor.ticket.util;
		//基本信息
		util.enableAutoScroll('#regist_user_name',focusArray);
		util.enableAutoScroll('#regist_passwd',focusArray);
		util.enableAutoScroll('#regist_confirmPasswd',focusArray);
		//详细信息
		util.enableAutoScroll('#regist_name',focusArray);
		util.enableAutoScroll('#regist_idNo',focusArray);
		//联系方式
		util.enableAutoScroll('#regist_mobile',focusArray);
		util.enableAutoScroll('#regist_email',focusArray);
		//附加信息
		util.enableAutoScroll('#regist_studentDepartment',focusArray);
		util.enableAutoScroll('#regist_studentClass',focusArray);
		util.enableAutoScroll('#regist_studentNo',focusArray);
		util.enableAutoScroll('#regist_passCode',focusArray);
		if(util.isIPhone()){
			//详细信息
			util.enableAutoScroll('#regist_sex',focusArray);
			util.enableAutoScroll('#regist_idType',focusArray);
			//附加信息
			util.enableAutoScroll('#regist_userType',focusArray);
			util.enableAutoScroll('#regist_studentSystem',focusArray);
			util.enableAutoScroll('#regist_studentEnterYear',focusArray);
		}
	}
	
	jq("#registView").live("pageinit", function() {
		registerAutoScroll();
		//init country id value
		
		var countryMap = mor.cache.countryMap;
		if(countryMap){
			var countryList = mor.ticket.cache.country;
			for(var i = 0; i < countryList.length; i++){
				countryMap[countryList[i].id] = countryList[i].value;
			};
		}

		var util = mor.ticket.util;
		var checkFormUtil = mor.ticket.checkForm;
		jq("#regist_country").bind("tap",function(){
			mor.ticket.util.changePage("selectCountry.html");
			return false;
		});
		
		jq("#regist_userType").bind("change",changeEvent);
		
		jq("#regist_studentProvince").bind("tap",function(){
			mor.ticket.util.changePage("selectProvince.html");
			return false;
		});
		
		jq("#regist_studentSchool").bind("tap",function(){
			if(mor.ticket.util.isNoValue(jq("#regist_studentProvince").val())){
				util.alertMessage("请选择学校省份");
			}else{
				mor.ticket.util.changePage("selectUniversity.html");
			}
			return false;
		});
		
		jq("#regist_preferenceFromStation").bind("tap",function(){
			mor.ticket.viewControl.isCityGo = true;
			mor.ticket.util.changePage("selectCity.html");
			return false;
		});

		jq("#regist_preferenceToStation").bind("tap",function(){
			mor.ticket.util.changePage("selectCity.html");
			return false;
		});
		
		jq("#registBtn").bind("tap",submit);
		
		jq("#regist_user_name").change(function () { 
			
			if(util.isNoValue(jq("#regist_user_name").val())){
				util.alertMessage("请填写用户名");
				return;
			}
			
			if(!checkFormUtil.validateUsersName(jq("#regist_user_name").val())){
				util.alertMessage("用户名只能填写字母数字下划线,开头必须为字母,且长度必须在6-30位内!");
				return;
			}
			
			if(jq("#regist_user_name").val().length>29||jq("#regist_user_name").val().length<6){
				util.alertMessage("用户名的长度必须在6-30位内!");
				return;
			}
		} );
		
		jq("#regist_passwd").change(function () { 
			if(util.isNoValue(jq("#regist_passwd").val())){
				util.alertMessage("请填写密码");
				return;
			}
			
			if(jq("#regist_passwd").val().length<6){
				util.alertMessage("密码的长度必须大于6位!");
				return;
			}
		} );
		
		jq("#regist_confirmPasswd").change(function () { 
			if(util.isNoValue(jq("#regist_confirmPasswd").val())){
				util.alertMessage("请输入确认密码");
				return;
			}
			
			if(jq("#regist_passwd").val()!=jq("#regist_confirmPasswd").val()){
				util.alertMessage("两次输入的密码不一致!");
				return;
			}
		} );
		
		jq("#regist_idType").change(function(){
			if(jq("#regist_idType").val()==="1" || jq("#regist_idType").val()==="2" || jq("#regist_idType").val()==="C" || jq("#regist_idType").val()==="G"){
				jq("#regist_country").val("中国CHINA");
				jq("#regist_country_code").val("CN");
			}
			if(jq("#regist_idType").val()==="1"){
				jq("#registSexDiv").css("display","none");
				jq("#registBornDateShowDiv").css("display","none");
			}else{
				jq("#registSexDiv").css("display","block");
				jq("#registBornDateShowDiv").css("display","block");
			}
			if(jq("#regist_idType").val() === "B"){
				jq("#registCountryDiv").css("display","block");
			}else{
				jq("#registCountryDiv").css("display","none");
			}
		});
		
		jq("#regist_name").change(function () { 
			if(util.isNoValue(jq("#regist_name").val())){
				util.alertMessage("请填写姓名");
				return;
			}
			if(!checkFormUtil.checkChar(jq("#regist_name").val())){
				util.alertMessage("姓名只能包含中文或者英文，如有生僻字或繁体字参见12306姓名填写规则进行填写！");
				return;
			}
		} );
		

		jq("#regist_idNo").change(function () { 
			if(util.isNoValue(jq("#regist_idNo").val())){
				util.alertMessage("请输入证件号码");
				return;
			}
			if(!checkFormUtil.checkIdValidStr(jq("#regist_idNo").val())){
				util.alertMessage("输入的证件编号中包含中文信息或特殊字符!");
				return;
			}
		} );
		
		jq("#regist_mobile").change(function () { 
			if(util.isNoValue(jq("#regist_mobile").val())){
				util.alertMessage("请输入手机号码");
				return;
			}
			if(!checkFormUtil.isMobile(jq("#regist_mobile").val())){
				util.alertMessage("请输入正确的手机号码!");
				return;
			}
		} );
		jq("#regist_studentDepartment").change(function () { 
			if(!util.isNoValue(jq("#regist_studentDepartment").val())&&!checkFormUtil.checkNameChar(jq("#regist_studentDepartment").val())){
				util.alertMessage("填写的院系只能包含中文、英文、数字！");
				return;
			}
		} );
		
		jq("#regist_studentClass").change(function () { 
			if(!util.isNoValue(jq("#regist_studentClass").val())&&!checkFormUtil.checkNameChar(jq("#regist_studentClass").val())){
				util.alertMessage("填写的班级只能包含中文、英文、数字！");
				return;
			}
		} );
		
		jq("#regist_studentNo").change(function () { 
			if(util.isNoValue(jq("#regist_studentNo").val())){
				util.alertMessage("请输入学号");
				return;
			}
			
			if(!checkFormUtil.checkNameChar(jq("#regist_studentNo").val())){
				util.alertMessage("填写的学号只能包含中文、英文、数字!");
				return;
			}
		} );
		
		jq("#regist_preferenceCardNo").change(function () { 
			if(!util.isNoValue(jq("#regist_preferenceCardNo").val())&&!checkFormUtil.checkNameChar(jq("#regist_preferenceCardNo").val())){
				util.alertMessage("填写的优惠卡只能包含中文、英文、数字!");
				return;
			}
		} );
		initSelects();
		registerToDateInputChangeListener();

	});
	
	jq("#registView").live("pagebeforeshow", function(e, data) {
		mor.ticket.history.inputyzm='';
		mor.ticket.viewControl.tab4_cur_page="regist.html";
		prevPage = data.prevPage.attr("id");
		jq("#registBackBtn").bind("tap",function(){
			if(prevPage=="moreOptionView"){
				mor.ticket.util.changePage("moreOption.html");
			}else{
				mor.ticket.util.changePage(vPathCallBack()+"loginTicket.html");
			}
			return false;
		});
	
		if(jq("#regist_country_code").val()){
			jq("#regist_country").val(mor.ticket.util.getCountryByCode(jq("#regist_country_code").val()));
		}else{
			jq("#regist_country").val(mor.ticket.util.getCountryByCode("CN"));
			jq("#regist_country_code").val("CN");
		}
		if(jq("#regist_bornDateShow").val()){
			if(mor.ticket.currentPagePath.fromPath == "selectCountry.html" || mor.ticket.currentPagePath.fromPath == "selectProvince.html" ||
				mor.ticket.currentPagePath.fromPath == "selectUniversity.html" || mor.ticket.currentPagePath.fromPath == "selectCity.html" || 
				mor.ticket.currentPagePath.fromPath == "enterRegist.html"){
			}else{
				jq("#regist_bornDateShow").val(jq("#birthdayPickerInputHidden").val());
			}
		}else{
			jq("#regist_bornDateShow").val("1900-01-01");
		}
		jq("#regist_studentEnterYear").val(mor.ticket.util.getNewDate().getFullYear());
	});

	function initSelects(){	
		jq('#regist_bornDate').scroller({
	        preset: 'date',
	        theme: 'ios',
	        yearText:'年',
	        monthText:'月',
	        dayText:'日',
	        setText:'确定',
	        cancelText:'取消',
	        display: 'modal',
	        mode: 'scroller',
	        dateOrder: 'yy mm dd',
	        dateFormat: 'yy-mm-dd',
	        height:40,
	        showLabel:true
		});

		var jq_studentEnterYear = jq("#regist_studentEnterYear");	
		jq_studentEnterYear.empty();
		var currYear = mor.ticket.util.getNewDate().getFullYear();
		var htmlStr = "<option value='"+currYear+"' selected>"+currYear+"</option>";		
		currYear --;
		jq_studentEnterYear.append(htmlStr);
		for(var i=0; i<8; i++){	
			var htmlStr = "<option value='"+currYear+"'>"+currYear+"</option>";			
			jq_studentEnterYear.append(htmlStr);
			currYear --;
		}
		jq("#regist_studentEnterYear").selectmenu('refresh', true);
	}
	
	
	function changeQuestionEvent(){
		if(jq("#regist_pwd_question").val()=="1"){
			jq("#regist_otherQuestionOption").show();
		}else{
			jq("#regist_otherQuestionOption").hide();
		}
		contentIscrollRefresh();
	}
	
	function changeEvent(){
		if(jq("#regist_userType").val()=="3"){
			mor.ticket.util.initJSONStoreFromLocal(null,null);
			jq("#regist_userType").parent().parent().parent().removeClass("departshortnoline");
			jq("#regist_userType").parent().parent().parent().addClass("departshortline");
			jq("#regist_schoolOptions").show();
		}else{
			jq("#regist_schoolOptions").hide();
		}
		contentIscrollRefresh();
	}
	
	function refreshPassCode(){
		var util = mor.ticket.util;		
		var commonParameters = util.prepareRequestCommonParameters({
			'baseDTO.user_name': ''
		});
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "postPassCode",
				parameters: [commonParameters]	
		};
		var options = {
				onSuccess: requestPassCodeSucceeded,
				onFailure: mor.ticket.util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(null, invocationData, options);
		return false;
	}
	
	
	function requestPassCodeSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (invocationResult.isSuccessful &&
				invocationResult.succ_flag === "1") {
			jq("#regist_passCodeImg").attr("src", "data:image/gif;base64," + invocationResult.passcode);
		} else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	function submit(){
		
		var util = mor.ticket.util;
		var checkFormUtil = mor.ticket.checkForm;
		if(util.isNoValue(jq("#regist_user_name").val())){
			util.alertMessage("请填用户名");
			return;
		}
		
		if(!checkFormUtil.validateUsersName(jq("#regist_user_name").val())){
			util.alertMessage("用户名只能填写字母数字下划线,开头必须为字母,且长度必须在6-30位内!");
			return;
		}
		
		if(jq("#regist_user_name").val().length>29||jq("#regist_user_name").val().length<6){
			util.alertMessage("用户名的长度必须在6-30位内！");
			return;
		}
		
		if(util.isNoValue(jq("#regist_passwd").val())){
			util.alertMessage("请填写密码");
			return;
		}
		
		if(jq("#regist_passwd").val().length<6){
			util.alertMessage("密码的长度必须大于6位！");
			return;
		}
		
		if(util.isNoValue(jq("#regist_confirmPasswd").val())){
			util.alertMessage("请输入确认密码");
			return;
		}
		
		if(jq("#regist_passwd").val()!=jq("#regist_confirmPasswd").val()){
			util.alertMessage("确认密码与密码不一致!");
			return;
		}
		
		if(util.isNoValue(jq("#regist_name").val())){
			util.alertMessage("请填写姓名");
			return;
		}
		
		if(jq("#regist_idType").val()=="B" ||  jq("#regist_idType").val()=="H"){
			if(!checkFormUtil.checkBHChar(jq("#regist_name").val())){
				util.alertMessage("姓名只能包含中文或者英文，如有生僻字或繁体字参见12306姓名填写规则进行填写！");
				return;
			}
		}else{
			if(!checkFormUtil.checkChar(jq("#regist_name").val())){
				util.alertMessage("姓名只能包含中文或者英文，如有生僻字或繁体字参见12306姓名填写规则进行填写！");
				return;
			}
		}
		
		
		
		if(util.isNoValue(jq("#regist_sex").val())){
			util.alertMessage("请选择性别");
			return;
		}
		
		if(util.isNoValue(jq("#regist_country").val())){
			util.alertMessage("请选择国家地区");
			return;
		}
		
		if(util.isNoValue(jq("#regist_idType").val())){
			util.alertMessage("请选择证件类型");
			return;
		}
		
		if(util.isNoValue(jq("#regist_idNo").val())){
			util.alertMessage("请输入证件号码");
			return;
		}
		
		if(!checkFormUtil.checkIdValidStr(jq("#regist_idNo").val())){
			util.alertMessage("输入的证件编号中包含中文信息或特殊字符！");
			return;
		}
		// 1 2 B C G H身份证验证
		if(jq("#regist_idType").val()=="1"){
			if(!checkFormUtil.validateSecIdCard(jq("#regist_idNo").val())){
				util.alertMessage("请正确输入18位的身份证号！");
				return;
			}
		}
		
		if(jq("#regist_idType").val()=="B"){
			if(!checkFormUtil.checkPassport(jq("#regist_idNo").val())){
				util.alertMessage("请输入有效的护照号码！");
				return;
			}
		}
		
		if(jq("#regist_idType").val()=="C"){
			if(!checkFormUtil.checkHkongMacao(jq("#regist_idNo").val())){
				util.alertMessage("请输入有效的港澳居民通行证号码！");
				return;
			}
		}
		
		if(jq("#regist_idType").val()=="G"){
			if(!checkFormUtil.checkTaiw(jq("#regist_idNo").val())){
				util.alertMessage("请输入有效的台湾居民通行证号码！");
				return;
			}
		}
		
		if(jq("#regist_idType").val()=="H"){
			if(!checkFormUtil.checkPassport(jq("#regist_idNo").val())){
				util.alertMessage("请输入有效的外国人居留证号码！");
				return;
			}
		}
		
		if(util.isNoValue(jq("#regist_mobile").val())){
			util.alertMessage("请输入手机号码");
			return;
		}
		
		if(!checkFormUtil.isMobile(jq("#regist_mobile").val())){
			util.alertMessage("请输入正确的手机号码！");
			return;
		}
		
		if(!util.isNoValue(jq("#regist_email").val()) && !checkFormUtil.isEmail(jq("#regist_email").val())){
			util.alertMessage("请输入正确的邮箱地址！");
			return;
		}
		if(util.isNoValue(jq("#regist_userType").val())){
			util.alertMessage("请选择旅客类型");
			return;
		}
		
		if(jq("#regist_userType").val()=="3"){
			
			if(util.isNoValue(jq("#regist_studentProvince").val())){
				util.alertMessage("请选择学校省份");
				return;
			}
			
			if(util.isNoValue(jq("#regist_studentSchool").val())){
				util.alertMessage("请选择学校名称");
				return;
			}
			
			if(!util.isNoValue(jq("#regist_studentDepartment").val())&&!checkFormUtil.checkNameChar(jq("#regist_studentDepartment").val())){
				util.alertMessage("填写的院系只能包含中文、英文、数字！");
				return;
			}
			
			if(!util.isNoValue(jq("#regist_studentClass").val())&&!checkFormUtil.checkNameChar(jq("#regist_studentClass").val())){
				util.alertMessage("填写的班级只能包含中文、英文、数字！");
				return;
			}
			
			if(util.isNoValue(jq("#regist_studentNo").val())){
				util.alertMessage("请输入学号");
				return;
			}
			
			if(!checkFormUtil.checkNameChar(jq("#regist_studentNo").val())){
				util.alertMessage("填写的学号只能包含中文、英文、数字！");
				return;
			}
			
			if(util.isNoValue(jq("#regist_studentSystem").val())){
				util.alertMessage("请选择学制");
				return;
			}
			
			

			if(util.isNoValue(jq("#regist_studentEnterYear").val())){
				util.alertMessage("请选择入学年份");
				return;
			}
			
			if(!util.isNoValue(jq("#regist_preferenceCardNo").val())&&!checkFormUtil.checkNameChar(jq("#regist_preferenceCardNo").val())){
				util.alertMessage("填写的优惠卡只能包含中文、英文、数字！");
				return;
			}
			
			if(util.isNoValue(jq("#regist_preferenceFromStation").val())||util.isNoValue(jq("#regist_preferenceToStation").val())){
				util.alertMessage("请选择优惠区间");
				return;
			}
		}
		var agree = document.getElementById("agreeChkbox").checked;
		if(!agree){
			util.alertMessage("请选择同意服务条款");
			return;
		}
		var commonParameters = {
			'user_type': jq("#regist_userType").val() ,
			'user_name': replaceChar(jq("#regist_user_name").val()),
			'name': replaceChar(jq("#regist_name").val()) ,
			'id_type_code': jq("#regist_idType").val() ,
			'id_no': jq("#regist_idNo").val() ,
			'password': hex_md5(jq("#regist_passwd").val()) ,
			'sex_code': jq("#regist_sex").val() ,
			'born_date': util.processDateCode(jq("#regist_bornDateShow").val()),
			'country_code': jq("#regist_country_code").val() ,
			'mobile_no': jq("#regist_mobile").val() ,
			'email': jq("#regist_email").val() ,
			'province_code':  jq("#regist_studentProvince_code").val(),
			'school_code': jq("#regist_studentSchool_code").val(),
			'department': jq("#regist_studentDepartment").val() ,
			'school_class': jq("#regist_studentClass").val() ,
			'student_no': jq("#regist_studentNo").val() ,
			'enter_year': jq("#regist_studentEnterYear").val(),
			'school_system': jq("#regist_studentSystem").val(),
			'preference_from_station_code':  jq("#regist_preferenceFromStation_code").val(),
			'preference_to_station_code': jq("#regist_preferenceToStation_code").val(),
			'preference_card_no': jq("#regist_preferenceCardNo").val()
		};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "checkUser4Regist"
		};
		
		var options =  {
				onSuccess: requestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	// 替换特殊字符
	function replaceChar(str) {
		var v = str.replace(/['"<> ?]/g,"");
		return v;
	}
	
	function contentIscrollRefresh(){
		if(jq("#registView .ui-content").attr("data-iscroll")!=undefined){
			jq("#registView .ui-content").iscrollview("refresh");
		}
	}
	
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			
			mor.ticket.registUserInfo.userInfo.user_type = jq("#regist_userType").val() ;
			mor.ticket.registUserInfo.userInfo.user_name = replaceChar(jq("#regist_user_name").val());
			mor.ticket.registUserInfo.userInfo.name = replaceChar(jq("#regist_name").val()) ;
			if(jq("#regist_idType").val() == "1"){
				mor.ticket.registUserInfo.userInfo.born_date = jq("#regist_idNo").val().substr(6,8);
				var sex =  /^[2468]$/;
				if(sex.test(jq("#regist_idNo").val().substr(16,1)) == true){
					mor.ticket.registUserInfo.userInfo.sex_code = "F";
				}else{
					mor.ticket.registUserInfo.userInfo.sex_code = "M";
				}
			}else{
				mor.ticket.registUserInfo.userInfo.sex_code = jq("#regist_sex").val() ;
				mor.ticket.registUserInfo.userInfo.born_date = util.processDateCode(jq("#regist_bornDateShow").val());
			}
			if(jq("#regist_idType").val() != "B"){
				mor.ticket.registUserInfo.userInfo.country_code = "CN";
			}else{
				mor.ticket.registUserInfo.userInfo.country_code = jq("#regist_country_code").val() ;
			}
			mor.ticket.registUserInfo.userInfo.id_type_code = jq("#regist_idType").val() ;
			mor.ticket.registUserInfo.userInfo.id_no = jq("#regist_idNo").val() ;
			mor.ticket.registUserInfo.userInfo.password = jq("#regist_passwd").val();
			mor.ticket.registUserInfo.userInfo.mobile_no = jq("#regist_mobile").val() ;
			mor.ticket.registUserInfo.userInfo.email = jq("#regist_email").val() ;
			mor.ticket.registUserInfo.userInfo.province_code =  jq("#regist_studentProvince_code").val();
			mor.ticket.registUserInfo.userInfo.school_code = jq("#regist_studentSchool_code").val();
			mor.ticket.registUserInfo.userInfo.department = jq("#regist_studentDepartment").val() ;
			mor.ticket.registUserInfo.userInfo.school_class = jq("#regist_studentClass").val() ;
			mor.ticket.registUserInfo.userInfo.student_no = jq("#regist_studentNo").val() ;
			mor.ticket.registUserInfo.userInfo.enter_year = jq("#regist_studentEnterYear").val();
			mor.ticket.registUserInfo.userInfo.school_system = jq("#regist_studentSystem").val();
			mor.ticket.registUserInfo.userInfo.preference_from_station_code =  jq("#regist_preferenceFromStation_code").val();
			mor.ticket.registUserInfo.userInfo.preference_to_station_code = jq("#regist_preferenceToStation_code").val();
			mor.ticket.registUserInfo.userInfo.preference_card_no = jq("#regist_preferenceCardNo").val();
			mor.ticket.util.alertMessage(invocationResult.error_msg);
			jq.mobile.changePage(vPathCallBack()+"enterRegist.html");
		} else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	function registerToDateInputChangeListener(){
		jq("#regist_bornDateShow").bind("tap",function(){
			if(document.activeElement&&document.activeElement.nodeName=='INPUT'){
				document.activeElement.blur();
			}
			var str = "regist.html";
			mor.ticket.currentPagePath.fromPath = str;
			var goingPath = vPathCallBack()+ "birthdayPicker.html";
			mor.ticket.currentPagePath.type = false;
			mor.ticket.util.changePage(goingPath);
			return false;
		});
		jq('#regist_bornDate').bind("change",function(){
			var date = jq(this).val();
			jq("#regist_bornDateShow").val(date);
		});
	}
})();