//
//  ZZNativeAnalytics.m
//  MobileTicket
//
//  Created by EASY on 15/11/17.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZNativeAnalytics.h"
#import <AVAnalytics.h>
#import <RCTConvert.h>

@implementation ZZNativeAnalytics

RCT_EXPORT_MODULE(NativeAnalytics)


#pragma mark - AVAnalytics

RCT_EXPORT_METHOD(beginPageView:(NSString *) pageName successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	[AVAnalytics beginLogPageView:pageName];
}

RCT_EXPORT_METHOD(endPageView:(NSString *) pageName successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	[AVAnalytics endLogPageView:pageName];
}

RCT_EXPORT_METHOD(event:(NSString *) event attributes:(id) attrs successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	if ([attrs isKindOfClass:[NSString class]]) {
		[AVAnalytics event:event label:(NSString *)attrs];
	} else if ([attrs isKindOfClass:[NSDictionary class]]) {
		[AVAnalytics event:event attributes:attrs];
	} else if ([attrs isKindOfClass:[NSNumber class]]) {
		[AVAnalytics event:event durations:[attrs intValue]];
	}
}

RCT_EXPORT_METHOD(updateOnlineConfigs:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	[AVAnalytics updateOnlineConfigWithBlock:^(NSDictionary *dict, NSError *error) {
		if (error) {
			fail(error);
		} else {
			dict = dict?: @{};
			success(@[dict]);
		}
	}];
}

RCT_EXPORT_METHOD(onlineConfigParam:(NSString *) key  successed:(RCTResponseSenderBlock) success failed:(RCTResponseErrorBlock) fail) {
	key = [RCTConvert NSString:key];
	NSString *string = [AVAnalytics getConfigParams:key];
	string = string ?: @"";
	success(@[string]);
}

@end
