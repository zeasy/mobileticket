//
//  MTDatabaseTableModel.m
//  MobileTicket
//
//  Created by EASY on 15/10/28.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZNativeDatabaseTableModel.h"
#import <objc/runtime.h>
#import <BZObjectStoreModelInterface.h>

@interface ZZNativeDatabaseTableModel ()

@property (nonatomic, strong) NSMutableDictionary<OSIgnoreAttribute> *customKeyValues;

@end

@implementation ZZNativeDatabaseTableModel

+(NSArray *) ignoredPropertyNames {
	return @[@"customKeyValues"];
}

-(NSMutableDictionary *)customKeyValues {
	if (!_customKeyValues) {
		_customKeyValues = [NSMutableDictionary dictionary];
	}
	return _customKeyValues;
}


-(void)setValue:(id)value forUndefinedKey:(NSString *)key {
	if (key) {
		if (value) {
			self.customKeyValues[key] = value;
		} else {
			[self.customKeyValues removeObjectForKey:key];
		}
	}
}

-(id)valueForUndefinedKey:(NSString *)key {
	return key?self.customKeyValues[key]:nil;
}

@end
