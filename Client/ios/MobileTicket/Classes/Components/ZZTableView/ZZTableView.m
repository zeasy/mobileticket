//
//  ZZTableView.m
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZTableView.h"
#import <RCTView.h>
#import "ZZTableViewCell.h"
#import "ZZTableViewHeaderFooterView.h"
#import <UIView+React.h>


#import "ZZTableDataSource.h"

@interface ZZTableView () <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) ZZTableDataSource *model;
@property (nonatomic, strong) NSMutableArray *_subviews;
<<<<<<< HEAD
=======

>>>>>>> 53e7e05571516cb5d343000bdf7fd1589036391b
@property (nonatomic, strong) RCTEventDispatcher *eventDispatcher;
@end

@implementation ZZTableView

- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher
{
	self = [super init];
	if (self) {
		self._subviews = [NSMutableArray array];
<<<<<<< HEAD
    self.eventDispatcher = eventDispatcher;
=======
		self.eventDispatcher = eventDispatcher;
>>>>>>> 53e7e05571516cb5d343000bdf7fd1589036391b
	}
	return self;
}

-(void) createTableView {
	if (_tableView) {
		[_tableView removeFromSuperview];
	}
	_tableView = [[UITableView alloc] initWithFrame:CGRectZero style:self.tableViewStyle];
	_tableView.delegate = self;
	_tableView.dataSource = self;
	[self addSubview:_tableView];
}

-(void) createDataSource {

}


-(void)setDataSource:(NSDictionary *)dataSource {
	_dataSource = dataSource;
	[self createDataSource];
}

-(void)setTableViewStyle:(UITableViewStyle)tableViewStyle {
		_tableViewStyle = tableViewStyle;
		[self createTableView];
}

-(void)layoutSubviews {
	[super layoutSubviews];
	_tableView.frame = self.bounds;
}

-(void) insertCell:(ZZTableViewCell *) cell {
	if (!cell.indexPath) {
//		[self.tableView registerClass:[_ZZTableViewCell class] forCellReuseIdentifier:cell.reuseIdentifier];
	} else {
		_ZZTableViewCell *c = (_ZZTableViewCell *)[self.tableView cellForRowAtIndexPath:cell.indexPath];
		c.cell = cell;
	}
}

-(void) insertHeaderFooterView:(ZZTableViewHeaderFooterView *) headerFooterView {
	if (!headerFooterView.section) {
		[self.tableView registerClass:[_ZZTableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:headerFooterView.reuseIdentifier];
	}  else {
		if (headerFooterView.isHeader) {
			_ZZTableViewHeaderFooterView *headerView = (_ZZTableViewHeaderFooterView *)[self.tableView headerViewForSection:[headerFooterView.section integerValue]];
			headerView.headerFooterView = headerFooterView;
		}else {
			_ZZTableViewHeaderFooterView *footerView = (_ZZTableViewHeaderFooterView *)[self.tableView footerViewForSection:[headerFooterView.section integerValue]];
			footerView.headerFooterView = headerFooterView;
		}
	}
}

#pragma mark -

- (void)insertReactSubview:(id<RCTComponent>)subview atIndex:(NSInteger)atIndex {
	if ([subview isKindOfClass:[ZZTableViewCell class]]) {
		ZZTableViewCell *cell = (ZZTableViewCell *) subview;
		[self insertCell:cell];
	} else if ([subview isKindOfClass:[ZZTableViewHeaderFooterView class]]) {
		ZZTableViewHeaderFooterView *headerFooterView = (ZZTableViewHeaderFooterView *) subview;
		[self insertHeaderFooterView:headerFooterView];
	} else {
		[self._subviews insertObject:subview atIndex:atIndex];
	}
}

- (void)removeReactSubview:(id<RCTComponent>)subview {
	if ([subview isKindOfClass:[ZZTableViewCell class]]) {
		
	} else if ([subview isKindOfClass:[ZZTableViewHeaderFooterView class]]) {
		
	} else {
		[self._subviews removeObject:subview];
	}
}

-(NSArray *)reactSubviews {
	return self._subviews;
}
 

#pragma mark - UITableViewDataSource


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	NSDictionary *sectionDict = self.dataSource[@"sections"][section];
	NSDictionary *headerDict = sectionDict[@"header"];
	NSString *reuseIdentifier = headerDict[@"reuseIdentifier"];
	if (reuseIdentifier) {
		_ZZTableViewHeaderFooterView *v = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:reuseIdentifier];
		if (!v.headerFooterView) {
			if (self.onReuseHeaderFooterView) {
				self.onReuseHeaderFooterView(@{@"reuseIdentifier":reuseIdentifier,@"section":@(section),@"isHeader":@(YES)});
			}
		}
		return v;
	} else {
		return nil;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	NSDictionary *sectionDict = self.dataSource[@"sections"][section];
	NSDictionary *headerDict = sectionDict[@"header"];
	return headerDict[@"height"]?[headerDict[@"height"] floatValue] : 20.f;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
	NSDictionary *sectionDict = self.dataSource[@"sections"][section];
	NSDictionary *footerDict = sectionDict[@"footer"];
	NSString *reuseIdentifier = footerDict[@"reuseIdentifier"];
	if (reuseIdentifier) {
		_ZZTableViewHeaderFooterView *v = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:reuseIdentifier];
		if (!v.headerFooterView) {
			if (self.onReuseHeaderFooterView) {
				self.onReuseHeaderFooterView(@{@"reuseIdentifier":reuseIdentifier,@"section":@(section),@"isHeader":@(NO)});
			}
		}
		
		return v;
	} else {
		return nil;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	NSDictionary *sectionDict = self.dataSource[@"sections"][section];
	NSDictionary *footerDict = sectionDict[@"footer"];
	return footerDict[@"height"]?[footerDict[@"height"] floatValue] : 20.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSDictionary *sectionDict = self.dataSource[@"sections"][section];
	return [sectionDict[@"rows"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *rowDict = self.dataSource[@"sections"][indexPath.section][@"rows"][indexPath.row] ?: @{};
	NSString *reuseIdentifier = rowDict[@"reuseIdentifier"] ?: @"default";
	UITableViewCellStyle cellStyle = rowDict[@"cellStyle"]?[rowDict[@"cellStyle"] integerValue] : UITableViewCellStyleDefault;
	
	_ZZTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
	if (!cell) {
			cell = [[_ZZTableViewCell alloc] initWithStyle:cellStyle reuseIdentifier:reuseIdentifier];
	}
	cell.textLabel.text = rowDict[@"text"];
	cell.detailTextLabel.text = rowDict[@"detailText"];
	cell.accessoryType = [rowDict[@"accessoryType"] integerValue];
	
	if (self.onReuseCell) {
		NSMutableDictionary *cellInfo = [@{
																			 @"indexPath":@{@"section":@(indexPath.section),@"row":@(indexPath.row)},
																			 @"reuseIdentifier":reuseIdentifier,
																			 @"rowData":rowDict} mutableCopy];
		if (cell.cell.reactTag) {
			cellInfo[@"reactTag"] = cell.cell.reactTag;
		}
//		else {
//			cellInfo[@"reactTag"] = [NSString stringWithFormat:@"cell_row_%ld_section_%ld",indexPath.row,indexPath.section];
//		}
		
		self.onReuseCell(cellInfo);
	}
	
	return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [self.dataSource[@"sections"] count];
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return self.dataSource[@"sections"][section][@"header"][@"title"];
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	return self.dataSource[@"sections"][section][@"footer"][@"title"];;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *rowDict = self.dataSource[@"sections"][indexPath.section][@"rows"][indexPath.row];
	return [rowDict[@"canEdit"] boolValue];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *rowDict = self.dataSource[@"sections"][indexPath.section][@"rows"][indexPath.row];
	return [rowDict[@"canMove"] boolValue];
}

- (nullable NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView  {
//	return self.dataSource[@"sectionIndexTitles"];
	NSMutableArray *titles = [NSMutableArray array];
	NSArray *sections = self.dataSource[@"sections"];
	for (NSDictionary *section in sections) {
		if (section[@"indexTitle"]) {
			[titles addObject:section[@"indexTitle"]];
		}
	}
	return titles;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index  {
	NSUInteger count = -1;
	NSArray *sections = self.dataSource[@"sections"];
	for (int i = 0;i < [sections count];i++) {
		NSDictionary *section = sections[i];
		if (section[@"indexTitle"]) {
			count++;
		}
		if (count == index) {
			return i;
		}
	}
	
	
	return index;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
<<<<<<< HEAD
  if (self.onDidSelectRowAtIndexPath) {
    self.onDidSelectRowAtIndexPath(@{@"indexPath":@{@"row":@(indexPath.row),@"section":@(indexPath.section)}});
  }
  
//  [self.eventDispatcher sendInputEventWithName:@"didSelectRowAtIndexPath"
//                                          body:@{
//                                                 @"target":self.reactTag,
//                                                 @"indexPath":@{@"row":@(indexPath.row),@"section":@(indexPath.section)}
//                                                 }];
}
=======
	if (self.onDidSelectedRow) {
		self.onDidSelectedRow(@{@"indexPath" : @{@"row":@(indexPath.row),@"section":@(indexPath.section)}});
	}
}


>>>>>>> 53e7e05571516cb5d343000bdf7fd1589036391b

@end
