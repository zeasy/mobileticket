//
//  CloudAnalytics.m
//  MobileTicketWeb
//
//  Created by EASY on 16/1/15.
//  Copyright © 2016年 Z.EASY. All rights reserved.
//

#import "CloudAnalytics.h"
#import "MTA.h"
#import "MTAConfig.h"
#import <RCTConvert.h>

@implementation CloudAnalytics

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(beginPageView:(NSString *) pageView s:(RCTResponseSenderBlock) s f:(RCTResponseErrorBlock) f) {
	pageView = [RCTConvert NSString:pageView];
	[MTA trackPageViewBegin:pageView];
}

RCT_EXPORT_METHOD(endPageView:(NSString *) pageView s:(RCTResponseSenderBlock) s f:(RCTResponseErrorBlock) f) {
	pageView = [RCTConvert NSString:pageView];
	[MTA trackPageViewEnd:pageView];
}

RCT_EXPORT_METHOD(event:(NSDictionary *) options s:(RCTResponseSenderBlock) s f:(RCTResponseErrorBlock) f) {
	options = [RCTConvert NSDictionary:options];
	NSString *event = [RCTConvert NSString:options[@"event"]];
	NSDictionary *attrs = [RCTConvert NSDictionary:options[@"attributes"]];
	NSString *label = [RCTConvert NSString:options[@"label"]];
	NSTimeInterval duration = [RCTConvert NSTimeInterval:options[@"duration"]];
	
	if (attrs) {
		[MTA trackCustomKeyValueEvent:event props:attrs];
	} else if (label) {
		[MTA trackCustomEvent:event args:@[label]];
	} else if (options[@"duration"]) {
		[MTA trackCustomKeyValueEventDuration:duration withEventid:event props:attrs];
	} else {
		[MTA trackCustomEvent:event args:@[]];
	}
}

RCT_EXPORT_METHOD(config:(NSString *) key s:(RCTResponseSenderBlock) s f:(RCTResponseErrorBlock) f) {
	key = [RCTConvert NSString:key];
	NSString *value = [[MTAConfig getInstance] getCustomProperty:key default:@""];
	if (s) {
		s(@[value]);
	}
}

@end
