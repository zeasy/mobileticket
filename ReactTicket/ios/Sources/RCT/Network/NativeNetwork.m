//
//  NativeNetwork.m
//  MobileTicketWeb
//
//  Created by EASY on 16/1/13.
//  Copyright © 2016年 Z.EASY. All rights reserved.
//

#import "NativeNetwork.h"
#import <AFNetworking.h>
#import <RCTConvert.h>
#import <CMDQueryStringSerialization.h>

@implementation NativeNetwork

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(request:(NSDictionary *) options resolve:(RCTPromiseResolveBlock) resolve reject:(RCTPromiseRejectBlock) reject) {
	options = [RCTConvert NSDictionary:options];
	NSString *url = [RCTConvert NSString:options[@"url"]];
    NSDictionary *params = options[@"params"];
	NSString *method = [RCTConvert NSString:options[@"method"]];
	NSDictionary *headers = [RCTConvert NSDictionary:options[@"headers"]];
	NSArray *cookies = [RCTConvert NSArray:options[@"cookies"]];
	
	NSString *format = [RCTConvert NSString:options[@"format"]];
	
	
	
	NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
	if ([cookies count]) {
		conf = [NSURLSessionConfiguration ephemeralSessionConfiguration];
		for (int i = 0; i < [cookies count]; i++) {
			NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookies[i]];
			[conf.HTTPCookieStorage setCookie:cookie];
		}
	}
	
	AFHTTPSessionManager *manager =  [[AFHTTPSessionManager alloc] initWithSessionConfiguration:conf];
	manager.securityPolicy.allowInvalidCertificates = YES;
	manager.securityPolicy.validatesDomainName = NO;
	
	manager.requestSerializer = [AFHTTPRequestSerializer serializer];
	manager.responseSerializer = [AFHTTPResponseSerializer serializer];

	// support timeout
	id timeout = options[@"timeout"];
	if (timeout) {
		manager.requestSerializer.timeoutInterval = [RCTConvert NSTimeInterval:timeout];
	}

	for (id key in [headers allKeys]) {
		id v = headers[key];
		[manager.requestSerializer setValue:v forHTTPHeaderField:key];
	}
    
    if([params isKindOfClass:[NSArray class]]) {
        [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * (NSURLRequest *  request, id   parameters, NSError *  __autoreleasing * error) {
          NSMutableString *builder = [NSMutableString string];
          
          for (int i = 0;i < [params count];i++) {
            id param = parameters[i];
            if ([param isKindOfClass:[NSDictionary class]]) {
                NSString *s = [CMDQueryStringSerialization queryStringWithDictionary:param];
              if (s) [builder appendString:s];
            } else if ([param isKindOfClass:[NSString class]]){
              [builder appendString:param];
            }
            if(i < params.count -1) {
              [builder appendString:@"&"];
            }
              
          }
          return builder;
          
        }];
    }
    

	
	void (^s)(NSURLSessionDataTask * , id ) = ^(NSURLSessionDataTask * task, id object){
		if ([@"DATA" compare:format options:NSCaseInsensitiveSearch] == NSOrderedSame) {
      object = [object base64EncodedStringWithOptions:0];
		} else {
			object = [[NSString alloc] initWithData:object encoding:NSUTF8StringEncoding];
		}

		resolve(object);
	};
	void (^f)(NSURLSessionDataTask * , id ) = ^(NSURLSessionDataTask * task, NSError *err){
		reject([@(err.code) stringValue],[err localizedFailureReason],err);
	};
	
	if ([@"POST" compare:method options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		[manager POST:url parameters:params progress:nil success:s failure:f];
	} else if ([@"PUT" compare:method options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		[manager PUT:url parameters:params success:s failure:f];
	} else if ([@"DELETE" compare:method options:NSCaseInsensitiveSearch] == NSOrderedSame) {
		[manager DELETE:url parameters:params success:s failure:f];
	} else {
		[manager GET:url parameters:params progress:nil success:s failure:f];
	}
}


@end
