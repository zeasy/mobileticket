/**
 * @providesModule RTHud
 * Created by easy on 16/3/19.
 */

import SVProgressHUD from 'SVProgressHUD'

export default class RTHud {

    static showLoading() {
        SVProgressHUD.showWithMaskType(SVProgressHUD.MaskType.Clear);
    }

    static dismiss() {
        SVProgressHUD.dismiss();
    }

    static showSuccess(status) {
        SVProgressHUD.showSuccessWithStatusAndMaskType(status,SVProgressHUD.MaskType.Clear);
    }

    static showFail(status) {
        SVProgressHUD.showErrorWithStatusAndMaskType(status,SVProgressHUD.MaskType.Clear);
    }

    static showInfo(status) {
        SVProgressHUD.showInfoWithStatusAndMaskType(status,SVProgressHUD.MaskType.Clear);
    }
}