#line 1 "/Users/easy/Desktop/Git/mobileticket/Hacker/MobileTicket/MobileTicket/MobileTicket.xm"




#include <logos/logos.h>
#include <substrate.h>
@class CustomHTTPProtocol; 
static id (*_logos_orig$_ungrouped$CustomHTTPProtocol$customHTTPProtocol$logWithFormat$)(CustomHTTPProtocol*, SEL, id, id); static id _logos_method$_ungrouped$CustomHTTPProtocol$customHTTPProtocol$logWithFormat$(CustomHTTPProtocol*, SEL, id, id); static id (*_logos_orig$_ungrouped$CustomHTTPProtocol$initWithRequest$cachedResponse$client$)(CustomHTTPProtocol*, SEL, id, id, id); static id _logos_method$_ungrouped$CustomHTTPProtocol$initWithRequest$cachedResponse$client$(CustomHTTPProtocol*, SEL, id, id, id); static void (*_logos_orig$_ungrouped$CustomHTTPProtocol$didReceiveAuthenticationChallenge$)(CustomHTTPProtocol*, SEL, NSURLAuthenticationChallenge *); static void _logos_method$_ungrouped$CustomHTTPProtocol$didReceiveAuthenticationChallenge$(CustomHTTPProtocol*, SEL, NSURLAuthenticationChallenge *); static id (*_logos_orig$_ungrouped$CustomHTTPProtocol$resolveAuthenticationChallenge$withCredential$)(CustomHTTPProtocol*, SEL, NSURLAuthenticationChallenge *, NSURLCredential *); static id _logos_method$_ungrouped$CustomHTTPProtocol$resolveAuthenticationChallenge$withCredential$(CustomHTTPProtocol*, SEL, NSURLAuthenticationChallenge *, NSURLCredential *); static NSURLRequest * (*_logos_orig$_ungrouped$CustomHTTPProtocol$connection$willSendRequest$redirectResponse$)(CustomHTTPProtocol*, SEL, NSURLConnection *, NSURLRequest *, NSURLResponse *); static NSURLRequest * _logos_method$_ungrouped$CustomHTTPProtocol$connection$willSendRequest$redirectResponse$(CustomHTTPProtocol*, SEL, NSURLConnection *, NSURLRequest *, NSURLResponse *); static void (*_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveAuthenticationChallenge$)(CustomHTTPProtocol*, SEL, NSURLConnection *, NSURLAuthenticationChallenge *); static void _logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveAuthenticationChallenge$(CustomHTTPProtocol*, SEL, NSURLConnection *, NSURLAuthenticationChallenge *); static void (*_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveResponse$)(CustomHTTPProtocol*, SEL, id, id); static void _logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveResponse$(CustomHTTPProtocol*, SEL, id, id); static void (*_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveData$)(CustomHTTPProtocol*, SEL, id, id); static void _logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveData$(CustomHTTPProtocol*, SEL, id, id); 

#line 5 "/Users/easy/Desktop/Git/mobileticket/Hacker/MobileTicket/MobileTicket/MobileTicket.xm"
#import "HackCustomHTTPProtocol.h"


static id _logos_method$_ungrouped$CustomHTTPProtocol$customHTTPProtocol$logWithFormat$(CustomHTTPProtocol* self, SEL _cmd, id p, id format) {
	NSLog(@"==========>[CustomHTTPProtocol logWithFormat]\n%@",format);
	return _logos_orig$_ungrouped$CustomHTTPProtocol$customHTTPProtocol$logWithFormat$(self, _cmd, p, format);
}

static id _logos_method$_ungrouped$CustomHTTPProtocol$initWithRequest$cachedResponse$client$(CustomHTTPProtocol* self, SEL _cmd, id req, id resp, id client) {
	self = _logos_orig$_ungrouped$CustomHTTPProtocol$initWithRequest$cachedResponse$client$(self, _cmd, req, resp, client);
	[[HackCustomHTTPProtocol sharedInstance] __initWithRequest:req cachedResponse:resp client:client];
	return self;
}

static void _logos_method$_ungrouped$CustomHTTPProtocol$didReceiveAuthenticationChallenge$(CustomHTTPProtocol* self, SEL _cmd, NSURLAuthenticationChallenge * challenge) {
	[[HackCustomHTTPProtocol sharedInstance] didReceiveAuthenticationChallenge:challenge];
}
static id _logos_method$_ungrouped$CustomHTTPProtocol$resolveAuthenticationChallenge$withCredential$(CustomHTTPProtocol* self, SEL _cmd, NSURLAuthenticationChallenge * challenge, NSURLCredential * c) {		[[HackCustomHTTPProtocol sharedInstance] resolveAuthenticationChallenge:challenge withCredential:c];
	return _logos_orig$_ungrouped$CustomHTTPProtocol$resolveAuthenticationChallenge$withCredential$(self, _cmd, challenge, c);
}



static NSURLRequest * _logos_method$_ungrouped$CustomHTTPProtocol$connection$willSendRequest$redirectResponse$(CustomHTTPProtocol* self, SEL _cmd, NSURLConnection * connection, NSURLRequest * request, NSURLResponse * response) {
	id result = _logos_orig$_ungrouped$CustomHTTPProtocol$connection$willSendRequest$redirectResponse$(self, _cmd, connection, request, response);
	[[HackCustomHTTPProtocol sharedInstance] connection:connection willSendRequest:request redirectResponse:response];
	return result;
}
static void _logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveAuthenticationChallenge$(CustomHTTPProtocol* self, SEL _cmd, NSURLConnection * conn, NSURLAuthenticationChallenge * c) {
	_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveAuthenticationChallenge$(self, _cmd, conn, c);
	[[HackCustomHTTPProtocol sharedInstance] connection:conn didReceiveAuthenticationChallenge:c];
}

static void _logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveResponse$(CustomHTTPProtocol* self, SEL _cmd, id connection, id response) {
	[[HackCustomHTTPProtocol sharedInstance] connection:connection didReceiveResponse:response];
	_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveResponse$(self, _cmd, connection, response);
	

}

static void _logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveData$(CustomHTTPProtocol* self, SEL _cmd, id connection, id data) {
	[[HackCustomHTTPProtocol sharedInstance] connection:connection didReceiveData:data];
	_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveData$(self, _cmd, connection, data);
	

}

static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$CustomHTTPProtocol = objc_getClass("CustomHTTPProtocol"); MSHookMessageEx(_logos_class$_ungrouped$CustomHTTPProtocol, @selector(customHTTPProtocol:logWithFormat:), (IMP)&_logos_method$_ungrouped$CustomHTTPProtocol$customHTTPProtocol$logWithFormat$, (IMP*)&_logos_orig$_ungrouped$CustomHTTPProtocol$customHTTPProtocol$logWithFormat$);MSHookMessageEx(_logos_class$_ungrouped$CustomHTTPProtocol, @selector(initWithRequest:cachedResponse:client:), (IMP)&_logos_method$_ungrouped$CustomHTTPProtocol$initWithRequest$cachedResponse$client$, (IMP*)&_logos_orig$_ungrouped$CustomHTTPProtocol$initWithRequest$cachedResponse$client$);MSHookMessageEx(_logos_class$_ungrouped$CustomHTTPProtocol, @selector(didReceiveAuthenticationChallenge:), (IMP)&_logos_method$_ungrouped$CustomHTTPProtocol$didReceiveAuthenticationChallenge$, (IMP*)&_logos_orig$_ungrouped$CustomHTTPProtocol$didReceiveAuthenticationChallenge$);MSHookMessageEx(_logos_class$_ungrouped$CustomHTTPProtocol, @selector(resolveAuthenticationChallenge:withCredential:), (IMP)&_logos_method$_ungrouped$CustomHTTPProtocol$resolveAuthenticationChallenge$withCredential$, (IMP*)&_logos_orig$_ungrouped$CustomHTTPProtocol$resolveAuthenticationChallenge$withCredential$);MSHookMessageEx(_logos_class$_ungrouped$CustomHTTPProtocol, @selector(connection:willSendRequest:redirectResponse:), (IMP)&_logos_method$_ungrouped$CustomHTTPProtocol$connection$willSendRequest$redirectResponse$, (IMP*)&_logos_orig$_ungrouped$CustomHTTPProtocol$connection$willSendRequest$redirectResponse$);MSHookMessageEx(_logos_class$_ungrouped$CustomHTTPProtocol, @selector(connection:didReceiveAuthenticationChallenge:), (IMP)&_logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveAuthenticationChallenge$, (IMP*)&_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveAuthenticationChallenge$);MSHookMessageEx(_logos_class$_ungrouped$CustomHTTPProtocol, @selector(connection:didReceiveResponse:), (IMP)&_logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveResponse$, (IMP*)&_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveResponse$);MSHookMessageEx(_logos_class$_ungrouped$CustomHTTPProtocol, @selector(connection:didReceiveData:), (IMP)&_logos_method$_ungrouped$CustomHTTPProtocol$connection$didReceiveData$, (IMP*)&_logos_orig$_ungrouped$CustomHTTPProtocol$connection$didReceiveData$);} }
#line 52 "/Users/easy/Desktop/Git/mobileticket/Hacker/MobileTicket/MobileTicket/MobileTicket.xm"
