
/* JavaScript content from js/controller/queryRefundInfo.js in folder common */
(function() {
	jq("#queryRefundInfoView").live("pageinit", function() {
	});
	jq("#queryRefundInfoView").live("pagebeforeshow", function() {
		mor.ticket.util.initAppVersionInfo();
		registerButtonListener();
		var user = mor.ticket.loginUser;
		if(user.isAuthenticated === "Y"){
			refundInfoDetailFn();
			//jq("#expressTrackBtn").bind("tap",queryExpressStatusDetail);
		}else{
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
			} else {
				registerAutoLoginHandler(refundInfoDetailFn, autologinFailJump);
			}
		}
	});
	function refundInfoDetailFn(){
		jq("#queryRefundInfoDetail").empty();
		for(var i=0 ; i<mor.ticket.orderRefundInfo.info.length;i++){
			if(mor.ticket.orderRefundInfo.info[i].origin_name ==null){
				mor.ticket.orderRefundInfo.info[i].origin_name = "";
			}else if (mor.ticket.orderRefundInfo.info[i].passenger_name == null){
				mor.ticket.orderRefundInfo.info[i].passenger_name ="";
			}else if(mor.ticket.orderRefundInfo.info[i].status ==null){
				mor.ticket.orderRefundInfo.info[i].status ="";
			} else if(mor.ticket.orderRefundInfo.info[i].transDate == null){
				mor.ticket.orderRefundInfo.info[i].transDate = "";
			}else if (mor.ticket.orderRefundInfo.info[i].transNo == null){
				mor.ticket.orderRefundInfo.info[i].transNo = "";
			}
		}
		jq("#queryRefundInfoDetail").html(generateQueryRefundInfoList(mor.ticket.orderRefundInfo.info));
		jq("#queryRefundInfoView .ui-content").iscrollview("refresh");
		setTimeout(function(){
			setAddressHeight();
		},100);
	}
	

	function setAddressHeight(){
		jq.grep(jq("li"),function(address,data_index){
			var li = jq("#refund_" + data_index);
			if(li.find("#transStatus").height() !== 32){
				li.find("#statusLabel").css("margin-top",-li.find("#transStatus").height()/2);
			}
		});
	}
	
	function registerButtonListener(){
		jq("#refundInfoBackBtn").bind("tap",function(e){
			e.stopImmediatePropagation();
			mor.ticket.util.changePage(vPathCallBack() + "finishedOrderDetail.html");
			return false;
		});
	}
	var queryRefundInfoTemplate =
		"{{~it :queryRefundInfo:index}}" +
			"{{ if(queryRefundInfo.transStatus == '-1'){ }}" +
				"<li id='refund_{{=index}}'>" +
				"<fieldset class='hasLegend' style='margin-top:10px;'>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
					"<label>明&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;细</label>" +
					"<div class='pencil-input'>" +
							"<div class='ui-input-text'>{{=queryRefundInfo.passenger_name}}&nbsp;&nbsp;{{=queryRefundInfo.status}}</div>" +
						"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
						"<label>退款金额</label>" +
						"<div class='pencil-input'>" +
							"<div class='ui-input-text' style='width:100%;'></div>" +
						"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
						"<label>来&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;源</label>" +
						"<div class='pencil-input'>" +
							"<div class='ui-input-text' style='width:100%;'>{{=queryRefundInfo.origin_name}}</div>" +
						"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
						"<label>申请时间</label>" +
						"<div class='pencil-input'>" +
							"<div class='ui-input-text'></div>" +
						"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
					"<label>流&nbsp;&nbsp;水&nbsp;&nbsp;号</label>" +
					"<div class='pencil-input'>" +
						"<div class='ui-input-text'></div>" +
					"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortnoline ui-field-contain'>" +
					"<label id='statusLabel'>状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态</label>" +
					"<div class='pencil-input' id='transStatus'>" +
						"<div class='ui-input-text'>退款处理中，如超过15个工作日未到账，请致电铁路客服12306进行查询。</div>" +
					"</div>" +
					"</div>" +
				"</fieldset>" +
			"</li>" +
			"{{ }else{ }}" +
				"<li id='refund_{{=index}}'>" +
					"<fieldset class='hasLegend' style='margin-top:10px;'>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
						"<label>明&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;细</label>" +
						"<div class='pencil-input'>" +
							"<div class='ui-input-text'>{{=queryRefundInfo.passenger_name}}&nbsp;&nbsp;{{=queryRefundInfo.status}}</div>" +
						"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
						"<label>退款金额</label>" +
						"<div class='pencil-input'>" +
							"<div class='ui-input-text' style='width:100%;'>{{=queryRefundInfo.transAmount}}元</div>" +
						"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
						"<label>来&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;源</label>" +
						"<div class='pencil-input'>" +
							"<div class='ui-input-text' style='width:100%;'>{{=queryRefundInfo.origin_name}}</div>" +
						"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
						"<label>申请时间</label>" +
						"<div class='pencil-input'>" +
							"<div class='ui-input-text'>{{=queryRefundInfo.transDate}}</div>" +
						"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortline ui-field-contain'>" +
					"<label>流&nbsp;&nbsp;水&nbsp;&nbsp;号</label>" +
					"<div class='pencil-input'>" +
						"<div class='ui-input-text'>{{=queryRefundInfo.transNo}}</div>" +
					"</div>" +
					"</div>" +
					"<div data-role='fieldcontain' class='departshortnoline ui-field-contain'>" +
					"<label id='statusLabel'>状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态</label>" +
					"<div class='pencil-input' id='transStatus'>" +
							"{{ if(queryRefundInfo.transAmount > 0){ }}" +
								"{{ if(queryRefundInfo.transStatus == '1'){ }}" +
								"<div class='ui-input-text'>成功，已成功退至银行，如已超过15个工作日尚未到账，请持支付银行卡至{{=mor.ticket.util.getBankName(queryRefundInfo.bankCode)}}查询</div>" +
								"{{ }else if(queryRefundInfo.transStatus == '2'){ }}" +
								"<div class='ui-input-text'>失败，银行退款失败，请与{{=mor.ticket.util.getBankName(queryRefundInfo.bankCode)}}联系. </div>" +
								"{{ }else if(queryRefundInfo.transStatus == '3'){ }}" +
								"<div class='ui-input-text'>处理中，退款处理中，如已超过15个工作日尚未到账，请致电铁路客服12306进行查询</div>" +
								"{{ }else{ }}" +
								"<div class='ui-input-text'>处理中，退款处理中，如已超过15个工作日尚未到账，请致电铁路客服12306进行查询</div>" +
								"{{ } }}" +
								"{{ }else{ }}" +
								"<div class='ui-input-text'>退款金额为0元，无后续处理。</div>" +	
								"{{ } }}" +
							"</div>" +
						"</div>" +
					"</fieldset>" +
				"</li>" +
			"{{ } }}" +
		"{{~}}";
	var generateQueryRefundInfoList = doT.template(queryRefundInfoTemplate);
})();
	