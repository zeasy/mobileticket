
/* JavaScript content from js/controller/agencySellTicketList.js in folder common */
(function() {		
	var page_no = 0; 
	var showMore = false;
	jq("#AgencySellTicketListView").live("pageinit", function() {
		registerAgencySellTicketListItemClickHandler();
		jq("#agencySellTicketList_content", this).bind( { 
	        "agency-iscroll_onpulldown" : onPullDown,    
	        "agency-iscroll_onpullup"   : onPullUp});
		jq("#agencySellTicketListBackBtn").bind("tap",function(){
			mor.ticket.util.changePage("agencySellTicketQuery.html");
			return false;
		});
	});
	

	//往下拉是重新刷新
	function onPullDown(event, data){
		setTimeout(function fakeRetrieveDataTimeout() { 
		      gotPullDownData(event, data); }, 
		      1500); 
	}
	
	function  gotPullDownData(event, data){
		page_no = 0;
		showMore =false;
		initQueryAgencySellTicket();	
		data.iscrollview.refresh();
	}
	//往上拉是加载更多
	function onPullUp(){
		++page_no;
		showMore = true;
		initQueryAgencySellTicket();		
	}
	
	function agencySellTicketListFn(e, data){
		page_no = 0;
		showMore =false;
		var prePage;
		//如果从agencySellTicketDetails页面返回，那么不重新查询
		prePage = data.prevPage.attr("id");
		if(prePage !="AgencySellTicketDetailView"){
			initQueryAgencySellTicket();
		}else{
			var agencySellTicketTempList = mor.ticket.currentQueryObject.agencySellTicketList;
			if(agencySellTicketTempList){//查询订单成功
				if(agencySellTicketTempList.length){
					var agencySellTicketPrompt = "<span>" + 
						mor.ticket.views.agencySellTicketQuery.province +
						mor.ticket.views.agencySellTicketQuery.city + 
						mor.ticket.views.agencySellTicketQuery.county + "共有<span class='text_orange' style='bottom:0px;'>" + agencySellTicketTempList.length + "</span>个代售点</span>";
					jq("#agencySellTicketPrompt .queryOrderTips").html(agencySellTicketPrompt);
					jq("#agencySellTicketPrompt").show();
					jq("#agencySellTicketList").html(generateAgencySellTicketsList(agencySellTicketTempList)).listview("refresh");
					jq("#agency-iscroll-pullup").hide();
					jq("#agency-iscroll-pulldown").hide();
				}else {//火车票代售点为空
					mor.ticket.util.alertMessage("没有查询到火车票代售点。");
					jq("#agency-iscroll-pullup").hide();
				}			
			} else {//查询代售点失败
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}
		}
	}
	jq("#AgencySellTicketListView").live("pageshow", function(e, data) {
		mor.ticket.util.initAppVersionInfo();
		agencySellTicketListFn(e, data);
	});
	
	function prepareQueryAgencySellTicketPrompt(){
		var jsonPrompt;	
		jsonPrompt = {
				'province':mor.ticket.views.agencySellTicketQuery.province,
				'city':mor.ticket.views.agencySellTicketQuery.city,
				'county':mor.ticket.views.agencySellTicketQuery.county,
				'agency_name':mor.ticket.views.agencySellTicketQuery.agency_name,
				'page_no':page_no.toString(),
				'rows_number':'8'
		};
		return jsonPrompt;
	}
	
	function initQueryAgencySellTicket(){
		var util = mor.ticket.util;
		var commonParameters = prepareQueryAgencySellTicketPrompt();
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "queryDetailsAgentSell"
		};
		
		var options =  {
				onSuccess: showMore?requestAddMoreSucceeded:requestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
	}
	
	function requestSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)){
			var agencySellTicketTempList = invocationResult.ofqAgencyCacheResult;
			mor.ticket.currentQueryObject.agencySellTicketList = agencySellTicketTempList;	
			if(agencySellTicketTempList){//查询火车票代售点成功
				if(agencySellTicketTempList.length){			
					var agencySellTicketPrompt = "<span>" + 
					mor.ticket.views.agencySellTicketQuery.province +
					mor.ticket.views.agencySellTicketQuery.city + 
					mor.ticket.views.agencySellTicketQuery.county + "共有<span class='text_orange' style='bottom:0px;'>" + agencySellTicketTempList.length + "</span>个代售点</span>";
					jq("#agencySellTicketPrompt .queryOrderTips").html(agencySellTicketPrompt);
					jq("#agencySellTicketPrompt").show();
					jq("#agencySellTicketList").html(generateAgencySellTicketsList(agencySellTicketTempList)).listview("refresh");
					jq("#agency-iscroll-pullup").hide();
					jq("#agency-iscroll-pulldown").hide();
				}else {//代售点为空
					mor.ticket.util.alertMessage("没有查询到火车票代售点。");
					jq("agency-iscroll-pullup").hide();
				}			
			} else {//查询订单失败
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}	
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	var agencySellTicketsListTemplate =
		"{{~it :agencySellTicket:index}}" +
		"<li data-index='{{=index}}'><a>" +
			"<div class='ui-grid-a'>" +
				"<div class='ui-block-a'><span>{{=index + 1}}</span></div>" +
				"<div class='ui-block-b'>{{=agencySellTicket.agency_name}}</div>" +
			"</div>" +
			"<div class='ui-grid-b'>" +
				"<div class='ui-block-a'>{{=agencySellTicket.address}}</div>" +
			"</div>" +
			"</a>" +
		"</li>" +
		"{{~}}";
	var generateAgencySellTicketsList = doT.template(agencySellTicketsListTemplate);
	
	function registerAgencySellTicketListItemClickHandler(){
		jq("#agencySellTicketList").off().on("tap", "li", function(e){
			e.stopImmediatePropagation();
			mor.ticket.currentQueryObject.agencySellTicket = mor.ticket.currentQueryObject.agencySellTicketList[jq(this).index()];
			mor.ticket.util.changePage("agencySellTicketDetail.html");			
			return false;
		});
	}
})();