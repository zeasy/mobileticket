/**
 * Created by easy on 15/11/3.
 */

var React = require('react-native');
var Components = require('Components');
var {
    AppRegistry,
    StyleSheet,
    NavigatorIOS,
    AlertIOS,
    View
    } = React;

var {
    Controller
    } = Components;

class TestController extends Controller {
    constructor() {
        super();
        this.route.title = 'TestController';
        this.route.rightButtonTitle = 'Done';
        this.route.onRightButtonPress = ()=> {
            AlertIOS.alert('a');
        }
    }
}

exports = module.exports = TestController;
