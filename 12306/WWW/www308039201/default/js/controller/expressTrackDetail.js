
/* JavaScript content from js/controller/expressTrackDetail.js in folder common */
(function() {
	jq("#expressTrackDetailView").live("pageinit", function() {
	});
	jq("#expressTrackDetailView").live("pagebeforeshow", function() {
		mor.ticket.util.initAppVersionInfo();
		var user = mor.ticket.loginUser;
		if(user.isAuthenticated === "Y"){
			jq("#expressTrack_no").html("运单号:"+mor.ticket.expressTrackDetail.expressTrack_no);
			jq("#expressTrackDetail").html(generateExpressTrackDetailList(mor.ticket.expressTrackDetail.expressTrackDetails));
			jq("#expressTrackDetailView .ui-content").iscrollview("refresh");
			jq("#expressTrackDetailViewBackBtn").bind("tap",function(){
				mor.ticket.util.changePage("expressStatusDetail.html");
				return false;
			});
		}else{
			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
			} else {
				
			}
		}
	});
	"<div class='pencil-input' style='font-size:14px;color:#333333;width:300px;'></div>"
	var expressTrackDetailListTemplate =
		"{{~it :expressTrackDetail:index}}" +
			"{{ if(index == 0){ }}" +
			"<li style='color:blue;'><fieldset>" +
					"<div data-role='fieldcontain' class='departshortnoline ui-field-contain' style='font-size:14px;'>{{=expressTrackDetail.deliver_time}}<div style='color:red;float:right;margin-right:10px;'>最新</div></div>" +
			"{{ }else{ }}" +
			"<li><fieldset>" +
					"<div data-role='fieldcontain' class='departshortnoline ui-field-contain' style='font-size:14px;'>{{=expressTrackDetail.deliver_time}}</div>" +
			"{{ } }}" +
						"<div class='pencil-input' style='font-size:14px;width:300px;margin:5px 10px 5px 10px;'>" +
							"<span>{{=expressTrackDetail.deliver_comment}}</span>" +
						"</div>" +
					"</div>" +
				"</fieldset>" +
			"</li>" +
		"{{~}}";
	var generateExpressTrackDetailList = doT.template(expressTrackDetailListTemplate);
})();
	