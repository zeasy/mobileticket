
/* JavaScript content from js/model/dataModel.js in folder common */
(function() {

	jq.extendModule("mor.ticket.registInfo", {
		"user_name" : "",
		"succ_flag" : "",
		"error_msg" : "",
		"name" : "",
		"id_type_code" : "",
		"country_code" : "",
		"address" : "",
		"user_type" : "",
		"id_no" : "",
		"mobile_no" : "",
		"phone_no" : "",
		"email" : "",
		"born_date" : "",
		"ivr_passwd" : "",
		"postalcode" : "",
		"pwd_question" : "",
		"sex_code" : "",
		"student_province_code" : "",
		"student_school_code" : "",
		"student_department" : "",
		"student_school_class" : "",
		"student_student_no" : "",
		"student_enter_year" : "",
		"student_school_system" : "",
		"student_from_station_code" : "",
		"student_to_station_code" : "",
		"student_card_no" : "",
		"pass_code" : ""
	});
	jq.extendModule("mor.ticket.loginUser", {
		"username" : "",// need to change to loginName to prevent
						// confusing,could be email or account name
		"password" : "",
		"isKeepUserPW" : true,
		"isAuthenticated" : "N",
		"id_type" : "",
		"id_no " : "",
		"email " : "",
		"mobile_no " : "",
		"user_status" : "",
		"check_id_flag" : "",
		"display_control_flag" : "",
		"user_type" : "",
		"realName" : "", // real name
		"accountName" : "", // 12306 account name
		"activeUser" : "Y",
		"start_receive" : "N",//是否启用手机号核验,Y-启用,N-不启用
		"is_receive" : "N",//手机号核验,Y-核验通过,N-既有用户未核验,X-新注册用户未核验
		"pic_control_flag" : ""//照片比对标志
	});

	jq.extendModule("mor.ticket.userInfo", {
		"user_name" : "",
		"succ_flag" : "",
		"error_msg" : "",
		"name" : "",
		"id_type_code" : "",
		"country_code" : "",
		"address" : "",
		"user_type" : "",
		"id_no" : "",
		"user_status" : "",
		"mobile_no" : "",
		"phone_no" : "",
		"email" : "",
		"born_date" : "",
		"ivr_passwd" : "",
		"postalcode" : "",
		"pwd_question" : "",
		"sex_code" : "",
		"student_province_code" : "",
		"student_school_code" : "",
		"student_department" : "",
		"student_school_class" : "",
		"student_student_no" : "",
		"student_enter_year" : "",
		"student_school_system" : "",
		"student_from_station_code" : "",
		"student_to_station_code" : "",
		"student_card_no" : "",
		"pass_code" : ""
	});

	jq.extendModule("mor.ticket.passengerDetail", {
		"name" : "",
		"old_name" : "",
		"sex_code" : "",
		"born_date" : "",
		"country_code" : "",
		"card_type" : "",
		"old_card_type" : "",
		"card_name" : "",
		"card_no" : "",
		"old_card_no" : "",
		"passenger_type" : "",
		"mobile_no" : "",
		"phone_no" : "",
		"email" : "",
		"address" : "",
		"postalcode" : "",
		"province_code" : "",
		"school_code" : "",
		"department" : "",
		"school_class" : "",
		"student_no" : "",
		"enter_year" : "",
		"school_system" : "",
		"preference_from_station_code" : "",
		"preference_to_station_code" : "",
		"preference_card_no" : "",
		"total_times" :0
	});
	jq.extendModule("mor.ticket.DcSeatTypeList", {
	      result : []
		});

	jq.extendModule("mor.ticket.passenger", {
		result : ""
	});

	jq.extendModule("mor.ticket.stateList", {
      result : []
	});
	jq.extendModule("mor.ticket.CityAllMap", {
	      key : [],
	      values : []
		});
	jq.extendModule("mor.ticket.seat_Type", {
      result : []
	});
	
	jq.extendModule("mor.ticket.yplist", {
      result : []
	});
	
	// 存储来源路径
	jq.extendModule("mor.ticket.history", {
       "url" : "",
       reservePeriodMonth : []
	});
	// 存储票类型
	jq.extendModule("mor.ticket.ticketTypeList", {
      result : []
	});
	jq.extendModule("mor.ticket.reservePeriodList",{
		result : []
	});
	// 存储席
	jq.extendModule("mor.ticket.seat_Type", {
       result : []
	});


	// 存储席
	jq.extendModule("mor.ticket.seat_Type_index", {
       result : []
	});
	// 旅行计划订单赋值
	jq.extendModule("mor.ticket.travel_plan", {
       result : []
	});



	jq.extendModule("mor.ticket.leftTicketQuery", {
		from_station_telecode : "",
		to_station_telecode : "",
		seat_Type : "",
		purpose_codes : '00',
		firstRegist : true,
		seatBack_Type : "",
		train_date : "", // YYYY-MM-DD
		train_date_type : 0,
		time_period : '0',
		station_train_code : "",
		train_headers : "",
		train_flag : "",
		/* back ticket */
		from_station_telecode_back : "",
		to_station_telecode_back : "",
		train_date_back : "",
		time_period_back : '0',
		station_train_code_back : "",
		result : []
	});

	jq.extendModule("mor.ticket.searchResultScreen", {
		trainHeaders : [],
		from_station_telecode : [],
		from_station_checked : [],
		to_station_telecode : [],
		to_station_checked :[],
		seat_type_list : [],
		seat_type_checked : [],
		seat_type_enabled : ["X"],
		begin_station_flag : false,
		end_station_flag : false,
		start_time : "0024",
		arrive_time : "0024",
		only_ticket : false,
		chooseQueryFlag : false,
		exitFlage : false,
		result : [],
		setQueryCondition : function(){
			var results = mor.ticket.leftTicketQuery.result;
			for ( var i = 0; i < results.length; i++) {
				
				this.from_station_telecode.push(results[i].from_station_telecode+","+results[i].from_station_name);
				this.to_station_telecode.push(results[i].to_station_telecode+","+results[i].to_station_name);
				
				for ( var s = 0, j = results[i].yplist.length; s < j; s++) {
					if (results[i].yplist[s].type == "无座") {
						results[i].yplist[s].type_id = "W";
					}
					if(i == 0){
						this.seat_type_list.push(results[i].yplist[s].type_id);
						if (results[i].yplist[s].num > 0) {
							this.seat_type_enabled.push(results[i].yplist[s].type_id);
						}
					}
					else{
						if(!mor.ticket.util.indexOf(this.seat_type_list,results[i].yplist[s].type_id)){
							this.seat_type_list.push(results[i].yplist[s].type_id);
						}
						if (results[i].yplist[s].num > 0 && !mor.ticket.util.indexOf(this.seat_type_enabled,results[i].yplist[s].type_id)) {
							this.seat_type_enabled.push(results[i].yplist[s].type_id);
						}
					}	
				}
			}
			this.from_station_telecode.sort();
			this.to_station_telecode.sort();
			this.from_station_telecode = mor.ticket.util.uniq(this.from_station_telecode);
			this.to_station_telecode = mor.ticket.util.uniq(this.to_station_telecode);
			this.seat_type_list = mor.ticket.util.uniq(this.seat_type_list);
			this.seat_type_list = mor.ticket.util.sortSeatTypeOrder(this.seat_type_list);
		},
		filterResult :function(){
			var results = mor.ticket.leftTicketQuery.result;
			var util = mor.ticket.util;
			var startTime0 = parseInt(this.start_time.substring(0, 2));
			var startTime1 = parseInt(this.start_time.substring(2));
			if(startTime1 > 0){
				startTime1 = startTime1 - 1;
			}
			var arriveTime0 = parseInt(this.arrive_time.substring(0, 2));
			var arriveTime1 = parseInt(this.arrive_time.substring(2));
			if(arriveTime1 > 0){
				arriveTime1 = arriveTime1 - 1;
			}
			var only_ticket = this.only_ticket;
			var that = mor.ticket.searchResultScreen;
			this.result = jq
			.grep(
					results,
					function(ticket, index) {
						var fromStation = ticket.from_station_telecode;
						var toStation = ticket.to_station_telecode;
						var trainHeader = ticket.station_train_code.substring(0, 1);
						var yplist = ticket.yplist;
						if (trainHeader == "G" || trainHeader == "C") {
							trainHeader = "D";
						}else if (/^[1-9]{1}$/.test(trainHeader)) {
							trainHeader = "QT";
						}
						var startTime = parseInt(ticket.start_time.substring(0, 2));
						var arriveTime = parseInt(ticket.arrive_time.substring(0, 2));
						if(util.indexOf(that.trainHeaders,"QB")){
							return util.indexOf(that.from_station_checked,fromStation) 
								&& util.indexOf(that.to_station_checked,toStation) 
								&& util.seatTypeForYp(that.seat_type_checked,yplist,only_ticket) 
								&& util.filterBeginAndEndStation(ticket,that.begin_station_flag,that.end_station_flag)
								&& (startTime0 <= startTime && startTime <= startTime1)
								&& (arriveTime0 <= arriveTime && arriveTime <= arriveTime1); 
						}else{
							return util.indexOf(that.trainHeaders,trainHeader)
							&& util.indexOf(that.from_station_checked,fromStation) 
							&& util.indexOf(that.to_station_checked,toStation) 
							&& util.seatTypeForYp(that.seat_type_checked,yplist,only_ticket) 
							&& util.filterBeginAndEndStation(ticket,that.begin_station_flag,that.end_station_flag)
							&& (startTime0 <= startTime && startTime <= startTime1)
							&& (arriveTime0 <= arriveTime && arriveTime <= arriveTime1); 
						}
						
					});
						
		},
		clear : function (){
			mor.ticket.searchResultScreen.chooseQueryFlag = false;
			mor.ticket.searchResultScreen.trainHeaders = [];
			mor.ticket.searchResultScreen.from_station_checked = [];
			mor.ticket.searchResultScreen.to_station_checked = [];
			mor.ticket.searchResultScreen.seat_type_checked = [];
			mor.ticket.searchResultScreen.seat_type_enabled = [];
			mor.ticket.searchResultScreen.begin_station_flag = false;
			mor.ticket.searchResultScreen.end_station_flag = false;
			mor.ticket.searchResultScreen.only_ticket = false;
			mor.ticket.searchResultScreen.from_station_telecode = [];
			mor.ticket.searchResultScreen.to_station_telecode = [];
			mor.ticket.searchResultScreen.seat_type_list = [];
			mor.ticket.searchResultScreen.result = [];
			mor.ticket.searchResultScreen.start_time = "0024";
			mor.ticket.searchResultScreen.arrive_time = "0024";
		},
		isHasFilterCondition : function(){
			var query = mor.ticket.searchResultScreen;
			if(query.trainHeaders.length > 0 && !mor.ticket.util.indexOf(query.trainHeaders,"QB")){
				return true;
			}
			if(query.from_station_checked.length > 0 || query.to_station_checked.length > 0
					|| query.seat_type_checked.length > 0 || query.begin_station_flag != false
					|| query.end_station_flag != false || query.only_ticket != false){
				return true;
			}
			if(query.start_time != "0024" || query.arrive_time != "0024"){
				return true;
			}
			return false;
		}
		
	});
	jq.extendModule("mor.ticket.currentTicket", {
		arrive_time : "",
		end_station_telecode : "",
		from_station_telecode : "",
		lishi : "",
		start_station_telecode : "",
		start_time : "",
		train_date : "", // YYYY-MM-DD
		train_date_back : "",
		station_train_code : "",
		to_station_telecode : "",
		train_class_name : "",
		is_support_card : "",
		yp_info : "",
		yplist : []
	});

	jq.extendModule("mor.ticket.currentPassenger", {
		user_name : "", // real name
		id_type : "",
		id_no : "",
		mobile_no : "",
		ticket_type : "",
		seat_type : ""
	});

	jq.extendModule("mor.ticket.passengerList", {
		seat_type:"",
		id_type:"",
		user_type:"",
		ticket_type:"",
		id_no:"",
		user_name:"",
		p_str:"",
		mobile_no:""			
	});
    //送票地址
	jq.extendModule("mor.ticket.sendTicketAddressList", {
		original_address_name:"",
		original_address_province:"",
		original_address_city:"",
		original_address_county:"",
		original_addressee_town:"",
		original_detail_address:"",
		original_mobile_no:"",
		default_address : "",
		addressListLength : 0,
		length : 0,
		checked : 0
	});
	
	// ticket order after confirm
	jq.extendModule("mor.ticket.confirmTicket", {
		sequence_no : "",
		batch_no : "",
		coach_name : "",
		coach_no : "",
		distance : "",
		limit_time : "",
		lose_time : "",
		passenger_id_no : "",
		passenger_id_type_code : "",
		passenger_name : "",
		pay_limit_time : "",
		pay_mode_code : "",
		reserve_time : "",
		seat_name : "",
		seat_no : "",
		seat_type_code : "",
		ticket_price : "",
		ticket_type_code : "",
		station_train_code : "",
		ticket_flag : false
	// indicate if back ticket for round trip
	});

	/*
	 * Go to js/controlller/orderManager for detail
	 * jq.extendModule("mor.ticket.orderManager", {
	 * 
	 * });
	 */

	jq.extendModule("mor.ticket.payment", {
		'interfaceName' : '',
		'interfaceVersion' : '',
		'tranData' : '',
		'merSignMsg' : '',
		'appId' : '',
		'transType' : '',
		'epayurl' : '',
		'batch_nos' : []
	});

	jq.extendModule("mor.ticket.passengersCache", {
		passengers : [],
		sortPassengers : function() {
			var passengers = mor.ticket.passengersCache.passengers;
			var newpassengers = [];
			var user_passenger = {};
			var user = mor.ticket.loginUser;
			
			// 添加拼音字段
			
		//	WL.Logger.debug("Successfully  1");
			
			user_passenger["user_name"] = user.realName;
			user_passenger["id_type"] = user.id_type;
			user_passenger["id_no"] = user.id_no;
			user_passenger["user_type"] = user.user_type;
			user_passenger["user_nameSM"] = jq("#py").val(user.realName).toPinyin();
			user_passenger["total_times"] = '00';
			if(user.display_control_flag=='1' || user.display_control_flag=='3' || user.display_control_flag=='5'){
				user_passenger["other_totaltimes"] = '00';
			}else{
				user_passenger["other_totaltimes"] = '01';
			}
			if(user.display_control_flag=='1'){
				user_passenger["two_totaltimes"] = '00';
			}else{
				user_passenger["two_totaltimes"] = '01';
			}
		
			
			for ( var i = 0; i < passengers.length; i++) {
				if(passengers[i] != null){
				newpassengers[i] = {};
				newpassengers[i]["checked"]   = 0;
				newpassengers[i]["seat"]      = "";
				newpassengers[i]["user_name"] = passengers[i].user_name;
				newpassengers[i]["id_type"] = passengers[i].id_type;
				newpassengers[i]["id_no"] = passengers[i].id_no;
				newpassengers[i]["is_smoker"] = passengers[i].is_smoker;
				newpassengers[i]["mobile_no"] = passengers[i].mobile_no;
				newpassengers[i]["user_type"] = passengers[i].user_type;
				newpassengers[i]["total_times"] = passengers[i].total_times;
				newpassengers[i]["p_str"] = passengers[i].p_str;
				newpassengers[i]["two_totaltimes"] = passengers[i].two_totaltimes;
				newpassengers[i]["other_totaltimes"] = passengers[i].other_totaltimes;
				newpassengers[i]["user_nameSM"] = jq("#py").val(
						passengers[i].user_name).toPinyin();
				}
			}
			
		//	WL.Logger.debug("Successfully  2");
			// 取出当前用户
			
			for ( var i = 0; i < newpassengers.length; i++) {
				if (newpassengers[i].user_name == user.realName
						&& newpassengers[i].id_type == user.id_type
						&& newpassengers[i].id_no == user.id_no 
						&& newpassengers[i].user_type == user.user_type) {
					user_passenger = newpassengers[i];
					newpassengers.splice(i, 1);
					break;
				}
			}
		//	WL.Logger.debug("Successfully  3");
			// 对newpassengers按拼音排序
			mor.ticket.util.sortPassengers(newpassengers);
			// 重组数据 当前用放第一位
		//	WL.Logger.debug("Successfully  4");
			passengers = [];
			for ( var i = 0; i < newpassengers.length + 1; i++) {
				if (i == 0) {
					passengers[0] = user_passenger;
				} else {
					passengers[i] = newpassengers[i - 1];
				}
			}
			mor.ticket.passengersCache.passengers = passengers;
		
		return mor.ticket.passengersCache.passengers;
		
		}
	});

	jq.extendModule("mor.cache.seatTypeMap", {});

	jq.extendModule("mor.cache.ticketTypeMap", {});

	jq.extendModule("mor.cache.idTypeMap", {});

	jq.extendModule("mor.cache.stationMap", {});

	jq.extendModule("mor.cache.countryMap", {});

	jq.extendModule("mor.cache.provinceMap", {});

	jq.extendModule("mor.cache.universityMap", {});

	jq.extendModule("mor.cache.cityMap", {});

	jq.extendModule("mor.ticket.common", {
		'baseDTO.os_type' : "a",
		'baseDTO.device_no' : "abcdaabbccdd",
		'baseDTO.mobile_no' : "123444",
		'baseDTO.time_str' : "20120430",
		'baseDTO.time_offset' : 0,
		'baseDTO.check_code' : "20120430",
		'baseDTO.version_no' : "1.1",
		'baseDTO.app_need_update' : "false",
		'baseDTO.app_download_url' : "",
		'baseDTO.version_no_new' : ""
	});

	jq.extendModule("mor.ticket.cache", {
		needSync : "N",
		syncList : "",
		syncVersionList : "",
		station_version : "Nov 27 2014 10:32AM",
		hotStation_version : 1.9,
		city_version : "Dec 11 2013  5:40PM",
		expressAddress_version : "Sep 02 2014  3:27PM",
		university_version :"Dec 11 2013  8:33AM",
		adver_version : "1.0",
		stations : [],
		seats : [],
		getStationNameByCode : function(id) {
			return mor.cache.stationMap[id];
		},
		getSeatTypeByCode : function(id) {
			return mor.cache.seatTypeMap[id];
		},
		getSeatTypeRateByCode : function(id) {
			return mor.cache.seatTypeRateMap[id];
		},
		getSeatTypeByCode : function(id) {
			return mor.cache.seatTypeMap[id];
		},
		passengers : []
	});

	jq.extendModule("mor.ticket.viewControl", {
		tab1_cur_page : "",
		tab2_cur_page : "",
		tab3_cur_page : "",
		tab4_cur_page : "",
		current_tab : "",

		// 登录超时页面记录
		session_out_page : "",
		// show busy indicator
		show_busy : true,
		// 订票类型：单程"dc"、往程"wc"、返程"fc"、改签"gc"
		bookMode : "dc",
		// 查询页面，查询类型：0今日订单，1表现7日内订单，2表示高级查询订单
		queryFinishedOrderType : "",

		// 找回密码的方式 email表示通过邮件 passwd表示通过密码提示
		findPwdMode : "email",

		isCityGo : false,
		isModifyPassenger : false,
		//判断新增和修改送票地址
		isModifyTicketAddress : false,
		// 是否需要重新加载dom节点中的selectPassenger页面
		isNeedRefreshSelectPassengers : false,
		// 订单查询页面 刷新
		isNeedRefreshUnfinishedOrder : false,
		// 已完成订单详情页面 退票 按钮 index
		cancelTicketIndex : 0,
		// 支付成功页面状态:true 为支付完成，false 为退票完成
		isPayfinishMode : true,

		// 每次显示的第一个车站的索引
		station_num : 0,
		// 车站显示总数
		station_show_num : 30,
		// 滑动后保留车站数
		station_keep_num : 10,
		// 搜索车站页面使用导航功能
		isStationNevClick : false,
		// 上拉下拉标记
		station_is_up : true,
		// 清除搜索框标记
		station_searchClean : false,
		// 显示出的车站
		station_show : [],

		// 每次显示第一个城市的索引
		city_num : -1,
		// 未完成订单 排队查询tourFlag
		unFinishedOrderTourFlag : "",
		// 是否需要重新请求数据
		isNeedRequest : true,
		// 投保成功标识
		succInsuranceFlag : false,
		// adapter
		adapterUsed : "CARSMobileServiceAdapterV2P1",
		//regist flag
		registFlag : "",		
		// append启售时间提示信息
		tipsFlag : true
	});

	jq.extendModule("mor.ticket.views.selectStation", {
		isFromStation : true,
		isTicketPriceQueryFromStation : true,
		isSearchResultScreen : true
	});
	jq.extendModule("mor.ticket.views.selectTrain", {
		isFromTrain : true
	});
	jq.extendModule("mor.ticket.views.trainQueryCode", {
		trainCode : ""
	});
	jq
			.extendModule(
					"mor.ticket.queryOrder",
					{
						queryOrderList : [],
						currentQueryOrder : {},
						currentUnfinishOrderIndex : 0,
						originPaidOrderList : [],
						setUnfinishedOrderList : function(orderList) {
							this.queryOrderList = orderList;
							//this.setCurrentUnfinishedOrders(0);
							return this.queryOrderList;
						},
						setCurrentUnfinishedOrders : function(index) {
							this.currentQueryOrder = this.queryOrderList[index];
						},
						getCurrentUnfinishedOrder : function() {
							return this.currentQueryOrder;
						},
						updateCurrentQueryOrder : function(orderList) {
							this.currentQueryOrder = orderList;
						},

						hasChangeTicket : function() {
							var orders = this.queryOrderList;
							var originOrderList = this.originPaidOrderList;
							var originList = [];
							for ( var i = 0; i < orders.length; i++) {
								var length = orders[i].myTicketList.length;
								for ( var j = 0; j < length; j++) {
									if (orders[i].myTicketList[j].ticket_status_code == 'j' || orders[i].myTicketList[j].ticket_status_code == 's') {
										originList = this
												.splicePaidOriginTicketList(
														orders[i].myTicketList,
														i);
										break;
									}
								}
								originOrderList.push(originList);
							}
						},

						splicePaidOriginTicketList : function(ticketList, index) {
							var length = ticketList.length;
							var originOrderList = [];
							for ( var i = 0; i < length; i++) {
								if (ticketList[i].ticket_status_code == 'e' || ticketList[i].ticket_status_code == 'q') {// 状态为“改签中和变更到站中”则放到originOrderList
									var ticket = ticketList.slice(i, i + 1)[0];
									originOrderList.push(ticket);
									ticketList.splice(i, 1);
									i--;
									length--;
								} else if (ticketList[i].ticket_status_code != 'j' && ticketList[i].ticket_status_code != 's') {// 状态不为“改签待支付和变更到站待支付”或者
																						// “改签中和变更到站中”或者则删除该ticket
									ticketList.splice(i, 1);
									i--;
									length--;
								}
							}
							return originOrderList;
						},

						getOrderPrice : function() {
							var unFinishedOrder = this.currentQueryOrder;
							var myTicketList = unFinishedOrder.myTicketList;
							var newPrice = 0;
							for ( var i = 0; i < myTicketList.length; i++) {
								newPrice += parseFloat(myTicketList[i].ticket_price);
							}
							unFinishedOrder.newTotalPrice = newPrice.toFixed(2);// 得到现在票价总金额
							var originPaidOrderList = mor.ticket.queryOrder.originPaidOrderList[this.currentUnfinishOrderIndex];
							var originPrice = 0;
							for ( var i = 0; i < originPaidOrderList.length; i++) {
								originPrice += parseFloat(originPaidOrderList[i].ticket_price);
							}
							unFinishedOrder.originTotalPrice = originPrice
									.toFixed(2);// 得到原改签票票价总金额
							
							//得到原票开车时间
							var originTrainDate = originPaidOrderList[0].train_date+""+originPaidOrderList[0].start_time+"00";
							unFinishedOrder.originTrainDate = originTrainDate;
							// 得到应返回金额
							unFinishedOrder.returnTotalPrice = (unFinishedOrder.originTotalPrice - unFinishedOrder.newTotalPrice)
									.toFixed(2);
						},

						finishedOrderList : [],
						currentFinishQueryOrder : [],
						changeTicketOrderList : [],
						setFinishedOrderList : function(orderList) {
							this.finishedOrderList = orderList;
							return this.finishedOrderList;
						},
						pushFinishedOrderList : function(orderList) {
							for ( var i = 0; i < orderList.length; i++) {
								this.finishedOrderList.push(orderList[i]);
							}
							return this.finishedOrderList;
						},
						// 根据提供的 sequence_no 替换最新的 order 信息.
						replaceOrderBySequenceNo: function(no, order) {
							var orderList = this.finishedOrderList;
							for ( var i = 0; i < orderList.length; i++) {
								var orderInfo = orderList[i];
								if (orderInfo.sequence_no === no) {
									this.finishedOrderList.splice(i, 1, order);
									this.setCurrentFinishedOrders(i);
									return;
								}
							}
						},
						spliceChangingTicketList : function() {
							/*
							 * var orders = this.finishedOrderList; for(var
							 * i=0;i<orders.length;i++){ for(var j=0; j<orders[i].myTicketList.length;j++){
							 * if(orders[i].myTicketList[j].ticket_status_code ==
							 * 'j'){ orders[i].myTicketList.splice(j,1); } } }
							 */
						},
						setCurrentFinishedOrders : function(index) {
							this.currentFinishQueryOrder = this.finishedOrderList[index];
						},
						getCurrentFinishedOrders : function() {
							return this.currentFinishQueryOrder;
						},
						getCurrentFinishedOrdersMyTicketList : function() {
							return this.currentFinishQueryOrder.myTicketList;
						},

						initChangeTicketOrderList : function() {
							this.changeTicketOrderList = [];
						},
						setChangeTicketOrderList : function(orderList) {
							this.changeTicketOrderList.push(orderList);
						},
						
						// 获取改签的联系人列表.
						getGcPassengers: function() {
							var passengerList = [];
							jq.each(this.changeTicketOrderList, function(i, orderTicket) {
								var passenger = {};
								passenger.id_no = orderTicket.passenger_id_no;
								passenger.id_type = orderTicket.passenger_id_type_code;
								passenger.mobile_no = '';
								passenger.user_name = orderTicket.passenger_name;
								passenger.ticket_type = orderTicket.ticket_type_code;
								passenger.seat_type = orderTicket.seat_type_code;
								passengerList.push(passenger);
							});
							return passengerList;
						},
						cancelTicket : {},
						setCancelTicketInfo : function(result) {
							this.cancelTicket = this.currentFinishQueryOrder.myTicketList[mor.ticket.viewControl.cancelTicketIndex];
							var info = this.cancelTicket;
							info.return_cost = result.return_cost;
							//申请退票手续费
							info.return_cost_price = result.return_cost_price;
							info.rate = result.rate;
							info.pay_limit_time = result.pay_limit_time;
							info.realize_time_char = result.realize_time_char;
						},
						getCancelTicketInfo : function() {
							return this.cancelTicket;
						}
					});

	jq.extendModule("mor.queryOrder.views.advanceQuery", {
		fromDate : "",
		toDate : "",
		trainFromDate : "",
		trainToDate : "",
		trainCode : "",
		passengerName : "",
		passengerId : "",
		flag : "",
		isSelectTrainDate : false,
		isFromDateInputShow : true,
		queryOrderTypeFlag : true
	// 查询已完成订单按日期选择标记
	});

	jq.extendModule("mor.ticket.poll", {
		intervalId : "",
		endTime : "",
		timer : ""
	});

	jq.extendModule("mor.ticket.paytimer", {
		intervalId : "",
		endTime : ""
	});
	//车站车次查询-->车站
	jq.extendModule("mor.ticket.stationTrainQuery", {
		queryStation : "",
		trainDate : "",
		startTimes : "",
		stopTimes : "",
		trainType : "",
		trainHeaders : [],
		stationTrainQueryResult:[]
	});
	//searchResultList //searchResultList 判断余票和票价的选中状态,1为查询出来的状态，2为点击item返回选中的状态
	jq.extendModule("mor.ticket.searchResultList", {
		leftOrPriceFlag : "",
		Results:[]	
	});
	//票价查询
	jq.extendModule("mor.ticket.ticketPriceQuery", {
		from_station_telecode : "",
		to_station_telecode : "",
		train_date : "",
		start_times:"",
		train_headers:"",
		seat_Type : "",
		purpose_codes : ""
	});
	//起售时间查询--选择车站
	jq.extendModule("mor.ticket.startSellTicketTimeQuery", {
		start_sell_ticket_time_station_telecode : ""
	});
	// 代售点查询
	jq.extendModule("mor.ticket.views.agencySellTicketQuery", {
		province : "",
		city : "",
		county : "",
		agency_name : ""
	});
	// 当前查询对象
	jq.extendModule("mor.ticket.currentQueryObject", {
	});
	// 页面跳转
	jq.extendModule("mor.ticket.currentPagePath", {
		fromPath : "",
		type : true,
		show : true,
		pickYears : true,
		calenderDate : "",
		advanceQueryDate : "",
		defaultDateValue : []
	});
	jq.extendModule("mor.ticket.passengerTrainlList",{
		passengerTrainList : []
	});
	jq.extendModule("mor.ticket.datebox.calbox",{
	});
	
	jq.extendModule("mor.ticket.barcodeScanResult",{
		resultCode : "",
		format : "",
		userCancelled : false,
		whichPlatform : ""
	});
	
	jq.extendModule("mor.ticket.dw_ticket",{
		station_train_code : [],
		dw_flag : false
	});
	//改签手续费
	jq.extendModule("mor.ticket.returnCost",{
		returnCost : "",
		returnFact : "",
		returnRate : "0",
		returnCostPrice : ""
	});
})();