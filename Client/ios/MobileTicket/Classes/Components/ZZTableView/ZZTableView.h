//
//  ZZTableView.h
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RCTEventDispatcher.h>
#import <RCTComponent.h>
#import "ZZView.h"

@interface ZZTableView : ZZView <RCTComponent>

@property (nonatomic, assign) UITableViewStyle tableViewStyle;

@property (nonatomic, strong) NSDictionary *dataSource;
@property (nonatomic, strong) NSDictionary *delegate;

@property (nonatomic, strong) RCTDirectEventBlock onReuseCell;
@property (nonatomic, strong) RCTDirectEventBlock onReuseHeaderFooterView;

@property (nonatomic, strong) RCTDirectEventBlock willDisplayCell;
@property (nonatomic, strong) RCTDirectEventBlock onDidSelectedRow;

@property (nonatomic, strong) UITableView *tableView;



@property (nonatomic, strong) RCTDirectEventBlock onDidSelectRowAtIndexPath;
- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher;

@end