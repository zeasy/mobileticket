/**
 * @providesModule ZZRandCodeView
 * Created by easy on 16/1/13.
 */
'use strict';

var React = require('react-native');
let ZZNetwork = require('ZZNetwork');
let FlatUI = require('ZZFlatUI');

var {
    View,
    Text,
    Image,
    StyleSheet,
    ActivityIndicatorIOS,
    Touchable,
    TouchableOpacity,
    NativeMethodsMixin
    } = React;

var ZZRandCodeView = React.createClass({
    mixins: [ Touchable.Mixin, NativeMethodsMixin],
    //api
    refresh : function() {
        this.points = [];
        this.setState({isRefresh: true,points:[],randCodeBase64Url:''});

        this.requestRandCode();
    },

    getPoints : function() {
        let points = [];

        for (let k in this.state.points) {
            let v = this.state.points[k];
            points.push({x:v.x,y:v.y - 30});
        }

        return points;
    },

    getDefaultProps: function() {
        return {
        };
    },

    getInitialState: function() {
        return {
            ...this.touchableGetInitialState(),
            randCodeBase64Url: '',
            isRefresh : false,
            points : []
        };
    },



    touchableHandlePress: function(e: Event) {
        if (this.state.isRefresh) {
            return;
        }

        let points = this.state.points;
        let point = {x:e.nativeEvent.locationX,y:e.nativeEvent.locationY};
        points.push(point);
        this.setState({points});
    },

    randCodeUrl: function () {
        return 'https://kyfw.12306.cn/otn/passcodeNew/getPassCodeNew?module=login&rand=sjrand&' + Math.random();
    },

    requestRandCode: function () {
        ZZNetwork.request({url:this.randCodeUrl(),format:'data'}).then((base64)=>{
            let uri = 'data:image/png;base64,' + base64;
            this.setState({isRefresh:false,randCodeBase64Url: uri});
        }).catch((err) => {
            this.setState({isRefresh:false});
        });
    },

    onClickPoint : function(x,y) {
        let points = this.state.points;
        let newPts = [];
        for (let k in points) {
            let pt = points[k];
            if (pt.x != x && pt.y != y) {
                newPts.push(pt);
            }
        }
        this.setState({points:newPts});
    },

    renderRefreshButton : function() {
        return (
            <TouchableOpacity style={styles.refreshButton} onPress={this.refresh} activeOpacity = {0.65} >
            <Text style={styles.submitText}>刷 新</Text>
        </TouchableOpacity>);
    },

    renderRefreshIndicator : function() {
        return this.state.isRefresh ? (
            <View style={styles.refreshView}>
            <View style={{flex:1}}>
                <ActivityIndicatorIOS style={{marginTop:70,alignSelf:'center'}}/>
                <Text style={{alignSelf:'center',marginTop:8,fontSize:14}}>验证码加载中</Text>
            </View>
                </View>
                ) : (<View/>);
    },

    renderImage : function() {
        return (<Image style={styles.randCodeImage} source={{uri:this.state.randCodeBase64Url}}/>);
    },

    renderPoints : function() {
        let width = 30;
        let height = 30;

        let childrens = [];

        for (let key in this.state.points) {
            let point = this.state.points[key];
            if (point.x < 0 || point.y < 30) {
                continue;
            }
            let left = point.x - width / 2;
            let top = point.y - height / 2;

            let pointView = <TouchableOpacity style={{left:left,top:top,width:width,height:height,
                                                        backgroundColor:FlatUI.carrot,position:'absolute',
                                                        borderRadius:width/2,borderWidth:2,borderColor:FlatUI.peterRiver}}
                                              activeOpacity={1}
                                              key={"point_"+point.x+"_"+point.y}
                                              onPress={()=>this.onClickPoint(point.x,point.y)}
            />;

            childrens.push(pointView);
        }

        let view = React.createElement(View,{style:styles.pointContainer},childrens);
        return view;
    },

    render: function() {
        return (<View
            onStartShouldSetResponder={this.touchableHandleStartShouldSetResponder}
            onResponderTerminationRequest={this.touchableHandleResponderTerminationRequest}
            onResponderGrant={this.touchableHandleResponderGrant}
            onResponderMove={this.touchableHandleResponderMove}
            onResponderRelease={this.touchableHandleResponderRelease}
            onResponderTerminate={this.touchableHandleResponderTerminate}
            {...this.props}>
                <View style={styles.container}>

                    {this.renderImage()}
                    {this.renderPoints()}
                    {this.renderRefreshButton()}
                    {this.renderRefreshIndicator()}
                </View>
        </View>);
    }
});

/**
 *
 *
 */

let styles = StyleSheet.create({
    container : {
        flex : 1
    },
    randCodeImage: {
        width: 293,
        height: 190,
        alignSelf: 'center',
    },
    pointContainer : {
        position:'absolute',top:0,left:0,bottom:0,right:0
    },

    refreshView : {
        position:'absolute',top:0,width:293,height:190
    },
    refreshIndicator : {
        position:'absolute',
        top:0,
    },

    refreshButton: {
        position:'absolute',
        borderRadius: 3,
        top: 5,
        right: 0,

        backgroundColor: FlatUI.carrot,
        height: 20,
        width : 40,
    },
    submitText: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        height: 15,
        marginTop:2

    }
});

module.exports = ZZRandCodeView;
