/**
 * Created by easy on 15/11/2.
 */


var React = require('react-native');
var Components = require('Components');
var {
    NavigatorIOS,
    Navigator,
    View
    } = React;

var {
    Controller
    } = Components;

var MainController = require('./MainController.ios.js');

class AppController extends Controller {
    render() {
        return (
            <Navigator renderScene={(route, navigator) => <MainController /> } />
        );
    }
}

exports = module.exports = AppController;