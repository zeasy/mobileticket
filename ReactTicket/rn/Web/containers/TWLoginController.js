/**
 * Created by easy on 16/3/17.
 */

import React,{
    Component,
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet
} from 'react-native'

import { bindActionCreators } from 'redux';
import {connect} from 'react-redux'
import * as actions from '../actions/LoginActions'

import {MKButton,MKColor,MKTextField} from 'react-native-material-kit'

class TWLoginController extends Component {
    render() {
        return (
            <TouchableOpacity style={{justifyContent:'center',flex:.7}} onPress={this.onPress.bind(this)}>
                <Text style={[{alignSelf:'center'}]}>Web</Text>
            </TouchableOpacity>
        );
    }


    onPress() {
        this.props.actions.loginRequest();
    }
}

let mapStateToProps = (state,props) => {
    return {...state.Web.Login}
}

let mapDispatchToProps = (dispatch,ownProps) => {
    return {
        actions : bindActionCreators(actions,dispatch)
    };
}

module.exports = connect(mapStateToProps,mapDispatchToProps)(TWLoginController);