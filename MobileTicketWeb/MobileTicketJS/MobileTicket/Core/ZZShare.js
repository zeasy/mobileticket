/**
 * @providesModule ZZShare
 * Created by easy on 16/1/13.
 */


'use strict';
let Async = require('ZZAsync');
let CloudShare = require('NativeModules').CloudShare;

class ZZShare {

    static async share(text,images,title='',url='') {
        return Async.invoke(CloudShare.share,{text,images,title,url});
    }
}

module.exports = ZZShare;