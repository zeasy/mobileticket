
/* JavaScript content from js/controller/sendTicketAddress.js in folder common */
(function() {
	jq.extendModule("mor.ticket.sendTicketAddressLi", {
		isLi : ""
	});
	var addressLists = [];
	var prevPage;
	jq("#sendTicketAddressView").live("pageinit", function() {
		jq("#ticketAddressBackBtn").bind("tap", function() {
			if(mor.ticket.viewControl.current_tab == "sendTicketVisitTab"){
				var currIndex = parseInt(mor.ticket.sendTicketAddress.thisAddress)-1;
				var orderInfo = mor.ticket.sendTicketAddress.sendOrderList[currIndex];
				mor.ticket.sendTicketAddress.selected = false;
				if(addressLists.length == 0){
					mor.ticket.sendTicketAddress.selected = false;
				}else{
					for(var i = 0;i<addressLists.length;i++){
						if((orderInfo.address_name == addressLists[i].addressee_name)
								&& (orderInfo.address_tel == addressLists[i].mobile_no)
								&& (orderInfo.address_addr == (addressLists[i].addressee_province+""+addressLists[i].addressee_city+""+addressLists[i].addressee_county))
								&& (orderInfo.address_detail == addressLists[i].detail_address)){
							mor.ticket.sendTicketAddress.thisAddress = currIndex;
							mor.ticket.sendTicketAddress = addressLists[i];
							mor.ticket.sendTicketAddress.selected = true;
							break;
						}
					}
				}
				mor.ticket.util.changePage("sendTicketVisit.html");
			}
			if(mor.ticket.viewControl.current_tab == "ticketAddressMessageTab"){
				mor.ticket.util.changePage("my12306.html");
			}
			return false;
		});
		jq("#addTicketAddressBtn").off().on("tap", ticketAddressAddFn);
		registerAddressListItemClickHandler();
	});
	jq("#sendTicketAddressView").live("pagebeforeshow", function(e, data) {
		mor.ticket.util.initAppVersionInfo();
		mor.ticket.sendTicketAddressLi.isLi = "";
		prevPage = data.prevPage.attr("id");
		if (prevPage === "***********View") {
			jq("#addTicketAddressBtn").html("确认选择");
		} else {
			jq("#addTicketAddressBtn").html("添加");
		}
		queryDeliverRequest();
		mor.ticket.util.initJSONStoreFromLocalKuaidi();
		setTimeout(function() {
			getProvince();
		}, 1000);
	});
	function ticketAddressAddFn() {
		if (prevPage === "***********View") {
			mor.ticket.util.changePage("modifyTicketAddress.html", true);
		} else {
			mor.ticket.viewControl.isModifyTicketAddress = false;
			mor.ticket.util.changePage("modifyTicketAddress.html", true);
		}
		return false;
	}
	function getProvince(){
		var option = {};
		WL.JSONStore.get("expressAddress").findAll(option)
		.then(function(res){
			var provinceArray = res[0].json.provinceList;
			if(mor.ticket.stateList.result == ""){
				if(provinceArray.length>0){
					for ( var i = 0; i < provinceArray.length; i++) {
						mor.ticket.stateList.result.push(provinceArray[i]);
					}
				}else{
					WL.Logger.info(errorObject.msg);
				}
			}
			
		})
		.fail(function(errorObject){
				WL.Logger.info(errorObject.msg);
				//jq("#modify_studentSchool").val("");
		});
	}
	function queryDeliverRequest(e) {

		var util = mor.ticket.util;
		var commonParameters = "";

		var invocationData = {
			adapter : mor.ticket.viewControl.adapterUsed,
			procedure : "queryDeliverAddress"
		};

		var options = {
			onSuccess : queryDeliverSucceeded,
			onFailure : util.creatCommonRequestFailureHandler()
		};

		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,
				options);
		return false;
	}
	function queryDeliverSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			addressLists = invocationResult.deliverAddressList;
			if(addressLists.length == 0){
				mor.ticket.util.alertMessage(invocationResult.error_msg);
				return false;
			}
			if (prevPage === "***********View") {
				jq("#ticketAddressList").html(
						generateAddressListSelected(addressLists)).listview(
						"refresh");
			} else if(mor.ticket.viewControl.current_tab == "sendTicketVisitTab"){
				var sendTicketAddressLists = mor.ticket.sendTicketAddressLists;
				if(sendTicketAddressLists == null || sendTicketAddressLists.length != addressLists.length){
					sendTicketAddressLists = [];
					mor.ticket.sendTicketAddressLists = addressLists;
				}
				var currIndex = parseInt(mor.ticket.sendTicketAddress.thisAddress);
				var arrayIndex = [];
				for(var i = 0;i<addressLists.length;i++){
					addressLists[i].selected = true;
				}
				if(mor.ticket.sendTicketAddress.sendTicketList.length <= 5){
					for(var i = 0;i<mor.ticket.sendTicketAddress.sendOrderList.length;i++){
						var orderInfo = mor.ticket.sendTicketAddress.sendOrderList[i];
						for(var j = 0;j<addressLists.length;j++){
							if(currIndex != orderInfo.order_no 
									&& (orderInfo.address_name == addressLists[j].addressee_name)
									&& (orderInfo.address_tel == addressLists[j].mobile_no)
									&& (orderInfo.address_addr == (addressLists[j].addressee_province+""+addressLists[j].addressee_city+""+addressLists[j].addressee_county))
									&& (orderInfo.address_detail == addressLists[j].detail_address)){
								arrayIndex.push(j);
								break;
							}
						}
					}
				}else{
					if(mor.ticket.sendTicketAddress.sendOrderList.length>2){
						for(var i = 0;i<mor.ticket.sendTicketAddress.sendOrderList.length;i++){
							var orderInfo = mor.ticket.sendTicketAddress.sendOrderList[i];
							for(var j = 0;j<addressLists.length;j++){
								if(currIndex != orderInfo.order_no
										&& (orderInfo.address_name == addressLists[j].addressee_name)
										&& (orderInfo.address_tel == addressLists[j].mobile_no)
										&& (orderInfo.address_addr == (addressLists[j].addressee_province+""+addressLists[j].addressee_city+""+addressLists[j].addressee_county))
										&& (orderInfo.address_detail == addressLists[j].detail_address)){
									arrayIndex.push(j);
									break;
								}
							}
						}
					}
				}
				if(arrayIndex.length > 0){
					for(var i = 0;i<arrayIndex.length;i++){
						addressLists[arrayIndex[i]].selected = false;
					}
				}
				jq("#ticketAddressList").html(generateSendTicketAddressList(addressLists)).listview("refresh");
			} else {
				jq("#ticketAddressList").html(generateAddressList(addressLists)).listview("refresh");
			}
			for ( var i = 0; i < addressLists.length; i++) {
				if (addressLists[i].default_address == "0") {
					jq("#ticketAddressList li[data-index='" + i + "']").find(
							"a").attr("style", "color:green");
				}
			}
			jq("#ticketAddressList").listview("refresh");
		} else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}

	function registerAddressListItemClickHandler() {
		var TAlist = mor.ticket.sendTicketAddressList;
		jq("#ticketAddressList")
				.off()
				.on(
						"tap",
						"li",
						function(e) {
							e.stopImmediatePropagation();
							if(mor.ticket.viewControl.current_tab == "sendTicketVisitTab"){
								var index = jq(this).attr("data-index");
								var selectedAddress = addressLists[index];
								var thisAddress = mor.ticket.sendTicketAddress.thisAddress;
								mor.ticket.sendTicketAddress = selectedAddress;
								mor.ticket.sendTicketAddress.thisAddress = thisAddress;
								mor.ticket.sendTicketAddress.selected = true;
								mor.ticket.util.changePage(vPathCallBack() + "sendTicketVisit.html");
								return false;
							}else{
								mor.ticket.sendTicketAddressLi.isLi = "yes";
								e.stopImmediatePropagation();
								var index = jq(this).attr("data-index");
								// if(TAlist.length != 0 && prevPage ===
								// "***********View"){
								if (TAlist.length == 1) {
									jq("#ticketAddressList").each(
											function() {
												jq(this).find("input").attr(
														"checked", false);
											});
								}
								var selectedAddress = addressLists[index];
								if(selectedAddress.canEditFlag == "N"){
									mor.ticket.util.alertMessage("此地址在车票快递中使用过，"+selectedAddress.canEditDate+"之前不允许修改！");
									return false;
								}
								mor.ticket.sendTicketAddressLi.isLi = "yes";
								TAlist.original_address_name = selectedAddress.addressee_name;
								TAlist.original_address_province = selectedAddress.addressee_province;
								TAlist.original_address_city = selectedAddress.addressee_city;
								TAlist.original_address_county = selectedAddress.addressee_county;
								TAlist.original_addressee_town = selectedAddress.addressee_town;
								TAlist.original_detail_address = selectedAddress.detail_address;
								TAlist.original_addressee_street = selectedAddress.addressee_street;
								TAlist.original_mobile_no = selectedAddress.mobile_no;
								TAlist.default_address = selectedAddress.default_address;
								TAlist.addressListLength = addressLists.length;
								TAlist.length = 1;
								TAlist.checked = 1;
								if (prevPage === "***********View") {
									jq(this).find("input").attr("checked", true);
								} else {
									mor.ticket.viewControl.isModifyTicketAddress = true;
									jq("#modifyTicketAddressView").remove();
									mor.ticket.util.changePage("modifyTicketAddress.html", true);
								}
							}
							return false;
						});
	}
	;

	var addressListTempate = "{{~it :deliverAddress:index}}"
			+ "<li data-index='{{=index}}' id='{{=index}}'><a>"
			+ "<div class='ui-grid-b' id = 'div'>"
			+ "<div class='ui-block-b'>{{=deliverAddress.addressee_name}}</div>"
			+ "<div class='ui-block-c'>{{=deliverAddress.addressee_city}}</div>"
			+ "<div class='ui-block-d'>{{=deliverAddress.detail_address}}</div>"
			+ "</div></a>" + "</li>" + "{{~}}";
	var generateAddressList = doT.template(addressListTempate);

	var addressListSelectTempate = "{{~it :deliverAddress:index}}"
			+ "<li data-val='' data-index='{{=index}}' id='{{=index}}'><a>"
			+ "<div class='ui-grid-b'>"
			+ " {{ if (deliverAddress.checked==1) { }} "
			+ "<div class='ui-block-a'><input id='list_{{=index}}' name=\"address\" type=\"checkbox\"  checked value=\"{{=index}}\"></div>"
			+ "{{ }else{ }}"
			+ "<div class='ui-block-a'><input id='list_{{=index}}' name=\"address\" type=\"checkbox\"  value=\"{{=index}}\"></div>"
			+ "{{ } }}"
			+ "<div class='ui-block-b'>{{=deliverAddress.addressee_name}}</div>"
			+ "<div class='ui-block-c'>{{=deliverAddress.addressee_city}}</div>"
			+ "<div class='ui-block-d'>{{=deliverAddress.detail_address}}</div>"
			+ "</div></a>" + "</li>" + "{{~}}";
	var generateAddressListSelected = doT.template(addressListSelectTempate);

	var sendTicketAddressListTempate = "{{~it :address:index}}"
		+ "<li data-index='{{=index}}' id='{{=index}}' "
		+ "{{ if(address.selected == false){ }}"
		+ "class='ui-disabled'"
		+ "{{ } }}"
		+ " >"
		+ "<a>"
		+ "<div class='ui-grid-b' id = 'div'>"
		+ "<div class='ui-block-b'>{{=address.addressee_name}}</div>"
		+ "<div class='ui-block-c'>{{=address.addressee_city}}</div>"
		+ "<div class='ui-block-d'>{{=address.detail_address}}</div>"
		+ "</div></a>" + "</li>" + "{{~}}";
	var generateSendTicketAddressList = doT.template(sendTicketAddressListTempate);
})();
