/**
 * @providesModule Database
 * Created by easy on 15/11/17.
 */

'use strict';

var NativeDatabase = require('NativeModules').NativeDatabase;
var Interface = require('Interface');

class Database {

    constructor(path) {
        this._path = path;
    }

    async asyncOpen() {
        if (!this._isOpen) {
            return Interface.asyncInvoke(NativeDatabase.openDatabase,this._path).then((result)=>{
                this._isOpen = true;
                return result;
            });
        }
    }

    async asyncClose() {
        if (this._isOpen) {
            return Interface.asyncInvoke(NativeDatabase.closeDatabase).then((result)=> {
                this._isOpen = false;
                return result;
            });
        }
    }

    isOpen() {
        return this._isOpen;
    }

    path() {
        return this._path;
    }

    table(name,schema) {
        if (!this._isOpen) {
            this.asyncOpen();
        }
        return new Table(this,name,schema);
    }

    static defaultDatabase() {
        return DefaultDatabase;
    }
}

/**
 * Schema
 * {
 *      type : number | string (支持类型)
 *      id   : bool (true | false) (是否是主键)
 * }
 * Condition
 * {
 *      where   : string
 *      fields  : array
 *      order   : string (ASC | DESC)
 *      limit   : number
 *      offset  : number
 * }
 * */

class Table {
    constructor(database,name,schema) {
        this._database = database;
        this._name = name;
        this._schema = schema;
        this.asyncCreate();
    }

    async asyncCreate() {
        return Interface.asyncInvoke(NativeDatabase.createTable,this._database.path(),this._name,this._schema);
    }

    async asyncSave(objects) {
        return Interface.asyncInvoke(NativeDatabase.save,this._database.path(),this._name,objects);
    }

    async asyncFetch(condition) {
        return Interface.asyncInvoke(NativeDatabase.fetch,this._database.path(),this._name,condition);
    }

    async asyncRemove(condition) {
        return Interface.asyncInvoke(NativeDatabase.remove,this._database.path(),this._name,condition);
    }

    async asyncCount(condition) {
        return Interface.asyncInvoke(NativeDatabase.count,this._database.path(),this._name,condition);
    }

    async asyncMax(column,condition) {
        return Interface.asyncInvoke(NativeDatabase.max,this._database.path(),this._name,column,condition);
    }

    async asyncMin(column,condition) {
        return Interface.asyncInvoke(NativeDatabase.min,this._database.path(),this._name,column,condition);
    }

    async asyncSum(column,condition) {
        return Interface.asyncInvoke(NativeDatabase.sum,this._database.path(),this._name,column,condition);
    }

    async asyncTotal(column,condition) {
        return Interface.asyncInvoke(NativeDatabase.total,this._database.path(),this._name,column,condition);
    }

    async asyncAvg(column,condition) {
        return Interface.asyncInvoke(NativeDatabase.avg,this._database.path(),this._name,column,condition);
    }
}

var DefaultDatabase = new Database('default.db');

Database.Table = Table;
exports = module.exports = Database;