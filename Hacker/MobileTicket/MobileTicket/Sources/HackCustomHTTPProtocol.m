//
//  HackCustomHTTPProtocol.m
//  
//
//  Created by EASY on 15/10/30.
//
//

#import "HackCustomHTTPProtocol.h"

@implementation HackCustomHTTPProtocol

+(instancetype) sharedInstance {
	static id instance;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[self alloc] init];
	});
	return instance;
}

-(void) __initWithRequest:(NSURLRequest *)request cachedResponse:(NSCachedURLResponse *)cachedResponse client:(id<NSURLProtocolClient>)client {
	NSString *body = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
	NSLog(@"[==REQT==]URL:%@\nMETHOD:%@\nHEADERS:%@\nBODY:\n%@",request.URL,request.HTTPMethod,request.allHTTPHeaderFields,body);
//
}

-(void) didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *) challenge {
	NSLog(@"%s",__FUNCTION__);
}

-(void) resolveAuthenticationChallenge:(NSURLAuthenticationChallenge *) challenge withCredential:(NSURLCredential *) c {
	NSLog(@"%s",__FUNCTION__);
}


-(NSURLRequest *) connection:(NSURLConnection *) connection willSendRequest:(NSURLRequest *) request redirectResponse:(NSURLResponse *) response {
	NSLog(@"%s",__FUNCTION__);
	return nil;
}

-(void) connection:(NSURLConnection *) conn didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *) c {
	NSLog(@"%s",__FUNCTION__);
}

-(void) connection:(NSURLConnection *) conn didReceiveResponse:(NSURLResponse *) response {
	NSURLRequest *request = conn.originalRequest;
	if ([request.URL.absoluteString rangeOfString:@"12306"].location == NSNotFound) {
		return;
	}
	NSLog(@"[==RESP==]%@",[(NSHTTPURLResponse *)response allHeaderFields]);

}

-(void)  connection:(NSURLConnection *) conn didReceiveData:(NSData *) data {
	NSURLRequest *request = conn.originalRequest;
	if ([request.URL.absoluteString rangeOfString:@"12306"].location == NSNotFound) {
		return;
	}

	
	NSString *body = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
	NSLog(@"[==RESP==]URL:%@\nMETHOD:%@\nHEADERS:%@\nBODY:\n%@",request.URL,request.HTTPMethod,request.allHTTPHeaderFields,body);
//	NSLog(@"[RESPONSE]URL:\n%@",request.URL);
	NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	NSLog(@"%@",responseString);
}
@end
