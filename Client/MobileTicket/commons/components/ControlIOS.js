/**
 * Created by easy on 15/11/13.
 */



var View = require('./View');
var React = require('react-native');
var findNodeHandle = require('findNodeHandle');
var ZZControlManager = require('NativeModules').ZZControlManager;

var {
    requireNativeComponent
    } = React;

var ZZNativeControlIOS = requireNativeComponent('ZZControl');

class ZZControlIOS extends View {

    constructor() {
        super();

    }

    addEvents(events,callback) {
        var node = findNodeHandle(this);
        if (node) {
            ZZControlManager.addEvents(node,events,(result) => {
                callback(events);
            });
        }

    }

    render() {

        return (
            <ZZNativeControlIOS {...this.props}/>
        );
    }
}



exports = module.exports = ZZControlIOS;