//
//  ZZTableViewHeaderFooterView.h
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RCTEventDispatcher.h>



@interface ZZTableViewHeaderFooterView : UIView

@property (nonatomic, strong) NSString *reuseIdentifier;

@property (nonatomic, assign) BOOL isHeader;
@property (nonatomic, strong) NSNumber *section;
@property (nonatomic, strong) NSDictionary *sectionData;


- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher;

@end


@interface _ZZTableViewHeaderFooterView : UITableViewHeaderFooterView

@property (nonatomic, strong) ZZTableViewHeaderFooterView *headerFooterView;

@end