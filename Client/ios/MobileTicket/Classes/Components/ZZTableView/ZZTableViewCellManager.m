//
//  ZZTableViewCellManager.m
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZTableViewCellManager.h"
#import "ZZTableViewCell.h"

@implementation ZZTableViewCellManager


RCT_EXPORT_MODULE()
RCT_EXPORT_VIEW_PROPERTY(reuseIdentifier, NSString)
RCT_EXPORT_VIEW_PROPERTY(cellStyle, UITableViewCellStyle)
RCT_EXPORT_VIEW_PROPERTY(indexPath, NSIndexPath)
RCT_EXPORT_VIEW_PROPERTY(rowData, NSDictionary)
RCT_EXPORT_VIEW_PROPERTY(sectionData, NSDictionary)

RCT_EXPORT_VIEW_PROPERTY(onSelected, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onHighlighted, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onEditing, RCTDirectEventBlock)



-(UIView<RCTComponent> *)view {
	return (UIView<RCTComponent> *)[[ZZTableViewCell alloc] initWithEventDispatcher:self.bridge.eventDispatcher];
}

-(NSDictionary *)constantsToExport {
	return @{};
}
@end


@implementation RCTConvert (NSIndexPath)

+(NSIndexPath *) NSIndexPath:(id) json {
	NSInteger section = [json[@"section"] integerValue];
	NSInteger row = [json[@"row"] integerValue];
	return [NSIndexPath indexPathForRow:row inSection:section];
}

@end