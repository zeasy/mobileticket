
/* JavaScript content from js/controller/login.js in folder common */
(function(){
	/*jq("#loginView").live("pagecreate", function() {
		mor.ticket.util.androidRemoveIscroll("#loginView");
	});*/
	var focusArray=[];
	jq("#loginView").live("pageshow", function() {
		focusArray=[];
	});
	function registerAutoScroll(){	
		var util = mor.ticket.util;	
		if(util.isIPhone()){	
			util.enableAutoScroll('#usernameInput',focusArray);
			util.enableAutoScroll('#passwordInput',focusArray);
		}
/*		
	    jq('#usernameInput, #passwordInput').on('focusin', function(e) { 
            jq('.footer_bar').hide(); 
}).on('focusout', function(e) { 
        if(!jq('#usernameInput, #passwordInput').is(':focus')) { 
                jq('[.footer_bar').show(); 
        } 
}); 
*/	    
	}
	
	jq("#loginView").live("pageinit", function(){
		jq.mobile.defaultHomeScroll = 0;
		
		var user=mor.ticket.loginUser;		
		user.isKeepUserPW = (window.ticketStorage.getItem("isKeepUserPW") == null ? true : window.ticketStorage.getItem("isKeepUserPW"));
		user.username     = window.ticketStorage.getItem("username");
		
		
	//	WL.Logger.debug("rizhi");

		registerMoreOptionBtnLisener();
		
		initKeepUsrChkbox(user); 
		registerKeepUsrChkboxListener(user);			
		registerLoginBtnLisener();		
		registerRegisterBtnLisener();	
		registerAutoScroll();
		initAutoLoginChkbox();
	
		//registerInputChangeLisener();	
		//registerCleanInputLisener();
		
	   //jq.mobile.loadPage(vPathViewCallBack()+"MobileTicket.html");//pre-load
		
		
		
	});
	

	
	
	jq("#loginView").live("pagebeforeshow", function(){
		mor.ticket.util.initAppVersionInfo();
		//单点登录--调整登录页面--记住密码及自动登录状态
		initKeepUsrChkbox(mor.ticket.loginUser); 
		initAutoLoginChkbox();
		//clear pass if no need store pass
		var user =  mor.ticket.loginUser;
		if (!user.isKeepUserPW) {
			jq("#passwordInput").val("");
		}else{
			//add zzc
			jq("#usernameInput").val(user.username);
			jq("#passwordInput").val(user.password);
		}
		if(window.ticketStorage.getItem("isKeepUserPW") == null){
			window.ticketStorage.setItem("isKeepUserPW", true);
		}
		


	});
	
	function registerInputChangeLisener(){		
		jq("#usernameInput").bind("keyup  focus",function(){
			jq("#deletePassword").hide();
			if(jq(this).val()){
				jq("#deleteUserName").show();
			} else {
				jq("#deleteUserName").hide();
			}
		});
		
		jq("#passwordInput").bind("keyup  focus",function(){
			jq("#deleteUserName").hide();
			if(jq(this).val()){
				jq("#deletePassword").show();
			}else {
				jq("#deletePassword").hide();
			}
		});
	}
	
	function registerCleanInputLisener(){
		jq("#deleteUserName").bind("tap",function(){
			jq("#usernameInput").val("");
			jq("#deleteUserName").hide();
			jq("#usernameInput").focus();			return false;
		});
		jq("#deletePassword").bind("tap",function(){
			jq("#passwordInput").val("");
			jq("#deletePassword").hide();
			jq("#passwordInput").focus();			return false;
		});
	}
	
	function initKeepUsrChkbox(user){
		var isChecked;
		if(user.isKeepUserPW === "true" || user.isKeepUserPW == true){
			isChecked = true;
		}
		else{
			isChecked = false;
		}
		jq("#keepUsrChkbox").attr("checked", isChecked);
	};
	
	
	function initAutoLoginChkbox(){
		
		var isChecked;
		var autologin =  window.ticketStorage.getItem("autologin");
	
		
		if(autologin === "true" || autologin == true){
			isChecked = true;
		}
		else{
			isChecked = false;
		}
		jq("#autologinChkbox").attr("checked", isChecked);

	};
	
	
	
	
	function registerKeepUsrChkboxListener(user){
		jq("#keepUsrChkbox").bind("change", function(event){
			//by yiguo 如果自动登录为选择状态，则不可以修改保存密码状态
			if(jq("#autologinChkbox").attr("checked")){
				mor.ticket.util.alertMessage("自动登录状态密码必须记住");
				jq("#keepUsrChkbox").attr("checked", true);
				user.isKeepUserPW = true;
				window.ticketStorage.setItem("isKeepUserPW", true);
				return false;
				
			}
			
			user.isKeepUserPW = event.target.checked;
			window.ticketStorage.setItem("isKeepUserPW", user.isKeepUserPW);
			return false;
		});
		
		
		 //  注册 自动登录方法
		jq("#autologinChkbox").bind("change", function(event){
			if ( event.target.checked){
				window.ticketStorage.setItem("autologin",true);
				user.isKeepUserPW = true;
				jq("#keepUsrChkbox").attr("checked", true);
				window.ticketStorage.setItem("isKeepUserPW", user.isKeepUserPW);
			}else{
				window.ticketStorage.setItem("autologin",false);
			}
			return false;
		});
					//window.ticketStorage.setItem("autologin", true);

	};
	
	function registerLoginBtnLisener(){
		jq("#loginBtn").bind("tap", sendLoginRequest);
	};
	
	
	
	function registerMoreOptionBtnLisener(){
		
		jq("#moreOptionBtn").bind("tap", function(){
			mor.ticket.util.changePage(vPathCallBack()+"moreOption.html");
		});
		
		jq("#loginView #bookTicketTab").bind("tap",function(){
			mor.ticket.util.changePage(vPathViewCallBack()+"MobileTicket.html");
		});
	};
	
	
	
	function loginRegisterFn(){
			jq("#registerBtn").off();
			jq("#registView").remove();
//			jq("#registView_basicInfo").remove();
			mor.ticket.registInfo=[];
			//单页注册
//			mor.ticket.util.changePage("regist_basicInfo.html",true);
			
			mor.ticket.util.changePage(vPathCallBack()+"regist.html",true);
			setTimeout(function(){
				jq("#registerBtn").on("tap",loginRegisterFn);
			},1000);
			return false;
	}

	
	
	function registerRegisterBtnLisener(){
		jq("#registerBtn").on("tap",loginRegisterFn );
	};
	
	
	
	
	function sendLoginRequest(e) {
		if(jq("#usernameInput").val()==""){
			mor.ticket.util.alertMessage("请填写用户名");
			mor.ticket.util.changePage(vPathCallBack()+"loginTicket.html");
			return;
		}
		
		if(jq("#passwordInput").val()==""){
			mor.ticket.util.alertMessage("请填写密码");
			mor.ticket.util.changePage(vPathCallBack()+"loginTicket.html");
			return;
		}
		
			
		var util = mor.ticket.util;	
				
		
//		set commonParameters and let mor.ticket.util.invokeWLProcedure method to merge the parameters
		var commonParameters = {
			'baseDTO.user_name': jq("#usernameInput").val(),
			'password': hex_md5(jq("#passwordInput").val())
			//'autologinChkbox': jq("#autologinChkbox").val()
		};
		
//		var commonParameters = util.prepareRequestCommonParameters({
//			'baseDTO.user_name': jq("#usernameInput").val(),
//			'password': hex_md5(jq("#passwordInput").val())
//		});
		
		var invocationData = {
			adapter: mor.ticket.viewControl.adapterUsed,
			procedure: "login"
		};
		
//		var invocationData = {
//				adapter: "AuthenticationAdapter",
//				procedure: "submitAuthentication",
//				parameters: [commonParameters]
//		};
		
		var options =  {
				onSuccess: requestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		
		// set commonParameters to null if no parameters to override
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		
//		WL.Client.invokeProcedure(invocationData, options);
		
		// remove busy.show() if using mor.ticket.util.invokeWLProcedure
//		busy.show();
		return false;
	}
	
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		if(mor.ticket.viewControl.session_out_page == ""){
			mor.ticket.viewControl.tab1_cur_page="";
			mor.ticket.viewControl.tab2_cur_page="";
			mor.ticket.viewControl.tab3_cur_page="";
			mor.ticket.viewControl.tab4_cur_page="";
			mor.ticket.viewControl.current_tab="";
			mor.ticket.leftTicketQuery.train_date = "";
			mor.ticket.leftTicketQuery.from_station_telecode = "";
			mor.ticket.leftTicketQuery.to_station_telecode = "";
			jq("#selectPassengerView").remove();
			jq("#queryOrderView").remove();
			
			
			var invocationResult = result.invocationResult;
			if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
				if(invocationResult.notice_one_session == "Y"){
					mor.ticket.util.alertMessage(invocationResult.notice_one_sessionMsg);
				}
				mor.ticket.loginUser["isAuthenticated"]="Y";
				mor.ticket.loginUser.is_receive = invocationResult.is_receive;
				mor.ticket.loginUser.start_receive = invocationResult.start_receive;
				saveUserPassIfNecessary(mor.ticket.loginUser);
				if(mor.ticket.loginUser["username"]==null||mor.ticket.loginUser["username"]==''){
					mor.ticket.loginUser["username"] = jq("#usernameInput").val();
				}
				
				// init loginUser
				var loginUser=mor.ticket.loginUser;
				loginUser.accountName=invocationResult.user_name;
				loginUser.realName=invocationResult.name;
				loginUser.id_type = invocationResult.id_type_code;
				loginUser.id_no =invocationResult.id_no;
				loginUser.user_status = invocationResult.user_status;
				loginUser.check_id_flag = invocationResult.check_id_flag;
				loginUser.display_control_flag = invocationResult.display_control_flag; 
				loginUser.mobile_no = invocationResult.mobileNo;
				loginUser.user_type=invocationResult.user_type;
				loginUser.email=invocationResult.email;
				loginUser.activeUser = invocationResult.is_active;
				//init passengerinfo	
				//push 注册用户
				registerChannelsUser(loginUser.accountName);
				
				if(invocationResult.passengerResult){
					mor.ticket.passengersCache.passengers=[];
					mor.ticket.passengersCache.passengers=invocationResult.passengerResult;
					mor.ticket.passengersCache.sortPassengers();
				}
				mor.ticket.viewControl.tab1_cur_page="";
				mor.ticket.viewControl.tab2_cur_page="";
				mor.ticket.viewControl.tab3_cur_page="";
				mor.ticket.viewControl.tab4_cur_page="";
				mor.ticket.viewControl.current_tab="";
				
				
				if(invocationResult.display_control_flag==="2"){
					if(mor.ticket.loginUser.id_type == "1"){
						 WL.SimpleDialog.show(
									"温馨提示", 
									"您在12306网站注册时填写信息有误，或已被他人冒用，为了保障您的个人信息安全，网站对您的账户采取了保护性措施，该账号目前在互联网上不能办理购票、退票和改签业务，您需尽快到就近的办理客运售票业务的铁路车站完成身份核验，通过后即可恢复网上正常购票、退票和改签业务，谢谢。", 
									[ {text : '确定', handler: function() {
									jq.mobile.changePage("my12306.html");
									}}]
						  );
					}else{
						 WL.SimpleDialog.show(
									"温馨提示", 
									"您的身份信息核验未通过，详见《铁路互联网购票身份核验须知》。", 
									[ {text : '确定', handler: function() {
									jq.mobile.changePage("my12306.html");
									}}]
						  );
					}
				}else if(invocationResult.display_control_flag==="3"){
					 WL.SimpleDialog.show(
								"温馨提示", 
								"您的身份信息正在进行网上核验，您也可持居民身份证原件到车站售票窗口或铁路客票代售点即时办理核验，详见《铁路互联网购票身份核验须知》。", 
								[ {text : '确定', handler: function() {
									jq.mobile.changePage(vPathViewCallBack()+"MobileTicket.html");
								}}]
					  );
				}else if(invocationResult.display_control_flag==="4"){
					 WL.SimpleDialog.show(
								"温馨提示", 
								"本系统不再支持一代居民身份证，请更改为二代居民身份证。", 
								[ {text : '确定', handler: function() {
									jq.mobile.changePage("my12306.html");
								}}]
					  );
				}else if(invocationResult.display_control_flag==="5"){
					 WL.SimpleDialog.show(
								"温馨提示", 
								"您的身份信息未经核验，需持在12306填写的有效身份证件原件到车站售票窗口办理预核验，详见《铁路互联网购票身份核验须知》。", 
								[ {text : '确定', handler: function() {
									jq.mobile.changePage(vPathViewCallBack()+"MobileTicket.html");
								}}]
					  );
				}else if(invocationResult.is_active==="N"){
					 WL.SimpleDialog.show(
								"温馨提示", 
								"如果您要接收12306的服务邮件，请到我的12306验证邮箱。", 
								[ {text : '确定', handler: function() {
									jq.mobile.changePage(vPathViewCallBack()+"MobileTicket.html");
								}}]
					  );
				}else{
						jq.mobile.changePage(vPathViewCallBack()+"MobileTicket.html");
				}
			} else {	
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}
		}else{
//			WL.Logger.debug("session_out_page:" + mor.ticket.viewControl.session_out_page);
			var session_out_page = mor.ticket.viewControl.session_out_page;
			mor.ticket.viewControl.session_out_page = "";
			if(mor.ticket.loginUser.isAuthenticated === "N"){
				var invocationResult = result.invocationResult;
				if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
					if(invocationResult.notice_one_session == "Y"){
						mor.ticket.util.alertMessage(invocationResult.notice_one_sessionMsg);
					}
					mor.ticket.loginUser["isAuthenticated"]="Y";
					mor.ticket.loginUser.is_receive = invocationResult.is_receive;
					mor.ticket.loginUser.start_receive = invocationResult.start_receive;
					saveUserPassIfNecessary(mor.ticket.loginUser);
					if(mor.ticket.loginUser["username"]==null||mor.ticket.loginUser["username"]==''){
						mor.ticket.loginUser["username"] = jq("#usernameInput").val();
					}
					
				
					var loginUser=mor.ticket.loginUser;
					loginUser.accountName=invocationResult.user_name;
					loginUser.realName=invocationResult.name;
					loginUser.id_type = invocationResult.id_type_code;
					loginUser.id_no =invocationResult.id_no;
					loginUser.user_status = invocationResult.user_status;
					loginUser.check_id_flag = invocationResult.check_id_flag;
					loginUser.display_control_flag = invocationResult.display_control_flag;
					loginUser.mobile_no = invocationResult.mobileNo;
					loginUser.user_type=invocationResult.user_type;
					loginUser.email=invocationResult.email;
					loginUser.activeUser = invocationResult.is_active;
					//init passengerinfo	
	
					
					if(invocationResult.passengerResult){
						mor.ticket.passengersCache.passengers=[];
						mor.ticket.passengersCache.passengers=invocationResult.passengerResult;
						mor.ticket.passengersCache.sortPassengers();
					}
					mor.ticket.viewControl.tab1_cur_page="";
					mor.ticket.viewControl.tab2_cur_page="";
					mor.ticket.viewControl.tab3_cur_page="";
					mor.ticket.viewControl.tab4_cur_page="";
					mor.ticket.viewControl.current_tab="";
					if(invocationResult.display_control_flag==="2"){
						if(mor.ticket.loginUser.id_type == "1"){
							 WL.SimpleDialog.show(
										"温馨提示", 
										"您在12306网站注册时填写信息有误，或已被他人冒用，为了保障您的个人信息安全，网站对您的账户采取了保护性措施，该账号目前在互联网上不能办理购票、退票和改签业务，您需尽快到就近的办理客运售票业务的铁路车站完成身份核验，通过后即可恢复网上正常购票、退票和改签业务，谢谢。", 
										[ {text : '确定', handler: function() {
										}}]
							  );
						}else{
							 WL.SimpleDialog.show(
										"温馨提示", 
										"您的身份信息核验未通过，详见《铁路互联网购票身份核验须知》。", 
										[ {text : '确定', handler: function() {
										}}]
							  );
						}
					}
					if(invocationResult.display_control_flag==="3"){
						 WL.SimpleDialog.show(
									"温馨提示", 
									"您的身份信息正在进行网上核验，您也可持居民身份证原件到车站售票窗口或铁路客票代售点即时办理核验，详见《铁路互联网购票身份核验须知》。", 
									[ {text : '确定', handler: function() {
									}}]
						  );
					}
					if(invocationResult.display_control_flag==="4"){
						 WL.SimpleDialog.show(
									"温馨提示", 
									"本系统不再支持一代居民身份证，请更改为二代居民身份证。", 
									[ {text : '确定', handler: function() {
									}}]
						  );
					}
					if(invocationResult.display_control_flag==="5"){
						 WL.SimpleDialog.show(
									"温馨提示", 
									"您的身份信息未经核验，需持在12306填写的有效身份证件原件到车站售票窗口办理预核验，详见《铁路互联网购票身份核验须知》。", 
									[ {text : '确定', handler: function() {
									}}]
						  );
					}
					if(invocationResult.is_active==="N"){
						 WL.SimpleDialog.show(
									"温馨提示", 
									"如果您要接收12306的服务邮件，请到我的12306验证邮箱。", 
									[ {text : '确定', handler: function() {
									}}]
						  );
					}
					if (session_out_page.indexOf('views')>=0){
						jq.mobile.changePage('../'+session_out_page);
					}else{
						jq.mobile.changePage(session_out_page);
					}
					
				}else {	
					mor.ticket.util.alertMessage(invocationResult.error_msg);
				}
				
			} else {	
				mor.ticket.util.alertMessage(invocationResult.error_msg);
			}
			
		}
	}
	
	function saveUserPassIfNecessary(user) {
		var latestTime = window.ticketStorage.getItem("pwdTime");
		if(latestTime && jq("#usernameInput").val() == user.username && jq("#passwordInput").val() == user.password && (Date.now() - latestTime  < 7*24*3600*1000)){
			return;
		}
		
		
		user.username = jq("#usernameInput").val();
		window.ticketStorage.setItem("username", user.username);
		if (user.isKeepUserPW) {
			user.password = jq("#passwordInput").val();
			if(user.password){
				//window.ticketStorage.setItem("password", user.password);
				WL.EncryptedCache.open("wlkey",true,onOpenComplete,onOpenError);
			}
		}
	}

	function onOpenComplete(status){ 
		WL.EncryptedCache.write("userPW", mor.ticket.loginUser.password, 
				function(){
					// add by yiguo
					window.ticketStorage.setItem("pwdTime", Date.now());
					
//					WL.Logger.debug("Successfully write to storage");
					WL.EncryptedCache.close(function(){
//						WL.Logger.debug("Encrypted Cache Closed.");
					}, 
				function(){/*WL.Logger.debug("Failed to close Encrypted Cache.");*/});},
				function(){/*WL.Logger.debug("Failed to write to ticketStorage");*/});
	}
	
	function onOpenError(status){
//		WL.Logger.debug("Cannot open encrypted cache");
	}
})();