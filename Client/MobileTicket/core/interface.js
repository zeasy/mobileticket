/**
 * @providesModule Interface
 * Created by easy on 15/11/17.
 */

'use strict';

class Interface {
    /**
     * @method asyncCall React Native Module Method
     * @return A Promise With RN Method callback
     * */
    static async asyncInvoke(method,... args) {
        return new Promise(async (resolve, reject) => {
            if (typeof method != 'function') {
                var error = new TypeError('method is not a function,can\'t asyncCall it');
                reject(error);
            } else {
                let failed = (err) => {
                    reject(err);
                };
                let success = (result) => {
                    resolve(result);
                };
                if (typeof args[args.length - 1] == 'function') {
                    failed = args.pop();
                    if (typeof args[args.length - 1] == 'function') {
                        success = args.pop();
                    } else {
                        success = failed;
                    }
                }

                method(...args,success,failed);
            }
        });
    }
}

exports = module.exports = Interface;