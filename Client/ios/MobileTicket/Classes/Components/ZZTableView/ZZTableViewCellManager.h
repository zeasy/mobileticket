//
//  ZZTableViewCellManager.h
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import <React/RCTViewManager.h>

@interface ZZTableViewCellManager : RCTViewManager

@end


@interface RCTConvert (NSIndexPath)

+(NSIndexPath *) NSIndexPath:(id) json;
@end