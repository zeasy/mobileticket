/**
 * @providesModule FileManager
 * Created by easy on 15/11/17.
 */

'use strict';

var NativeFileManager = require('NativeModules').NativeFileManager;
var Interface = require('Interface');

class FileManager {
    static async asyncCacheDirectory() {
        return Interface.asyncInvoke(NativeFileManager.cacheDirectory);
    }

    static async asyncDocumentDirectory() {
        return Interface.asyncInvoke(NativeFileManager.documentDirectory);
    }

    static async asyncTemporaryDirectory() {
        return Interface.asyncInvoke(NativeFileManager.temporaryDirectory);
    }

    static async asyncMkdir(path) {
        return Interface.asyncInvoke(NativeFileManager.mkdir,path);
    }

    static async asyncRmdir(path) {
        return Interface.asyncInvoke(NativeFileManager.rmdir,path);
    }

    static async asyncExists(path) {
        return Interface.asyncInvoke(NativeFileManager.exists,path);
    }

    static async asyncCopy(src,dest) {
        return Interface.asyncInvoke(NativeFileManager.copy,src,dest);
    }

    static async asyncMove(src,dest) {
        return Interface.asyncInvoke(NativeFileManager.move,src,dest);
    }
}

exports = module.exports = FileManager;