
/* JavaScript content from js/controller/fareList.js in folder common */
(function() {
	/*jq("#selectFarePassengerView").live("pagecreate", function() {
		//mor.ticket.util.androidRemoveIscroll("#selectFarePassengerView");
	});*/
	
	
	var farelist = [];
	var cache_passenger=[];
	var add_passenger = false;
	jq("#selectFarePassengerView").live("pageinit", function() {
		var countryMap = mor.cache.countryMap;
		if(countryMap){
			var countryList = mor.ticket.cache.country;
			for(var i = 0; i < countryList.length; i++){
				countryMap[countryList[i].id] = countryList[i].value;
			};
		}	
		jq(".ui-filterable").append("<div class=\"farelistadd\"><a id=\"addFareBtn\" ></a></div>");
		jq(".ui-input-search").css("width","87%");
		jq(".ui-input-search").css("padding-right","0px");
		registerSelectFarePassengerBackClickHandler();
		addFareBtnClickHandler();
		registFareSlider_navClickHandler();
	});
	
	function fareListFn(){
		showCheckedSelectd();
		selectPassengerFunBtnClickHandler();
		registerfarePassengerListItemClickHandler();
		mor.ticket.viewControl.isNeedRefreshSelectPassengers = true;
		var deviceHeight = document.body.clientHeight + window.screenTop;
		var headerHeight = 44;
		var footerHeight = 50;
		var topHeight = 120;
		var sliderHeight = deviceHeight - footerHeight - topHeight - headerHeight;
		var size = parseInt(jq("#selectPassengerNavLength").val());
		//fix navList height bug
		jq('#selectPassenger_slider_nav-nav').height(sliderHeight);
		jq('#fareSlider_navList').height(sliderHeight);
		jq("#fareSlider_navList li").height(sliderHeight/size);

	}
	
	jq("#selectFarePassengerView").live("pagebeforeshow", function() {
		mor.ticket.util.initAppVersionInfo();
		   if(mor.ticket.loginUser.isAuthenticated !== "Y"){
				mor.ticket.util.keepPageURL();

			if (window.ticketStorage.getItem("autologin") != "true") {
				autologinFailJump();
			} else {
				registerAutoLoginHandler(fareListFn, autologinFailJump);
			}				
		   }else{
			   fareListFn();
		   }
	});
	
	
	function registerSelectFarePassengerBackClickHandler(){
		jq("#selectFarePassengerBackBtn").off().bind("tap",function(e){
            if (mor.ticket.history.url=='bookticket'){
                bookticketFun();
            }else{
                fareFun();
            }
			return false;
		});
	}
	
	function bookticketFun(){
			mor.ticket.util.changePage(vPathViewCallBack()+"MobileTicket.html");
			return ;
	}

	function fareFun(){
			mor.ticket.util.changePage(vPathCallBack()+"passengers.html");
	}

	function selectPassengerFunBtnClickHandler(){
		jq("#selectFarePassengerFunBtn").off().bind("tap",function(e){
			mor.ticket.passengerList=jq.merge([],cache_passenger);
	        if (mor.ticket.history.url=='bookticket'){
		  	   bookticketFun();
			}else{
				fareFun();
			}
			return false;
		});
	}
	
	  function farelistAddFareFn(){
			jq("#addFareBtn").off();
			var util = mor.ticket.util;
			if (mor.ticket.history.url==''){
				mor.ticket.history.url='fareList';
			}
		    var passengerInfo = mor.ticket.passengerDetail;
		    var util = mor.ticket.util;		
			util.clearObject(mor.ticket.passengerDetail);
			mor.ticket.viewControl.isModifyPassenger = false;
			jq("#modifyPassengerView").remove();
			add_passenger = true;
			mor.ticket.util.changePage("modifyPassenger.html", true);
			setTimeout(function(){		jq("#addFareBtn").off().on("tap",farelistAddFareFn);},1000);
			return false;
		}
	 
	 function addFareBtnClickHandler(){		 
		jq("#addFareBtn").off().on("tap",farelistAddFareFn);		  
	 }
	
	function registFareSlider_navClickHandler(){
		jq("#selectFarePassengerView a[id$='_Anchor']").off().on("tap",function(){
			var anchor = jq(this).attr("href").charAt(1);
			var curSelector = "#selectFarePassengerView #slectpassengers_"+anchor;
			var jqNode=jq(curSelector);
			var top=0;
			while(!jqNode.hasClass('iscroll-scroller')){
				top+=jqNode.position().top;
				jqNode=jqNode.offsetParent();
			}
			
			var screenHeight = jq("#selectFarePassengerView .iscroll-wrapper").height();
			var contentHeight = jq("#selectFarePassengerView .iscroll-scroller").height();
			var y=(top+screenHeight<=contentHeight)?top:contentHeight-screenHeight;
			jq("#selectFarePassengerView .iscroll-wrapper").iscrollview('scrollTo',0,-y,0);
		    return false;
		});
	};
	function registerfarePassengerListItemClickHandler() {

		jq("#farePassengerList").off().on("tap", "li",function(e) {
			if(jq(this).attr("data-role")!=null&&jq(this).attr("data-role")=='list-divider'){
				jq(this).removeClass("ui-btn-active");
				return false;
			}
			var index = jq(this).attr("data-index");
			var passengers = mor.ticket.passengersCache.passengers;
			 if (passengers=="" || passengers==undefined){
				  passengers = [[]];
			 }
			var selectedPassenger = passengers[index];
			
			var int_auto = cache_passenger.length;
			if (jq(this).find("input").is(":checked")){
				jq(this).find("input").attr("checked", false);
				for(var i=0;i<cache_passenger.length;i++){
						if(cache_passenger[i].id_no== selectedPassenger.id_no 
								&& selectedPassenger.user_name == cache_passenger[i].user_name 
								&& selectedPassenger.user_type == cache_passenger[i].user_type 
								&& selectedPassenger.checked == cache_passenger[i].checked){
							cache_passenger.splice(i,1);
				          selectedPassenger.checked =0;
						 break;
					}
				  }
			}else{
				if (int_auto>4){
					mor.ticket.util.alertMessage("乘车人最多选择5个");
				}else{
					var init_auto=0;
					  for(var ii =0;ii<cache_passenger.length;ii++){
						  if(cache_passenger[ii].id_no== selectedPassenger.id_no 
								  && selectedPassenger.user_name == cache_passenger[ii].user_name
								  && selectedPassenger.user_type == cache_passenger[ii].user_type 
								  && selectedPassenger.checked == cache_passenger[ii].checked ){
							  init_auto++;
						  }
					  }
					  if (init_auto>0){
						jq(this).find("input").attr("checked", true);
					  }else{
						  	var thisInput = jq(this);
						  	//复制出来的儿童票不做提示
						  	if(selectedPassenger.copy_child){
						  		thisInput.find("input").attr("checked", true);
								selectedPassenger.checked = 1;
								var A = [];
								A['seat_type']     = '';
								A['user_type']     = selectedPassenger.user_type;
								A['id_type']       = selectedPassenger.id_type;
								A['id_no']         = selectedPassenger.id_no;
								A['user_name']     = selectedPassenger.user_name;
								A['mobile_no']     = selectedPassenger.mobile_no;
								A['ticket_type']     = "1";
								A['p_str']     = selectedPassenger.p_str;
								A['checked'] = selectedPassenger.checked;
								A['total_times'] = selectedPassenger.total_times;
								A['two_totaltimes'] = selectedPassenger.two_totaltimes;
								A['other_totaltimes'] = selectedPassenger.other_totaltimes;
								cache_passenger.push(A);
								return; 
						  	}
						  	//针对儿童/学生/残军票购买者提示
						  	else if(selectedPassenger.user_type == "2" || selectedPassenger.user_type == "3" || selectedPassenger.user_type == "4"){
							  	var messages = "";
							  	var userType = selectedPassenger.user_type;
								if(userType == "2"){
									messages = "您是要购买儿童票吗（随同成人旅行身高1.2～1.5米的儿童，应当购买儿童票；超过1.5米时应买全价票。每一成人旅客可免费携带一名身高不足1.2米的儿童，超过一名时，超过的人数应买儿童票，详见购买儿童票有关规定。如不符合相关规定，请点击“否”。）？";
								}
								if(userType == "3"){
									messages = "您是要购买学生票吗（须凭购票时所使用的有效身份证件原件和附有学生火车票优惠卡的有效学生证原件换票乘车，详见购买学生票有关规定。如不符合相关规定，请点击“否”。）？";
								}
								if(userType == "4"){
									messages = "您是要购买残军票吗（须凭购票时所使用的有效身份证件原件和有效的“中华人民共和国残疾军人证”、“中华人民共和国伤残人民警察证”原件换票乘车，详见购买残疾军人优待票有关规定。如不符合相关规定，请点击“否”。）？";
								}
								if(mor.ticket.util.isAndroid()){
									WL.SimpleDialog.show(
											"温馨提示", 
											messages, 
											[ 
											  {text : '是', handler: function(){
												  thisInput.find("input").attr("checked", true);
													selectedPassenger.checked = 1;
													 var A = [];
													 A['seat_type']     = '';
													 A['user_type']     = selectedPassenger.user_type;
													 A['id_type']       = selectedPassenger.id_type;
													 A['id_no']         = selectedPassenger.id_no;
													 A['user_name']     = selectedPassenger.user_name;
													 A['mobile_no']     = selectedPassenger.mobile_no;
													 A['ticket_type']     = selectedPassenger.user_type;
													 A['p_str']     = selectedPassenger.p_str;
													 A['checked'] = selectedPassenger.checked;
													 A['total_times'] = selectedPassenger.total_times;
													 A['two_totaltimes'] = selectedPassenger.two_totaltimes;
													 A['other_totaltimes'] = selectedPassenger.other_totaltimes;
													 cache_passenger.push(A);
											  }},
											  {text : '否', handler: function(){
												  thisInput.find("input").attr("checked", true);
													selectedPassenger.checked = 1;
													 var A = [];
													 A['seat_type']     = '';
													 A['user_type']     = selectedPassenger.user_type;
													 A['id_type']       = selectedPassenger.id_type;
													 A['id_no']         = selectedPassenger.id_no;
													 A['user_name']     = selectedPassenger.user_name;
													 A['mobile_no']     = selectedPassenger.mobile_no;
													 A['ticket_type']     = "1";
													 A['p_str']     = selectedPassenger.p_str;
													 A['checked'] = selectedPassenger.checked;
													 A['total_times'] = selectedPassenger.total_times;
													 A['two_totaltimes'] = selectedPassenger.two_totaltimes;
													 A['other_totaltimes'] = selectedPassenger.other_totaltimes;
													 cache_passenger.push(A);
											  }}
											  ]
										);
								}else{
									WL.SimpleDialog.show(
											"温馨提示", 
											messages, 
											[ 
											  {text : '否', handler: function(){
												  thisInput.find("input").attr("checked", true);
													selectedPassenger.checked = 1;
													 var A = [];
													 A['seat_type']     = '';
													 A['user_type']     = selectedPassenger.user_type;
													 A['id_type']       = selectedPassenger.id_type;
													 A['id_no']         = selectedPassenger.id_no;
													 A['user_name']     = selectedPassenger.user_name;
													 A['mobile_no']     = selectedPassenger.mobile_no;
													 A['ticket_type']     = "1";
													 A['p_str']     = selectedPassenger.p_str;
													 A['checked'] = selectedPassenger.checked;
													 A['total_times'] = selectedPassenger.total_times;
													 A['two_totaltimes'] = selectedPassenger.two_totaltimes;
													 A['other_totaltimes'] = selectedPassenger.other_totaltimes;
													 cache_passenger.push(A);
											  }},
											  {text : '是', handler: function(){
												  thisInput.find("input").attr("checked", true);
													selectedPassenger.checked = 1;
													 var A = [];
													 A['seat_type']     = '';
													 A['user_type']     = selectedPassenger.user_type;
													 A['id_type']       = selectedPassenger.id_type;
													 A['id_no']         = selectedPassenger.id_no;
													 A['user_name']     = selectedPassenger.user_name;
													 A['mobile_no']     = selectedPassenger.mobile_no;
													 A['ticket_type']     = selectedPassenger.user_type;
													 A['p_str']     = selectedPassenger.p_str;
													 A['checked'] = selectedPassenger.checked;
													 A['total_times'] = selectedPassenger.total_times;
													 A['two_totaltimes'] = selectedPassenger.two_totaltimes;
													 A['other_totaltimes'] = selectedPassenger.other_totaltimes;
													 cache_passenger.push(A);
											  }}
											]
										);
								}
						  	}else{
						  		thisInput.find("input").attr("checked", true);
								selectedPassenger.checked = 1;
								 var A = [];
								 A['seat_type']     = '';
								 A['user_type']     = selectedPassenger.user_type;
								 A['id_type']       = selectedPassenger.id_type;
								 A['id_no']         = selectedPassenger.id_no;
								 A['user_name']     = selectedPassenger.user_name;
								 A['mobile_no']     = selectedPassenger.mobile_no;
								 A['ticket_type']     = selectedPassenger.user_type;
								 A['p_str']     = selectedPassenger.p_str;
								 A['checked'] = selectedPassenger.checked;
								 A['total_times'] = selectedPassenger.total_times;
								 A['two_totaltimes'] = selectedPassenger.two_totaltimes;
								 A['other_totaltimes'] = selectedPassenger.other_totaltimes;
								 cache_passenger.push(A);
						  	}
					  }
				}
				
			}
			return false;
		});
//		).on(
//				"taphold", "li",function(e){
//					if(jq(this).attr("data-role")!=null&&jq(this).attr("data-role")=='list-divider'){
//						jq(this).removeClass("ui-btn-active");
//						return false;
//					}
//					var index = jq(this).attr("data-index");
//					var passengers = mor.ticket.passengersCache.passengers;
//					 if (passengers=="" || passengers==undefined){
//						  passengers = [[]];
//					 }
//					var indes = parseInt(index)+1;
//					var selectedPassenger = passengers[index];
//					if(selectedPassenger.user_type == "1" || selectedPassenger.user_type == "4"){
//					var changePassenger = new Object();
//					changePassenger.id_type =selectedPassenger.id_type;
//					changePassenger.id_no =selectedPassenger.id_no;
//					changePassenger.user_name =selectedPassenger.user_name;
//					changePassenger.mobile_no =selectedPassenger.mobile_no;
//					changePassenger.user_type ="2";
//					changePassenger.p_str = selectedPassenger.p_str;
//					changePassenger.user_nameSM = selectedPassenger.user_nameSM;
//					changePassenger.total_times = selectedPassenger.total_times;
//					changePassenger.two_totaltimes = selectedPassenger.two_totaltimes;
//					changePassenger.other_totaltimes = selectedPassenger.other_totaltimes;
//					changePassenger.copy_child = 1;
//					passengers.splice(indes,0,changePassenger);
//					jq("#farePassengerList").html(generatefarePassengerList(passengers)).listview("refresh");
//					showDisabled();
//					}
//					return false;
//				});
		
	};					
	// 初始化复选框
	function showCheckedSelectd(){	
			var passengers = mor.ticket.passengersCache.passengers;
			if(!add_passenger){
				cache_passenger=jq.merge([],mor.ticket.passengerList);
			}else{
				add_passenger = false;
			}
			 if (passengers=="" || passengers==undefined){
				  passengers = [[]];
			 }
			 
			 for(var i=0;i<passengers.length;i++){
				 passengers[i].checked = 0;
				 if(passengers[i].copy_child == 1){
					 passengers.splice(i,1);
					 i = i-1;
				 }
			 }
			 
			 for(var i=0;i<cache_passenger.length;i++){
				var isErTongPassenger = 0; 
				for(var j=0;j<passengers.length;j++){
					var new_id_type = passengers[j].id_type;
					var new_id_no = passengers[j].id_no;
					var new_user_name = passengers[j].user_name;
					var new_user_type = passengers[j].user_type;
					if((new_id_type === cache_passenger[i].id_type) && (new_id_no === cache_passenger[i].id_no) && 
					   (new_user_name === cache_passenger[i].user_name) && (new_user_type == cache_passenger[i].user_type) && 
					   passengers[j].checked != 1){
						passengers[j].checked=1;
						if(passengers[j].user_type=="2"){
							isErTongPassenger = 1;
						}
						break;
					}					
				}
				
				if(cache_passenger[i].user_type=="2" && isErTongPassenger!=1){
					for(var w=0;w<passengers.length;w++){
						var new_id_type_cache = passengers[w].id_type;
						var new_id_no_cache = passengers[w].id_no;
						var new_user_name_cache = passengers[w].user_name;
						if((new_id_type_cache == cache_passenger[i].id_type) && (new_id_no_cache == cache_passenger[i].id_no) &&
							(new_user_name_cache == cache_passenger[i].user_name) && passengers[w].user_type!="2"){
							var newErTongPassengerCache = {};
							newErTongPassengerCache['checked']   = 1;
							newErTongPassengerCache['seat']      = "";
							newErTongPassengerCache['user_name'] = cache_passenger[i].user_name;
							newErTongPassengerCache['id_type'] = cache_passenger[i].id_type;
							newErTongPassengerCache['id_no'] = cache_passenger[i].id_no;
							newErTongPassengerCache['mobile_no'] = cache_passenger[i].mobile_no;
							newErTongPassengerCache['user_type'] = cache_passenger[i].user_type;
							newErTongPassengerCache['total_times'] = passengers[w].total_times;
							newErTongPassengerCache['two_totaltimes'] = passengers[w].two_totaltimes;
							newErTongPassengerCache['other_totaltimes'] = passengers[w].other_totaltimes;
							newErTongPassengerCache['p_str'] = cache_passenger[i].p_str;
							newErTongPassengerCache['user_nameSM'] = passengers[w].user_nameSM;
							newErTongPassengerCache['copy_child'] = 1;
							passengers.splice(w+1,0,newErTongPassengerCache);
						}
					}
				}
			}

			jq("#farePassengerList").html(generatefarePassengerList(passengers)).listview("refresh");
			if(mor.ticket.loginUser.id_type != "1" && mor.ticket.loginUser.id_type != "2"){
				for(var i=0;i<passengers.length;i++){
					if(passengers[i].id_type=='1' || passengers[i].id_type == '2'){
						jq("#farePassengerList li[data-index='"+i+"']").addClass("ui-disabled");
					}
				}	
			}
			showDisabled();
	}
	function showDisabled(){
		var passengers = mor.ticket.passengersCache.passengers;
		for(var i=0;i<passengers.length;i++){
			if(passengers[i].id_type == "2"){
				jq("#farePassengerList li[data-index='"+i+"']").addClass("ui-disabled");
			}
			if(passengers[i].id_type == "1"){
				var arrys = new Array();
				var str = passengers[i].two_totaltimes;
				arrys = str.split("#");
				var flag = Boolean(arrys.in_array(passengers[i].total_times));
				if( flag == false){
					jq("#farePassengerList li[data-index='"+i+"']").addClass("ui-disabled");
				}
			}
				if(passengers[i].id_type !='1' && passengers[i].id_type != '2'){
					var arrys = new Array();
					var str = passengers[i].other_totaltimes;
					arrys = str.split("#");
					var flag = Boolean(arrys.in_array(passengers[i].total_times));
					if( flag == false){
					jq("#farePassengerList li[data-index='"+i+"']").addClass("ui-disabled");
					}
				}				
	}
	};
	Array.prototype.in_array = function(strs){
		for(var j=0;j<this.length&&this[j]!=strs;j++);
			return !(j== this.length);
		}
	var farePassengerListTempate = "{{~it :passenger:index}}"
			+ "{{ if(index==0) { }}"
			+ "<li data-role='list-divider' id='{{=passenger.user_nameSM.toUpperCase().charAt(0)}}'>当前用户</li>"
			+ "{{ } }}"
			+ "{{else if(index==1) { }}"//两个名字首字母相同情况下纠正
			+ "<li data-role='list-divider' id='slectpassengers_{{=it[index].user_nameSM.toUpperCase().charAt(0)}}'>{{=it[index].user_nameSM.toUpperCase().charAt(0)}}</li>"
			+ "{{ } }}"
			+ "	{{else if(passenger.user_nameSM.toUpperCase().charCodeAt(0)!=it[index-1].user_nameSM.toUpperCase().charCodeAt(0)){}}"
			+ "	    <li data-role='list-divider' id='slectpassengers_{{=passenger.user_nameSM.toUpperCase().charAt(0)}}'>{{=passenger.user_nameSM.toUpperCase().charAt(0)}}</li>"
			+ "	{{}}}" 
			+ "<li data-val='' data-index='{{=index}}'><a>"
			+ "<div class='ui-grid-b'>"
			+ " {{ if (passenger.checked==1) { }} "
			
			+ "<div class='ui-block-a'><input id='list_{{=index}}' type=\"checkbox\"  checked value=\"{{=index}}\"></div>"
			
			+ "{{ }else{ }}"
			+ "<div class='ui-block-a'><input id='list_{{=index}}' type=\"checkbox\"  value=\"{{=index}}\"></div>"
			+ "{{ } }}"
			+ "<div class='ui-block-b'>{{=passenger.user_name}}</div>"
			+ "<div class='ui-block-c'>{{=mor.ticket.util.getPassengerTypeName(passenger.user_type)}}</div>"
			+ "	{{if((passenger.total_times =='99'||passenger.total_times =='97'||passenger.total_times =='95'||passenger.total_times =='93')&&passenger.id_type =='1'){}}"
			+ "<div class='ui-block-c'>{{=mor.ticket.util.getIdNoStar(passenger.id_no)}}</div>"
			+ "	{{}else{}}"
			+ "<div class='ui-block-c'>{{=passenger.id_no}}</div>"
			+ "	{{}}}"
			+ "<div class='ui-block-e' style='display:none'>{{=passenger.user_nameSM}}</div>"
			+ "</div></a>"
			+ "</li>"
			+ "{{~}}";
	var generatefarePassengerList = doT.template(farePassengerListTempate);

	var fareSlider_navListTempate =
		"{{var size=0;}}" 
		+ "{{ for(var i=1;i<it.length;i++){ }}"
		+ "	{{if(i==1){}}"
		+ "	    <li><a id='{{=it[i].user_nameSM.toUpperCase().charAt(0)}}_Anchor' href='#{{=it[i].user_nameSM.toUpperCase().charAt(0)}}' class='anchor'>{{=it[i].user_nameSM.toUpperCase().charAt(0)}}</a></li>"
		+ " {{size++;}}"
		+ "	{{}}}"
		+ "	{{else if(it[i].user_nameSM.toUpperCase().charCodeAt(0)!=it[i-1].user_nameSM.toUpperCase().charCodeAt(0)){}}"
		+ "	    <li><a id='{{=it[i].user_nameSM.toUpperCase().charAt(0)}}_Anchor' href='#{{=it[i].user_nameSM.toUpperCase().charAt(0)}}' class='anchor'>{{=it[i].user_nameSM.toUpperCase().charAt(0)}}</a></li>"
		+ " {{size++;}}"
		+ "	{{}}}"
		+ "{{ } }}"
		+ " <input id='selectPassengerNavLength'  type='hidden' value='{{=size}}' />";
	var fareSlider_navList = doT.template(fareSlider_navListTempate);
})();