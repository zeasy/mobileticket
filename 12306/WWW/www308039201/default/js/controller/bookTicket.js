
/* JavaScript content from js/controller/bookTicket.js in folder common */
(function() {
	/*
	 * jq("#bookTicketView").live("pagecreate", function() {
	 * //mor.ticket.util.androidRemoveIscroll("#bookTicketView"); });
	 */
	var tempTipsClosed = "";// 消息临时性关闭
	var focusArray = [];
	var prevPageID = "";
	var prevId = "";
	jq("#bookTicketView").live("pageshow", function() {
		focusArray = [];
	});
	function registerAutoScroll() {
		var util = mor.ticket.util;
		if (util.isIPhone()) {
			util.enableAutoScroll('#timePeriodSelect',focusArray);
			util.enableAutoScroll('#trainSeatInput', focusArray);
		}
	}

	jq("#bookTicketView").live("pagehide", function() {
		jq("#bookTicketView").css("pointer-events", "auto");
	});
	jq("#bookTicketView").live(
			"pageinit",
			function() {
				jq.mobile.defaultHomeScroll = 0;
				var user = mor.ticket.loginUser;
				user.isKeepUserPW = (window.ticketStorage
						.getItem("isKeepUserPW") == null ? true
						: window.ticketStorage.getItem("isKeepUserPW"));
				user.username = window.ticketStorage.getItem("username");
				// 后续优化
				myPrepareCacheMap();
				registerAutoScroll();
				registerStudentSeatTypeListener();
				registerchangeSeatClickHandler();
				registerExitChangeTicketBtnHandler();
				registerFromStationInputClickHandler();
				registerTrainHeadersBtnGroupClickHandler();
				registerToStationInputClickHandler();
				registerTimePeriodSelectChangeHandler();
				registerQuerySingleTicketBtnClickHandler();
				registerSeatSelectChangeHandler(); // 选择席别控制
				registerRecentStationLineListener();
				var passengers = mor.ticket.passengersCache.passengers;
				for(var i =0;i<passengers.length;i++){
					mor.ticket.passengersCache.passengers[i].checked =0;
					
				}
				mor.ticket.passengerList = [];
			});

	jq("#bookTicketView")
			.live(
					"pagebeforeshow",
					function(e, data) {
						prevId = data.prevPage.attr("id");
						if(prevId === "continuePaymentView" || prevId === "orderDetailsView" ){
							prevPageID = prevId;
						}
						showAddedPassager();
						mor.ticket.viewControl.current_tab = "bookTicketTab";
						mor.ticket.viewControl.tab1_cur_page = vPathViewCallBack()
								+ "MobileTicket.html";
						mor.ticket.util.contentIscrollTo(0, 0, 0);
						registerAddFare2fareListBtnClickHandler();// 添加乘客
						initSeatSelectScroller();
						util = mor.ticket.util;

						util.syncDef = util.syncDef || jq.Deferred();
						util.syncDef
								.then(function(serverCache) {
									initTipsMessage();
									mor.ticket.util.initAppVersionInfo();
									var st = new Date(window.ticketStorage
											.getItem('serverTime'));
									var ct = new Date();
									var reservePeriod = calReservedPeriod();
									// 如果系统时间和服务时间不一致， 需要重新渲染。
									if (st.getFullYear() !== ct.getFullYear()
											|| st.getMonth() != ct.getMonth()
											|| st.getDate() != ct.getDate() || reservePeriod !== mor.ticket.history.reservePeriod) {
										// 因为在初始化的时候， 这个内容已经被赋值了，所以有可能是错误的值.
										var currDate = ct.format("yyyy-MM-dd");
										var sysDate = st.format("yyyy-MM-dd");
										var reservePeriodMonth = mor.ticket.history.reservePeriodMonth;
										if(!util.indexOf(reservePeriodMonth,ct.format("yyyyMM"))){
											mor.ticket.leftTicketQuery.train_date = sysDate;
											jq("#trainDateInput").val(mor.ticket.leftTicketQuery.train_date);
										}else{
											var today = mor.ticket.util.getNewDate();
											var todayDate = today.getDate();
											today = today.setDate(todayDate + parseInt(reservePeriod,10) - 1);
											var reserveDay = new Date(today).format("yyyy-MM-dd"); 
											if(sysDate > currDate || currDate > reserveDay){
												mor.ticket.leftTicketQuery.train_date = sysDate;
												jq("#trainDateInput").val(mor.ticket.leftTicketQuery.train_date);
											}
										}
									}
									var canStudentTicket = window.ticketStorage.getItem("canStudentTicket");
									var model = mor.ticket.leftTicketQuery;
									if(canStudentTicket == true || canStudentTicket == "true"){
										jq("#studentSeatType").css("display","block");
										jq("#red").css("display","block");
										if(model.purpose_codes === '00'){
											jq("#red").attr("checked",false).checkboxradio("refresh");
										}else{
											jq("#red").attr("checked",true).checkboxradio("refresh");
										}
									}else{
										jq("#studentSeatType").css("display","none");
										jq("#red").css("display","none");
									}
									initRecentStationLine();
									jq("#bookTicketView .iscroll-wrapper").iscrollview('refresh');
								});
						
						initPageComponent();
						refreshFormFromModel();
						initRecentStationLine();
						//初始化筛选train_hearder
						if(mor.ticket.leftTicketQuery.train_headers == ""){
							jq("#trainHeadersBtnGroup a").eq(0).addClass("ui-btn-active ui-state-persist");
						}else{
							var newtrain_headers = mor.ticket.leftTicketQuery.train_headers.split("#");
							for ( var i = 0, len = jq("#trainHeadersBtnGroup a").length; i < len; i++) {
								var jq_a = jq("#trainHeadersBtnGroup a:eq("
										+ i + ")");
								if(jq_a.hasClass("ui-btn-active ui-state-persist")){
									jq_a.removeClass("ui-btn-active ui-state-persist");
								}
								if (util.indexOf(newtrain_headers,jq_a.attr("id"))) {
									jq_a.addClass("ui-btn-active ui-state-persist");
								}
							}
						}

						//初始化开车时间段
						var new_time_period = mor.ticket.leftTicketQuery.time_period;
						if(mor.ticket.viewControl.bookMode == "fc"){
							new_time_period = mor.ticket.leftTicketQuery.time_period_back;
						}
						
						jq("#timePeriodSelect option").eq(parseInt(new_time_period)).attr("selected",true);
						if(jq("#timePeriodSelect").val() != "0"){
							jq("#timePeriodSelect-button").css("color","#298CCF");
						}else{
							jq("#timePeriodSelect-button").css("color","#000");
						}
						jq("#timePeriodSelect").selectmenu('refresh', true);
						
						//初始化席别
//						mor.ticket.leftTicketQuery.seat_Type = "";
						mor.ticket.leftTicketQuery.seatBack_Type = "";
//						jq("#trainSeatInput").val("0");
//						jq("#trainSeatInput").selectmenu('refresh', true);
						jq("#trainSeatInput").eq(util.getTicketTypeName(mor.ticket.leftTicketQuery.seat_Type)).attr("selected",true);
						if(jq("#trainSeatInput").val() != "0"){
							jq("#trainSeatInput-button").css("color","#298CCF");
						}else{
							jq("#trainSeatInput-button").css("color","#000");
						}
						jq("#trainSeatInput").selectmenu('refresh', true);
						mor.ticket.seat_Type = "";
						mor.ticket.seat_Type_index = "";
//						var model = mor.ticket.leftTicketQuery;
//						getLocation();
					});
	function refreshFormFromModel() {
		/*
		 * 初始化查询车站
		 */
		//修改最近查询的发到站
		var recentStationLineList = window.ticketStorage.
		getItem("recentStationLine") == null ? [] : JSON.
		parse(ticketStorage.getItem("recentStationLine"));
		var model = mor.ticket.leftTicketQuery;
		if (!model.from_station_telecode) {
			model.from_station_telecode = (recentStationLineList.length == 0 ? "BJP"
					: recentStationLineList[recentStationLineList.length-1].split("-")[0]);//window.ticketStorage.getItem("set_from_station_telecode"));
		}
		if (!model.to_station_telecode) {
			model.to_station_telecode = (recentStationLineList.length == 0 ? "SHH"
					: recentStationLineList[recentStationLineList.length-1].split("-")[1]);//window.ticketStorage.getItem("set_to_station_telecode"));
		}
		if (!model.train_date) {
			var date = mor.ticket.util.getNewDate();
			date.setDate(date.getDate());
			if (window.ticketStorage.getItem("set_train_date_type") != null && window.ticketStorage.getItem("set_train_date_type") >= date.format("yyyy-MM-dd") && window.ticketStorage.getItem("set_train_date_type") != "") {
				model.train_date = window.ticketStorage.getItem("set_train_date_type");
			} else {
				date.setDate(date.getDate() + 1);
				model.train_date = date.format("yyyy-MM-dd");
			}
		}

		/*
		 * 页面初始化出发地,目的地,日期,席别等
		 */
		var cache = mor.ticket.cache;
		var reservePeriod = calReservedPeriod();
		if (model.train_date_back === "") {
			var date = mor.ticket.util.setMyDate(model.train_date);
			var today = mor.ticket.util.getNewDate();
			var todayDate = today.getDate();
			today = today.setDate(todayDate
					+ parseInt(reservePeriod,
							10) - 1);
			var reserveDay = new Date(today);
			var date_back;
			if (date < reserveDay || !jq.isNumeric(reservePeriod)) {
				date_back = new Date(date.setDate(date.getDate() + 1));
			} else {
				date_back = date;
			}
			model.train_date_back = date_back.format("yyyy-MM-dd");
		}else{
			var date_back = mor.ticket.util.setMyDate(model.train_date_back);
			var today = mor.ticket.util.getNewDate();
			var todayDate = today.getDate();
			today = today.setDate(todayDate
					+ parseInt(reservePeriod,
							10));
			var reserveDay = new Date(today);
			if (date_back > reserveDay) {
				model.train_date_back = reserveDay.format("yyyy-MM-dd");
			} 
		}
		if(jq("#trainDateInput").hasClass("ui-disabled")){
			jq("#trainDateInput").removeClass("ui-disabled");
		}
		if (mor.ticket.viewControl.bookMode === "fc") {
			jq("#fromStationInput").val(cache.getStationNameByCode(model.from_station_telecode_back));
			jq("#toStationInput").val(cache.getStationNameByCode(model.to_station_telecode_back));
			if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
				model.train_date_back = mor.ticket.currentPagePath.calenderDate;
			}
			jq("#trainDateInput").val(model.train_date_back);
		}else{
			jq("#fromStationInput").val(cache.getStationNameByCode(model.from_station_telecode));
			jq("#toStationInput").val(cache.getStationNameByCode(model.to_station_telecode));
			if(prevId == "datePickerView" && mor.ticket.currentPagePath.calenderDate != ""){
				model.train_date = mor.ticket.currentPagePath.calenderDate;
			}
			jq("#trainDateInput").val(model.train_date);
		}
		var canStudentTicket = window.ticketStorage.getItem("canStudentTicket");
		if(canStudentTicket == true || canStudentTicket == "true"){
			jq("#studentSeatType").css("display","block");
			jq("#red").css("display","block");
			if(model.purpose_codes === '00'){
				jq("#red").attr("checked",false).checkboxradio("refresh");
			}else{
				jq("#red").attr("checked",true).checkboxradio("refresh");
			}
		}else{
			jq("#studentSeatType").css("display","none");
			jq("#red").css("display","none");
		}
		jq("#trainSeatInput").selectmenu('refresh', true);
	}

	/*
	 * 初始化CacheMap 数据
	 */
	function myPrepareCacheMap() {
		prepareCacheMap();
		myPrepareCacheMap = function() {
		};
	}

	/*
	 * 初始化页面状态
	 */

	function initPageComponent() {
		var mode = mor.ticket.viewControl.bookMode;
		var model = mor.ticket.leftTicketQuery;
		var purposeArray = new Array();
		var orderManager = mor.ticket.orderManager;
		var changeOrderList = mor.ticket.queryOrder.changeTicketOrderList;
		jq("#bookTicketView .ui-header>h1").html("车票预订");
		jq("#red").attr("disabled",false);
			if(jq("#fromToStationId").hasClass("ui-disabled")){
			jq("#fromToStationId").removeClass("ui-disabled");
		}
		if (mode === "fc") {
			jq(".ui-header .ui-btn-left").css("background","block");
			jq("#bookTicketView .ui-header>h1").html("车票预订(返程)");
			jq("#exitChangeTicket").show();
			var from_station = model.to_station_telecode;
			var to_station = model.from_station_telecode;
			model.from_station_telecode_back = from_station;
			model.to_station_telecode_back = to_station;
			jq("#AddPassenger").hide();
			jq("#fromToStationId").addClass("ui-disabled");
			var ticket = mor.ticket.confirmTicket;
			if(mor.ticket.util.getTicketTypeName(ticket.ticket_type_code) == "成人票"){
				jq("#red").attr("checked",false).checkboxradio("refresh");
			}else if(mor.ticket.util.getTicketTypeName(ticket.ticket_type_code) == '学生票'){
				jq("#red").attr("checked",true).checkboxradio("refresh");
			}
			purposeArray = jq.grep(orderManager.orderTicketList,function(item, planIndex) {
				return item.ticket_type_code != "3";	
		});	
			if(purposeArray.length >0){
				jq("#red").attr("disabled",true);
			}else{
			jq("#red").attr("disabled",false);
			}
		} else if (mode === "gc") {
			jq("#exitChangeTicket").html("");
			jq(".ui-header .ui-btn-left").css("background","block");
			jq("#bookTicketView .ui-header>h1").html("车票预订("+mor.ticket.changeOrchangeStation.changeOrchangeStation+")");
			jq("#exitChangeTicket").show();
			if(mor.ticket.changeOrchangeStation.changeOrchangeStation === "变更到站"){
				jq("#fromStationInput").addClass("ui-disabled");
				jq("#changeSeat").addClass("ui-disabled");
			}else{
				jq("#fromToStationId").addClass("ui-disabled");
			}
			jq('#AddPassenger').hide();
			var orderDetail = mor.ticket.confirmTicket;
			if(mor.ticket.util.getTicketTypeName(orderDetail.ticket_type_code,orderDetail.seat_no) == "成人票"){
				jq("#red").attr("checked",false).checkboxradio("refresh");
			}else if(mor.ticket.util.getTicketTypeName(orderDetail.ticket_type_code,orderDetail.seat_no) == '学生票'){
				jq("#red").attr("checked",true).checkboxradio("refresh");
			}
			purposeArray = jq.grep(changeOrderList,function(item, planIndex) {
				return item.ticket_type_code != "3";	
		});	
			if(purposeArray.length >0){
				jq("#red").attr("disabled",true);
			}else{
			jq("#red").attr("disabled",false);
			}
		} else {// single trip
			jq("#exitChangeTicket").hide();
			jq("#goInfo").children().removeClass("ui-disabled");
			jq("#fromStationInput").removeClass("ui-disabled");
			jq("#changeSeat").removeClass("ui-disabled");
			jq('#AddPassenger').show();
		}
	}
	function calReservedPeriod(){
		var model = mor.ticket.leftTicketQuery;
		var reservedPeriodType = '1';
		if(model.firstRegist == true || jq("#typeSelect1").css("visibility")=="visible"){
			reservedPeriodType = '1';
			model.purpose_codes = '00';
			jq("#red").attr("checked",false).checkboxradio("refresh");
		}else if(jq("#typeSelect2").css("visibility")=="visible"){
				model.purpose_codes = '0X';
				reservedPeriodType = '3';
				se_codes = '00';
				jq("#red").attr("checked",true).checkboxradio("refresh");
			}
		var reservePeriod = parseInt(window.ticketStorage
				.getItem("reservePeriod_" + reservedPeriodType), 10);
		var reservePeriodMonth = window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType) == null ? [] : 
			JSON.parse(window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType));
		mor.ticket.history.reservePeriodMonth = reservePeriodMonth;
		mor.ticket.history.reservePeriod = reservePeriod;
		model.firstRegist = false;
		return reservePeriod;
	}
	jq("#trainDateInput").bind("tap",function(){
		var current = mor.ticket.currentPagePath;
		var monthValue;
		var model = mor.ticket.leftTicketQuery;
		current.defaultDateValue[0] = jq("#trainDateInput").val().slice(0,4);
		monthValue = parseInt(jq("#trainDateInput").val().slice(5,7));
		if(monthValue == "1"){
			monthValue = "0";
		}else{
			monthValue = monthValue-1;
		}
		if(monthValue < 10){
			current.defaultDateValue[1] = "0"+monthValue;
		}else{
			current.defaultDateValue[1] = monthValue;
		}
		current.defaultDateValue[2]=jq("#trainDateInput").val().slice(8,10);
		var reservedPeriodType = "1";
		if(jq("#red").is(":checked")){
			model.purpose_codes = '0X';
			reservedPeriodType = "3";
		}else{
			model.purpose_codes = '00';
			reservedPeriodType = "1";
		}
		var reservePeriod = parseInt(window.ticketStorage
				.getItem("reservePeriod_" + reservedPeriodType), 10);
		mor.ticket.history.reservePeriod = reservePeriod;
		jQuery.extend(jQuery.mobile.datebox.prototype.options, {
			mode:"calbox",
			useInline: true,
			calControlGroup: true,
			defaultValue: current.defaultDateValue,
			showInitialValue: true,
			lockInput: false,
			useFocus: true,
			maxDays: reservePeriod,
			buttonIcon: "grid",
			calUsePickers: true,
			afterToday: true,
			beforeToday: false,
			notToday: false,
			calOnlyMonth: false,
			overrideSlideFieldOrder: ['y','m','d','h','i']
		});
		mor.ticket.currentPagePath.fromPath = "../MobileTicket.html";
		var goingPath = vPathCallBack()+ "datePicker.html";
		mor.ticket.currentPagePath.type = true;
		mor.ticket.currentPagePath.pickYears = false;
		mor.ticket.currentPagePath.show = true;
		var reservePeriodMonth = window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType) == null ? [] : 
			JSON.parse(window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType));
		mor.ticket.history.reservePeriodMonth = reservePeriodMonth;
		jq("#datePickerInputHidden").trigger('datebox',{'method':'changeMaxDays'});
		mor.ticket.util.changePage(goingPath);
		return false;
	});

	/*
	 * 初始化席位
	 */
	function initSeatSelectScroller() {
		var jq_dateInput = jq("#trainSeatInput");
		jq_dateInput.empty();
		mor.ticket.seatName = [ '不限', '特等座', '一等座', '二等座', '高级软卧', '软卧', '硬卧',
				'软座', '硬座', '无座' ];
		mor.ticket.seatId = [ '0', '9', 'P', 'M', 'O', '6', '4', '3', '2', '1' ];
		// 商务座
		var htmlStr = null;
		htmlStr += "<option " + trainSeatSelectd('') + " value='0'>不限</option>";
		htmlStr += "<option " + trainSeatSelectd('9')
				+ " value='9'>商务座</option>";
		htmlStr += "<option " + trainSeatSelectd('P')
				+ " value='P'>特等座</option>";
		htmlStr += "<option " + trainSeatSelectd('M')
				+ " value='M'>一等座</option>";
		htmlStr += "<option " + trainSeatSelectd('O')
				+ " value='O'>二等座</option>";
		htmlStr += "<option " + trainSeatSelectd('6')
				+ " value='6'>高级软卧</option>";
		htmlStr += "<option " + trainSeatSelectd('4')
				+ " value='4'>软卧</option>";
		htmlStr += "<option " + trainSeatSelectd('3')
				+ " value='3'>硬卧</option>";
		htmlStr += "<option " + trainSeatSelectd('2')
				+ " value='2'>软座</option>";
		htmlStr += "<option " + trainSeatSelectd('1')
				+ " value='1'>硬座</option>";

		jq_dateInput.append(htmlStr);
	}
	;

	/*
	 * 返回单程出发席位状态选择
	 */

	function trainSeatSelectd(val) {
		if (mor.ticket.leftTicketQuery.seat_Type == "" && val == "") {
			return "selected='selected'";
		} else if (mor.ticket.leftTicketQuery.seat_Type == val) {
			return "selected='selected'";
		} else {
			return;
		}
	}

	/*
	 * 返回往返席位状态选择
	 */
	function trainSeatInputBackSelectd(val) {
		if (mor.ticket.leftTicketQuery.seatBack_Type == "" && val == "") {
			return "selected='selected'";
		} else if (mor.ticket.leftTicketQuery.seatBack_Type == val) {
			return "selected='selected'";
		} else {
			return;
		}
	}

	/*
	 * 返回日期的星期
	 */
	function getWeek(prompt) {
		var desc = [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ];
		if (typeof prompt == "string") {
			var date = mor.ticket.util.setMyDate(prompt);
			return desc[date.getDay()];
		} else {
			return desc[prompt.getDay()];
		}
	}

	/*
	 * 返回乘客的左右样式
	 */
	function getFareBtnStyle(i) {
		if (i == 1) {
			return "ui-corner-left";
		} else if (i == 5) {
			return "ui-corner-right";
		} else {
			return '';
		}
	}

	/*
	 * 显示选择乘客列表
	 */
	function showAddedPassager() {
		var auto_i = 0;
		var html = '';
		mor.ticket.passengerList.sort();
		jq.each(mor.ticket.passengerList,
				function(i, passenger) {
					if (passenger.id_no && passenger.id_no != 'undefined') {
						auto_i++;
						html += createBtn(auto_i,
									'ui-exit-btn ui-btn text-ellipsis '
											+ getFareBtnStyle(auto_i),
									passenger.user_name);

					}
				});
		if (auto_i < 5) {
			auto_i++;
			html += createBtn(auto_i, 'ui-btn add-btn '
					+ getFareBtnStyle(auto_i));
		}

		var index = 5 - auto_i;

		for ( var autos = 1; autos <= index; autos++) {
			auto_i++;
			html += createBtn(auto_i, 'ui-btn '
					+ getFareBtnStyle(auto_i));
		}

		jq("#FareHeadersBtnGroup .ui-controlgroup-controls").html(html);
		setTimeout(registerAddFare2fareListBtnClickHandler, 20);
		return;
	}

	function createBtn(index, clazz, val) {
		var html = '<a ';
		if (val) {
			html += 'data-index="' + index + '" ';
		}
		html += 'data-role="button" href="#" id="A_' + index
				+ '" ';
		if (clazz) {
			html += 'class="' + clazz + '"';
		}
		html += '>';
		if (val) {
			html += val;
		}else if(clazz.indexOf('add-btn') != -1){
			html += '<span class="add-btn-tip">乘客</span>';
		}
		return html += '</a>';
	}

	/*
	 * 出发地和目的地交换数据
	 */

	function registerchangeSeatClickHandler() {

		jq("#changeSeat").live("tap", function() {

			var model = mor.ticket.leftTicketQuery;

			var fromStationInput = jq("#toStationInput").val();
			var toStationInput = jq("#fromStationInput").val();

			jq("#toStationInput").val(toStationInput);
			jq("#fromStationInput").val(fromStationInput);

			var from_station = model.to_station_telecode;
			var to_station = model.from_station_telecode;

			model.from_station_telecode = from_station;
			model.to_station_telecode = to_station;

			return false;
		});
//		jq("#travelPlanChoose").live("tap",function(e){
//			e.stopImmediatePropagation();
//			mor.ticket.util.changePage(vPathCallBack() + "travelPlan.html");
//			return false;
//		});
	}
	
	function registerFromStationInputClickHandler() {

		jq("#fromStationInput").bind("tap", function(e) {
			e.stopImmediatePropagation();
			jq("#bookTicketView").css("pointer-events", "none");
			mor.ticket.views.selectStation.isFromStation = true;
			mor.ticket.util.changePage(vPathCallBack() + "selectStation.html");
			return false;
		});
	}
	;

	function registerToStationInputClickHandler() {

		jq("#toStationInput").bind("tap", function(e) {
			e.stopImmediatePropagation();
			jq("#bookTicketView").css("pointer-events", "none");
			mor.ticket.views.selectStation.isFromStation = false;
			mor.ticket.util.changePage(vPathCallBack() + "selectStation.html");
			return false;
		});
	}
	;

	function registerStudentSeatTypeListener(){
		jq("#red").bind("change",function(){
			if(!jq(this).is(":checked")){
				var currSelectedDate = jq("#trainDateInput").val();
				var date = mor.ticket.util.setMyDate(currSelectedDate);
				var today = mor.ticket.util.getNewDate();
				var todayDate = today.getDate();
				mor.ticket.leftTicketQuery.purpose_codes = '00';
				var reservePeriod = parseInt(window.ticketStorage
						.getItem("reservePeriod_1"), 10);
				mor.ticket.history.reservePeriod = reservePeriod;
				today = today.setDate(todayDate + parseInt(reservePeriod));
				var reserveDay = new Date(today);
				if (date > reserveDay) {
					jq("#trainDateInput").attr("value",reserveDay.format("yyyy-MM-dd"));
				}
				mor.ticket.leftTicketQuery.train_date = jq("#trainDateInput").val();
			}
			else{
				var currSelectedDate = jq("#trainDateInput").val();
				var date = mor.ticket.util.setMyDate(currSelectedDate);
				var today = mor.ticket.util.getNewDate();
				var todayDate = today.getDate();
				mor.ticket.leftTicketQuery.purpose_codes = '0X';
				var reservePeriod = parseInt(window.ticketStorage
						.getItem("reservePeriod_1"), 10);
				mor.ticket.history.reservePeriod = reservePeriod;
				today = today.setDate(todayDate + parseInt(reservePeriod));
				var reserveDay = new Date(today);
				if (date > reserveDay) {
					jq("#trainDateInput").attr("value",reserveDay.format("yyyy-MM-dd"));
				}
				mor.ticket.leftTicketQuery.train_date = jq("#trainDateInput").val();
			}
		});
	}
	function registerSeatSelectChangeHandler() {

		mor.ticket.util.bindSelectFocusBlurListener("#trainSeatInput");

		jq("#trainSeatInput").bind("change", function() {
			if(mor.ticket.viewControl.bookMode == 'fc'){
				if (jq(this).val() == 0) {
					mor.ticket.leftTicketQuery.seatBack_Type = '';
				} else {
					mor.ticket.leftTicketQuery.seatBack_Type = jq(this).val();
				}
			}else{
				if (jq(this).val() == 0) {
					mor.ticket.leftTicketQuery.seat_Type = '';
				} else {
					mor.ticket.leftTicketQuery.seat_Type = jq(this).val();
				}
			}			
			if(jq(this).val() == "0"){
				jq("#trainSeatInput-button").css("color","black");
			}else{
				jq("#trainSeatInput-button").css("color","#298CCF");
			}
			return false;
		});

	}
	;
	
	function registerTimePeriodSelectChangeHandler() {
		mor.ticket.util.bindSelectFocusBlurListener("#timePeriodSelect");
		jq("#timePeriodSelect").bind("change", function() {
			if(mor.ticket.viewControl.bookMode == 'fc'){
				mor.ticket.leftTicketQuery.time_period_back = jq(this).val();
			}else{
				mor.ticket.leftTicketQuery.time_period = jq(this).val();
			}
			if(jq(this).val() == "0"){
				jq("#timePeriodSelect-button").css("color","black");
			}else{
				jq("#timePeriodSelect-button").css("color","#298CCF");
			}
			return false;
		});
	}
	;
	
	function registerTrainHeadersBtnGroupClickHandler() {
		jq("#trainHeadersBtnGroup").on("tap", "a", function() {
			if(!mor.ticket.datebox.calbox){
				return false;
			}
			var activeCls = "ui-btn-active ui-state-persist";
			var that = jq(this);
			var id = that.attr('id');
			var trainHeaders = mor.ticket.searchResultScreen.trainHeaders;
			if (that.hasClass(activeCls)) {
				if (id != "QB") {
					that.removeClass(activeCls);
					trainHeaders.splice(jq.inArray(id,trainHeaders),1);
					// 判断如果出了全部之外的alink都没有ui-btn-active
					// ui-state-persist，那么给QB添加此属性
					if (!jq("#D,#Z,#T,#K,#QT").hasClass(activeCls)) {
						mor.ticket.searchResultScreen.trainHeaders = [];
						jq("#QB").addClass(activeCls);
					}
				}
			} else {
				if (id == "QB") {
					mor.ticket.searchResultScreen.trainHeaders = [];
					that.addClass(activeCls).siblings().removeClass(activeCls);
				} else {
					that.addClass(activeCls);
					trainHeaders.push(id);
					if (jq("#QB").hasClass(activeCls)) {
						jq("#QB").removeClass(activeCls);
					}
				}
			}
			mor.ticket.leftTicketQuery.train_headers = "";
			for(var i = 0;i<mor.ticket.searchResultScreen.trainHeaders.length;i++){
				mor.ticket.leftTicketQuery.train_headers += mor.ticket.searchResultScreen.trainHeaders[i]+"#"; 
			}
			return false;
		});
	}

	/*
	 * 验证输入框是否选择数据
	 */
	function validateInputs() {
		var query = mor.ticket.leftTicketQuery;
		if (mor.ticket.viewControl.bookMode === "fc") {
			if (query.from_station_telecode_back == '') {
				mor.ticket.util.alertMessage("出发地不能为空");
				return false;
			} else if (query.to_station_telecode_back == '') {
				mor.ticket.util.alertMessage("目的地不能为空");
				return false;
			}
			;
		} else {
			if (query.from_station_telecode == ''
					|| jq('#fromStationInput').val() == '') {
				mor.ticket.util.alertMessage("出发地不能为空");
				return false;
			} else if (query.to_station_telecode == ''
					|| jq('#toStationInput').val() == '') {
				mor.ticket.util.alertMessage("目的地不能为空");
				return false;
			}
		}
		if (mor.ticket.viewControl.bookMode == "fc") {
			var model = mor.ticket.leftTicketQuery;
			var wcDate = mor.ticket.util.setMyDate(model.train_date);
			var fcDate = mor.ticket.util.setMyDate(model.train_date_back);
			if (fcDate < wcDate) {
				mor.ticket.util.alertMessage("返回日期需要大于出发日期!");
				return false;
			}
		}
		return true;
	}

	function registerQuerySingleTicketBtnClickHandler() {
		jq("#querySingleTicketBtn")
				.bind(
						"tap",
						function(e) {
							e.stopImmediatePropagation();
							//add by zzc
							mor.ticket.searchResultList.leftOrPriceFlag = "1";
							var model = mor.ticket.leftTicketQuery;
							var reservedPeriodType = "1";
							if(jq("#red").is(":checked")){
								model.purpose_codes = '0X';
								reservedPeriodType = "3";
							}else{
								model.purpose_codes = '00';
								reservedPeriodType = "1";
							}
							var reservePeriod = parseInt(window.ticketStorage
									.getItem("reservePeriod_" + reservedPeriodType), 10);
							mor.ticket.history.reservePeriod = reservePeriod;
							var reservePeriodMonth = window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType) == null ? [] : 
								JSON.parse(window.ticketStorage.getItem("reservePeriodMonth_" + reservedPeriodType));
							mor.ticket.history.reservePriodMonth = reservePeriodMonth;
							if (model.from_station_telecode == model.to_station_telecode) {
								mor.ticket.util.alertMessage("目的地不能与出发地相同！");
								return false;
							}

							if (validateInputs()) {
								var query = mor.ticket.leftTicketQuery;
								var filterQuery = mor.ticket.searchResultScreen;
								query.result = [];
								query.train_headers = "";
								query.train_flag = "";
								filterQuery.clear();
								for ( var i = 0, len = jq("#trainHeadersBtnGroup a").length; i < len; i++) {
									var jq_a = jq("#trainHeadersBtnGroup a:eq("
											+ i + ")");
									if (jq_a
											.hasClass("ui-btn-active ui-state-persist")) {
										query.train_headers += jq_a.attr("id")
												+ "#";
										filterQuery.trainHeaders.push(jq_a.attr("id"));
									}
								}
								if (query.train_headers == "") {
									query.train_headers = "QB#";
									jq("#QB").addClass(
											"ui-btn-active ui-state-persist");
									filterQuery.trainHeaders.push("QB");
								}
								var seatType = jq("#trainSeatInput").val();
								if(seatType !== "0"){
									filterQuery.seat_type_checked.push(seatType);
								}
								
								var start_time_begin = mor.ticket.util.getStartTimeBeginCode(jq("#timePeriodSelect").val());
								var start_time_end = mor.ticket.util.getStartTimeEndCode(jq("#timePeriodSelect").val());
								var start_time= start_time_begin.substring(0,2)+""+start_time_end.substring(0,2);
								filterQuery.start_time = start_time;
								
								if (!busy.isVisible()) {
									busy.show();
								}
								mor.ticket.history.url == '';
								mor.ticket.util.changePage(vPathCallBack() + "searchSingleTicketResults.html");		
							}

							return false;
						});
	}

	function registerExitChangeTicketBtnHandler() {
		jq("#exitChangeTicket").bind(
				"tap",
				function(e) {
					e.stopImmediatePropagation();
					if(!mor.ticket.datebox.calbox){
						return false;
					}
					if (mor.ticket.viewControl.bookMode === "gc") {
						mor.ticket.viewControl.bookMode = "dc";
						mor.ticket.util.changePage(vPathCallBack()
								+ "finishedOrderDetail.html");
						jq("#red").attr("disabled",false);
					}
					if (mor.ticket.viewControl.bookMode === "fc") {
						mor.ticket.viewControl.bookMode = "dc";
						if(prevPageID === "orderDetailsView"){
							mor.ticket.util.changePage(vPathCallBack()
									+ "orderDetails.html");
						}else if(prevPageID === "continuePaymentView"){
							mor.ticket.util.changePage(vPathCallBack()
									+ "continuePayment.html");
						}else{
							jq.mobile.changePage(vPathViewCallBack()
									+ "MobileTicket.html", {
								allowSamePageTransition : true,
								transition : 'none',
								reloadPage : false
							});
						}
						jq("#red").attr("disabled",false);
					}
					return false;
				});
	}
	/**
	 * function refreshPage(page){ // Page refresh page.trigger('pagecreate');
	 * //page.listview('refresh'); }
	 */
	// add by yiguo
	function addPassenger() {
		// mod by yiguo
		jq("#FareHeadersBtnGroup a.add-btn").off();
		var usercheck = mor.ticket.loginUser;
		mor.ticket.history.url = 'bookticket';
		jq("#selectFarePassengerView").remove();
		mor.ticket.util.changePage(vPathCallBack() + "fareList.html");
		setTimeout(function() {
			jq("#FareHeadersBtnGroup a.add-btn").off().on("tap", addPassenger)
		}, 1000);
		return false;
	}
	function registerAddFare2fareListBtnClickHandler() {
		// mod by yiguo
		jq("#FareHeadersBtnGroup a.add-btn").off().on("tap", addPassenger);
		jq("#FareHeadersBtnGroup a.ui-exit-btn").off().on(
				"tap",
				function() {
					var node = jq(this);
					var index = jq(this).attr("data-index");
					if (index) {
						if(mor.ticket.util.isAndroid()){
							WL.SimpleDialog.show("温馨提示", "确定删除该乘客吗?",
									[
	       								{
	       									text : "确认",
	       									handler : function() {
	       										node.removeClass('ui-btn-hover-c')
	       												.removeClass('ui-btn-down-c')
	       												.addClass('ui-btn-up-c');
	       										
	       										var passengers = mor.ticket.passengersCache.passengers;
	       										for(var i=0; i < passengers.length; i++){
	       											if(mor.ticket.passengerList[index-1].id_no==passengers[i].id_no 
	       													 && mor.ticket.passengerList[index-1].user_name==passengers[i].user_name
	       													 && mor.ticket.passengerList[index-1].user_type==passengers[i].user_type
	       													 && mor.ticket.passengerList[index-1].checked==passengers[i].checked){
	       												passengers[i].checked =0;
	       												break;
	       											}
	       										}
	       										mor.ticket.passengerList.splice(index - 1, 1);
	       										showAddedPassager();
	       										return;
	       									}
	       								},
	       								{
	       									text : "取消",
	       									handler : function() {
	       										node.removeClass('ui-btn-hover-c')
	       												.removeClass('ui-btn-down-c')
	       												.addClass('ui-btn-up-c');
	       										return false;
	       									}
	       								}
	       							]);
							}
						else{
							WL.SimpleDialog.show("温馨提示", "确定删除该乘客吗?",
								[
								 	{
										text : "取消",
										handler : function() {
											node.removeClass('ui-btn-hover-c')
													.removeClass('ui-btn-down-c')
													.addClass('ui-btn-up-c');
											return false;
										}
									},
       								{
       									text : "确认",
       									handler : function() {
       										node.removeClass('ui-btn-hover-c')
       												.removeClass('ui-btn-down-c')
       												.addClass('ui-btn-up-c');
       										
       										var passengers = mor.ticket.passengersCache.passengers;
       										for(var i=0; i < passengers.length; i++){
       											if(mor.ticket.passengerList[index-1].id_no==passengers[i].id_no 
       													 && mor.ticket.passengerList[index-1].user_name==passengers[i].user_name
       													 && mor.ticket.passengerList[index-1].user_type==passengers[i].user_type
       													 && mor.ticket.passengerList[index-1].checked==passengers[i].checked){
       												passengers[i].checked =0;
       												break;
       											}
       										}
       										mor.ticket.passengerList.splice(index - 1, 1);
       										showAddedPassager();
       										return;
       									}
       								}
       							]);
					}
						}
						
				}).on(
				"taphold",
				function() {
					var node = jq(this);
					if(mor.ticket.util.isAndroid()){
						WL.SimpleDialog.show("温馨提示", "确定删除所有的乘客吗?", 
								[
         							{
         								text : "确认",
         								handler : function() {
         									node.removeClass('ui-btn-hover-c')
         											.removeClass('ui-btn-down-c')
         											.addClass('ui-btn-up-c');
         									mor.ticket.passengerList = [];
         									var passengers = mor.ticket.passengersCache.passengers;
         									for(var i=0; i < passengers.length; i++){
         											passengers[i].checked =0;
         				
         									}
         									showAddedPassager();
         									return;
         								}
         							} ,
         							{
         								text : "取消",
         								handler : function() {
         									node.removeClass('ui-btn-hover-c')
         											.removeClass('ui-btn-down-c')
         											.addClass('ui-btn-up-c');
         									return false;
         								}
         							}
         						]);		
					}
					else{
						WL.SimpleDialog.show("温馨提示", "确定删除所有的乘客吗?", 
								[
         							{
         								text : "取消",
         								handler : function() {
         									node.removeClass('ui-btn-hover-c')
         											.removeClass('ui-btn-down-c')
         											.addClass('ui-btn-up-c');
         									return false;
         								}
         							},
         							{
         								text : "确认",
         								handler : function() {
         									node.removeClass('ui-btn-hover-c')
         											.removeClass('ui-btn-down-c')
         											.addClass('ui-btn-up-c');
         									mor.ticket.passengerList = [];
         									var passengers = mor.ticket.passengersCache.passengers;
         									for(var i=0; i < passengers.length; i++){
         											passengers[i].checked =0;
         				
         									}
         									showAddedPassager();
         									return;
         								}
         							}
         						]);
					}
					
				});
	}
	
	// 常用线路展示
	function initRecentStationLine(){
		if (mor.ticket.viewControl.bookMode != "dc") {
			jq(".cylx").hide();
			jq(".cylx2").hide();
			return false;
		}
		var recentStationLineList = window.ticketStorage.
			getItem("recentStationLine") == null ? [] : JSON.
			parse(ticketStorage.getItem("recentStationLine"));
		var cache = mor.ticket.cache;
//		var jq_cyxl = "<div class='departshortline''></div>" + 
//			"<a class='a_btn departshortline ui-link' id='travelPlanChoose'>出行计划 </a>";
        var jq_cyxl = "";
		for(var i = recentStationLineList.length-1 ; i >= 0 ; i--){
			var stationLine = recentStationLineList[i].split("-");
			jq_cyxl += "<a class='a_btn departshortline ui-link' id='"+recentStationLineList[i]+"'>" + cache.getStationNameByCode(stationLine[0]) + "--" 
					  + cache.getStationNameByCode(stationLine[1]) + "</a>";
		}
		if(jq_cyxl != ""){
		    jq_cyxl = "<div class='departshortline''></div>" + jq_cyxl;
			jq(".cylx2").html(jq_cyxl);
			jq(".cylx").show();
			jq(".cylx2").show();
		}
		jq("#bookTicketView .iscroll-wrapper").iscrollview('refresh');
	}
	// 提示信息
	function initTipsMessage(){
		var tipsClosed = window.ticketStorage.getItem("tipsClosed") == null ? "" :
			window.ticketStorage.getItem("tipsClosed");
		var tipsMessage = window.ticketStorage.getItem("tipsMessage") == null ? "" :
			window.ticketStorage.getItem("tipsMessage");
		var tipsDate = window.ticketStorage.getItem("tipsDate") == null ? "" : 
			JSON.parse(window.ticketStorage.getItem("tipsDate"));
		// 消息有效期
		var tipsExpDate = window.ticketStorage.getItem("tipsExpDate") == null ? "" : 
			JSON.parse(window.ticketStorage.getItem("tipsExpDate"));
		// 当前日期
		var currDate = mor.ticket.util.getNewDate().format("yyyyMMdd")
		if(currDate > tipsExpDate){
			return false;
		}
		// 不再显示
		if(tipsClosed != "" && tipsDate == tipsClosed){
			return false;
		}
		// 临时性关闭
		if(tempTipsClosed != ""){
			return false;
		}
		if(tipsMessage != "" && tipsDate != ""){
			jq("#tipsMessage").html(tipsMessage);
			jq(".cylx3Group").show();
			jq("#bookTicketView .iscroll-wrapper").iscrollview('refresh');
		}
	}
	function registerRecentStationLineListener(){
	jq(".cylx2").off().on("tap","a",function(e){
		//jq(".cylx2").off().on("tap","a[id!='travelPlanChoose']",function(e){
			var recentStationLine = jq(this).attr("id").split("-");
			var model = mor.ticket.leftTicketQuery;
			model.from_station_telecode = recentStationLine[0];
			model.to_station_telecode = recentStationLine[1];
			refreshFormFromModel();
			jq("#bookTicketView .iscroll-wrapper").iscrollview('refresh');
			return false;
		});
		// 关闭消息显示
		jq("#closeTips").bind("tap",function() {
			WL.SimpleDialog.show("温馨提示", "确定关闭提示消息吗?", [
					{
						text : "取消",
						handler : function() {
							return false;
						}
					},
					{
						text : "关闭",
						handler : function() {
							jq(".cylx3Group").hide();
							tempTipsClosed = "closed";
							return;
						}
					},
					{
						text : "不再显示",
						handler : function() {
							jq(".cylx3Group").hide();
							window.ticketStorage.setItem("tipsClosed", window.ticketStorage.getItem("tipsDate"));
							return;
						}
					} 
					]);
			
		});
	}

	function getLocation(){
		if(navigator.geolocation){
			navigator.geolocation.getCurrentPosition(getPositionSuccessCallback, getPositionErrorCallback);
		}else{
			mor.ticket.util.alertMessage("服务没开启!");
			return false;
		}
	}
	function getPositionSuccessCallback(position){
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
		mor.ticket.util.alertMessage("你所在位置：经度("+lat+"),纬度("+lng+")");
		return false;
	} 
	function getPositionErrorCallback(error){
		
	}
})();