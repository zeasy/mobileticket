/**
 * @providesModule Function
 * Created by easy on 15/11/17.
 */

"use strict";
var NativeFunction = require('NativeModules').NativeFunction;
var Interface = require('Interface');

class Function {
    static async asyncCall(func,parameters) {
        return Interface.asyncInvoke(NativeFunction.call,func,parameters);
    }
}

exports = module.exports = Function;