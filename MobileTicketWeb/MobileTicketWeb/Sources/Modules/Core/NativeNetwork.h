//
//  NativeNetwork.h
//  MobileTicketWeb
//
//  Created by EASY on 16/1/13.
//  Copyright © 2016年 Z.EASY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RCTBridgeModule.h>

@interface NativeNetwork : NSObject <RCTBridgeModule>

@end
