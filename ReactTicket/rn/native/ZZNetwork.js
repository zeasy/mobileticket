/**
 * @providesModule ZZNetwork
 * Created by easy on 16/3/19.
 */

let NativeNetwork = require('NativeModules').NativeNetwork;

class ZZNetwork {
    static async request(options) {
        return NativeNetwork.request({url:'',format:'',method:'',cookies:[],headers:{},params:{},...options});
    }
}

module.exports = ZZNetwork;