
/* JavaScript content from js/controller/pushmessage.js in folder common */
(function(){
	var popIndex=0;
	jq("#pushmessageView").live("pageinit", function(){
		jq("#pushmessageBackBtn").off().bind("tap",function(e){
			mor.ticket.util.changePage("about.html");
			return false;
		});
		jq("#pushmessage-pulldown").show();
		jq("#push_content", this).bind( {
			"iscroll_onpulldown" : onPullDown,
			"iscroll_onpullup"	 : onPullUp
		});
		jq("#push_scroller").off();
		jq.event.special.tap.emitTapOnTaphold = false;
	});

	jq("#pushmessageView").live("pageshow", function(){
		loadingPushMessage("first");
		updateMenuIcon();
	});
	
	//往下拉是重新刷新
	function onPullDown(event, data){
		loadingPushMessage("pulldown");
		setTimeout(function(){							//这个定时函数可以控制上拉框的显示时间
			data.iscrollview.refresh();
			updateMenuIcon();
		},1500);
	}
	//往上拉是加载更多
	function onPullUp(event, data){
		loadingPushMessage("pushup");
		setTimeout(function(){							//这个定时函数可以控制上拉框的显示时间
			data.iscrollview.refresh();
			updateMenuIcon();
		},1500);
	}
	
	function loadingPushMessage(freshType){
		var count = window.ticketStorage.pushmessageNum();
		var displayedNum=0,showNum =0,showHTML="";			//displayedNum表示加载前页面显示的信息个数，showNum表示即将需要加载的信息的个数
		try{
			showNum = document.getElementById("push_scroller").children.length;
		}
		catch(e){}
		/*
		window.localStorage.clear();
		for(var i=0;i<15;i++){
			var value = {};
			value.time = ((new Date()).toString());
			value.readOrNo = false;
			value.message = "每个数据区域内含有若干个数据单元。无论用户选择的是简单查询还是复合查询，分布式系统能够根据车次信息自动定位到包含该车次的所有数据内存单元，同时与这些内存单元对应的CPU会将数据读取出来，进行运算。";
			if(i<5){
				value.theme = "测试主题"+i;
			}else{
				value.theme = "ceshizhutin比较测试主题全评测和铁道部运输dsfdffgdf局的技术评审"+i;
			}			
			var str = JSON.stringify(value);
			window.ticketStorage.setItem("pushmessage"+i,str);		
		}
		alert("加载完毕");
		return;
*/
		if(showNum === null || showNum === undefined){
			showNum = 0;
			displayedNum = 0;
		}else{
			displayedNum = showNum;
		}
		if(freshType === "first" && displayedNum > 0 && count != 0){
			return;
		}
		if(freshType === "pulldown" || freshType === "first"){
			if(count === 0){
				//showHTML += "<li id='pushNo'>没有任何推送消息</li>";
				mor.ticket.util.alertMessage("没有新的系统消息。");
				jq("#pushmessage-pulldown").show();
				jq("#pushmessage-pullup").hide();
				return;
			}else{
				jq("#pushmessage-pulldown").show();
				if(count< 10){
					showNum = count;
					jq("#pushmessage-pullup").hide();
				}else{
					if(showNum === null || showNum === undefined || showNum < 10){
						showNum = 10;
					}else{
						showNum = (count - showNum >10)?showNum:count;
						jq("#pushmessage-pullup").show();
					}
				}
			}
			for(var i =0;i<showNum;i++){
				var pushmessage = window.ticketStorage.getItem("pushmessage"+i);
				if(pushmessage === null || pushmessage === undefined ||pushmessage.length ===0)
					continue;
				try{
					pushmessage = jq.parseJSON(pushmessage);
					if(pushmessage.theme === "" || pushmessage.theme === undefined || pushmessage.theme === null || pushmessage.theme === "推送主题"){
						pushmessage.theme = "无主题";
					}
					var time = formatShowTime(pushmessage.time);
					if(pushmessage.readOrNo === true){			//消息已读
						showHTML += ("<li id='pushmessage"+i+"' class='messageContainer alreadyread'>"
						+	"<span style='display:none;'>true</span>"
						+	"<div class='message-theme-field'>"
						+		"<label class='theme-top-left'>"+pushmessage.theme+"</label>"
						+		"<div class='time-top-right'>" + time + "</div>"
						+	"</div>"
						+	"<div id='messageContent' style='display:none;'>" + pushmessage.message + "</div>"
						+"</li>");
					}else{								//消息未读
						showHTML += ("<li id='pushmessage"+i+"' class='messageContainer noread'>"
						+	"<span style='display:none;'>false</span>"
						+	"<div class='message-theme-field'>"
						+		"<label class='theme-top-left'>"+pushmessage.theme+"</label>"
						+		"<div class='time-top-right'>" + time + "</div>"
						+	"</div>"
						+	"<div id='messageContent' style='display:none;'>" + pushmessage.message + "</div>"
						+"</li>");
					}
				}catch(e){
					alert("解析pussmessage内容出错！错误原因为e.message："+e.message + " !。 此时循环的的推送内容为第："+i +"个");
				}
			}
			jq("#push_scroller").html(showHTML);
			jq('#popupDiv').popup();
			if(showNum -displayedNum === 0){
				jq("#pushmessage_top_info").html("<span>已更新到最新！</span>");
			}else{
				jq("#pushmessage_top_info").html("<span>更新了"+(showNum -displayedNum) +"条推送信息。</span>");
			}
			jq("#pushmessage_top_info").show();
			jq("#pushmessage_top_info").fadeOut(2500);
			if(showNum >= 10){
				jq("#pushmessage-pullup").show();
			}		
			jq("#push_scroller").listview("refresh");
		}else if(freshType === "pushup"){
			if(displayedNum < 10){
				jq("#pushmessage-pullup").hide();
				return;
			}
			if(displayedNum >= 10){
				jq("#pushmessage-pullup").show();
			}
			showNum =(count - displayedNum > 10 ? (displayedNum + 10):count);
			for(var j = displayedNum;j < showNum;j++){
				var appendPushmessage = window.ticketStorage.getItem("pushmessage"+j);
				if(appendPushmessage === null || appendPushmessage === undefined ||appendPushmessage.length ===0)
					continue;
				try{
					appendPushmessage = jq.parseJSON(appendPushmessage);
					if(appendPushmessage.theme === "" || appendPushmessage.theme === undefined  || appendPushmessage.theme === null   || appendPushmessage.theme === "推送主题"){
						appendPushmessage.theme = "无主题";
					}
					var showtime = formatShowTime(appendPushmessage.time);
					if(appendPushmessage.readOrNo === true){			//消息已读
						showHTML += ("<li id='pushmessage"+j+"' class='messageContainer alreadyread'>"
						+	"<span style='display:none;'>true</span>"
						+	"<div class='message-theme-field'>"
						+		"<label class='theme-top-left'>"+appendPushmessage.theme+"</label>"
						+		"<div class='time-top-right'>" + showtime + "</div>"
						+	"</div>"
						+	"<div id='messageContent' style='display:none;'>" + appendPushmessage.message + "</div>"
						+"</li>");
					}else{								//消息未读
						showHTML += ("<li id='pushmessage"+j+"' class='messageContainer noread'>"
						+	"<span style='display:none;'>false</span>"
						+	"<div class='message-theme-field'>"
						+		"<label class='theme-top-left'>"+appendPushmessage.theme+"</label>"
						+		"<div class='time-top-right'>" + showtime + "</div>"
						+	"</div>"
						+	"<div id='messageContent' style='display:none;'>" + appendPushmessage.message + "</div>"
						+"</li>");
					}
				}catch(e){
					alert("解析pussmessage内容出错！错误原因为e.message："+e.message + " !。 此时循环的的推送内容为第："+j +"个");
				}
			}
			jq("#push_scroller").append(showHTML);
			if(showNum === displayedNum){
				jq("#pushmessage_bottom_info").html("<span>所有信息已经加载完毕！</span>");
			}else{
				jq("#pushmessage_bottom_info").html("<span>加载了"+(showNum -displayedNum) +"条推送信息。</span>");
			}
			
			jq("#pushmessage_bottom_info").show();
			jq("#pushmessage_bottom_info").fadeOut(2500);
			jq("#push_scroller").listview("refresh");
		}else if(freshType === "delFresh"){
			if(displayedNum < 10){
				jq("#pushmessage-pullup").hide();
				if(displayedNum < 1){
					return;		
				}
			}
			for(var k =popIndex; k <displayedNum;k++){
				jq("#pushmessage"+(k+1)).attr("id","pushmessage"+k);
			}
			jq("#push_scroller").listview("refresh");
		}		
	}
	
	jq("#push_scroller .messageContainer").live("tap",function(){
		if(jq(this).children("span").text() === "true" ){			//已读的信息只需要收缩即可
			if(jq(this).children("#messageContent").is(":hidden")){
				jq(this).children("#messageContent").css("display","block");
				jq(".theme-top-left",this).css({"white-space":"normal","height":"auto"});
			}else{
				jq(this).children("#messageContent").css("display","none");
				jq(".theme-top-left",this).css({"white-space":"nowrap","height":"1.8em"});
			}
		}else{														//未读变已读
			jq(this).children("span").text("true");
			//改变页面显示颜色
			jq(this).removeClass("noread");
			jq(this).addClass("alreadyread");
			jq(this).children("label").css({"color":"#000","font-weight":"500","font-size":"16px"});
			//jq(this).children("div").css("color","#bbb");
			//jq(this).children("#messageContent").css({"padding":"3px 5px 0px 5px","color":"#aaa"});
			jq(this).children("div").css("color","#000");
			jq(this).children("#messageContent").css({"padding":"10px 5px 0px 5px"});
			//jq(this).children("#messageContent").toggle();
			
			if(jq(this).children("#messageContent").is(":hidden")){
				jq(this).children("#messageContent").css("display","block");
				jq(".theme-top-left",this).css({"white-space":"normal","height":"auto"});
			}else{
				jq(this).children("#messageContent").css("display","none");
				jq(".theme-top-left",this).css({"white-space":"nowrap","height":"1.8em"});
			}
			//更新存储信息的标记状态
			var key = jq(this).attr("id");
			var pushmessage = window.ticketStorage.getItem(key);
			pushmessage = jq.parseJSON(pushmessage);
			pushmessage.readOrNo = true;
			window.ticketStorage.setItem(key, JSON.stringify(pushmessage));
			//接下来检查未读个数以便更新主菜单未读的图标显示
			updateMenuIcon();
		}
		jq("#push_scroller").listview("refresh");
		return false;
	});
	
	jq("#push_scroller .messageContainer").live("taphold", "li",function(e){
		e.stopImmediatePropagation();
		e.stopPropagation();
		e.preventDefault();
		try{
			jq('#popupDiv').popup();
			jq('#popupDiv').popup('open');
		}catch(e){
			alert("弹出层出错！错误原因为e.message："+e.message);
		}
		popIndex = parseInt(jq(this).attr("id").substring(11));
		return false;
	});
	
	
	
	
	
	jq("#popcopy").live("tap",function(e) {
		e.stopImmediatePropagation();
		e.stopPropagation();
		e.preventDefault();
		WL.App.copyToClipboard(jq("#pushmessage"+popIndex).find("#messageContent").html());
		jq('#popupDiv').popup('close');
		mor.ticket.util.alertMessage("复制成功");
		return false;
	});
	
	jq("#popdelete").live("tap",function(e){
		e.stopImmediatePropagation();
		e.stopPropagation();
		e.preventDefault();
		jq('#popupDiv').popup('close');
		setTimeout(null,1000);
		if(removeOneValue(popIndex)){
			jq("#pushmessage"+popIndex).remove();
			mor.ticket.util.alertMessage("删除成功");
			loadingPushMessage("delFresh");
		}else{
			alert("删除失败！");
		}
		updateMenuIcon();
		jq("#push_scroller").listview("refresh");
		return false;
	});
	
	/*jq("#popexit").live("tap",function(e){
		e.stopImmediatePropagation();
		e.stopPropagation();
		e.preventDefault();
		jq('#popupDiv').popup('close');
		return false;
	});*/
	
	function removeOneValue(key){
		var count = window.ticketStorage.pushmessageNum();
		if(key.length < 1){
			return false;
		}else{
			if(key >= count){
				return false;
			}else{
				window.ticketStorage.removeItem("pushmessage"+key, alert);
				for(var i=key;i<count-1;i++){
					var getValue = window.ticketStorage.getItem("pushmessage"+(i+1));
					if(getValue.length != 0){
						window.ticketStorage.setItem("pushmessage"+i, getValue);
						window.ticketStorage.removeItem("pushmessage"+(i+1));
					}
				}
				return true;
			}
		}
	}
	
	function updateMenuIcon(){
		mor.ticket.util.initAppVersionInfo();
	}
	
	function formatShowTime(time){
		var append_dt = new Date(time);
		var append_dt_day = append_dt.getDate();
		var append_now = new Date();
		var append_now_day = append_now.getDate();
		var append_dayDiff = parseInt( (append_now.getTime() - append_dt.getTime()) / parseInt(1000*3600*24));
		var append_time = "";
		if(append_dayDiff === 0 && append_now_day-append_dt_day === 0){
			append_time = "今天" + (append_dt.getHours()>9?append_dt.getHours():"0"+append_dt.getHours())
				+ ":" + (append_dt.getMinutes()>9?append_dt.getMinutes():"0"+append_dt.getMinutes()) 
				+ ":" + (append_dt.getSeconds()>9?append_dt.getSeconds():"0"+append_dt.getSeconds());
		}else if((append_dayDiff === 0|| append_dayDiff ===1) && append_now_day-append_dt_day === 1) {
			append_time = "昨天" + (append_dt.getHours()>9?append_dt.getHours():"0"+append_dt.getHours())
				+ ":" + (append_dt.getMinutes()>9?append_dt.getMinutes():"0"+append_dt.getMinutes()) 
				+ ":" + (append_dt.getSeconds()>9?append_dt.getSeconds():"0"+append_dt.getSeconds());
		}else if((append_dayDiff === 1 || append_dayDiff ===2) && append_now_day-append_dt_day === 2 ){
			append_time = "前天" + (append_dt.getHours()>9?append_dt.getHours():"0"+append_dt.getHours())
				+ ":" + (append_dt.getMinutes()>9?append_dt.getMinutes():"0"+append_dt.getMinutes()) 
				+ ":" + (append_dt.getSeconds()>9?append_dt.getSeconds():"0"+append_dt.getSeconds());
		}else if(append_dayDiff < 365){
			append_time = (append_dt.getMonth()+1)+ "月" + append_dt.getDate() +"日";
		}else if(append_dayDiff < 730){
			append_time = "去年" + (append_dt.getMonth()+1)+ "月" + append_dt.getDate() +"日" ;
		}else{
			append_time = append_dt.getFullYear() + "年" + (append_dt.getMonth()+1)+ "月" + append_dt.getDate() +"日" ;
		}
		return append_time;
	}
	
})();