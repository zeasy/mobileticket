/**
 * @providesModule CCNavigator
 * Created by easy on 16/3/12.
 */

import React,{View,Text,TouchableOpacity,Navigator,NavigatorIOS,Platform} from 'react-native';
let iOS = Platform.OS === 'ios';

let CCNavigationBar = require('./CCNavigationBar');

let CCNavigator = React.createClass({

    _onBackButtonPress : function(route) {
        if (this.refs.navigator.getCurrentRoutes().length > 1)
        this.pop();
    },

    _renderScene : function(route, navigator) {

        let Component = route.component;
        let props = route.passProps;



        let initRoute = {
            barTintColor:this.props.initialRoute.barTintColor,
            tintColor:this.props.initialRoute.tintColor,
            titleTextColor:this.props.initialRoute.titleTextColor
        };
        let r = {...initRoute,...route};

        if (navigator.getCurrentRoutes().length > 1 && navigator.getCurrentRoutes().indexOf(route) != 0) {
            if (!r.backButtonTitle) r.backButtonTitle = 'Back';
        }

        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <CCNavigationBar {...r} onBackButtonPress={_=>this._onBackButtonPress(route)} navigator={navigator}/>
                <Component {... props} navigator={navigator} />
            </View>
        )
    },

    _configureScene : function(route, routeStack) {
        return (route._configureScene && route._configureScene(route,routeStack)) || ({...Navigator.SceneConfigs.HorizontalSwipeJump,gestures:{}});
    },

    replace : function(route) {
        this.refs.navigator.replace(route);
    },

    replacePrevious : function(route) {
        this.refs.navigator.replacePrevious(route);
    },

    replacePreviousAndPop : function(route) {
        this.refs.navigator.replacePreviousAndPop(route);
    },

    resetTo : function(route) {
        this.refs.navigator.resetTo(route);
    },

    popToRoute : function(route) {
        this.refs.navigator.popToRoute(route);
    },

    popToTop : function() {
        this.refs.navigator.popToTop();
    },

    pop : function() {
        this.refs.navigator.pop();
    },

    push : function(route) {
        this.refs.navigator.push(route);
    },

    present : function(route) {
        let r = {...route};
        r._configureScene = ()=> {
            let jump = {...Navigator.SceneConfigs.VerticalUpSwipeJump};
            jump.gestures = {};
            return jump;
        }
        this.push(r);
    },

    dismiss : function() {
        let routes = this.refs.navigator.getCurrentRoutes();
        if (routes.length > 1) {
            let route = routes[routes.length - 2];
            let r = {...route};
            r._configureScene = ()=> {
                let jump = {...Navigator.SceneConfigs.VerticalDownSwipeJump};
                jump.gestures = {};

            }
            this.refs.navigator.popToRoute(r);
        }
    },

    render : function() {
        let props = {...this.props};
        props.ref = undefined;

        let NativeNavigator = iOS?NavigatorIOS:Navigator;

        return (
            <NativeNavigator style={{flex:1}} configureScene={this._configureScene} {...props} renderScene={this._renderScene} ref="navigator" />
        );
    }

});

module.exports = CCNavigator;