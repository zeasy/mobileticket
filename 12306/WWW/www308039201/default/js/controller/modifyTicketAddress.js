
/* JavaScript content from js/controller/modifyTicketAddress.js in folder common */
(function() {
	var prevPage;
	var focusArray = [];
	jq("#modifyTicketAddressView").live("pageshow", function() {
		focusArray = [];
	});
	
	jq("#modifyTicketAddressView").live("pagebeforeshow",function(e, data) {
		mor.ticket.util.initAppVersionInfo();
		registerAutoScroll();
		addressQueryBtnLisener();
		jq("#townSelect").parent().parent().removeClass("ui-disabled");
		jq("#streetSelect").parent().parent().removeClass("ui-disabled");
		prevPage = data.prevPage.attr("id");
		getStateMessage();
	});
	function registerAutoScroll() {
		var util = mor.ticket.util;
		util.enableAutoScroll('#citySelect', focusArray);
		util.enableAutoScroll('#countrySelect', focusArray);
		util.enableAutoScroll('#townSelect', focusArray);
		util.enableAutoScroll('#streetSelect', focusArray);
		util.enableAutoScroll('#addressDetail', focusArray);
		util.enableAutoScroll('#attnName', focusArray);
		util.enableAutoScroll('#attnMobileNo', focusArray);
		
	}
	function addressQueryBtnLisener() {
		jq("#modifyTicketAddressBackBtn").off().on("tap", function() {
			mor.ticket.util.changePage("sendTicketAddress.html");
			return false;
		});
		jq("#modifyTicketAddressBtn").off().on("tap",modifyTicketAddressFn);
		jq("#deleteTicketAddressBtn").off().on("tap",subDelete);
		jq("#provinceSelect").on("change", getCity);
		jq("#citySelect").on("change", getCountry);
		jq("#countrySelect").on("change", getTown);
		jq("#townSelect").on("change", getStreet);
	};
	function subDelete(){
		if(mor.ticket.util.isAndroid()){
			WL.SimpleDialog.show(
					"温馨提示", 
					"确定要删除送票地址吗？", 
					[ 
					  {text : '确定', handler: deleteTicketAddress},
					  {text : '取消', handler: function(){return false;}}
					]
				);	
		}else{
			WL.SimpleDialog.show(
					"温馨提示", 
					"确定要删除送票地址吗？", 
					[ 
					  {text : '取消', handler: function(){return false;}},
					  {text : '确定', handler: deleteTicketAddress}
					]
				);	
		}
	}
	function getCity() {
		var option = {};
		WL.JSONStore.get("expressAddress").findAll(option)
		.then(function(res){
			var selectedProvince = jq("#provinceSelect").val().trim();
			var cityArray = res[0].json.mapCityList[selectedProvince];
			if(mor.ticket.sendTicketAddressLi.isLi == "yes"){
				var TAlist = mor.ticket.sendTicketAddressList;
				for ( var i = 0; i < cityArray.length; i++) {
					var htmlStr = "<option value='"
							+ cityArray[i] + "'>"
							+ cityArray[i]
							+ "</option>";
					jq("#citySelect").append(htmlStr);
				}
				jq("#citySelect option[value="+TAlist.original_address_city+"]").attr("selected", true);
				jq("#citySelect").selectmenu('refresh', true);
				getCountry();
			}else{
				jq("#townSelect").parent().parent().removeClass("ui-disabled");
				jq("#streetSelect").parent().parent().removeClass("ui-disabled");
				jq("#citySelect").empty();
				jq("#citySelect").append("<option  value = '0' selected='selected'>请选择市</option>");
				jq("#countrySelect").empty();
				jq("#countrySelect").append("<option  value = '0' selected='selected'>请选择区县</option>");
				jq("#townSelect").empty();
				jq("#townSelect").append("<option  value = '0' selected='selected'>请选择乡镇</option>");
				jq("#streetSelect").empty();
				jq("#streetSelect").append("<option  value = '0' selected='selected'>请选择街道</option>");
				jq("#citySelect").selectmenu('refresh', true);
				jq("#countrySelect").selectmenu('refresh', true);
				jq("#townSelect").selectmenu('refresh', true);
				jq("#streetSelect").selectmenu('refresh', true);
				for ( var i = 0; i < cityArray.length; i++) {
					var htmlStr = "<option value='"
							+ cityArray[i] + "'>"
							+ cityArray[i]
							+ "</option>";
					jq("#citySelect").append(htmlStr);
				}
				jq("#citySelect").selectmenu('refresh', true);
			}
		})
		.fail(function(errorObject){
				WL.Logger.info(errorObject.msg);
				//jq("#modify_studentSchool").val("");
		});
	}
	function getCountry(){
		var option = {};
			WL.JSONStore.get("expressAddress").findAll(option)
			.then(function(res){
				//jq("#modify_studentSchool").val(res[0].json.university_name);
				var countryArray = res[0].json.mapCountryList[jq("#citySelect").val()];
				if(mor.ticket.sendTicketAddressLi.isLi == "yes"){
					var TAlist = mor.ticket.sendTicketAddressList;
					for ( var i = 0; i < countryArray.length; i++) {
						var htmlStr = "<option value='"
								+ countryArray[i] + "'>"
								+ countryArray[i]
								+ "</option>";
						jq("#countrySelect").append(htmlStr);
					}
					jq("#countrySelect option[value="+TAlist.original_address_county+"]").attr("selected", true);
					jq("#countrySelect").selectmenu('refresh', true);
					getTown();
				}else{
					jq("#countrySelect").empty();
					jq("#countrySelect").append("<option  value = '0' selected='selected'>请选择区县</option>");
					jq("#townSelect").empty();
					jq("#townSelect").append("<option  value = '0' selected='selected'>请选择乡镇</option>");
					jq("#streetSelect").empty();
					jq("#streetSelect").append("<option  value = '0' selected='selected'>请选择街道</option>");
					jq("#countrySelect").selectmenu('refresh', true);
					jq("#townSelect").selectmenu('refresh', true);
					jq("#streetSelect").selectmenu('refresh', true);
					for ( var i = 0; i < countryArray.length; i++) {
						var htmlStr = "<option value='"
								+ countryArray[i] + "'>"
								+ countryArray[i]
								+ "</option>";
						jq("#countrySelect").append(htmlStr);
					}
					jq("#countrySelect").selectmenu('refresh', true);
				}
			})
			.fail(function(errorObject){
					WL.Logger.info(errorObject.msg);
					//jq("#modify_studentSchool").val("");
			});	
		}
		function getTown(){
			var option = {};
			WL.JSONStore.get("expressAddress").findAll(option)
			.then(function(res){
				//jq("#modify_studentSchool").val(res[0].json.university_name);
				var townArray = res[0].json.mapTownList[jq("#countrySelect").val()];
				jq("#townSelect").parent().parent().removeClass("ui-disabled");
				jq("#streetSelect").parent().parent().removeClass("ui-disabled");
				if(mor.ticket.sendTicketAddressLi.isLi == "yes"){
					var TAlist = mor.ticket.sendTicketAddressList;
					if(townArray.length == 0){
						jq("#townSelect").append("<option  value='0' selected='selected'>请选择乡镇</option>");
						jq("#townSelect").selectmenu('refresh', true);
						jq("#streetSelect").append("<option  value='0' selected='selected'>请选择街道</option>");
						jq("#streetSelect").selectmenu('refresh', true);
						jq("#townSelect").parent().parent().addClass("ui-disabled");
						jq("#streetSelect").parent().parent().addClass("ui-disabled");
						jq("#addressDetail").val(TAlist.original_detail_address);
						jq("#attnName").val(TAlist.original_address_name);
						jq("#attnMobileNo").val(TAlist.original_mobile_no);
						if(TAlist.default_address == "0"){
							jq("#pretermitAddressInfo input[name='pretermit']:checkbox").attr("checked",true);	
						}else{
							jq("#pretermitAddressInfo input[name='pretermit']:checkbox").attr("checked",false);	
						}
						jq("#deleteTicketAddressOptions").show();
						mor.ticket.sendTicketAddressLi.isLi = "";
						//isLi = "";
						return;
					}
					for ( var i = 0; i < townArray.length; i++) {
						var htmlStr = "<option value='"
								+ townArray[i] + "'>"
								+ townArray[i]
								+ "</option>";
						jq("#townSelect").append(htmlStr);
					}
					jq("#townSelect option[value="+TAlist.original_addressee_town+"]").attr("selected", true);
					jq("#townSelect").selectmenu('refresh', true);
					getStreet();
				}else{
					if(townArray.length == 0){
						jq("#townSelect").parent().parent().addClass("ui-disabled");
						jq("#streetSelect").parent().parent().addClass("ui-disabled");
						return;
					}
					jq("#townSelect").empty();
					jq("#townSelect").append("<option  value = '0' selected='selected'>请选择乡镇</option>");
					jq("#streetSelect").empty();
					jq("#streetSelect").append("<option  value = '0' selected='selected'>请选择街道</option>");
					jq("#townSelect").selectmenu('refresh', true);
					jq("#streetSelect").selectmenu('refresh', true);
					jq("#townSelect").parent().parent().removeClass("ui-disabled");
					jq("#streetSelect").parent().parent().removeClass("ui-disabled");
					for ( var i = 0; i < townArray.length; i++) {
						var htmlStr = "<option value='"
								+ townArray[i] + "'>"
								+ townArray[i]
								+ "</option>";
						jq("#townSelect").append(htmlStr);
					}
					jq("#townSelect").selectmenu('refresh', true);
				}
			})
			.fail(function(errorObject){
					WL.Logger.info(errorObject.msg);
					//jq("#modify_studentSchool").val("");
			});
		}
		
		function getStreet(){
			var option = {};
			WL.JSONStore.get("expressAddress").findAll(option)
			.then(function(res){
				//jq("#modify_studentSchool").val(res[0].json.university_name);
				var streetArray = res[0].json.mapStreetList[jq("#townSelect").val()];
				jq("#townSelect").parent().parent().removeClass("ui-disabled");
				jq("#streetSelect").parent().parent().removeClass("ui-disabled");
				if(mor.ticket.sendTicketAddressLi.isLi == "yes"){
					var TAlist = mor.ticket.sendTicketAddressList;
					if(streetArray.length == 0){
						jq("#streetSelect").append("<option  value = '0' selected='selected'>请选择街道</option>");
						jq("#streetSelect").selectmenu('refresh', true);
						jq("#addressDetail").val(TAlist.original_detail_address);
						jq("#attnName").val(TAlist.original_address_name);
						jq("#attnMobileNo").val(TAlist.original_mobile_no);
						if(TAlist.default_address == "0"){
							jq("#pretermitAddressInfo input[name='pretermit']:checkbox").attr("checked",true);	
						}else{
							jq("#pretermitAddressInfo input[name='pretermit']:checkbox").attr("checked",false);	
						}
						jq("#deleteTicketAddressOptions").show();
						mor.ticket.sendTicketAddressLi.isLi = "";
						//isLi = "";
						return;
					}
					for ( var i = 0; i < streetArray.length; i++) {
						var htmlStr = "<option value='"
								+ streetArray[i] + "'>"
								+ streetArray[i]
								+ "</option>";
						jq("#streetSelect").append(htmlStr);
					}
					jq("#streetSelect option[value="+TAlist.original_addressee_street+"]").attr("selected", true);
					jq("#streetSelect").selectmenu('refresh', true);
					jq("#addressDetail").val(TAlist.original_detail_address);
					jq("#attnName").val(TAlist.original_address_name);
					jq("#attnMobileNo").val(TAlist.original_mobile_no);
					if(TAlist.default_address == "0"){
						jq("#pretermitAddressInfo input[name='pretermit']:checkbox").attr("checked",true);	
					}else{
						jq("#pretermitAddressInfo input[name='pretermit']:checkbox").attr("checked",false);	
					}
					jq("#deleteTicketAddressOptions").show();
					mor.ticket.sendTicketAddressLi.isLi = "";
					//isLi = "";
				}else{
					if(streetArray.length == 0){
						jq("#streetSelect").parent().parent().addClass("ui-disabled");
						return;
					}
					jq("#streetSelect").empty();
					jq("#streetSelect").append("<option  value = '0' selected='selected'>请选择街道</option>");
					jq("#streetSelect").selectmenu('refresh', true);
					if(streetArray.length == 0){
						jq("#streetSelect").parent().parent().addClass("ui-disabled");
						return;
					}
					jq("#townSelect").parent().parent().removeClass("ui-disabled");
					jq("#streetSelect").parent().parent().removeClass("ui-disabled");
					//streetArrayLength = invocationResult.listExpressAddressBean.length;
					for ( var i = 0; i < streetArray.length; i++) {
						var htmlStr = "<option value='"
								+ streetArray[i] + "'>"
								+ streetArray[i]
								+ "</option>";
						jq("#streetSelect").append(htmlStr);
					}
					jq("#streetSelect").selectmenu('refresh', true);
				}
			})
			.fail(function(errorObject){
					WL.Logger.info(errorObject.msg);
					//jq("#modify_studentSchool").val("");
			});
		}
	function refreshForm() {
		if (prevPage === "sendTicketAddressView") {
			if (mor.ticket.viewControl.isModifyTicketAddress) {
				getCity();
			}else{
				jq("#provinceSelect option[value = '0']").remove();
			    jq("#provinceSelect").prepend("<option value = '0'>"+"请选择省"+"</option>");
			    jq("#provinceSelect option[value = '0']").attr("selected", "selected");
			    jq("#provinceSelect").selectmenu('refresh', true);
				jq("#citySelect").empty();
				jq("#citySelect").prepend("<option value ='0'>"+"请选择市"+"</option>");
				jq("#citySelect option[value = '0']").attr("selected", "selected");
				jq("#citySelect").selectmenu('refresh', true);
				jq("#countrySelect").empty();
				jq("#countrySelect").prepend("<option value ='0'>"+"请选择区县"+"</option>");
				jq("#countrySelect option[value = '0']").attr("selected", "selected");
				jq("#countrySelect").selectmenu('refresh', true);
				jq("#townSelect").empty();
				jq("#townSelect").prepend("<option value ='0'>"+"请选择乡镇"+"</option>");
				jq("#townSelect option[value = '0']").attr("selected", "selected");
				jq("#townSelect").selectmenu('refresh', true);
				jq("#streetSelect").empty();
				jq("#streetSelect").prepend("<option value ='0'>"+"请选择街道"+"</option>");
				jq("#streetSelect option[value = '0']").attr("selected", "selected");
				jq("#streetSelect").selectmenu('refresh', true);
				jq("#addressDetail").attr("value","");
				jq("#attnName").attr("value","");
				jq("#attnMobileNo").attr("value","");
				jq("#pretermitAddressInfo input[name='pretermit']:checkbox").attr("checked",false);
				jq("#deleteTicketAddressOptions").hide();
			}
		}else{
			contentIscrollRefresh();
		}
	}
	function getStateMessage() {
		jq("#provinceSelect").empty();
		var ss = jq("#proviceSelect").find("option").length;
		if (ss == '0') {
			var state = mor.ticket.stateList.result;
			for ( var i = 0; i < state.length; i++) {
				var htmlStr = "<option value='" + state[i] + "'>" + state[i]
						+ "</option>";
				jq("#provinceSelect").append(htmlStr);
				jq("#provinceSelect").selectmenu('refresh', true);
			}
		}
		var TAlist = mor.ticket.sendTicketAddressList;
		var province = TAlist.original_address_province.trim();
		jq("#provinceSelect option[value="+province+"]").attr("selected", true);
		jq("#provinceSelect").selectmenu('refresh', true);
		refreshForm();
	}
	function contentIscrollRefresh(){
		if(jq("#modifyTicketAddressView .ui-content").attr("data-iscroll")!=undefined){
			jq("#modifyTicketAddressView .ui-content").iscrollview("refresh");
		}
	}
	function modifyTicketAddressFn() {
		var util = mor.ticket.util;
		var pretermit;
		var checkFormUtil = mor.ticket.checkForm;
		var TAlist = mor.ticket.sendTicketAddressList;
		if(jq("#proviceSelect").val() == "0"){
			util.alertMessage("请选择所在省份");
			return;
		}
		if(jq("#citySelect").val() == "0"){
			util.alertMessage("请选择所在市");
			return;
		}
		if(jq("#countrySelect").val() == "0"){
			util.alertMessage("请选择所在区县");
			return;
		}
		if ( jq("#addressDetail").val() != "" && jq("#attnName").val() != "" 
			&& jq("#attnMobileNo").val() != ""&& jq("#proviceSelect").val() != "0") {
			var adressArray = jq("#addressDetail").val().split("");
			if(!checkFormUtil.checkAddressChar){
				util.alertMessage("请输入正确的地址信息");
				return;
			}
			if(adressArray.length > 100){
				util.alertMessage("地址信息超长，请重新输入！");
				return;
			}

		if (!checkFormUtil.checkChar(jq("#attnName").val()) || (jq("#attnName").val().length<2 || jq("#attnName").val().length>30)) {
			isVisiblebusy();
			util.alertMessage("姓名只能包含中文或者英文，如有生僻字或繁体字参见12306姓名填写规则进行填写，且最长不超过30个字");
			return;
		}
		jq("#attnMobileNo").change(
				function() {
					if (!util.isNoValue(jq("#attnMobileNo").val())
							&& !checkFormUtil.isMobile(jq("#attnMobileNo")
									.val())) {
						util.alertMessage("请输入正确的手机号码");
						return;
					}
				});
		}else{
			util.alertMessage("请补全送票地址的详细信息.");
			return;
		}
		if (jq("#pretermitAddressInfo input[name='pretermit']:checkbox").is(":checked")) {
			pretermit = "0";
		}else {
			pretermit = "1";
		}
		if(mor.ticket.viewControl.isModifyTicketAddress){
			var commonParameters = {
					'default_address' : pretermit,
					'addressee_name' : jq("#attnName").val(),
					'addressee_province' : jq("#provinceSelect").val(),
					'addressee_city' : jq("#citySelect").val(),	
					'addressee_county' : jq("#countrySelect").val(),
					'detail_address' : jq("#addressDetail").val(),
					'mobile_no' : jq("#attnMobileNo").val(),
					'original_address_name' : TAlist.original_address_name,
					'original_address_province' : TAlist.original_address_province,
					'original_address_city' : TAlist.original_address_city,
					'original_address_county' : TAlist.original_address_county,
					'original_detail_address' : TAlist.original_detail_address,
					'original_mobile_no' : TAlist.original_mobile_no,
					'original_address_town' : TAlist.original_addressee_town,
					'addressee_town' : jq("#townSelect").val() != "0" ? jq("#townSelect").val():"",
					'original_address_street' : TAlist.original_addressee_street,
					'addressee_street' : jq("#streetSelect").val() != "0" ? jq("#streetSelect").val():"",
				};
				var invocationData = {
					adapter : mor.ticket.viewControl.adapterUsed,
					procedure : "ModifyDeliverAddress"
				};

				var options = {
					onSuccess : AddOrModifySucceeded,
					onFailure : util.creatCommonRequestFailureHandler()
				};
				mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,
						options);
				return false;
		}else{
				if(TAlist.addressListLength >= 20){
					util.alertMessage("每个用户最多可以添加20条送票地址。");
					return;
			}
			var commonParameters = {
					'default_address' : pretermit,
					'addressee_name' : jq("#attnName").val(),
					'addressee_province' : jq("#provinceSelect").val(),
					'addressee_city' : jq("#citySelect").val(),
					'addressee_county' : jq("#countrySelect").val(),
					'addressee_town' : jq("#townSelect").val() != "0" ? jq("#townSelect").val():"",
					'addressee_street' : jq("#streetSelect").val() != "0" ? jq("#streetSelect").val():"",
					'detail_address' : jq("#addressDetail").val(),
					'mobile_no' : jq("#attnMobileNo").val()
				};
				var invocationData = {
					adapter : mor.ticket.viewControl.adapterUsed,
					procedure : "addDeliverAddress"
				};

				var options = {
					onSuccess : AddOrModifySucceeded,
					onFailure : util.creatCommonRequestFailureHandler()
				};
				mor.ticket.util.invokeWLProcedure(commonParameters, invocationData,
						options);
				return false;
		}		
	}

	function AddOrModifySucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			WL.SimpleDialog.show(
					"温馨提示", invocationResult.error_msg, 
					[ {text : '确定', handler: function() {	
							jq.mobile.changePage("sendTicketAddress.html");												
					}}]
				);	
		}else{
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	function isVisiblebusy() {
		if (busy.isVisible()) {
			busy.hide();
		}

	}
	function deleteTicketAddress(){
		var TAlist = mor.ticket.sendTicketAddressList;
		var commonParameters = {
			'addressee_name' : TAlist.original_address_name,
			'addressee_province' : TAlist.original_address_province,
			'addressee_city' :TAlist.original_address_city,
			'addressee_county' :TAlist.original_address_county,
			'addressee_town': TAlist.original_addressee_town,
			'detail_address' : TAlist.original_detail_address,
			'default_address' : TAlist.default_address,
			"addressee_street" : TAlist.original_addressee_street
		};
		
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "deleteDeliverAddress"
		};
		
		var options =  {
				onSuccess: deleteTicketAddressSucceeded,
				onFailure: mor.ticket.util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function deleteTicketAddressSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			WL.SimpleDialog.show(
					"温馨提示", invocationResult.error_msg, 
					[ {text : '确定', handler: function() {	
						jq.mobile.changePage("sendTicketAddress.html");												
					}}]
				);	
	}else{
		mor.ticket.util.alertMessage(invocationResult.error_msg);
	}
	}
	function queryDeliverSucceeded(result) {
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			jq("#ticketAddressList").html(generatePassengerList(invocationResult)).listview("refresh");
		} else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
})();
