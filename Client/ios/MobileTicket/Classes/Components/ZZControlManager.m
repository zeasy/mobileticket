//
//  ZZControlManager.m
//  MobileTicket
//
//  Created by EASY on 15/11/13.
//  Copyright (c) 2015年 Facebook. All rights reserved.
//

#import "ZZControlManager.h"
#import "ZZControl.h"
#import <RCTUIManager.h>
#import <RCTSparseArray.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <RCTComponent.h>
@implementation ZZControlManager
RCT_EXPORT_MODULE()

RCT_EXPORT_VIEW_PROPERTY(onTouch, RCTDirectEventBlock)

RCT_EXPORT_METHOD(addEvents:(nonnull NSNumber *) reactTag events:(int) events successed:(RCTResponseSenderBlock) success) {
  [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, RCTSparseArray *viewRegistry) {
    ZZControl *control = viewRegistry[reactTag];
    if ([control isKindOfClass:[ZZControl class]]) {
      [[control rac_signalForControlEvents:events] subscribeNext:^(id x) {
        NSLog(@"--");
//        if (success) {
          //success(@[]);
        //}
//        if (control.onTouch) {
//          control.onTouch(@{@"events":@(events)});
//        }
        
//        [self.bridge.eventDispatcher sendInputEventWithName:@"topTouchEvent" body:@{
//                                                                                    @"target":reactTag,
//                                                                                    @"events":@(events)
//                                                                                    }];
      }];
    }
  }];
}

-(UIView<RCTComponent> *)view {
  return (UIView<RCTComponent> *)[[ZZControl alloc] initWithEventDispatcher:self.bridge.eventDispatcher];
}

@end
