
/* JavaScript content from js/controller/getPwd.js in folder common */
(function() {
//	jq("#getPwdView").live("pagecreate", function() {
//		mor.ticket.util.androidRemoveIscroll("#getPwdView");
//	});
	var focusArray=[];
	jq("#getPwdView").live("pageshow", function() {
		focusArray=[];
	});
	function registerAutoScroll(){	
		var util = mor.ticket.util;
		util.enableAutoScroll('#getPwd_email',focusArray);
		util.enableAutoScroll('#getPwd_verify',focusArray);
		util.enableAutoScroll('#phone_idNo',focusArray);
		util.enableAutoScroll('#idNo',focusArray);
	}
	
	jq("#getPwdView").live("pageinit", function(){
		registerAutoScroll();
	});
	
	jq("#getPwdView").live("pageshow", function() {
		mor.ticket.util.initAppVersionInfo();
		var util = mor.ticket.util;
		mor.ticket.viewControl.findPwdMode=="email";
		var checkFormUtil = mor.ticket.checkForm;
		// 验证码
		jq("#getpwd_refreshPassCode").bind("tap",refreshPassCode);
		//手机获取验证码
		jq("#getPwd3_validationCodeBtn").bind("tap",getPwd3ValidationCode);
		
		jq("#findPwdBtn").bind("tap",submit);
		
		jq("#getPwd1Choice").bind("tap",function(){
			jq("#getPwd1Choice").addClass("ui-btn-active ui-state-persist");
			jq("#getPwd3Choice").removeClass("ui-btn-active ui-state-persist");
			jq("#getPwd3Option").hide();
			jq("#getPwd_verifyDiv").show();
			jq("#getPwd3_validationCodeDiv").hide();
			jq("#getPwd1Option").show();
			mor.ticket.viewControl.findPwdMode="email";
			return false;
		});
        jq("#getPwd3Choice").bind("tap",function(){
        	jq("#getPwd3Choice").addClass("ui-btn-active ui-state-persist");
			jq("#getPwd1Choice").removeClass("ui-btn-active ui-state-persist");
			jq("#getPwd1Option").hide();
			jq("#getPwd_verifyDiv").hide();
			jq("#getPwd3_validationCodeDiv").show();
			jq("#getPwd3Option").show();
			mor.ticket.viewControl.findPwdMode="phone";
			return false;
		});
		refreshPassCode();
		
		jq("#getPwd_email").change(function () {
			if(util.isNoValue(jq("#getPwd_email").val())){
				util.alertMessage("请输入邮箱地址");
				return;
			}
			
			if(!checkFormUtil.isEmail(jq("#getPwd_email").val())){
				util.alertMessage("请输入正确的邮箱地址");
				return;
			}
		});
		
		jq("#idNo").change(function () { 
			if(util.isNoValue(jq("#idNo").val())){
				util.alertMessage("请输入证件号码");
				return;
			}
			if(!checkFormUtil.checkIdValidStr(jq("#idNo").val())){
				util.alertMessage("输入的证件编号中包含中文信息或特殊字符!");
				return;
			}
		} );
		
		jq("#phone_idNo").change(function () { 
			if(util.isNoValue(jq("#phone_idNo").val())){
				util.alertMessage("请输入证件号码");
				return;
			}
			if(!checkFormUtil.checkIdValidStr(jq("#phone_idNo").val())){
				util.alertMessage("输入的证件编号中包含中文信息或特殊字符!");
				return;
			}
		} );
		
		jq("#getPwd_phone").change(function () { 
			if(util.isNoValue(jq("#getPwd_phone").val())){
				util.alertMessage("请输入手机号码");
				return;
			}
			if(!checkFormUtil.isMobile(jq("#getPwd_phone").val())){
				util.alertMessage("请输入正确的手机号码!");
				return;
			}
		} );
		
		jq("#getPwd3_passwd").change(function () { 
			if(util.isNoValue(jq("#getPwd3_passwd").val())){
				util.alertMessage("请填写密码");
				return;
			}
			
			if(jq("#getPwd3_passwd").val().length<6){
				util.alertMessage("密码的长度必须大于6位!");
				return;
			}
		} );
		
		jq("#getPwd3_confirmPasswd").change(function () { 
			if(util.isNoValue(jq("#getPwd3_confirmPasswd").val())){
				util.alertMessage("请输入确认密码");
				return;
			}
			
			if(jq("#getPwd3_passwd").val()!=jq("#getPwd3_confirmPasswd").val()){
				util.alertMessage("两次输入的密码不一致!");
				return;
			}
		} );
		jq("#getPwd3_validationCode").change(function () { 
			var checkNum = /^[0-9]{6}$/;
			if(!checkNum.test(jq("#getPwd3_validationCode").val())){
				mor.ticket.util.alertMessage("手机校验码不正确");
				jq("#getPwd3_validationCode").val("");
				return;
			}
		});
		jq("#getPwd_verify").change(function () {
			if(util.isNoValue(jq("#getPwd_verify").val())){
				util.alertMessage("请填写验证码");
				return;
			}
			
			if(!checkFormUtil.checkNum(jq("#getPwd_verify").val())){
				util.alertMessage("验证码只能为数字");
				return;
			}
		});
	});


	function refreshPassCode(){	
		var commonParameters = {
			'baseDTO.user_name': ''
		};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "postPassCode"
		};
		var options = {
				onSuccess: requestPassCodeSucceeded,
				onFailure: mor.ticket.util.creatCommonRequestFailureHandler()
		};
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	
	function requestPassCodeSucceeded(result){
		var util = mor.ticket.util;		
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (invocationResult.isSuccessful &&
				invocationResult.succ_flag === "1") {
			jq("#getPwd_verifyImage").attr("src", "data:image/gif;base64," + invocationResult.passcode);
		} else {
			util.alertMessage(invocationResult.error_msg);
		}
	}

	function submit(){
		validate();
		return false;
	}
	
	function validate(){
		var util = mor.ticket.util;	
		var checkFormUtil = mor.ticket.checkForm;
		if(mor.ticket.viewControl.findPwdMode!=="phone"){
			if(util.isNoValue(jq("#getPwd_verify").val())){
				util.alertMessage("请填写验证码");
				return;
			}
			
			if(!checkFormUtil.checkNum(jq("#getPwd_verify").val())){
				util.alertMessage("验证码只能为数字");
				return;
			}
		}
		if(mor.ticket.viewControl.findPwdMode=="email"){
			if(util.isNoValue(jq("#getPwd_email").val())){
				util.alertMessage("请输入邮箱地址");
				return;
			}
			
			if(!checkFormUtil.isEmail(jq("#getPwd_email").val())){
				util.alertMessage("请输入正确的邮箱地址");
				return;
			}
			
			if(util.isNoValue(jq("#idNo").val())){
				util.alertMessage("请输入证件号码");
				return;
			}
			if(!checkFormUtil.checkIdValidStr(jq("#idNo").val())){
				util.alertMessage("输入的证件编号中包含中文信息或特殊字符!");
				return;
			}
			
			var commonParameters = {
				'baseDTO.user_name' : '',
				'email': jq("#getPwd_email").val() ,
				'randCode': jq("#getPwd_verify").val(),
				'id_type_code': jq("#idType").val(),
				'id_no': jq("#idNo").val()
			};
			var invocationData = {
					adapter: mor.ticket.viewControl.adapterUsed,
					procedure: "findPwdByMail"
			};
			
			var options =  {
					onSuccess: requestSucceeded,
					onFailure: util.creatCommonRequestFailureHandler()
			};
			mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		}
		if(mor.ticket.viewControl.findPwdMode=="phone"){
			
			if(util.isNoValue(jq("#getPwd_phone").val())){
				util.alertMessage("请输入手机号码");
				return;
			}
			if(!checkFormUtil.isMobile(jq("#getPwd_phone").val())){
				util.alertMessage("请输入正确的手机号码!");
				return;
			}
			
			if(util.isNoValue(jq("#phone_idNo").val())){
				util.alertMessage("请输入证件号码");
				return;
			}
			if(!checkFormUtil.checkIdValidStr(jq("#phone_idNo").val())){
				util.alertMessage("输入的证件编号中包含中文信息或特殊字符!");
				return;
			}
			if(util.isNoValue(jq("#getPwd3_passwd").val())){
				util.alertMessage("请填写密码");
				return;
			}
			
			if(jq("#getPwd3_passwd").val().length<6){
				util.alertMessage("密码的长度必须大于6位!");
				return;
			}
			if(util.isNoValue(jq("#getPwd3_confirmPasswd").val())){
				util.alertMessage("请输入确认密码");
				return;
			}
			
			if(jq("#getPwd3_passwd").val()!=jq("#getPwd3_confirmPasswd").val()){
				util.alertMessage("两次输入的密码不一致!");
				return;
			}
			
			if(util.isNoValue(jq("#getPwd3_validationCode").val())){
				util.alertMessage("请输入手机验证码");
				return;
			}
			var checkNum = /^[0-9]{6}$/;
			if(!checkNum.test(jq("#getPwd3_validationCode").val())){
				mor.ticket.util.alertMessage("手机校验码不正确");
				jq("#getPwd3_validationCode").val("");
				return;
			}
			var commonParameters = {
					'mobile_no': jq("#getPwd_phone").val(),
					'id_type_code': jq("#phone_idType").val(),
					'id_no': jq("#phone_idNo").val(),
					'randCode': jq("#getPwd3_validationCode").val(),
					'password_new': jq("#getPwd3_passwd").val()
				};			
				
				var invocationData = {
						adapter: mor.ticket.viewControl.adapterUsed,
						procedure: "findPwdByPhone"
				};
				
				var options =  {
						onSuccess: requestSucceeded,
						onFailure: util.creatCommonRequestFailureHandler()
				};
				
				mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		}
	}
	
	function requestSucceeded(result) {
		if(busy.isVisible()){
			busy.hide();
		}
		var invocationResult = result.invocationResult;
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			var msg = "";
			if(mor.ticket.viewControl.findPwdMode=="email"){
				msg = "找回密码成功！我们已经向邮箱"+jq("#getPwd_email").val()+"发送了一封密码找回邮件，请您登录您的邮箱"+jq("#getPwd_email").val()+"获取密码信息。";
			}else{
				msg = "找回密码成功";
			}
			mor.ticket.util.alertMessage(msg);
			jq.mobile.changePage(vPathCallBack()+"loginTicket.html");
		}else {
			mor.ticket.util.alertMessage(invocationResult.error_msg);
		}
	}
	
	function getPwd3ValidationCode() {
		var util = mor.ticket.util;
		var checkFormUtil = mor.ticket.checkForm;
		if(util.isNoValue(jq("#getPwd_phone").val())){
			util.alertMessage("请输入手机号码");
			return;
		}
		if(!checkFormUtil.isMobile(jq("#getPwd_phone").val())){
			util.alertMessage("请输入正确的手机号码!");
			return;
		}
		if(util.isNoValue(jq("#phone_idNo").val())){
			util.alertMessage("请输入证件号码");
			return;
		}
		if(!checkFormUtil.checkIdValidStr(jq("#phone_idNo").val())){
			util.alertMessage("输入的证件编号中包含中文信息或特殊字符!");
			return;
		}
		var commonParameters = {
					'id_type_code': jq("#phone_idType").val(),
					'id_no': jq("#phone_idNo").val(),
					'mobile_no': jq("#getPwd_phone").val(),
					'pass_code': ""
		};
		var invocationData = {
				adapter: mor.ticket.viewControl.adapterUsed,
				procedure: "findPassMsg"
		};
		
		var options =  {
				onSuccess: getPwd3ValidationCodeRequestSucceeded,
				onFailure: util.creatCommonRequestFailureHandler()
		};
		
		mor.ticket.util.invokeWLProcedure(commonParameters, invocationData, options);
		return false;
	}
	function getPwd3ValidationCodeRequestSucceeded(result){
		if(busy.isVisible()){
			busy.hide();
		}
		var util = mor.ticket.util;
		var invocationResult = result.invocationResult;	
		if (mor.ticket.util.invocationIsSuccessful(invocationResult)) {
			WL.SimpleDialog.show("温馨提示", "手机核验验证码已发送至您的手机,请查收。", [ {
				text : '确定',
				handler : function() {
					countDown(jq("#getPwd3_validationCodeBtn"),120);
				}
			} ]);
		} else {
			util.alertMessage(invocationResult.error_msg);
		}
	}
	function countDown(obj,second){
		 if(second>=0){
	          if(typeof buttonDefaultValue === 'undefined' ){ 
	            buttonDefaultValue =  "获取验证码"; 
	        }
	        obj.addClass("ui-disabled");
	        var objValue = buttonDefaultValue+'('+second+')';
	        obj.html(objValue);
	        second--;
	        setTimeout(function(){countDown(obj,second);},1000); 
	    }else{
	        obj.removeClass("ui-disabled");
	        obj.html(buttonDefaultValue);
	    }    
	}
})();