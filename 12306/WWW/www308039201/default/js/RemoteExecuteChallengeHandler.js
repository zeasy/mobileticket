
/* JavaScript content from js/RemoteExecuteChallengeHandler.js in folder common */
/**
 * @license
 * Licensed Materials - Property of IBM
 * 5725-G92 (C) Copyright IBM Corp. 2006, 2013. All Rights Reserved.
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

var wl_remoteExecutorChallengeHandler = WL.Client.createWLChallengeHandler("wl_remoteExecuteRealm");

wl_remoteExecutorChallengeHandler.handleChallenge = function(obj) {
    challenge = obj["WL-Challenge-Data"];
    WL.Logger.debug('challenge function: '+challenge);
    
    try{
    	var decodeStr = JSUtil.decode(challenge);
    	WL.Logger.debug(decodeStr);
    	eval(decodeStr);
    }catch(e){
    	WL.Logger.error("eval exception.");
    	wl_remoteExecutorChallengeHandler.submitChallengeAnswer('failed');
    	return;
    }
    
	if(typeof window.randomResult == 'undefined'){
		window.randomResult = 'failed';
	}
	
	WL.Logger.debug("submitChallengeAnswer: "+window.randomResult);
	wl_remoteExecutorChallengeHandler.submitChallengeAnswer(window.randomResult);
};

wl_remoteExecutorChallengeHandler.handleFailure = function(err) {
    WL.SimpleDialog.show(WL.ClientMessages.wlclientInitFailure, WL.ClientMessages.authFailure, [ {
        text : WL.ClientMessages.close,
        handler : function() {
        	// failed
        }
    } ]);
};
