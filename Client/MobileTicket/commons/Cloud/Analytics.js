/**
 * Created by easy on 15/10/30.
 */

"use strict";
var BaseObject = require('./../BaseObject');
var NativeCloud = require('NativeModules').ZZCloud;

class Analytics extends BaseObject {
    static beginPageView(pageView : String) {
        //NativeCloud.analyticsBeginPageView(pageView);
    }

    static endPageView(pageView : String) {
        //NativeCloud.analyticsEndPageView(pageView);
    }

    static event(event : String,attrs : Object,String,Number) {
        NativeCloud.analyticsEvent(event,attrs);
    }

    static async updateOnlineConfigs() {
        return new Promise(async (resolve, reject)=>{
            NativeCloud.analyticsUpdateOnlineConfigs((result)=>{
                resolve(result);
            },(err)=>{
                reject(err);
            });
        });
    }

    static async onlineConfig(key) {
        return new Promise(async (resolve, reject)=>{
            NativeCloud.analyticsOnlineConfigParam(key,(result)=>{
                resolve(result);
            });
        });
    }
}

exports = module.exports = Analytics;