//
//  ZZTableViewCell.m
//  MobileTicket
//
//  Created by EASY on 15/11/5.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#import "ZZTableViewCell.h"

@interface ZZTableViewCell ()

@end

@implementation ZZTableViewCell

- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher
{
	self = [super init];
	if (self) {
	}
	return self;
}


@end


@implementation _ZZTableViewCell

-(void)setCell:(ZZTableViewCell *)cell {
	if (_cell != cell) {
		[self.contentView addSubview:cell];
		_cell.cell = nil;
		[_cell removeFromSuperview];
		_cell = cell;
		_cell.cell = self;
		_cell.frame = self.contentView.bounds;
		[self setNeedsLayout];
	}
}

-(void)layoutSubviews {
	[super layoutSubviews];
	_cell.frame = self.contentView.bounds;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
	[super setEditing:editing animated:animated];
	if (_cell.onEditing) {
		_cell.onEditing(@{@"editing":@(editing),@"animated":@(animated)});
	}
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
	[super setHighlighted:highlighted animated:animated];
	if (_cell.onHighlighted) {
		_cell.onHighlighted(@{@"highlighted":@(highlighted),@"animated":@(animated)});
	}
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	if (_cell.onSelected) {
		_cell.onSelected(@{@"selected":@(selected),@"animated":@(animated)});
	}
}

@end